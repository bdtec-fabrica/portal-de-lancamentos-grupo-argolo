<?php

define("DATA_LAYER_CONFIG", [
    "driver" => "mysql",
    "host" => "192.122.127.125",
    "port" => "3306",
    "dbname" => "bispoe98_lancamentos_teste",
    "username" => "root",
    "passwd" => "",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

//MIX BAHIA
define("DATA_LAYER_CONFIG2", [
    "driver" => "oci",
    "host" => "192.122.123.254",
    "port" => "1521",
    "dbname" => "orcl",
    "username" => "rms",
    "passwd" => "mbarmsora",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);
//BISPO E DANTAS
define("DATA_LAYER_CONFIG3", [
    "driver" => "oci",
    "host" => "192.122.124.254",
    "port" => "1521",
    "dbname" => "orcldb",
    "username" => "rms",
    "passwd" => "mbarmsora",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);
//NOVO MIX
define("DATA_LAYER_CONFIG4", [
    "driver" => "oci",
    "host" => "192.168.130.254",
    "port" => "1521",
    "dbname" => "orcl",
    "username" => "rms",
    "passwd" => "bancorms",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);
//UNIMAR
define("DATA_LAYER_CONFIG5", [
    "driver" => "oci",
    "host" => "187.94.58.250",
    "port" => "1521",
    "dbname" => "ORAINST7",
    "username" => "rms_select",
    "passwd" => "rms_select",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);
//rede mais
define("DATA_LAYER_CONFIG6", [
    "driver" => "oci",
    "host" => "152.67.48.14",
    "port" => "1521",
    "dbname" => "sky76007",
    "username" => "CONSULTA",
    "passwd" => "C086xBc16z",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG7", [
    "driver" => "oci",
    "host" => "192.122.126.254",
    "port" => "1521",
    "dbname" => "orcln",
    "username" => "rms",
    "passwd" => "mbarmsora",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);
// BemBarato
define("DATA_LAYER_CONFIG8", [
    "driver" => "oci",
    "host" => "157.90.34.30",
    "port" => "1521",
    "dbname" => "orcln",
    "username" => "rms",
    "passwd" => "mbarmsora",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

// Argolos
define("DATA_LAYER_CONFIG10", [
    "driver" => "oci",
    "host" => "192.122.127.254",
    "port" => "1521",
    "dbname" => "orclag",
    "username" => "rms",
    "passwd" => "mbarmsora",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG51", [
    "driver" => "mysql",
    "host" => "192.166.20.150", //LOJA_TIDELLY 5-1   
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG86", [
    "driver" => "mysql",
    "host" => "192.166.49.150", //LOJA_LIDER 8-6  
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG116", [
    "driver" => "mysql",
    "host" => "192.166.41.150", //LOJA_JMD 11-6  
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG124", [
    "driver" => "mysql",
    "host" => "192.166.10.150", //LOJA_JC 12-4
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG132", [
    "driver" => "mysql",
    "host" => "192.166.43.150", //LOJA_IDEAL 13-2
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG140", [
    "driver" => "mysql",
    "host" => "192.166.40.150", //LOJA_LSA 14-0 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG159", [
    "driver" => "mysql",
    "host" => "192.166.35.150", //LOJA_ARATU 15-9 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG175", [
    "driver" => "mysql",
    "host" => "192.168.254.160", //LOJA_LOGOS 17-5  
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG191", [
    "driver" => "mysql",
    "host" => "192.166.50.150", //LOJA_PIRAJA 19-1  
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG256", [
    "driver" => "mysql",
    "host" => "192.166.32.150", //LOJA_CALIFORNIA 25-6
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG272", [
    "driver" => "mysql",
    "host" => "192.166.54.150", //LOJA_JR 27-2
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG310", [
    "driver" => "mysql",
    "host" => "10.10.100.170", //LOJA_SUPERMARKET 31-0  
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG329", [
    "driver" => "mysql",
    "host" => "192.168.100.195", //LOJA_JR 32-9  
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG337", [
    "driver" => "mysql",
    "host" => "192.166.58.150", //LOJA_SUPER_DELI 33-7 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG353", [
    "driver" => "mysql",
    "host" => "192.166.57.150", //LOJA_VAM_MIX 35-3 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG361", [
    "driver" => "mysql",
    "host" => "192.168.19.160", //LOJA_MIX_FEIRA 36-1 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG370", [
    "driver" => "mysql",
    "host" => "192.166.59.150", //LOJA_JR 37-0 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG400", [
    "driver" => "mysql",
    "host" => "192.166.56.150", //LOJA_MIX_JACUIPE 40-0 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG434", [
    "driver" => "mysql",
    "host" => "192.168.1.170", //LOJA_SURPRESA 43-4 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG469", [
    "driver" => "mysql",
    "host" => "192.166.64.160", //LOJA_MIX_7deABRIL 46-9 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG485", [
    "driver" => "mysql",
    "host" => "192.166.65.150", //LOJA_SUPER_PONTA 48-5 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG507", [
    "driver" => "mysql",
    "host" => "192.166.67.150", //LOJA_MIX_AREMBEPE 50-7 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG531", [
    "driver" => "mysql",
    "host" => "192.166.70.150", //LOJA_MIX_CAMACARI 53-1 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG540", [
    "driver" => "mysql",
    "host" => "192.166.71.150:9091", //LOJA_MIX_BONOCO 54-0 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG566", [
    "driver" => "mysql",
    "host" => "192.166.73.150", //LOJA_MIX_SIMOES_FILHO 56-6
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG574", [
    "driver" => "mysql",
    "host" => "192.166.74.150", //LOJA_MIX_SAJ 57-4 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG582", [
    "driver" => "mysql",
    "host" => "192.166.75.150", //LOJA_MIX_VALENCA_GRA?A 58-2 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG590", [
    "driver" => "mysql",
    "host" => "192.166.76.150", //LOJA_MIX_VALENCA_GRA?A 59-0 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG612", [
    "driver" => "mysql",
    "host" => "192.166.77.150", //LOJA_MIX_ITAPUAN 61-2 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG620", [
    "driver" => "mysql",
    "host" => "192.166.78.150", //LOJA_MIX_RSA 62-0 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG698", [
    "driver" => "mysql",
    "host" => "192.166.82.150", //LOJA_MIX_UBAITABA 69-8 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG701", [
    "driver" => "mysql",
    "host" => "192.166.85.150", //LOJA_MIX_CONCORDIA 70-1 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG728", [
    "driver" => "mysql",
    "host" => "192.166.83.150", //LOJA_MIX_PRIME 72-8 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG744", [
    "driver" => "mysql",
    "host" => "192.166.72.150", //LOJA_MIX_PETROPOLIS 74-4 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG752", [
    "driver" => "mysql",
    "host" => "192.166.80.150", //LOJA_MIX_PARQUE_SAO_PAULO 75-2
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG760", [
    "driver" => "mysql",
    "host" => "192.166.84.150", //LOJA_MIX_SAPEACU 76-0 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG787", [
    "driver" => "mysql",
    "host" => "192.166.86.150", //LOJA_MIX_SUPER_DAMACESNO 78-7 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG795", [
    "driver" => "mysql",
    "host" => "192.166.87.150", //LOJA_MIX_SAJ_FEIRA 79-5 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG809", [
    "driver" => "mysql",
    "host" => "192.166.79.150", //LOJA_MIX_MATA_ESCURA 80-9 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("DATA_LAYER_CONFIG850", [
    "driver" => "mysql",
    "host" => "192.166.88.150", //LOJA_MIX_IPIRA 85-0 
    "port" => "3306",
    "dbname" => "retag",
    "username" => "root",
    "passwd" => "123456",
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("SENDGRID", [
    "user" =>"bdtecatende@bispoedantas.com.br",
    "password" =>"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",
    //@senha1234567890
]);

define("URL_BASE", "http://lancamentosnfe.bdtecsolucoes.com.br:8090");

define("SITE", "BDTec");

function url(string $uri = null): string
{
    if($uri) {
        return URL_BASE."{$uri}";
    }

    return URL_BASE;
}

function dd($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
    die;
}

function __output_header__( $_dados = array() ){
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode(
        $_dados
        );
    exit;
}

function echodie($data){
    echo $data;
    die;
}

function dump($data) {
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
    echo "<br>";
}

function telegram($store) {
    switch ($store) {
        case '5':
            return '-1001571163658';
            break;
        
        case '8':
            return '-1001716465433';
            break;
        case '11':
            return '-1001754067200';
            break;
        case '12':
            return '-1001569670447';
            break;
        case '13':
            return '-1001256864181';
            break;
        case '14':
            return '-1001632531252';
            break;
        case '15':
            return '-1001789969248';
            break;
        case '17':
            return '-1001741412037';
            break;
        case '19':
            return '-1001772802702';
            break;
        case '25':
            return '-1001736640739';
            break;
        case '27':
            return '-1001795765195';
            break;
        case '31':
            return '-686028916';
            break;
        case '32':
            return '-1001795765195';
            break;
        case '33':
            return '-1001516346103';
            break;
        case '35':
            return '-1001660667354';
            break;
        case '36':
            return '-1001767708042';
            break;
        case '37':
            return '-1001795765195';
            break;
        case '40':
            return '-1001788834526';
            break;
        case '43':
            return '-1001504068687';
            break;
        case '46':
            return '-1001702052726';
            break;
        case '48':
            return '-1001756598492';
            break;
        case '50':
            return '-1001648073980';
            break;
        case '54':
            return '-1001754067200';
            break;
        case '56':
            return '-1001613537727';
            break;
        case '57':
            return '-1001613537727';
            break;
        case '82':
            return '-1001613537727';
            break;
        case '58':
            return '-1001613537727';
            break;
        case '59':
            return '-1001613537727';
            break;
        case '61':
            return '-1001649324305';
            break;
        case '81':
            return '-722125808';
            break;
        case '67':
            return '-605793858';
            break;
        case '69':
            return '-632237896';
            break;
        case '70':
            return '-686028916';
            break;
        case '72':
            return '-1001613537727';
            break;
        case '74':
            return '-626999027';
            break;
        case '75':
            return '-669602724';
            break;
        case '76':
            return '-1001613537727';
            break;
        case '78':
            return '-1001613537727';
            break;
        case '79':
            return '-1001613537727';
            break;
        case '80':
            return '-1001613537727';
            break;
        case '85':
            return '-1001613537727';
            break;
        case '83':
            return '-764723228';
            break;
        case '87':
            return '-623395379';
            break;
        case '91':
            return '-782747371';
            break;
        case '20':
            return '-680725676';
            break;
        case '21':
            return '-634960097';
            break;
        case '84':
            return '-642708005';
            break;
        case '86':
            return '-667684942';
            break;
        case '95':
            return '-755163081';
            break;
        case '102':
            return '-713089592';
            break;
        case '23':
            return '-796154002';
            break;
        case '93':
            return '-782747371';
            break;
        case '110':
            return '-782747371';
            break;
        case '101':
            return '-1001628197270';
            break;
        case '18':
            return '-722253864';
            break;
        case '49':
            return '-1001750236739';
            break;
        case '97':
            return '-782747371';
            break;
        case '94':
            return '-782747371';
            break;
        case '104':
            return '-699388841';
            break;
        case '105':
            return '-699388841';
            break;
        case '10':
            return '-574907683';
            break;
        case '92':
            return '-752263139';
            break;
        case '111':
            return '-713929296';
            break;
        case '115':
            return '-660722141';
            break;
    }     

}

function telegramNovoMix($store,$type = null) {
    switch ($store) {
        case '7':
            return '-1001719819942';
            break;        
        case '16':
            return '-736151970';
            break;
        case '22':
            return '-796111657';
            break;
        case '38':
            return '-638649930';
            break;        
        case '42':
            return '-501683623';
            break;        
        case '50':
            if ($type == 'E') {
                return '-773297274';
            } else if ($type == 'S') {
                return '-753020988';
            } else {
                return '-773297274';
            }
            
            break;
        case '51':
            if ($type == 'E') {
                return '-773297274';
            } else if ($type == 'S') {
                return '-753020988';
            } else {
                return '-773297274';
            }
            break;        
        case '52':
            if ($type == 'E') {
                return '-773297274';
            } else if ($type == 'S') {
                return '-753020988';
            } else {
                return '-773297274';
            }
            break;        
        case '63':
            return '-613247269';
            break;       
        case '66':
            return '-754291865';
            break;
        case '67':
            if ($type == 'E') {
                return '-773297274';
            } else if ($type == 'S') {
                return '-753020988';
            } else {
                return '-773297274';
            }
            break;
    }
}

function hsmTos($time) {
    list($hora,$minuto, $segundo) = explode(':',$time);
    $calculo = $hora * 3600 + $minuto * 60 + $segundo;
    return $calculo;
}

function sToHsm($seconds) {
    $hour = floor($seconds / 3600);
    $minutes = floor($seconds % 3600 / 60);
    $seconds = floor($seconds % 3600 % 60);

    return str_pad($hour,2,'0').':'.str_pad($minutes,2,'0').':'.str_pad($seconds,2,'0');
}

function pastaImportacaoRMS($store,$accessKey,$group) {
    if ($group == 1 || $group == 10) {
        $ok = copy( __DIR__."\XML\\$store\NFe$accessKey.xml",__DIR__."\XML_IMPORTAR\NFe$accessKey.xml");
        $output = shell_exec(__DIR__."\importacao.bat");
    } else if ($group ==  2 || $group == 7){
        $ok = copy( __DIR__."\XML\\$store\NFe$accessKey.xml",__DIR__."\XML_IMPORTAR_BD\NFe$accessKey.xml");
        $output = exec(__DIR__."\importacao2.exe");
    }
    
    return $output;
}

function somarHoras($h1,$h2) {
    
    if (!is_string($h1) or !is_string($h2)) {
        return '00:00';
    }
    
    list($hora,$minuto, $segundo) = explode(':',$h1);
    $calculo = ($hora * 3600) + ($minuto * 60) + $segundo;

    list($hora2,$minuto2, $segundo2) = explode(':',$h2);
    $calculo2 = ($hora2 * 3600) + ($minuto2 * 60) + $segundo2;
    $calculo = $calculo + $calculo2;
    $hour = floor($calculo / 3600);
    $minutes = floor($calculo % 3600 / 60);
    $seconds = floor($calculo % 3600 % 60);
    $total = str_pad($hour,2,'0',STR_PAD_LEFT).':'.str_pad($minutes,2,'0',STR_PAD_LEFT).':'.str_pad($seconds,2,'0',STR_PAD_LEFT);
    return $total;
}

function subtrairHoras($h1,$h2) {
    if (is_string($h1) or is_string($h2)) {
        return '00:00';
    }
    list($hora,$minuto, $segundo) = explode(':',$h1);
    $calculo = ($hora * 3600) + ($minuto * 60) + $segundo;

    list($hora2,$minuto2, $segundo2) = explode(':',$h2);
    $calculo2 = ($hora2 * 3600) + ($minuto2 * 60) + $segundo2;

    $calculo = $calculo - $calculo2;
    $hour = floor($calculo / 3600);
    $minutes = floor($calculo % 3600 / 60);
    $seconds = floor($calculo % 3600 % 60);
    $total = str_pad($hour,2,'0',STR_PAD_LEFT).':'.str_pad($minutes,2,'0',STR_PAD_LEFT).':'.str_pad($seconds,2,'0',STR_PAD_LEFT);
    return $total;
}

function diferencaHorasTimestamp($date1, $date2) {
    $datatime1 = new DateTime($date1);
    $datatime2 = new DateTime($date2);

    $data1  = $datatime1->format('Y-m-d H:i:s');
    $data2  = $datatime2->format('Y-m-d H:i:s');

    $diff = $datatime1->diff($datatime2);
    $horas = $diff->i + ($diff->h *60) +(($diff->days * 24) * 60);
    return $horas;
}

function inputDateToRMSDate($date) {
    return ('1'.substr($date,2,2).substr($date,5,2).substr($date,8,2));
}

function dateRMSToDate($date) {
    if ($date) {
        return substr($date,5,2)."-".substr($date,3,2)."-20".substr($date,1,2);
    } else {
        return '';
    }
    
}

function dateRMSToDateI($date) {
    return "20".substr($date,1,2)."-".substr($date,3,2)."-".substr($date,5,2);
}

function i0($data, $data2) {
    if($data == null && $data2 != null) {
        return $data2;
    } else if ($data == null && $data2 == null){
        return 1;
    } else {
        return $data;
    }
}

//finalizadoras MB
function finalizaMB($variable) {
    switch ($variable) {
        case '1':
            return 'DINHEIRO';
            break;
        
        case '2':
            return 'CHEQUE A VISTA';
            break;
        case '3':
            return 'CHEQUE PRE';
            break;
        case '4':
            return 'CARTAO CREDITO';
            break;
        case '5':
            return 'C CRED PARC LOJA';
            break;
        case '6':
            return 'C CRED PARC ADM';
            break;
        case '7':
            return 'CARTAO DEBITO';
            break;
        case '8':
            return 'CARTAO CRED POS';
            break;
        case '9':
            return 'CARTAO DEB POS';
            break;
        case '10':
            return 'TICKET PAPEL';
            break;
        case '12':
            return 'TROCA';
            break;
        case '13':
            return 'PRIVATE LABEL';
            break;
        case '14':
            return 'CADERNETA';
            break;
        case '15':
            return 'C VOUCHER';
            break;
        case '16':
            return 'C VOUCHER POS';
            break;
        case '17':
            return 'CRM FIDELIDADE';
            break;
        case '18':
            return 'AME DIGITAL';
            break;
        case '19':
            return 'CARTEIRA DIGITAL TEF';
            break;
        case '20':
            return 'CARTEIRA DIGITAL POS';
            break;
        
    }

    function userBD() {
        // AMANDA  
        // BDCRIS
        // BDILANA 
        // BDLORENA
        // BDMILENA
        // BDPALOMA
        // BDSTEFAN
        // CRISTINA
        // ELINALDO
        // IURI    
        // KARIELLY
        // KLARA   
        // MENDONCA
        // NATHALYA
        // NATALIE
    }
         
}