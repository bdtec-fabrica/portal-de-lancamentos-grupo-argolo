<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class NCM extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("ncm",[],"id", true);        
    }
}