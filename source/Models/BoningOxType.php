<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class BoningOxType extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("boning_ox_type",[],"id", false);        
    }
    
}