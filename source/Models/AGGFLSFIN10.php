<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer10;

class AGGFLSFIN10 extends DataLayer10
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AGG_FLS_FIN",[],"", false);        
    }
}