<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ReasonPendingClient extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("reason_pending_client",[],"id", true);        
    }
    
}