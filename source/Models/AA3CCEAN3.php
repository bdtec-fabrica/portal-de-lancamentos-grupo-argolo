<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer4;


class AA3CCEAN3 extends DataLayer4
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA3CCEAN",[],"", false);        
    }

    public function aa3citem() {
        $aa3citem = (new AA3CITEM3())->find("GIT_COD_ITEM = ".substr($this->EAN_COD_PRO_ALT,0,strlen($this->EAN_COD_PRO_ALT)-1))->fetch();
        return $aa3citem;
    }

    public function aa1forit() {
        $aa1forit = (new AA1FORIT3())->find("FORITE_COD_ITEM = ".substr($this->EAN_COD_PRO_ALT,0,strlen($this->EAN_COD_PRO_ALT)-1))->fetch(true);
        return $aa1forit;
    }

    public function aa2ctipo() {
        $aa2ctipo = (new AA2CTIPO3())->find("TIP_CODIGO = ".$this->aa3citem()->GIT_COD_FOR)->fetch();
        return $aa2ctipo;
    }
}