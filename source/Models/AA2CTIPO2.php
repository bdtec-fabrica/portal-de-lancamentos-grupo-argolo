<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer3;

class AA2CTIPO2 extends DataLayer3
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA2CTIPO",[],"", false);        
    }

    public function aa1forit() {
        $aa1forit = (new AA1FORIT2())->find("FORITE_COD_FORN = $this->TIP_CODIGO")->fetch(true);
        return $aa1forit;
    }
}