<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use DateTime;

class RegistroCadastro extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("registro_cadastro",[],"id", true);        
    }

    public function operator() {
        $user = (new User())->find("id = $this->operator_id")->fetch();
        return $user;
    }
    
}