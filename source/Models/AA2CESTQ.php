<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AA2CESTQ extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA2CESTQ",[],"", false);        
    }

    public function code () {
        return substr($this->GET_COD_PRODUTO,0,strlen($this->GET_COD_PRODUTO)-1);
    }

    public function store () {
        return substr($this->GET_COD_LOCAL,0,strlen($this->GET_COD_LOCAL)-1);
    }

    public function getAA1LINHP() {
        $aa1linhp = (new AA1LINHP())->find("LINP_COD_ITEM = :lci and LIMP_FILIAL = :lf","lci=$this->code&lf=$this->store")->fetch();
        return $aa1linhp;
    }

    public function getAA3CITEM () {
        $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM||GIT_DIGITO = :gci","gci=$this->GET_COD_PRODUTO")->fetch();
        return $aa3citem;
    }    

    public function dtUltEnt(){
        if ($this->GET_DT_ULT_ENT == 0){
            return 0;
        } else {
            return substr(str_pad((string)$this->GET_DT_ULT_ENT, 6, "0", STR_PAD_LEFT),0,2)."/".substr(str_pad((string)$this->GET_DT_ULT_ENT, 6, "0", STR_PAD_LEFT),2,2)."/".substr(str_pad((string)$this->GET_DT_ULT_ENT, 6, "0", STR_PAD_LEFT),4,2);
        }
        
    }

    public function dtUltFat(){
        if ($this->GET_DT_ULT_FAT == 0){
            return 0;
        } else {
            return substr(str_pad((string)$this->GET_DT_ULT_FAT, 6, "0", STR_PAD_LEFT),0,2)."/".substr(str_pad((string)$this->GET_DT_ULT_FAT, 6, "0", STR_PAD_LEFT),2,2)."/".substr(str_pad((string)$this->GET_DT_ULT_FAT, 6, "0", STR_PAD_LEFT),4,2);
        }
        
    }

    public function getPrice() {
        $data = "1".date("ymd");
        $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $data","pl=$this->store&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_PRECO;
        }
        return $price;
    }
}