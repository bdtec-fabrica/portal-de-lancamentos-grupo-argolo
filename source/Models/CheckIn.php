<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use DateTime;

class CheckIn extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("check_in",[],"id", true);        
    }

    public function getDaily(){
        $daily = (new Daily())->find("id = :id","id=$this->daily_id")->fetch();
        return $daily;
    }
}