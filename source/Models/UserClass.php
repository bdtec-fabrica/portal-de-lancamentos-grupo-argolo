<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class UserClass extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("user_class",["description", "type"],"id", false);        
    }
}