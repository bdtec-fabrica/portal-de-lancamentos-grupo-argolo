<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class FiscalPeriod extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("fiscal_period",[],"id", false);        
    }
    
}