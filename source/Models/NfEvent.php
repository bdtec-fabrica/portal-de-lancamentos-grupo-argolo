<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use DateTime;

class NfEvent extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfe_event",[],"id", true);        
    }

    public function getAverageDuration($status) {
        $date = date('Y-m-d');
        $events = $this->find("status_id = :status_id and substr(created_at,1,10) = :date","status_id=$status&date=$date")->fetch(true);
        $count = 0;
        foreach($events as $event){
            $dateTime = new DateTime($event->created_at);
            if ($event->updated_at == $event->created_at) {
                $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            } else {
                $diff = $dateTime->diff(new DateTime($event->updated_at));
            }
            
            $hours += $diff->format('%H');
            $minutes += $diff->format('%i');
            $count++;   
        }
        
        $m = (integer)$minutes/60;
        $hours = $hours + $m;
        if($count >0){
            $hours = (integer) ($hours /$count);
        }
        

        if ($hours == 0) {
            if($count >0){
                $minutes = (integer)(($minutes)/$count);
            }
            
        } else if ($hours > 0) {
            $minutes = (integer)(($minutes%60)/$count);
        }
        
        if (!is_int($hours)){
            $hours = 0;
        }
        return str_pad($hours, 2, "0", STR_PAD_LEFT).":".str_pad($minutes, 2, "0", STR_PAD_LEFT);
    }

    public function getIdsDuration($status) {
        $date = date('Y-m-d');
        $events = $this->find("status_id = :status_id and substr(created_at,1,10) = :date","status_id=$status&date=$date")->fetch(true);
        $count = 0;
        foreach($events as $event){
            $ids[$count] = $event->id_nf_receipt;
            $count++;
        }
        if(is_array($ids)){
            $ids = implode(', ',$ids);
        }
              
        return $ids; 
    }

    public function getAverageStatusId($id,$status) {
        $count = (new Nfevent())->find("id_nf_receipt = :id and status_id = :status","status=$status&id=$id")->count();
        if ($count >0) {
            $events = (new Nfevent())->find("id_nf_receipt = :id and status_id = :status","status=$status&id=$id")->fetch(true);
            foreach($events as $event){
                $dateTime = new DateTime($event->created_at);
                $diff = $dateTime->diff(new DateTime($event->updated_at));
                return $diff->format("P%YY%MM%dDT%hH%iM");
            }
        } else {
            return "P0Y0M0DT0H0M0S";
        }
        
           
    }
    public function getAverageDaysStatusId($id,$status) {
        $count = (new Nfevent())->find("id_nf_receipt = :id and status_id = :status","status=$status&id=$id")->count();
        if ($count >0) {
            $events = (new Nfevent())->find("id_nf_receipt = :id and status_id = :status","status=$status&id=$id")->fetch(true);
            foreach($events as $event){
                $dateTime = new DateTime($event->created_at);
                $diff = $dateTime->diff(new DateTime($event->updated_at));
                return $diff->format("%dD");
            }
        } else {
            return "0D";
        }
        
           
    }

    public function getAverageDurationPeriod($status,$initial, $final) {

        $events = $this->find("status_id = :status_id and concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) >= :date1 and concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) <= :date2","status_id=$status&date1=$initial&date2=$final")->fetch(true);
        $count = 0;
        foreach($events as $event){
            $dateTime = new DateTime($event->created_at);
            if ($event->updated_at == $event->created_at) {
                $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            } else {
                $diff = $dateTime->diff(new DateTime($event->updated_at));
            }
            
            $hours += $diff->format('%H');
            $minutes += $diff->format('%i');
            $count++;   
        }
        
        $m = (integer)$minutes/60;
        $hours = $hours + $m;
        if($count >0){
            $hours = (integer) ($hours /$count);
        }
        

        if ($hours == 0) {
            if($count >0){
                $minutes = (integer)(($minutes)/$count);
            }
            
        } else if ($hours > 0) {
            $minutes = (integer)(($minutes%60)/$count);
        }
        
        if (!is_int($hours)){
            $hours = 0;
        }
        return str_pad($hours, 2, "0", STR_PAD_LEFT).":".str_pad($minutes, 2, "0", STR_PAD_LEFT);
    }

    public function getIdsDurationPeriod($status,$initial, $final) {
        $date = date('Y-m-d');
        $events = $this->find("status_id = :status_id and concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) >= :date1 and concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) =<= :date2","status_id=$status&date1=$initial&date2=$final")->fetch(true);
        $count = 0;
        foreach($events as $event){
            $ids[$count] = $event->id_nf_receipt;
            $count++;
        }
        if(is_array($ids)){
            $ids = implode(', ',$ids);
        }
              
        return $ids; 
    }

    public function getAmount() {
        return (new NfReceipt())->findById($this->id_nf_receipt)->getAmountItems;
    }

    public function getFinalUser() {
        $user = (new User())->findById($this->final_user_id);
        if (!$user) {
            return '';
        } else {
            return $user->name;
        }
    }

    public function getFinalUserClass() {
        $user = (new User())->findById($this->final_user_id);
        if (!$user) {
            return '';
        } else {
            return $user->class;
        }
    }

    public function getAmountItems() {
        return (new RegisterAmountItems())->find("nf_receipt_id = :nri","nri=$this->id_nf_receipt")->fetch()->amount;
    }
}