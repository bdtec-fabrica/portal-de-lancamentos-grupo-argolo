<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;
use \DateInterval;

class NfReceipt2 extends DataLayer
{

    public function save2() {
        $date = date("Y-m-d");
        $nfReceipt = (new NfReceipt2())->find("nf_number = :nn and access_key = :ak and substr(created_at,1,10) = :date1 and store_id = :si",
        "nn=$this->nf_number&ak=$this->access_key&date1=$date&si=$this->store_id")->count();
        if ($nfReceipt > 0) {
            return false;
        } else {
            return $this->save();
        }
    }

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_receipt_2",["store_id","group_id","nf_number","status_id","client_id","operator_id","type_id","operation_id","inclusion_type"],"id", true);        
    }
    public function getCnpjFornecedor() {
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $CNPJ = (string)$xml->NFe->infNFe->emit->CNPJ;
            return $CNPJ;
        } else {
            return '';
        }
        
    }

    public function getStore() {
        $store = (new Store());
        $store = $store->find("id = :store_id and group_id = :group_id","store_id={$this->store_id}&group_id={$this->group_id}")->fetch();
        return $store->company_name;
    }

    public function store() {
        $store = (new Store())->find("code = :code","code=$this->store_id")->fetch();
        return $store;
    }

    public function getDueDate() {
        $dueDate = new NfDueDate();
        $dueDate = $dueDate->find("nf_id = :nf_id","nf_id=$this->id")->fetch(true);
        return $dueDate;

    }

    public function getNfeDueDate() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $dup = $xml->NFe->infNFe->cobr->dup;
        return $dup;
        // foreach($xml->NFe->infNFe->cobr->dup as $dup) {
        //     echo "<pre>";
        //     var_dump($dup);
        //     // ->dup->dVenc
        //     echo "</pre>";
        // }
        
    }

    public function getItems() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $det = $xml->NFe->infNFe->det;
        return $det;
    }

    public function getItemsIds($data){
        
        $requests = (new NfReceipt2())->find("id in ($data)")->fetch(true);
        foreach($requests as $request){
            $count += $request->getAmountItems;
        }
        return $count;
    }

    public function getAmountItems() {
        if(strlen($this->access_key)==44){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $dets = $xml->NFe->infNFe->det;
            $count = 0;
            foreach ($dets as $det) {
                $count++;
            } return $count;
        } else {
            return $this->items_quantity;
        }
        
    }

    public function getFornecedor() {
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $xNome = $xml->NFe->infNFe->emit->xNome;
            return $xNome;
        } else {
            return '';
        }
        
    }

    public function getGroup() {
        $group = (new Group());
        $group = $store->find("id = :group_id","group_id={$this->group_id}")->fetch();
        return $group->name;
    }

    public function getStatus() {
        $status = (new NfStatus());
        $status = $status->find("id = :status_id","status_id={$this->status_id}")->fetch();
        return $status->name;
    }

    public function getClient() {
        $client = (new User());
        $client = $client->find("id = :client_id","client_id={$this->client_id}")->fetch();
        return $client->name;
    }

    public function getOperator() {
        $operator = (new User());
        $operator = $operator->find("id = :operator_id","operator_id={$this->operator_id}")->fetch();
        return $operator->name;
    }

    public function getType() {
        $type = (new NfType());
        $type = $type->find("id = :type_id","type_id={$this->type_id}")->fetch();
        return $type->name;
    }

    public function getOperation() {
        $operation = (new NfOperation());
        $operation = $operation->find("id = :operation_id","operation_id={$this->operation_id}")->fetch();
        return $operation->name;
    }

    public function getOperationColor() {
        $operation = (new NfOperation());
        $operation = $operation->find("id = :operation_id","operation_id={$this->operation_id}")->fetch();
        return $operation->color;
    }

    public function getInclusion() {
        if ($this->inclusion_type == 0) {
            return "Manual";
        } else {
            return "Painel";
        }
    }

    public function clientGroup()
    {
        $group = (new User())->findById($this->client_id)->store_group_id;
        if ($group) {
            $group = (new StoreGroup())->findById($group);
            return $group->name;
        } else {
            return 0;
        }
        
        
    }

    public function getCreatedAt()
    {
        return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)."<br>".substr($this->created_at,11);
    }

    public function getReport() {
        $nfReport = (new NfReport())->find("id_request = :id_request","id_request=$this->id")->count();
        return $nfReport;
    }

    public function getReportData() {
        $nfReport = (new NfReport())->find("id_request = :id_request","id_request=$this->id")->fetch();
        return $nfReport;
    }

    public function getUpdatedAt()
    {
        return substr($this->updated_at,8,2)."/".substr($this->updated_at,5,2)."/".substr($this->updated_at,0,4)."<br>".substr($this->updated_at,11);
    }

    public function getCreatedAtInt()
    {
        return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)." ".substr($this->created_at,11);
    }

    public function getAttachment()
    {
        $attachment = (new NfAttachment());
        $attachment = $attachment->find("nf_id = :id","id={$this->id}")->fetch(true);
        return $attachment;
    }

    public function nfAdd($storeId, $nfNumber, $statusId, $clientId, $operatorId, $itemsQuantity, $typeId, $operationId, $inclusioType, $accessKey = null, $note = null) {
        $this->store_id = $storeId;
        $this->nf_number = $nfNumber;
        $this->status_id = 1;
        $this->client_id = $clientId;
        $this->operator_id = $operatorId;
        $this->type_id = $typeId;
        $this->operation_id = $operationId;
        $this->inclusion_type = $inclusioType;
        if ($accessKey) {
            $this->access_key = $accessKey;
        }
        if ($note) {
            $this->note = $note;
        }
        $save = $this->save;
        return $save;
    }

    public function userAdministrator() {
        $user = (new User())->findById($this->client_id);
        return $user;
    }

    public function userOperator() {
        $user = (new User())->findById($this->operator_id);
        return $user->name;
    }

    public function saveLog($user,$operatorId, $nfNumber, $accessKey, $status, $itemsQuantity, $type, $operationId,$note, $id)
    {   

        $request = (new NfReceipt2())->findByID($id);
        if ($operatorId != $request->operator_id) {
            $descriptionLog[0] = "Operador alterado de  '$request->operator_id' para '$operatorId' \n";
        }
        if ($nfNumber != $request->nf_number) {
            $descriptionLog[1] = "Número do documento alterado de $request->nf_number para $nfNumber \n";
        }
        if ($accessKey != $request->access_key) {
            $descriptionLog[2] = "Chave de acesso alterada de '$this->access_key' para '$accessKey' \n";
        }
        if ($status != $request->status_id) {
            $descriptionLog[3] = "Status alterado de '$request->status_id' para '$status' \n";
            $nfEventCount = (new NfEvent())->find("situation = :situation and id_nf_receipt = :id_nf_receipt","situation=A&id_nf_receipt=$id")->count();
            if ($nfEventCount == 0) {
                $nfEvent = new NfEvent();
                $nfEvent->id_nf_receipt = $id;
                $nfEvent->status_id = $status;
                $nfEvent->user_id = $user;
                $nfEvent->situation = 'A';
                $nfEventId = $nfEvent->save();

            } else {
                $nfEventRegister = (new NfEvent())->find("situation = :situation and id_nf_receipt = :id_nf_receipt","situation=A&id_nf_receipt=$id")->fetch();
                if ($nfEventRegister->status_id != $status) {
                    $nfEventUpdate = (new NfEvent())->findById($nfEventRegister->id);
                    $nfEventUpdate->situation = 'F';
                    $nfEventUpdate->final_user_id = $user;
                    $nfEventUpdateId = $nfEventUpdate->save();

                    $nfEvent = new NfEvent();
                    $nfEvent->id_nf_receipt = $id;
                    $nfEvent->status_id = $status;
                    $nfEvent->user_id = $user;
                    $nfEvent->situation = 'A';
                    $nfEventId = $nfEvent->save();
                }
            }
        }	
        if ($itemsQuantity != $request->items_quantity) {
            $descriptionLog[4] = "Quantidade de itens alterada de '$request->items_quantity' para '$itemsQuantity' \n";
        }
        if ($type != $request->type_id) {
            $descriptionLog[5] = "Tipo de nota alterado de '$request->type_id' para '$type' \n";
        }
        if ($operationId != $request->operation_id) {
            $descriptionLog[6] = "Operação alterada de '$request->operation_id' para '$operationId' \n";
        }
        if ($note != $request->note) {
            $descriptionLog[7] = "Observação alterada de '$request->note' para '$note' \n";
        }
        
        if ($descriptionLog) {
            $descriptionLog = implode(",", $descriptionLog);
            $requestLog = new NfReceiptLog();
            $requestLog->description = $descriptionLog;
            $requestLog->user_id = $user;
            $requestLog->nf_receipt_id = $id;
            $requestLogId = $requestLog->save();
        } else {
            $requestLogId = true;
        }      
        
        if ($requestLogId) {
            return $this->save();
        }else{
            return false;
        }
        
    }

    public function getLogs()
    {
        return (new NfReceiptLog())->find("nf_receipt_id = :rid","rid={$this->id}")->order("id ASC")->fetch(true);
    }

    public function getAmount1(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6)")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1 ) {
                if ($request->operation_id == 1 ){
                    $count++; 
                }
                if ($request->operation_id == 4){
                    $count++; 
                }
                if ($request->operation_id == 11){
                    $count++; 
                }
                if ($request->operation_id == 12){
                    $count++; 
                }
            }
        }
        return $count;
    }

    public function getAmount2(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount4(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount6(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount8(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount24(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getNf1(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6)")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1 ) {
                if ($request->operation_id == 1 ){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 4){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 11){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 12){
                    $count++;
                    $nf[$count] = $request->id;
                }
                
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf2(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        
        return $count;
    }

    public function getNf4(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf6(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf8(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf24(){
        $requests = (new NfReceipt2())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getAmountPC0(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') < 1 ) {
                $count++;                    
            }
        }
        return $count;
    }

    public function getAmountPC1(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1 && $diff->format('%d') < 2) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC2(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 2 && $diff->format('%d') < 4) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC4(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 4 && $diff->format('%d') < 7) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC7(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 7 && $diff->format('%d') < 10) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC10(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 10) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getNfPC0(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC1(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1 && $diff->format('%d') < 2) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC2(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 2 && $diff->format('%d') < 4) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC4(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 4 && $diff->format('%d') < 7) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC7(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 7 && $diff->format('%d') < 10) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC10(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 10) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function inRMS(){
        if (strlen($this->access_key) == 44){
            $nfRMS = (new NfRms())->find("access_key = :access_key and store_id = :sid","access_key=$this->access_key&sid=$this->store_id")->count();
        } else {
            $nfRMS = (new NfRms())->find("nf_number = :nf_number and store_id = :store_id","store_id=$this->store_id&nf_number=$this->nf_number")->count();
        }
        return $nfRMS;
    }

    public function getTimeDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }
        $dia = $diff->format("%d");
        $dia = $dia." dia(s), ";
        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%Y-%m-%d %H:%i"));
        $date->sub(new DateInterval($event->getAverageStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $dia.$date->format("H:i:s");
        } else {
            return $date->format('H:i:s');
        }
        
    }

    public function getDaysDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }

        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%d"));
        $date->sub(new DateInterval($event->getAverageDaysStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $date->format("d");
        } else {
            return $date->format('d');
        }
        
    }

    public function getTimesDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }
        $dia = $diff->format("%d");
        $dia = $dia." dia(s), ";
        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%Y-%m-%d %H:%i"));
        $date->sub(new DateInterval($event->getAverageStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $dia.$date->format("H:i:s");
        } else {
            return $date->format('H:i:s');
        }
        
    }

    public function electronicInvoiceRecorded(){
        // if (strlen($this->access_key) == 44){
        //     $nfRMS = (new NfRms())->find("access_key = :access_key and store_id = :sid","access_key=$this->access_key&sid=$this->store_id")->count();
        // } else {
        //     $nfRMS = (new NfRms())->find("nf_number = :nf_number and store_id = :store_id","store_id=$this->store_id&nf_number=$this->nf_number")->count();
        // }
        // return $nfRMS;
        // if ($count = 0){
        //     return false;
        // } else {
        //     return true;
        // }
    }

    public function getDuedateAmount() {
        $dueDate = new NfDueDate();
        $dueDate = $dueDate->find("nf_id = :nf_id","nf_id=$this->id")->count();
        return $dueDate;
    }

    public function getAA1CFISC() {
        
        $aa1cfisc = (new AA1CFISC())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34")->fetch(true);
        foreach ($aa1cfisc as $nR) {
            if (strlen($this->access_key) == 44 ) {
                if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                    $nfReceiptReturn = $nR;
                }
            } else {
                if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                    $nfReceiptReturn = $nR;
                }
                
            }
            
        }
        if (!$nfReceiptReturn) {
            $aa1cfisc2 = (new AA1CFISC2())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34")->fetch(true);
            foreach ($aa1cfisc2 as $nR) {
                if (strlen($this->access_key) == 44 ) {
                    if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                        $nfReceiptReturn = $nR;
                    }
                } else {
                    if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                        $nfReceiptReturn = $nR;
                    }
                }  
            }
        }
        
        if (!$nfReceiptReturn) {
            return "Não encontrada.";
        } else {
            return "Encontrada.";
        }
        
    }

    public function getAmountCC1(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1  && $diff->format('%H') < 2  && $diff->format('%d') < 1){
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC2(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC4(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC6(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC8(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC24(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getRequestCC1(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1  && $diff->format('%H') < 2  && $diff->format('%d') < 1){
                $count++;
                $nf[$count] = $request->id;
                
            }
        }
        if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        return $count;
    }

    public function getRequestCC2(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        return $count;
    }

    public function getRequestCC4(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        return $count;
    }

    public function getRequestCC6(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        return $count;
    }

    public function getRequestCC8(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        return $count;
    }

    public function getRequestCC24(){
        $requests = (new NfReceipt2())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
$count = implode(",",$nf);
} else {
$count = 0;
}
        return $count;
    }

}