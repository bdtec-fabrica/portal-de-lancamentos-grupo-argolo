<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class NfRms extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_rms",[],"id", true);        
    }

    public function inPortal(){
        if (strlen($this->access_key) == 44){
            $nfPortal = (new NfReceipt())->find("access_key = :access_key","access_key=$this->access_key")->count();
        } else {
            $nfPortal = (new NfReceipt())->find("nf_number = :nf_number and store_id = :store_id","store_id=$this->store_id&nf_number=$this->nf_number")->count();
        }
        if($nfPortal > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}