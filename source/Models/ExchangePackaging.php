<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ExchangePackaging extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("exchange_packaging",["store_id","cnpj","packing_of","packing_for","quantity_for"],"id", false);        
    }
}