<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class User extends DataLayer
{
    public function __construct()
    {
        #$entity, array $required, $primary, $timestamp
        parent::__construct("users",["name","email","password","store_code","class"],"id", true);        
    }

    public function getCheckIns($date,$id) {
        $daily = (new Daily())->find("substr(created_at,1,10) = '$date' and user_id = $id")->fetch();
        $i = 1;
        if ($daily) {
            foreach ($daily->getCheckIns() as $checkIn) {
                $data[$i] = $checkIn->created_at;
                $i++;
            }
        }
        
        return $data;
    }

    public function getStoreGroup()
    {
        $userStoreGroup = (new UserStoreGroup())->find("user_id = :user_id","user_id={$this->id}")->fetch(true);
        return $userStoreGroup;
    }
    
    public function getUsersForClass($class){
        $users = $this->find("class = :class","class=$class")->fetch(true);
        foreach ($users as $user){
            $userClass[$user->id] = $user->id;
        }

        if (is_array($userClass)){
            return  implode(",",$userClass);
        } else {
            return "class = :class "." class=$class";
        }
    }

    public function getStoreGroup2()
    {
        $userStoreGroup = (new UserStoreGroup())->find("user_id = :user_id","user_id={$this->id}")->fetch(true);
        if (!empty($userStoreGroup)) {
            foreach ($userStoreGroup as $usg){
                $stores = (new Store())->find("group_id = :group_id","group_id=$usg->store_group_id")->fetch(true);
                foreach ($stores as $store){
                    $storeCodes[$store->code] = $store->code;
                }
            }
        }
        
        if (is_array($storeCodes)){
            return implode(",",$storeCodes);
        } else {
            return '';
        }
       
    }

    public function getGroup()
    {
        return (new Group())->findById($this->store_group_id);
    }

    // public function getUserSector()
    // {
    //     $userSector = (new UserSector())->find("user_id = :user_id","user_id={$this->id}")->fetch(true);
    //     return $userSector;
    // }

    public function validateGroup($id)
    {
        $userStoreGroup = (new UserStoreGroup())->find("user_id = :user_id and store_group_id = :store_group_id","user_id={$this->id}&store_group_id={$id}")->count();
        if ($userStoreGroup >0) {
            return true;
        } else {
            return false;
        }
    }

    // public function validateSector($id)
    // {
    //     $userSector = (new UserSector())->find("user_id = :user_id and sector_id = :sector_id","user_id={$this->id}&sector_id={$id}")->count();
    //     if ($userSector >0) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // public function permissions()
    // {
    //     return (new Permission())->find("id = :uid", "uid={$this->id}")->fetch(true);
    // }

    // public function clocks()
    // {
    //     return (new Clock())->find("id = :uid", "uid={$this->id}")->fetch(true);
    // }

    // public function getId()
    // {
    //     return intval($this->id);
    // }

    public function getLogin()
    {
        return ucfirst($this->name);
    }

    public function getName($id)
    {
        return (new User())->findById($id)->name;
    }

    public function getUserClass()
    {
        return (new UserClass())->find("type = :type","type={$this->class}")->fetch();
    }

    public function userEdit($name = null,$email = null,$store_code = null,$class = null, $id,$group= null, $tell = null, $whatsapp = null, $admissao = null, $libera = null)
    {
        $validation = true;

        if ($name) {
            $this->name = $name;
            $this->save();
        }

        if ($whatsapp) {
            $this->whatsapp = $whatsapp;
            $this->save();
        }

        if ($tell) {
            $this->tell = $tell;
            $this->save();
        }
        
        if ($email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)!= false) {
                $this->email = $email;
                $this->save();
            } else {
                $validation = false;
            }
            
        }
        if ($store_code) {
            $this->store_code = $store_code;
            $this->save();
            
        }

        if ($group) {
            $this->store_group_id = $group;
            $this->save();
            
        }

        if ($libera) {
            $this->libera = $libera;
            $this->save();
            
        }

        if ($admissao) {
            $this->admissao = $admissao;
            $this->save();
        }

        if ($class) {
            $this->class = $class;
            $this->save();            
        }

        if ($validation == false) {
            return false;
        } else {
            return true;
        }

    }

    public function parameters () {
        $parameters = (new ParameterOperator())->find("user_id = $this->id")->fetch(true);
        return $parameters;
    }

}