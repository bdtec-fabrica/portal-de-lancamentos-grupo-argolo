<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class FiscalActivityType extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("fiscal_activity_type",[],"id", false);        
    }
    
}