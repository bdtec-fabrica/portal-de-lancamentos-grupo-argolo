<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ParameterProvider extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("parameter_provider",[],"id", true);        
    }
    
}