<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class NfReport extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_report",["id_request","note"],"id", true);        
    }
    
    public function getType(){
        $reportType = (new NfReportType())->findById($this->type)->name;
        return $reportType;
    }

    public function getOperator() {
        $user = (new User())->findById($this->user_id);
        return $user;
    }

    public function getOperatornName() {
        $user = (new User())->findById($this->operator_id);
        return $user;
    }

    public function getStore() {
        $store = (new NfReceipt())->findById($this->id_request)->store_id;
        return $store;
    }

    public function getNfReceipt() {
        $store = (new NfReceipt())->findById($this->id_request);
        return $store;
    }
}