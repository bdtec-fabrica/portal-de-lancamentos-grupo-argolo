<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class FiscalActivity extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("fiscal_activity",[],"id", false);        
    }

    public function getUserName() {
        $user = (new User())->findById($this->user_id);
        return $user->name;
    }

    public function getStoreCode() {
        $store = (new FiscalStores())->findById($this->store_id);
        return $store->code;
    }

    public function getPeriodName() {
        $store = (new FiscalPeriod())->findById($this->period_id);
        return $store->name;
    }

    public function getTypeName() {
        $store = (new FiscalType())->findById($this->type_id);
        return $store->name;
    }

    public function getTypeActivityName() {
        $store = (new FiscalActivityType())->findById($this->activity_type_id);
        return $store->name;
    }
    
}