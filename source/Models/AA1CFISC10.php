<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer10;

class AA1CFISC10 extends DataLayer10
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1CFISC",[],"", false);        
    }

    public  function getDueDate()
    {
        $aa3lvenc = (new AA3LVENC10())->find("FIV_SERIE = '$this->FIS_SERIE' and FIV_LOJ_ORG = :flo and FIV_NRO_NOTA = :fnn and
         FIV_OPER = :fo","flo=$this->FIS_LOJ_ORG&fnn=$this->FIS_NRO_NOTA&fo=$this->FIS_OPER")->fetch(true);
         return $aa3lvenc;
    }

    public  function getDueDateAmount()
    {
        $aa3lvenc = (new AA3LVENC10())->find("FIV_SERIE = '$this->FIS_SERIE' and FIV_LOJ_ORG = :flo and FIV_NRO_NOTA = :fnn and
         FIV_OPER = :fo","flo=$this->FIS_LOJ_ORG&fnn=$this->FIS_NRO_NOTA&fo=$this->FIS_OPER")->fetch(true);
         $i = 0;
         foreach($aa3lvenc as $a) {
            $i++;
         }
         return $i;
    }

    public function getProvider(){
        $aa2ctipo = (new AA2CTIPO10())->find("TIP_CODIGO = :tc","tc=$this->FIS_LOJ_ORG")->fetch();
        return $aa2ctipo;
    }

    public function getAccessKey() {
        $nfeControle = (new NFE_CONTROLE10())->find("substr(CHAVE_ACESSO_NFE,26,9) = :CHAVE_ACESSO_NFE and dateto_rms7(DATA_PROCESSAMENTO) = :DATA_PROCESSAMENTO",
        "DATA_PROCESSAMENTO=$this->FIS_DTA_AGENDA&CHAVE_ACESSO_NFE=".str_pad($this->FIS_NRO_NOTA,9,'0',STR_PAD_LEFT))->fetch();
        return $nfeControle;
    }

    public function getNfReceipt() {
        $nfReceipt = (new NfReceipt())->find("nf_number = :nn and store_id = :sid","nn=$this->FIS_NRO_NOTA&sid=$this->FIS_LOJ_DST")->fetch(true);
        foreach ($nfReceipt as $nR) {
            if (strlen($nR->access_key) == 44 ) {
                if (substr($nR->access_key,6,14) == $this->getProvider()->TIP_CGC_CPF) {
                    $nfReceiptReturn = $nR;
                }
            } else {
                if ('1'.substr($nR->updated_at,2,2).substr($nR->updated_at,5,2).substr($nR->updated_at,8,2) == $this->FIS_DTA_AGENDA){
                    $nfReceiptReturn = $nR;
                }
                
            }
        }
        return $nfReceiptReturn;
    }

    public function ag1iensa() {
        $aa1tcon = (new AA1CTCON10())->getEntradas4;
        $ag1iensa = (new AG1IENSA10())->find("ESCHC_AGENDA3 in ($aa1tcon) AND ESCHC_AGENDA3 != 499 AND ESCHC_AGENDA3 != $this->FIS_OPER AND ESCHC_AGENDA3 in (1,11,9) AND ESCHC_NRO_NOTA3 = $this->FIS_NRO_NOTA AND ESCHLJC_CODIGO3 = $this->FIS_LOJ_ORG AND ESCHC_SER_NOTA3 = $this->FIS_SERIE AND ESCLC_CODIGO = $this->FIS_LOJ_DST")->fetch(true);
        $i = 0;
        foreach($ag1iensa as $iensa) {
            $i++;
        }
        return $i;
    }

    public function ag1iensaCredito() {
        $aa1tcon = (new AA1CTCON10())->getEntradas4;
        $credito = false;
        $ag1iensa = (new AG1IENSA10())->find("ESCHC_AGENDA3 in ($aa1tcon) AND ESCHC_AGENDA3 != 499 AND ESCHC_AGENDA3 != $this->FIS_OPER AND ESCHC_NRO_NOTA3 = $this->FIS_NRO_NOTA AND ESCHLJC_CODIGO3 = $this->FIS_LOJ_ORG AND ESCHC_SER_NOTA3 = $this->FIS_SERIE AND ESCLC_CODIGO = $this->FIS_LOJ_DST")->fetch(true);
        foreach($ag1iensa as $iensa) {
            if ($iensa->ENTSAIC_PERC_ICMS < 12) {
                $credito = true;
            }
        }
        return $credito;
    }

    public function dataAgenda(){
        return substr($this->FIS_DTA_AGENDA,5,2)."/".substr($this->FIS_DTA_AGENDA,3,2)."/".substr($this->FIS_DTA_AGENDA,1,2);
    }
    
}