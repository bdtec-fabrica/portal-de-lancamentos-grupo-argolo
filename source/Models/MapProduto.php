<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;

class MapProduto extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("consinco.map_produto",[]);        
    }

    public function mapFamilia() {
        $mapFamilia = (new MapFamilia())->find("seqfamilia  = $this->SEQFAMILIA")->fetch();
        return $mapFamilia;
    }

    public function mapFamDivisao() {
        $mapFamDivisao = (new MapFamdivisao())->find("seqfamilia = $this->SEQFAMILIA and nrodivisao = 1")->fetch();
        return $mapFamDivisao;
    }

    public function mapProdCodigo() {
        $mapProdCodigo = (new MapProdCodigo())->find("seqproduto = $this->SEQPRODUTO")->fetch();
        return $mapProdCodigo;
    }

    public function madProdLogPreco($nroempresa) {
        $madProdLogPreco = (new MadProdLogPreco())->find("seqproduto = $this->SEQPRODUTO  and nroempresa = $nroempresa ")->fetch(true);
        return $madProdLogPreco;
    }

    public function mrlLanctoestoque() {
        $mrlLanctoestoque = (new MrlLanctoestoque())->find("seqproduto = $this->SEQPRODUTO")->fetch();
        return $mrlLanctoestoque;
    }

}
