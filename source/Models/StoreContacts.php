<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class StoreContacts extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("store_contacts",[],"id", true);        
    }

    public function getStore() {
        return (new Store())->findById($this->store_id);
        
    }
}