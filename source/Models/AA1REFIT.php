<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;


class AA1REFIT extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1REFIT",[],"", false);        
    }

    public function aa3citem (){
        $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM = $this->REF_COD_ITEM")->fetch();
        return $aa3citem;
    }

    public function aa3ccean(){
        $aa3ccean = (new AA3CCEAN())->find("EAN_COD_PRO_ALT = ".$this->aa3citem()->GIT_COD_ITEM.$this->aa3citem()->GIT_DIGITO)->fetch(true);
        return $aa3ccean;
    }

    public function aa2ctipo() {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $this->REF_COD_FOR")->fetch();
        return $aa2ctipo;
    }

    public function references() {
        $reference[$this->REF_REFERENCIA] = $this->REF_REFERENCIA;
        $reference[$this->aa3citem()->GIT_REFERENCIA] = $this->aa3citem()->GIT_REFERENCIA;
        return $reference;
    }

    public function eans(){
        $aa3ccean = (new AA3CCEAN())->find("EAN_COD_PRO_ALT = ".$this->aa3citem()->GIT_COD_ITEM.$this->aa3citem()->GIT_DIGITO)->fetch(true);
        foreach ($aa3ccean as $ean) {
            $eans[$ean->EAN_COD_EAN] = $ean->EAN_COD_EAN;
        }
        return $eans;
    }


}