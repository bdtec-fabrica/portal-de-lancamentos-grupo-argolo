<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;
use \DateInterval;

class NfReceipt extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_receipt",["store_id","group_id","nf_number","status_id","client_id","operator_id","type_id","operation_id","inclusion_type"],"id", true);        
    }

    public function getInteractions() {
        $interactions = (new Interaction())->find("id_nf_receipt = $this->id")->fetch(true);
        return $interactions;
    }

    public function save2() {
        $date = date("Y-m-d");
        $nfReceipt = (new NfReceipt())->find("nf_number = :nn and access_key = :ak and substr(created_at,1,10) = :date1 and store_id = :si",
        "nn=$this->nf_number&ak=$this->access_key&date1=$date&si=$this->store_id")->count();
        if ($nfReceipt > 0) {
            return false;
        } else {
            return $this->save();
        }
    }

    public function getStore() {
        $store = (new Store());
        $store = $store->find("id = :store_id and group_id = :group_id","store_id={$this->store_id}&group_id={$this->group_id}")->fetch();
        return $store->company_name;
    }

    public function store() {
        $store = (new Store())->find("code = :code","code=$this->store_id")->fetch();
        return $store;
    }

    public function getDueDate() {
        $dueDate = new NfDueDate();
        $dueDate = $dueDate->find("nf_id = :nf_id","nf_id=$this->id")->order("concat('1',substr(`due_date`,3,2),substr(`due_date`,6,2),substr(`due_date`,9,2)) asc","","nf_due_date.*,DATE_FORMAT(due_date,'%Y-%b-%d') date1")->fetch(true);
        return $dueDate;

    }

    public function getNfeDueDate() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $dup = $xml->NFe->infNFe->cobr->dup;
        return $dup;
        // foreach($xml->NFe->infNFe->cobr->dup as $dup) {
        //     echo "<pre>";
        //     var_dump($dup);
        //     // ->dup->dVenc
        //     echo "</pre>";
        // }
        
    }

    public function getItems() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $det = $xml->NFe->infNFe->det;
        return $det;
    }

    public function getAmount() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $det = $xml->NFe->infNFe->det;
        if (strlen($this->access_key) == 44) {
            if ($xml) {
                foreach ($det as $d) {
                    $i += 1;
                }
            } else {
                $i = 'Sem XML';
            }
        } else {
            $i = $this->items_quantity;
        }
        
        
        return $i;
    }


    public function getItemsIds($data){
        
        $requests = (new NfReceipt())->find("id in ($data)")->fetch(true);
        foreach($requests as $request){
            $count += $request->getAmountItems;
        }
        return $count;
    }

    public function getAmountItems() {
        if(strlen($this->access_key)==44){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $dets = $xml->NFe->infNFe->det;
            $count = 0;
            foreach ($dets as $det) {
                $count++;
            } return $count;
        } else {
            return $this->items_quantity;
        }
        
    }

    public function getFornecedor() {
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');

            if ($xml->NFe->infNFe->emit->CRT == 1) {
                $xNome = $xml->NFe->infNFe->emit->xNome." (SIMPLES NACIONAL)";
            } else {
                $xNome = $xml->NFe->infNFe->emit->xNome.'';
            }
            return $xNome;
        } else {
            return '';
        }
        
    }

    public function codigoInternoCD() {

        if($this->type_id == 4) {
            foreach($this->getItems() as $det){
                if (strlen($det->prod->cEAN) < 8 || $det->prod->cEAN == 'SEM GTIN') {
                    return "Codigo Interno";
                }
            }
            return "";
        }
    }

    public function getSimplesFabricante() {
        $ok = false;
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $xNome = $xml->NFe->infNFe->emit->xNome." (Simples Nacional)";
            if ($xml->NFe->infNFe->emit->CRT == 1) {
                
                foreach($xml->NFe->infNFe->det as $det) {
                    if($det->prod->CFOP == 5101) {
                        $ok = true;
                    }
                }
            }
            
        }
        return $ok;
    }

    public function creditoICMS() {
        //oioioi
    }

    public function getGroup() {
        $group = (new Group());
        $group = $store->find("id = :group_id","group_id={$this->group_id}")->fetch();
        return $group->name;
    }

    public function getStatus() {
        $status = (new NfStatus());
        $status = $status->find("id = :status_id","status_id={$this->status_id}")->fetch();
        return $status->name;
    }

    public function getClient() {
        $client = (new User());
        $client = $client->find("id = :client_id","client_id={$this->client_id}")->fetch();
        return $client->name;
    }

    public function getClientClass() {
        $client = (new User());
        $client = $client->find("id = :client_id","client_id={$this->client_id}")->fetch();
        return $client;
    }

    public function getOperator() {
        $operator = (new User());
        $operator = $operator->find("id = :operator_id","operator_id={$this->operator_id}")->fetch();
        return $operator->name;
    }

    public function getOperatorClass() {
        $operator = (new User());
        $operator = $operator->find("id = :operator_id","operator_id={$this->operator_id}")->fetch();
        return $operator;
    }

    public function getType() {
        $type = (new NfType());
        $type = $type->find("id = :type_id","type_id={$this->type_id}")->fetch();
        return $type->name;
    }

    public function getOperation() {
        $operation = (new NfOperation());
        $operation = $operation->find("id = :operation_id","operation_id={$this->operation_id}")->fetch();
        return $operation->name;
    }

    public function getOperationColor() {
        $operation = (new NfOperation());
        $operation = $operation->find("id = :operation_id","operation_id={$this->operation_id}")->fetch();
        return $operation->color;
    }

    public function getInclusion() {
        if ($this->inclusion_type == 0) {
            return "Manual";
        } else {
            return "Painel";
        }
    }

    public function clientGroup() {
        $group = (new User())->findById($this->client_id)->store_group_id;
        if ($group) {
            $group = (new StoreGroup())->findById($group);
            return $group->name;
        } else {
            return 0;
        }
        
        
    }

    public function getCreatedAt() {
        return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)."<br>".substr($this->created_at,11);
    }

    public function getReport() {
        $nfReport = (new NfReport())->find("id_request = :id_request","id_request=$this->id")->count();
        return $nfReport;
    }

    public function getReportData() {
        $nfReport = (new NfReport())->find("id_request = :id_request","id_request=$this->id")->fetch();
        return $nfReport;
    }

    public function getUpdatedAt()
    {
        return substr($this->updated_at,8,2)."/".substr($this->updated_at,5,2)."/".substr($this->updated_at,0,4)."<br>".substr($this->updated_at,11);
    }

    public function getCreatedAtInt()
    {
        return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)." ".substr($this->created_at,11);
    }

    public function getAttachment()
    {
        $attachment = (new NfAttachment());
        $attachment = $attachment->find("nf_id = :id","id={$this->id}")->fetch(true);
        return $attachment;
    }

    public function nfAdd($storeId, $nfNumber, $statusId, $clientId, $operatorId, $itemsQuantity, $typeId, $operationId, $inclusioType, $accessKey = null, $note = null) {
        $this->store_id = $storeId;
        $this->nf_number = $nfNumber;
        $this->status_id = 1;
        $this->client_id = $clientId;
        $this->operator_id = $operatorId;
        $this->type_id = $typeId;
        $this->operation_id = $operationId;
        $this->inclusion_type = $inclusioType;
        if ($accessKey) {
            $this->access_key = $accessKey;
        }
        if ($note) {
            $this->note = $note;
        }
        $save = $this->save();
        return $save;
    }

    public function userAdministrator() {
        $user = (new User())->findById($this->client_id);
        return $user;
    }

    public function userOperator() {
        $user = (new User())->findById($this->operator_id);
        return $user->name;
    }
    
    public function getCnpjFornecedor() {
        if ($this->access_key != ''){
            $CNPJ = substr($this->access_key,6,14);
            return $CNPJ;
        } else {
            return '';
        }
        
    }

    public function getNFeSerie() {
        if ($this->access_key != ''){
            $serie = substr($this->access_key,22,3);
            return $serie;
        } else {
            return '';
        }
        
    }

    public function saveLog($user,$operatorId, $nfNumber, $accessKey, $status, $itemsQuantity, $type, $operationId,$note, $id)
    {   

        $request = (new NfReceipt())->findByID($id);
        
        if ($operatorId != $request->operator_id) {
            $descriptionLog[0] = "Operador alterado de  '$request->operator_id' para '$operatorId' \n";
        }
        if ($nfNumber != $request->nf_number) {
            $descriptionLog[1] = "Número do documento alterado de $request->nf_number para $nfNumber \n";
        }
        if ($accessKey != $request->access_key) {
            $descriptionLog[2] = "Chave de acesso alterada de '$this->access_key' para '$accessKey' \n";
        }
        if ($status != $request->status_id) {
            $descriptionLog[3] = "Status alterado de '$request->status_id' para '$status' \n";
            $nfEventCount = (new NfEvent())->find("status_id = $request->status_id and situation = :situation and id_nf_receipt = :id_nf_receipt","situation=A&id_nf_receipt=$id")->count();
            if ($nfEventCount == 0) {
                $nfEvent = new NfEvent();
                $nfEvent->id_nf_receipt = $id;
                $nfEvent->status_id = $status;
                $nfEvent->user_id = $user;
                $nfEvent->situation = 'A';
                $nfEventId = $nfEvent->save();

            } else {
                $nfEventRegister = (new NfEvent())->find("status_id = $request->status_id and situation = :situation and id_nf_receipt = :id_nf_receipt","situation=A&id_nf_receipt=$id")->fetch();
                if ($nfEventRegister->status_id != $status) {
                    $nfEventUpdate = (new NfEvent())->findById($nfEventRegister->id);
                    $nfEventUpdate->situation = 'F';
                    $nfEventUpdate->final_user_id = $user;
                    $nfEventUpdateId = $nfEventUpdate->save();

                    $nfEvent = new NfEvent();
                    $nfEvent->id_nf_receipt = $id;
                    $nfEvent->status_id = $status;
                    $nfEvent->user_id = $user;
                    $nfEvent->situation = 'A';
                    $nfEventId = $nfEvent->save();
                }
            }
        }	

        if ($itemsQuantity != $request->items_quantity) {
            $descriptionLog[4] = "Quantidade de itens alterada de '$request->items_quantity' para '$itemsQuantity' \n";
        }

        if ($type != $request->type_id) {
            $descriptionLog[5] = "Tipo de nota alterado de '$request->type_id' para '$type' \n";
        }

        if ($operationId != $request->operation_id) {
            $descriptionLog[6] = "Operação alterada de '$request->operation_id' para '$operationId' \n";
        }

        if ($note != $request->note) {
            $descriptionLog[7] = "Observação alterada de '$request->note' para '$note' \n";
        }
        
        if ($descriptionLog) {
            $descriptionLog = implode(",", $descriptionLog);
            $requestLog = new NfReceiptLog();
            
            $requestLog->description = $descriptionLog;
            $requestLog->user_id = $user;
            $requestLog->nf_receipt_id = $id;
            $requestLogId = $requestLog->save();


        } else {
            $requestLogId = true;
        }      
        
        if ($requestLogId) {
            return $this->save();
        }else{
            return false;
        }
        
    }

    public function getLogs()
    {
        return (new NfReceiptLog())->find("nf_receipt_id = :rid","rid={$this->id}")->order("id ASC")->fetch(true);
    }

    public function getAmount1(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6)")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1 or $diff->format('%d') > 0) {
                if ($request->operation_id == 1 ){
                    $count++; 
                } else if ($request->operation_id == 4){
                    $count++; 
                } else if ($request->operation_id == 11){
                    $count++; 
                } else if ($request->operation_id == 12){
                    $count++; 
                }
            }
        }
        return $count;
    }

    public function getAmount2(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount4(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount6(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount8(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount24(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getNf1(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6)")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1 ) {
                if ($request->operation_id == 1 ){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 4){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 11){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 12){
                    $count++;
                    $nf[$count] = $request->id;
                }
                
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
        $count = implode(",",$nf);
            } else {
        $count = 0;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf2(){   
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
        $count = implode(",",$nf);
                } else {
        $count = 0;
        }
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        
        return $count;
    }

    public function getNf4(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
        $count = implode(",",$nf);
                    } else {
            $count = 0;
            }
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf6(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf8(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
                if (is_array($nf)) {
                    $count = implode(",",$nf);
                } else {
                    $count = 0;
                }
            } else {
                $count = $nf;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf24(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getAmountPC0(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') < 1 ) {
                $count++;                    
            }
        }
        return $count;
    }

    public function getAmountPC1(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1 && $diff->format('%d') < 2) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC2(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 2 && $diff->format('%d') < 4) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC4(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 4 && $diff->format('%d') < 7) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC7(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 7 && $diff->format('%d') < 10) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC10(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 10) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC0Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') < 1 ) {
                $count++;                    
            }
        }
        return $count;
    }

    public function getAmountPC1Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1 && $diff->format('%d') < 2) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC2Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 2 && $diff->format('%d') < 4) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC4Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 4 && $diff->format('%d') < 7) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC7Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 7 && $diff->format('%d') < 10) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC10Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 10) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getNfPC0(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC1(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1 && $diff->format('%d') < 2) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC2(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 2 && $diff->format('%d') < 4) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC4(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 4 && $diff->format('%d') < 7) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC7(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 7 && $diff->format('%d') < 10) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC10(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 10) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function inRMS(){
        if (strlen($this->access_key) == 44){
            $nfRMS = (new NfRms())->find("access_key = :access_key and store_id = :sid","access_key=$this->access_key&sid=$this->store_id")->count();
        } else {
            $nfRMS = (new NfRms())->find("nf_number = :nf_number and store_id = :store_id","store_id=$this->store_id&nf_number=$this->nf_number")->count();
        }
        return $nfRMS;
    }

    public function getTimeDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }
        $dia = $diff->format("%d");
        $dia = $dia." dia(s), ";
        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%Y-%m-%d %H:%i"));
        $date->sub(new DateInterval($event->getAverageStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $dia.$date->format("H:i:s");
        } else {
            return $date->format('H:i:s');
        }
        
    }

    public function getDaysDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }

        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%d"));
        $date->sub(new DateInterval($event->getAverageDaysStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $date->format("d");
        } else {
            return $date->format('d');
        }
        
    }

    public function getTimesDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }
        $dia = $diff->format("%d");
        $dia = $dia." dia(s), ";
        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%Y-%m-%d %H:%i"));
        $date->sub(new DateInterval($event->getAverageStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $dia.$date->format("H:i:s");
        } else {
            return $date->format('H:i:s');
        }
        
    }

    public function electronicInvoiceRecorded(){
        // if (strlen($this->access_key) == 44){
        //     $nfRMS = (new NfRms())->find("access_key = :access_key and store_id = :sid","access_key=$this->access_key&sid=$this->store_id")->count();
        // } else {
        //     $nfRMS = (new NfRms())->find("nf_number = :nf_number and store_id = :store_id","store_id=$this->store_id&nf_number=$this->nf_number")->count();
        // }
        // return $nfRMS;
        // if ($count = 0){
        //     return false;
        // } else {
        //     return true;
        // }
    }

    public function getDuedateAmount() {
        $dueDate = new NfDueDate();
        $dueDate = $dueDate->find("nf_id = :nf_id","nf_id=$this->id")->count();
        return $dueDate;
    }

    public function dueDate() {
        $dueDate = new NfDueDate();
        $dueDate = $dueDate->find("nf_id = :nf_id","nf_id=$this->id","nf_due_date.*, DATE_FORMAT(due_date,'%d-%b-%y') date1")->order("concat('1',substr(`due_date`,3,2),substr(`due_date`,6,2),substr(`due_date`,9,2))")->fetch(true);
        return $dueDate;
    }

    public function getAA1CFISC() {
        
        $aa1cfisc = (new AA1CFISC())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag145 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34&ag145=145")->fetch(true);

        foreach ($aa1cfisc as $nR) {
            if (strlen($this->access_key) == 44 ) {
                if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                    $nfReceiptReturn = $nR;
                }
            } else {
                if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                    $nfReceiptReturn = $nR;
                }
                
            }
            
        }
        if (!$nfReceiptReturn) {
            $aa1cfisc2 = (new AA1CFISC2())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34")->fetch(true);
            foreach ($aa1cfisc2 as $nR) {
                if (strlen($this->access_key) == 44 ) {
                    if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                        $nfReceiptReturn = $nR;
                    }
                } else {
                    if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                        $nfReceiptReturn = $nR;
                    }
                }  
            }
        }
        
        if (!$nfReceiptReturn) {
            return "Não encontrada.";
        } else {
            return "Encontrada.";
        }
        
    }

    public function AA1CFISC() {
        
        $aa1cfisc = (new AA1CFISC())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag145 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34&ag145=145")->fetch(true);
        foreach ($aa1cfisc as $nR) {
            if (strlen($this->access_key) == 44 ) {
                if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                    $nfReceiptReturn = $nR;
                }
            } else {
                if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                    $nfReceiptReturn = $nR;
                }
                
            }
            
        }
        if (!$nfReceiptReturn) {
            $aa1cfisc2 = (new AA1CFISC2())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34")->fetch(true);
            foreach ($aa1cfisc2 as $nR) {
                if (strlen($this->access_key) == 44 ) {
                    if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                        $nfReceiptReturn = $nR;
                    }
                } else {
                    if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                        $nfReceiptReturn = $nR;
                    }
                }  
            }
        }
        
        return $nfReceiptReturn;
        
    }

    public function getUpdatedAtRms() {
        return '1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2);
    }
    
    public function getStoreNoDigit() {
        return substr($this->store_id,0,strlen($this->store_id)-1);
    }


    public function updated() {
        // return true;
        if (strlen($this->nf_number)>7) {
            $numero = substr($this->nf_number,strlen($this->nf_number)-7);
        } else {
            $numero = $this->nf_number;
        }
        if ($this->group_id == 1 ) {
            $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
            $aa1cfisc = (new AA1CFISC())->find("FIS_SITUACAO != '9' and FIS_SERIE = $this->getNFeSerie and FIS_LOJ_ORG = $aa2ctipo->TIP_CODIGO and FIS_LOJ_DST = $this->store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON())->getEntradas3).")")->fetch();
        } else if ($this->group_id == 2) {
            if ($this->store_id == 850) {
                $store_id = 85;
            } else {
                $store_id = $this->store_id;
            }
            $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
            if (strlen($this->access_key) != 44) {
                $aa1cfisc = (new AA1CFISC2())->find("FIS_SITUACAO != '9' and FIS_LOJ_DST = $store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON2())->getEntradas3).")")->order("FIS_DTA_AGENDA ASC");
                
            } else {
                $aa1cfisc = (new AA1CFISC2())->find("FIS_SITUACAO != '9' and FIS_SERIE = $this->getNFeSerie and FIS_LOJ_ORG = $aa2ctipo->TIP_CODIGO and FIS_LOJ_DST = $store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON2())->getEntradas3).")")->fetch();
            }
            
        } else if ($this->group_id == 4 ||$this->group_id == 8) {
            $aa2ctipo = (new AA2CTIPO3())->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
            $store_id = substr($this->store_id,0,strlen($this->store_id)-1);
            $aa1cfisc = (new AA1CFISC3())->find("FIS_SITUACAO != '9' and FIS_SERIE = $this->getNFeSerie and FIS_LOJ_ORG = $aa2ctipo->TIP_CODIGO and FIS_LOJ_DST = $store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON3())->getEntradas3).")")->fetch();
        } else if ($this->group_id == 3) {
            if ($this->store_id == 33) {
                $mlfNotaFiscal = (new MlfNotaFiscal())->find("SEQPESSOA = ".$this->getPessoa()->SEQPESSOA." and lpad(trim(SERIENF),3,'0') = $this->getNFeSerie and NUMERONF = $this->nf_number and NROEMPRESA = $this->store_id")->fetch();
            } else {
                $mlfNotaFiscal = (new MlfNotaFiscal())->find("SEQPESSOA = ".$this->getPessoa()->SEQPESSOA." and lpad(trim(SERIENF),3,'0') = '$this->getNFeSerie' and NUMERONF = $this->nf_number and NROEMPRESA = substr($this->store_id,0,length($this->store_id)-1)")->fetch();           
            }
            
            return $mlfNotaFiscal;
        } else if ($this->group_id == 10) {
            if ($this->store_id == 2003) {
                $storeCode = 200;
                $aa2ctipo = (new AA2CTIPO10())->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
                $aa1cfisc = (new AA1CFISC10())->find("FIS_SITUACAO != '9' and FIS_SERIE = $this->getNFeSerie and FIS_LOJ_ORG = $aa2ctipo->TIP_CODIGO and FIS_LOJ_DST = $storeCode and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON10())->getEntradas3).")")->fetch();
            } else {
                $aa2ctipo = (new AA2CTIPO10())->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
                $aa1cfisc = (new AA1CFISC10())->find("FIS_SITUACAO != '9' and FIS_SERIE = $this->getNFeSerie and FIS_LOJ_ORG = $aa2ctipo->TIP_CODIGO and FIS_LOJ_DST = $this->store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON10())->getEntradas3).")")->fetch();
            }

        } else {
            $aa1cfisc = true;
        }

        if ($aa1cfisc) {
            return $aa1cfisc;
        } else {
            return false;
        }
    }

    public function AG2DETNT() {
        $ag2detnt = (new AG2DETNT())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
        "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        
        $error = false;
        foreach ($ag2detnt as $items) {
            if ($items->getMargin > 50 || $items->getMargin < 0){
                $error = true;
            }
        }
        return $error;
    }

    public function retArrayCodes() {
        switch ($this->group_id) {
            case '1':
                $margemRecebimento = (new MargemRecebimento());
                break;       
            case '10':
                $margemRecebimento = (new MargemRecebimento10());
                break;
            case '2':
                $margemRecebimento = (new MargemRecebimento2());
                break;
            case '9':
                $margemRecebimento = (new MargemRecebimento2());
                break;
            case '3':
                $margemRecebimento = (new MlfAuxnotafiscal());
                break;
            
        }
        if ($this->group_id == 3) {
            if($this->store_id == 33) {
                $margemRecebimento = $margemRecebimento->find("NUMERONF = $this->nf_number AND SERIENF = $this->getNFeSerie AND NROEMPRESA = $this->store_id")->fetch();
            } else {
                $margemRecebimento = $margemRecebimento->find("NUMERONF = $this->nf_number AND SERIENF = $this->getNFeSerie AND NROEMPRESA = SUBSTR($this->store_id,0,length($this->store_id)-1)")->fetch();
            }
            
            if ($margemRecebimento) {
                foreach($margemRecebimento->mlfNfitem() as $margem) {
                    $data[$margem->SEQPRODUTO] = $margem->SEQPRODUTO;
    
                }
            }
            
            
        } else {
            $margemRecebimento = $margemRecebimento->find("REN_NOTA = $this->nf_number AND REN_SERIE = $this->getNFeSerie AND REN_DESTINO = $this->store_id||dac($this->store_id) AND TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch(true);

            foreach($margemRecebimento as $margem) {
                $data[$margem->GIT_COD_ITEM] = $margem->GIT_COD_ITEM;
            }
        }
        
        return $data;
    }

    public function getCodes()
    {
        $codes = (new NfReceiptCodes())->find("nf_receipt_id = $this->id")->fetch(true);
        return $codes;
    }

    public function AG2DETNT2() {
        if ($this->group_id == 1){
            $ag2detnt = (new AG2DETNT())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
            "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        } else if ($this->group_id == 4 ||$this->group_id == 8){
            $ag2detnt = (new AG2DETNT3())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
            "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        } else if ($this->group_id == 10){
            $ag2detnt = (new AG2DETNT10())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
            "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        } else if ($this->group_id == 2){
            $ag2detnt = (new AG2DETNT2())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
            "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        }
        
        
        $error = false;
        foreach ($ag2detnt as $items) {
            if ($items->getMargin > 50 || $items->getMargin < 0){
                $error .= "Codigo: ".$items->getItemRegister()->GIT_COD_ITEM." Descrição: ".$items->getItemRegister()->GIT_DESCRICAO." Margem: ". $items->getMargin.".";
            }
        }
        return $error;
    }

    public function getAmountCC1(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1  && $diff->format('%H') < 2  && $diff->format('%d') < 1){
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC2(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC4(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC6(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC8(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC24(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getRequestCC1(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1  && $diff->format('%H') < 2  && $diff->format('%d') < 1){
                $count++;
                $nf[$count] = $request->id;
                
            }
        }
        if (is_array($nf)) {
            $count = implode(",",$nf);
        } else {
            $count = 0;
        }
        return $count;
    }

    public function getRequestCC2(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
            $count = implode(",",$nf);
        } else {
            $count = 0;
        }
        return $count;
    }

    public function getRequestCC4(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
        $count = implode(",",$nf);
            }    else {
        $count = 0;
            }   
        return $count;
    }

    public function getRequestCC6(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
            $count = implode(",",$nf);
        }   else {
        $count = 0;
        }          
        return $count;
    }

    public function getRequestCC8(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
        $count = implode(",",$nf);
        } else {
        $count = 0;
            }
        return $count;
    }

    public function getRequestCC24(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
            $count = implode(",",$nf);
        } else {
            $count = 0;
        }
        return $count;
    }

    public function nfSefaz() {
        return (new NfSefaz())->find("access_key = :access_key","access_key=$this->access_key")->fetch();
    }

    public function aa2detnt() {
        
        if ($this->store()->group_id == 1) {
            $aa1ctcon = new AA1CTCON();
            $agendas = $aa1ctcon->entradasAtualizaCusto();
            $aa2detnt = (new AG2DETNT())->find("REN_TPO_AGENDA in ($agendas) and REN_NOTA = $this->nf_number and substr(REN_DESTINO,0,length(REN_DESTINO)-1) = $this->store_id")->fetch(true);

            return $aa2detnt;
            
        } else if ($this->store()->group_id == 10) {
            $aa1ctcon = new AA1CTCON10();
            $agendas = $aa1ctcon->entradasAtualizaCusto();
            $aa2detnt = (new AG2DETNT10())->find("REN_TPO_AGENDA in ($agendas) and REN_NOTA = $this->nf_number and substr(REN_DESTINO,0,length(REN_DESTINO)-1) = $this->store_id")->fetch(true);

            return $aa2detnt;
        }
    }

    public function diferencaCustoPortal($reference = null) {
        foreach ($this->getItems as $det1) {
            
            if ("'".$det1->prod->cProd."'" == "'$reference'") {
                   
                $custoAtual = $det1->prod->vUnCom;
                
                $nfSefaz = (new NfSefaz())->find("access_key != '$this->access_key'  and substr(access_key,7,14) = ".$this->getCnpjFornecedor()." and store_id = $this->store_id")->order("nfe_number ASC")->fetch(true);
                foreach ($nfSefaz as $nf) {
                    foreach ($nf->getItems as $det) {
                        if ("'".$det->prod->cProd."'" == "'".$det1->prod->cProd."'") {
                            $custoAnterior =  $det->prod->vUnCom;
                        }
                    }
                }
                
            }
           
        }
        
        if ($custoAnterior == '') {
            $valor = 100;
        } else {
            $valor = (($custoAnterior - $custoAtual)/$custoAnterior)*100;
        }
        return $valor;
        
    }

    public function custoAnterior($reference = null) {
        foreach ($this->getItems as $det1) {
            
            if ("'".$det1->prod->cProd."'" == "'$reference'") {
                   
                $custoAtual = $det1->prod->vUnCom;
                
                $nfSefaz = (new NfSefaz())->find("access_key != '$this->access_key'  and substr(access_key,7,14) = ".$this->getCnpjFornecedor()." and store_id = $this->store_id")->order("nfe_number ASC")->fetch(true);
                foreach ($nfSefaz as $nf) {
                    foreach ($nf->getItems as $det) {
                        if ("'".$det->prod->cProd."'" == "'".$det1->prod->cProd."'") {
                            $custoAnterior =  $det->prod->vUnCom;
                        }
                    }
                }
                
            }
           
        }
        
        if ($custoAnterior == '') {
            $custoAnterior = 0;
            $valor = 100;
        } else {
            $valor = (($custoAnterior - $custoAtual)/$custoAnterior)*100;
        }
        return $custoAnterior;
        
    }

    public function notaAnterior($reference = null) {
        foreach ($this->getItems as $det1) {
            
            if ("'".$det1->prod->cProd."'" == "'$reference'") {
                   
                $custoAtual = $det1->prod->vUnCom;
                
                $nfSefaz = (new NfSefaz())->find("access_key != '$this->access_key'  and substr(access_key,7,14) = ".$this->getCnpjFornecedor()." and store_id = $this->store_id")->order("nfe_number ASC")->fetch(true);
                foreach ($nfSefaz as $nf) {
                    foreach ($nf->getItems as $det) {
                        if ("'".$det->prod->cProd."'" == "'".$det1->prod->cProd."'") {
                            $custoAnterior =  $det->prod->vUnCom;
                            $nota = $nf->nfe_number;
                        }
                    }
                }
                
            }
           
        }
        
        if ($custoAnterior == '') {
            $custoAnterior = 0;
            $valor = 100;
        } else {
            $valor = (($custoAnterior - $custoAtual)/$custoAnterior)*100;
        }
        return $nota;
        
    }

    public function custoAtual($reference = null) {
        foreach ($this->getItems as $det1) {
            
            if ("'".$det1->prod->cProd."'" == "'$reference'") {
                   
                $custoAtual = $det1->prod->vUnCom;
                
                $nfSefaz = (new NfSefaz())->find("access_key != '$this->access_key'  and substr(access_key,7,14) = ".$this->getCnpjFornecedor()." and store_id = $this->store_id")->order("nfe_number ASC")->fetch(true);
                foreach ($nfSefaz as $nf) {
                    foreach ($nf->getItems as $det) {
                        if ("'".$det->prod->cProd."'" == "'".$det1->prod->cProd."'") {
                            $custoAnterior =  $det->prod->vUnCom;
                        }
                    }
                }
                
            }
           
        }
        
        if ($custoAnterior == '') {
            $valor = 100;
        } else {
            $valor = (($custoAnterior - $custoAtual)/$custoAnterior)*100;
        }
        return $custoAtual;
        
    }

    public function error() {
        
        foreach ($this->getItems as $det) {
            if ($this->aa2detnt()) {
                foreach ($this->aa2detnt as $aa2detnt) {
                    if (in_array($det->prod->cProd, $aa2detnt->references())) {
                        if (((round($aa2detnt->diferencaValor) - round($this->diferencaCustoPortal($det->prod->cProd))) > 0.10 || (round($aa2detnt->diferencaValor) - round($this->diferencaCustoPortal($det->prod->cProd))) < -0.10)
                        && $aa2detnt->REN_PRC_UN > 0 && $this->custoAnterior($det->prod->cProd) > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }else if (in_array($det->prod->cEAN, $aa2detnt->eans())) {
                        if (((round($aa2detnt->diferencaValor) - round($this->diferencaCustoPortal($det->prod->cProd))) > 0.10 || (round($aa2detnt->diferencaValor) - round($this->diferencaCustoPortal($det->prod->cProd))) < -0.10)
                        && $aa2detnt->REN_PRC_UN > 0 && $this->custoAnterior($det->prod->cProd) > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } 
                }
            } else {
                return false;
            }
            
        }
        return false;
    }

    public function diferencaVariacaoCusto() {
        
        if (!empty($this->getAG1IENSA())) {
            foreach ($this->getAG1IENSA() as $ag1iensa) {
                foreach ($this->getItems() as $det) {
                    if (in_array($det->prod->cProd,$ag1iensa->references()) or in_array($det->prod->cEAN,$ag1iensa->EANs())){
                        if (round($ag1iensa->perecentualVariacao()) <> round($this->diferencaCustoPortal($det->prod->cProd))) {
                            if ($this->custoAnterior($det->prod->cProd) > 0 or $ag1iensa->getMargin($this->store_id) > 30 or $ag1iensa->getMargin($this->store_id) < -5) {
                                if ($ag1iensa->getDatePrice2() >= 1210101) {
                                    $data[$ag1iensa->ESITC_CODIGO] = "Código produto $ag1iensa->ESITC_CODIGO - $ag1iensa->ESITC_DIGITO, Custo anterior ".$this->custoAnterior($det->prod->cProd)." Nota Anterior ".$this->notaAnterior($det->prod->cProd).", Margem ".$ag1iensa->getMargin($this->store_id)." Variação divergente!";
                                }
                                
                            }
                        }
                    }
                }
            }
            if (!empty($data)) {
                $data['msg'] = implode('\n',$data);
                $data['value'] = true;
            } else {
                $data['msg'] = "Sem divergencias na variação.";
                $data['value'] = false;
            }
        } else {
            $data['msg'] = "Nota Não Atualizada";
            $data['value'] = false;
        }
        return $data;
    }

    public function checkVencimento() {
        if ($this->group_id == 1 or $this->group_id == 10 or $this->group_id == 2) {
            if (strlen($this->access_key) == 44) {
                
                if ($this->updated() && $this->updated() !== true) {
                    if (($this->getDueDateAmount() == $this->updated()->getDueDateAmount())){
                        $i = 1;
                        foreach($this->getDueDate() as $venc) {
                            $portal[$i] = $venc->due_date;
                            $i++;
                        }
                        $ok = true;
        
                        foreach($this->updated()->getDuedate() as $venc) {
                            $rms[$i] = dateRMSToDateI($venc->FIV_DTA_VENCTO);
                            $i++;
                            if (in_array(dateRMSToDateI($venc->FIV_DTA_VENCTO), $portal)) {
                                //nothing to do
                            } else {
                                $msg .= "Não encontrado Portal ".dateRMSToDateI($venc->FIV_DTA_VENCTO)." , ";
                                $ok = false;
                            }
        
                        }
        
                        foreach($this->getDueDate() as $venc) {
                            if (in_array($venc->due_date, $rms)) {
                                //nothing to do
                            } else {
                                $msg .= "Não encontrado RMS ".$venc->due_date." , ";
                                $ok = false;
                            }
                        }
        
                        if ($ok) {
                            $retorno['ok'] = true;
                            if (is_array($rms)) {
                                $rms = implode(' ,',$rms);
                            }
                            if (is_array($portal)) {
                                $portal = implode(' ,',$portal);
                            }
                            $retorno['msg'] = "Vencimentos sem divergencias. RMS: ".$rms.". Portal: ".$portal.".";
                            return $retorno;
                        } else {
                            $retorno['ok'] = false;
                            $retorno['msg'] = "Divergencias Vencimentos: $msg .";
                            return $retorno;
                        }
                        
                    } else {
                        
                        $retorno['ok'] = false;
                        $retorno['msg'] = "Quantidade de vencimentos divergentes: RMS 0".$this->updated()->getDueDateAmount()." Portal 0" .$this->getDueDateAmount().".";
                        return $retorno;
                    }        
                } else {
                    $retorno['ok'] = false;
                    $retorno['msg'] = "Nota não Atualizada!";
                    return $retorno;
                }
            } else {
                $retorno['ok'] = true;
                $retorno['msg'] = "Impossivel veificar sem chave de acesso.";
                return $retorno;
            } 
        } else {
            $retorno['ok'] = true;
            $retorno['msg'] = "Integração ainda não preparada.";
            return $retorno;
        }
       
        
    }

    public function getItemCadastro($reference) {
        $r = (new RegistroCadastro)->find("nf_receipt_id = $this->id and referencia = $reference")->fetch();
        if ($r) {
            return true;
        } else {
            return false;
        }
    }


    public function aa2ctipo() {
        /**
         * selecionar base de dados
         */
        switch ($this->group_id) {
            case 1:
                $aa2ctipo = new AA2CTIPO();
                break;
            case 10:
                $aa2ctipo = new AA2CTIPO10();
                break;
            case 2:
                $aa2ctipo = new AA2CTIPO2();
                break;
            case 3:
                $aa2ctipo = new AA2CTIPO3();
                break;
            default:
                return null;
                break;
        }
        $tipo = $aa2ctipo->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
        return $tipo->TIP_CODIGO;
    }

    public function getAG1IENSA()
    {
        /**
         * selecionar base de dados
         */
        switch ($this->group_id) {
            case 1:
                $ag1iensa = new AG1IENSA();
                break;
            case 10:
                $ag1iensa = new AG1IENSA10();
                break;
            case 2:
                $ag1iensa = new AG1IENSA2();
                break;
            case 3:
                $ag1iensa = new AG1IENSA3();
                break;
            default:
                return null;
                break;
        }
        $itens = $ag1iensa->find("ESCLC_CODIGO = $this->store_id and  ESCHLJC_CODIGO = $this->aa2ctipo and  ESCHC_NRO_NOTA = $this->nf_number and ESCHC_SER_NOTA = $this->getNFeSerie")->fetch(true);
        return $itens;
    }

    public function validacaoPreco() {
        switch ($this->group_id) {
            case '1':
                $aa5cbprc = new AA5CBPRC();
                break;
            
            case '10':
                $aa5cbprc = new AA5CBPRC();
                break;
            case '2':
                $aa5cbprc = new AA5CBPRC2();
                break;
            case '9':
                $aa5cbprc = new AA5CBPRC2();
                break;            
        }
        $v = $aa5cbprc->find("BPRC_NRO_NOTA = $this->nf_number AND BPRC_LOJA_FIS = $this->store_id AND BPRC_LOJ_ORG = $this->aa2ctipo")->fetch(true);
        return $v;

    }

    public function margemRecebimento() {
        switch ($this->group_id) {
            case '1':
                $margemRecebimento = (new MargemRecebimento());
                break;       
            case '10':
                $margemRecebimento = (new MargemRecebimento10());
                break;
            case '2':
                $margemRecebimento = (new MargemRecebimento2());
                break;
            case '9':
                $margemRecebimento = (new MargemRecebimento2());
                break;
            case '3':
                $margemRecebimento = (new MlfAuxnotafiscal());
                break;
            
        }
        if ($this->group_id == 3) {
            if($this->store_id == 33) {
                $margemRecebimento = $margemRecebimento->find("NUMERONF = $this->nf_number AND SERIENF = $this->getNFeSerie AND NROEMPRESA = $this->store_id")->fetch();
            } else {
                $margemRecebimento = $margemRecebimento->find("NUMERONF = $this->nf_number AND SERIENF = $this->getNFeSerie AND NROEMPRESA = SUBSTR($this->store_id,0,length($this->store_id)-1)")->fetch();
            }
            

            foreach($margemRecebimento->mlfNfitem() as $margem) {
                $preco = $margem->preco()->PRECO;

                if($preco == 0) {
                    $preco = $margem->custo();
                }
                $margem2 = round(100-((($margem->custo()/$preco) * 100)+(($margem->pisCofins-($margem->pisCofins * ($margem->ICMS/100)))+$margem->ICMS)),2);
                $data[$margem->SEQPRODUTO]['codigo'] = $margem->SEQPRODUTO;
                $data[$margem->SEQPRODUTO]['descricao'] = trim($margem->mapProduto()->DESCCOMPLETA);
                if ($margem->mapProduto()->mrlLanctoestoque()) {
                    $data[$margem->SEQPRODUTO]['GET_DT_ULT_FAT'] = 1;
                } else {
                    $data[$margem->SEQPRODUTO]['GET_DT_ULT_FAT'] = 0;
                }
                
                if ($margem->mapProduto()->mapProdCodigo()->tipcodigo == 'B') {
                    $data[$margem->SEQPRODUTO]['GIT_TIPO_ETQ'] = 4;
                } else {
                    $data[$margem->SEQPRODUTO]['GIT_TIPO_ETQ'] = 1;
                }
                
                $data[$margem->SEQPRODUTO]['custo'] = $margem->custo();
                $data[$margem->SEQPRODUTO]['preco'] = $margem->preco()->PRECO;
                $data[$margem->SEQPRODUTO]['embalagem'] = $margem->QTDEMBALAGEM;
                $data[$margem->SEQPRODUTO]['quantidade'] = $margem->QUANTIDADE;
                $updated_at = new DateTime($margem->preco()->UPDATED_AT);
                $updated_at =  $updated_at->format('d/m/Y');
                $data[$margem->SEQPRODUTO]['dataPreco'] = $updated_at;
                $data[$margem->SEQPRODUTO]['margem'] = $margem2;
            }
            
        } else {
            $margemRecebimento = $margemRecebimento->find("REN_NOTA = $this->nf_number AND REN_SERIE = $this->getNFeSerie AND REN_DESTINO = $this->store_id||dac($this->store_id) AND TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch(true);

            foreach($margemRecebimento as $margem) {
                $data[$margem->GIT_COD_ITEM]['codigo'] = $margem->GIT_COD_ITEM;
                $data[$margem->GIT_COD_ITEM]['descricao'] = trim($margem->GIT_DESCRICAO);
                $data[$margem->GIT_COD_ITEM]['custo'] = $margem->REN_CUS_UN;
                $data[$margem->GIT_COD_ITEM]['preco'] = $margem->PRE_PRECO;
                $data[$margem->GIT_COD_ITEM]['embalagem'] = $margem->REN_EMB;
                $data[$margem->GIT_COD_ITEM]['quantidade'] = $margem->REN_QTD_FAT;
                $data[$margem->GIT_COD_ITEM]['GET_DT_ULT_FAT'] = $margem->GET_DT_ULT_FAT;
                $data[$margem->GIT_COD_ITEM]['GIT_TIPO_ETQ'] = $margem->GIT_TIPO_ETQ;
                $data[$margem->GIT_COD_ITEM]['dataPreco'] = dateRMSToDate($margem->PRE_DAT_INICIO);
                $data[$margem->GIT_COD_ITEM]['margem'] = $margem->MARGEM;
            }
        }
        
        return $data;
    }

    public function getItemsRms($code) {
        if ($this->group_id == 1) {
            if ($code) {
                $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
                $det = $xml->NFe->infNFe->det;
                $emit = $xml->NFe->infNFe->emit;
    
                $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF = $emit->CNPJ")->fetch();
    
                
                $array = array();
                foreach($det as $item) {
     
                    (string)$ean = $item->prod->cEAN;
                    (string)$prod = $item->prod->cProd;
    
                    if ($ean == 'SEM GTIN') {
                        $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM = $code AND TRIM(GIT_REFERENCIA) = '$prod' and GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                        if ($aa3citem) {
                            $array['aa3citem'] = $aa3citem;
                            $array['nota'] = $item->prod;
                            return $array;
                        } else {
                            $aa1forit = (new AA1FORIT())->find("FORITE_COD_ITEM = $code AND TRIM(FORITE_REFERENCIA) = '$prod' and FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch();
                            if ($aa1forit) {
                            $array['aa1forit'] = $aa1forit;
                            $array['nota'] = $item->prod;
                            return $array;
                            } else {
                                $aa1refit = (new AA1REFIT())->find("REF_COD_ITEM = $code and TRIM(REF_REFERENCIA) = '$prod' and REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                                if ($aa1refit) {
                            $array['aa1refit'] = $aa1refit;
                            $array['nota'] = $item->prod;
                            return $array;
                                } else {
                                    $array['erro'] = "Não encontrado";
                                    $array['nota'] = $item;
                                    return $array;
                                }
                            }
                        }
                    } else {
                        $aa3ccean = (new AA3CCEAN())->find("EAN_COD_PRO_ALT = $code||dac($code) and EAN_COD_EAN = '$ean'")->fetch();
                        
                        $array['aa3ccean'] = $aa3ccean;
                        $array['nota'] = $item->prod;
                        
                    }
                    
                }
            } else {
                $array = array();
                foreach($det as $item) {
                    (string)$ean = $item->prod->cEAN;
                    (string)$prod = $item->prod->cProd;
                    if ($ean == 'SEM GTIN') {
                        $aa3citem = (new AA3CITEM())->find("TRIM(GIT_REFERENCIA) = '$prod' and GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                        if ($aa3citem) {
                            $array['aa3citem'] = $aa3citem;
                            $array['nota'] = $item->prod;
                        } else {
                            $aa1forit = (new AA1FORIT())->find("TRIM(FORITE_REFERENCIA) = '$prod' and FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch();
                            if ($aa1forit) {
                            $array['aa1forit'] = $aa1forit;
                            $array['nota'] = $item->prod;
                            } else {
                                $aa1refit = (new AA1REFIT())->find("TRIM(REF_REFERENCIA) = '$prod' and REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                                if ($aa1refit) {
                            $array['aa1refit'] = $aa1refit;
                            $array['nota'] = $item->prod;
                                } else {
                                    $array['erro'] = "Não encontrado";
                                    $array['nota'] = $item;
                                }
                            }
                        }
                    } else {
                        $aa3ccean = (new AA3CCEAN())->find("EAN_COD_EAN = '$ean'")->fetch();
                        $array['aa3ccean'] = $aa3ccean;
                        $array['nota'] = $item->prod;
                    }
                }
            }
        } else if ($this->group_id == 2) {
            if ($code) {
                $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
                $det = $xml->NFe->infNFe->det;
                $emit = $xml->NFe->infNFe->emit;
    
                $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF = $emit->CNPJ")->fetch();
    
                
                $array = array();
                foreach($det as $item) {
     
                    (string)$ean = $item->prod->cEAN;
                    (string)$prod = $item->prod->cProd;
    
                    if ($ean == 'SEM GTIN') {
                        $aa3citem = (new AA3CITEM2())->find("GIT_COD_ITEM = $code AND TRIM(GIT_REFERENCIA) = '$prod' and GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                        if ($aa3citem) {
                            $array['aa3citem'] = $aa3citem;
                            $array['nota'] = $item->prod;
                            return $array;
                        } else {
                            $aa1forit = (new AA1FORIT2())->find("FORITE_COD_ITEM = $code AND TRIM(FORITE_REFERENCIA) = '$prod' and FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch();
                            if ($aa1forit) {
                            $array['aa1forit'] = $aa1forit;
                            $array['nota'] = $item->prod;
                            return $array;
                            } else {
                                $aa1refit = (new AA1REFIT2())->find("REF_COD_ITEM = $code and TRIM(REF_REFERENCIA) = '$prod' and REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                                if ($aa1refit) {
                            $array['aa1refit'] = $aa1refit;
                            $array['nota'] = $item->prod;
                            return $array;
                                } else {
                                    $array['erro'] = "Não encontrado";
                                    $array['nota'] = $item;
                                    return $array;
                                }
                            }
                        }
                    } else {
                        $aa3ccean = (new AA3CCEAN2())->find("EAN_COD_PRO_ALT = $code||dac($code) and EAN_COD_EAN = '$ean'")->fetch();
                        
                        $array['aa3ccean'] = $aa3ccean;
                        $array['nota'] = $item->prod;
                        
                    }
                    
                }
            } else {
                $array = array();
                foreach($det as $item) {
                    (string)$ean = $item->prod->cEAN;
                    (string)$prod = $item->prod->cProd;
                    if ($ean == 'SEM GTIN') {
                        $aa3citem = (new AA3CITEM2())->find("TRIM(GIT_REFERENCIA) = '$prod' and GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                        if ($aa3citem) {
                            $array['aa3citem'] = $aa3citem;
                            $array['nota'] = $item->prod;
                        } else {
                            $aa1forit = (new AA1FORIT2())->find("TRIM(FORITE_REFERENCIA) = '$prod' and FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch();
                            if ($aa1forit) {
                            $array['aa1forit'] = $aa1forit;
                            $array['nota'] = $item->prod;
                            } else {
                                $aa1refit = (new AA1REFIT2())->find("TRIM(REF_REFERENCIA) = '$prod' and REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                                if ($aa1refit) {
                            $array['aa1refit'] = $aa1refit;
                            $array['nota'] = $item->prod;
                                } else {
                                    $array['erro'] = "Não encontrado";
                                    $array['nota'] = $item;
                                }
                            }
                        }
                    } else {
                        $aa3ccean = (new AA3CCEAN2())->find("EAN_COD_EAN = '$ean'")->fetch();
                        $array['aa3ccean'] = $aa3ccean;
                        $array['nota'] = $item->prod;
                    }
                }
            }
        } else if ($this->group_id == 10) {
            if ($code) {
                $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
                $det = $xml->NFe->infNFe->det;
                $emit = $xml->NFe->infNFe->emit;
    
                $aa2ctipo = (new AA2CTIPO10())->find("TIP_CGC_CPF = $emit->CNPJ")->fetch();
    
                
                $array = array();
                foreach($det as $item) {
     
                    (string)$ean = $item->prod->cEAN;
                    (string)$prod = $item->prod->cProd;
    
                    if ($ean == 'SEM GTIN') {
                        $aa3citem = (new AA3CITEM10())->find("GIT_COD_ITEM = $code AND TRIM(GIT_REFERENCIA) = '$prod' and GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                        if ($aa3citem) {
                            $array['aa3citem'] = $aa3citem;
                            $array['nota'] = $item->prod;
                            return $array;
                        } else {
                            $aa1forit = (new AA1FORIT10())->find("FORITE_COD_ITEM = $code AND TRIM(FORITE_REFERENCIA) = '$prod' and FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch();
                            if ($aa1forit) {
                            $array['aa1forit'] = $aa1forit;
                            $array['nota'] = $item->prod;
                            return $array;
                            } else {
                                $aa1refit = (new AA1REFIT10())->find("REF_COD_ITEM = $code and TRIM(REF_REFERENCIA) = '$prod' and REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                                if ($aa1refit) {
                            $array['aa1refit'] = $aa1refit;
                            $array['nota'] = $item->prod;
                            return $array;
                                } else {
                                    $array['erro'] = "Não encontrado";
                                    $array['nota'] = $item;
                                    return $array;
                                }
                            }
                        }
                    } else {
                        $aa3ccean = (new AA3CCEAN10())->find("EAN_COD_PRO_ALT = $code||dac($code) and EAN_COD_EAN = '$ean'")->fetch();
                        
                        $array['aa3ccean'] = $aa3ccean;
                        $array['nota'] = $item->prod;
                        
                    }
                    
                }
            } else {
                $array = array();
                foreach($det as $item) {
                    (string)$ean = $item->prod->cEAN;
                    (string)$prod = $item->prod->cProd;
                    if ($ean == 'SEM GTIN') {
                        $aa3citem = (new AA3CITEM10())->find("TRIM(GIT_REFERENCIA) = '$prod' and GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                        if ($aa3citem) {
                            $array['aa3citem'] = $aa3citem;
                            $array['nota'] = $item->prod;
                        } else {
                            $aa1forit = (new AA1FORIT10())->find("TRIM(FORITE_REFERENCIA) = '$prod' and FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch();
                            if ($aa1forit) {
                            $array['aa1forit'] = $aa1forit;
                            $array['nota'] = $item->prod;
                            } else {
                                $aa1refit = (new AA1REFIT10())->find("TRIM(REF_REFERENCIA) = '$prod' and REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                                if ($aa1refit) {
                            $array['aa1refit'] = $aa1refit;
                            $array['nota'] = $item->prod;
                                } else {
                                    $array['erro'] = "Não encontrado";
                                    $array['nota'] = $item;
                                }
                            }
                        }
                    } else {
                        $aa3ccean = (new AA3CCEAN10())->find("EAN_COD_EAN = '$ean'")->fetch();
                        $array['aa3ccean'] = $aa3ccean;
                        $array['nota'] = $item->prod;
                    }
                }
            }
        }

        
        
        return $array;
    }

    public function getPessoa() {
        $pessoa = (new GePessoa())->find("STATUS = 'A' and cast(NROCGCCPF||lpad(DIGCGCCPF,2,'0') as number(38)) = ".(integer)$this->getCnpjFornecedor())->fetch();
        return $pessoa;
    }

    public function getMesAno() {
        $mesAno = (new NfReceipt())
        ->find(
            "id = $this->id",
            "",
            "concat(substr(replace(created_at,'-',''),5,2),substr(replace(created_at,'-',''),1,4)) mesano"
            )
        ->fetch();
        return $mesAno->mesano;

    }

    public function getNotaFiscalRetaguarda() {
        $mes = str_pad($this->getMesAno,6,'0',STR_PAD_LEFT);
        $fiscal = (new MultiModels($this->group_id,$mes))
                    ->find("FISC_DST = $this->store_id and FISC_T03_ENS = 'E'")
                    ->fetch();
                    dd($fiscal);
        if ($fiscal) {
            return true;
        } else {
            return false;
        }
    }

}