<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class InventarioContabil extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("invantario_contabil",[],"id", true);        
    }

    public function inventarioContabilItens() {
        $ici = (new InventarioContabilItens())->find("inventario_contabil_id = $this->id")->fetch(true);
        return $ici;
    }

    public function userOperator() {
        $user = (new User())->findById($this->user_id);
        return $user;
    }
}