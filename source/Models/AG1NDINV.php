<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AG1NDINV extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG1NDINV",[],"", false);        
    }

    public function getPrice() {
        $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= :data","data=$this->getDate&pl=$this->getStore&pci=$this->FI_ITEM&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_PRECO;
        }
        return $price;
    }

    public function getDate() {
        $ag2icntr = (new AG2ICNTR())->find("CNI_SEQUEN = :cs and CNI_LOJA_1 = :cl","cs=$this->FI_NUM_INVENT&cl=$this->FI_LOJA")->fetch();
        return "1$ag2icntr->CNI_DATA";
    }

    public function getStore() {
        return (substr($this->FI_LOJA,0,strlen($this->FI_LOJA)-1));
    }

    
}