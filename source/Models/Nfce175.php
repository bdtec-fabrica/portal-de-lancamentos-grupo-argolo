<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer175;

class Nfce175 extends DataLayer175
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}