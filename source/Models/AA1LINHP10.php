<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer10;

class AA1LINHP10 extends DataLayer10
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1LINHP",[],"", false);        
    }

    public function getAA2CESTQ($data) {
        $aa2cestq = (new AA2CESTQ10())->find("substr(GET_COD_PRODUTO,0,length(GET_COD_PRODUTO) - 1) = :gcp and substr(GET_COD_LOCAL,0,length(GET_COD_LOCAL) - 1) = :gcl and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
        rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < :gdue) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
        rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < :gduf)","gcp=$this->LINP_COD_ITEM&gcl=$this->LINP_FILIAL&gdue=$data&gduf=$data")->fetch();
        return $aa2cestq;
    }
}