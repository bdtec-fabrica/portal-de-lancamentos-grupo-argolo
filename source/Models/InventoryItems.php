<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class InventoryItems extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("inventories_items",[],"id", false);        
    }

}