<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class MarginVerification extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("margin_verification",[],"id", true);        
    }

    public function getApprovedName() {
        switch($this->approved){
            case "N":
                return "Não";
                break;
            case "S":
                return "Sim";
                break;
            case "C":
                return "Corrigido";
                break;
        }
    }

    public function getOperator() {
        $nfReceipt = (new NfReceipt())->find("nf_number = :nn and store_id = :sid and substr(updated_at,1,10) = :ua","ua=".substr($this->created_at,0,10)."&nn=$this->nf_number&sid=$this->store_id")->fetch();
        return $nfReceipt->operator_id;
    }
    
}