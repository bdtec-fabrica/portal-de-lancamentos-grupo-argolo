<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class CollectionNfeItems extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("collection_nfe_items",[],"id", false);        
    }
    
}