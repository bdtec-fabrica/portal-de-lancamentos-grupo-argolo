<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;


class AA3CITEM extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA3CITEM",[],"", false);        
    }

    public function getMovement(){
       return $ag1iensa = (new AG1IENSA())-> find("ESITC_CODIGO = :ec and ESCHC_DATA3 = :ed", "ec={$this->GIT_COD_ITEM}&ed=1210812")->fetch(true);
    }

    public function getAA3CITEM($code) {
        return $this->find("GIT_COD_ITEM = $code")->fetch();
    }

    public function code () {
        return $this->GIT_COD_ITEM;
    }

    public function description () {
        return $this->GIT_DESCRICAO;
    }

    public function getInventories($date,$store) {
        $date = substr($date,1);

        $ag2icntr = (new AG2ICNTR())->find("CNI_ITENS != :ci and CNI_DATA = :cd and CNI_LOJA_1 = :cl","ci=C&cd=$date&cl=$store")->order('CNI_DATA')->fetch(true);
        $date2 = '20'.substr($date,1,2)."-".substr($date,3,2)."-".substr($date,5,2);
        $inventories = (new Inventories())->find("store_id = :sid and date_inventory = :di","sid=$store&di=$date2")->fetch(true);
        foreach ($inventories->getInventoryItems as $items) {
            if($items->code == $this->GIT_COD_ITEM) {
                $amount += $items->amount;
            }
        }

        
        foreach($ag2icntr as $icntr) {
            foreach($icntr->getAG1NDINV as $ndinv){
                if ($ndinv->FI_ITEM == $this->GIT_COD_ITEM){
                    $amount += $ndinv->FI_QTD_UNI;
                }
            }           
        }

        return str_replace('.',',',number_format((float)$amount, 2, '.', ''));
    }

    public function getInventoriesCost($date,$store){
        $date = substr($date,1);
        
        $ag2icntr = (new AG2ICNTR())->find("CNI_DATA = :cd and CNI_LOJA_1 = :cl","cd=$date&cl=$store")->order('CNI_SEQUEN')->fetch(true);
        foreach($ag2icntr as $icntr) {
            foreach($icntr->getAG1NDINV as $ndinv){
                if ($ndinv->FI_ITEM == $this->GIT_COD_ITEM){
                    $valor = $ndinv->getPrice * $ndinv->FI_QTD_UNI;
                }
            }           
        }
        return str_replace('.',',',number_format((float)$valor, 2, '.', ''));
        
    }

    public function getSales($date1,$date2,$type,$store) {
        if($type == 1) {
            $aa1ctcon = (new AA1CTCON())->getSaidas1();
        } else {
            $aa1ctcon = (new AA1CTCON())->getSaidas2();
        }
        
        $storeId = substr($store,0,strlen($store)-1);
        $ag1iensa = (new AG1IENSA())->find("ESCHLJC_CODIGO3 = $storeId and ESCHC_AGENDA3 in ($aa1ctcon) and ESCHC_DATA between $date1 and $date2 and ESITC_CODIGO = $this->GIT_COD_ITEM and ENTSAIC_SITUACAO != '9'")->fetch(true);
        
        foreach ($ag1iensa as $iensa) {
            $amount += $iensa->ENTSAIC_QUANTI_UN;
        }
        return str_replace('.',',',number_format((float)$amount, 2, '.', ''));

    }

    public function getSalesCost($date1,$date2,$type,$store){
        if($type == 1) {
            $aa1ctcon = (new AA1CTCON())->getSaidas1();
        } else {
            $aa1ctcon = (new AA1CTCON())->getSaidas2();
        }
        
        $storeId = substr($store,0,strlen($store)-1);
        $ag1iensa = (new AG1IENSA())->find("ESCHLJC_CODIGO3 = $storeId and ESCHC_AGENDA3 in ($aa1ctcon) and ESCHC_DATA between $date1 and $date2 and ESITC_CODIGO = $this->GIT_COD_ITEM and ENTSAIC_SITUACAO != '9'")->fetch(true);
        foreach ($ag1iensa as $iensa) {
            $valor += $iensa->ENTSAIC_PRC_UN * $iensa->ENTSAIC_QUANTI_UN;
        }
        return str_replace('.',',',number_format((float)$valor, 2, '.', ''));
    }

    public function getPurchases($date1,$date2,$type,$store) {
        if($type == 1) {
            $aa1ctcon = (new AA1CTCON())->getEntradas1();
        } else {
            $aa1ctcon = (new AA1CTCON())->getEntradas2();
        }
        $storeId = substr($store,0,strlen($store)-1);
        $ag1iensa = (new AG1IENSA())->find("ESCLC_CODIGO = $storeId and ESCHC_AGENDA3 in ($aa1ctcon) and ESCHC_DATA between $date1 and $date2 and ESITC_CODIGO = $this->GIT_COD_ITEM and ENTSAIC_SITUACAO != '9'")->fetch(true);
        foreach ($ag1iensa as $iensa) {
            $amount += $iensa->ENTSAIC_QUANTI_UN;
        }
        return str_replace('.',',',number_format((float)$amount, 2, '.', ''));

    }

    public function getPurchasesCost($date1,$date2,$type,$store) {
        if($type == 1) {
            $aa1ctcon = (new AA1CTCON())->getEntradas1();
        } else {
            $aa1ctcon = (new AA1CTCON())->getEntradas2();
        }
        $storeId = substr($store,0,strlen($store)-1);
        $ag1iensa = (new AG1IENSA())->find("ESCLC_CODIGO = $storeId and ESCHC_AGENDA3 in ($aa1ctcon) and ESCHC_DATA between $date1 and $date2 and ESITC_CODIGO = $this->GIT_COD_ITEM and ENTSAIC_SITUACAO != '9'")->fetch(true);
        foreach ($ag1iensa as $iensa) {
            $amount += $iensa->ENTSAIC_QUANTI_UN;
            $valor += $iensa->ENTSAIC_PRC_UN * $iensa->ENTSAIC_QUANTI_UN;
        }
        return str_replace('.',',',number_format((float)$valor, 2, '.', ''));
    }

    public function result($date1,$date2,$type,$store) {
        return str_replace('.',',',number_format((float)$this->getSales($date1,$date2,$type,$store) + $this->getInventories($date2,$store) - $this->getPurchases($date1,$date2,$type,$store) - $this->getInventories($date1,$store), 2, '.', ''));
        // venda+estoque final - compras - estoque inicial
    }

    public function resultCost($date1,$date2,$type,$store) {
        return str_replace('.',',',number_format((float)$this->getSalesCost($date1,$date2,$type,$store) + $this->getInventoriesCost($date2,$store) - $this->getPurchasesCost($date1,$date2,$type,$store) - $this->getInventoriesCost($date1,$store), 2, '.', ''));
        // venda+estoque final - compras - estoque inicial
    }

    public function getEstoque($date2,$store) {
        $dimPer = (new DIMPER())->getIdDt(substr($date2, 5, 2).'/'.substr($date2, 3, 2).'/'.'20'.substr($date2, 1, 2));
        $storeClass = (new Store())->find("code = :code","code=$store")->fetch();
        if($storeClass->group_id == 1 || $storeClass->group_id == 10) {
            $aggEstqProd = (new AGGESTQPROD())->find("ID_DT = :ID_DT and CD_FIL = :CD_FIL and CD_PROD = :CD_PROD","CD_PROD=$this->GIT_COD_ITEM&ID_DT=$dimPer&CD_FIL=$store","QTD_ESTOQUE")->fetch();
            if (!$aggEstqProd) {
                return 0;
            } else {
                return $aggEstqProd->QTD_ESTOQUE;
            }
        } else if ($storeClass->group_id == 2) {
            $aggEstqProd = (new AGGESTQPROD2())->find("ID_DT = :ID_DT and CD_FIL = :CD_FIL and CD_PROD = :CD_PROD","CD_PROD=$this->GIT_COD_ITEM&ID_DT=$dimPer&CD_FIL=$store","QTD_ESTOQUE")->fetch();
            if (!$aggEstqProd) {
                return 0;
            } else {
                return $aggEstqProd->QTD_ESTOQUE;
            }
        } else if ($storeClass->group_id == 4 || $storeClass->group_id == 8) {
            $aggEstqProd = (new AGGESTQPROD3())->find("ID_DT = :ID_DT and CD_FIL = :CD_FIL and CD_PROD = :CD_PROD","CD_PROD=$this->GIT_COD_ITEM&ID_DT=$dimPer&CD_FIL=$store","QTD_ESTOQUE")->fetch();
            if (!$aggEstqProd) {
                return 0;
            } else {
                return $aggEstqProd->QTD_ESTOQUE;
            }
        }
        
    }

    public function aa2ctipo() {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :TIP_CODIGO","TIP_CODIGO=$this->GIT_COD_FOR")->fetch();
        return $aa2ctipo;
    }

    public function fornecedor() {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :TIP_CODIGO","TIP_CODIGO=$this->GIT_COD_FOR")->fetch();
        return $aa2ctipo;
    }

    public function aa1ditem() {
        $aa1ditem = (new AA1DITEM())->find("DET_COD_ITEM = :DET_COD_ITEM","DET_COD_ITEM=$this->GIT_COD_ITEM")->fetch();
        return $aa1ditem;
    }

    public function getICMSAliquot(){
        $aa2tfisc = (new AA2TFISC())->find("TFIS_ORIGEM = :t1 AND TFIS_FIGURA = :t2 AND TFIS_CODIGO = :t3
        AND TFIS_AUTOMACAO = :t4","t1=BA&t2=$this->GIT_NAT_FISCAL&t3=512&t4=S")->fetch();
        

        return $aa2tfisc->TFIS_ALIQ_ICM;
    }

    public function getPISCOFINSAliquot(){
        if ($this->GIT_CATEGORIA_ANT == 0){
            return 9.25;
        } else {
            return 0;
        }
        
    }

    public function getLog() {
        $aa0logit = (new AA0LOGIT())->find("LOGIT_TRANSACAO = 0 and LOGIT_COD_ITEM = $this->GIT_COD_ITEM and trim(LOGIT_DADO_ANTERIOR) = 'INCLUSAO DE PRODUTO'","","AA0LOGIT.*,substr(lpad(LOGIT_HOR_ALTER,8,'0'),0,2) HORA")->fetch();
        return $aa0logit;
    }

    public function aa3ccean(){
        $aa3ccean = (new AA3CCEAN())->find("EAN_COD_PRO_ALT = $this->GIT_COD_ITEM$this->GIT_DIGITO")->fetch(true);
        return $aa3ccean;
    }
    
    public function aa1forit() {
        $aa1forit = (new AA1FORIT())->find("FORIT_COD_ITEM = $this->GIT_COD_ITEM")->fetch(true);
        return $aa1forit;
    }

    public function getAa2cestq($loja) {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
        $aa2cestq = (new AA2CESTQ())->find("GET_COD_PRODUTO = $this->GIT_COD_ITEM$this->GIT_DIGITO and GET_COD_LOCAL = $aa2ctipo->TIP_CODIGO$aa2ctipo->TIP_DIGITO")->fetch();
        return $aa2cestq;
    }

    public function getPrice($store) {
        $date = "1".date("ymd");
        $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = :pt","pt=N&pl=$store&pci=$this->GIT_COD_ITEM&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_PRECO;
        }
        return $price;
    }

    public function getMargin($store) {
        if ($this->getPrice($store) > 0){
            $margin = 100-((($this->aa2cestq($store)->GET_CUS_ULT_ENT/$this->getPrice($store))*100)+(($this->getPISCOFINSAliquot-($this->getPISCOFINSAliquot*($this->getICMSAliquot()/100)))+$this->getICMSAliquot()));
            return $margin;
        } else {
            $margin = 0;
        return round($margin,2);
        }                
    }

    public function linha($store) {
        $aa1linhp = (new AA1LINHP())->find("LINP_COD_ITEM = $this->GIT_COD_ITEM and LINP_FILIAL = $store")->fetch();
        if (!empty($aa1linhp)) {
            return true;
        } else {
            return false;
        }
    }
}