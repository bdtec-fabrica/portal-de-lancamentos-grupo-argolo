<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer4;

class AA2CTIPO3 extends DataLayer4
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA2CTIPO",[],"", false);        
    }

    public function aa1forit() {
        $aa1forit = (new AA1FORIT3())->find("FORITE_COD_FORN = $this->TIP_CODIGO")->fetch(true);
        return $aa1forit;
    }
}