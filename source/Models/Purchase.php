<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Purchase extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("purchase",[],"id", true);        
    }

    public function purchaseItems() {
        $purchaseItems = (new PurchaseItem())->find("purchase_id = $this->id")->fetch(true);
        return $purchaseItems;
    }
    

}