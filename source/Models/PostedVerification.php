<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class PostedVerification extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("posted_verification",[],"id", true);        
    }
    
}