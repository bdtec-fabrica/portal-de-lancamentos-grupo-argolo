<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use DateTime;

class CodigoInternoConsinco extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("codigo_interno_consinco",[],"id", true);        
    }

    public function codigoInterno($store) {
        $codigoInterno = (new CodigoInterno())->find("code = $this->code and store_id = $store")->fetch();
        return $codigoInterno;
    }
}