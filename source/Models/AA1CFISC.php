<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AA1CFISC extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1CFISC",[],"", false);        
    }

    public function getAA2CTIPO() {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $this->FIS_LOJ_ORG")->fetch();
        return $aa2ctipo;
    }

    public function getAA2CTIPO2() {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $this->FIS_LOJ_DST")->fetch();
        return $aa2ctipo;
    }

    public  function getDueDate()
    {
        $aa3lvenc = (new AA3LVENC())->find("FIV_SERIE = '$this->FIS_SERIE' and FIV_LOJ_ORG = :flo and FIV_NRO_NOTA = :fnn and
         FIV_OPER = :fo","flo=$this->FIS_LOJ_ORG&fnn=$this->FIS_NRO_NOTA&fo=$this->FIS_OPER")->fetch(true);
         return $aa3lvenc;
    }

    public  function getDueDateAmount()
    {
        $aa3lvenc = (new AA3LVENC())->find("FIV_SERIE = '$this->FIS_SERIE' and FIV_LOJ_ORG = :flo and FIV_NRO_NOTA = :fnn and
         FIV_OPER = :fo","flo=$this->FIS_LOJ_ORG&fnn=$this->FIS_NRO_NOTA&fo=$this->FIS_OPER")->fetch(true);
         $i = 0;
         foreach($aa3lvenc as $a) {
            $i++;
         }
         return $i;
    }

    public function getProvider(){
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :tc","tc=$this->FIS_LOJ_ORG")->fetch();
        return $aa2ctipo;
    }

    public function getAccessKey() {
        $nfeControle = (new NFE_CONTROLE())->find("substr(CHAVE_ACESSO_NFE,7,28) = :CHAVE_ACESSO_NFE",
        "CHAVE_ACESSO_NFE=".$this->getProvider()->TIP_CGC_CPF."55".str_pad(str_replace(" ","",$this->FIS_SERIE),3,'0',STR_PAD_LEFT).str_pad($this->FIS_NRO_NOTA,9,'0',STR_PAD_LEFT))->fetch();
        return $nfeControle->CHAVE_ACESSO_NFE;
        // 
    }

    public function getNfReceipt() {
        $nfReceipt = (new NfReceipt())->find("access_key = :access_key","access_key=$this->getAccessKey")->fetch(true);
        
        foreach ($nfReceipt as $nR) {
            $nfReceiptReturn = $nR;
            // if (strlen($nR->access_key) == 44 ) {
            //     if (substr($nR->access_key,6,14) == $this->getProvider()->TIP_CGC_CPF) { 
            //     }
            // } else {
            //     if ('1'.substr($nR->updated_at,2,2).substr($nR->updated_at,5,2).substr($nR->updated_at,8,2) == '1'.$this->FIS_DT_PROC && $this->FIS_NRO_NOTA == $nR->nf_number){
            //         $nfReceiptReturn = $nR;
            //     }    
            // }
        }
        
        return $nfReceiptReturn;
    }

    public function ag1iensa() {
        $aa1tcon = (new AA1CTCON())->getEntradas4;
        $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 in ($aa1tcon) AND ESCHC_AGENDA3 != 499 AND ESCHC_AGENDA3 != $this->FIS_OPER AND ESCHC_AGENDA3 in (1,11,9) AND ESCHC_NRO_NOTA3 = $this->FIS_NRO_NOTA AND ESCHLJC_CODIGO3 = $this->FIS_LOJ_ORG AND ESCHC_SER_NOTA3 = $this->FIS_SERIE AND ESCLC_CODIGO = $this->FIS_LOJ_DST")->fetch(true);
        $i = 0;
        foreach($ag1iensa as $iensa) {
            $i++;
        }
        return $i;
    }

    public function ag1iensaCredito() {
        $aa1tcon = (new AA1CTCON())->getEntradas4;
        $credito = false;
        $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 in ($aa1tcon) AND ESCHC_AGENDA3 != 499 AND ESCHC_AGENDA3 != $this->FIS_OPER AND ESCHC_NRO_NOTA3 = $this->FIS_NRO_NOTA AND ESCHLJC_CODIGO3 = $this->FIS_LOJ_ORG AND ESCHC_SER_NOTA3 = $this->FIS_SERIE AND ESCLC_CODIGO = $this->FIS_LOJ_DST")->fetch(true);
        foreach($ag1iensa as $iensa) {
            if ($iensa->ENTSAIC_PERC_ICMS < 12) {
                $credito = true;
            }
        }
        return $credito;
    }

    public function dataAgenda(){
        return substr($this->FIS_DTA_AGENDA,5,2)."/".substr($this->FIS_DTA_AGENDA,3,2)."/".substr($this->FIS_DTA_AGENDA,1,2);
    }

    
}