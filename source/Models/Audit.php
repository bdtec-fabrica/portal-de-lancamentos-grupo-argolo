<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Audit extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("audit",[],"id", true);        
    }

}