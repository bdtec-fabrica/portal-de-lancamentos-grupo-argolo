<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer3;

class AG1PDVCC2 extends DataLayer3
{
    public function __construct() {
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG1PDVCC",[],"", false);        
    }
}