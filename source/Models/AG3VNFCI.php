<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AG3VNFCI extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG3VNFCI_".AA1FFISI,[],"", false);        
    }
}