<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;

class MapProdCodigo extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("consinco.map_prodcodigo",[]);        
    }

    public function mapProduto() {
        $mapProduto = (new MapProduto())->find('SEQPRODUTO = $this->SEQPRODUTO')->fetch();
        return $mapProduto;
    }

}
