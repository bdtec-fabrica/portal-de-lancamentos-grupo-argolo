<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class RegisterAmountItems extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("register_amount_items",[],"id", true);        
    }
    
}