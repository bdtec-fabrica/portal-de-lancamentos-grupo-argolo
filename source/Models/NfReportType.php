<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class NfReportType extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_report_type",["name"],"id", true);        
    }
}