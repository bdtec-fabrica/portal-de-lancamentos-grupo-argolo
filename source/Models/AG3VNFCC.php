<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AG3VNFCC extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG3VNFCC_".AA1FFISI,[],"", false);        
    }
    
    public function AG3VNFCI() {
        $aa1ffisi = (new AG3VNFCI())->find("NFCI_ID = $this->NFCC_ID")->fetch(true);
        return $aa1ffisi;
    }
}