<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Inventories extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("inventories",[],"id", true);        
    }

    public function getInventoryItems() {
        $inventoryItems = (new InventoryItems())->find("inventory_id = :ii","ii=$this->id")->fetch(true);
        return $inventoryItems;
    }

}