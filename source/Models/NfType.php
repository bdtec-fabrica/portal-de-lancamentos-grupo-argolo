<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class NfType extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfe_type",["name"],"id", true);        
    }
}