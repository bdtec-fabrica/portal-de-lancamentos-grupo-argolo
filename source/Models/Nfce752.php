<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer752;

class Nfce752 extends DataLayer752
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}