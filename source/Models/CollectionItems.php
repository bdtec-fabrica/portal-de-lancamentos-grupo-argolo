<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class CollectionItems extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("collection_items",[],"id", false);        
    }
    
    public function getQuantity(){
        return $this->quantity_found;
    }
    public function getEan(){
        return $this->ean_found;
    }
    
    public function getStatus() {
        if ($this->quantity_found == 0){
            return "E";
        } else if ($this->quantity != $this->quantity_found) {
            return "E";
        } else {
            return "C";
        }
    }
    public function getPackingQuantity(){
        return $this->packing_quantity;
    }
}