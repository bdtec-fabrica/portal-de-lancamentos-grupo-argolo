<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AG2DETNT extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG2DETNT",[],"", false);        
    }

    public function getItemRegister(){
        $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM = substr(to_char($this->REN_ITEM,'fm000000000000000'),8,7)")->fetch();
        return $aa3citem;
    }

    public function getPrice() {
        $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $this->REN_DTA_AGENDA and PRE_TIPO = :pt","pt=N&pl=".substr($this->REN_DESTINO,0,strlen($this->REN_DESTINO)-1)."&pci=".$this->getItemRegister()->GIT_COD_ITEM."&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_PRECO;
        }
        return $price;
    }
    
    public function getICMSAliquot(){
        $aa2cloja = (new AA2CLOJA())->find("LOJ_CODIGO = :lc","lc=".substr($this->REN_DESTINO,0,strlen($this->REN_DESTINO)-1))->fetch();
        $aa2tfisc = (new AA2TFISC())->find("TFIS_ORIGEM = :t1 AND TFIS_FIGURA = :t2 AND TFIS_CODIGO = :t3
        AND TFIS_AUTOMACAO = :t4","t1=BA&t2=$this->REN_FIGURA&t3=512&t4=$aa2cloja->LOJ_AUTONOMIA")->fetch();
        
        

        return $aa2tfisc->TFIS_ALIQ_ICM;
    }

    public function getPISCOFINSAliquot(){
        if ($this->getItemRegister()->GIT_CATEGORIA_ANT == 0){
            return 9.25;
        } else {
            return 0;
        }
        
    }
    
    public function getMargin() {
        if ($this->getPrice() > 0){
            $margin = 100-((($this->REN_CUS_UN/$this->getPrice())*100)+($this->getPISCOFINSAliquot+$this->getICMSAliquot));
            return $margin;
        } else {
            $margin = 0;
            return round($margin,2);
        }
        
                
    }

    public function dadosFornecedor() {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $this->REN_DISTRIB")->fetch();
        return $aa2ctipo;
    }

    public function aa1forit() {
        $aa1forit= (new AA1FORIT())->find("FORITE_COD_ITEM||FORITE_DIG_ITEM = ".$this->getItemRegister()->GIT_COD_ITEM.$this->getItemRegister()->GIT_DIGITO.
        " and FORITE_COD_FORN = ".$this->REN_DISTRIB)->fetch();
        return $aa1forit;
    }

    public function aa1refit() {
        $aa1forit= (new AA1REFIT())->find("REF_COD_ITEM = ".$this->getItemRegister()->GIT_COD_ITEM." and REF_COD_FOR = ".$this->REN_DISTRIB)->fetch(true);
        return $aa1forit;
    }



    public function ultimaEntradaAG1IENSA() {
        $ag1iensa = (new AG1IENSA())->find("ESCHC_NRO_NOTA3 != $this->REN_NOTA and ENTSAIC_SITUACAO != '9' AND  ESCLC_CODIGO = ".substr($this->REN_DESTINO,0,strlen($this->REN_DESTINO)-1).
        " and ESCHLJC_CODIGO3 = $this->REN_DISTRIB and ESITC_CODIGO||ESITC_DIGITO = ".substr(str_pad($this->REN_ITEM,15,'0',STR_PAD_LEFT),8,7))->order("ESCHC_DATA ASC")->fetch(true);

        foreach($ag1iensa as $iensa) {
            $valor = $iensa->ENTSAIC_PRC_UN;
        }
        // REN_ITEM codigo do item apartir do 7
        if($valor == null) {
            return '0';
        } else {
            return $valor;
        }
        
    }

    public function dtUltimaEntradaAG1IENSA() {
        $ag1iensa = (new AG1IENSA())->find("ESCHC_NRO_NOTA3 != $this->REN_NOTA and ENTSAIC_SITUACAO != '9' AND  ESCLC_CODIGO = ".substr($this->REN_DESTINO,0,strlen($this->REN_DESTINO)-1).
        " and ESCHLJC_CODIGO3 = $this->REN_DISTRIB and ESITC_CODIGO||ESITC_DIGITO = ".substr(str_pad($this->REN_ITEM,15,'0',STR_PAD_LEFT),8,7))->order("ESCHC_DATA ASC")->fetch(true);

        foreach($ag1iensa as $iensa) {
            $valor = $iensa->date();
        }
        // REN_ITEM codigo do item apartir do 7
        if($valor == null) {
            return '0';
        } else {
            return $valor;
        }
        
    }

    public function nFUltimaEntradaAG1IENSA() {
        $ag1iensa = (new AG1IENSA())->find("ESCHC_NRO_NOTA3 != $this->REN_NOTA and ENTSAIC_SITUACAO != '9' AND  ESCLC_CODIGO = ".substr($this->REN_DESTINO,0,strlen($this->REN_DESTINO)-1).
        " and ESCHLJC_CODIGO3 = $this->REN_DISTRIB and ESITC_CODIGO||ESITC_DIGITO = ".substr(str_pad($this->REN_ITEM,15,'0',STR_PAD_LEFT),8,7))->order("ESCHC_DATA ASC")->fetch(true);

        foreach($ag1iensa as $iensa) {
            $valor = $iensa->ESCHC_NRO_NOTA3;
        }
        // REN_ITEM codigo do item apartir do 7
        if($valor == null) {
            return '0';
        } else {
            return $valor;
        }
        
    }

    public function references() {
        foreach ($this->aa1refit() as $refit) {
            $reference[$refit->REF_REFERENCIA] = $refit->REF_REFERENCIA;
        }
        $reference[$this->aa1forit()->FORITE_REFERENCIA] = $this->aa1forit()->FORITE_REFERENCIA;
        $reference[$this->getItemRegister()->GIT_REFERENCIA] = $this->getItemRegister()->GIT_REFERENCIA;
        return $reference;
    }

    public function eans() {
        $aa3ccean = (new AA3CCEAN())->find("EAN_COD_PRO_ALT = ".$this->getItemRegister()->GIT_COD_ITEM.$this->getItemRegister()->GIT_DIGITO)->fetch(true);
        foreach($aa3ccean as $ean) {
            $eans[$ean->EAN_COD_EAN] = $ean->EAN_COD_EAN;
        }
        return $eans;
    }

    public function diferencaValor() {
        if ($this->ultimaEntradaAG1IENSA == '0') {
            $valor = 100;
        } else {
            $valor = (($this->ultimaEntradaAG1IENSA - $this->REN_PRC_UN)/$this->ultimaEntradaAG1IENSA)*100;
        }
        
        return round($valor,2);
    }    
}