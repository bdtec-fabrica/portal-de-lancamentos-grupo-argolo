<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class OxHistory extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("ox_history",[],"id", true);        
    }

    public function getOxDeboningHistory(){
        $oxDeboningHistory = (new OxDeboningHistory)->find("ox_history_id = :id","id=$this->id")->order("reference,description")->fetch(true);
        return $oxDeboningHistory;
    }

    public function getOxPurchaseHistory(){
        $oxPurchaseHistory = (new OxPurchaseHistory)->find("ox_history_id = :id","id=$this->id")->order("reference,description")->fetch(true);
        return $oxPurchaseHistory;
    }  
}