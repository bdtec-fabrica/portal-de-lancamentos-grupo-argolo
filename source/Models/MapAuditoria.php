<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;

class MapAuditoria extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("consinco.map_auditoria",[]);        
    }
}
