<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AA1RTITU extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1RTITU",[],"", false);        
    }

    public function aa2ctipo() {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF = $this->DUP_CGC_CPF")->fetch();
        return $aa2ctipo;
    }
}