<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer4;

class AG2DETNT3 extends DataLayer4
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG2DETNT",[],"", false);        
    }

    public function getItemRegister(){
        $ag2detnt = (new AG2DETNT3())->find("REN_DESTINO = :REN_DESTINO and REN_TPO_AGENDA = :REN_TPO_AGENDA and REN_NOTA = :REN_NOTA and REN_ITEM = :REN_ITEM",
        "REN_DESTINO=$this->REN_DESTINO&REN_TPO_AGENDA=$this->REN_TPO_AGENDA&REN_NOTA=$this->REN_NOTA&REN_ITEM=$this->REN_ITEM","to_number(substr(to_char(REN_ITEM, 'FM000000000000000'),8,7)) item")->fetch();
        $aa3citem = (new AA3CITEM3())->find("GIT_COD_ITEM = :gci","gci=$ag2detnt->item")->fetch();
        return $aa3citem;
    }

    public function getPrice() {
        $aa2cprec = (new AA2CPREC3())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $this->REN_DTA_AGENDA and PRE_TIPO = :pt","pt=N&pl=".substr($this->REN_DESTINO,0,strlen($this->REN_DESTINO)-1)."&pci=".$this->getItemRegister()->GIT_COD_ITEM."&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_PRECO;
        }
        return $price;
    }
    
    public function getICMSAliquot($store){
        $aa2cloja = (new AA2CLOJA3())->find("LOJ_CODIGO = :lc","lc=".substr($this->REN_DESTINO,0,strlen($this->REN_DESTINO)-1)."")->fetch();
        $aa2tfisc = (new AA2TFISC3())->find("TFIS_ORIGEM = :t1 AND TFIS_FIGURA = :t2 AND TFIS_CODIGO = :t3
        AND TFIS_AUTOMACAO = :t4","t1=BA&t2=$this->REN_FIGURA&t3=512&t4=$aa2cloja->LOJ_AUTONOMIA")->fetch();
        
        

        return $aa2tfisc->TFIS_ALIQ_ICM;
    }

    public function getPISCOFINSAliquot(){
        if ($this->getItemRegister()->GIT_CATEGORIA_ANT == 0){
            return 9.25;
        } else {
            return 0;
        }
        
    }
    
    public function getMargin() {
        if ($this->getPrice() > 0){
            $margin = 100-((($this->REN_CUS_UN/$this->getPrice())*100)+($this->getPISCOFINSAliquot+$this->getICMSAliquot(substr($this->REN_DESTINO,0,strlen($this->REN_DESTINO)-1))));
            return $margin;
        } else {
            $margin = 0;
        return round($margin,2);
        }
        
                
    }

    
}