<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer5;

class AA1DITEM4 extends DataLayer5
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("rms.AA1DITEM",[],"", false);        
    }

    public function aa3citem() {
        $aa3citem = (new AA3CITEM4())->find("GIT_COD_ITEM = :GIT_COD_ITEM","GIT_COD_ITEM=$this->DET_COD_ITEM")->fetch();
        return $aa3citem;
    }

    public function NCMValidation() {
        $ncm = (new NCM())->find("ncm = :ncm","ncm=".str_pad($this->DET_CLASS_FIS, 8, 0, STR_PAD_LEFT))->count();
        switch ($ncm) {
            case 1:
                return true;
                break;            
            case 0:
                return false;
                break;
        }
    }
}