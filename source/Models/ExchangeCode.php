<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ExchangeCode extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("exchange_code",["store_id","cnpj"],"id", false);        
    }
}