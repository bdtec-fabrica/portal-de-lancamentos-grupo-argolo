<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class FiscalStores extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("fiscal_store",[],"id", false);        
    }

    public function file1 ($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 1 and type_id = 1 and activity_type_id = 7")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else if ( $this->apura_periodo == 'N') {
            return "Confere Mensal";
        }
         else {
            if (date("mYd") >= $date.'11') {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
            
        }
        
    }

    public function file2 ($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 2 and type_id = 1 and activity_type_id = 7")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else if ( $this->apura_periodo == 'N') {
            return "Confere Mensal";
        } else {
            if (date("mYd") >= $date.'21') {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function file3 ($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 7")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else if ( $this->apura_periodo == 'N') {
            return "Confere Mensal";
        } else {
            if (date('mY') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function file4 ($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 2 and activity_type_id = 7")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else {
            if (date('mY') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function inconsistence1($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 1 and type_id = 1 and activity_type_id = 8")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else if ( $this->apura_periodo == 'N') {
            return "Confere Mensal";
        } else {
            if (date("mYd") >= $date.'11') {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function inconsistence2($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 2 and type_id = 1 and activity_type_id = 8")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else if ( $this->apura_periodo == 'N') {
            return "Confere Mensal";
        } else {
            if (date("mYd") >= $date.'21') {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function inconsistence3($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 8")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else {
            if (date('mY') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function spreadsheet($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 10")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else {
            if (date('mY') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function sailsConference($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 9")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else {
            if (date('mY') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function ICMSClousure($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 11")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else {
            if (date('mY') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function fileSended($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 2 and activity_type_id = 12")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else {
            if (date('mY') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
        
    }

    public function fileSendedF($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 13")->fetch();
        if ($fiscalActivity) {
            return "OK - $fiscalActivity->getUserName";
        } else {
            if (date('mY') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Pendente";
            }
        }
    }

    public function pendente($store_id, $date) {
        if (strripos($this->file1($store_id, $date),"Pendente Agora") 
        or strripos($this->file2($store_id, $date),"Pendente Agora") 
        or strripos($this->file3($store_id, $date),"Pendente Agora") 
        or strripos($this->inconsistence1($store_id, $date),"Pendente Agora") 
        or strripos($this->inconsistence2($store_id, $date),"Pendente Agora") 
        or strripos($this->inconsistence3($store_id, $date),"Pendente Agora") 
        or strripos($this->spreadsheet($store_id, $date),"Pendente Agora") 
        or strripos($this->sailsConference($store_id, $date),"Pendente Agora")
        or strripos($this->ICMSClousure($store_id, $date),"Pendente Agora") 
        or strripos($this->fileSended($store_id, $date),"Pendente Agora") 
        or strripos($this->fileSendedF($store_id, $date),"Pendente Agora")) {
            return true;
        } else {
            return false;
        }
    }

    //segundos metodos
    public function file1_ ($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 1 and type_id = 1 and activity_type_id = 7")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function file2_ ($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 2 and type_id = 1 and activity_type_id = 7")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function file3_ ($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 7")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function file4_ ($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 2 and activity_type_id = 7")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function inconsistence1_($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 1 and type_id = 1 and activity_type_id = 8")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function inconsistence2_($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 2 and type_id = 1 and activity_type_id = 8")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function inconsistence3_($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 8")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function spreadsheet_($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 10")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function sailsConference_($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 9")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function ICMSClousure_($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 11")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function fileSended_($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 2 and activity_type_id = 12")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function fileSendedF_($store_id, $date) {
        $fiscalActivity = (new FiscalActivity())->find("store_id = $store_id and month = $date and period_id = 3 and type_id = 1 and activity_type_id = 13")->fetch();
        if ($fiscalActivity) {
            return $fiscalActivity;
        } else {
            return "";
        }
        
    }

    public function KW($store,$date) {

        $kw = (new KwConference())->find("date = '$date' and store_id = $store")->fetch();
        if ($kw) {
            return "OK - $kw->getUserName";
        } else {
            if (date('Y-m-d') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Aguardando...";
            }
        }
    }

    public function Conector($store,$date) {

        $kw = (new ConectorConference())->find("date = '$date' and store_id = $store")->fetch();
        if ($kw) {
            return "OK - $kw->getUserName";
        } else {
            if (date('Y-m-d') > $date ) {
                return "<b>Pendente Agora</b>";
            } else {
                return "Aguardando...";
            }
        }
    }
    

}