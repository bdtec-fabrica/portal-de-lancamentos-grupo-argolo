<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer5;

class AA1CFISC4 extends DataLayer5
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("rms.AA1CFISC",[],"", false);        
    }

    public  function getDueDate()
    {
        $aa3lvenc = (new AA3LVENC3())->find("FIV_SERIE = $this->FIS_SERIE and FIV_LOJ_ORG = :flo and FIV_NRO_NOTA = :fnn and
         FIV_OPER = :fo","flo=$this->FIS_LOJ_ORG&fnn=$this->FIS_NRO_NOTA&fo=$this->FIS_OPER")->fetch(true);
         return $aa3lvenc;
    }

    public  function getDueDateAmount()
    {
        $aa3lvenc = (new AA3LVENC3())->find("FIV_SERIE = $this->FIS_SERIE and FIV_LOJ_ORG = :flo and FIV_NRO_NOTA = :fnn and
         FIV_OPER = :fo","flo=$this->FIS_LOJ_ORG&fnn=$this->FIS_NRO_NOTA&fo=$this->FIS_OPER")->fetch(true);
         $i = 0;
         foreach($aa3lvenc as $a) {
            $i++;
         }
         return $i;
    }

    public function getProvider(){
        $aa2ctipo = (new AA2CTIPO4())->find("TIP_CODIGO = :tc","tc=$this->FIS_LOJ_ORG")->fetch();
        return $aa2ctipo;
    }

    public function getAccessKey() {
        $nfeControle = (new NFE_CONTROLE4())->find("substr(CHAVE_ACESSO_NFE,26,9) = :CHAVE_ACESSO_NFE and dateto_rms7(DATA_PROCESSAMENTO) = :DATA_PROCESSAMENTO",
        "DATA_PROCESSAMENTO=$this->FIS_DTA_AGENDA&CHAVE_ACESSO_NFE=".str_pad($this->FIS_NRO_NOTA,9,'0',STR_PAD_LEFT))->fetch();
        return $nfeControle;
    }

    public function getNfReceipt() {
        $nfReceipt = (new NfReceipt())->find("nf_number = :nn and store_id = :sid","nn=$this->FIS_NRO_NOTA&sid=$this->FIS_LOJ_DST")->fetch(true);
        foreach ($nfReceipt as $nR) {
            if (strlen($nR->access_key) == 44 ) {
                if (substr($nR->access_key,6,14) == $this->getProvider()->TIP_CGC_CPF) {
                    $nfReceiptReturn = $nR;
                }
            } else {
                if ('1'.substr($nR->updated_at,2,2).substr($nR->updated_at,5,2).substr($nR->updated_at,8,2) == $this->FIS_DTA_AGENDA){
                    $nfReceiptReturn = $nR;
                }
                
            }
        }
        return $nfReceiptReturn;
    }

    
}