<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer400;

class Nfce400 extends DataLayer400
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}