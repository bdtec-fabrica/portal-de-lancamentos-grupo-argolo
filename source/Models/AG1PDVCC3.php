<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer4;

class AG1PDVCC3 extends DataLayer4
{
    public function __construct() {
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG1PDVCC",[],"", false);        
    }
}