<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;

class MlfNotaFiscal extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("consinco.Mlf_Notafiscal",[]);        
    }

    public function mlfNfitem() {
        $mlfNfitem = (new MlfNfitem())->find("numeronf = $this->NUMERONF and nroempresa = $this->NROEMPRESA")->fetch(true);
        return $mlfNfitem;
    }  

    public function gePessoa() {
        $gePessoa = (new GePessoa())->find("SEQPESSOA = $this->SEQPESSOA")->fetch();
        return $gePessoa;
    }

    public function getDueDateAmount() {
        $fiTitulo = (new FiTitulo())->find("STATUSTITULOFIN is null and NRODOCUMENTO = $this->NUMERONF and NROEMPRESA = $this->NROEMPRESA and SEQPESSOA = ".$this->gePessoa()->SEQPESSOA)->fetch(true);

        $i = 0;
        foreach ($fiTitulo as $titulo) {
            $i++;
        }
        return $i;
    }

    public function getDueDate() {
        $fiTitulo = (new FiTitulo())->find("STATUSTITULOFIN is null and NRODOCUMENTO = $this->NUMERONF and NROEMPRESA = $this->NROEMPRESA and SEQPESSOA = ".$this->gePessoa()->SEQPESSOA)->order("DTAVENCIMENTO ASC")->fetch(true);

        return $fiTitulo;
    }

}
