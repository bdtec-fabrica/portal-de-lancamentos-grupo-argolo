<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class LogManifestation extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("log_manifestacao",[],"id", true);        
    }

    public function getName() {
        $user = (new User())->findById($this->user_id);
        return $user;
    }

    public function getManifestationName() {
        switch ($this->manifestation) {
            case '210200':
            return "Confirmação da Operação";
                break;
            case '210210':
            return "Ciência da Operação";
                break;
            case '210220':
            return "Desconhecimento da Operação";
                break;
            case '210240':
            return "Operação não Realizada";
                break;
        }
    }
}