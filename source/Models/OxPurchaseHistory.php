<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class OxPurchaseHistory extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("ox_purchase_history",[],"id", true);        
    }
    
}