<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer51;

class Nfce51 extends DataLayer51
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("retag.nfce",[],"", false);        
    }
    
}