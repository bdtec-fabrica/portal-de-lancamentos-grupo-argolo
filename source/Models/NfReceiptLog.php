<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class NfReceiptLog extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_receipt_log",["description", "user_id","nf_receipt_id"],"id", true);        
    }

    public function getUser(){
        return (new User())->find("id = :rid","rid={$this->user_id}")->fetch();

    }

    public function getCreatedAt()
    {
    return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)." | ".substr($this->created_at,11);
    }
}