<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class NFE_CONTROLE extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("NFE_CONTROLE",[],"", false);        
    }

    public function getFornecedor() {
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF = ".SUBSTR(trim($this->CHAVE_ACESSO_NFE), 6, 14))->fetch();
        return $aa2ctipo;
    }

    public function getAa1cfisc(){
        $aa1cfisc = (new AA1CFISC())->find($this->getFornecedor()->TIP_CODIGO." = FIS_LOJ_ORG AND ".(integer)SUBSTR(TRIM($this->CHAVE_ACESSO_NFE), 22, 3)." = TO_NUMBER(FIS_SERIE) AND '".SUBSTR(TRIM($this->CHAVE_ACESSO_NFE), 25, 9)."' = LPAD(FIS_NRO_NOTA, 9, '0') AND $this->CODIGO_SITUACAO = 99")->fetch();
        return $aa1cfisc;
    }

    
}