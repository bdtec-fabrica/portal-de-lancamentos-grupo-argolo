<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;
use \Datetime;

class MultiModels extends DataLayer2
{

    public function __construct($verificar, $mes) {
        $this->verificar = $verificar;
        parent::__construct("rms.AA1FFISC_$mes", [], "id", true);
    }
}