<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer3;

class AA1CTCON2 extends DataLayer3
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1CTCON",[],"", false);        
    }

    public function getComprasFiscal($type = null) {
        if ($type == 'T' ||$type == 'C' ||$type == 'D' ||$type == 'R' ||$type == 'F' ||$type == 'V' ||$type == 'T' ||$type == 'O') {
            $agendas = $this->find("TBC_INTG_3 = 'E' and TBC_INTG_4 = 'S' and TBC_INTG_11 = '$type'")->fetch(true);
        } else if ($type == 1){
            $agendas = $this->find("TBC_INTG_3 = 'E' and TBC_INTG_4 = 'S'")->fetch(true);
        } else {
            $agendas = $this->find("TBC_INTG_3 = 'E' and TBC_INTG_4 = 'S'")->fetch(true);
        }
        
        foreach ($agendas as $agenda) {
            $agendasCompras[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
            
        }
        return implode(' ,',$agendasCompras);
    }

    public function getEmiteNota() {
        $agendas = $this->find("tbc_intg_2 = :t2","t2=S")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getEntradas1() {
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_intg_7 = :t7 and tbc_codigo = :tc","t3=E&t9=S&t7=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getEntradas2() {
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_intg_7 = :t7 and tbc_intg_11 = :t11 and tbc_codigo = :tc","t11=C&t3=E&t9=S&t7=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getEntradas4() {
        $agendas = $this->find("tbc_intg_4 = 'S' and tbc_intg_2 = 'N' and tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_codigo = :tc","t3=E&t9=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getEntradas3() { // entradas
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_codigo = :tc","t3=E&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getSaidas1() {
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_intg_7 = :t7 and tbc_codigo = :tc","t3=S&t9=S&t7=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getSaidas2() {
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_intg_7 = :t7 and tbc_intg_11 = :t11 and tbc_codigo = :tc","t11=V&t3=S&t9=S&t7=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function entradasAtualizaCusto() {// atualiza custo e estoque.
        $agendas = $this->find("TBC_INTG_3 = 'E' AND TBC_INTG_7 = 'S' AND TBC_INTG_8 = 'S'")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }    
}