<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AG1PDVCC extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG1PDVCC",[],"", false);        
    }
}