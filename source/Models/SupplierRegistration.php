<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class SupplierRegistration extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("supplier_registration",["cnpj"],"id", false);        
    }

}