<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class NfStarted extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_started",[],"id", true);        
    }

    public function getStore(){
        $store = (new NfSefaz())->find("access_key = :access_key","access_key={$this->access_key}")->fetch();
        $store = $store->store_id;
        return $store;

    }

    public function getNfNumber(){
        return (integer)substr($this->access_key,25,9);
    }

    public function getCreatedAt()
    {
        return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)."<br>".substr($this->created_at,11);
    }

    public function getUpdatedAt()
    {
        return substr($this->updated_at,8,2)."/".substr($this->updated_at,5,2)."/".substr($this->updated_at,0,4)."<br>".substr($this->updated_at,11);
    }

    public function getFornecedor() {
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->getStore.'/NFe'.$this->access_key.'.xml');
            $xNome = $xml->NFe->infNFe->emit->xNome;
            return $xNome;
    }

    public function itemsAmount() {
        $xml1 = simplexml_load_file(__DIR__."/../XML/".$this->getStore.'/NFe'.$this->access_key.'.xml');
        $xml = $xml1->NFe->infNFe;
        $i=0;
        foreach($xml->det as $det){
            $i++;
        }
        return $i;
    }
    
    public function getStatus() {
        $status = (new NfStatus());
        $status = $status->find("id = :status_id","status_id={$this->status_id}")->fetch();
        return $status->name;
    }
}