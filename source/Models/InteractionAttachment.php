<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class InteractionAttachment extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("interaction_attachment",["interaction_id","name","type"],"id", false);        
    }

    public function getName(){
        if ($this->type == "I") {
            $name = substr($this->name,24);

        } elseif ($this->type == "F") {
            $name = substr($this->name,22);

        }
        return $name;
    }

}