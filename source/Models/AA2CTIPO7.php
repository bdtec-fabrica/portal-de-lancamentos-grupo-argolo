<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer8;

class AA2CTIPO7 extends DataLayer8
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA2CTIPO",[],"", false);        
    }

    public function aa1forit() {
        $aa1forit = (new AA1FORIT())->find("FORITE_COD_FORN = $this->TIP_CODIGO")->fetch(true);
        return $aa1forit;
    }

    
}