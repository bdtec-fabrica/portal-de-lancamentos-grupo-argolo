<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use DateTime;

class CodigoInterno extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("codigo_interno",[],"id", true);        
    }

    public function codigoInternoConsinco() {
        $array['tabela'] = "map_prodcodigo";
        $array['where'] = "CODACESSO = '$this->code' and TIPCODIGO = 'B'";

        $json = json_encode($array);

        $ch = curl_init('http://indicadores-redemais.bdtecsolucoes.com.br:8080/bdtecindicadores/acesso');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($json))                                                                       
        );
        $jsonRet = curl_exec($ch);
        $aa3citem2 = json_decode($jsonRet);
        

        
        return $aa3citem2;
    }
}