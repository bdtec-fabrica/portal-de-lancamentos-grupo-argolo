<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Aa3citemnm extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("aa3citem",[],"GIT_COD_ITEM", false);        
    }
    public function getAA3CITEM2($code) {
        return $this->find("concat(GIT_COD_ITEM,GIT_DIGITO) = $code")->fetch();
    }

    public function getAA3CITEM($code) {
        return $this->find("GIT_COD_ITEM = $code")->fetch();
    }

}