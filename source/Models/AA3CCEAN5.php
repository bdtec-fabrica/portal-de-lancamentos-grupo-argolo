<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;


class AA3CCEAN5 extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("rms.AA3CCEAN",[],"", false);        
    }

    public function aa3citem() {
        $aa3citem = (new AA3CITEM5())->find("GIT_COD_ITEM = ".substr($this->EAN_COD_PRO_ALT,0,strlen($this->EAN_COD_PRO_ALT)-1))->fetch();
        return $aa3citem;
    }

    public function aa1forit() {
        $aa1forit = (new AA1FORIT5())->find("FORITE_COD_ITEM = ".substr($this->EAN_COD_PRO_ALT,0,strlen($this->EAN_COD_PRO_ALT)-1))->fetch(true);
        return $aa1forit;
    }

    
    public function aa2ctipo() {
        $aa2ctipo = (new AA2CTIPO5())->find("TIP_CODIGO = ".$this->aa3citem()->GIT_COD_FOR)->fetch();
        return $aa2ctipo;
    }
}