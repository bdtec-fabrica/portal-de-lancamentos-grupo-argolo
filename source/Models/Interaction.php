<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Interaction extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("interactions",["id_nf_receipt","id_user","description"],"id", true);        
    }

    public function user()
    {
        return (new User())->find("id = :rid","rid={$this->id_user}")->fetch();
    }

    public function getCreatedAt()
    {
    return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)."  ".substr($this->created_at,11);
    }

    public function nfReceipt()
    {
        return (new NfReceipt())->find("id = :rid","rid={$this->id_nf_receipt}")->fetch();
    }

    public function imageAttachment()
    {
        return (new InteractionAttachment())->find("interaction_id = :rid AND type = :tid","rid={$this->id}&tid=I")->fetch(true);
    }

    public function fileAttachment()
    {
        return (new InteractionAttachment())->find("interaction_id = :rid AND type = :tid","rid={$this->id}&tid=F")->fetch(true);
    }
    
    public function audioAttachment()
    {
        return (new InteractionAttachment())->find("interaction_id = :rid AND type = :tid","rid={$this->id}&tid=A")->fetch(true);
    }

}