<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;

class MapFamdivisao extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("consinco.map_famdivisao",[]);        
    }

    public function mapTributacaoUF() {
        $mapTributacaoUF = (new MapTributacaoUf())->find("nrotributacao = $this->nrotributacao and trib.ufempresa = 'BA' and trib.ufclientefornec = 'BA' and trib.tiptributacao = 'SN' and trib.nroregtributacao = 0")->fetch();
        return $mapTributacaoUF;
    }

}
