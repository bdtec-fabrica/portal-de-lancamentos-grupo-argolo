<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use NFePHP\NFe\ToolsNFe;

class NfSefaz extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfe_sefaz",["store_id","access_key", "nfe_number"],"id", false);        
    }

    public function store() {
        $store = (new Store())->find("code = :code","code=$this->store_id")->fetch();
        return $store;
    }

    public function getDueDate() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $dup = $xml->NFe->infNFe->cobr->dup;
        return $dup;
        
    }

    public function getFornecedor() {
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $xNome = $xml->NFe->infNFe->emit->xNome;
            if ($xml->NFe->infNFe->emit->CRT == 1) {
                $xNome = "(Simples Nacional)".$xNome;
            }
            return $xNome;
        } else {
            return '';
        }
        
    }
    
    public function getSimples() {
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');

            if ($xml->NFe->infNFe->emit->CRT == 1) {
               return true;
            } else {
                return false;
            }
            
        } else {
            return false;
        }
    }

    public function fornecedor() {
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $emit = $xml->NFe->infNFe->emit;
            if ( simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml')) {
                return $emit;
            } else {
                return false;
            }
            return $emit;
        } else {
            return false;
        }
        
    }

    public function cnpj() {
        return (new Store())->find("code = :code","code=$this->store_id")->fetch()->cnpj;
    }

    public function autorized() {
        $ultNSU = $this->nsu;
        $loja = $this->store_id;
        $nfe = new ToolsNFe(__DIR__."/../StoreConfig/$this->store_id.json");
        $nfe->setModelo('55');
        $verification = 0;
        $salvaBanco = 0;
        $baixadaPasta = 0;
        $xJust = 'bispo&dantas';
        $tpEvento = '210210'; //ciencia da operação
        $aManifestacao = array();
         
        $manifestacao = $nfe->sefazManifesta($this->access_key, 1, $xJust, $tpEvento, $aManifestacao); //Reaizar a manifestação.
        if ($aManifestacao['evento'][0]['xMotivo'] == "Rejeicao: Duplicidade de evento") {
            $manifestacao = $nfe->sefazManifesta($this->access_key, 1, $xJust, "210200", $aManifestacao); //Reaizar a manifestação.
        }   
        return $aManifestacao['evento'][0]['xMotivo'];

        if ($this->autorized() == 'h') {
            $ultNSU = $this->nsu;
            $loja = $this->store_id;
            $nfe = new ToolsNFe(__DIR__."/../../StoreConfig/ $this->store_id.json");
            return $nfe;
            // $nfe->setModelo('55');
            // $verification = 0;
            // $salvaBanco = 0;
            // $baixadaPasta = 0;
            // $xml = $nfe->sefazDistDFe('AN', '1', $this->cnpj, '0', $ultNSU, $aResposta);        
            // return $xml;
                // if($aResposta['cStat']=='138'){//documento localizado                    
                //     if($aResposta['aDoc'][0]['schema']=='resNFe_v1.00.xsd'){ // Não tem xml para baixar. Manifestar para baixar posteriormente.
                //         $chave = (simplexml_load_string($aResposta['aDoc'][0]['doc'])->chNFe);
                //         $numeroNFE = substr($chave,25,9);
                //         $nProt = (simplexml_load_string($aResposta['aDoc'][0]['doc'])->nProt);
                //         $dataEmissao = (simplexml_load_string($aResposta['aDoc'][0]['doc'])->dhEmi);
                //         $cnpjEmit = (simplexml_load_string($aResposta['aDoc'][0]['doc'])->CNPJ);
                //         $xJust = 'bispo&dantas';
                //         $tpEvento = '210210'; //ciencia da operação
                //         $aManifestacao = array();
                        
                //         $manifestacao = $nfe->sefazManifesta($chave, 1, $xJust, $tpEvento, $aManifestacao); //Reaizar a manifestação.
    
                //         $row = (new NfSefaz())->find("access_key = :access_key","access_key=$chave")->count();
                //         if($row == 0){
                //             $nfeSefaz = (new NFSefaz());
                //             $nfeSefaz->access_key = $chave;
                //             $nfeSefaz->nfe_number = $numeroNFE;
                //             $nfeSefaz->protocol = $nProt;
                //             $nfeSefaz->issuer_cnpj-> $cnpjEmit;
                //             $nfeSefaz->nsu = $ultNSU;
                //             $nfeSefaz->manifestation = '';
                //             $nfeSefaz->store_id = $loja;
                //             $nfeSefaz->emission_date = $dataEmissao;
                //             $nfeSefazId = $nfeSefaz->save();
                //             if ($nfeSefazId) {
                //                 $salvaBanco++;
                //                 $storeCode = (new Store())->findById($store->id);
                //                 $storeCode->nsu = $ultNSU;
                //                 $storeCodeId = $storeCode->save();
                //                 $ultNSU++;
                //             }
                //         } else {
                //             $storeCode = (new Store())->findById($store->id);
                //             $storeCode->nsu = $ultNSU;
                //             $storeCodeId = $storeCode->save();
                //             $ultNSU++;
                //         }
                //     } else if($aResposta['aDoc'][0]['schema']=='procNFe_v4.00.xsd'){ // tem xml pra baixar ...
                //         $xml = simplexml_load_string($aResposta['aDoc'][0]['doc']);
                //         $cnpj = $xml->NFe->infNFe->emit->CNPJ;
                //         $company_name = $xml->NFe->infNFe->emit->xNome;
                //         $fantasy_name = $xml->NFe->infNFe->emit->xFant;
                //         $state_registration = $xml->NFe->infNFe->emit->IE;
                //         $public_place = $xml->NFe->infNFe->emit->enderEmit->xLgr;
                //         $number = $xml->NFe->infNFe->emit->enderEmit->nro;
                //         $district = $xml->NFe->infNFe->emit->enderEmit->xBairro;
                //         $county_code = $xml->NFe->infNFe->emit->enderEmit->cMun;
                //         $county = $xml->NFe->infNFe->emit->enderEmit->xMun;
                //         $uf = $xml->NFe->infNFe->emit->enderEmit->UF;
                //         $zip_code = $xml->NFe->infNFe->emit->enderEmit->CEP;
                //         $country_code = $xml->NFe->infNFe->emit->enderEmit->cPais;
                //         $country = $xml->NFe->infNFe->emit->enderEmit->xPais;
                //         $phone = $xml->NFe->infNFe->emit->enderEmit->fone;
                //         $crt = $xml->NFe->infNFe->emit->CRT;

                //         $supplierRegistration = new SupplierRegistration();
                //         $productRegistration = new ProductRegistration();
                        
                //         $countSupplierRegistration = $supplierRegistration->find("cnpj = :cnpj", "cnpj=$cnpj")->count();
                //         if ($countSupplierRegistration == 0) {
                //             $supplierRegistrationAdd = new SupplierRegistration();
                //             $supplierRegistrationAdd->cnpj = $cnpj;
                //             $supplierRegistrationAdd->company_name = $company_name;
                //             $supplierRegistrationAdd->fantasy_name = $fantasy_name;
                //             $supplierRegistrationAdd->state_registration = $state_registration;
                //             $supplierRegistrationAdd->public_place = $public_place;
                //             $supplierRegistrationAdd->number = $number;
                //             $supplierRegistrationAdd->district = $district;
                //             $supplierRegistrationAdd->county_code = $county_code;
                //             $supplierRegistrationAdd->county = $county;
                //             $supplierRegistrationAdd->uf = $uf;
                //             $supplierRegistrationAdd->zip_code = $zip_code;
                //             $supplierRegistrationAdd->country_code = $country_code;
                //             $supplierRegistrationAdd->country = $country;
                //             $supplierRegistrationAdd->phone = $phone;
                //             $supplierRegistrationAdd->crt = $crt;
                //             $supplierRegistrationAddSave = $supplierRegistrationAdd->save();

                //         } else {
                //             $supplierRegistrationAdd = (new SupplierRegistration())->find("cnpj = :cnpj", "cnpj=$cnpj")->fetch();
                //         }
                //         foreach($xml->NFe->infNFe->det as $prod){
                            
                //             $productRegistration->store_id = $store->code;
                //             $productRegistration->reference = $prod->prod->cProd;
                //             $productRegistration->ean = $prod->prod->cEAN;
                //             $productRegistration->description = $prod->prod->xProd;
                //             $productRegistration->ncm = $prod->prod->NCM;
                //             $productRegistration->cest = $prod->prod->CEST;
                //             $productRegistration->packing = $prod->prod->uCom;
                //             $productRegistration->cost = $prod->prod->vUnCom;
                //             $productRegistration->supplier_id = $supplierRegistrationAdd->id;
                //             $productRegistrationId = $productRegistration->save();
                //         }
                        


                //         $chave = $xml->NFe->infNFe['Id'][0];
                //         $dhEmi = $xml->NFe->infNFe->ide->dhEmi;
                //         if(strlen($chave)==47){                    
                //             $xml1 = $xml->NFe->infNFe;
                //             foreach($xml1->emit as $xml1){
                //                 $CNPJ = $xml1->CNPJ;
                //             }
                //             $parametroRow = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
                //             #FAZER O TROCA EMBALAGEM AQUI
                //             if($parametroRow > 0){
                //                 $exchangePackagings = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                //                 foreach ($exchangePackagings as $assoc) {
                //                     foreach( $xml1->NFe->infNFe->det as $xml){
                //                         if($xml->prod->cProd == str_replace('-','',$assoc->code) &&  $xml->prod->uCom == $assoc->packing_of){
                //                             /*
                //                             * Embalagem
                //                             */
                //                             $xml->prod->uCom = $assoc->packing_for;
                //                             $xml->prod->uTrib = $assoc->packing_for;
                //                             /*
                //                              * Custo unitario
                //                              */
                //                             $xml->prod->vUnCom = floatval((floatval($xml->prod->vUnCom)*floatval($xml->prod->qCom))/$assoc->packing_for);
                //                             $xml->prod->vUnTrib = floatval((floatval($xml->prod->vUnTrib)*floatval($xml->prod->qTrib))/$assoc->packing_for);
                //                             /*
                //                              * Quantidade
                //                              */
                //                             $xml->prod->qCom = $xml->prod->qCom/$assoc->packing_for;
                //                             $xml->prod->qTrib = $xml->prod->qTrib/$assoc->packing_for;
                //                             $xml1->asXML('/../XML/'.$loja.'/'.$chave.'.xml');
                //                         }
                //                     }
                //                 }
                //             }
                //             $checkRow = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
                //             #FAZER O TROCA CODIGO AQUI
                //             if($checkRow > 0){
                //                 $exchangeCode = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                //                 foreach($exchangeCode as $assoc) {
                //                     foreach( $xml1->det as $xml1){
                //                         if($xml1->prod->cProd == str_replace('-','',$assoc->source_code)){
                //                             $verificacao = true;
                //                             $xml1->prod->cEAN = $assoc->destination_code;
                //                             $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                //                         }
                //                     }
                //                 }
                //                 $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                //             }else{
                //                 $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                //             }            
                //         }
                //         $baixadaPasta ++;
                //         $storeCode = (new Store())->findById($store->id);
                //         $storeCode->nsu = $ultNSU;
                //         $storeCodeId = $storeCode->save();
                //         $ultNSU++;
                //     } else if($aResposta['aDoc'][0]['schema']=='resEvento_v1.00.xsd'){ //evento
                //         $storeCode = (new Store())->findById($store->id);
                //         $storeCode->nsu = $ultNSU;
                //         $storeCodeId = $storeCode->save();
                //         $ultNSU++;
                //     } else if($aResposta['aDoc'][0]['schema']=='procEventoNFe_v1.00.xsd'){ //evento
                //         $storeCode = (new Store())->findById($store->id);
                //         $storeCode->nsu = $ultNSU;
                //         $storeCodeId = $storeCode->save();
                //         $ultNSU++;
                //     }
                // } else if($aResposta['cStat']=='589'){ // Maximo NSU encontrado. Salvar o ultimo Nsu e sair.
                //     $storeCode = (new Store())->findById($this->store_id);
                //     $storeCode->nsu = $ultNSU;
                //     $storeCodeId = $storeCode->save();
                //     $verification = 1;
                // } else if($aResposta['cStat']=='137'){// Nenhum arquivo localizado par ao NSU. Gravar o NSU somar                   
                //         $storeCode = (new Store())->findById($this->store_id);
                //         $storeCode->nsu = $ultNSU;
                //         $storeCodeId = $storeCode->save();
                //         $ultNSU++;
                // }
    
        }     
    }

    public function getUF() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $xNome = $xml->NFe->infNFe->emit->enderEmit->UF;
        return $xNome;
    }

    public function getItems() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $det = $xml->NFe->infNFe->det;
        return $det;        
    }

    public function natOp() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $xNome = $xml->NFe->infNFe->ide->natOp;
        return $xNome;
    }

    public function valorContabil() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $xNome = $xml->NFe->infNFe->total->ICMSTot->vNF;
        return $xNome;
    }

    public function getManifestation() {
        switch ($this->manifestation) {
            case 210200:
                return 'Confirmação da Operação';
                break;
            case 210210:
                return 'Ciência da Operação';
                break;
            case 210220:
                return 'Desconhecimento da Operação';
                break;
            case 210240:
                return 'Operação não Realizada';
                break;
        }
    }

    public function getEntradaSaida() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $det = $xml->NFe->infNFe->det;
        foreach ($det as $d) {
            $CFOP = $d->prod->CFOP;
        }

        if (substr($CFOP,0,1) == 1 or substr($CFOP,0,1) == 2 or substr($CFOP,0,1) == 3) {
            return 'E';
        } else {
            return 'S';
        }
    }
    
    public function referenciada() {
        if ($this->getEntradaSaida() == 'E') {
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $xNome = $xml->NFe->infNFe->ide->NFref->refNFe;
            return $xNome;
        }
    }

    public function retorno() {
        if ($this->getEntradaSaida() == 'E') {
            if ($this->referenciada) {
                $nfeSefaz = (new NfSefaz())->find("access_key = $this->referenciada")->fetch();
                return "Nota de entrada: ".$nfeSefaz->access_key;
            }
        }
    }

    public function getStatus(){
        if ($this->store_id == 710 || $this->store_id == 450 || $this->store_id == 531 || $this->store_id == 396 || $this->store_id == 299){
            $nfReceipt = (new NfReceipt2())->find("access_key = :access_key","access_key={$this->access_key}")->count();
        } else {
            $nfReceipt = (new NfReceipt())->find("access_key = :access_key and status_id != :status_id","status_id=7&access_key={$this->access_key}")->count();
        }
        
        if ($nfReceipt == 0) {
            return 1;
        } else {
            return 2;
        }

    }

    public function getAmountItems() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $dets = $xml->NFe->infNFe->det;
        $count = 0;
        foreach ($dets as $det) {
            $count++;
        } 
        return $count;
    }

    public function getAmountItemsId($data) {
        $requests = (new NfSefaz())->find("id in ($data)")->fetch(true);
        foreach($requests as $request){
            $count += $request->getAmountItems;
        }
        return $count;
    }    

    public function getAA1CFISC() {
        $store = (new Store())->find("code = $this->store_id")->fetch();
        switch ($store->group_id) {
            case 1:
                $aa1cfisc = (new AA1CFISC())->find("FIS_NRO_NOTA = $this->nfe_number and FIS_LOJ_DST = $this->store_id")->fetch(true);

                foreach ($aa1cfisc as $nR) {
        
                    if (strlen($this->access_key) == 44 ) {
                        if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                            $nfReceiptReturn = $nR;
                        }
                    } 
                    
                }
                break;
            case 2:
                $aa1cfisc = (new AA1CFISC2())->find("FIS_NRO_NOTA = $this->nfe_number and FIS_LOJ_DST = $this->store_id")->fetch(true);

                foreach ($aa1cfisc as $nR) {
                    if (strlen($this->access_key) == 44 ) {
                        if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                            $nfReceiptReturn = $nR;
                        }
                    } 
            
                }
                break;
            case 8:
                $aa1cfisc = (new AA1CFISC3())->find("FIS_NRO_NOTA = $this->nfe_number and FIS_LOJ_DST = ".substr($this->store_id,0,strlen($this->store_id)-1)."")->fetch(true);

                foreach ($aa1cfisc as $nR) {
                    if (strlen($this->access_key) == 44 ) {
                        if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                            $nfReceiptReturn = $nR;
                        }
                    } 
            
                }
                break;
        }
        
        if (!$nfReceiptReturn) {
            return false;
        } else {
            return true;
        }
    }

    public function getAG1IENSA() {
        $ag1iensa = (new AG1IENSA())->find("ESCHC_NRO_NOTA3 = $this->nfe_number and ESCLC_CODIGO = $this->store_id")->fetch(true);
        foreach ($ag1iensa as $nR) {
            if (substr($this->access_key,6,14) == $nR->razaoDestino()->TIP_CGC_CPF) {
                return true;
            }            
        }
    }

    public function getPostedOrNot() {
        $nfReceipt = (new NfReceipt())->find("access_key = :access_key and status_id != :status_id","status_id=7&access_key=$this->access_key")->count();
        if ($nfReceipt == 0 ){
            return false;
        } else {
            return true;
        }
    }

    public function getLogManifestation(){
        $log = (new LogManifestation())->find("nf_id = $this->id")->fetch(true);
        return $log;
    }

    public function addNfreceipt($note, $user, $type, $operation, $status, $vencimentos) {
        $class = (new User())->findById($user)->class;
    
        if ($this->getEntradaSaida == 'E') {
            $data['value'] = false;
            $data['msg'] = "Nota Fiscal de Entrada não permitida para postagem!";
        }
    
        // definindo o type 
    
        foreach ($this->getItems() as $item) {
            if ($item->prod->CFOP == 5910 ||$item->prod->CFOP == 6910 ) {
                $bonificacao = true;
            } else if ($item->prod->CFOP == 5124 or $item->prod->CFOP == 5902 or $item->prod->CFOP == 5908 or $item->prod->CFOP == 5949 or $item->prod->CFOP == 6949) {
                $outros = true;
            } else {
                $compra = true;
            }
        }
        if ($compra === true) {
            $type = 2;
        } else if ($bonificacao === true) {
            $type = 3;
        } else if ($outros === true && $type != 10) {
            $type = 7;
        }
    
        $xml=simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
    
        $cnpj=(string)$xml->NFe->infNFe->emit->CNPJ;
        $totalNota=$xml->NFe->infNFe->total->ICMSTot->vNF;
    
        $codigos=$xml->NFe->infNFe->det;
    
        //troca codigo realizado aqui
    
        foreach ($codigos as $codigo) {
            $code=$codigo->prod->cProd;
    
            $count = (new ExchangeCode());
            $count = $count->find("store_id = :store and cnpj = :cnpj and source_code = :code", "store=$this->store_id&cnpj=$cnpj&code=$code")->count();
    
            if ($count > 0) {
                $exchangeCode=(new ExchangeCode())->find("store_id = :store and cnpj = :cnpj and source_code = :code", "store=$this->store_id&cnpj=$cnpj&code=$code")->fetch();
                $codigo->prod->cEAN=str_replace('-', '', $exchangeCode->destination_code);
                $codigo->prod->cEANTrib=str_replace('-', '', $exchangeCode->destination_code);
                $xml->asXML(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
    
                if ($operation == 2) {
                    $operation = 10;
                } else {
                    $operation = 9;
                }
            }
    
        }
    
        if ($type == 4) {
            $operation = 7;
        }
    
        if ($note=='') {
            $note = $this->nfe_number;
        }
    
        if ($cnpj=='10838648000195'|| $cnpj=='10838648000519'|| $cnpj=='10838648000357') {
            $operation = 7;
            $type=4;
        }
    
        if($class == "C2") {
            $status = 10;
        } else {
            $status = 1;
        }
        $parameterProvider = (new ParameterProvider())->find("cnpj = '$cnpj'")->fetch(true);
        if (!empty($parameterProvider)) {
            foreach ($parameterProvider as $provider) {
                if ($provider->parameter_id == 4) {
                    $type = $provider->content;
                } else if ($provider->parameter_id == 5) {
                    $operation = $provider->content;
                }
            }
        }
        $store = (new Store())->find("code = $this->store_id")->fetch();
        $nfReceipt = new NfReceipt();
        $nfReceipt->store_id = $this->store_id;
        $nfReceipt->group_id = $store->group_id;
        $nfReceipt->nf_number = $this->nfe_number;
        $nfReceipt->access_key = $this->access_key;
        $nfReceipt->status_id = $status;
        $nfReceipt->note = nl2br($note);
        $nfReceipt->client_id = $user;
        $nfReceipt->operator_id = 147;
        $nfReceipt->items_quantity = $this->getAmountItems();
        $nfReceipt->type_id = $type;
        $nfReceipt->operation_id = $operation;
        $nfReceipt->inclusion_type = 2;
        $nfReceiptId = $nfReceipt->save();
    
        if ($nfReceiptId === true) {
            if ($type != 3 && $type != 10) {
    
                // Outros sem Vencimento e Bonificação não grava o vencimento.
                if ( !empty($vencimentos)) {
                    foreach ($vencimentos as $vencimento) {
                        if (strlen($vencimento) == 10) {
                            $dueDate = new NfDueDate();
                            $dueDate->nf_id = $nfReceipt->id;
                            $dueDate->due_date = $vencimento;
                            $dueDateId = $dueDate->save();
                            $ok = true;
                        }
                    }
                    if ($ok === true) {
                        $data['value'] = true;
                        $data['msg'] = "$nfReceipt->id";
                    } else {
                        $nfReceipt->destroy();
                        $data['value'] = false;
                        $data['msg'] = "Nenhum vencimento valido!";
                    }
    
                    
                } else {
                    $nfReceipt->destroy();
                    $data['value'] = false;
                    $data['msg'] = "Necessario informa o vencimento!";
                }
            } else {
                $data['value'] = true;
                $data['msg'] = "$nfReceipt->id";
            }
        } else {
            if ($nfReceipt->fail->errorInfo[0]==23000) {
                $data['value'] = false;
                $data['msg'] = "Nota Fiscal já postada!";
            }
        }
    
        return $data;
    }

    public function group_id() {
        $store = (new Store())->find("code = $this->store_id")->fetch();
        return $store->group_id;
    }

    public function registerActivity() {
        $nfReceiptError = $this;
        $cnpj = (string) $nfReceiptError->fornecedor()->CNPJ;
        if ($nfReceiptError->group_id == 1 or $nfReceiptError->group_id  == 10) {
            //percorra os itens da nota
            foreach ($nfReceiptError->getItems() as $det) {   
                $cProd = ltrim($det->prod->cProd, '0');                    
                //se o produto tiver ean "SEM GTIN" ou codigo ean invalido (menor que 8 digitos) entre                                                
                if($det->prod->cEAN == "SEM GTIN" || strlen($det->prod->cEAN) < 8){
                        $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF =  $cnpj")->fetch();
                        //se o fornecedor existir entre
                        if ($aa2ctipo) {
                            
                            $aa3citem = (new AA3CITEM())->find("GIT_COD_FOR = $aa2ctipo->TIP_CODIGO and GIT_REFERENCIA like '%$cProd%'")->fetch();
                            if (!$aa3citem) {
                                $aa1forit = (new AA1FORIT())->find("FORITE_REFERENCIA like '%$cProd%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
                                //se não houver aa1forit entre
                                if (!$aa1forit) {
                                    $aa1refit = (new AA1REFIT())->find("REF_REFERENCIA like '%$cProd%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                    if (!$aa1refit) {
                                        $exchangeCode = (new ExchangeCode())->find("store_id = $nfReceiptError->store_id and cnpj = $cnpj and source_code = ".$cProd)->fetch();
                                        if (!$exchangeCode){
                                            $boningOx = (new BoningOx())->find("reference = '".$cProd."' and cnpj = $cnpj")->count();
                                            if ($boningOx == 0) {
                                                $description .=  "Fornecedor codigo $aa2ctipo->TIP_CODIGO cnpj ".$cnpj." Produto referencia ".$cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
                                            }                                                                     
                                        } else {
                                            if ($nfReceiptError->group_id == 1) {
                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                $output = shell_exec(__DIR__."/../importacao.bat");
                                            } else if ($nfReceiptError->group_id == 2) {
                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                $output = shell_exec(__DIR__."/../importacao2.bat");
                                            }    
                                        }
                                    // senão jogue o xml na pasta
                                    } else {
                                        if ($nfReceiptError->group_id == 1) {
                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                            $output = shell_exec(__DIR__."/../importacao.bat");
                                        } else if ($nfReceiptError->group_id == 2) {
                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                            $output = shell_exec(__DIR__."/../importacao2.bat");
                                        }
                                    } 
                                // senão jogue o xml na pasta                                                         
                                } else {
                                    if ($nfReceiptError->group_id == 1) {
                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                        $output = shell_exec(__DIR__."/../importacao.bat");
                                    } else if ($nfReceiptError->group_id == 2) {
                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                        $output = shell_exec(__DIR__."/../importacao2.bat");
                                    }
                                }
                            }
                        // se o fornecedor não existir entre
                        } else {
                            $description =  " Fornecedor ".$cnpj." sem Cadastro.<br />";
                        }
                    //senão (se tiver EAN valido)
                } else {
                    $cnpj = (string) $nfReceiptError->fornecedor()->CNPJ;
                    $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF =  ".$cnpj)->fetch();
                    // se o fornecedor existir entre
                    if ($aa2ctipo) {
                        $aa3ccean = (new AA3CCEAN())->find("EAN_COD_EAN = ".$det->prod->cEAN)->fetch();
                        //se não tiver o EAN cadastrado na AA3CCEAN entre
                        if (!$aa3ccean) {
                                $aa3citem = (new AA3CITEM())->find("GIT_COD_FOR = $aa2ctipo->TIP_CODIGO and GIT_REFERENCIA like '%$cProd%'")->fetch();

                                if (!$aa3citem) {
                                    $aa1forit = (new AA1FORIT())->find("FORITE_REFERENCIA like '%$cProd%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
                                    //se não houver aa1forit entre
                                    if (!$aa1forit) {
                                        $aa1refit = (new AA1REFIT())->find("REF_REFERENCIA like '%$cProd%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
                                        // se não houver aa1refit entreentre
                                        if (!$aa1refit) {
                                            $exchangeCode = (new ExchangeCode())->find("1=0")->fetch();
                                            //se não houver troca codigo entre
                                            if (!$exchangeCode){
                                                //adicione uma linha a produtos sem cadastro (description);
                                                $cProd = (string) $det->prod->cProd;

                                                $boningOx = (new BoningOx())->find("reference = '".$cProd."' and cnpj = $cnpj")->count();
                                                if ($boningOx == 0) {
                                                    $description .=  "*Fornecedor  codigo $aa2ctipo->TIP_CODIGO cnpj ".$cnpj." Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
                                                } 
                                                // senão jogue o xml na pasta
                                            } else {
                                                if ($nfReceiptError->group_id == 1) {
                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                    $output = shell_exec(__DIR__."/../importacao.bat");
                                                } else if ($nfReceiptError->group_id == 2) {
                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                    $output = shell_exec(__DIR__."/../importacao2.bat");
                                                }
                                            }
                                        // senão jogue o xml na pasta
                                        } else {
                                            if ($nfReceiptError->group_id == 1) {
                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                $output = shell_exec(__DIR__."/../importacao.bat");
                                            } else if ($nfReceiptError->group_id == 2) {
                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                $output = shell_exec(__DIR__."/../importacao2.bat");
                                            }
                                        } 
                                    // senão jogue o xml na pasta                                                         
                                    } else {
                                        if ($nfReceiptError->group_id == 1) {
                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                            $output = shell_exec(__DIR__."/../importacao.bat");
                                        } else if ($nfReceiptError->group_id == 2) {
                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                            $output = shell_exec(__DIR__."/../importacao2.bat");
                                        }
                                    }
                                } else {
                                    if ($nfReceiptError->group_id == 1) {
                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                        $output = shell_exec(__DIR__."/../importacao.bat");
                                    } else if ($nfReceiptError->group_id == 2) {
                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                        $output = shell_exec(__DIR__."/../importacao2.bat");
                                    }
                                }
                            // senão jogue o xml na pasta
                        } else {
                            // echo "<pre>";
                            // var_dump($aa3ccean);
                            // echo "</pre>";
                            if ($nfReceiptError->group_id == 1) {
                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                $output = shell_exec(__DIR__."/../importacao.bat");
                            } else if ($nfReceiptError->group_id == 2) {
                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                $output = shell_exec(__DIR__."/../importacao2.bat");
                            }
                        }
                        // se o fornecedor não existir entre
                        } else {
                            $description .=  " Fornecedor ".$cnpj." sem Cadastro.<br />";
                        }
                }
            }
        } 
        // else if ($nfReceiptError->group_id == 2) {
        //     foreach ($nfReceiptError->getItems() as $det) {
        //         if ( $nfReceiptError->access_key == '29220212275487000102550020000918461002449238') {                                                
        //             if($det->prod->cEAN == "SEM GTIN" || strlen($det->prod->cEAN) < 8){
        //                 $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF =  $cnpj")->fetch();
        //                 if ($aa2ctipo) {
        //                     $aa1forit = (new AA1FORIT2())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
        //                     if (!$aa1forit) {
        //                         $aa1refit = (new AA1REFIT2())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
        //                         if (!$aa1refit) {
        //                             $exchangeCode = (new ExchangeCode())->find("store_id = $nfReceiptError->store_id and cnpj = $cnpj and source_code = ".$det->prod->cProd)->fetch();
        //                             if (!$exchangeCode){
        //                                 $description .=  "Fornecedor codigo $aa2ctipo->TIP_CODIGO cnpj  Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
        //                             } else {
        //                                 if ($nfReceiptError->group_id == 1) {
        //                                     copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
        //                                     $output = shell_exec(__DIR__."/../importacao.bat");
        //                                 } else if ($nfReceiptError->group_id == 2) {
        //                                     copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
        //                                     $output = shell_exec(__DIR__."/../importacao2.bat");
        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }
        //             } else {
        //                 $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF =  $cnpj")->fetch();
        //                 if ($aa2ctipo) {
        //                     $aa3ccean = (new AA3CCEAN2())->find("EAN_COD_EAN = ".$det->prod->cEAN)->fetch();
        //                     if (!$aa3ccean) {
        //                         $aa1forit = (new AA1FORIT2())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
        //                         if (!$aa1forit) {
        //                             $aa1refit = (new AA1REFIT2())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
        //                             if (!$aa1refit) {
        //                                 $exchangeCode = $exchangeCode = (new ExchangeCode())->find("1=0")->fetch();
        //                                 if (!$exchangeCode){
        //                                     $description .=  "Fornecedor  codigo $aa2ctipo->TIP_CODIGO cnpj ".$cnpj." Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
        //                                 } else {
        //                                     if ($nfReceiptError->group_id == 1) {
        //                                         copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
        //                                         $output = shell_exec(__DIR__."/../importacao.bat");
        //                                     } else if ($nfReceiptError->group_id == 2) {
        //                                         copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
        //                                         $output = shell_exec(__DIR__."/../importacao2.bat");
        //                                     }
        //                                 }
        //                             }                                                            
        //                         }
        //                     } else {
        //                         if ($nfReceiptError->group_id == 1) {
        //                             copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
        //                             $output = shell_exec(__DIR__."/../importacao.bat");
        //                         } else if ($nfReceiptError->group_id == 2) {
        //                             copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
        //                             $output = shell_exec(__DIR__."/../importacao2.bat");
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
        return $description;

    }

    public function getNfeControle() {

        switch ($this->group_id()) {
            case 1:
                $nfeControle = (new NFE_CONTROLE())->find("ACAO != 'NE' AND CHAVE_ACESSO_NFE = '$this->access_key' AND DESCRICAO_SITUACAO = 'NF Entrada'")->fetch();
                return $nfeControle;
                break;
            case 10:
                $nfeControle = (new NFE_CONTROLE())->find("ACAO != 'NE' AND CHAVE_ACESSO_NFE = '$this->access_key' AND DESCRICAO_SITUACAO = 'NF Entrada'")->fetch();
                return $nfeControle;
                break;
            case 2:
                $nfeControle = (new NFE_CONTROLE2())->find("ACAO != 'NE' AND CHAVE_ACESSO_NFE = '$this->access_key' AND DESCRICAO_SITUACAO = 'NF Entrada'")->fetch();
                return $nfeControle;
                break;
            case 9:
                $nfeControle = (new NFE_CONTROLE2())->find("ACAO != 'NE' AND CHAVE_ACESSO_NFE = '$this->access_key' AND DESCRICAO_SITUACAO = 'NF Entrada'")->fetch();
                return $nfeControle;
                break;
        }

    }

    public function rms():bool
    {
        $nfeSefaz = (new NfSefaz())->findById(528218);
        if (!$nfeSefaz->getNfeControle()->getAa1cfisc()) {
            return false;
        } else {
            return true;
        }
    }
 }