<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class NfAttachment extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_attachment",["nf_id","file_dir"],"id", true);        
    }

    public function getName(){
        if ($this->type == "I") {
            $name = substr($this->file_dir,24);

        } elseif ($this->type == "F") {
            $name = substr($this->file_dir,22);

        }
        return $name;
    }
}