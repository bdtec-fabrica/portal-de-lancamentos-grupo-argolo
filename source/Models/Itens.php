<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer5;

class Itens
{
    public function selectStore($store) {
        switch ($store){
            case 5:
                return (new Itens51());
                exit;
            case 8:
                return (new Itens86());
                exit;
            case 11:
                return (new Itens116());
                exit;
            case 12:
                return (new Itens124());
                exit;
            case 13:
                return (new Itens132());
                exit;
            case 14:
                return (new Itens140());
                exit;
            case 15:
                return (new Itens159());
                exit;
            case 17:
                return (new Itens175());
                exit;
            case 19:
                return (new Itens191());
                exit;
            case 25:
                return (new Itens256());
                exit;
            case 27:
                return (new Itens272());
                exit;
            case 31:
                return (new Itens310());
                exit;
            case 32:
                return (new Itens329());
                exit;
            case 33:
                return (new Itens337());
                exit;
            case 35:
                return (new Itens353());
                exit;
            case 36:
                return (new Itens361());
                exit;
            case 37:
                return (new Itens370());
                exit;
            case 40:
                return (new Itens400());
                exit;
            case 43:
                return (new Itens434());
                exit;
            case 46:
                return (new Itens469());
                exit;
            case 48:
                return (new Itens485());
                exit;
            case 50:
                return (new Itens507());
                exit;
            case 53:
                return (new Itens531());
                exit;
            case 54:
                return (new Itens540());
                exit;
            case 56:
                return (new Itens566());
                exit;
            case 57:
                return (new Itens574());
                exit;
            case 58:
                return (new Itens582());
                exit;
            case 59:
                return (new Itens590());
                exit;
            case 61:
                return (new Itens612());
                exit;
            case 62:
                return (new Itens620());
                exit;
            case 69: //faltando 67
                return (new Itens698());
                exit;
            case 70:
                return (new Itens701());
                exit;
            case 72:
                return (new Itens728());
                exit;
            case 74:
                return (new Itens744());
                exit;
            case 75:
                return (new Itens752());
                exit;
            case 76:
                return (new Itens760());
                exit;
            case 78:
                return (new Itens787());
                exit;
            case 79:
                return (new Itens795());
                exit;
            case 80:
                return (new Itens809());
                exit;
            case 85:
                return (new Itens850());
                exit;           
        }
    }

    public function selectStoreNfce($store) {
        switch ($store){
            case 5:
                return (new Nfce51());
                exit;
            case 8:
                return (new Nfce86());
                exit;
            case 11:
                return (new Nfce116());
                exit;
            case 12:
                return (new Nfce124());
                exit;
            case 13:
                return (new Nfce132());
                exit;
            case 14:
                return (new Nfce140());
                exit;
            case 15:
                return (new Nfce159());
                exit;
            case 17:
                return (new Nfce175());
                exit;
            case 19:
                return (new Nfce191());
                exit;
            case 25:
                return (new Nfce256());
                exit;
            case 27:
                return (new Nfce272());
                exit;
            case 31:
                return (new Nfce310());
                exit;
            case 32:
                return (new Nfce329());
                exit;
            case 33:
                return (new Nfce337());
                exit;
            case 35:
                return (new Nfce353());
                exit;
            case 36:
                return (new Nfce361());
                exit;
            case 37:
                return (new Nfce370());
                exit;
            case 40:
                return (new Nfce400());
                exit;
            case 43:
                return (new Nfce434());
                exit;
            case 46:
                return (new Nfce469());
                exit;
            case 48:
                return (new Nfce485());
                exit;
            case 50:
                return (new Nfce507());
                exit;
            case 53:
                return (new Nfce531());
                exit;
            case 54:
                return (new Nfce540());
                exit;
            case 56:
                return (new Nfce566());
                exit;
            case 57:
                return (new Nfce574());
                exit;
            case 58:
                return (new Nfce582());
                exit;
            case 59:
                return (new Nfce590());
                exit;
            case 61:
                return (new Nfce612());
                exit;
            case 62:
                return (new Nfce620());
                exit;
            case 69: //faltando 67
                return (new Nfce698());
                exit;
            case 70:
                return (new Nfce701());
                exit;
            case 72:
                return (new Nfce728());
                exit;
            case 74:
                return (new Nfce744());
                exit;
            case 75:
                return (new Nfce752());
                exit;
            case 76:
                return (new Nfce760());
                exit;
            case 78:
                return (new Nfce787());
                exit;
            case 79:
                return (new Nfce795());
                exit;
            case 80:
                return (new Nfce809());
                exit;
            case 85:
                return (new Nfce850());
                exit;           
        }
    }
}