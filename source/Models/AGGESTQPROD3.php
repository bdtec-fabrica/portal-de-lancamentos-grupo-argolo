<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer3;

class AGGESTQPROD3 extends DataLayer3
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AGG_ESTQ_PROD",[],"", false);        
    }
}