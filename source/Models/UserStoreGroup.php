<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class UserStoreGroup extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("user_store_group",[],"id", true);        
    }
}