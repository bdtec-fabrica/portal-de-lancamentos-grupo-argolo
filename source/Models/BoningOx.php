<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class BoningOx extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("boning_ox",[],"id", true);        
    }
    
    public function getPercente($cnpj,$store,$reference) {
        $percentages = $this->find("cnpj = :cnpj and store_id = :store_id and reference = :reference","cnpj=$cnpj&store_id=$store&reference=$reference")->fetch(true);
        foreach ($percentages as $percentage) {
            $totalPercentage += $percentage->percentage;
        }
        return $totalPercentage;
    }
    
    public function getTypeName() {
        $boningOxType = (new BoningOxType())->find("code = :code","code=$this->type")->fetch();
        return $boningOxType->name;
    }

    public function getPrice(){
        $date = "1".date("ymd");
        $store = (new Store())->find("code = :code","code=$this->store_id")->fetch();

        
        if ($store->group_id == 1) {
            $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = 'N'","pl=$this->store_id&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
            foreach($aa2cprec as $prec){
                $price = $prec->PRE_PRECO;
            }
            if ($price == 0){
                $price = 0.01;
             }
            return $price;
        } else if ($store->group_id == 2) {
            $aa2cprec = (new AA2CPREC2())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = 'N'","pl=$this->store_id&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
            foreach($aa2cprec as $prec){
                $price = $prec->PRE_PRECO;
            }
            if ($price == 0){
                $price = 0.01;
             }
            return $price;
        } else if($store->group_id == 10) {
            $aa2cprec = (new AA2CPREC10())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = 'N'","pl=$this->store_id&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
            foreach($aa2cprec as $prec){
                $price = $prec->PRE_PRECO;
            }
            if ($price == 0){
                $price = 0.01;
             }
            return $price;
        } else {
            // $store_id = substr($this->store_id,0,strlen($this->store_id)-1);
            // $aa2cprec = (new AA2CPREC3())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = 'N'","pl=$store_id&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
            // foreach($aa2cprec as $prec){
            //     $price = $prec->PRE_PRECO;
            // }
            // if ($price == 0){
            //     $price = 0.01;
            //  }
            return $this->price;
        }
    }
}