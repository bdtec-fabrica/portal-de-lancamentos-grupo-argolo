<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer4;

class AG2ICNTR3 extends DataLayer4
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG2ICNTR",[],"", false);        
    }

    public function getAG1NDINV(){
        $ag1ndinv = (new AG1NDINV3())->find("FI_NUM_INVENT = :fni and FI_LOJA = :fl","fni=$this->CNI_SEQUEN&fl=$this->CNI_LOJA_1")->fetch(true);
        return $ag1ndinv;
    }
}