<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class NCMALIQUOTA extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("ncm_aliquota",[],"id", true);        
    }

    public function aliquotaS($ncm = null) {
        $ncm = $ncm*1;

        $aliquotas = $this->find("ncm = $ncm")->fetch(true);

        if ($aliquotas){
            foreach ($aliquotas as $aliquota){
                return $aliquota->aliquota;
            }
        } else {
            return 18;
        }
    }
}