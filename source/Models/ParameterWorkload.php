<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ParameterWorkload extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("parameter_workload",[],"id", true);        
    }

    public function parameter($user,$parameterWeek,$parameterDayWeek) {
        return $this->find("user_id = $user and parameter_week_id = $parameterWeek and parameter_day_week_id = $parameterDayWeek")->fetch();
    }
    
}