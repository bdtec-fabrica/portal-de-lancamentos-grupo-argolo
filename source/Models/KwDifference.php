<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class KwDifference extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("kw_difference",[],"id", true);        
    }

    public function getAttachment() {
        $attachment = (new KwDifferenceAttachment())->find("kw_difference_id = $this->id")->fetch(true);
        return $attachment;
    }

    public function getOperator() {
        $operator = (new User())->findById($this->operator_id);
        return $operator;
    }

    public function getClient() {
        $operator = (new User())->findById($this->client_id);
        return $operator;
    }
}