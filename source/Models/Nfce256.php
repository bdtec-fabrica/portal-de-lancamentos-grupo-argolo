<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer256;

class Nfce256 extends DataLayer256
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}