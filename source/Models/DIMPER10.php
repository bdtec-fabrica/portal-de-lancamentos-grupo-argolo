<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer10;

class DIMPER10 extends DataLayer10
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("DIM_PER",[],"", false);        
    }

    public function getIdDt($date) {
        return $this->find("DT_COMPL = TO_DATE(:d, 'DD/MM/YYYY')","d=$date","ID_DT")->fetch()->ID_DT;
    }

    

    
}