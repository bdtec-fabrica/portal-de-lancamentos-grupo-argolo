<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer620;

class Nfce620 extends DataLayer620
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}