<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use DateTime;

class Daily extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("daily",[],"id", true);        
    }

    public function getCheckIns(){
        $checkIn = (new CheckIn())->find("daily_id = :id","id=$this->id")->order("created_at ASC")->fetch(true);
        return $checkIn;
    }

    public function getUser() {
        $user = (new User())->find("id = :id","id=$this->user_id")->fetch();
        return $user;
    }

    public function countHours($userId,$Y_m_d) {
        $dailies = (new Daily())->find("substr(created_at,1,10) = :date and user_id = :uId","date=$Y_m_d&uId=$userId")->fetch();
        $count = (new Daily())->find("substr(created_at,1,10) = :date and user_id = :uId","date=$Y_m_d&uId=$userId")->count();
        $i = 1;

        foreach($dailies->getCheckIns as $checkIn) {
            
            if ($i == 1) {
                $first = $checkIn->created_at;
            } else if ($i == 2) {
                $second = $checkIn->created_at;
            } else if ($i == 3) {
                $third = $checkIn->created_at;
            } else if ($i == 4) {
                $fourth = $checkIn->created_at;
            }
            $i++;
            
        }
        if (!isset($second)) {
            $second = date("Y-m-d H:i:s");
            
            $startDate = new DateTime($first);
            $sinceStart = $startDate->diff(new DateTime($second));
        } else {
            $startDate = new DateTime($first);
            $sinceStart = $startDate->diff(new DateTime($second));
        }
        $output = str_pad($sinceStart->h,2,0,STR_PAD_LEFT).':'.str_pad($sinceStart->i,2,0,STR_PAD_LEFT).':'.str_pad($sinceStart->s,2,0,STR_PAD_LEFT);

        if(!isset($third)) {
            return $output;
        } else {
            $startDate2 = new DateTime($third);
            if (!isset($fourth)){
                $fourth = date('Y-m-d H:i:s');
            }
            $sinceStart2 = $startDate2->diff(new DateTime($fourth));

            $output2 = str_pad($sinceStart2->h,2,0,STR_PAD_LEFT).':'.str_pad($sinceStart2->i,2,0,STR_PAD_LEFT).':'.str_pad($sinceStart2->s,2,0,STR_PAD_LEFT);


            list($hora,$minuto, $segundo) = explode(':',$output);
            $calculo = ($hora * 3600) + ($minuto * 60) + $segundo;

            list($hora2,$minuto2, $segundo2) = explode(':',$output2);
            $calculo2 = ($hora2 * 3600) + ($minuto2 * 60) + $segundo2;

            $calculo = $calculo + $calculo2;

            $hour = floor($calculo / 3600);
            $minutes = floor($calculo % 3600 / 60);
            $seconds = floor($calculo % 3600 % 60);

        $total = str_pad($hour,2,'0',STR_PAD_LEFT).':'.str_pad($minutes,2,'0',STR_PAD_LEFT).':'.str_pad($seconds,2,'0',STR_PAD_LEFT);
        return $total;
        }
        
        
        
    }
}