<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AGGFLSPROD extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AGG_FLS_PROD",[],"", false);        
    }

    public function getItemRegister(){
        $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM = :gci","gci=$this->CD_PROD")->fetch();
        return $aa3citem;
    }

    public function getPrice() {
        $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $this->ID_DT and PRE_TIPO = :pt","pt=N&pl=$this->CD_FIL&pci=$this->CD_PROD&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_PRECO;
        }
        return $price;
    }

    public function getDatePrice() {
        $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_TIPO = :pt","pt=N&pl=$this->CD_FIL&pci=$this->CD_PROD&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_DAT_INICIO;
        }
        return substr($price,5,2)."/".substr($price,3,2)."/".substr($price,1,2);
    }

    public function getICMSAliquot(){
        $store = $this->CD_FIL;
        $aa2cloja = (new AA2CLOJA())->find("LOJ_CODIGO = :lc","lc=$store")->fetch();
        $aa2tfisc = (new AA2TFISC())->find("TFIS_ORIGEM = :t1 AND TFIS_FIGURA = :t2 AND TFIS_CODIGO = :t3
        AND TFIS_AUTOMACAO = :t4","t1=BA&t2=".$this->getItemRegister()->GIT_NAT_FISCAL."&t3=512&t4=$aa2cloja->LOJ_AUTONOMIA")->fetch();       

        return $aa2tfisc->TFIS_ALIQ_ICM;
    }

    public function getPISCOFINSAliquot(){
        if ($this->getItemRegister()->GIT_CATEGORIA_ANT == 0){
            return 9.25;
        } else {
            return 0;
        }
        
    }

    public function getMargin($store) {
        if (($this->VL_VDA/$this->QTD_VDA) > 0){
            $margin = 100-(((($this->VL_CMV/$this->QTD_VDA)/($this->VL_VDA/$this->QTD_VDA))*100)+(($this->getPISCOFINSAliquot-($this->getPISCOFINSAliquot*($this->getICMSAliquot($store)/100)))+$this->getICMSAliquot()));
            return $margin;
        } else {
            $margin = 0;
        return round($margin,2);
        }
        
                
    }

    public function aa2cestq() {
        $aa2cestq = (new AA2CESTQ())->find("substr(GET_COD_PRODUTO,0,length(GET_COD_PRODUTO)-1) = :GET_COD_PRODUTO and substr(GET_COD_LOCAL,0,length(GET_COD_LOCAL)-1) = :GET_COD_LOCAL",
                                            "GET_COD_LOCAL=$this->CD_FIL&GET_COD_PRODUTO=$this->CD_PROD")->fetch();
        return $aa2cestq;

    }
    
    public function dimPer() {
        $dimPer = (new DIMPER())->find("ID_DT = :ID_DT","ID_DT=$this->ID_DT")->fetch();
        return $dimPer;
    }

    
}