<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;
use \DateInterval;

class NfReceiptLost extends DataLayer
{

    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nf_receipt_lost",[],"id",false);        
    }


    public function getStore() {
        $store = (new Store());
        $store = $store->find("id = :store_id and group_id = :group_id","store_id={$this->store_id}&group_id={$this->group_id}")->fetch();
        return $store->company_name;
    }

    public function store() {
        $store = (new Store())->find("code = :code","code=$this->store_id")->fetch();
        return $store;
    }

    public function getDueDate() {
        $dueDate = new NfDueDate();
        $dueDate = $dueDate->find("nf_id = :nf_id","nf_id=$this->id")->order("concat('1',substr(`due_date`,3,2),substr(`due_date`,6,2),substr(`due_date`,9,2)) asc")->fetch(true);
        return $dueDate;

    }

    public function getNfeDueDate() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $dup = $xml->NFe->infNFe->cobr->dup;
        return $dup;        
    }

    public function getItems() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $det = $xml->NFe->infNFe->det;
        return $det;
    }

    public function getAmount() {
        $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
        $det = $xml->NFe->infNFe->det;
        if (strlen($this->access_key) == 44) {
            if ($xml) {
                foreach ($det as $d) {
                    $i += 1;
                }
            } else {
                $i = 'Sem XML';
            }
        } else {
            $i = $this->items_quantity;
        }
        
        
        return $i;
    }


    public function getItemsIds($data){
        
        $requests = (new NfReceipt())->find("id in ($data)")->fetch(true);
        foreach($requests as $request){
            $count += $request->getAmountItems;
        }
        return $count;
    }

    public function getAmountItems() {
        if(strlen($this->access_key)==44){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $dets = $xml->NFe->infNFe->det;
            $count = 0;
            foreach ($dets as $det) {
                $count++;
            } return $count;
        } else {
            return $this->items_quantity;
        }
        
    }

    public function getFornecedor() {
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');

            if ($xml->NFe->infNFe->emit->CRT == 1) {
                $xNome = $xml->NFe->infNFe->emit->xNome." (SIMPLES NACIONAL)";
            } else {
                $xNome = $xml->NFe->infNFe->emit->xNome.'';
            }
            return $xNome;
        } else {
            return '';
        }
        
    }

    public function codigoInternoCD() {

        if($this->type_id == 4) {
            foreach($this->getItems() as $det){
                if (strlen($det->prod->cEAN) < 8 || $det->prod->cEAN == 'SEM GTIN') {
                    return "Codigo Interno";
                }
            }
            return "";
        }
    }

    public function getSimplesFabricante() {
        $ok = false;
        if ($this->access_key != ''){
            $xml = simplexml_load_file(__DIR__."/../XML/".$this->store_id.'/NFe'.$this->access_key.'.xml');
            $xNome = $xml->NFe->infNFe->emit->xNome." (Simples Nacional)";
            if ($xml->NFe->infNFe->emit->CRT == 1) {
                
                foreach($xml->NFe->infNFe->det as $det) {
                    if($det->prod->CFOP == 5101) {
                        $ok = true;
                    }
                }
            }
            
        }
        return $ok;
    }

    public function creditoICMS() {
        //oioioi
    }

    public function getGroup() {
        $group = (new Group());
        $group = $store->find("id = :group_id","group_id={$this->group_id}")->fetch();
        return $group->name;
    }

    public function getStatus() {
        $status = (new NfStatus());
        $status = $status->find("id = :status_id","status_id={$this->status_id}")->fetch();
        return $status->name;
    }

    public function getClient() {
        $client = (new User());
        $client = $client->find("id = :client_id","client_id={$this->client_id}")->fetch();
        return $client->name;
    }

    public function getClientClass() {
        $client = (new User());
        $client = $client->find("id = :client_id","client_id={$this->client_id}")->fetch();
        return $client;
    }

    public function getOperator() {
        $operator = (new User());
        $operator = $operator->find("id = :operator_id","operator_id={$this->operator_id}")->fetch();
        return $operator->name;
    }

    public function getOperatorClass() {
        $operator = (new User());
        $operator = $operator->find("id = :operator_id","operator_id={$this->operator_id}")->fetch();
        return $operator;
    }

    public function getType() {
        $type = (new NfType());
        $type = $type->find("id = :type_id","type_id={$this->type_id}")->fetch();
        return $type->name;
    }

    public function getOperation() {
        $operation = (new NfOperation());
        $operation = $operation->find("id = :operation_id","operation_id={$this->operation_id}")->fetch();
        return $operation->name;
    }

    public function getOperationColor() {
        $operation = (new NfOperation());
        $operation = $operation->find("id = :operation_id","operation_id={$this->operation_id}")->fetch();
        return $operation->color;
    }

    public function getInclusion() {
        if ($this->inclusion_type == 0) {
            return "Manual";
        } else {
            return "Painel";
        }
    }

    public function clientGroup()
    {
        $group = (new User())->findById($this->client_id)->store_group_id;
        if ($group) {
            $group = (new StoreGroup())->findById($group);
            return $group->name;
        } else {
            return 0;
        }
        
        
    }

    public function getCreatedAt()
    {
        return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)."<br>".substr($this->created_at,11);
    }

    public function getReport() {
        $nfReport = (new NfReport())->find("id_request = :id_request","id_request=$this->id")->count();
        return $nfReport;
    }

    public function getReportData() {
        $nfReport = (new NfReport())->find("id_request = :id_request","id_request=$this->id")->fetch();
        return $nfReport;
    }

    public function getUpdatedAt()
    {
        return substr($this->updated_at,8,2)."/".substr($this->updated_at,5,2)."/".substr($this->updated_at,0,4)."<br>".substr($this->updated_at,11);
    }

    public function getCreatedAtInt()
    {
        return substr($this->created_at,8,2)."/".substr($this->created_at,5,2)."/".substr($this->created_at,0,4)." ".substr($this->created_at,11);
    }

    public function getAttachment()
    {
        $attachment = (new NfAttachment());
        $attachment = $attachment->find("nf_id = :id","id={$this->id}")->fetch(true);
        return $attachment;
    }

    public function nfAdd($storeId, $nfNumber, $statusId, $clientId, $operatorId, $itemsQuantity, $typeId, $operationId, $inclusioType, $accessKey = null, $note = null) {
        $this->store_id = $storeId;
        $this->nf_number = $nfNumber;
        $this->status_id = 1;
        $this->client_id = $clientId;
        $this->operator_id = $operatorId;
        $this->type_id = $typeId;
        $this->operation_id = $operationId;
        $this->inclusion_type = $inclusioType;
        if ($accessKey) {
            $this->access_key = $accessKey;
        }
        if ($note) {
            $this->note = $note;
        }
        $save = $this->save();
        return $save;
    }

    public function userAdministrator() {
        $user = (new User())->findById($this->client_id);
        return $user;
    }

    public function userOperator() {
        $user = (new User())->findById($this->operator_id);
        return $user->name;
    }
    
    public function getCnpjFornecedor() {
        if ($this->access_key != ''){
            $CNPJ = substr($this->access_key,6,14);
            return $CNPJ;
        } else {
            return '';
        }
        
    }

    public function getNFeSerie() {
        if ($this->access_key != ''){
            $serie = substr($this->access_key,22,3);
            return $serie;
        } else {
            return '';
        }
        
    }

    public function getLogs()
    {
        return (new NfReceiptLog())->find("nf_receipt_id = :rid","rid={$this->id}")->order("id ASC")->fetch(true);
    }

    public function getAmount1(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6)")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1 or $diff->format('%d') > 0) {
                if ($request->operation_id == 1 ){
                    $count++; 
                } else if ($request->operation_id == 4){
                    $count++; 
                } else if ($request->operation_id == 11){
                    $count++; 
                } else if ($request->operation_id == 12){
                    $count++; 
                }
            }
        }
        return $count;
    }

    public function getAmount2(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount4(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount6(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8 && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount8(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1 ) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmount24(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getNf1(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6)")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1 ) {
                if ($request->operation_id == 1 ){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 4){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 11){
                    $count++;
                    $nf[$count] = $request->id;
                }
                if ($request->operation_id == 12){
                    $count++;
                    $nf[$count] = $request->id;
                }
                
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
        $count = implode(",",$nf);
            } else {
        $count = 0;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf2(){   
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
        $count = implode(",",$nf);
                } else {
        $count = 0;
        }
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        
        return $count;
    }

    public function getNf4(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
        $count = implode(",",$nf);
                    } else {
            $count = 0;
            }
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf6(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf8(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1 ) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
                if (is_array($nf)) {
                    $count = implode(",",$nf);
                } else {
                    $count = 0;
                }
            } else {
                $count = $nf;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNf24(){
        $requests = (new NfReceipt())->find("status_id in (1,4,5,6) and operation_id != 1 and operation_id != 4 and operation_id != 11 and operation_id != 12")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getAmountPC0(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') < 1 ) {
                $count++;                    
            }
        }
        return $count;
    }

    public function getAmountPC1(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1 && $diff->format('%d') < 2) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC2(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 2 && $diff->format('%d') < 4) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC4(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 4 && $diff->format('%d') < 7) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC7(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 7 && $diff->format('%d') < 10) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC10(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 10) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC0Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') < 1 ) {
                $count++;                    
            }
        }
        return $count;
    }

    public function getAmountPC1Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1 && $diff->format('%d') < 2) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC2Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 2 && $diff->format('%d') < 4) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC4Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 4 && $diff->format('%d') < 7) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC7Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 7 && $diff->format('%d') < 10) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountPC10Store($store){
        $requests = (new NfReceipt())->find("status_id = :status_id and store_id = :sid","status_id=2&sid=$store")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 10) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getNfPC0(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC1(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1 && $diff->format('%d') < 2) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC2(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 2 && $diff->format('%d') < 4) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC4(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 4 && $diff->format('%d') < 7) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC7(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 7 && $diff->format('%d') < 10) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function getNfPC10(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=2")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->updated_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 10) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if(is_array($nf)){
            if (is_array($nf)) {
                $count = implode(",",$nf);
            } else {
                $count = 0;
            }
        } else {
            $count = $nf;
        }
        return $count;
    }

    public function inRMS(){
        if (strlen($this->access_key) == 44){
            $nfRMS = (new NfRms())->find("access_key = :access_key and store_id = :sid","access_key=$this->access_key&sid=$this->store_id")->count();
        } else {
            $nfRMS = (new NfRms())->find("nf_number = :nf_number and store_id = :store_id","store_id=$this->store_id&nf_number=$this->nf_number")->count();
        }
        return $nfRMS;
    }

    public function getTimeDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }
        $dia = $diff->format("%d");
        $dia = $dia." dia(s), ";
        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%Y-%m-%d %H:%i"));
        $date->sub(new DateInterval($event->getAverageStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $dia.$date->format("H:i:s");
        } else {
            return $date->format('H:i:s');
        }
        
    }

    public function getDaysDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }

        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%d"));
        $date->sub(new DateInterval($event->getAverageDaysStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $date->format("d");
        } else {
            return $date->format('d');
        }
        
    }

    public function getTimesDuration(){
        $event = new NfEvent();
        $dateTime = new DateTime($this->created_at);
        if($this->status_id == 3) {
            $diff = $dateTime->diff(new DateTime($this->updated_at));
        } else {
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
        }
        $dia = $diff->format("%d");
        $dia = $dia." dia(s), ";
        // return $event->getAverageStatusId($this->id,2);
        $date = new DateTime($diff->format("%Y-%m-%d %H:%i"));
        $date->sub(new DateInterval($event->getAverageStatusId($this->id,2)));
        if ($date->format('d') > 0) {
            return $dia.$date->format("H:i:s");
        } else {
            return $date->format('H:i:s');
        }
        
    }

    public function getDuedateAmount() {
        $dueDate = new NfDueDate();
        $dueDate = $dueDate->find("nf_id = :nf_id","nf_id=$this->id")->count();
        return $dueDate;
    }

    public function dueDate() {
        $dueDate = new NfDueDate();
        $dueDate = $dueDate->find("nf_id = :nf_id","nf_id=$this->id")->order("concat('1',substr(`due_date`,3,2),substr(`due_date`,6,2),substr(`due_date`,9,2))")->fetch(true);
        return $dueDate;
    }

    public function getAA1CFISC() {
        
        $aa1cfisc = (new AA1CFISC())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag145 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34&ag145=145")->fetch(true);

        foreach ($aa1cfisc as $nR) {
            if (strlen($this->access_key) == 44 ) {
                if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                    $nfReceiptReturn = $nR;
                }
            } else {
                if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                    $nfReceiptReturn = $nR;
                }
                
            }
            
        }
        if (!$nfReceiptReturn) {
            $aa1cfisc2 = (new AA1CFISC2())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34")->fetch(true);
            foreach ($aa1cfisc2 as $nR) {
                if (strlen($this->access_key) == 44 ) {
                    if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                        $nfReceiptReturn = $nR;
                    }
                } else {
                    if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                        $nfReceiptReturn = $nR;
                    }
                }  
            }
        }
        
        if (!$nfReceiptReturn) {
            return "Não encontrada.";
        } else {
            return "Encontrada.";
        }
        
    }

    public function AA1CFISC() {
        
        $aa1cfisc = (new AA1CFISC())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag145 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34&ag145=145")->fetch(true);
        foreach ($aa1cfisc as $nR) {
            if (strlen($this->access_key) == 44 ) {
                if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                    $nfReceiptReturn = $nR;
                }
            } else {
                if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                    $nfReceiptReturn = $nR;
                }
                
            }
            
        }
        if (!$nfReceiptReturn) {
            $aa1cfisc2 = (new AA1CFISC2())->find("FIS_NRO_NOTA = :nn and FIS_LOJ_DST = :sid and (FIS_OPER = :ag143 or FIS_OPER = :ag7 or FIS_OPER = :ag4 or FIS_OPER = :ag1 or FIS_OPER = :ag3 or FIS_OPER = :ag10 or FIS_OPER = :ag19  or FIS_OPER = :ag11 or FIS_OPER = :ag12 or FIS_OPER = :ag82 or FIS_OPER = :ag9 or FIS_OPER = :ag24 or FIS_OPER = :ag87 or FIS_OPER = :ag16 or FIS_OPER = :ag34 or FIS_OPER = :ag149)","nn=$this->nf_number&sid=$this->store_id&ag1=1&ag16=16&ag3=3&ag10=10&ag11=11&ag12=12&ag82=82&ag9=9&ag24=24&ag87=87&ag4=4&ag7=7&ag143=143&ag19=19&ag149=149&ag34=34")->fetch(true);
            foreach ($aa1cfisc2 as $nR) {
                if (strlen($this->access_key) == 44 ) {
                    if (substr($this->access_key,6,14) == $nR->getProvider()->TIP_CGC_CPF) {
                        $nfReceiptReturn = $nR;
                    }
                } else {
                    if ('1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2) == $nR->FIS_DTA_AGENDA){
                        $nfReceiptReturn = $nR;
                    }
                }  
            }
        }
        
        return $nfReceiptReturn;
        
    }

    public function getUpdatedAtRms() {
        return '1'.substr($this->updated_at,2,2).substr($this->updated_at,5,2).substr($this->updated_at,8,2);
    }
    
    public function getStoreNoDigit() {
        return substr($this->store_id,0,strlen($this->store_id)-1);
    }


    public function updated() {
        // return true;
        if (strlen($this->nf_number)>7) {
            $numero = substr($this->nf_number,strlen($this->nf_number)-7);
        } else {
            $numero = $this->nf_number;
        }
        if ($this->group_id == 1 ||$this->group_id == 10) {
            $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
            $aa1cfisc = (new AA1CFISC())->find("FIS_SITUACAO != '9' and FIS_SERIE = $this->getNFeSerie and FIS_LOJ_ORG = $aa2ctipo->TIP_CODIGO and FIS_LOJ_DST = $this->store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON())->getEntradas3).")")->fetch();
        } else if ($this->group_id == 2) {
            if ($this->store_id == 850) {
                $store_id = 85;
            } else {
                $store_id = $this->store_id;
            }
            $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
            if (strlen($this->access_key) != 44) {
                $aa1cfisc = (new AA1CFISC2())->find("FIS_SITUACAO != '9' and FIS_LOJ_DST = $store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON2())->getEntradas3).")")->order("FIS_DTA_AGENDA ASC");
                
            } else {
                $aa1cfisc = (new AA1CFISC2())->find("FIS_SITUACAO != '9' and FIS_SERIE = $this->getNFeSerie and FIS_LOJ_ORG = $aa2ctipo->TIP_CODIGO and FIS_LOJ_DST = $store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON2())->getEntradas3).")")->fetch();
            }
            
        } else if ($this->group_id == 4 ||$this->group_id == 8) {
            $aa2ctipo = (new AA2CTIPO3())->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
            $store_id = substr($this->store_id,0,strlen($this->store_id)-1);
            $aa1cfisc = (new AA1CFISC3())->find("FIS_SITUACAO != '9' and FIS_SERIE = $this->getNFeSerie and FIS_LOJ_ORG = $aa2ctipo->TIP_CODIGO and FIS_LOJ_DST = $store_id and FIS_NRO_NOTA = $numero and FIS_OPER in (".((new AA1CTCON3())->getEntradas3).")")->fetch();
        } else {
            $aa1cfisc = true;
        }

        if ($aa1cfisc) {
            return $aa1cfisc;
        } else {
            return false;
        }
    }

    public function AG2DETNT() {
        $ag2detnt = (new AG2DETNT())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
        "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        
        $error = false;
        foreach ($ag2detnt as $items) {
            if ($items->getMargin > 50 || $items->getMargin < 0){
                $error = true;
            }
        }
        return $error;
    }

    public function AG2DETNT2() {
        if ($this->group_id == 1 ||$this->group_id == 10){
            
            $ag2detnt = (new AG2DETNT())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
            "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        } else if ($this->group_id == 4 ||$this->group_id == 8){
            $ag2detnt = (new AG2DETNT3())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
            "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        } else if ($this->group_id == 2){
            $ag2detnt = (new AG2DETNT2())->find("substr(REN_DESTINO,1,length(REN_DESTINO)-1) = :store and REN_NOTA = :nn",
            "store=$this->store_id&nn=$this->nf_number")->fetch(true);
        }
        
        
        $error = false;
        foreach ($ag2detnt as $items) {
            if ($items->getMargin > 50 || $items->getMargin < 0){
                $error .= "Codigo: ".$items->getItemRegister()->GIT_COD_ITEM." Descrição: ".$items->getItemRegister()->GIT_DESCRICAO." Margem: ". $items->getMargin.".";
            }
        }
        return $error;
    }

    public function getAmountCC1(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1  && $diff->format('%H') < 2  && $diff->format('%d') < 1){
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC2(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC4(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC6(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC8(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1) {
                $count++;
            }
        }
        return $count;
    }

    public function getAmountCC24(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
            }
        }
        return $count;
    }
    
    public function getRequestCC1(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 1  && $diff->format('%H') < 2  && $diff->format('%d') < 1){
                $count++;
                $nf[$count] = $request->id;
                
            }
        }
        if (is_array($nf)) {
            $count = implode(",",$nf);
        } else {
            $count = 0;
        }
        return $count;
    }

    public function getRequestCC2(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 2 && $diff->format('%H') < 4   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
            $count = implode(",",$nf);
        } else {
            $count = 0;
        }
        return $count;
    }

    public function getRequestCC4(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 4 && $diff->format('%H') < 6   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
        $count = implode(",",$nf);
            }    else {
        $count = 0;
            }   
        return $count;
    }

    public function getRequestCC6(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 6 && $diff->format('%H') < 8   && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
            $count = implode(",",$nf);
        }   else {
        $count = 0;
        }          
        return $count;
    }

    public function getRequestCC8(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%H') >= 8 && $diff->format('%H') < 24  && $diff->format('%d') < 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
        $count = implode(",",$nf);
        } else {
        $count = 0;
            }
        return $count;
    }

    public function getRequestCC24(){
        $requests = (new NfReceipt())->find("status_id = :status_id","status_id=10")->fetch(true);
        $count = 0;
        foreach ($requests as $request){
            $dateTime = new DateTime($request->created_at);
            $diff = $dateTime->diff(new DateTime(date("Y-m-d H:i:s")));
            if ($diff->format('%d') >= 1) {
                $count++;
                $nf[$count] = $request->id;
            }
        }
        if (is_array($nf)) {
            $count = implode(",",$nf);
        } else {
            $count = 0;
        }
        return $count;
    }

    public function nfSefaz() {
        return (new NfSefaz())->find("access_key = :access_key","access_key=$this->access_key")->fetch();
    }

    public function aa2detnt() {
        
        if ($this->store()->group_id == 1 || $this->store()->group_id == 10) {
            $aa1ctcon = new AA1CTCON();
            $agendas = $aa1ctcon->entradasAtualizaCusto();
            $aa2detnt = (new AG2DETNT())->find("REN_TPO_AGENDA in ($agendas) and REN_NOTA = $this->nf_number and substr(REN_DESTINO,0,length(REN_DESTINO)-1) = $this->store_id")->fetch(true);

            return $aa2detnt;
            
        }
    }

    public function diferencaCustoPortal($reference = null) {
        foreach ($this->getItems as $det1) {
            
            if ("'".$det1->prod->cProd."'" == "'$reference'") {
                   
                $custoAtual = $det1->prod->vUnCom;
                
                $nfSefaz = (new NfSefaz())->find("access_key != '$this->access_key'  and substr(access_key,7,14) = ".$this->getCnpjFornecedor()." and store_id = $this->store_id")->order("nfe_number ASC")->fetch(true);
                foreach ($nfSefaz as $nf) {
                    foreach ($nf->getItems as $det) {
                        if ("'".$det->prod->cProd."'" == "'".$det1->prod->cProd."'") {
                            $custoAnterior =  $det->prod->vUnCom;
                        }
                    }
                }
                
            }
           
        }
        
        if ($custoAnterior == '') {
            $valor = 100;
        } else {
            $valor = (($custoAnterior - $custoAtual)/$custoAnterior)*100;
        }
        return $valor;
        
    }

    public function custoAnterior($reference = null) {
        foreach ($this->getItems as $det1) {
            
            if ("'".$det1->prod->cProd."'" == "'$reference'") {
                   
                $custoAtual = $det1->prod->vUnCom;
                
                $nfSefaz = (new NfSefaz())->find("access_key != '$this->access_key'  and substr(access_key,7,14) = ".$this->getCnpjFornecedor()." and store_id = $this->store_id")->order("nfe_number ASC")->fetch(true);
                foreach ($nfSefaz as $nf) {
                    foreach ($nf->getItems as $det) {
                        if ("'".$det->prod->cProd."'" == "'".$det1->prod->cProd."'") {
                            $custoAnterior =  $det->prod->vUnCom;
                        }
                    }
                }
                
            }
           
        }
        
        if ($custoAnterior == '') {
            $custoAnterior = 0;
            $valor = 100;
        } else {
            $valor = (($custoAnterior - $custoAtual)/$custoAnterior)*100;
        }
        return $custoAnterior;
        
    }

    public function notaAnterior($reference = null) {
        foreach ($this->getItems as $det1) {
            
            if ("'".$det1->prod->cProd."'" == "'$reference'") {
                   
                $custoAtual = $det1->prod->vUnCom;
                
                $nfSefaz = (new NfSefaz())->find("access_key != '$this->access_key'  and substr(access_key,7,14) = ".$this->getCnpjFornecedor()." and store_id = $this->store_id")->order("nfe_number ASC")->fetch(true);
                foreach ($nfSefaz as $nf) {
                    foreach ($nf->getItems as $det) {
                        if ("'".$det->prod->cProd."'" == "'".$det1->prod->cProd."'") {
                            $custoAnterior =  $det->prod->vUnCom;
                            $nota = $nf->nfe_number;
                        }
                    }
                }
                
            }
           
        }
        
        if ($custoAnterior == '') {
            $custoAnterior = 0;
            $valor = 100;
        } else {
            $valor = (($custoAnterior - $custoAtual)/$custoAnterior)*100;
        }
        return $nota;
        
    }

    public function custoAtual($reference = null) {
        foreach ($this->getItems as $det1) {
            
            if ("'".$det1->prod->cProd."'" == "'$reference'") {
                   
                $custoAtual = $det1->prod->vUnCom;
                
                $nfSefaz = (new NfSefaz())->find("access_key != '$this->access_key'  and substr(access_key,7,14) = ".$this->getCnpjFornecedor()." and store_id = $this->store_id")->order("nfe_number ASC")->fetch(true);
                foreach ($nfSefaz as $nf) {
                    foreach ($nf->getItems as $det) {
                        if ("'".$det->prod->cProd."'" == "'".$det1->prod->cProd."'") {
                            $custoAnterior =  $det->prod->vUnCom;
                        }
                    }
                }
                
            }
           
        }
        
        if ($custoAnterior == '') {
            $valor = 100;
        } else {
            $valor = (($custoAnterior - $custoAtual)/$custoAnterior)*100;
        }
        return $custoAtual;
        
    }

    public function error() {
        
        foreach ($this->getItems as $det) {
            if ($this->aa2detnt()) {
                foreach ($this->aa2detnt as $aa2detnt) {
                    if (in_array($det->prod->cProd, $aa2detnt->references())) {
                        if (((round($aa2detnt->diferencaValor) - round($this->diferencaCustoPortal($det->prod->cProd))) > 0.10 || (round($aa2detnt->diferencaValor) - round($this->diferencaCustoPortal($det->prod->cProd))) < -0.10)
                        && $aa2detnt->REN_PRC_UN > 0 && $this->custoAnterior($det->prod->cProd) > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }else if (in_array($det->prod->cEAN, $aa2detnt->eans())) {
                        if (((round($aa2detnt->diferencaValor) - round($this->diferencaCustoPortal($det->prod->cProd))) > 0.10 || (round($aa2detnt->diferencaValor) - round($this->diferencaCustoPortal($det->prod->cProd))) < -0.10)
                        && $aa2detnt->REN_PRC_UN > 0 && $this->custoAnterior($det->prod->cProd) > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } 
                }
            } else {
                return false;
            }
            
        }
        return false;
    }

    public function diferencaVariacaoCusto() {
        
        if (!empty($this->getAG1IENSA())) {
            foreach ($this->getAG1IENSA() as $ag1iensa) {
                foreach ($this->getItems() as $det) {
                    if (in_array($det->prod->cProd,$ag1iensa->references()) or in_array($det->prod->cEAN,$ag1iensa->EANs())){
                        if (round($ag1iensa->perecentualVariacao()) <> round($this->diferencaCustoPortal($det->prod->cProd))) {
                            if ($this->custoAnterior($det->prod->cProd) > 0 or $ag1iensa->getMargin($this->store_id) > 30 or $ag1iensa->getMargin($this->store_id) < -5) {
                                if ($ag1iensa->getDatePrice2() >= 1210101) {
                                    $data[$ag1iensa->ESITC_CODIGO] = "Código produto $ag1iensa->ESITC_CODIGO - $ag1iensa->ESITC_DIGITO, Custo anterior ".$this->custoAnterior($det->prod->cProd)." Nota Anterior ".$this->notaAnterior($det->prod->cProd).", Margem ".$ag1iensa->getMargin($this->store_id)." Variação divergente!";
                                }
                                
                            }
                        }
                    }
                }
            }
            if (!empty($data)) {
                $data['msg'] = implode('\n',$data);
                $data['value'] = true;
            } else {
                $data['msg'] = "Sem divergencias na variação.";
                $data['value'] = false;
            }
        } else {
            $data['msg'] = "Nota Não Atualizada";
            $data['value'] = false;
        }
        return $data;
    }

    public function checkVencimento() {
        if ($this->group_id == 1 or $this->group_id == 10 or $this->group_id == 2) {
            if (strlen($this->access_key) == 44) {
                
                if ($this->updated() && $this->updated() !== true) {
                    if (($this->getDueDateAmount() == $this->updated()->getDueDateAmount())){
                        $i = 1;
                        foreach($this->getDueDate() as $venc) {
                            $portal[$i] = $venc->due_date;
                            $i++;
                        }
                        $ok = true;
        
                        foreach($this->updated()->getDuedate() as $venc) {
                            $rms[$i] = dateRMSToDateI($venc->FIV_DTA_VENCTO);
                            $i++;
                            if (in_array(dateRMSToDateI($venc->FIV_DTA_VENCTO), $portal)) {
                                //nothing to do
                            } else {
                                $msg .= "Não encontrado Portal ".dateRMSToDateI($venc->FIV_DTA_VENCTO)." , ";
                                $ok = false;
                            }
        
                        }
        
                        foreach($this->getDueDate() as $venc) {
                            if (in_array($venc->due_date, $rms)) {
                                //nothing to do
                            } else {
                                $msg .= "Não encontrado RMS ".$venc->due_date." , ";
                                $ok = false;
                            }
                        }
        
                        if ($ok) {
                            $retorno['ok'] = true;
                            if (is_array($rms)) {
                                $rms = implode(' ,',$rms);
                            }
                            if (is_array($portal)) {
                                $portal = implode(' ,',$portal);
                            }
                            $retorno['msg'] = "Vencimentos sem divergencias. RMS: ".$rms.". Portal: ".$portal.".";
                            return $retorno;
                        } else {
                            $retorno['ok'] = false;
                            $retorno['msg'] = "Divergencias Vencimentos: $msg .";
                            return $retorno;
                        }
                        
                    } else {
                        
                        $retorno['ok'] = false;
                        $retorno['msg'] = "Quantidade de vencimentos divergentes: RMS 0".$this->updated()->getDueDateAmount()." Portal 0" .$this->getDueDateAmount().".";
                        return $retorno;
                    }        
                } else {
                    $retorno['ok'] = false;
                    $retorno['msg'] = "Nota não Atualizada!";
                    return $retorno;
                }
            } else {
                $retorno['ok'] = true;
                $retorno['msg'] = "Impossivel veificar sem chave de acesso.";
                return $retorno;
            } 
        } else {
            $retorno['ok'] = true;
            $retorno['msg'] = "Integração ainda não preparada.";
            return $retorno;
        }
       
        
    }

    public function getItemCadastro($reference) {
        $r = (new RegistroCadastro)->find("nf_receipt_id = $this->id and referencia = $reference")->fetch();
        if ($r) {
            return true;
        } else {
            return false;
        }
    }


    public function aa2ctipo() {
        /**
         * selecionar base de dados
         */
        switch ($this->group_id) {
            case 1:
                $aa2ctipo = new AA2CTIPO();
                break;
            case 10:
                $aa2ctipo = new AA2CTIPO();
                break;
            case 2:
                $aa2ctipo = new AA2CTIPO2();
                break;
            case 3:
                $aa2ctipo = new AA2CTIPO3();
                break;
            default:
                return null;
                break;
        }
        $tipo = $aa2ctipo->find("TIP_CGC_CPF = $this->getCnpjFornecedor")->fetch();
        return $tipo->TIP_CODIGO;
    }

    public function getAG1IENSA()
    {
        /**
         * selecionar base de dados
         */
        switch ($this->group_id) {
            case 1:
                $aa2ctipo = new AA2CTIPO();
                $ag1iensa = new AG1IENSA();
                break;
            case 10:
                $aa2ctipo = new AA2CTIPO();
                $ag1iensa = new AG1IENSA();
                break;
            case 2:
                $aa2ctipo = new AA2CTIPO2();
                $ag1iensa = new AG1IENSA2();
                break;
            case 3:
                $aa2ctipo = new AA2CTIPO3();
                $ag1iensa = new AG1IENSA3();
                break;
            default:
                return null;
                break;
        }
        $itens = $ag1iensa->find("ESCLC_CODIGO = $this->store_id and  ESCHLJC_CODIGO = $this->aa2ctipo and  ESCHC_NRO_NOTA = $this->nf_number and ESCHC_SER_NOTA = $this->getNFeSerie")->fetch(true);
        return $itens;
    }

}