<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ParameterWeek extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("parameter_week",[],"id", true);        
    }
    
}