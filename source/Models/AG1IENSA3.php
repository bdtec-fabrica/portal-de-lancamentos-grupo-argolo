<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer4;

class AG1IENSA3 extends DataLayer4
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG1IENSA",[],"", false);        
    }   

    public function getItemRegister(){
        $aa3citem = (new AA3CITEM3())->find("GIT_COD_ITEM = :gci","gci=$this->ESITC_CODIGO")->fetch();
        return $aa3citem;
    }

    public function getPrice() {
        $aa2cprec = (new AA2CPREC3())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $this->ESCHC_DATA3","pl=$this->ESCLC_CODIGO&pci=$this->ESITC_CODIGO&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_PRECO;
        }
        return $price;
    }

    public function getPrice2() {
        $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $this->ESCHC_DATA3 and PRE_TIPO = :pt","pt=N&pl=$this->ESCHLJC_CODIGO3&pci=$this->ESITC_CODIGO&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_PRECO;
        }
        return $price;
    }
    
    public function getICMSAliquot($store){
        $aa2cloja = (new AA2CLOJA3())->find("LOJ_CODIGO = :lc","lc=$store")->fetch();
        $aa2tfisc = (new AA2TFISC3())->find("TFIS_ORIGEM = :t1 AND TFIS_FIGURA = :t2 AND TFIS_CODIGO = :t3
        AND TFIS_AUTOMACAO = :t4","t1=BA&t2=$this->ENTSAIC_FIGURA&t3=512&t4=$aa2cloja->LOJ_AUTONOMIA")->fetch();
        

        return $aa2tfisc->TFIS_ALIQ_ICM;
    }

    public function getPISCOFINSAliquot(){
        if ($this->getItemRegister()->GIT_CATEGORIA_ANT == 0){
            return 9.25;
        } else {
            return 0;
        }
        
    }
    public function razaoDestino() {
        $aa2ctipo = (new AA2CTIPO3())->find("TIP_CODIGO = :code","code=$this->ESCHLJC_CODIGO3")->fetch();
        return $aa2ctipo;
    }
    public function getMargin($store) {
        if ($this->getPrice() > 0){
            $margin = 100-((($this->ENTSAIC_CUS_UN/$this->getPrice())*100)+(($this->getPISCOFINSAliquot-($this->getPISCOFINSAliquot*($this->getICMSAliquot($store)/100)))+$this->getICMSAliquot($store)));
            return $margin;
        } else {
            $margin = 0;
        return round($margin,2);
        }
        
                
    }

    public function getMargin2($store) {
        if ($this->getPrice2() > 0){
            $margin = 100-((($this->ENTSAIC_CUS_UN/$this->getPrice2())*100)+(($this->getPISCOFINSAliquot-($this->getPISCOFINSAliquot*($this->getICMSAliquot($store)/100)))+$this->getICMSAliquot($store)));
            return $margin;
        } else {
            $margin = 0;
        return round($margin,2);
        }
        
                
    }

    public function validateStore(){

        $store = (new Store())->find("substr(code,1,length(code)-1) = :cd","cd=$this->ESCLC_CODIGO")->count();
        if($store > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function validateStore2(){
        $store = (new Store())->find("substr(code,1,length(code)-1) = :cd","cd=$this->ESCHLJC_CODIGO3")->count();
        if($store > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAA1CFISC() {
        $aa1cfisc = (new AA1CFISC3())->find("FIS_LOJ_ORG = :flo and FIS_LOJ_DST = :fld and FIS_NRO_NOTA = :fnn and FIS_OPER = :fo",
        "flo=$this->ESCHLJC_CODIGO3&fld=$this->ESCLC_CODIGO&fnn=$this->ESCHC_NRO_NOTA3&fo=$this->ESCHC_AGENDA3")->fetch();
        return $aa1cfisc;
    }

    public function getPosted() {
        $updated_at = "20".substr($this->ESCHC_DATA,1,2)."-".substr($this->ESCHC_DATA,3,2)."-".substr($this->ESCHC_DATA,5,2);
         $nfReceipt = (new NfReceipt())->find("nf_number = :nn and store_id = :sid and substr(updated_at,1,10) = :ua","nn=$this->ESCHC_NRO_NOTA3&sid=$this->ESCLC_CODIGO&ua=$updated_at")->fetch();
         return $nfReceipt;
    }

    public function agenda987() {
        $ag1iensa = (new AG1IENSA3())->find("ESCHC_AGENDA3 = :ESCHC_AGENDA3 and ESCHLJC_CODIGO3 = :ESCHLJC_CODIGO3
         and ESITC_CODIGO = :ESITC_CODIGO and ENTSAIC_QUANTI_UN = :ENTSAIC_QUANTI_UN",
         "ESCHC_AGENDA3=987&ESCHLJC_CODIGO3=$this->ESCHLJC_CODIGO3&ESITC_CODIGO=$this->ESITC_CODIGO&ENTSAIC_QUANTI_UN=$this->ENTSAIC_QUANTI_UN")->fetch(true);
        $i = 0;
        foreach ($ag1iensa as $iensa) {
            $i++;
        }
        if ($i == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getDatePrice() {
        $aa2cprec = (new AA2CPREC3())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $this->ESCHC_DATA3 and PRE_TIPO = :pt","pt=N&pl=$this->ESCLC_CODIGO&pci=$this->ESITC_CODIGO&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
        foreach($aa2cprec as $prec){
            $price = $prec->PRE_DAT_INICIO;
        }
        return substr($price,5,2)."/".substr($price,3,2)."/".substr($price,1,2); 
    }

    public function ultimaSaida() {
        $aa2cestq = (new AA2CESTQ3())->find("GET_COD_PRODUTO = :GET_COD_PRODUTO AND GET_COD_LOCAL = :GET_COD_LOCAL","GET_COD_LOCAL=$this->ESCLC_CODIGO$this->ESCLC_DIGITO&GET_COD_PRODUTO=$this->ESITC_CODIGO$this->ESITC_DIGITO")->fetch();
        $ultimaSaida = substr(str_pad($aa2cestq->GET_DT_ULT_FAT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($aa2cestq->GET_DT_ULT_FAT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($aa2cestq->GET_DT_ULT_FAT,6,'0',STR_PAD_LEFT),4,2);
        return $ultimaSaida;
    }

    public function varicaoCusto(int $produto = null, int $loja, string $periodo, float $percentualVariacao = null, $type = null) {
        $aa3citem = new Aa3citemnm();
        $ctcon = (new AA1CTCON3())->getComprasFiscal($type);
        if ($loja != null) {
            $params['ESCLC_CODIGO'] = $loja;
            $comparation[0] = "ESCLC_CODIGO = :ESCLC_CODIGO";
        }
        
        if ($produto != null) {
            $params['ESITC_CODIGO'] = $produto;
            $comparation[1] = "ESITC_CODIGO = :ESITC_CODIGO";
        }
        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);
        $max = $this->find($comparation." AND ENTSAIC_SITUACAO != '9' AND ESCHC_DATA BETWEEN $periodo and ESCHC_AGENDA IN ($ctcon)",$params,"ESITC_CODIGO,ESITC_DIGITO, MAX(ENTSAIC_CUS_UN) MAX")->group("ESITC_CODIGO,ESITC_DIGITO")->order("ESITC_CODIGO ASC")->fetch(true);

    
        
        $min = $this->find($comparation." AND ENTSAIC_SITUACAO != '9' AND ESCHC_DATA BETWEEN $periodo and ESCHC_AGENDA IN ($ctcon)",$params,"ESITC_CODIGO,ESITC_DIGITO, MIN(ENTSAIC_CUS_UN) MIN")->group("ESITC_CODIGO,ESITC_DIGITO")->order("ESITC_CODIGO DESC")->fetch(true);

        foreach ($max as $M) {
            $relacao[$M->ESITC_CODIGO][$M->ESITC_CODIGO]['codigo'] = $M->ESITC_CODIGO.$M->ESITC_DIGITO;
            $relacao[$M->ESITC_CODIGO][$M->ESITC_CODIGO]['descricao'] = $aa3citem->getAA3CITEM($M->ESITC_CODIGO)->GIT_DESCRICAO;
            $relacao[$M->ESITC_CODIGO][$M->ESITC_CODIGO]['maiores'] = $M->MAX;

        }

        foreach ($min as $m) {
            $relacao[$m->ESITC_CODIGO][$m->ESITC_CODIGO]['menores'] = $m->MIN;

        }
        
        

        return $relacao;
    }

    public function getLastCost($periodo, $item, $store) {
        $ctcon = (new AA1CTCON3())->getComprasFiscal('C');
        $custos = $this->find("ESCHC_DATA3 BETWEEN $periodo AND ESITC_CODIGO||ESITC_DIGITO = $item  AND ESCHC_AGENDA3 IN ($ctcon) AND ESCLC_CODIGO = $store")->order("ESCHC_DATA3 DESC")->fetch(true);

        foreach ($custos as $custo) {
            return $custo->ENTSAIC_CUS_UN;
        }
    }
}