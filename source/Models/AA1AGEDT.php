<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AA1AGEDT extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1AGEDT",[],"", false);        
    }

    public function dobraNota($DET_LOJA,$DET_TIPO,$DET_NOTA,$DET_SERIE,$DET_LOJA_1,$DET_VALOR,$CNPJ) {
        if (strlen($DET_NOTA) > 1) {
            $aa1agedt = new AA1AGEDT();
            $aa1agedt->DET_TRANSMIT = 0;
            $aa1agedt->DET_INTG_CP = ' ';
            $aa1agedt->DET_INTG_RC = ' ';
            $aa1agedt->DET_DATA = '1'.date("ymd");
            $aa1agedt->DET_LOJA = $DET_LOJA;
            $aa1agedt->DET_TIPO = $DET_TIPO;
            $aa1agedt->DET_NOTA = $DET_NOTA;
            $aa1agedt->DET_SERIE = $DET_SERIE;
            $aa1agedt->DET_LOJA_1 = $DET_LOJA_1;
            $aa1agedt->DET_VALOR = $DET_VALOR;
            $aa1agedt->DET_FLAG = 0;
            $aa1agedt->DET_ROM = 0;
            $aa1agedt->DET_DIG = 0;
            $aa1agedt->DET_CRIT = 0;
            $aa1agedt->DET_INTG_LF = ' ';
            $aa1agedt->DET_INTG_DB = ' ';
            $aa1agedt->DET_SECAO = 0;
            $aa1agedt->DET_ERRO = '                     ';
            $aa1agedt->DET_RATEIO = ' ';
            $aa1agedt->DET_LIBERA = ' ';
            $aa1agedt->DET_DIVERGE = ' ';
            $aa1agedt->DET_FLAG_RAT = ' ';
            $aa1agedt->DET_PORTARIA = ' ';
            $aa1agedt->RMSRSV_001 = 'D';
            
            $aa1agedtID = $aa1agedt->save();  
            $aa1agedt = new AA1AGEDT();
            $aa1agedt->execute2($DET_LOJA,$DET_TIPO,$DET_NOTA,$DET_SERIE,$DET_LOJA_1,$DET_VALOR,$CNPJ);
            $ag2detnt = (new AG2DETNT())->find("REN_DESTINO = $DET_LOJA and REN_DTA_AGENDA = ".'1'.date("ymd")." and REN_TPO_AGENDA = $DET_TIPO and REN_NOTA = $DET_NOTA and REN_SERIE = $DET_SERIE and REN_DISTRIB = $DET_LOJA_1")->fetch(true);
            $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $DET_LOJA_1")->fetch();
            $i = 1;
            //Criar um Array com todos os itens da nota
            foreach ($ag2detnt as $detnt) {
                $item[$i] = substr($detnt->REN_ITEM,7)*1;
                $i++;
            }
            
            foreach ($ag2detnt as $detnt) {
                // verificar se algum dos itens tem troca codigo (source_code)
                $exageCode = (new ExchangeCode())->find("store_id = ".substr($DET_LOJA,0,strlen($DET_LOJA)-1)." and source_code = ".(substr($detnt->REN_ITEM,7)*1)." and cnpj = $aa2ctipo->TIP_CGC_CPF")->fetch();
                // se tiver troca codigo
                if ($exageCode->destination_code) {
                    // verificar se o codigo de destino do troca codigo esta entre os itens da nota
                    $aa3ccean = (new AA3CCEAN())->find("EAN_COD_EAN = $exageCode->destination_code")->fetch();
                    if (in_array($aa3ccean->EAN_COD_PRO_ALT, $item)) {
                        $update = (new AG2DETNT());
                        // onde o codigo do produto for igual ao codigo de destino do troca codigo
                        $where = "(substr(REN_ITEM,7)*1) = $aa3ccean->EAN_COD_PRO_ALT and REN_DESTINO = $DET_LOJA and REN_DTA_AGENDA = ".'1'.date("ymd")." and REN_TPO_AGENDA = $DET_TIPO and REN_NOTA = $DET_NOTA and REN_SERIE = $DET_SERIE and REN_DISTRIB = $DET_LOJA_1";
                        // 
    
                        $code = "REN_QTD_REC = REN_QTD_REC + $detnt->REN_QTD_REC , REN_QTD_FAT = REN_QTD_FAT + $detnt->REN_QTD_FAT , REN_PRC_EMB = ((REN_PRC_EMB * REN_QTD_FAT) + ($detnt->REN_QTD_FAT * $detnt->REN_PRC_EMB))/(REN_QTD_FAT + $detnt->REN_QTD_FAT), REN_PRC_VEN_CAD =((REN_PRC_EMB * REN_QTD_FAT) + ($detnt->REN_QTD_FAT * $detnt->REN_PRC_EMB))/(REN_QTD_FAT + $detnt->REN_QTD_FAT), REN_PRC_INF = ((REN_PRC_EMB * REN_QTD_FAT) + ($detnt->REN_QTD_FAT * $detnt->REN_PRC_EMB))/(REN_QTD_FAT + $detnt->REN_QTD_FAT)";
    
    
                        $updateId = $update->update2($code,$where);
                       if ($updateId) {
                        $destroy = $update->destroy2("REN_ITEM = $detnt->REN_ITEM and REN_DESTINO = $DET_LOJA and REN_DTA_AGENDA = ".'1'.date("ymd")." and REN_TPO_AGENDA = $DET_TIPO and REN_NOTA = $DET_NOTA and REN_SERIE = $DET_SERIE and REN_DISTRIB = $DET_LOJA_1");
                       }
                        
                    } else {
                        $update = (new AG2DETNT());
                        // onde o codigo do produto for igual ao codigo de destino do troca codigo
                        $where = "REN_ITEM = $detnt->REN_ITEM and REN_DESTINO = $DET_LOJA and REN_DTA_AGENDA = ".'1'.date("ymd")." and REN_TPO_AGENDA = $DET_TIPO and REN_NOTA = $DET_NOTA and REN_SERIE = $DET_SERIE and REN_DISTRIB = $DET_LOJA_1";
                        // 
                        $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM||GIT_DIGITO = $aa3ccean->EAN_COD_PRO_ALT")->fetch();
                        $code = "REN_ITEM = ".str_pad($aa3citem->GIT_COD_FOR,7,'0',STR_PAD_RIGHT).str_pad($aa3ccean->EAN_COD_PRO_ALT,8,'0',STR_PAD_LEFT);
                        // echo "UPDATE AG2DETNT SET $code WHERE $where";
    
                        $updateId = $update->update2($code,$where);
                    }
                    
    
                    
                }
            }
        }


        
    }

    public function emissaoPropria($DET_LOJA,$DET_TIPO,$DET_NOTA,$DET_SERIE,$DET_LOJA_1,$DET_VALOR) {
        
        $aa1agedt = new AA1AGEDT();
        $aa1agedt->DET_TRANSMIT = 0;
        $aa1agedt->DET_INTG_CP = ' ';
        $aa1agedt->DET_INTG_RC = ' ';
        $aa1agedt->DET_DATA = '1'.date("ymd");
        $aa1agedt->DET_LOJA = $DET_LOJA;
        $aa1agedt->DET_TIPO = $DET_TIPO;
        $aa1agedt->DET_NOTA = $DET_NOTA;
        $aa1agedt->DET_SERIE = $DET_SERIE;
        $aa1agedt->DET_LOJA_1 = $DET_LOJA_1;
        $aa1agedt->DET_VALOR = $DET_VALOR;
        $aa1agedt->DET_FLAG = 0;
        $aa1agedt->DET_ROM = 0;
        $aa1agedt->DET_DIG = 0;
        $aa1agedt->DET_CRIT = 0;
        $aa1agedt->DET_INTG_LF = ' ';
        $aa1agedt->DET_INTG_DB = ' ';
        $aa1agedt->DET_SECAO = 0;
        $aa1agedt->DET_ERRO = '                     ';
        $aa1agedt->DET_RATEIO = ' ';
        $aa1agedt->DET_LIBERA = ' ';
        $aa1agedt->DET_DIVERGE = ' ';
        $aa1agedt->DET_FLAG_RAT = ' ';
        $aa1agedt->DET_PORTARIA = ' ';
        $aa1agedt->RMSRSV_001 = 'D';
        $aa1agedtID = $aa1agedt->save();  
        $aa1agedt = new AA1AGEDT();
        $aa1agedt->execute2($DET_LOJA,$DET_TIPO,$DET_NOTA,$DET_SERIE,$DET_LOJA_1,$DET_VALOR);
        $ag2detnt = (new AG2DETNT())->find("REN_DESTINO = $DET_LOJA and REN_DTA_AGENDA = ".'1'.date("ymd")." and REN_TPO_AGENDA = $DET_TIPO and REN_NOTA = $DET_NOTA and REN_SERIE = $DET_SERIE and REN_DISTRIB = $DET_LOJA_1")->fetch(true);
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $DET_LOJA_1")->fetch();
        $i = 1;
        //Criar um Array com todos os itens da nota
        foreach ($ag2detnt as $detnt) {
            $item[$i] = substr($detnt->REN_ITEM,7)*1;
            $i++;
        }
        
        foreach ($ag2detnt as $detnt) {
            // verificar se algum dos itens tem troca codigo (source_code)
            $exageCode = (new ExchangeCode())->find("store_id = ".substr($DET_LOJA,0,strlen($DET_LOJA)-1)." and source_code = ".(substr($detnt->REN_ITEM,7)*1)." and cnpj = $aa2ctipo->TIP_CGC_CPF")->fetch();
            // se tiver troca codigo
            if ($exageCode->destination_code) {
                // verificar se o codigo de destino do troca codigo esta entre os itens da nota
                $aa3ccean = (new AA3CCEAN())->find("EAN_COD_EAN = $exageCode->destination_code")->fetch();
                if (in_array($aa3ccean->EAN_COD_PRO_ALT, $item)) {
                    $update = (new AG2DETNT());
                    // onde o codigo do produto for igual ao codigo de destino do troca codigo
                    $where = "(substr(REN_ITEM,7)*1) = $aa3ccean->EAN_COD_PRO_ALT and REN_DESTINO = $DET_LOJA and REN_DTA_AGENDA = ".'1'.date("ymd")." and REN_TPO_AGENDA = $DET_TIPO and REN_NOTA = $DET_NOTA and REN_SERIE = $DET_SERIE and REN_DISTRIB = $DET_LOJA_1";
                    // 

                    $code = "REN_QTD_REC = REN_QTD_REC + $detnt->REN_QTD_REC , REN_QTD_FAT = REN_QTD_FAT + $detnt->REN_QTD_FAT , REN_PRC_EMB = ((REN_PRC_EMB * REN_QTD_FAT) + ($detnt->REN_QTD_FAT * $detnt->REN_PRC_EMB))/(REN_QTD_FAT + $detnt->REN_QTD_FAT), REN_PRC_VEN_CAD =((REN_PRC_EMB * REN_QTD_FAT) + ($detnt->REN_QTD_FAT * $detnt->REN_PRC_EMB))/(REN_QTD_FAT + $detnt->REN_QTD_FAT), REN_PRC_INF = ((REN_PRC_EMB * REN_QTD_FAT) + ($detnt->REN_QTD_FAT * $detnt->REN_PRC_EMB))/(REN_QTD_FAT + $detnt->REN_QTD_FAT)";


                    $updateId = $update->update2($code,$where);
                   if ($updateId) {
                    $destroy = $update->destroy2("REN_ITEM = $detnt->REN_ITEM and REN_DESTINO = $DET_LOJA and REN_DTA_AGENDA = ".'1'.date("ymd")." and REN_TPO_AGENDA = $DET_TIPO and REN_NOTA = $DET_NOTA and REN_SERIE = $DET_SERIE and REN_DISTRIB = $DET_LOJA_1");
                   }
                    
                } else {
                    $update = (new AG2DETNT());
                    // onde o codigo do produto for igual ao codigo de destino do troca codigo
                    $where = "REN_ITEM = $detnt->REN_ITEM and REN_DESTINO = $DET_LOJA and REN_DTA_AGENDA = ".'1'.date("ymd")." and REN_TPO_AGENDA = $DET_TIPO and REN_NOTA = $DET_NOTA and REN_SERIE = $DET_SERIE and REN_DISTRIB = $DET_LOJA_1";
                    // 
                    $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM||GIT_DIGITO = $aa3ccean->EAN_COD_PRO_ALT")->fetch();
                    $code = "REN_ITEM = ".str_pad($aa3citem->GIT_COD_FOR,7,'0',STR_PAD_RIGHT).$aa3ccean->EAN_COD_PRO_ALT;
                    // echo "UPDATE AG2DETNT SET $code WHERE $where";

                    $updateId = $update->update2($code,$where);
                }
                

                
            }
        }

        
    }
}

/**
 * Percorrer os itens da nota guardando os codigos dos itens.
 * Percorrer novamente os itens da nota, desta vez, verificar item por item se algum esta no troca codigo.
 * Verificar se o codigo de destino do item encontrado já existe na nota. Caso exista,
 *  selecione altere ele somando os valores e depois exclua o item que tem troca codigo.
 * 
 */