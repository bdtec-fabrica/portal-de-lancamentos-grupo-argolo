<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer124;

class Nfce124 extends DataLayer124
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}