<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;


class AA1FORIT5 extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("rms.AA1FORIT",[],"", false);        
    }

    public function aa3citem (){
        $aa3citem = (new AA3CITEM5())->find("GIT_COD_ITEM = $this->FORITE_COD_ITEM")->fetch();
        return $aa3citem;
    }

    public function aa3ccean(){
        $aa3ccean = (new AA3CCEAN5())->find("EAN_COD_PRO_ALT = ".$this->aa3citem()->GIT_COD_ITEM.$this->aa3citem()->GIT_DIGITO)->fetch(true);
        return $aa3ccean;
    }

    public function aa2ctipo() {
        $aa2ctipo = (new AA2CTIPO5())->find("TIP_CODIGO = $this->FORITE_COD_FORN")->fetch();
        return $aa2ctipo;
    }
}