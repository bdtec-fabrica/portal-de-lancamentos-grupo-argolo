<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer5;


class AA1FORIT4 extends DataLayer5
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("rms.AA1FORIT",[],"", false);        
    }

    public function aa3citem (){
        $aa3citem = (new AA3CITEM4())->find("GIT_COD_ITEM = $this->FORITE_COD_ITEM")->fetch();
        return $aa3citem;
    }

    public function aa3ccean(){
        $aa3ccean = (new AA3CCEAN4())->find("EAN_COD_PRO_ALT = ".$this->aa3citem()->GIT_COD_ITEM.$this->aa3citem()->GIT_DIGITO)->fetch(true);
        return $aa3ccean;
    }

    public function aa2ctipo() {
        $aa2ctipo = (new AA2CTIPO4())->find("TIP_CODIGO = $this->FORITE_COD_FORN")->fetch();
        return $aa2ctipo;
    }
}