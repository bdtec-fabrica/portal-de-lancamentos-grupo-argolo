<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer310;

class Nfce310 extends DataLayer310
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}