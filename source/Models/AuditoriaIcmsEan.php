<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class AuditoriaIcmsEan extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("auditoria_icms_ean",[],"id", true);        
    }

}