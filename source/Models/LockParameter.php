<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use Exception;

class LockParameter extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("lock_parameter",[]);        
    }

    public function newSave() {
        $lockParameter = (new LockParameter())->find("type = $this->type and parameter_id = $this->parameter_id")->count();
        
        if ($lockParameter == 0) {
            return $this->save();
        } else {
            $data['msg'] = "Parametro já existe";
            $data['success'] = false;
            return $data;
        }
    }
}