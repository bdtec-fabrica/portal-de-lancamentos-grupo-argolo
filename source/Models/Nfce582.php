<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer582;

class Nfce582 extends DataLayer582
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}