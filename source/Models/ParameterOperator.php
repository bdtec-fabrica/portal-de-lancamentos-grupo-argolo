<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;


class ParameterOperator extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("parameter_operator",[],"id", true);        
    }

    public function parameterName () {
        $name = (new Parameter())->find("id = $this->parameter_id")->fetch()->name;
        return $name;
    }


    
}