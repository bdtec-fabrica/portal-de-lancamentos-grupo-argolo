<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class CollectionHeader extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("collection_header",[],"id", true);        
    }

    public function getCollectionItems() {
        return $CollectionItems = (new CollectionItems)->find("collection_header_id = :chid", "chid=$this->id")->fetch(true);

    }

    public function getStatus() {
        $collectionItems = (new CollectionItems())->find("collection_header_id = :chid","chid=$this->id")->fetch(true);
        
        foreach($collectionItems as $collectionItem) {
            if ($collectionItem->getStatus() == "E") {
                return "E";
                die;
            }
                
        }
        return "C";
    }
    
}