<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Parameter extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("parameter",[],"id", true);        
    }

    public function veification ($userId,$request) {
        $parameters = $this->find()->fetch(true);
        $data['success'] = true;
        foreach ($parameters as $parameter) {
            $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id")->count();
            if ($parameterOperator > 0) {
                switch ($parameter->id) {
                    case 1:
                        $nfReceipt = (new NfReceipt())->find("operator_id = $userId and status_id not in (2,3,5,6,7,8,9,14,16)")->count();
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content > $nfReceipt")->count();
                        if ($parameterOperator == 0) {
                            $data['success'] = false;
                            $data['message'] .= "Operador com $nfReceipt notas limite de $parameter->content | ";
                        }
                        break;
                    case 2:
                        $cnpj = $request->getCnpjFornecedor();
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content = '$cnpj'")->count();
                        if ($parameterOperator != 0) {
                            $data["success"] = false;
                            $data["message"] .= "Operador não pode lançar a nota desde fornecedor. | ";
                        }
                        break;
                    case 3:
                        $store = $request->store_id;
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content = '$store'")->count();
                        if ($parameterOperator != 0) {
                            $data["success"] = false;
                            $data["message"] .= "Operador não pode lançar as notas dessa Loja. | ";
                        }
                        break;
                    case 4:
                        $type = $request->type_id;
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content = '$type'")->count();
                        if ($parameterOperator == 0) {
                            $data["success"] = false;
                            $data["message"] .= "Operador não pode lançar as notas deste tipo. | ";
                        }
                        break;
                    case 5:
                        $operation = $request->operation_id;
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content = '$operation'")->count();
                        if ($parameterOperator == 0) {
                            $data["success"] = false;
                            $data["message"] .= "Operador não pode lançar as notas desta operação. | ";
                        }
                        break;
                    case 6:
                        $amount = $request->items_quantity;
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content > '$amount'")->count();
                        if ($parameterOperator == 0) {
                            $data["success"] = false;
                            $data["message"] .= "Operador não pode lançar as notas Com essa quantidade de itens. | ";
                        }
                        break;
                    case 7:
                        $amount = $request->items_quantity;
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content < '$amount'")->count();
                        if ($parameterOperator == 0) {
                            $data["success"] = false;
                            $data["message"] .= "Operador não pode lançar as notas Com essa quantidade de itens. | ";
                        }
                        break;
                    case 8:
                        $user = (new User())->findById("$userId");
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content < '$user->status'")->count();
                        if ($parameterOperator == 0) {
                            $data["success"] = false;
                            $data["message"] .= "Operador não esta operando no momento. | ";
                        }
                        break;
                    case 9:
                        $group = $request->group_id;
                        $parameterOperator = (new ParameterOperator())->find("user_id = $userId and parameter_id = $parameter->id and content = '$group'")->count();
                        if ($parameterOperator == 0) {
                            $data["success"] = false;
                            $data["message"] .= "Operador não pode lançar as notas deste grupo economico. | ";
                        }
                        break;

                }
            }
        }
        return $data;
    }

    // public function name($id) {
    //     $name = $this->findById($id);
    //     return $name->name;
    // }
    
}