<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class KwDifferenceAttachment extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("kw_difference_attachment",[],"id", true);        
    }

    public function getUser() {
        $user = (new User())->findById($this->user_id);
        return $user;
    }
}