<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class AuxiliarCancelamento extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("auxiliar_cancelamento",[],"id", true);        
    }

}