<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AA1FFISC extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1FFISC_".AA1FFISI,[],"", false);        
    }

    public function AA1FFISI() {
        $aa1ffisi = (new AA1FFISI())->find("FISI_ID = $this->FISC_ID")->fetch(true);
        return $aa1ffisi;
    }
}