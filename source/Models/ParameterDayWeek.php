<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ParameterDayWeek extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("parameter_day_week",[],"id", true);        
    }
    
}