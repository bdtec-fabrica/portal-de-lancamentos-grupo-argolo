<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer10;

class AA2CTIPO10 extends DataLayer10
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA2CTIPO",[],"", false);        
    }

    public function aa1forit() {
        $aa1forit = (new AA1FORIT10())->find("FORITE_COD_FORN = $this->TIP_CODIGO")->fetch(true);
        return $aa1forit;
    }
}