<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;

class MlfAuxnfitem extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("consinco.mlf_auxnfitem",[]);        
    }

    public function mapProduto() {
        $mapProduto = (new MapProduto())->find("seqproduto = $this->SEQPRODUTO")->fetch();
        return $mapProduto;
    }

    public function mlfAuxnotafiscal() {
        $capa = (new MlfAuxnotafiscal())->find("seqauxnotafiscal = $this->SEQAUXNOTAFISCAL")->fetch();
        return $capa;
    }

    public function custo() {
        $custo = round(($this->VLRITEM/$this->QUANTIDADE) - ($this->VLRICMS/$this->QUANTIDADE) - ($this->VLRPIS/$this->QUANTIDADE) - ($this->VLRCOFINS/$this->QUANTIDADE),2);
        return $custo;
    }

    public function preco() {
        $precos = (new MadProdLogPreco())->find("seqproduto = $this->SEQPRODUTO and nroempresa = ".$this->mlfAuxnotafiscal()->NROEMPRESA." and tipoaltpreco  = 'N'","","consinco.mad_prodlogpreco.*,TO_DATE(TO_CHAR(dtahoralteracao), 'DD-MM-YYYY') updated_at")->order("dtahoralteracao ASC")->fetch(true);
        foreach ($precos as $precos2) {
            $preco = $precos2;
        }
        return $preco;
    }

    public function ICMS() {
        $mapTributacaoUf = (new MapTributacaoUf())->find("nrotributacao = ".$this->mapProduto()->mapFamDivisao()->NROTRIBUTACAO." and ufempresa = 'BA' and ufclientefornec = 'BA' and tiptributacao = 'SN' and nroregtributacao = 0")->fetch();
        return $mapTributacaoUf->PERALIQUOTA; 
    }

    public function pisCofins() {
        return $this->PERALIQUOTAPIS + $this->PERALIQUOTACOFINS;
    }
}