<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer744;

class Nfce744 extends DataLayer744
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}