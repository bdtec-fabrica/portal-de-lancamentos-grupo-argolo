<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class InventarioContabilItens extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("invantario_contabil_itens",[],"id", true);        
    }
}