<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Store extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("stores",[],"id", true);        
    }

    public function getCertName() {
        $json = file_get_contents(__DIR__."/../StoreConfig/$this->code.json");
        $data = json_decode($json);

        return $data->certPfxName;

    }

    public function getCertPassword() {
        $json = file_get_contents(__DIR__."/../StoreConfig/$this->code.json");
        $data = json_decode($json);

        return $data->certPassword;
    }

    public function getCertValidity() {
        $file = file_get_contents(__DIR__."/../StoreConfig/$this->code.json");
        $json = json_decode($file);
        $certName = $json->certPfxName;
        $password = $json->certPassword;
        $pfxCertPrivado = __DIR__."/../Certs/$certName";
        $pfxContent = file_get_contents($pfxCertPrivado);

        if (!openssl_pkcs12_read($pfxContent, $x509certdata, $password)) {
            return "b";
            } else {
        $CertPriv   = array();
        $CertPriv   = openssl_x509_parse(openssl_x509_read($x509certdata['cert']));

        $PrivateKey = $x509certdata['pkey'];

        $pub_key = openssl_pkey_get_public($x509certdata['cert']);
        $keyData = openssl_pkey_get_details($pub_key);

        $PublicKey  = $keyData['key'];
        $data2 = date("d/m/Y",$CertPriv['validTo_time_t']);
        return $data2;
                  
            }  
        
    }    

    public function getGroup() {
        $group = (new StoreGroup())->find("id = :group_id","group_id=$this->group_id")->fetch();
        return $group->name;
    }

    public function getParams()
    {
        $stores = $this->find()->fetch(true);
        foreach($stores as $store){
            $code = $store->code;
            $codes[$code] = "ag$code=$code";
        }
        return "&".implode("&",$code);

    }
    public function getCompration()
    {
        $stores = $this->find()->fetch(true);
        foreach($stores as $store){
            $code = $store->code;
            $codes[$code] = " ESCLC_CODIGO = ag$code ";
        }
        return " and (".implode(" or ",$code).")";
    }

    public function getInventories() {

        if ($this->group_id == 1 || $this->group_id == 10) {
            $ag2icntr = (new AG2ICNTR())->find("substr(CNI_LOJA_1,0,length(CNI_LOJA_1)-1) = :sid and CNI_ITENS != :ci","ci=C&sid=$this->code")->order("CNI_DATA")->fetch(true);
            $inventories = (new Inventories())->find("store_id = :sid","sid=$this->code")->fetch(true);
        } elseif ($this->group_id == 2) {
            $ag2icntr = (new AG2ICNTR2())->find("substr(CNI_LOJA_1,0,length(CNI_LOJA_1)-1) = :sid and CNI_ITENS != :ci","ci=C&sid=$this->code")->order("CNI_DATA")->fetch(true);
            $inventories = (new Inventories())->find("store_id = :sid","sid=$this->code")->fetch(true);
        } elseif($this->group_id == 8 || $this->group_id == 4) {
            $ag2icntr = (new AG2ICNTR3())->find("CNI_LOJA_1 = :sid and CNI_ITENS != :ci","ci=C&sid=$this->code")->order("CNI_DATA")->fetch(true);
            $inventories = (new Inventories())->find("store_id = :sid","sid=$this->code")->fetch(true);
        }

        foreach($inventories as $inventory) {
            $dates[$inventory->id." - ".$inventory->title] = substr($inventory->date_inventory,2,2).substr($inventory->date_inventory,5,2).substr($inventory->date_inventory,8,2);
        }
    
        
        foreach($ag2icntr as $icntr) {
            $dates[$icntr->CNI_SEQUEN." - ".$icntr->CNI_DESCRICAO] = $icntr->CNI_DATA;
        }
        asort($dates);
        return $dates;
    }

    public function contacts() {
        $storeContacts = (new StoreContacts())->find("store_id = $this->id")->fetch(true);
        return $storeContacts;
    }

    public function contactsO() {
        $storeContacts = (new StoreContacts())->find("store_id = $this->id and type = 'O'")->fetch(true);
        return $storeContacts;
    }
    
}