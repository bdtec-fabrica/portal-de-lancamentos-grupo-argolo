<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Group extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("groups",[],"id", false);        
    }
    
}