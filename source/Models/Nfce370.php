<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer370;

class Nfce370 extends DataLayer370
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}