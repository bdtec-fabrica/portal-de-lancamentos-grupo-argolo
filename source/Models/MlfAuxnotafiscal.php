<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer6;

class MlfAuxnotafiscal extends DataLayer6
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("consinco.mlf_auxnotafiscal",[]);        
    }

    public function mlfNfitem() {
        $mlfNfitem = (new MlfAuxnfitem())->find("seqauxnotafiscal = $this->SEQAUXNOTAFISCAL")->fetch(true);
        return $mlfNfitem;
    }  

    public function gePessoa() {
        $gePessoa = (new GePessoa())->find("SEQPESSOA = $this->SEQPESSOA")->fetch();
        return $gePessoa;
    }

    public function vencimentos() {
       $vencimentos = (new MlfAuxnfvencimento())->find("SEQAUXNOTAFISCAL = $this->SEQ_AUXNOTAFISCL")->fetch(true);
       return $vencimentos;
    
    }
}