<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer760;

class Nfce760 extends DataLayer760
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}