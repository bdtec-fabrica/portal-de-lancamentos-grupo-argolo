<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer701;

class Nfce701 extends DataLayer701
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}