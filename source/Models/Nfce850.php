<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer850;

class Nfce850 extends DataLayer850
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("nfce",[],"", false);        
    }
    
}