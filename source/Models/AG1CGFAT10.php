<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer10;

class AG1CGFAT10 extends DataLayer10
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AG1CGFAT",[],"", false);        
    } 
}