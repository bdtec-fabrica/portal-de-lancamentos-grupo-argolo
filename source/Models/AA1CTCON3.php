<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer4;

class AA1CTCON3 extends DataLayer4
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA1CTCON",[],"", false);        
    }
    public function getEntradas1() {
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_intg_7 = :t7 and tbc_codigo = :tc","t3=E&t9=S&t7=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getEntradas2() {
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_intg_7 = :t7 and tbc_intg_11 = :t11 and tbc_codigo = :tc","t11=C&t3=E&t9=S&t7=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getEntradas3() { // entradas
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_codigo = :tc","t3=E&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getSaidas1() {
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_intg_7 = :t7 and tbc_codigo = :tc","t3=S&t9=S&t7=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getSaidas2() {
        $agendas = $this->find("tbc_intg_3 = :t3 and tbc_intg_9 = :t9 and tbc_intg_7 = :t7 and tbc_intg_11 = :t11 and tbc_codigo = :tc","t11=V&t3=S&t9=S&t7=S&tc=0")->fetch(true);
        foreach ($agendas as $agenda) {
            $code[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
        }
        return implode(",",$code);
    }

    public function getComprasFiscal($type = null) {
        if ($type == 'T' ||$type == 'C' ||$type == 'D' ||$type == 'R' ||$type == 'F' ||$type == 'V' ||$type == 'T' ||$type == 'O') {
            $agendas = $this->find("TBC_INTG_3 = 'E' and TBC_INTG_4 = 'S' and TBC_INTG_11 = '$type'")->fetch(true);
        } else if ($type == 1){
            $agendas = $this->find("TBC_INTG_3 = 'E' and TBC_INTG_4 = 'S'")->fetch(true);
        } else {
            $agendas = $this->find("TBC_INTG_3 = 'E' and TBC_INTG_4 = 'S'")->fetch(true);
        }
        
        foreach ($agendas as $agenda) {
            $agendasCompras[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
            
        }
        return implode(' ,',$agendasCompras);
    }

    public function getComprasNaoFiscal($type = null) {
        $agendas = $this->find("TBC_INTG_3 = 'E' and TBC_INTG_4 = 'N' and TBC_INTG_7 = 'S'")->fetch(true);
        
        foreach ($agendas as $agenda) {
            $agendasCompras[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
            
        }
        return implode(' ,',$agendasCompras);
    }

    public function getSaidasNaoFiscal($type = null) {
        $agendas = $this->find("TBC_INTG_3 = 'S' and TBC_INTG_4 = 'N' and TBC_INTG_7 = 'S' and TBC_AGENDA != 601")->fetch(true);
        
        foreach ($agendas as $agenda) {
            $agendasCompras[$agenda->TBC_AGENDA] = $agenda->TBC_AGENDA;
            
        }
        return implode(' ,',$agendasCompras);
    }
    
}