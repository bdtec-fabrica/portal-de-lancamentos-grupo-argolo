<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;
use \Datetime;

class ConectorConference extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("conector_conference",[],"id", true);        
    }

    public function getUserName() {
        $user = (new User())->findById($this->user_id);
        return $user->name;
    }
}