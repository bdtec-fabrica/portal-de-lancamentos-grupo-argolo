<?php 

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class WhatsappSends extends DataLayer {

    public function __construct() 
    {
        #$entity, array $required, $primary, $timestamp
        parent::__construct("whatsapp_sends",[],"", false); 
    }

    public function lastSend() {
        $sends  = $this->find()->order("id")->fetch(true);
        foreach($sends as $send) {
            $lastSend[$send->name] = $send;
        }
        return $lastSend;
    }

}