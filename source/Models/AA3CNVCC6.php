<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer2;

class AA3CNVCC extends DataLayer2
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("AA3CNVCC",[],"", false);        
    }

    public function getSecao($s) {
        $secao = $this->find("NCC_SECAO = $s and NCC_GRUPO = 0 and NCC_SUBGRUPO = 0")->fetch();
        return $secao->NCC_DESCRICAO;
    }

    public function getSecao2($s) {
        $secao = $this->find("NCC_DESCRICAO =  '$s'")->fetch();
        return $secao->NCC_SECAO;
    }

    public function getGrupo($s,$g) {
        $secao = $this->find("NCC_SECAO = $s and NCC_GRUPO = $g and NCC_SUBGRUPO = 0")->fetch();
        return $secao->NCC_DESCRICAO;
    }

    public function getGrupo2($g) {
        $secao = $this->find("NCC_DESCRICAO =  '$g'")->fetch();
        return $secao->NCC_GRUPO;
    }

    public function getSubGrupo($s,$g,$sg) {
        $secao = $this->find("NCC_SECAO = $s and NCC_GRUPO = $g and NCC_SUBGRUPO = $sg")->fetch();
        return $secao->NCC_DESCRICAO;
    }

    public function getSubGrupo2($sg) {
        $secao = $this->find("NCC_DESCRICAO =  '$sg'")->fetch();
        return $secao->NCC_SUBGRUPO;
    }

    
}