<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class StoreGroup extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("stores_group",[],"id", false);        
    }
    
    public function getStores()
    {
        return (new Store())->find("store_group_id = :sid", "sid={$this->id}")->fetch(true);
    }
}