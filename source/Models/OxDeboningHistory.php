<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class OxDeboningHistory extends DataLayer
{
    public function __construct(){
        #$entity, array $required, $primary, $timestamp
        parent::__construct("ox_deboning_history",[],"id",true);        
    }

    public function getStoreId() {
        $oxHistory = (new OxHistory())->findById($this->ox_history_id);
        return $oxHistory->store_id;
    }
    
    public function getPrice(){
        $date = "1".date("ymd");
        $store = (new Store())->find("code = :code","code=$this->getStoreId")->fetch();
        
        if ($store->group_id == 1 ) {
            $aa2cprec = (new AA2CPREC())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = 'N'","pl=$this->getStoreId&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
            foreach($aa2cprec as $prec){
                $price = $prec->PRE_PRECO;
            }
            if ($price == 0){
                $price = 0.01;
             }
            return $price;
        } else if ($store->group_id == 2) {
            $aa2cprec = (new AA2CPREC2())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = 'N'","pl=$this->getStoreId&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
            
            foreach($aa2cprec as $prec){
                $price = $prec->PRE_PRECO;
            }
            if ($price == 0){
                $price = 0.01;
             }
            return $price;
        } else if ($store->group_id == 10){
            $aa2cprec = (new AA2CPREC10())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = 'N'","pl=$this->getStoreId&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
            foreach($aa2cprec as $prec){
                $price = $prec->PRE_PRECO;
            }
            if ($price == 0){
                $price = 0.01;
             }
            return $price;
        } else {
            // $storeId = substr($this->getStoreId,0,strlen($this->getStoreId)-1);
            // $aa2cprec = (new AA2CPREC3())->find("PRE_COD_OFERTA = :pco and PRE_LOJA = :pl and PRE_COD_ITEM = :pci and PRE_DAT_INICIO <= $date and PRE_TIPO = 'N'","pl=$storeId&pci=$this->code&pco=0")->order("PRE_DAT_INICIO ASC")->fetch(true);
            // foreach($aa2cprec as $prec){
            //     $price = $prec->PRE_PRECO;
            // }
            // if ($price == 0){
            //     $price = 0.01;
            //  }
            return $this->price;
        }
    }
}