<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\StoreContacts;
use Source\Models\UserClass;
use Monolog\Logger;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use Source\Models\Daily;
use Source\Models\CheckIn;
use Source\Models\LockParameter;
use Source\Models\Parameter;
use Source\Models\ParameterDayWeek;
use Source\Models\ParameterOperator;
use Source\Models\ParameterWeek;
use Source\Models\ParameterWorkload;

class Admin 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    /**
     * visão do painel de usuarios - GET
     */
    public function users($data) {
        
        
        ////////////////////////////////////////////

        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");

        } else {
            
            $identification = $_COOKIE['login'];
            $user = (new User())->find("email = :login","login=$identification")->fetch();
            $class = $user->class;
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $parameterWeek = (new ParameterWeek())->find()->fetch(true);
            $parameterDayWeek = (new ParameterDayWeek())->find()->fetch(true);
            $parameterWorkload = new ParameterWorkload();
            $parameters = (new Parameter())->find()->fetch(true);
            $para = new Parameter();
            if ($class == 'A') {

                if ($data['id']) {
                    $id = $data['id'];
                    $user = new User();
                    $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
                    $paginator = new Paginator(URL_BASE."/usuarios/{$id}?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
                    $paginator->pager($user->find("id in ($id)")->count(), 10, $page, 2);
                    $users = $user->find("id in ($id)")->limit($paginator->limit())->offset($paginator->offset())->order("name ASC")->fetch(true);
                    $paginatorRender = $paginator->render();
                    $userClass = (new UserClass())->find()->fetch(true);
                    echo $this->view->render("administrator/users", ["title" => "Ususarios | " . SITE,"para" => $para, "parameters" => $parameters,"parameterWorkload" => $parameterWorkload, "parameterDayWeek" => $parameterDayWeek, "parameterWeek" => $parameterWeek, "users" => $users, "paginator" => $paginator, "userClass" => $userClass, "storeGroups" => $storeGroups]);;
                } else {
                    $user = new User();
                    $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
                    $paginator = new Paginator(URL_BASE."/usuarios?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
                    $paginator->pager($user->find()->count(), 10, $page, 2);
                    $users = $user->find()->limit($paginator->limit())->offset($paginator->offset())->order("name ASC")->fetch(true);
                    $paginatorRender = $paginator->render();
                    $userClass = (new UserClass())->find()->fetch(true);
                    echo $this->view->render("administrator/users", ["title" => "Ususarios | " . SITE,"para" => $para,"parameters" => $parameters, "parameterWorkload" => $parameterWorkload,"parameterDayWeek" => $parameterDayWeek, "parameterWeek" => $parameterWeek, "users" => $users, "paginator" => $paginator, "userClass" => $userClass, "storeGroups" => $storeGroups]);
                }
            } else {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-1001299426191";
                // $tele_channel = "@BDTecAtende";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                $logger->pushHandler($tele_handler);
                $logger->info("$user->name Tentou acessar o painel de usúarios.");
                echo $this->view->render("administrator/users", ["title" => "Ususarios | " . SITE, ]);  
            }
        }
    }

    /**
     * procurar usuarios - POST
     */
    public function usersSearch() {
        $store = $_POST['store'];
        $group = $_POST['group'];
        $class = $_POST['class'];

        if ($store !='') {
            $params['store_code'] = $store; 
            $comparation[1] = " store_code = :store_code";
        }
        if ($group !='') {
            $params['store_group_id'] = $group; 
            $comparation[2] = " store_group_id = :store_group_id";
        }
        if ($class !='') {
            $params['class'] = $class; 
            $comparation[3] = " class = :class";
        }
        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);
        $users = (new User())->find($comparation,$params)->fetch(true);

        // foreach ($users as $user) {
        //     $arrayUsers[$user->id] = (array) $user->data;
        // }

        // echodie(json_encode($arrayUsers));
        if ($users) {
            foreach ($users as $user) {
                $id[$user->id] = $user->id;
            }
            $id = implode(",",$id);
        } else {
            $id = '-1';
        }
        

        $this->router->redirect("/usuarios/{$id}");

    }

    /**
     * adicionar um usuario - POST
     */
    public function usersAdd() {

            $storeId = $_POST['store'];
            $tell = $_POST['tel'];
            $login = $_POST['login'];
            $name = $_POST['name'];
            $class = $_POST['class'];
            $password = $_POST['password'];
            $storeGroup = $_POST['storeGroup'];
            $user = new User();
            $count = $user->find("email = :name", "name=$login")->count();
            if (strlen($password) < 6) {
                $return['success'] = false;
                $return['message'] = 'Defina uma senha com mais de 6 digitos';
                echo json_encode($return);
                return;
            }
            if ($storeId == '') {
                $return['success'] = false;
                $return['message'] = 'Informe a loja.';
                echo json_encode($return);
                return;
            }
            if ($count == 1) {
                $return['success'] = false;
                $return['message'] = 'Já existe um Usúario com este login.';
                echo json_encode($return);
                return;
            } else {
                $user->name = $name;
                $user->email = $login;
                $user->password = $password;
                $user->store_code = $storeId;
                $user->store_group_id = $storeGroup;
                $user->tell = $tell;
                $user->class = 'B';
                
                $userSave = $user->save();
                $numRow = (new Store())->findById($storeId);
                if ($numRow == 0) {
                    $store = new Store();
                    $store->code = $storeId;
                    $store->store_group_id = $storeGroup;
                    $store->save();
                }
                if ($userSave) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-674916838";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Usúario $login Loja $storeId entrar  portal para liberar.");
                    $return['success'] = true;
                    $return['message'] = 'Usúario cadastrado.';
                    echo json_encode($return);
                    return;
                } else {
                    $return['success'] = false;
                    $return['message'] = 'Problemas ao cadastrar usuario no banco de dados.';
                    echo json_encode($return);
                }
                
            }
    }

    /**
     * alterar a senha do usuario - POST
     */
    public function usersUpdatePassword() {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            
            $current = $_POST['current-password'];
            $new = $_POST['new-password'];
            $repeat = $_POST['repeat-new-password'];
            $id = $_POST['id'];
            if ($repeat == $new) {
                $user = (new User())->findById($id);
                $user->password = $new;
                $userId = $user->save();
                if ($userId == 1) {
                    $return['success'] = true;
                    $return['message'] = 'Senha Alterada com Sucesso!';
                    echo json_encode($return);
                    return;
                } else {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-1001299426191";
                    // $tele_channel = "@BDTecAtende";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                    $logger->pushHandler($tele_handler);
                    $email = $_COOKIE['login'];
                    $logger->info("Usúario $email não conseguiu alterar a senha do usúario $user->name.");
                    $return['success'] = false;
                    $return['message'] = 'Erro no processo!';
                    echo json_encode($return);
                    return;
                }

            } else {
                $return['success'] = false;
                $return['message'] = 'Nova senha diferente do campo repetir senha.';
                echo json_encode($return);
                return;
            }
        }
    }

    /**
     * editar dados do usuario - POST
     */
    public function userEdit(){
        
        $name = $_POST['name'];
        $email = $_POST['email'];
        $storeCode = $_POST['storeCode'];
        $group = $_POST['group'];
        $class = $_POST['class'];
        $id = $_POST['id'];
        $tell = $_POST['tel'];
        $admissao = $_POST['admissao'];
        $libera = $_POST['libera'];
        if (!isset($_POST['whatsapp'])) {
            $whatsapp = "F";
        } else {
            $whatsapp = "T";
        }
        $user = (new User())->findById($id);
        $userId = $user->userEdit($name,$email,$storeCode,$class,$id,$group,$tell,$whatsapp,$admissao,$libera);
        if ($userId == true) {
            $return['success'] = true;
            $return['message'] = 'Alteração realizada com sucesso!';
            echo json_encode($return);
            return;
        } else {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = "-1001299426191";
            // $tele_channel = "@BDTecAtende";
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
            $logger->pushHandler($tele_handler);
            $email = $_COOKIE['login'];
            $logger->info("Usúario $email não conseguiu alterar dados do usúario $user->name.");

            $return['success'] = false;
            $return['message'] = 'Alteração não realizada';
            echo json_encode($return);
            return;
        }
    }

    /**
     * parametrizar grupos economicos dos operadores - POST
     */
    public function storeGroupsParameters() {
        $id = $_POST['id'];
        $validate = true;
        $storeGroups = (new StoreGroup())->find()->fetch(true);
        foreach ($storeGroups as $storeGroup) {
            if ($_POST[$storeGroup->id] == 1) {
                $userStoreGroup = (new UserStoreGroup());
                $count = $userStoreGroup->find("user_id = :id and store_group_id = :store_group_id","id={$id}&store_group_id={$storeGroup->id}")->count();
                if ($count==0) {
                    $userStoreGroup = (new UserStoreGroup());
                    $userStoreGroup->user_id = $id;
                    $userStoreGroup->store_group_id = $storeGroup->id;
                    $userStoreGroup = $userStoreGroup->save();
                    if (!$userStoreGroup) {
                        $validate = false;
                        $msg = $msg."$id ativo $storeGroup->id";
                    }
                }
            } else {
                $userStoreGroup = (new UserStoreGroup());
                $find = $userStoreGroup->find("user_id = :id and store_group_id = :store_group_id","id={$id}&store_group_id={$storeGroup->id}");
                $count = $find->count();
                if ($count==1) {
                    $storeGroupfind = $find->fetch();
                    $storeGroupId = $storeGroupfind->id;
                    $userStoreGroupDestroy = $userStoreGroup->findById($storeGroupId);
                    $userStoreGroupDestroy = $userStoreGroupDestroy->destroy();
                    if (!$userStoreGroupDestroy) {
                        $validate = false;
                        $msg = $msg."inativo";
                    }
                }
            }
        }
        if ($validate == true) {
            $data['success'] = true;
            $data['msg'] = "Parametros Ajustados!";
            echo json_encode($data);
        } else {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = "-1001299426191";
            // $tele_channel = "@BDTecAtende";
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
            $logger->pushHandler($tele_handler);
            $email = $_COOKIE['login'];
            $logger->info("Usúario $email não conseguiu parametrizar grupos economicos do usúario $user->name.");

            $data['success'] = false;
            $data['msg'] = "Falha ao ajustar parametros: $msg!";
            echo json_encode($data);
        }
    }

    public function stores($data) {
        
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");

        } else {
            
            $identification = $_COOKIE['login'];
            $user = (new User())->find("email = :login","login=$identification")->fetch();
            $class = $user->class;
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            if ($class == 'A') {

                if ($data['id']) {
                    $id = $data['id'];
                    $store = new Store();
                    $stores = $store->find("id in ($id)")->order("company_name ASC")->fetch(true);
                    echo $this->view->render("administrator/stores", ["title" => "Lojas | " . SITE, "stores" => $stores]);
                } else {
                    $store = new Store();
                    $stores = $store->find()->order("code ASC")->fetch(true);
                    $storeGroup = (new StoreGroup())->find()->fetch(true);
                  
                    echo $this->view->render("administrator/stores", ["title" => "Lojas | " . SITE, "stores" => $stores, "storeGroup" => $storeGroup]);
                }
            }
        }
    }

    public function storesSearch() {

        $store = $_POST['store'];

        if ($store !='') {
            $params['store_code'] = $store; 
            $comparation[1] = " store_code = :store_code";
        }
        if ($group !='') {
            $params['store_group_id'] = $group; 
            $comparation[2] = " store_group_id = :store_group_id";
        }

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);
        $stores = (new Store())->find($comparation,$params)->fetch(true);
        if ($stores) {
            foreach ($stores as $store) {
                $id[$store->id] = $store->id;
            }
            $id = implode(",",$id);
        } else {
            $id = '-1';
        }
        

        $this->router->redirect("/lojas/{$id}");

    }

    public function storeContacts() {
        $store = new Store();
        $stores = $store->find()->order("code ASC")->fetch(true);
        echo $this->view->render("administrator/storeContacts", ["title" => "Lojas | " . SITE, "stores" => $stores]);
    }

    public function storeContactsAdd() {
        $storeContacts = new StoreContacts();
        $storeContacts->store_id = $_POST['store'];
        $storeContacts->name = $_POST['name'];
        $storeContacts->phone = $_POST['phone'];
        $storeContacts->type = $_POST['type'];
        $storeContactsId = $storeContacts->save();
        $data['sucesso'] = true;
        $data['msg'] = "Registro Incluido.";
        $data['id'] = $storeContacts->id;
        echo json_encode($data);
    }
    
    public function storeContactsDelete() {
        $storeContacts = (new StoreContacts())->findById($_POST['id']);
        $storeContacts->destroy();
        $data['sucesso'] = true;
        $data['msg'] = "Registro Excluido.";
        echo json_encode($data);
    }

    public function storeContactsUpdate() {
        $storeContacts = (new StoreContacts())->findById($_POST['id']);
        $storeContacts->phone = $_POST['phone'];
        $storeContacts->name = $_POST['name'];
        $storeContacts->type = $_POST['type'];
        $storeContactsId = $storeContacts->save();
        $data['sucesso'] = true;
        $data['msg'] = "Registro Excluido.";
        echo json_encode($data);
    }

    public function updateCert() {
        $password = $_POST['password'];
        $loja = $_POST['store'];
        $files = $_FILES;

        $store = (new Store())->find("code = :code","code=$loja")->fetch();
        $certName = $store->getCertName;
        
        if(!empty($files['file'])) {
            $file = $files['file'];

            if(empty($file['type'])) {
                $data['sucesso'] = false;
                $data['msg'] = "Arquivo no formato invalido".$file['type'];
                echo json_encode($data);
            } else {
                $move = move_uploaded_file($_FILES['file']['tmp_name'],"source/Certs/$certName");
                if($move){
                    //abrindo o json externo
                    $json = json_decode(file_get_contents(__DIR__."/../StoreConfig/$loja.json"));
                    
                    //Editando a linha que vc quer
                    $json->certPassword = "$password";
                    
                    //Salvando as edições
                    $json_editado = file_put_contents(__DIR__."/../StoreConfig/$loja.json",json_encode($json));
                    
                    $data['sucesso'] = true;
                    $data['msg'] = "Certificado Atualizado.";
                    echo json_encode($data);
                }
            }

        } else {
            $data['sucesso'] = false;
            $data['msg'] = "Informe um arquivo.";
            echo json_encode($data);
        }
    }

    public function updateStore() {
        $id = $_POST['id'];
        $name = $_POST['name'];
        $password = $_POST['password'];

        $store = (new Store())->findById($id);
        $code = $store->code;
        $store->company_name = $name;
        $oldPassword = $store->getCertPassword;
        $storeId = $store->save();
        if ($oldPassword != $password) {
            $json = json_decode(file_get_contents(__DIR__."/../StoreConfig/$code.json"));
                    
            //Editando a linha que vc quer
            $json->certPassword = "$password";
                  
            //Salvando as edições
            $json_editado = file_put_contents(__DIR__."/../StoreConfig/$code.json",json_encode($json));
                   
            $data['success'] = true;
            $data['message'] = "Dados Atualizados.";
            echo json_encode($data);
        }

    }

    public function addCheckIn() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $date = date("Y-m-d");
        $dailyCount = (new Daily())->find("substr(created_at,1,10) = :date and user_id = :uid", "date=$date&uid=$user->id")->count();
        if ($dailyCount == 0 ) {
            $daily = new Daily();
            $daily->user_id = $user->id;
            $dailyId = $daily->save();

            $checkIn = new CheckIn();
            $checkIn->daily_id = $daily->id;
            $checkInId = $checkIn->save();
            echo "<script>window.alert('Batimento Realizado');</script>";
            echo "<script>window.close()</script>";
        } else {
            $daily = (new Daily())->find("substr(created_at,1,10) = :date and user_id = :uid", "date=$date&uid=$user->id")->fetch();
            if ((new CheckIn())->find("daily_id = $daily->id and substr(created_at,1,13) = '".date("Y-m-d H")."'")->count() == 0) {
                $checkInCount = (new CheckIn())->find("daily_id = $daily->id")->count();
                if (!is_int($checkInCount/2)) {
                    $operador = (new User())->findById($user->id);
                    $operador->status = 2;
                    $operadorId = $operador->save();
                    $checkIn = new CheckIn();
                    $checkIn->daily_id = $daily->id;
                    $checkInId = $checkIn->save();
                    echo "<script>window.alert('Batimento Realizado');</script>";
                    echo "<script>window.close()</script>";
                } else {
                    $operador = (new User())->findById($user->id);
                    $operador->status = 1;
                    $operadorId = $operador->save();
                    $checkIn = new CheckIn();
                    $checkIn->daily_id = $daily->id;
                    $checkInId = $checkIn->save();
                    echo "<script>window.alert('Batimento Realizado');</script>";
                    echo "<script>window.close()</script>";
                }
                
            } else {
                echo "<script>window.alert('Batimento Já realizado!');</script>";
                echo "<script>window.close()</script>";
            }            
        }

    }

    public function viewCheckIns(){
        $operators = (new User())->find("class = :class","class=A")->fetch(true);
        $dailies = (new Daily())->find("substr(created_at,1,10) = CURDATE()")->fetch(true);
        echo $this->view->render("administrator/viewCheckIns", ["title" => "Lojas | " . SITE, "dailies" => $dailies,"operators" => $operators]);
        
    }

    public function reportCheckIns(){
        // $operators = (new User())->find("class = :class","class=A")->fetch(true);
        $date1 = $_POST['day1'];
        $date2 = $_POST['day2'];
        if ($_POST['operator']) {
            $operator = " and user_id = '".$_POST['operator']."'";
        }
        $dailies = (new Daily())->find("substr(created_at,1,10) between '$date1' and '$date2' $operator")->order('created_at ASC')->fetch(true);
        foreach($dailies as $daily) {
            $operators[$daily->getUser()->name] = $daily->getUser()->name;
            $dailyOperators[$daily->getUser()->name][$daily->id] = $daily;
        }
        echo $this->view->render("administrator/reportCheckIns", ["title" => "Relatorio Batidas de Ponto | " . SITE, "operators" => $operators, "dailyOperators" => $dailyOperators
        ]);
        
    }
    
    public function searchViewCheckIns(){
        $date = $_POST['day'];
        if ($_POST['operator']) {
            $operator = " and user_id = '".$_POST['operator']."'";
        }
        
        $operators = (new User())->find("class = :class","class=A")->fetch(true);
        $dailies = (new Daily())->find("substr(created_at,1,10) = '$date' $operator")->fetch(true);
        echo $this->view->render("administrator/viewCheckIns", ["title" => "Lojas | " . SITE, "dailies" => $dailies,"operators" => $operators]);
        
    }

    public function updateParameterWeek(){
        $week = $_POST["week"];
        $user = $_POST['user'];
        foreach (array(1,2,3,4,5,6,7) as $i) {
            $horaInical = $_POST["horaInical$i$week$user"];
            $intervaloInicial = $_POST["intervaloInicial$i$week$user"];
            $intervaloFinal = $_POST["intervaloFinal$i$week$user"];
            $horaFinal = $_POST["horaFinal$i$week$user"];
            $id = (new ParameterWorkload())->find("user_id = $user and parameter_week_id = $week and parameter_day_week_id = $i")->fetch();
            $parameterWorkload = (new ParameterWorkload())->findById($id->id);
            $parameterWorkload->start_time = $horaInical;
            $parameterWorkload->start_break = $intervaloInicial;
            $parameterWorkload->end_break = $intervaloFinal;
            $parameterWorkload->end_time = $horaFinal;
            $parameterWorkloadId = $parameterWorkload->save();

        }
        $data['success'] = true;
        $data['message'] = "Dados Atualizados.";
        echo json_encode($data);
    }

    public function addParameter(){
        $user = $_POST['user'];
        $parameter = new ParameterOperator();
        $parameter->user_id = $user;
        $parameter->parameter_id = $_POST['parametro'];
        $parameter->content = $_POST['content'];       
        $prameterId = $parameter->save();
        $data['success'] = true;
        $data['message'] = "Dados Incluidos.";
        echo json_encode($data);
    }

    public function deleteParameter(){
        $id = $_POST['id'];
        $parameter = new ParameterOperator();
        $parameter = $parameter->findById($id);      
        $prameterId = $parameter->destroy();
        $data['success'] = true;
        $data['message'] = "Dados Excluidos.";
        echo json_encode($data);
    }

    public function lockParameter() {
        echo $this->view->render("administrator/lockParameter", ["title" => "Parametros de bloqueio | " . SITE, ]);  
    }

    public function createLockParameter() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $id = $_COOKIE['id'];
        $value = $_POST['value'];
        $type =$_POST['type'];
        $lockParameter = new LockParameter();
        $lockParameter->parameter_id = $value;
        $lockParameter->type  = $type;
        $lockParameter->note = "Incluido por $user->id - $user->name";
        $lockParameterId = $lockParameter->save();
        
        if (!$lockParameterId) {
            $data['sucesso'] = false;
            $data['msg'] = "Falha ao gravar novo parâmetro.";
            echo json_encode($data);
        } else {
            $data['sucesso'] = true;
            $data['msg'] = "Parâmetro salvo com sucesso.";
            echo json_encode($data);
        }
    }

    public function readLockParameter() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $id = $_COOKIE['id'];
        if (is_numeric($_POST['value'])) {
            $value = $_POST['value'];
            $array['value'] = " parameter_id = $value ";
        }
        if (is_string($_POST['type']) && strlen($_POST['type']) == 1) {
            $type = $_POST['type'];
            $array['type'] = "type = '$type' ";
        }
        if (isset($array)) {
            $array = implode(' and ', $array); 
        }
        
        $lockParameter = (new LockParameter())->find($array)->fetch(true);

        echo $this->view->render("administrator/lockParameter", ["title" => "Parametros de bloqueio | " . SITE,"rows" => $lockParameter, "read" => true ]);
        
    }

    public function updateLockParameter() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $id = $_COOKIE['id'];
        $lockParameterId = $_POST['id'];
        $value = $_POST['value'];
        $type =$_POST['type'];
        $lockParameter = (new LockParameter())->findById($lockParameterId);
        $lockParameter->parameter_id = $value;
        $lockParameter->type  = $type;
        $lockParameter->note = "Alterado por $user->id - $user->name";
        $lockParameterUpdate = $lockParameter->save();
        
        if ($lockParameterUpdate) {
            $data['sucesso'] = false;
            $data['msg'] = "Falha ao alterar parâmetro.";
            echo json_encode($data);
        } else {
            $data['sucesso'] = true;
            $data['msg'] = "Parâmetro alterado com sucesso.";
            echo json_encode($data);
        }
    }

    public function deleteLockParameter() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $id = $_COOKIE['id'];
        $lockParameterId = $_POST['id'];
        $lockParameter = (new LockParameter())->findById($lockParameterId);
        $lockParameterDestroy = $lockParameter->destroy();
        
        if (!$lockParameterDestroy) {
            $data['sucesso'] = false;
            $data['msg'] = "Falha ao deletar parâmetro.";
            echo json_encode($data);
        } else {
            $data['sucesso'] = true;
            $data['msg'] = "Parâmetro deletado com sucesso.";
            echo json_encode($data);
        }
    }
 }