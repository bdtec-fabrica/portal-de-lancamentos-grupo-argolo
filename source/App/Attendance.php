<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\RegistroCadastro;
use Source\Models\UserStoreGroup;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\AA1AGEDT;
use Source\Models\AA2CTIPO2;
use Source\Models\AA3CCEAN2;
use Source\Models\AA1FORIT2;
use Source\Models\AA1REFIT2;
use Source\Models\AA1AGEDT2;
use Source\Models\AG2DETNT2;
use Source\Models\NfRms;
use Source\Models\NfType;
use Source\Models\AA2CTIPO;
use Source\Models\AA3CCEAN;
use Source\Models\AA1REFIT;
use Source\Models\AG2DETNT;
use Source\Models\Parameter;
use Source\Models\Desbloqueio;
use Monolog\Logger;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use Source\Models\AA1AGEDT10;
use Source\Models\NfReceipt;
use Source\Models\NfReceipt2;
use Source\Models\NfStatus;
use Source\Models\NfAttachment;
use Source\Models\NfDueDate;
use Source\Models\NfOperation;
use Source\Models\NfReport;
use Source\Models\NfSefaz;
use Source\Models\ExchangeCode;
use Source\Models\NfEvent;
use Source\Models\NfStarted;
use Source\Models\RegisterAmountItems;
use Source\Models\ReasonPendingClient;
use Source\Models\AA1FORIT;
use Source\Models\AA1FORIT10;
use Source\Models\AA1REFIT10;
use Source\Models\AA2CESTQ;
use Source\Models\AA2CESTQ2;
use Source\Models\AA2CTIPO10;
use Source\Models\AA3CCEAN10;
use Source\Models\AA3CITEM;
use Source\Models\AA3CITEM10;
use Source\Models\AA3CITEM2;
use Source\Models\ExchangePackaging;
use Source\Models\BoningOx;
use Source\Models\AuxiliarCancelamento;
use Source\Models\CodigoInterno;
use Source\Models\CodigoInternoConsinco;
use Source\Models\NfReceiptCodes;

class Attendance 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function request($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $storeGroup = $usuario->store_group_id;
            $storiesCodes = $usuario->getStoreGroup2();
            $requests = new NfReceipt();
            $users = (new User())->find("store_group_id = :sgi","sgi=$storeGroup")->fetch(true);
            $i = 0;
            foreach ($users as $user) {
                $userId[$i] = $user->id;
                $i++;
            }
            $userId = implode(",",$userId);
            // $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
            if ($class == "C"){
                
                // $userToastes = (new User())->find("store_group_id = :store_group_id","store_group_id=$storeGroup")->fetch(true);
                // $t = 0;
                // foreach ($userToastes as $userToaste) {
                //     $idToaste[$t] = $userToaste->id;
                //     $t++;
                // }
                // $idToast = implode(",",$idToaste);
                // $requestsToastes = $requests->find("store_id = $store and status_id = 3 and client_id in ($idToast)")->fetch(true);
                // foreach ($requestsToastes as $requestToaste) {
                //     $requestScore = (new RequestScore())->find("request_id = :request_id","request_id=$requestToaste->id")->count();
                //     if ($requestScore == 0){
                //         $toaste[$requestToaste->id] = $requestToaste;
                //     }
                // }
            }
            if ($data['id']) { //redirecionamento da requestSearch()
                $id = $data['id'];
                // $paginator = new Paginator(URL_BASE."/request/{$id}?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
                
                if ($class == "C") {
                    // $paginator->pager($requests->find("id in ($id) and store_id = $store and client_id in ($userId)")->count(), 8, $page, 2);
                    $requests = $requests->find("id in ($id)  and store_id = $store and client_id in ($userId)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if($class == "A") {
                    // $paginator->pager($requests->find("id in ($id)")->count(), 8, $page, 2);
                    $requests = $requests->find("id in ($id)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($class == "C2") {
                    // $paginator->pager($requests->find("id in ($id) and store_id = $store and client_id in ($userId)")->count(), 8, $page, 2);
                    if ($usuario->store_code == 299|| $usuario->store_code == 531 || $usuario->store_code == 450) {
                        $requests = (new NfReceipt2())->find("id in ($id)  and store_id = $store and client_id in ($userId)")->order("id ASC")->fetch(true);
                    } else {
                        $requests = $requests->find("id in ($id)  and store_id = $store and client_id in ($userId)")->order("id ASC")->fetch(true);
                    }

                    
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($class == "OG") {
                    // $paginator->pager($requests->find("id in ($id) and store_id = $store and client_id in ($userId)")->count(), 8, $page, 2);
                    if ($usuario->store_code == 299 || $usuario->store_code == 531 || $usuario->store_code == 450) {
                        $requests = (new NfReceipt2())->find("id in ($id)  and store_id in ($storiesCodes) and client_id in ($userId)")->order("id ASC")->fetch(true);
                    } else {
                        $requests = $requests->find("id in ($id)  and store_id in ($storiesCodes) and client_id in ($userId)")->order("id ASC")->fetch(true);
                    }

                    
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else {
                    //store group
                    if (is_array($usuario->getStoreGroup())){
                        foreach ($usuario->getStoreGroup() as $storeGroup) {
                            //meus store groups
                            $stores = (new Store())->find("store_group_id = :id","id={$storeGroup->store_group_id}")->fetch(true);
                            if (is_array($stores)){
                                foreach ($stores as $store) {
                                    $storeArray[$store->code] = $store->code;
                                }
                            }
                        } 
                        if (is_array($storeArray)) {
                            $store = implode(", ",$storeArray);                            
                        } else {
                            $store = 0;
                        }
                         
                    }else{
                        $store = 0;
                    }
    
                    // $paginator->pager($requests->find("id in ($id) and store_id in ($store)")->count(), 8, $page, 2);
                    $requests = $requests->find("id in ($id) and store_id in ($store)")->order("id ASC")->fetch(true); 
                    // ->limit($paginator->limit())->offset($paginator->offset())                   
                }
            }else {//entrada direta
                // $paginator = new Paginator(URL_BASE."/request?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);               
                if ($class == "C") {
                    // $paginator->pager($requests->find("store_id = $store and client_id in ($userId)")->count(), 8, $page, 2);
                    // $requests = $requests->find("store_id = $store and client_id in ($userId)")->limit(20)->order("field(status_id,2,1,4,3) ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($class == "A") {
                    // $paginator->pager($requests->find("status_id in (2,1,4)")->count(), 8, $page, 2);
                    // $requests = $requests->find("status_id in (2,1,4)")->order("field(status_id,1,4,2) ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else {
                    //store group
                    // if (is_array($usuario->getStoreGroup())){
                    //     foreach ($usuario->getStoreGroup() as $storeGroup) {
                    //         //meus store groups
                    //         $stores = (new Store())->find("store_group_id = :id","id={$storeGroup->store_group_id}")->fetch(true); 
                    //         if (is_array($stores)){
                    //             foreach ($stores as $store) {
                    //                 $storeArray[$store->code] = $store->code;
                    //             }
                    //         }
                    //     }                         
                    //     if (is_array($storeArray)) {
                    //         $store = implode(", ",$storeArray);                            
                    //     } else {
                    //         $store = 0;
                    //     }
                    // }else{
                    //     $store = 0;
                    // }
                    
                    // $paginator->pager($requests->find("status_id in (2,1,4) and store_id in ($store)")->count(), 8, $page, 2);
                    // $requests = $requests->find("status_id in (2,1,4) and store_id in ($store)")->order("field(status_id,1,4,2) ASC")->fetch(true);    
                    // ->limit($paginator->limit())->offset($paginator->offset())             
                }
                
            }
            
            // $paginatorRender = $paginator->render();
            $users = new User();
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);
            if ($class == 'A') {
                foreach($requests as $request) {
                    $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                    $content[$request->id]['error_cost'] = $request->error_cost;
                    $content[$request->id]['id'] = $request->id;
                    $content[$request->id]['store_id'] = $request->store_id;
                    $content[$request->id]['nf_number'] = $request->nf_number;
                    $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                    $content[$request->id]['getClient'] = $request->getClient;
                    $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                    $content[$request->id]['getType'] = $request->getType;
                    $content[$request->id]['clientGroup'] = $request->clientGroup;
                    $content[$request->id]['items_quantity'] = $request->items_quantity;
                    $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                    $content[$request->id]['getOperator'] = $request->getOperator;
                    $content[$request->id]['operator'] = $request->operator_id;
                    $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                    $content[$request->id]['getOperation'] = $request->getOperation;
                    $content[$request->id]['operation'] = $request->operation_id;
                    $content[$request->id]['status_id'] = $request->status_id;
                    $content[$request->id]['getStatus'] = $request->getStatus;
                    // $content[$request->id]['AG2DETNT2'] = $request->AG2DETNT2;
                }
                $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            } else if($class == 'O') {
                echo $this->view->render("operator/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation             
                ]);
            } else if($class == 'C') {
                echo $this->view->render("client/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            } else if($class == 'OG' || $class == 'OG2') {
                $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
                    
                foreach ($userStoreGroup as $userGroups) {
                    $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
                }
                if (is_array($groups)){     
                    $groups = implode(", ",$groups);
                    $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                }
                echo $this->view->render("operator/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                "groups" => $groups,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            }   else if ($class == 'C2') {
                $status = (new NfStatus())->find("id >= :id","id=10")->fetch(true);
                echo $this->view->render("client/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            }
            
        }
    }

    public function request2($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            if ($data['id']) {
                $id = $data['id'];
                $requests = (new NfReceipt())->find("id in ($id)")->order("id ASC")->fetch(true);
            } else {
                $requests = (new NfReceipt())->find("status_id = :status_id","status_id=1")->order("id ASC")->fetch(true);
            }
            
            foreach($requests as $request) {
                
                $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                $content[$request->id]['error_cost'] = $request->error_cost;
                $content[$request->id]['id'] = $request->id;
                $content[$request->id]['store_id'] = $request->store_id;
                $content[$request->id]['nf_number'] = $request->nf_number;
                $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                $content[$request->id]['getClient'] = $request->getClient;
                $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                $content[$request->id]['getType'] = $request->getType;
                $content[$request->id]['clientGroup'] = $request->clientGroup;
                $content[$request->id]['items_quantity'] = $request->items_quantity;
                $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                $content[$request->id]['getOperator'] = $request->getOperator;
                $content[$request->id]['operator'] = $request->operator_id;
                $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                $content[$request->id]['getOperation'] = $request->getOperation;
                $content[$request->id]['operation'] = $request->operation_id;
                $content[$request->id]['status_id'] = $request->status_id;
                $content[$request->id]['getStatus'] = $request->getStatus;
                $content[$request->id]['codigoInternoCD'] = $request->codigoInternoCD();
                // $content[$request->id]['AG2DETNT2'] = $request->AG2DETNT2;
            }
            $content = json_encode($content);

            $users = new User();
            $users = $users->find("class = :class","class=A")->fetch(true);
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);
            echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
            "requests" => $content,
            "status" => $status,
            "storeGroups" => $storeGroups,
            "nfeType" => $nfeType,
            "nfeOperation" => $nfeOperation
            ]);           
        }
    }

    public function requestPendente($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $operation = $data['operation'];
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $storiesCodes = $usuario->getStoreGroup2();
            $storeGroup = $usuario->store_group_id;
            $requests = new NfReceipt();
            $users = (new User())->find("store_group_id = :sgi","sgi=$storeGroup")->fetch(true);
            $i = 0;
            foreach ($users as $user) {
                $userId[$i] = $user->id;
                $i++;
            }
            $userId = implode(",",$userId);
            $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
            if ($class == "C"){
                
                // $userToastes = (new User())->find("store_group_id = :store_group_id","store_group_id=$storeGroup")->fetch(true);
                // $t = 0;
                // foreach ($userToastes as $userToaste) {
                //     $idToaste[$t] = $userToaste->id;
                //     $t++;
                // }
                // $idToast = implode(",",$idToaste);
                // $requestsToastes = $requests->find("store_id = $store and status_id = 3 and client_id in ($idToast)")->fetch(true);
                // foreach ($requestsToastes as $requestToaste) {
                //     $requestScore = (new RequestScore())->find("request_id = :request_id","request_id=$requestToaste->id")->count();
                //     if ($requestScore == 0){
                //         $toaste[$requestToaste->id] = $requestToaste;
                //     }
                // }
            }
            //entrada direta
             /** MUDAR ISSO AQUI
         * */$paginator = new Paginator(URL_BASE."/request/pendente/{$operation}?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);               
             if ($class == "C") {
                $paginator->pager($requests->find("store_id = $store and client_id in ($userId) and status_id = 1 and operation_id = $operation")->count(), 8, $page, 2);
                $requests = $requests->find("store_id = $store and client_id in ($userId) and status_id = 1 and operation_id = $operation")->limit($paginator->limit())->offset($paginator->offset())->order("field(status_id,2,1,4,3) ASC")->fetch(true);
            } else if ($class == "A") {
                // $paginator->pager($requests->find("status_id = 1 and operation_id = $operation")->count(), 8, $page, 2);
                $requests = $requests->find("status_id = 1 and operation_id = $operation")->order("field(status_id,1,4,2) ASC")->fetch(true);
                // ->limit($paginator->limit())->offset($paginator->offset())
            }                       
            $paginatorRender = $paginator->render();
            $users = new User();
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);
            if ($class == 'A') {
                foreach($requests as $request) {
                    $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                    $content[$request->id]['error_cost'] = $request->error_cost;
                    $content[$request->id]['id'] = $request->id;
                    $content[$request->id]['store_id'] = $request->store_id;
                    $content[$request->id]['nf_number'] = $request->nf_number;
                    $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                    $content[$request->id]['getClient'] = $request->getClient;
                    $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                    $content[$request->id]['getType'] = $request->getType;
                    $content[$request->id]['clientGroup'] = $request->clientGroup;
                    $content[$request->id]['items_quantity'] = $request->items_quantity;
                    $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                    $content[$request->id]['getOperator'] = $request->getOperator;
                    $content[$request->id]['operator'] = $request->operator_id;
                    $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                    $content[$request->id]['getOperation'] = $request->getOperation;
                    $content[$request->id]['operation'] = $request->operation_id;
                    $content[$request->id]['status_id'] = $request->status_id;
                    $content[$request->id]['getStatus'] = $request->getStatus;
                    // $content[$request->id]['AG2DETNT2'] = $request->AG2DETNT2;
                }
                $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            } else if($class == 'O') {
                echo $this->view->render("operator/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation             
                ]);
            } else if($class == 'C') {
                echo $this->view->render("client/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            }
            
        }
    }

    public function requestAtualizada($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $operation = $data['operation'];
            $date = date("Y-m-d");
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $storeGroup = $usuario->store_group_id;
            $requests = new NfReceipt();
            $users = (new User())->find("store_group_id = :sgi","sgi=$storeGroup")->fetch(true);
            $i = 0;
            foreach ($users as $user) {
                $userId[$i] = $user->id;
                $i++;
            }
            $userId = implode(",",$userId);
            if ($class == "C") {
                // $paginator->pager($requests->find("store_id = $store and client_id in ($userId) and status_id = 3 and operation_id = $operation")->count(), 8, $page, 2);
                $requests = $requests->find("store_id = $store and client_id in ($userId) and status_id = 3 and operation_id = $operation and substr(updated_at,1,10) = $date")->order("field(status_id,2,1,4,3) ASC")->fetch(true);
                // ->limit($paginator->limit())->offset($paginator->offset())
            } else if ($class == "A") {
                // $paginator->pager($requests->find("status_id = 3 and operation_id = $operation")->count(), 8, $page, 2);
                $requests = $requests->find("status_id = :status_id and operation_id = :operation_id and substr(updated_at,1,10) = :date","status_id=3&operation_id=$operation&date=$date")->order("field(status_id,1,4,2) ASC")->fetch(true);
                // ->limit($paginator->limit())->offset($paginator->offset())
            }
            $users = new User();
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);
            if ($class == 'A') {
                foreach($requests as $request) {
                    $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                    $content[$request->id]['error_cost'] = $request->error_cost;
                    $content[$request->id]['id'] = $request->id;
                    $content[$request->id]['store_id'] = $request->store_id;
                    $content[$request->id]['nf_number'] = $request->nf_number;
                    $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                    $content[$request->id]['getClient'] = $request->getClient;
                    $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                    $content[$request->id]['getType'] = $request->getType;
                    $content[$request->id]['clientGroup'] = $request->clientGroup;
                    $content[$request->id]['items_quantity'] = $request->items_quantity;
                    $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                    $content[$request->id]['getOperator'] = $request->getOperator;
                    $content[$request->id]['operator'] = $request->operator_id;
                    $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                    $content[$request->id]['getOperation'] = $request->getOperation;
                    $content[$request->id]['operation'] = $request->operation_id;
                    $content[$request->id]['status_id'] = $request->status_id;
                    $content[$request->id]['getStatus'] = $request->getStatus;
                    // $content[$request->id]['AG2DETNT2'] = $request->AG2DETNT2;
                }
                $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            } else if($class == 'O') {
                echo $this->view->render("operator/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation             
                ]);
            } else if($class == 'C') {
                echo $this->view->render("client/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            }
            
        }
    }
    
    public function requestAtualizadaOperator($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $operator = $data['operator'];
            $date = date("Y-m-d");
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $storeGroup = $usuario->store_group_id;
            $requests = new NfReceipt();
            if($data['cd'] == "1") {
                $requests = $requests->find("status_id = :status_id and operator_id = :operator_id and substr(updated_at,1,10) = :date","status_id=3&operator_id=$operator&date=$date")->order("field(status_id,1,4,2) ASC")->fetch(true);
            } else {
                $requests = $requests->find("status_id = :status_id and operator_id = :operator_id and substr(updated_at,1,10) = :date and type_id != :type","type=4&status_id=3&operator_id=$operator&date=$date")->order("field(status_id,1,4,2) ASC")->fetch(true);
            }       
            $users = new User();
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);
            if ($class == 'A') {
                foreach($requests as $request) {
                    $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                    $content[$request->id]['error_cost'] = $request->error_cost;
                    $content[$request->id]['id'] = $request->id;
                    $content[$request->id]['store_id'] = $request->store_id;
                    $content[$request->id]['nf_number'] = $request->nf_number;
                    $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                    $content[$request->id]['getClient'] = $request->getClient;
                    $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                    $content[$request->id]['getType'] = $request->getType;
                    $content[$request->id]['clientGroup'] = $request->clientGroup;
                    $content[$request->id]['items_quantity'] = $request->items_quantity;
                    $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                    $content[$request->id]['getOperator'] = $request->getOperator;
                    $content[$request->id]['operator'] = $request->operator_id;
                    $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                    $content[$request->id]['getOperation'] = $request->getOperation;
                    $content[$request->id]['operation'] = $request->operation_id;
                    $content[$request->id]['status_id'] = $request->status_id;
                    $content[$request->id]['getStatus'] = $request->getStatus;
                    // $content[$request->id]['AG2DETNT2'] = $request->AG2DETNT2;
                }
                $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            }
            
        }
    }

    public function requestClientOperator($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {

            $operator = $data['operator'];
            $date = date("Y-m-d");
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $storeGroup = $usuario->store_group_id;
            $requests = new NfReceipt();
            $requests = $requests->find("status_id = :status_id and operator_id = :operator_id","status_id=2&operator_id=$operator")->order("id ASC")->fetch(true);
           
            
            $users = new User();
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);
            if ($class == 'A') {
                foreach($requests as $request) {
                    $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                    $content[$request->id]['error_cost'] = $request->error_cost;
                    $content[$request->id]['id'] = $request->id;
                    $content[$request->id]['store_id'] = $request->store_id;
                    $content[$request->id]['nf_number'] = $request->nf_number;
                    $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                    $content[$request->id]['getClient'] = $request->getClient;
                    $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                    $content[$request->id]['getType'] = $request->getType;
                    $content[$request->id]['clientGroup'] = $request->clientGroup;
                    $content[$request->id]['items_quantity'] = $request->items_quantity;
                    $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                    $content[$request->id]['getOperator'] = $request->getOperator;
                    $content[$request->id]['operator'] = $request->operator_id;
                    $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                    $content[$request->id]['getOperation'] = $request->getOperation;
                    $content[$request->id]['operation'] = $request->operation_id;
                    $content[$request->id]['status_id'] = $request->status_id;
                    $content[$request->id]['getStatus'] = $request->getStatus;
                    // $content[$request->id]['AG2DETNT2'] = $request->AG2DETNT2;
                }
                $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            }
            
        }
    }

    public function requestStoreStatus($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $date = date("Y-m-d");
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            // $store = $usuario->store_code;
            $store = $data['store'];
            $status = $data['status'];

            $storeGroup = $usuario->store_group_id;
            $requests = new NfReceipt();
            if($status == "3") {
                $requests = $requests->find("status_id = :status_id and store_id = :store_id and substr(updated_at,1,10) = :date","status_id=3&store_id=$store&date=$date")->order("id ASC")->fetch(true);
            } else {
                $requests = $requests->find("store_id = :store_id and substr(created_at,1,10) = :date","store_id=$store&date=$date")->order("field(status_id,1,4,2) ASC")->fetch(true);
            }         
            $users = new User();
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);
            if ($class == 'A') {
                foreach($requests as $request) {
                    $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                    $content[$request->id]['error_cost'] = $request->error_cost;
                    $content[$request->id]['id'] = $request->id;
                    $content[$request->id]['store_id'] = $request->store_id;
                    $content[$request->id]['nf_number'] = $request->nf_number;
                    $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                    $content[$request->id]['getClient'] = $request->getClient;
                    $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                    $content[$request->id]['getType'] = $request->getType;
                    $content[$request->id]['clientGroup'] = $request->clientGroup;
                    $content[$request->id]['items_quantity'] = $request->items_quantity;
                    $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                    $content[$request->id]['getOperator'] = $request->getOperator;
                    $content[$request->id]['operator'] = $request->operator_id;
                    $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                    $content[$request->id]['getOperation'] = $request->getOperation;
                    $content[$request->id]['operation'] = $request->operation_id;
                    $content[$request->id]['status_id'] = $request->status_id;
                    $content[$request->id]['getStatus'] = $request->getStatus;
                    // $content[$request->id]['AG2DETNT2'] = $request->AG2DETNT2;
                }
                $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                // "paginator" => $paginator,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            }
            
        }
    }
    
    public function getRequest($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $storiesCodes = $usuario->getStoreGroup2();
            $requests = new NfReceipt();
            $date = date("Y-m-d");
            // $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
            $status = $data['status'];
            // $paginator = new Paginator(URL_BASE."/request/dashboard/{$status}?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
            if($class == "A") {
                if($status == 1){
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 2) {
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 3) {
                    // $paginator->pager($requests->find("status_id = :status and substr(updated_at,1,10) = :date","status=$status&date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and substr(updated_at,1,10) = :date","status=$status&date=$date")->order("id ASC")->fetch(true);      
                    // ->limit($paginator->limit())->offset($paginator->offset())                  
                } else if ($status == 4) {
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);    
                    // ->limit($paginator->limit())->offset($paginator->offset())                    
                } else if ($status == 6) {
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);    
                    // ->limit($paginator->limit())->offset($paginator->offset())                    
                } else if ($status == 'i') {
                    // $paginator->pager($requests->find("substr(created_at,1,10) = :date","date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("substr(created_at,1,10) = :date","date=$date")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 9) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 10) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 11) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 12) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 13) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 14) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 15) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                }
                else if ($status == 17) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if($status == 18){
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                }
                foreach($requests as $request) {
                    $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                    $content[$request->id]['error_cost'] = $request->error_cost;
                    $content[$request->id]['id'] = $request->id;
                    $content[$request->id]['store_id'] = $request->store_id;
                    $content[$request->id]['nf_number'] = $request->nf_number;
                    $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                    $content[$request->id]['getClient'] = $request->getClient;
                    $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                    $content[$request->id]['getType'] = $request->getType;
                    $content[$request->id]['clientGroup'] = $request->clientGroup;
                    $content[$request->id]['items_quantity'] = $request->items_quantity;
                    $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                    $content[$request->id]['getOperator'] = $request->getOperator;
                    $content[$request->id]['operator'] = $request->operator_id;
                    $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                    $content[$request->id]['getOperation'] = $request->getOperation;
                    $content[$request->id]['operation'] = $request->operation_id;
                    $content[$request->id]['status_id'] = $request->status_id;
                    $content[$request->id]['getStatus'] = $request->getStatus;
                    // $content[$request->id]['AG2DETNT2'] = $request->AG2DETNT2;
                }
                $content = json_encode($content);
            } 
            if($class == "T") {
                if($status == 1){
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 2) {
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 3) {
                    // $paginator->pager($requests->find("status_id = :status and substr(updated_at,1,10) = :date","status=$status&date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and substr(updated_at,1,10) = :date","status=$status&date=$date")->order("id ASC")->fetch(true);      
                    // ->limit($paginator->limit())->offset($paginator->offset())                  
                } else if ($status == 4) {
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);    
                    // ->limit($paginator->limit())->offset($paginator->offset())                    
                } else if ($status == 6) {
                    // $paginator->pager($requests->find("status_id = :status","status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);    
                    // ->limit($paginator->limit())->offset($paginator->offset())                    
                } else if ($status == 'i') {
                    // $paginator->pager($requests->find("substr(created_at,1,10) = :date","date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("substr(created_at,1,10) = :date","date=$date")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 9) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 10) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 11) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 12) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 13) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 14) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 15) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status","status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                }
            } 
            if($class == "C") {
                if($status == '1,4,5,6'){
                    // $paginator->pager($requests->find("status_id in (1,4,5,6) and store_id = $store")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id in (1,4,5,6) and store_id = $store")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 2) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 3) {
                    // $paginator->pager($requests->find("status_id = :status and substr(updated_at,1,10) = :date and store_id = :store_id","store_id=$store&status=$status&date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and substr(updated_at,1,10) = :date and store_id = :store_id","store_id=$store&status=$status&date=$date")->order("id ASC")->fetch(true);    
                    // ->limit($paginator->limit())->offset($paginator->offset())
            
                } else if ($status == 4) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 'i') {
                    // $paginator->pager($requests->find("substr(created_at,1,10) = :date and store_id = :store_id","store_id=$store&date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("substr(created_at,1,10) = :date and store_id = :store_id","store_id=$store&date=$date")->order("id ASC")->fetch(true);     
                    // ->limit($paginator->limit())->offset($paginator->offset())                   
                }   else if ($status == 9) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                }
            }  
            if($class == "C2") {
                if ($store == 299 || $store == 531 ||$store ==  450){
                    $requests = (new NfReceipt2());
                }
                if($status == 10){
                    // $paginator->pager($requests->find("status_id in (1,4,5,6) and store_id = $store")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                }else if ($status == 12) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 11) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 14) {
                    // $paginator->pager($requests->find("status_id = :status and substr(updated_at,1,10) = :date and store_id = :store_id","store_id=$store&status=$status&date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and substr(updated_at,1,10) = :date and store_id = :store_id","store_id=$store&status=$status&date=$date")->order("id ASC")->fetch(true);    
                    // ->limit($paginator->limit())->offset($paginator->offset())
            
                } else if ($status == 13) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->order("id ASC")->fetch(true);  
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 'i') {
                    // $paginator->pager($requests->find("substr(created_at,1,10) = :date and store_id = :store_id","store_id=$store&date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("substr(created_at,1,10) = :date and store_id = :store_id","store_id=$store&date=$date")->order("id ASC")->fetch(true);     
                    // ->limit($paginator->limit())->offset($paginator->offset())                   
                }
            }  
            if($class == "OG") {
                if($status == 10){
                    // $paginator->pager($requests->find("status_id in (1,4,5,6) and store_id = $store")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = $status and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                }else if ($status == 12) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = $status and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 11) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = $status and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 14) {
                    // $paginator->pager($requests->find("status_id = :status and substr(updated_at,1,10) = :date and store_id = :store_id","store_id=$store&status=$status&date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = $status and substr(updated_at,1,10) = $date and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
            
                } else if ($status == 13) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests = $requests->find("status_id = $status and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 'i') {
                    // $paginator->pager($requests->find("substr(created_at,1,10) = :date and store_id = :store_id","store_id=$store&date=$date")->count(), 8, $page, 2);
                    $requests = $requests->find("substr(created_at,1,10) = $date and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);     
                    // ->limit($paginator->limit())->offset($paginator->offset())                   
                }
                $requests2 = (new NfReceipt2());
                if($status == 10){
                    // $paginator->pager($requests->find("status_id in (1,4,5,6) and store_id = $store")->count(), 8, $page, 2);
                    $requests2 = $requests2->find("status_id = $status and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                }else if ($status == 12) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests2 = $requests2->find("status_id = $status and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 11) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests2 = $requests2->find("status_id = $status and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
                } else if ($status == 14) {
                    // $paginator->pager($requests->find("status_id = :status and substr(updated_at,1,10) = :date and store_id = :store_id","store_id=$store&status=$status&date=$date")->count(), 8, $page, 2);
                    $requests2 = $requests2->find("status_id = $status and substr(updated_at,1,10) = $date and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())
            
                } else if ($status == 13) {
                    // $paginator->pager($requests->find("status_id = :status and store_id = :store_id","store_id=$store&status=$status")->count(), 8, $page, 2);
                    $requests2 = $requests2->find("status_id = $status and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);
                    // ->limit($paginator->limit())->offset($paginator->offset())                      
                } else if ($status == 'i') {
                    // $paginator->pager($requests->find("substr(created_at,1,10) = :date and store_id = :store_id","store_id=$store&date=$date")->count(), 8, $page, 2);
                    $requests2 = $requests2->find("substr(created_at,1,10) = $date and store_id in ($storiesCodes)")->order("id ASC")->fetch(true);     
                    // ->limit($paginator->limit())->offset($paginator->offset())                   
                }
            }                        
            // $paginatorRender = $paginator->render();
            $users = new User();
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            if ($class == 'C') {
                echo $this->view->render("client/requests", ["id" => "Solicitação de lançamentos | " . SITE,
                "users" => $users,
                "requests" => $requests,
                
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType
                ]);
            }
            if ($class == 'A') {
                echo $this->view->render("administrator/requests2", ["id" => "Solicitação de lançamentos | " . SITE,
                    "users" => $users,
                    "requests" => $content,
                    "status" => $status,
                    "storeGroups" => $storeGroups,
                    "nfeType" => $nfeType
                    ]);               
            }
            if ($class == 'T') {
                
                echo $this->view->render("administrator/requests copy", ["id" => "Solicitação de lançamentos | " . SITE,
                "users" => $users,
                "requests" => $requests,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType
                ]);
            }    
            if ($class == 'C2') {
                $status = (new NfStatus())->find("id >= :id","id=10")->fetch(true);
                echo $this->view->render("client/requests1", ["id" => "Solicitação de lançamentos | " . SITE,
                "users" => $users,
                "requests" => $requests,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType
                ]);
            } 
            if ($class == 'OG') {
                $status = (new NfStatus())->find("id >= :id","id=10")->fetch(true);
                echo $this->view->render("client/requests1", ["id" => "Solicitação de lançamentos | " . SITE,
                "users" => $users,
                "requests" => $requests,
                "requests2" => $requests2,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType
                ]);
            }        
        }
    }

    public function searchNfReceipt() {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $loja = $usuario->store_code;
            $class = $usuario->class;
            
            $store = $_POST['store'];
            if ($loja != 2000){
                $store = $loja;
            }
            $requestCode = $_POST['id'];
            $status = $_POST['status'];
            
            $criacaoInicial = $_POST['criacaoInicial'];
            $criacaoFinal = $_POST['criacaoFinal'];
            $alteracaoInicial = $_POST['alteracaoInicial'];
            $alteracaoFinal = $_POST['alteracaoFinal'];
            $idOperator = $_POST['idOperator'];
            $storeGroup = $_POST['storeGroup'];
            $accessKey = $_POST['accessKey'];
            $operation = $_POST['operation'];
            $nfNumber = $_POST['nfNumber'];
            $type = $_POST['type'];
            $params = array();
            $comparation = array();
            if ($requestCode != '') {
                $params['id'] = $requestCode;
                $comparation[10] = "id = :id";
            }

            if ($type != '') {
                $params['type_id'] = $type;
                $comparation[77] = "type_id = :type_id";
            }

            if ($storeGroup != '') {
                $params['group_id'] = $storeGroup;
                $comparation[99999] = "group_id = :group_id";
            }

            if ($operation != '') {
                $params['operation_id'] = $operation;
                $comparation[88] = "operation_id = :operation_id";
            }

            if ($accessKey != '') {
                $params['access_key'] = $accessKey;
                $comparation[99] = "access_key = :access_key";
            }

            if ($nfNumber != '') {
                $params['nf_number'] = $nfNumber;
                $comparation[10] = "nf_number = :nf_number";
            }
            
            if ($idOperator != '') {
                $params['operator_id'] = $idOperator;
                $comparation[2] = "operator_id = :operator_id";
            }
            if ($criacaoInicial != '') {
                $params['criacaoInicial'] = '1' . substr($criacaoInicial, 2, 2) . substr($criacaoInicial, 5, 2) . substr($criacaoInicial, 8, 2);
                $comparation[4] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) >= :criacaoInicial";
            }
            if ($criacaoFinal != '') {
                $params['criacaoFinal'] = '1' . substr($criacaoFinal, 2, 2) . substr($criacaoFinal, 5, 2) . substr($criacaoFinal, 8, 2);
                $comparation[5] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) <= :criacaoFinal";
            }
            if ($alteracaoInicial != '') {
                $params['alteracaoInicial'] = '1' . substr($alteracaoInicial, 2, 2) . substr($alteracaoInicial, 5, 2) . substr($alteracaoInicial, 8, 2);
                $comparation[6] = "concat('1',substr(`updated_at`,3,2),substr(`updated_at`,6,2),substr(`updated_at`,9,2)) >= :alteracaoInicial";
            }
            if ($alteracaoFinal != '') {
                $params['alteracaoFinal'] = '1' . substr($alteracaoFinal, 2, 2) . substr($alteracaoFinal, 5, 2) . substr($alteracaoFinal, 8, 2);
                $comparation[7] = "concat('1',substr(`updated_at`,3,2),substr(`updated_at`,6,2),substr(`updated_at`,9,2)) <= :alteracaoFinal";
            }
            if ($status != '') {
                $params['status_id'] = $status;
                $comparation[8] = "status_id = :status_id";
            }
            if ($store != '') {
                $params['store_id'] = $store;
                $comparation[9] = "store_id = :store_id";
            } else {
                if ($class == 'OG' || $class == 'OG2') {
                    $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
                    foreach ($userStoreGroup as $userGroups) {
                        $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
                    }
            
                    if (is_array($groups)){
                        $groups = implode(", ",$groups);
                        $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                        foreach($groups as $group){
                            $params2["store_id$group->code"] = $group->code;
                            $comparation2[$group->code] = "store_id = :store_id$group->code";
                        }
                        $comparation2 = " and (".implode(' or ', $comparation2).")";
                        $params2 = "&".http_build_query($params2);
                        
                    }
                }
            }
            
            $comparation = implode(' and ', $comparation);
            $params = http_build_query($params);

            if(!isset($comparation)) {
                $comparation = "store_id = :store_id";
            }
            if($params == '') {
                $params = "store_id=$loja";
            }
            $users = new User();
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);
            $requests = new NfReceipt();
            $requests = $requests->find($comparation.$comparation2, $params.$params2)->order("id ASC")->fetch(true);
            $requests2 = new NfReceipt2();
            $requests2 = $requests2->find($comparation.$comparation2, $params.$params2)->order("id ASC")->fetch(true);
            if ($class == 'A') {
                foreach($requests as $request) {
                    $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                    $content[$request->id]['error_cost'] = $request->error_cost;
                    $content[$request->id]['id'] = $request->id;
                    $content[$request->id]['store_id'] = $request->store_id;
                    $content[$request->id]['nf_number'] = $request->nf_number;
                    $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                    $content[$request->id]['getClient'] = $request->getClient;
                    $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                    $content[$request->id]['getType'] = $request->getType;
                    $content[$request->id]['clientGroup'] = $request->clientGroup;
                    $content[$request->id]['items_quantity'] = $request->items_quantity;
                    $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                    $content[$request->id]['getOperator'] = $request->getOperator;
                    $content[$request->id]['operator'] = $request->operator_id;
                    $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                    $content[$request->id]['getOperation'] = $request->getOperation;
                    $content[$request->id]['operation'] = $request->operation_id;
                    $content[$request->id]['status_id'] = $request->status_id;
                    $content[$request->id]['getStatus'] = $request->getStatus;
                    $content[$request->id]['codigoInternoCD'] = $request->codigoInternoCD();
                    // $content[$request->id]['error'] = $request->error;
                }
                $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                "users" => $users,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
                ]);
            } else if($class == 'O') {
                echo $this->view->render("operator/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                "users" => $users,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation             
                ]);
            } else if($class == 'C') {
                echo $this->view->render("client/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation,
                "users" => $users
                ]);
            } else if($class == 'OG' || $class == 'OG2') {
                $groups = null;
                $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
                    
                foreach ($userStoreGroup as $userGroups) {
                    $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
                }
                if (is_array($groups)){     
                    $groups = implode(", ",$groups);
                    $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                }
                $status = (new NfStatus())->find("id >= :id","id=10")->fetch(true);
                echo $this->view->render("operator/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                "requests2" => $requests2,
                "groups" => $groups,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation,
                "users" => $users
                ]);
            }   else if ($class == 'C2') {
                $status = (new NfStatus())->find("id >= :id","id=10")->fetch(true);
                echo $this->view->render("client/requests", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $requests,
                "requests2" => $requests2,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation,
                "users" => $users
                ]);
            }
        }
    }

    public function addNfReceipt() {
        

        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $user = $usuario->id;
            $nfDuoDate1 = $_POST['dueDate1'];
            $nfDuoDate2 = $_POST['dueDate2'];
            $nfDuoDate3 = $_POST['dueDate3'];

            if (strlen($nfDuoDate1) < 10 &&  $_POST['type'] != 3) {
                $data['success'] = false;
                $data['message'] = "Informe os Vencimentos da Nota Fiscal.";
                echo json_encode($data);
                die;
            }
            $accessKey = $_POST['accessKey'];
            $cnpj = substr($accessKey,6,14);
            $itemsQuantity = $_POST['itemsQuantity'];
            $nfeOperation = $_POST['nfeOperation'];
            $type = $_POST['type'];
            if ($type == 8) {
                if ($usuario->store_group_id == 1) {
                    $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $store")->fetch();
                    $aa2ctipo2 = (new AA2CTIPO())->find("TIP_CGC_CPF = ".$_POST['cnpj'])->fetch();
                    if (!$aa2ctipo2) {
                        $data['success'] = false;
                        $data['message'] = "CNPJ não cadastrado.";
                        echo json_encode($data);
                        die;
                    }
                    $aa1agedt = new AA1AGEDT();
                    $aa1agedt->addAa1agedt('1'.date("ymd"),$aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO,$_POST['nfNumber'],$aa2ctipo2->TIP_CODIGO,$_POST['totalCost'],2,112,'1 ');
                    $titleInteraction = "<table class='table'><tr><th>CNPJ ".$_POST['cnpj']."</th><th>Razão Social $aa2ctipo2->TIP_RAZAO_SOCIAL</th><th> Número da Nota: ".$_POST['nfNumber']."</th><th> Custo Total: ".$_POST['totalCost']."</th></tr><br>";
                    
                    for ($i=1;$i<=count($_POST['ean']);$i++){
                        if ($_POST['ean'][$i] == '' || $_POST['cost'][$i]==''){
                        } else {
                            $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $store")->fetch();
                            $aa3ccean = (new AA3CCEAN())->find("EAN_COD_EAN = ".$_POST['ean'][$i])->fetch();
                            $ag2detnt = new AG2DETNT();
                            if ($aa2ctipo->TIP_ESTADO != 'BA') {
                                $cfop = '2102';
                            } else {
                                $cfop = '1102';
                            }
                            $ag2detnt->addAg2detnt($cfop,$aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO,str_pad($aa3ccean->aa3citem()->GIT_COD_FOR,7,'0',STR_PAD_RIGHT).str_pad($aa3ccean->EAN_COD_PRO_ALT,8,0,STR_PAD_LEFT),$aa2ctipo2->TIP_CODIGO,$_POST['cost'][$i],'1'.date("ymd"),$_POST['nfNumber'],$_POST['amount'][$i],$i,2,40);
                            $titleInteraction .= "<tr><td>EAN ".$aa3ccean->EAN_COD_EAN."</td><td> Custo: ".$_POST['cost'][$i]."</td><td> Quantidade: ".$_POST['amount'][$i]."</td></tr><br>";
                        }
                        
    
                    }


                } else if ($usuario->store_group_id == 2) {
                    $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = $store")->fetch();
                    $aa2ctipo2 = (new AA2CTIPO2())->find("TIP_CGC_CPF = ".$_POST['cnpj'])->fetch();
                    if (!$aa2ctipo2) {
                        $data['success'] = false;
                        $data['message'] = "CNPJ não cadastrado.";
                        echo json_encode($data);
                        die;
                    }
                    
                    $aa1agedt = new AA1AGEDT2();
                    $aa1agedt->addAa1agedt('1'.date("ymd"),$aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO,$_POST['nfNumber'],$aa2ctipo2->TIP_CODIGO,$_POST['totalCost'],2,112,'1 ');
                    $titleInteraction = "<table class='table'><tr><th>CNPJ ".$_POST['cnpj']."</th><th>Razão Social $aa2ctipo2->TIP_RAZAO_SOCIAL</th><th> Número da Nota: ".$_POST['nfNumber']."</th><th> Custo Total: ".$_POST['totalCost']."</th></tr><br>";
                    
                    for ($i=1;$i<=count($_POST['ean']);$i++){
                        if ($_POST['ean'][$i] == '' || $_POST['cost'][$i]==''){
                        } else {
                            $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = $store")->fetch();
                            $aa3ccean = (new AA3CCEAN2())->find("EAN_COD_EAN = ".$_POST['ean'][$i])->fetch();
                            $ag2detnt = new AG2DETNT2();
                            if ($aa2ctipo->TIP_ESTADO != 'BA') {
                                $cfop = '2102';
                            } else {
                                $cfop = '1102';
                            }
                            $ag2detnt->addAg2detnt($cfop,$aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO,str_pad($aa3ccean->aa3citem()->GIT_COD_FOR,7,'0',STR_PAD_RIGHT).str_pad($aa3ccean->EAN_COD_PRO_ALT,8,0,STR_PAD_LEFT),$aa2ctipo2->TIP_CODIGO,$_POST['cost'][$i],'1'.date("ymd"),$_POST['nfNumber'],$_POST['amount'][$i],$i,2,40);
                            $titleInteraction .= "<tr><td>EAN ".$aa3ccean->EAN_COD_EAN."</td><td> Custo: ".$_POST['cost'][$i]."</td><td> Quantidade: ".$_POST['amount'][$i]."</td></tr><br>";
                        }
                        
    
                    }
                }

            }
            $xml = simplexml_load_file("/bdtecsolucoes.com.br:8090/../XML/".$store.'/NFe'.$accessKey.'.xml');

            $cnpj = $xml->NFe->infNFe->emit->CNPJ;
            
            $codigos = $xml->NFe->infNFe->det;
            
            foreach ($codigos as $codigo) {
                
                $code = $codigo->prod->cProd;
                
                $count = (new ExchangeCode());
                $count = $count->find("store_id = :store and cnpj = :cnpj and source_code = :code","store=$store&cnpj=$cnpj&code=$code");
                $count = $count->count();
                if ($count > 0) {
                    // echo "store_id=$store&cnpj=$cnpj&source_code=$code";
                    $exchangeCode = (new ExchangeCode())->find("store_id = :store and cnpj = :cnpj and source_code = :code","store=$store&cnpj=$cnpj&code=$code")->fetch();
                    $codigo->prod->cEAN = str_replace('-','',$exchangeCode->destination_code);
                    $codigo->prod->cEANTrib = str_replace('-','',$exchangeCode->destination_code);
                    $xml->asXML("/bdtecsolucoes.com.br:8090/../XML/".$store.'/NFe'.$accessKey.'.xml');
                    if ($nfeOperation == 2) {
                        $nfeOperation = 10;
                    } else {
                        $nfeOperation = 9;
                    }
                }
                
            }
            if ($type == 4){
                $nfeOperation = 7;
            }
       
            //iniciando classes para upload
            $fileUpload = new File("storage", "files");
            $dobleNfe = (new NfReceipt())->find("access_key = :access_key","access_key=$accessKey")->count();
            $nfNumber = $_POST['nfNumber'];
            $client = $usuario->id;
            $store_group = $usuario->store_group_id;
            $note = nl2br($_POST['note']);
            $files = $_FILES;
            if ($note == '') {
                $note = $nfNumber;
            }
                if ($nfNumber == '') {
                    $data['success'] = false;
                    $data['message'] = "Informe o número do documento.";
                    echo json_encode($data);
                } else {
                    if ($type != 3 && !$_POST['dueDate1']) {
                        $data['success'] = false;
                        $data['message'] = "Informe pelo menos um vencimento.";
                        echo json_encode($data);
                    } else {
                        if ($dobleNfe > 0 && $_POST['accessKey'] != '') {
                            $data['success'] = false;
                            $data['message'] = "Chave de Acesso já Postada";
                            echo json_encode($data);
                        } else {
    
                            //incluindo o solicitação de lançamento efetivamente
                            if ($cnpj == '10838648000195' || $cnpj == '10838648000519' || $cnpj == '10838648000357') {
                                $nfeOperation = 7;
                                $type = 4;
                                                                
                            }
                            if ($type == 1){
                                if ((new NfReceipt())->find("nf_number = :nf and store_id = :store","nf=$nfNumber&store=$store")->count()>0) {
                                    $data['success'] = false;
                                    $data['message'] = "Número da NF já postado no portal.";

                                    echo json_encode($data);
                                    die;
                                } elseif (empty($files['files'])) {
                                    // $data['success'] = false;
                                    // $data['message'] = "Gentileza anexar a NF a ser lançada.";
                                    // echo json_encode($data);
                                }
                            }
                            
                            if ($store == 299 || $store == 531 || $store == 450) {
                                $request = new NfReceipt2();
                            } else {
                                $request = new NfReceipt();
                            }
                            
                            $request->store_id = $store;
                            $request->group_id = $store_group;
                            $request->nf_number = $nfNumber;
                            if ($accessKey == '') {
                                $request->access_key = $nfNumber;
                            } else {
                                $request->access_key = $accessKey;
                            }
                            
                            $request->status_id = 1;
                            $request->note = nl2br($note);
                            $request->client_id = $client;
                            $request->operator_id = 147;
                            $request->items_quantity = $itemsQuantity;
                            $request->type_id = $type;
                            $request->operation_id = $nfeOperation;
                            $request->inclusion_type = 1;
                            $receipt =  (new NfReceipt())->find("store_id = :sid and access_key = :ak and nf_number = :nn",
                            "sid=$store&ak=$accessKey&nn=$nfNumber")->count();
                            if ($receipt == 0){
                                if (strlen($accessKey) <> 44) {
                                    $requestId = $request->save2();
                                    $request->access_key = $request->id;
                                    $requestId = $request->save2();
                                } else {
                                    $requestId = $request->save2();
                                }
                            } else {

                                $data['success'] = false;
                                $data['message'] = "NFe já postada.";
                                echo json_encode($data);
                            }
                            if ($requestId) {
                                $id = $request->id;
                                $nfEventCount = (new NfEvent())->find("situation = :situation and id_nf_receipt = :id_nf_receipt and status_id = :sid","situation=A&id_nf_receipt=$requestId&sid=1")->count();
                                if ($nfEventCount == 0) {
                                    $nfEvent = new NfEvent();
                                    $nfEvent->id_nf_receipt = $requestId;
                                    if ($class == 'C2') {
                                        $nfEvent->status_id = 10;
                                    } else {
                                        $nfEvent->status_id = 1;
                                    }
                                    
                                    $nfEvent->user_id = $client;
                                    $nfEvent->situation = 'A';
                                    $nfEventId = $nfEvent->save();
                    
                                }
                                $nfStartedCount = (new NfStarted())->find("access_key = :access_key","access_key=$accessKey")->count();
                                if($nfStartedCount>0){
                                    $nfStarted = (new NfStarted())->find("access_key = :access_key","access_key=$accessKey")->fetch();
                                    $requestStarted = (new NfReceipt())->findById($requestId);
                                    $requestStarted->status_id = $nfStarted->status_id;
                                    $requestStarted->operator_id = $nfStarted->operator_id;
                                    $requestStartedId = $requestStarted->save();
                                    if (strlen($nfStarted->note)>1){
                                        $note = $nfStarted->note;
                                        $interactionStarted = (new Interaction());
                                        $interactionStarted->id_nf_receipt = $requestId;
                                        $interactionStarted->id_user = $nfStarted->operator_id;
                                        $interactionStarted->description = $nfStarted->note;
                                        $interactionStartedId = $interactionStarted->save();
                                    }
                                }
                                $id = $request->id;
                                if ($nfDuoDate1){
                                    $nfDuoDate = (new NfDueDate());
                                    $nfDuoDate->nf_id = $id;
                                    $nfDuoDate->due_date = $nfDuoDate1;
                                    $nfDuoDateId = $nfDuoDate->save();
                                }
                                if ($nfDuoDate2){
                                    $nfDuoDate = (new NfDueDate());
                                    $nfDuoDate->nf_id = $id;
                                    $nfDuoDate->due_date = $nfDuoDate2;
                                    $nfDuoDateId = $nfDuoDate->save();
                                }
                                if ($nfDuoDate3){
                                    $nfDuoDate = (new NfDueDate());
                                    $nfDuoDate->nf_id = $id;
                                    $nfDuoDate->due_date = $nfDuoDate3;
                                    $nfDuoDateId = $nfDuoDate->save();
                                }
                                $attachmentSuccess = true;
                                if (!empty($files['files'])) {
                                    $documents = $files['files'];
                                    for ($i = 0;$i < count($documents['type']);$i++) {
                                        foreach (array_keys($documents) as $keys) {
                                            $documentFiles[$i][$keys] = $documents[$keys][$i];
                                        }
                                    }
                                    $i = 0;
                                    foreach ($documentFiles as $file) {
                                        if (empty($file['type'])) {
                                            $attachmentSuccess = false;
                                            $mensage = "Selecione um arquivo valido " . $file['name'] . " não encontrado!\n";
                                        } elseif (!in_array($file['type'], $fileUpload::isAllowed())) {
                                            $attachmentSuccess = false;
                                            $mensage = "O arquivo " . $file['name'] . " do tipo ".$file['type']. " não é válido! <strong>O solicitação de lançamento não foi aberto</strong>.\n";
                                        } else {
                                            $uploaded = $fileUpload->upload($file, pathinfo($file["name"], PATHINFO_FILENAME), 1920);
                                            $requestAttachment = new NfAttachment();
                                            $requestAttachment->nf_id = $id;
                                            $requestAttachment->file_dir = $uploaded;
                                            if ($file['type'] == "image/jpg" || $file['type'] == "image/jpeg" || $file['type'] == "image/png"|| $file['type'] =="image/gif") {
                                                $requestAttachment->type = 'I';
                                            } else {
                                                $requestAttachment->type = 'F';
                                            }
                                            $requestAttachmentId = $requestAttachment->save();
                                            if (!$requestAttachmentId) {
                                                $name = $file["name"].$file["type"];
                                                $attachmentSuccess = false;
                                                $mensage = "Erro ao incluir o enaxo $name.";
                                            }
                                        }
                                        $i++;
                                    }
                                }                                
                                if ($attachmentSuccess == false) {
                                    $request = (new NfReceipt())->findById($id);
                                    $request->destroy();
                                    $data['success'] = false;
                                    $data['message'] = "Falha ao incluir o anexo! $mensage";
                                    echo json_encode($data);
                                } else {
                                    if( $titleInteraction) {
                                        $interactionItens = new Interaction();
                                        $interactionItens->id_nf_receipt = $id;
                                        $interactionItens->id_user = $usuario->id;
                                        $interactionItens->description	= $titleInteraction;
                                        $interactionItensID =  $interactionItens->save();
                                    }
                                    if($_POST['code'][0] != '') {
                                        for ($j=0;$j<=count($_POST['code']);$j++) {
                                            if ($_POST['code'][$j] == ''){
                    
                                            } else {
                                                if($_POST['code'][$j] > 0){
                                                    $nfReceiptCode = new NfReceiptCodes();
                                                    $nfReceiptCode->nf_receipt_id = $request->id;
                                                    $nfReceiptCode->code = $_POST['code'][$j];
                                                    $nfReceiptCodeId = $nfReceiptCode->save();
                                                }
                                                
                                            }
                                            
                                        }
                                    }
                                    
                                    $alterando = (new NfReceipt())->findById($request->id);
                                    if (strlen($alterando->access_key) <> 44) {
                                        $alterando->access_key = $request->id;
                                        $alterandoId = $alterando->save();
                                    }
                                    
                                    $data['success'] = true;
                                    $data['message'] = "solicitação de lançamento $id aberto com sucesso!  $note";
                                    echo json_encode($data);
    
                                }
                            } else {                                
                                $data['success'] = false;
                                $data['message'] = "Nota fiscal já postada!";
                                echo json_encode($data);
                            }                            
                        }
                    }
                }             
        }
    }


    public function addNfReceiptAuto() {
        if (!isset($_COOKIE['login']) == true) {
            $data['success']=false;
            $data['message']="Usuário desconectado, gentileza realizar o login novamente.";
            echo json_encode($data);
            die;
        } else {
            $type = $_POST['type'];
            
            $note = $_POST['note'];
            if ($note == '') {
                $note = $_POST['nfNumber'];
            }
            $operation = $_POST['nfeOperation'];
            $status = 1;
            $accessKey = $_POST['accessKey'];
            $identification=$_COOKIE['login'];
            $usuario=(new User())->find("email = :email", "email=$identification")->fetch();
            $user = $usuario->id;
            $class = $usuario->class;
            $vencimentos=["1" => $_POST['dueDate1'], "2" => $_POST['dueDate2'], "3" => $_POST['dueDate3']];
    
            $nfSefaz = (new NfSefaz())->find("access_key = :access_key", "access_key=$accessKey")->fetch();
            $group = (new Store())->find("code = $nfSefaz->store_id")->fetch()->group_id;


            $add = $nfSefaz->addNfreceipt($note, $user, $type, $operation, $status, $vencimentos);
    
            $fileUpload = new File("storage", "files");

            if ($add['value'] === true) {
                $logger=new Logger("web");
                $tele_key="1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $xml = simplexml_load_file(__DIR__."/../XML/".$nfSefaz->store_id.'/NFe'.$nfSefaz->access_key.'.xml');
                if ($nfSefaz->store()->telegram != '') {
                    $tele_channel = $nfSefaz->store()->telegram;
                } else {
                    $tele_channel="@BDTecLancamentos";
                }
    
                $tele_handler=new TelegramBotHandler($tele_key, $tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                $logger->pushHandler($tele_handler);
                $logger->info("Solicitação de lançamento ".$add['msg']." aberta! - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/".$add['msg']."");
                $nfEvent=new NfEvent();
                $nfEvent->id_nf_receipt=$add['msg'];
    
                if($class == "C2") {
                    $nfEvent->status_id = 10;
                } else {
                    $nfEvent->status_id = 1;
                }
    
                $nfEvent->user_id = $user;
                $nfEvent->situation = 'A';
                $nfEventId=$nfEvent->save();
                $checkRow=(new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id", "cnpj=$nfSefaz->cnpj&store_id=$nfSefaz->store_id")->count();
    
                #FAZER O TROCA CODIGO AQUI 
                if($checkRow > 0) {
                    $exchangeCode=(new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id", "cnpj=$nfSefaz->cnpj&store_id=$nfSefaz->store_id")->fetch(true);
    
                    foreach($exchangeCode as $assoc) {
                        foreach($xml1->det as $xml1) {
                            if($xml1->prod->cProd==str_replace('-', '', $assoc->source_code)) {
                                $verificacao=true;
                                $xml1->prod->cEAN=$assoc->destination_code;
                                $xml=$xml->asXML(__DIR__."/../XML/".$nfSefaz->store_id.'/NFe'.$nfSefaz->access_key.'.xml');
                            }
                        }
                    }

                    $xml = $xml->asXML(__DIR__."/../XML/".$nfSefaz->store_id.'/NFe'.$nfSefaz->access_key.'.xml');
                }
    
                $nfStartedCount=(new NfStarted())->find("access_key = :access_key", "access_key=$nfSefaz->access_key")->count();
    
                if($nfStartedCount>0) {
                    $nfStarted=(new NfStarted())->find("access_key = :access_key", "access_key=$nfSefaz->access_key")->fetch();
                    $requestStarted=(new NfReceipt())->findById($add['msg']);
                    $requestStarted->status_id=$nfStarted->status_id;
                    $requestStarted->operator_id=$nfStarted->operator_id;
                    $requestStartedId=$requestStarted->save();
    
                    if (strlen($nfStarted->note)>1) {
                        $note=$nfStarted->note;
                        $interactionStarted=(new Interaction());
                        $interactionStarted->id_nf_receipt=$add['msg'];
                        $interactionStarted->id_user=$nfStarted->operator_id;
                        $interactionStarted->description=$nfStarted->note;
                        $interactionStartedId=$interactionStarted->save();
                    }
                }
    
    
                $nfSefaz=(new NfSefaz())->find("access_key = :access_key", "access_key=$accessKey")->fetch();
                $nfSefazId=(new NfSefaz())->findById($nfSefaz->id);
                $nfSefazId->status_id=2;
                $nfSefazId2=$nfSefazId->save();
                $id = $add['msg'];
    
                $nfReceiptError=(new NfReceipt())->findById($id);
    
                if($nfReceiptError->error()==true && $nfReceiptError->type_id !=4 && $nfReceiptError->type_id !=6 && $nfReceiptError->type_id !=7 && ($nfReceiptError->group_id==1 or $nfReceiptError->group_id==10 or $nfReceiptError->group_id==2) && $nfReceiptError->type_id !=1) {
                    $nfReceiptError->error_cost='E';
                    $nfReceiptErrorId=$nfReceiptError->save();
                    $logger=new Logger("web");
                    $tele_key="1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel="1247513338";
                    $tele_handler=new TelegramBotHandler($tele_key, $tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                    $logger->pushHandler($tele_handler);
                    $logger->info(URL_BASE."/request/interacao/$id");
                }
    
                if($nfReceiptError->type_id==2) {
                    $interaction=(new Interaction());
                    $interaction->id_nf_receipt=$nfReceiptError->id;
                    $interaction->id_user=$nfReceiptError->operator_id;
    
                    //se a nota for do grupo 1 ou 10 entre
                    if ($nfReceiptError->group_id == 1) {
    
                        //percorra os itens da nota
                        foreach ($nfReceiptError->getItems() as $det) {
    
                            //se a loja for 61 entre
                            if (($nfReceiptError->store_id==61 || $nfReceiptError->store_id==108 || $nfReceiptError->store_id==56 || $nfReceiptError->store_id==57 || $nfReceiptError->store_id==58 || $nfReceiptError->store_id==59 || $nfReceiptError->store_id==80 || $nfReceiptError->store_id==82 || $nfReceiptError->store_id==85 || $nfReceiptError->store_id==72 || $nfReceiptError->store_id==78 || $nfReceiptError->store_id==76 || $nfReceiptError->store_id==53)) {
    
                                //se o produto tiver ean "SEM GTIN" ou codigo ean invalido (menor que 8 digitos) entre                                                
                                if($det->prod->cEAN=="SEM GTIN"|| strlen($det->prod->cEAN) < 8) {
    
                                    $aa2ctipo=(new AA2CTIPO())->find("TIP_CGC_CPF =  $nfReceiptError->getCnpjFornecedor")->fetch();
    
                                    //se o fornecedor existir entre
                                    if ($aa2ctipo) {
                                        $aa3citem=(new AA3CITEM())->find("GIT_REFERENCIA = '".$det->prod->cProd."' GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
    
                                        if ( !$aa3citem) {
                                            $aa1forit=(new AA1FORIT())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
    
                                            //se não houver aa1forit entre
                                            if ( !$aa1forit) {
                                                $aa1refit=(new AA1REFIT())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
    
                                                if ( !$aa1refit) {
                                                    (new ExchangeCode())->find("store_id = $nfReceiptError->store_id and cnpj = $nfReceiptError->getCnpjFornecedor and source_code = ".$det->prod->cProd)->fetch();
    
                                                    if ( !$exchangeCode) {
                                                        $boningOx=(new BoningOx())->find("reference = '".$det->prod->cProd."' and cnpj = $nfReceiptError->getCnpjFornecedor")->count();
    
                                                        if ($boningOx==0) {
                                                            $descriptionError .=" Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
                                                        }
                                                    }
    
                                                    else {
                                                        if ($nfReceiptError->group_id==1) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao.bat");
                                                        }
    
                                                        else if ($nfReceiptError->group_id==2) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao2.bat");
                                                        } else if ($nfReceiptError->group_id==10) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao10.bat");
                                                        }
    
    
    
                                                    }
    
                                                    // senão jogue o xml na pasta
                                                }
    
                                                else {
                                                    if ($nfReceiptError->group_id==1) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao.bat");
                                                    }
    
                                                    else if ($nfReceiptError->group_id==2) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao2.bat");
                                                    } else if ($nfReceiptError->group_id==10) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao10.bat");
                                                    }
    
                                                }
    
                                                // senão jogue o xml na pasta                                                         
                                            }
    
                                            else {
                                                if ($nfReceiptError->group_id==1) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao.bat");
                                                }
    
                                                else if ($nfReceiptError->group_id==10) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao10.bat");
                                                }
                                            }
                                        }
    
                                        // se o fornecedor não existir entre
                                    }
    
                                    else {
                                        $description=" Fornecedor ".$nfReceiptError->getCnpjFornecedor." sem Cadastro.<br />";
                                    }
    
                                    //senão (se tiver EAN valido)
                                }  else {
                                    if ($nfReceiptError->group_id == 1) {
                                        $aa2ctipo=(new AA2CTIPO())->find("TIP_CGC_CPF =  $nfReceiptError->getCnpjFornecedor")->fetch();
    
                                        // se o fornecedor existir entre
                                        if ($aa2ctipo) {
                                            $aa3ccean=(new AA3CCEAN())->find("EAN_COD_EAN = ".$det->prod->cEAN)->fetch();
        
                                            //se não tiver o EAN cadastrado na AA3CCEAN entre
                                            if ( !$aa3ccean) {
                                                $aa3citem=(new AA3CITEM())->find("GIT_REFERENCIA = '".$det->prod->cProd."' GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
        
                                                if ( !$aa3citem) {
                                                    $aa1forit=(new AA1FORIT())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                                    //se não houver aa1forit entre
                                                    if ( !$aa1forit) {
                                                        $aa1refit=(new AA1REFIT())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                                        // se não houver aa1refit entreentre
                                                        if ( !$aa1refit) {
                                                            $exchangeCode=(new ExchangeCode())->find("1=0")->fetch();
        
                                                            //se não houver troca codigo entre
                                                            if ( !$exchangeCode) {
                                                                //adicione uma linha a produtos sem cadastro (description);
                                                                $boningOx=(new BoningOx())->find("reference = '".$det->prod->cProd."' and cnpj = $nfReceiptError->getCnpjFornecedor")->count();
        
                                                                if ($boningOx==0) {
                                                                    $descriptionError .=" Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
                                                                }
        
                                                                // senão jogue o xml na pasta
                                                            }
        
                                                            else {
                                                                if ($nfReceiptError->group_id==1) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao.bat");
                                                                }
        
                                                                else if ($nfReceiptError->group_id==2) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao2.bat");
                                                                } else if ($nfReceiptError->group_id==10) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao10.bat");
                                                                }
                                                                
                                                            }
        
                                                            // senão jogue o xml na pasta
                                                        }
        
                                                        else {
                                                            if ($nfReceiptError->group_id==1) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao.bat");
                                                            }
        
                                                            else if ($nfReceiptError->group_id==2) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao2.bat");
                                                            } else if ($nfReceiptError->group_id==10) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao10.bat");
                                                            }
        
                                                        }
        
                                                        // senão jogue o xml na pasta                                                         
                                                    }
        
                                                    else {
                                                        if ($nfReceiptError->group_id==1) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao.bat");
                                                        }
        
                                                        else if ($nfReceiptError->group_id==2) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao2.bat");
                                                        } else if ($nfReceiptError->group_id==10) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao10.bat");
                                                        }
                                                    }
                                                }
        
                                                else {
                                                    if ($nfReceiptError->group_id==1) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao.bat");
                                                    }
        
                                                    else if ($nfReceiptError->group_id==2) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao2.bat");
                                                    }
                                                }
        
                                                // senão jogue o xml na pasta
                                            }
        
                                            else {
                                                if ($nfReceiptError->group_id==1) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao.bat");
                                                }
        
                                                else if ($nfReceiptError->group_id==2) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao2.bat");
                                                } else if ($nfReceiptError->group_id==10) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao10.bat");
                                                }
                                            }
        
                                            // se o fornecedor não existir entre
                                        }
                                    } else if ($nfReceiptError->group_id == 2) {
                                        $aa2ctipo=(new AA2CTIPO2())->find("TIP_CGC_CPF =  $nfReceiptError->getCnpjFornecedor")->fetch();
    
                                        // se o fornecedor existir entre
                                        if ($aa2ctipo) {
                                            $aa3ccean=(new AA3CCEAN2())->find("EAN_COD_EAN = ".$det->prod->cEAN)->fetch();
        
                                            //se não tiver o EAN cadastrado na AA3CCEAN entre
                                            if ( !$aa3ccean) {
                                                $aa3citem=(new AA3CITEM2())->find("GIT_REFERENCIA = '".$det->prod->cProd."' GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
        
                                                if ( !$aa3citem) {
                                                    $aa1forit=(new AA1FORIT2())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                                    //se não houver aa1forit entre
                                                    if ( !$aa1forit) {
                                                        $aa1refit=(new AA1REFIT2())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                                        // se não houver aa1refit entreentre
                                                        if ( !$aa1refit) {
                                                            $exchangeCode=(new ExchangeCode())->find("1=0")->fetch();
        
                                                            //se não houver troca codigo entre
                                                            if ( !$exchangeCode) {
                                                                //adicione uma linha a produtos sem cadastro (description);
                                                                $boningOx=(new BoningOx())->find("reference = '".$det->prod->cProd."' and cnpj = $nfReceiptError->getCnpjFornecedor")->count();
        
                                                                if ($boningOx==0) {
                                                                    $descriptionError .=" Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
                                                                }
        
                                                                // senão jogue o xml na pasta
                                                            }
        
                                                            else {
                                                                if ($nfReceiptError->group_id==1) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao.bat");
                                                                }
        
                                                                else if ($nfReceiptError->group_id==2) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao2.bat");
                                                                } else if ($nfReceiptError->group_id==10) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao10.bat");
                                                                }
                                                            }
        
                                                            // senão jogue o xml na pasta
                                                        }
        
                                                        else {
                                                            if ($nfReceiptError->group_id==1) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao.bat");
                                                            }
        
                                                            else if ($nfReceiptError->group_id==2) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao2.bat");
                                                            } else if ($nfReceiptError->group_id==10) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao10.bat");
                                                            }
        
                                                        }
        
                                                        // senão jogue o xml na pasta                                                         
                                                    }
        
                                                    else {
                                                        if ($nfReceiptError->group_id==1) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao.bat");
                                                        }
        
                                                        else if ($nfReceiptError->group_id==2) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao2.bat");
                                                        } else if ($nfReceiptError->group_id==10) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao10.bat");
                                                        }
                                                    }
                                                }
        
                                                else {
                                                    if ($nfReceiptError->group_id==1) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao.bat");
                                                    }
        
                                                    else if ($nfReceiptError->group_id==2) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao2.bat");
                                                    }
                                                }
        
                                                // senão jogue o xml na pasta
                                            }
        
                                            else {
                                                if ($nfReceiptError->group_id==1) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao.bat");
                                                }
        
                                                else if ($nfReceiptError->group_id==2) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao2.bat");
                                                } else if ($nfReceiptError->group_id==10) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao10.bat");
                                                }
                                            }
        
                                            // se o fornecedor não existir entre
                                        }
                                    } else if ($nfReceiptError->group_id == 10) {
                                        $aa2ctipo=(new AA2CTIPO10())->find("TIP_CGC_CPF =  $nfReceiptError->getCnpjFornecedor")->fetch();
    
                                        // se o fornecedor existir entre
                                        if ($aa2ctipo) {
                                            $aa3ccean=(new AA3CCEAN10())->find("EAN_COD_EAN = ".$det->prod->cEAN)->fetch();
        
                                            //se não tiver o EAN cadastrado na AA3CCEAN entre
                                            if ( !$aa3ccean) {
                                                $aa3citem=(new AA3CITEM10())->find("GIT_REFERENCIA = '".$det->prod->cProd."' GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
        
                                                if ( !$aa3citem) {
                                                    $aa1forit=(new AA1FORIT10())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                                    //se não houver aa1forit entre
                                                    if ( !$aa1forit) {
                                                        $aa1refit=(new AA1REFIT10())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                                        // se não houver aa1refit entreentre
                                                        if ( !$aa1refit) {
                                                            $exchangeCode=(new ExchangeCode())->find("1=0")->fetch();
        
                                                            //se não houver troca codigo entre
                                                            if ( !$exchangeCode) {
                                                                //adicione uma linha a produtos sem cadastro (description);
                                                                $boningOx=(new BoningOx())->find("reference = '".$det->prod->cProd."' and cnpj = $nfReceiptError->getCnpjFornecedor")->count();
        
                                                                if ($boningOx==0) {
                                                                    $descriptionError .=" Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
                                                                }
        
                                                                // senão jogue o xml na pasta
                                                            }
        
                                                            else {
                                                                if ($nfReceiptError->group_id==1) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao.bat");
                                                                }
        
                                                                else if ($nfReceiptError->group_id==2) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao2.bat");
                                                                } else if ($nfReceiptError->group_id==10) {
                                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                                    $output=shell_exec(__DIR__."/../importacao10.bat");
                                                                }
                                                            }
        
                                                            // senão jogue o xml na pasta
                                                        }
        
                                                        else {
                                                            if ($nfReceiptError->group_id==1) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao.bat");
                                                            }
        
                                                            else if ($nfReceiptError->group_id==2) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao2.bat");
                                                            } else if ($nfReceiptError->group_id==10) {
                                                                copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                                $output=shell_exec(__DIR__."/../importacao10.bat");
                                                            }
        
                                                        }
        
                                                        // senão jogue o xml na pasta                                                         
                                                    }
        
                                                    else {
                                                        if ($nfReceiptError->group_id==1) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao.bat");
                                                        }
        
                                                        else if ($nfReceiptError->group_id==2) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao2.bat");
                                                        } else if ($nfReceiptError->group_id==10) {
                                                            copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                            $output=shell_exec(__DIR__."/../importacao10.bat");
                                                        }
                                                    }
                                                }
        
                                                else {
                                                    if ($nfReceiptError->group_id==1) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao.bat");
                                                    }
        
                                                    else if ($nfReceiptError->group_id==2) {
                                                        copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                        $output=shell_exec(__DIR__."/../importacao2.bat");
                                                    }
                                                }
        
                                                // senão jogue o xml na pasta
                                            }
        
                                            else {
                                                if ($nfReceiptError->group_id==1) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao.bat");
                                                }
        
                                                else if ($nfReceiptError->group_id==2) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao2.bat");
                                                } else if ($nfReceiptError->group_id==10) {
                                                    copy("/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml", "/../XML_IMPORTAR_ARGOLO/NFe$nfReceiptError->access_key.xml");
                                                    $output=shell_exec(__DIR__."/../importacao10.bat");
                                                }
                                            }
        
                                            // se o fornecedor não existir entre
                                        }
                                    }
                                    
    
                                    else {
                                        $description=" Fornecedor ".$nfReceiptError->getCnpjFornecedor." sem Cadastro.<br />";
                                    }
                                }
    
                            }
    
                        }
                    }
    
                    if ($descriptionError) {
                        $interaction->description="Gentileza enviar fotos dos produtos abaixo pois não constam na nossa base de dados <br />".$descriptionError;
                        $interactionId=$interaction->save();
                        $pendencia=(new NfReceipt())->findById($id);
                        $pendencia->status_id=2;
                        $pendenciaId=$pendencia->save();
                    }
                }
                $storePortal = (new Store())->find("code = $nfSefaz->store_id")->fetch();
                if ($storePortal->group_id == 1) {
                    $aa2ctipo=(new AA2CTIPO());
                    $loja = $aa2ctipo->find("TIP_CODIGO = $nfSefaz->store_id")->fetch();
                    $loja = $loja->TIP_CODIGO.$loja->TIP_DIGITO;
                    $cd = $aa2ctipo->find("TIP_CGC_CPF = ".$nfSefaz->fornecedor()->CNPJ."")->fetch()->TIP_CODIGO;
                    $aa1agedt = new AA1AGEDT();
        
                    if ($nfSefaz->fornecedor()->CNPJ == '10838648000195'|| $nfSefaz->fornecedor()->CNPJ == '10838648000519'|| $nfSefaz->fornecedor()->CNPJ == '10838648000357') {
                        if ($nfSefaz->store_id==74) {
                            $aa1agedt=new AA1AGEDT();
                            $aa1agedt->dobraNota($loja, 143, $$_POST['nfNumber'], '1  ', $cd, $nfSefaz->valorContabil(), $nfSefaz->fornecedor()->CNPJ);
                        }
        
                        else {
                            $aa1agedt=new AA1AGEDT();
                            $aa1agedt->dobraNota($loja, 10, $_POST['nfNumber'], '1  ', $cd, $nfSefaz->valorContabil(), $nfSefaz->fornecedor()->CNPJ);
                        }
        
                    }
                } else if ($storePortal->group_id == 2) {
                    $aa2ctipo=(new AA2CTIPO2());
                    $loja = $aa2ctipo->find("TIP_CODIGO = $nfSefaz->store_id")->fetch();
                    $loja = $loja->TIP_CODIGO.$loja->TIP_DIGITO;
                    $cd = $aa2ctipo->find("TIP_CGC_CPF = ".$nfSefaz->fornecedor()->CNPJ."")->fetch()->TIP_CODIGO;
                    $aa1agedt = new AA1AGEDT2();
        
                    if ($nfSefaz->fornecedor()->CNPJ == '10838648000195'|| $nfSefaz->fornecedor()->CNPJ == '10838648000519'|| $nfSefaz->fornecedor()->CNPJ == '10838648000357') {
                        if ($nfSefaz->store_id==74) {
                            $aa1agedt=new AA1AGEDT2();
                            $aa1agedt->dobraNota($loja, 143, $$_POST['nfNumber'], '1  ', $cd, $nfSefaz->valorContabil(), $nfSefaz->fornecedor()->CNPJ);
                        }
        
                        else {
                            $aa1agedt=new AA1AGEDT2();
                            $aa1agedt->dobraNota($loja, 10, $_POST['nfNumber'], '1  ', $cd, $nfSefaz->valorContabil(), $nfSefaz->fornecedor()->CNPJ);
                        }
        
                    }
                } else if ($storePortal->group_id == 10) {
                    $aa2ctipo=(new AA2CTIPO10());
                    $loja = $aa2ctipo->find("TIP_CODIGO = $nfSefaz->store_id")->fetch();
                    $loja = $loja->TIP_CODIGO.$loja->TIP_DIGITO;
                    $cd = $aa2ctipo->find("TIP_CGC_CPF = ".$nfSefaz->fornecedor()->CNPJ."")->fetch()->TIP_CODIGO;
                    $aa1agedt = new AA1AGEDT10();
        
                    if ($nfSefaz->fornecedor()->CNPJ == '48852264000108') {
                        if ($nfSefaz->store_id==74) {
                            $aa1agedt=new AA1AGEDT10();
                            $aa1agedt->dobraNota($loja, 143, $$_POST['nfNumber'], '1  ', $cd, $nfSefaz->valorContabil(), $nfSefaz->fornecedor()->CNPJ);
                        }
        
                        else {
                            $aa1agedt=new AA1AGEDT10();
                            $aa1agedt->dobraNota($loja, 10, $_POST['nfNumber'], '1  ', $cd, $nfSefaz->valorContabil(), $nfSefaz->fornecedor()->CNPJ);
                        }
        
                    }
                }
                
    
                if ($type !=3 && $type !=10) {
                    $dueDate=(new NfDueDate())->find("nf_id = $id")->count();
    
                    if ($dueDate==0) {
                        $venc=(new NfReceipt())->findById($id);
                        $venc->destroy();
                        $data['success']=false;
                        $data['message']="Vencimento não informado!";
                        echo json_encode($data);
                    }  else {
                        if ($_POST['code'][0] != '') {
                            for ($j=0;$j<=count($_POST['code']);$j++) {
                                if ($_POST['code'][$j] == ''){
        
                                } else {
                                    
                                    if($_POST['code'][$j] > 0){
                                    $nfReceiptCode = new NfReceiptCodes();
                                    $nfReceiptCode->nf_receipt_id = $id;
                                    $nfReceiptCode->code = $_POST['code'][$j];
                                    $nfReceiptCodeId = $nfReceiptCode->save();
                                    }
                                }
                            }
                        }

                        $data['success']=true;
                        $data['message']="solicitação de lançamento ".$add['value']." aberto com sucesso! $note";
                        echo json_encode($data);
                    }
                } else {
                    if ($_POST['code'][0] != '') {
                        for ($j=0;$j<=count($_POST['code']);$j++) {
                            if ($_POST['code'][$j] == ''){
    
                            } else {
                                if($_POST['code'][$j] > 0){
                                    $nfReceiptCode = new NfReceiptCodes();
                                    $nfReceiptCode->nf_receipt_id = $id;
                                    $nfReceiptCode->code = $_POST['code'][$j];
                                    $nfReceiptCodeId = $nfReceiptCode->save();
                                }
                                
                            }
                        }
                    }
                   
                    $data['success']=true;
                    $data['message']="solicitação de lançamento $id aberto com sucesso! $note";
                    echo json_encode($data);
                }
    
    
            } else {
                $data['success'] = $add['value'];
                $data['message']= $add['msg'];
                echo json_encode($data);
            }
        }
    }

    public function addNfReceiptAuto2() {
            if ((!isset($_COOKIE['login']) == true)) {
                $this->router->redirect("/login");
            } else {
                $identification = $_COOKIE['login'];
                $usuario = (new User())->find("email = :email","email=$identification")->fetch();
                $class = $usuario->class;
                $store = $usuario->store_code;
                $accessKey = $_POST['accessKey'];
                $nfSefaz = (new NfSefaz())->find("access_key = :access_key","access_key=$accessKey")->fetch();
                $dueDate1 = $_POST['dueDate1'];
                $dueDate2 = $_POST['dueDate2'];
                $dueDate3 = $_POST['dueDate3'];
                if (strlen($dueDate1) < 10 &&  $_POST['type'] != 3 &&  $_POST['type'] != 10) {
                    $data['success'] = false;
                    $data['message'] = "Informe os Vencimentos da Nota Fiscal.";
                    echo json_encode($data);
                    die;
                }
                if ($nfSefaz->getEntradaSaida == 'E') {
                    $data['success'] = false;
                    $data['message'] = "Nota Fiscal de entrada não permitida para postagem!";
                    echo json_encode($data);
                    return;
                }
                $nfeOperation = $_POST['nfeOperation'];
                $type = $_POST['type'];
                $xml = simplexml_load_file(__DIR__."/../XML/".$store.'/NFe'.$accessKey.'.xml');
    
                $cnpj = (string)$xml->NFe->infNFe->emit->CNPJ;
                $totalNota = $xml->NFe->infNFe->total->ICMSTot->vNF;
    
                $codigos = $xml->NFe->infNFe->det;
                $compra = false;
                foreach ($codigos as $codigo) {
                    if ($codigo->prod->CFOP == 5902 || $codigo->prod->CFOP == 5908) {
                        $outro = true;
                    } else if ($codigo->prod->CFOP == 5910 || $codigo->prod->CFOP == 6910) {
                        $bonificacao = true;
                    } else if ($codigo->prod->CFOP == 5949 || $codigo->prod->CFOP == 6949) {
                        $type = 10;
                        $outro = true;
                    } else {
                        $compra = true;
                    }
                    
                    $code = $codigo->prod->cProd;
                    
                    $count = (new ExchangeCode());
                    $count = $count->find("store_id = :store and cnpj = :cnpj and source_code = :code","store=$store&cnpj=$cnpj&code=$code")->count();
                    if ($count > 0) {
                        $exchangeCode = (new ExchangeCode())->find("store_id = :store and cnpj = :cnpj and source_code = :code","store=$store&cnpj=$cnpj&code=$code")->fetch();
                        $codigo->prod->cEAN = str_replace('-','',$exchangeCode->destination_code);
                        $codigo->prod->cEANTrib = str_replace('-','',$exchangeCode->destination_code);
                        $xml->asXML(__DIR__."/../XML/".$store.'/NFe'.$accessKey.'.xml');
                        if ($nfeOperation == 2) {
                            $nfeOperation = 10;
                        } else {
                            $nfeOperation = 9;
                        }
                    }
                    
                }
                if ($type == 4){
                    $operation = 7;
                }
                
                
                $dobleNfe = (new NfReceipt())->find("access_key = :access_key","access_key=$accessKey")->count();
                $fileUpload = new File("storage", "files");
                $nfNumber = $_POST['nfNumber'];
                $client = $usuario->id;
                $store_group = $usuario->store_group_id;
                $note = nl2br($_POST['note']);
                $arquivo = __DIR__."/../XML/$usuario->store_code/NFe$accessKey.xml";
                $i = 0;
                if (file_exists($arquivo)) {
                    $xml1 = simplexml_load_file($arquivo);
                    $xml = $xml1->NFe->infNFe;
                    foreach($xml->det as $det){
                        $i++;
                    }
                    $itemsQuantity  = $i;
                    $xml = $xml1->NFe->infNFe;
                    $i = 1;
                }
                if ($note == '') {
                    $note = $nfNumber;
                }
                if ($dueDate1 == '' && $type != 3  && $type != 10) {
                        $data['success'] = false;
                        $data['message'] = "Informe os Vencimentos da Nota Fiscal.";
                        echo json_encode($data);
                }    
                if ($nfNumber == '') {
                    $data['success'] = false;
                    $data['message'] = "Informe o número do documento.";
                    echo json_encode($data);
                } else {
                    if ($dobleNfe > 0) {
                        $data['success'] = false;
                        $data['message'] = "NFe já postada.";
                        echo json_encode($data);
                    } else {
                        if ($type != 10 && $type != 3 && !$_POST['dueDate1']) {
                            $data['success'] = false;
                            $data['message'] = "Informe pelo menos um vencimento.";
                            echo json_encode($data);
                            die;
                        } else {
                            if ($itemsQuantity == 0) {
                                $itemsQuantity = 1;
                            }
                            $cnpj = substr($accessKey,6,14);
                            //incluindo o solicitação de lançamento efetivamente
                            if ($cnpj == '10838648000195' || $cnpj == '10838648000519' || $cnpj == '10838648000357') {
                                $nfeOperation = 7;
                                $type = 4;
                            }
                            if ($store == 299 || $store == 531 || $store == 450) {
                                $request = new NfReceipt2();
                            } else {
                                $request = new NfReceipt();
                            }
                            $request->store_id = $store;
                            $request->group_id = $store_group;
                            $request->nf_number = $nfNumber;
                            $request->access_key = $accessKey;
                            if($class == "C2") {
                                $request->status_id = 10;
                            } else {
                                $request->status_id = 1;
                            }
                            $request->note = nl2br($note);
                            $request->client_id = $client;
                            $request->operator_id = 147;
                            $request->items_quantity = $itemsQuantity;
                                if ($bonificacao && !$compra) {
                                    $type = 3;
                                }
                                if ($outro == true && $type != 10 && !$compra) {
                                    $type = 7;
                                }
                                $request->type_id = $type;
                                $request->operation_id = $nfeOperation;
                                $request->inclusion_type = 2;
                                $receipt =  (new NfReceipt())->find("access_key = :ak","ak=$accessKey")->count();
                                if ($receipt == 0){
                                    $requestId = $request->save2();
                                    if (strlen($accessKey) <> 44) {
                                        $request->access_key = $request->id;
                                        $requestID = $request->save();
                                    }
                                } else {
                                    $data['success'] = false;
                                    $data['message'] = "NFe já postada.";
                                    echo json_encode($data);
                                }
                                
                                if ($requestId) {
                                    $logger = new Logger("web");
                                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                    if ($request->store()->telegram != '') {
                                        $tele_channel = $request->store()->telegram;
                                    } else {
                                        $tele_channel = "@BDTecLancamentos";
                                    }
                                    
                                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                    $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                                    $logger->pushHandler($tele_handler);
                                    $logger->info("Solicitação de lançamento $request->id aberta! - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$request->id");
                                    $nfEvent = new NfEvent();
                                    $nfEvent->id_nf_receipt = $request->id;
                                    if($class == "C2") {
                                        $nfEvent->status_id = 10;
                                    } else {
                                        $nfEvent->status_id = 1;
                                    }
                                    $nfEvent->user_id = $client;
                                    $nfEvent->situation = 'A';
                                    $nfEventId = $nfEvent->save();
                                    $checkRow = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$cnpj&store_id=$store")->count();
                                    #FAZER O TROCA CODIGO AQUI
                                    if($checkRow > 0){
                                        $exchangeCode = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$cnpj&store_id=$store")->fetch(true);
                                        foreach($exchangeCode as $assoc) {
                                            foreach( $xml1->det as $xml1){
                                                if($xml1->prod->cProd == str_replace('-','',$assoc->source_code)){
                                                    $verificacao = true;
                                                    $xml1->prod->cEAN = $assoc->destination_code;
                                                    $xml = $xml->asXML(__DIR__."/../XML/".$store.'/'.$accessKey.'.xml');
                                                }
                                            }
                                        }
                                        $xml = $xml->asXML(__DIR__."/../XML/".$store.'/'.$accessKey.'.xml');
                                    }
                                    $nfStartedCount = (new NfStarted())->find("access_key = :access_key","access_key=$accessKey")->count();
                                    if($nfStartedCount>0){
                                        $nfStarted = (new NfStarted())->find("access_key = :access_key","access_key=$accessKey")->fetch();
                                        $requestStarted = (new NfReceipt())->findById($request->id);
                                        $requestStarted->status_id = $nfStarted->status_id;
                                        $requestStarted->operator_id = $nfStarted->operator_id;
                                        $requestStartedId = $requestStarted->save();
                                        if (strlen($nfStarted->note)>1){
                                            $note = $nfStarted->note;
                                            $interactionStarted = (new Interaction());
                                            $interactionStarted->id_nf_receipt = $request->id;
                                            $interactionStarted->id_user = $nfStarted->operator_id;
                                            $interactionStarted->description = $nfStarted->note;
                                            $interactionStartedId = $interactionStarted->save();
                                        }
                                    }
    
    
                                    $nfSefaz = (new NfSefaz())->find("access_key = :access_key","access_key=$accessKey")->fetch();
                                    $nfSefazId = (new NfSefaz())->findById($nfSefaz->id);
                                    $nfSefazId->status_id = 2;
                                    $nfSefazId2 = $nfSefazId->save();
                                    $id = $request->id;
                                    if ($compra && $type != 3 && $type != 10) {
                                        if ($dueDate1){                              
                                            $nfDueDate1 = (new NfDueDate());
                                            $nfDueDate1->nf_id = $id;
                                            $nfDueDate1->due_date = $dueDate1;
                                            $nfDueDateId1 = $nfDueDate1->save();
                                        }
                                        if ($dueDate2){
                                            $nfDueDate2 = (new NfDueDate());
                                            $nfDueDate2->nf_id = $id;
                                            $nfDueDate2->due_date = $dueDate2;
                                            $nfDueDateId2 = $nfDueDate2->save();
                                        }
                                        if ($dueDate3){
                                            $nfDueDate3 = (new NfDueDate());
                                            $nfDueDate3->nf_id = $id;
                                            $nfDueDate3->due_date = $dueDate3;
                                            $nfDueDateId3 = $nfDueDate3->save();
                                        }
                                    }                                
                                    $nfReceiptError = (new NfReceipt())->findById($id);
                                    if($nfReceiptError->error() == true && $nfReceiptError->type_id != 4 && $nfReceiptError->type_id != 6 && $nfReceiptError->type_id != 7 && ($nfReceiptError->group_id == 1 or $nfReceiptError->group_id ==  10 or $nfReceiptError->group_id == 2) && $nfReceiptError->type_id != 1) {
                                        $nfReceiptError->error_cost = 'E';
                                        $nfReceiptErrorId = $nfReceiptError->save();
                                        $logger = new Logger("web");
                                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                        $tele_channel = "1247513338";
                                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                        $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                                        $logger->pushHandler($tele_handler);
                                        $logger->info(URL_BASE."/request/interacao/$id");
                                    }
                                    if($nfReceiptError->type_id == 2) {
                                    $interaction = (new Interaction());
                                    $interaction->id_nf_receipt = $nfReceiptError->id;
                                    $interaction->id_user = $nfReceiptError->operator_id;
                                    //se a nota for do grupo 1 ou 10 entre
                                    if ($nfReceiptError->group_id == 1 or $nfReceiptError->group_id  == 10) {
                                            //percorra os itens da nota
                                        foreach ($nfReceiptError->getItems() as $det) {
                                        //se a loja for 61 entre
                                            if (( $nfReceiptError->store_id == 61
                                                || $nfReceiptError->store_id == 108 
                                                || $nfReceiptError->store_id == 56 
                                                || $nfReceiptError->store_id == 57 
                                                || $nfReceiptError->store_id == 58 
                                                || $nfReceiptError->store_id == 59 
                                                || $nfReceiptError->store_id == 80 
                                                || $nfReceiptError->store_id == 82 
                                                || $nfReceiptError->store_id == 85 
                                                || $nfReceiptError->store_id == 72 
                                                || $nfReceiptError->store_id == 78 
                                                || $nfReceiptError->store_id == 76 
                                                || $nfReceiptError->store_id == 53)) {
                                                //se o produto tiver ean "SEM GTIN" ou codigo ean invalido (menor que 8 digitos) entre                                                
                                                if($det->prod->cEAN == "SEM GTIN" || strlen($det->prod->cEAN) < 8){
                                                        
                                                        $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF =  $nfReceiptError->getCnpjFornecedor")->fetch();
                                                        //se o fornecedor existir entre
                                                        if ($aa2ctipo) {
                                                            $aa3citem = (new AA3CITEM())->find("GIT_REFERENCIA = '".$det->prod->cProd."' GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                                                            if (!$aa3citem) {
                                                                $aa1forit = (new AA1FORIT())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
                                                                //se não houver aa1forit entre
                                                                if (!$aa1forit) {
                                                                    $aa1refit = (new AA1REFIT())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
            
                                                                    if (!$aa1refit) {
                                                                        (new ExchangeCode())->find("store_id = $nfReceiptError->store_id and cnpj = $nfReceiptError->getCnpjFornecedor and source_code = ".$det->prod->cProd)->fetch();
                                                                        if (!$exchangeCode){
                                                                            $boningOx = (new BoningOx())->find("reference = '".$det->prod->cProd."' and cnpj = $nfReceiptError->getCnpjFornecedor")->count();
                                                                            if ($boningOx == 0) {
                                                                                $descriptionError .=  " Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
                                                                            }                                                                     
                                                                        } else {
                                                                            if ($nfReceiptError->group_id == 1) {
                                                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                                $output = shell_exec(__DIR__."/../importacao.bat");
                                                                            } else if ($nfReceiptError->group_id == 2) {
                                                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                                $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                            } else if ($nfReceiptError->group_id == 10) {
                                                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                                $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                            }
                                                                            
        
                                                                            
                                                                        }
                                                                    // senão jogue o xml na pasta
                                                                    } else {
                                                                        if ($nfReceiptError->group_id == 1) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao.bat");
                                                                        } else if ($nfReceiptError->group_id == 2) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                        }
        
                                                                    } 
                                                                // senão jogue o xml na pasta                                                         
                                                                } else {
                                                                    if ($nfReceiptError->group_id == 1) {
                                                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                        $output = shell_exec(__DIR__."/../importacao.bat");
                                                                    } else if ($nfReceiptError->group_id == 2) {
                                                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                        $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                    } else if ($nfReceiptError->group_id == 10) {
                                                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                        $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                    }
                                                                }
                                                            }
                                                        // se o fornecedor não existir entre
                                                        } else {
                                                            $description =  " Fornecedor ".$nfReceiptError->getCnpjFornecedor." sem Cadastro.<br />";
                                                        }
                                                    //senão (se tiver EAN valido)
                                                    } else {
                                                        $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF =  $nfReceiptError->getCnpjFornecedor")->fetch();
    
                                                        // se o fornecedor existir entre
                                                        if ($aa2ctipo) {
                                                            $aa3ccean = (new AA3CCEAN())->find("EAN_COD_EAN = ".$det->prod->cEAN)->fetch();
                                                            //se não tiver o EAN cadastrado na AA3CCEAN entre
                                                            if (!$aa3ccean) {
                                                                $aa3citem = (new AA3CITEM())->find("GIT_REFERENCIA = '".$det->prod->cProd."' GIT_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch();
                                                                if (!$aa3citem) {
                                                                    $aa1forit = (new AA1FORIT())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
                                                                    //se não houver aa1forit entre
                                                                    if (!$aa1forit) {
                                                                        $aa1refit = (new AA1REFIT())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
                                                                        // se não houver aa1refit entreentre
                                                                        if (!$aa1refit) {
                                                                            $exchangeCode = (new ExchangeCode())->find("1=0")->fetch();
                                                                            //se não houver troca codigo entre
                                                                            if (!$exchangeCode){
                                                                                //adicione uma linha a produtos sem cadastro (description);
                                                                                $boningOx = (new BoningOx())->find("reference = '".$det->prod->cProd."' and cnpj = $nfReceiptError->getCnpjFornecedor")->count();
                                                                            if ($boningOx == 0) {
                                                                                $descriptionError .=  " Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
                                                                            } 
                                                                            // senão jogue o xml na pasta
                                                                            } else {
                                                                                if ($nfReceiptError->group_id == 1) {
                                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                                    $output = shell_exec(__DIR__."/../importacao.bat");
                                                                                } else if ($nfReceiptError->group_id == 2) {
                                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                                    $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                                } else if ($nfReceiptError->group_id == 10) {
                                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                                    $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                                }
                                                                            }
                                                                        // senão jogue o xml na pasta
                                                                        } else {
                                                                            if ($nfReceiptError->group_id == 1) {
                                                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                                $output = shell_exec(__DIR__."/../importacao.bat");
                                                                            } else if ($nfReceiptError->group_id == 2) {
                                                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                                $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                            } else if ($nfReceiptError->group_id == 10) {
                                                                                copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                                $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                            }
        
                                                                        } 
                                                                    // senão jogue o xml na pasta                                                         
                                                                    } else {
                                                                        if ($nfReceiptError->group_id == 1) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao.bat");
                                                                        } else if ($nfReceiptError->group_id == 2) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                        } else if ($nfReceiptError->group_id == 10) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                        }
                                                                    }
                                                                } else {
                                                                    if ($nfReceiptError->group_id == 1) {
                                                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                        $output = shell_exec(__DIR__."/../importacao.bat");
                                                                    } else if ($nfReceiptError->group_id == 2) {
                                                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                        $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                    } else if ($nfReceiptError->group_id == 10) {
                                                                        copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                        $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                    }
                                                                }
    
                                                            // senão jogue o xml na pasta
                                                            } else {
                                                                if ($nfReceiptError->group_id == 1) {
                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                    $output = shell_exec(__DIR__."/../importacao.bat");
                                                                } else if ($nfReceiptError->group_id == 2) {
                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                    $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                } else if ($nfReceiptError->group_id == 10) {
                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                    $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                }
                                                            }
        
                                                        // se o fornecedor não existir entre
                                                        } else {
                                                            $description =  " Fornecedor ".$nfReceiptError->getCnpjFornecedor." sem Cadastro.<br />";
                                                        }
                                                    }
                                                   
                                                }
                                                
                                            }
                                    }
                                    if ($store_group == 2) {
                        
                                            foreach ($nfReceiptError->getItems() as $det) {
                                                if (( $nfReceiptError->access_key == '29220212275487000102550020000918461002449238')) {                                                
                                                    if($det->prod->cEAN == "SEM GTIN" || strlen($det->prod->cEAN) < 8){
                                                        $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF =  $nfReceiptError->getCnpjFornecedor")->fetch();
                                                        if ($aa2ctipo) {
                                                            
                                                            $aa1forit = (new AA1FORIT2())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                                            if (!$aa1forit) {
                                                                $aa1refit = (new AA1REFIT2())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
        
                                                                if (!$aa1refit) {
                                                                    (new ExchangeCode())->find("store_id = $nfReceiptError->store_id and cnpj = $nfReceiptError->getCnpjFornecedor and source_code = ".$det->prod->cProd)->fetch();
                                                                    if (!$exchangeCode){
                                                                        
                                                                        $description .=  " Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
    
                                                                        
                                                                    } else {
                                                                        if ($nfReceiptError->group_id == 1) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao.bat");
                                                                        } else if ($nfReceiptError->group_id == 2) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                        } else if ($nfReceiptError->group_id == 10) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                        }
                                                                    }
                                                                }
                                                            }
        
                                                        }
                                                    } else {
                                                        $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF =  $nfReceiptError->getCnpjFornecedor")->fetch();
                                                        
                                                        if ($aa2ctipo) {
                                                            $aa3ccean = (new AA3CCEAN2())->find("EAN_COD_EAN = ".$det->prod->cEAN)->fetch();
                                                            if (!$aa3ccean) {
                                                                $aa1forit = (new AA1FORIT2())->find("FORITE_REFERENCIA LIKE '".$det->prod->cProd."%' AND FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO")->fetch(true);
                                                                if (!$aa1forit) {
                                                                    $aa1refit = (new AA1REFIT2())->find("REF_REFERENCIA LIKE '".$det->prod->cProd."%' AND REF_COD_FOR = $aa2ctipo->TIP_CODIGO")->fetch(true);
                                                                    if (!$aa1refit) {
                                                                        $exchangeCode = (new ExchangeCode())->find("1=0")->fetch();
                                                                    if (!$exchangeCode){
                                                                        $description .=  " Produto referencia ".$det->prod->cProd." descricao ".$det->prod->xProd." sem referencia cadastradas.<br />";
    
                                                                    } else {
                                                                        if ($nfReceiptError->group_id == 1) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao.bat");
                                                                        } else if ($nfReceiptError->group_id == 2) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                        } else if ($nfReceiptError->group_id == 10) {
                                                                            copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                            $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                        }
                                                                    }
                                                                    }                                                            
                                                                }
                                                            } else {
                                                                if ($nfReceiptError->group_id == 1) {
                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR/NFe$nfReceiptError->access_key.xml");
                                                                    $output = shell_exec(__DIR__."/../importacao.bat");
                                                                } else if ($nfReceiptError->group_id == 2) {
                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                    $output = shell_exec(__DIR__."/../importacao2.bat");
                                                                } else if ($nfReceiptError->group_id == 10) {
                                                                    copy( "/../XML/$nfReceiptError->store_id/NFe$nfReceiptError->access_key.xml","/../XML_IMPORTAR_BD/NFe$nfReceiptError->access_key.xml");
                                                                    $output = shell_exec(__DIR__."/../importacao10.bat");
                                                                }
                                                            }
        
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
    
                                        if ($descriptionError) {
                                            $interaction->description = "Gentileza enviar fotos dos produtos abaxo pois não constam na nossa base de dados <br />".$descriptionError;
                                            $interactionId = $interaction->save();
                                            $pendencia = (new NfReceipt())->findById($id);
                                            $pendencia->status_id = 2;
                                            $pendenciaId = $pendencia->save();
                                        }
                                    }
                                    
                                    $aa2ctipo = (new AA2CTIPO());
                                    $loja = $aa2ctipo->find("TIP_CODIGO = $store")->fetch();
                                    $loja = $loja->TIP_CODIGO.$loja->TIP_DIGITO;
                                    $cd = $aa2ctipo->find("TIP_CGC_CPF = $cnpj")->fetch()->TIP_CODIGO;
                                    $aa1agedt = new AA1AGEDT();
    
                                    if ($cnpj == '10838648000195' || $cnpj == '10838648000519' || $cnpj == '10838648000357') {     
                                        if ($store == 74) {
                                            $aa1agedt = new AA1AGEDT();
                                            $aa1agedt->dobraNota($loja, 143, $nfNumber, '1  ', $cd, $totalNota,$cnpj);
                                        } else {
                                            $aa1agedt = new AA1AGEDT();
                                            $aa1agedt->dobraNota($loja, 10, $nfNumber, '1  ', $cd, $totalNota,$cnpj);
                                        }                           
                                        
                                    }
                                    if ($type != 3 && $type != 10) {
                                        $dueDate = (new NfDueDate())->find("nf_id = $id")->count();
                                        if ($dueDate == 0) {
                                            $venc = (new NfReceipt())->findById($id);
                                            $venc->destroy();
                                            $data['success'] = false;
                                            $data['message'] = "Vencimento não informado!";
                                            echo json_encode($data);
                                        } else {
                                            $data['success'] = true;
                                            $data['message'] = "solicitação de lançamento $id aberto com sucesso! $note";
                                            echo json_encode($data);
                                        }
                                    } else {
                                        $data['success'] = true;
                                        $data['message'] = "solicitação de lançamento $id aberto com sucesso! $note";
                                        echo json_encode($data);
                                    }
                                    
        
                                } else{
                                    $data['success'] = false;
                                    $data['message'] = "NFe já postada.";
                                    echo json_encode($data);
                                }                         
                            }
                        }
                    }
             
            }
    }

    public function requestUpdate() {
           
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $user = $usuario->id;
        $class = $usuario->class;
        $storeId = $usuario->store_code;
        $operation = $_POST['operation'];
        $status = $_POST['status'];
        $operator = $_POST['operator'];
        $id = $_POST['id'];
        /**
         * Selecionando tabela de solicitações de lançamento
         */
        $request = (new NfReceipt())->findByID($id);

        if($status == 3) {
            /**
            * Verificar se os codigos internos estão na nota fiscal
            */
            if ($request->group_id != 9) {
                foreach($request->getCodes() as $code) {
                    if (is_array($request->retArrayCodes())) {
                        if (!in_array($code->code, $request->retArrayCodes())) {
                            $return['success'] = false;
                            $return['message'] = "Codigo $code->code informado pelo cliente não consta na NFE";
                            echo json_encode($return);
                            die;
                        } 
                    }
                }
            }
        }
     
        if ($request->operator_id == $user or $request->operator_id == 147 or $usuario->libera == 'S') {
            if ($operator != '') {
                /**
                 * Verificação do parametro para checar se existe algum bloqueio via parametro para a alteração.
                 */
                if ($request->operator_id != $operator) {
                    $parameters = new Parameter();
                    if (!$parameters->veification($operator,$request)['success']) {
                        $return['success'] = false;
                        $return['message'] = "Alteração não realziada! ".$parameters->veification($operator,$request)["message"];
                        echo json_encode($return);
                        return;
                    } else if ($request->operator_id != 147 && $usuario->libera != 'S') {
                       $return['success'] = false;
                       $return['message'] = "Alteração não realziada! Nota já alocada.";
                       echo json_encode($return);
                       return;
                    } else {
                       if ($status == 3) {
                        $request->operator_id = $user;
                       } else {
                        $request->operator_id = $operator;
                       }
                    }
                }
            }
    
        
            if ($status != '') {
                /**
                 * Veificação do status Pendente Cliente para definir a operação para 1 caso seja retorno.
                 */
                if ($request->status_id == 2 && $status != 2) {
                    $request->operation_id = 1;
                }
        
                if ($status == 3) {
                    
                    if ($request->group_id == 1 or $request->group_id == 10 or $request->group_id == 2 or $request->group_id == 9) {
                        if(empty($request->validacaoPreco)) {
                            // $logger = new Logger("web");
                            // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            // $tele_channel = "-741960561";
                            // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            // $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%",null,true));
                            // $logger->pushHandler($tele_handler);
                            // $logger->info("Solicitação de lançamento $id : Loja $request->store_id - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$id");
                        }
                    }
                    
                    //não atualizada
                    if (!$request->updated && $request->store_id != 1002  && $request->store_id != 1004 && $request->store_id != 1001 && $request->store_id != 1003) {
                        $return['success'] = false;
                        $return['message'] = "Nota Fiscal não atualizada no Retaguarda!";
                        echo json_encode($return);
                        return;
                    } else {
                        
                    }
                    //erro de custo
                    if ($request->error_cost == 'E') {
                        $return['success'] = false;
                        $return['message'] = "NFe Bloqueada!";
                        echo json_encode($return);
                        return;
                    }                  
                    //vencimento divergente
                    if ($request->store_id != 1004 && $request->group_id != 10 && $request->store_id != 1002 && $request->store_id != 1001 && $request->store_id != 1003) {             
                        /**
                         * ! Verificar se a nota é diferente de outros e avulsa
                         */
                        if ($request->type_id != 1 && $request->type_id != 10) {
                            /**
                             * Verificar se a quntidade de vencimentos bate
                             */
                            if ($request->getDuedateAmount != $request->updated()->getDueDateAmount) {
                                $return['success'] = false;
                                $return['message'] = "Nota Fiscal com vencimento divergente do Retaguarda! portal : 0$request->getDuedateAmount vencimentos Retaguarda : 0".$request->updated()->getDueDateAmount." vencimentos";
                                echo json_encode($return);
                                return;         
                            } else {  
                                /**
                                 *  Verificar se os vencimentos batem
                                 */     
                                if ($request->group_id == 3) {
                                    $i = 1;
                                    foreach($request->dueDate as $dd){
                                        $portal[$i] = $dd->date1;
                                        $i++;
                                    }
        
                                    $i = 1;
                                    
                                    foreach($request->updated()->getDueDate as $dd2){
                                        $Retaguarda[$i] = $dd2->DTAVENCIMENTO;
                                        $i++;
                                    }
                                } else {
                                    $i = 1;
                                    foreach($request->dueDate as $dd){
                                        $portal[$i] = inputDateToRMSDate($dd->due_date);
                                        $i++;
                                    }
        
                                    $i = 1;
                                    foreach($request->updated()->getDueDate as $dd2){
                                        $Retaguarda[$i] = str_replace('/','-',$dd2->FIV_DTA_VENCTO);
                                        $i++;
                                    }
                                }
                                $v = true;
                                for($x=1;$x<=$i;$x++){
                                    if(strtoupper($portal[$x]) != $Retaguarda[$x]) {
                                        $v = false;
                                        $msgv .= strtoupper($portal[$x])." != ".$Retaguarda[$x]."|";
                                    }
                                }                        
                                if(!$v){
                                    $return['success'] = false;
                                    $return['message'] = "Nota Fiscal com vencimento divergente do Retaguarda! $msgv";
                                    echo json_encode($return);
                                    return;
                                }
                            }
                        }
        
        
                    } 
                    // fornecedor fabricante simples nacional com credito diferente de 12
                    if ($request->store_id != 1004  && $request->group_id != 10 && $request->store_id != 250 && $request->store_id != 1002 && $request->store_id != 2003 && $request->store_id != 1001 && $request->store_id != 1003 && $request->store_id != 33  && $request->type_id == 1  && $request->type_id == 2  && $request->type_id == 4 && $request->getSimplesFabricante && $request->AA1CFISC()->ag1iensa2()->ag1iensaCredito) {
                        $return['success'] = false;
                        $return['message'] = "Nota Fiscal Simples Nacional Fabricante com menos de 12% de Credito!";
                        echo json_encode($return);
                        return;
                    }
                         

                }
                
                if($status == 2) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    if ($request->store()->telegram != '') {
                        $tele_channel = $request->store()->telegram;
                    } else {
                        $tele_channel = "@BDTecLancamentos";
                    }
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Solicitação de lançamento $id alterada para Pendente Cliente: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$id");
                }   
                //tudo ok
                $request->status_id = $status;  
            }
        
            if ($operation != '') {
                $request->operation_id = $operation;
            }

            $requestId = $request->saveLog($user,$operator, $request->nf_number, $request->access_key, $request->status_id, $request->items_quantity, $request->type_id, $request->operation_id,$request->note, $request->id);
            $return['success'] = true;
            $return['message'] = "Alteração realizada!";
            echo json_encode($return);
            return;
        } else {
            $return['success'] = false;
            $return['message'] = "Nota Fiscal esta alocada a $request->getOperator";
            echo json_encode($return);
        }
    }

    public function nfReprocess() {
        $loja = $_POST['store'];
        $chave = $_POST['key'];
        $xml = simplexml_load_file('/../XML/'.$loja.'/'.$chave.'.xml');
        $chave = $xml->NFe->infNFe['Id'][0];
        $dhEmi = $xml->NFe->infNFe->ide->dhEmi;
        if(strlen($chave)==47){                    
            $xml1 = $xml->NFe->infNFe;
            foreach($xml1->emit as $cnpj){
                $CNPJ = $cnpj->CNPJ;
            }
            simplexml_load_file('/../XML/'.$loja.'/'.$chave.'.xml');
            $parametroRow = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
            #FAZER O TROCA EMBALAGEM AQUI
            if($parametroRow > 0){
                $exchangePackagings = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                foreach ($exchangePackagings as $assoc) {
                    foreach( $xml1->NFe->infNFe->det as $xml2){
                        if($xml2->prod->cProd == str_replace('-','',$assoc->code) &&  $xml2->prod->uCom == $assoc->packing_of){
                            /*
                            * Embalagem
                            */
                            $xml2->prod->uCom = $assoc->packing_for;
                            $xml2->prod->uTrib = $assoc->packing_for;
                            /*
                             * Custo unitario
                             */
                            $xml2->prod->vUnCom = floatval((floatval($xml2->prod->vUnCom)*floatval($xml2->prod->qCom))/$assoc->packing_for);
                            $xml2->prod->vUnTrib = floatval((floatval($xml2->prod->vUnTrib)*floatval($xml2->prod->qTrib))/$assoc->packing_for);
                            /*
                             * Quantidade
                             */
                            $xml2->prod->qCom = $xml2->prod->qCom/$assoc->packing_for;
                            $xml2->prod->qTrib = $xml2->prod->qTrib/$assoc->packing_for;
                            $xml1->asXML('/../XML/'.$loja.'/'.$chave.'.xml');
                        }
                    }
                }
            }
            $checkRow = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
            #FAZER O TROCA CODIGO AQUI
            if($checkRow > 0){
                $exchangeCode = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                foreach($exchangeCode as $assoc) {
                    foreach( $xml1->det as $xml1){
                        if($xml1->prod->cProd == str_replace('-','',$assoc->source_code)){
                            $verificacao = true;
                            $xml1->prod->cEAN = $assoc->destination_code;
                            $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                        }
                    }
                }
                $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
            }
        }
    }

    public function editNfReceipt() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $user = $usuario->id;
        $class = $usuario->class;
        $name = $usuario->name;
        $nfNumber = $_POST['nfNumber'];
        $accessKey = $_POST['accessKey'];
        $operatorId = $_POST['operatorId'];
        $status = $_POST['status'];
        $itemsQuantity = $_POST['itemsQuantity'];
        $type= $_POST['type'];
        $operationId = $_POST['operationId'];
        $note = $_POST['note'];
        $id = $_POST['id'];
        $amount = $_POST['amount'];
        $reason = $_POST['cad'];
        $request = (new NfReceipt())->findById($id);
        $i = 1;
        foreach($request->dueDate as $dueDate) {
            $dd = (new NfDueDate())->findById($dueDate->id);
            $dd->due_date = $_POST['dueDate'.$i];
            $ddId = $dd->save();
            $i++;
        }
        if ($status == '') {
            $status = $request->status_id;
        }
        if ($status == 9 ){
            $rai = new RegisterAmountItems();
            $rai->nf_receipt_id = $id;
            $rai->amount = $amount;
            $raiId = $rai->save();
        } else if ($status == 2) {
            $rai = new ReasonPendingClient();
            $rai->nf_receipt_id = $id;
            $rai->amount = $amount;
            $rai->reason_id = $reason;
            $raiId = $rai->save();

        }
        if ($request->operator_id != $user  && $request->operator_id != 147 && $usuario->libera != 'S') {
            $return['success'] = false;
            $return['message'] = "Alteração não realizada! Nota já alocada.";
            echo json_encode($return);
            return;
        }
        $request->nf_number = $nfNumber;
        $request->access_key = $accessKey;
        if ($status == 4 || $status == 3 ) {
            $operatorId = $user;
        } else if ($operatorId == null) {
            $operatorId = $request->operator_id;
        }

        
        $request->operator_id = $operatorId;

        $request->status_id = $status;

        $request->items_quantity = $itemsQuantity;
        $request->type_id = $type;
        $request->operation_id = "$operationId ";
        $request->note = $note;
        if ($request->status_id == 2 && $status != 2) {
            $request->operation_id = 1;
        }
        
        if ($status == 3) {
            if (!$request->updated  && $request->store_id != 1002 && $request->store_id != 1001  && $request->store_id != 1003  && $request->store_id != 1004) {
                if ($user == 150) {
                    $requestId = $request->saveLog($user,$request->operator_id, $request->nf_number, $request->access_key, $status, $request->items_quantity, $request->type_id, $request->operation_id,$request->note, $request->id);
                    $return['success'] = false;
                    $return['message'] = "Nota Fiscal não atualizado do Retaguarda!";
                    echo json_encode($return);
                    return;
                } else {
                    $return['success'] = false;
                    $return['message'] = "Nota Fiscal não atualizado do Retaguarda!";
                    echo json_encode($return);
                    return;
                }
            } else {
                $requestId = $request->saveLog($user,$user, $nfNumber, $accessKey, $status, $itemsQuantity, $type, $operationId,$note, $id);
            }
        } else {
            
            $requestId = $request->saveLog($user,$user, $nfNumber, $accessKey, $status, $itemsQuantity, $type, $operationId,$note, $id);

        }

        if ($requestId) {
            
            if ($status == 3) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                if ($request->store()->telegram != '') {
                    $tele_channel = $request->store()->telegram;
                } else {
                    $tele_channel = "@BDTecLancamentos";
                }
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                $logger->pushHandler($tele_handler);
                $logger->info("solicitação de lançamento $id Concluido: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$id");
            } else if ($status == 2) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                if ($request->store()->telegram != '') {
                    $tele_channel = $request->store()->telegram;
                } else {
                    $tele_channel = "@BDTecLancamentos";
                }
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                $logger->pushHandler($tele_handler);
                $logger->info("solicitação de lançamento $id Alterado para Pendente Cliente: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$id");                
            } else if ($status == 1) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                if ($request->store()->telegram != '') {
                    $tele_channel = $request->store()->telegram;
                } else {
                    $tele_channel = "@BDTecLancamentos";
                }
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                $logger->pushHandler($tele_handler);
                $logger->info("solicitação de lançamento $id Alterado para Pendente Empresa: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$id");                
            } else if ($status == 4) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                if ($request->store()->telegram != '') {
                    $tele_channel = $request->store()->telegram;
                } else {
                    $tele_channel = "@BDTecLancamentos";
                }
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                $logger->pushHandler($tele_handler);
                $logger->info("solicitação de lançamento $id Alterado para Em Atendimento: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$id");                
            }
            $return['success'] = true;
            $return['message'] = "Alteração Realizada!";
            echo json_encode($return);
            return;
        } else if ($requestId === false) {
            // echo "<pre>";
            // var_dump($requestId);
            // echo "</pre>";
            // $logger = new Logger("web");
            // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            // $tele_channel = "-1001299426191";
            // // $tele_channel = "@bdteclancamentos";
            // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            // $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
            // $logger->pushHandler($tele_handler);
            // $email = $_COOKIE['login'];
            // $post = implode("','",$_POST);
            // $logger->info("Usúario $email não conseguiu alterar o solicitação de lançamento $id. Dados postados : $post");

            $return['success'] = false;
            $return['message'] = "Alteração não realziada!";
            echo json_encode($return);
            return;
        }
    }

    public function deleteNfReceipt() {

        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $group = $usuario->store_group_id;

        $id = $_POST['id'];
        
        if ($group == 8 || $group == 4){
            $request = (new NfReceipt2())->findById($id);
        } else {
            $request = (new NfReceipt())->findById($id);
        }
        $request->destroy();

        $return['success'] = true;
        $return['message'] = "Lançameto excluido!";
        echo json_encode($return);
        return;
    }

    public function addReport() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $user = $usuario->id;
        $class = $usuario->class;


        $user_id = $usuario->id;
        $id_request = $_POST['idRequest'];
        $nfReceipt = (new NfReceipt())->findById($id_request);

        $note = $_POST['note'];
        $type = $_POST['type'];

        $nfReport = (new NfReport());
        $nfReport->user_id = $user_id;
        $nfReport->id_request = $id_request;
        $nfReport->note = $note;
        $nfReport->type = $type;
        $nfReport->operator_id = $nfReceipt->operator_id;
        $nfReportId = $nfReport->save();

        $interaction = (new Interaction());
        $interaction->id_nf_receipt = $id_request;
        $interaction->id_user = $user_id;
        $interaction->description = "<strong>$note</strong>";
        $interactionId = $interaction->save();

        $request = (new NfReceipt())->findById($id_request);
        $request->status_id = 1;
        if (1 == 1) {
            $request->operator_id = 150;
        }
        if ($class == 'C') {
            $request->operation_id = 11;
        } else {
            $request->operation_id = 12;
        }
        $requestId = $request->save();

        $data['sucesso'] = true;
        $data['msg'] = "NF Reportada!";
        echo json_encode($data);  
        
    }

    public function interactions($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $id = $data['id'];
            $request = (new NfReceipt())->findById($id);
            $interactions = new Interaction();
            $nfeType = (new NfType())->find()->fetch(true);
            $users = (new User())->find("class = 'A'")->fetch(true);
            // $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
            // $paginator = new Paginator(URL_BASE."/request/interacao/{$id}?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
            // $paginator->pager($interactions->find("id_nf_receipt = :rid", "rid=$id")->count(), 5, $page, 2);
            $interactions = $interactions->find("id_nf_receipt = :rid", "rid=$id")->order("created_at DESC")->fetch(true);
            // $paginatorRender = $paginator->render();
            if ($class == 'A') {
                echo $this->view->render("administrator/interactions", ["id" => "Interações | " . SITE, "interactions" => $interactions,
                // "paginator" => $paginator,
                "id" => $id,
                "request" => $request,
                "nfeType" =>$nfeType,
                "users" => $users
                ]);
            } else if ($class == 'O') {
                echo $this->view->render("operator/interactions", ["id" => "Interações | " . SITE, "interactions" => $interactions,
                // "paginator" => $paginator,
                "id" => $id,
                "request" => $request
                ]);
            } else {
                echo $this->view->render("client/interactions", ["id" => "Interações | " . SITE, "interactions" => $interactions,
                // "paginator" => $paginator,
                "id" => $id,
                "request" => $request
                ]);

            }
            
        }
    }

    public function addInteraction() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        $name = $usuario->name;
    
        $files = $_FILES;
        $idRequest = $_POST['idRequest'];
        $idUser = $usuario->id;
        $description = $_POST['descricao'];
        $check = $_POST['check'];
    
        if ($description == '') {
            $data['sucesso'] = false;
            $data['msg'] = "Comentario não informado.";
            echo json_encode($data);            
        } else {
    
            $upload = new Image("storage", "images");
            $fileUpload = new File("storage", "files");
            $interaction = new Interaction();
            $request = (new NfReceipt())->findById($idRequest);
        
            $interaction->id_nf_receipt = $idRequest;
            $interaction->id_user = $idUser;
            $interaction->description = nl2br($description);
            $interactionId = $interaction->save();
    
            if ($interactionId) {
                $id = $interaction->id;
                $attachmentSuccess = true;
                if (!empty($files['files'])) {
                    $documents = $files["files"];
                    for ($i = 0;$i < count($documents['type']);$i++) {
                        foreach (array_keys($documents) as $keys) {
                            $documentFiles[$i][$keys] = $documents[$keys][$i];
                        }
                    }
                    $i = 0;
                    foreach ($documentFiles as $file) {
                        if (empty($file['type'])) {
                            $attachmentSuccess = false;
                            $msg2[$i] = "Selecione uma imagem valida " . $file['name'] . " não encontrado!\n";
                        } elseif (!in_array($file['type'], $fileUpload::isAllowed())) {
                            $attachmentSuccess = false;
                            $msg2[$i] = "O arquivo " . $file['name'] . " tipo " . $file['type'] . " não é válido!\n";
                        } else {
                            $uploaded = $fileUpload->upload($file, pathinfo($file["name"], PATHINFO_FILENAME), 350);
                            $interactionAttachment = new InteractionAttachment();
                            $interactionAttachment->interaction_id = $id;
                            $interactionAttachment->name = $uploaded;
                            if ($file['type'] == "image/jpg" || $file['type'] == "image/jpeg" || $file['type'] == "image/png"|| $file['type'] =="image/gif") {
                                $interactionAttachment->type = 'I';
                            } else if ($file['type'] == 'audio/ogg') {
                                $interactionAttachment->type = 'A';
                            } else {
                                $interactionAttachment->type = 'F';
                            }
                            $interactionAttachmentId = $interactionAttachment->save();
                            if (!$interactionAttachmentId) {
                                $attachmentSuccess = false;
                            }
                        }
                        $i++;
                    }          
                }
                // if(is_array($msg)){
                //     $msg = implode($msg);
                // }
                if(is_array($msg2)){
                    $msg = implode($msg2);
                }

                if (!$attachmentSuccess) {
                    $data['sucesso'] = false;
                    $interaction = (new Interaction())->findById($id);
                    $interaction->destroy();
                    $data['msg'] = "Falha ao anexar arquivo: \n $msg!";
                    echo json_encode($data);
                } else {
                    
                    //concluido
                    if ($check) {
                        $request = (new NfReceipt())->findById($idRequest);
                        $request->operator_id = $idUser;
                        $request->updated_at = date("Y-m-d H:i:s");
                        $request->status_id = 3;
                        $requestId = $request->saveLog($idUser, $idUser, $request->nf_number, $request->access_key, '3', $request->items_quantity, $request->type_id, $request->operation_id,$request->note, $idRequest);
                        $name = $request->userOperator;
                        if ($requestId) { 
                            
                            // $logger = new Logger("web");
                            // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            // if ($request->store()->telegram != '') {
                            //     $tele_channel = $request->store()->telegram;
                            // } else {
                            //     $tele_channel = "@BDTecLancamentos";
                            // }
                            // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            // $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                            // $logger->pushHandler($tele_handler);
                            // $logger->info("solicitação de lançamento $idRequest Concluido: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$idRequest");
                        }
                    //pendente cliente                    
                    } elseif ($request->operator_id == $idUser) {
                        $request = (new NfReceipt())->findById($idRequest);
                        $request->updated_at = date("Y-m-d H:i:s");
                        $request->operator_id = $idUser;
                        
                        $request->status_id = 2;
                        $requestId = $request->saveLog($idUser, $idUser, $request->nf_number, $request->access_key, '2', $request->items_quantity, $request->type_id, $request->operation_id,$request->note, $idRequest);
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        if ($request->store()->telegram != '') {
                            $tele_channel = $request->store()->telegram;
                        } else {
                            $tele_channel = "@BDTecLancamentos";
                        }
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                        $logger->pushHandler($tele_handler);
                        $logger->info("solicitação de lançamento $idRequest Alterado para Pendente Cliente: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$idRequest");                
                        //pendente empresa
                    } elseif ($class == "C") {
                        
                        $request = (new NfReceipt())->findById($idRequest);
                        
                        $request->updated_at = date("Y-m-d H:i:s");
                        if ($request->status_id == 2) {
                            $request->operation_id = 1;
                            $request->operator_id = 147;
                        }
                        $request->status_id = 17;
                        
                        $requestId = $request->saveLog($idUser, $request->operator_id, $request->nf_number, $request->access_key, '1', $request->items_quantity, $request->type_id, $request->operation_id,$request->note, $idRequest);

                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "@BDTecLancamentos";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                        $logger->pushHandler($tele_handler);
                        $logger->info("solicitação de lançamento $idRequest Alterado para Pendente Empresa: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$idRequest");                
                        //pendente cliente
                    } elseif ($idUser != $request->client_id) {
                        $request = (new NfReceipt())->findById($idRequest);
                        $request->updated_at = date("Y-m-d H:i:s");
                        $request->operator_id = $idUser;
                        $request->status_id = 2;
                        $requestId = $request->saveLog($idUser, $idUser, $request->nf_number, $request->access_key, '2', $request->items_quantity, $request->type_id, $request->operation_id,$request->note, $idRequest);
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        if ($request->store()->telegram != '') {
                            $tele_channel = $request->store()->telegram;
                        } else {
                            $tele_channel = "@BDTecLancamentos";
                        }
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                        $logger->pushHandler($tele_handler);
                        $logger->info("solicitação de lançamento $idRequest Alterado para Pendente Cliente: Loja $request->store_id - Operador {$name} - $request->id - ".date("d/m/Y H:i:s")." https://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/$idRequest");                
                    }
                    
                    $data['sucesso'] = true;
                    $data['msg'] = "Interação enviada com sucesso.";
                    echo json_encode($data);
                }
            } else {
                $data['sucesso'] = false;
                $data['msg'] = "Falha ao inserir interação no banco de dados.";
                echo json_encode($data);
    
            }  
    
        }        
    }

    public function slaIndicator($data) {

        $stores = (new Store())->find()->order("code")->fetch(true);
        $storeGroups = (new StoreGroup())->find()->fetch(true);
        if ($data['type']){
            $store = $data['store'];
            $group = $data['group'];
            $dataInicial = $data['dataInicial'];
            $dataFinal = $data['dataFinal'];
            $type = $data['type'];

            if ($store != '') {
                $params['store_id'] = $store;
                $comparation[0] = "store_id = :store_id";

            }    
            if ($group != '') {
                $params['group_id'] = $group;
                $comparation[1] = "group_id = :group_id";
            }
    
            if ($dataInicial != '') {
                $params['dataInicial'] = '1' . substr($dataInicial, 2, 2) . substr($dataInicial, 5, 2) . substr($dataInicial, 8, 2);
                $comparation[4] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) >= :dataInicial";
            }
    
            if ($dataFinal != '') {
                $params['dataFinal'] = '1' . substr($dataFinal, 2, 2) . substr($dataFinal, 5, 2) . substr($dataFinal, 8, 2);
                $comparation[5] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) <= :dataFinal";
            }

            if ($type == "S") {

                $comparation = implode(' and ', $comparation);
                $params = http_build_query($params);
                
                $requests = (new NfReceipt())->find($comparation, $params)->fetch(true);
                $i=0;
                foreach ($requests as $request) {
                                        
                    $horaStore[$request->store_id] = $request->getAverageTime()['horas']*60;
                    
                    $minutoStore[$request->store_id] = $request->getAverageTime()['minutos'];
                    
                    $minutosStores[$request->store_id] += $horaStore[$request->store_id] + $minutoStore[$request->store_id];
                    $i++;
                    $j[$request->store_id] += 1;
                    $stories[$i] = $request->store_id;

                    $hora = $request->getAverageTime()["horas"]*60;
                    $horaOperator[$request->userOperator()->name] = $request->getAverageTime()['horas']*60;
                    
                    $minuto = $request->getAverageTime()['minutos'];
                    $minutoOperator[$request->userOperator()->name] = $request->getAverageTime()['minutos'];
                    
                    $minutos += $hora + $minuto;
                    $minutosOperators[$request->userOperator()->name] = $horaOperator[$request->userOperator()->name] + $minutoOperator[$request->userOperator()->name];
                    
                    
                    $k[$request->userOperator()->name] += 1;
                    $operators[$i] = $request->userOperator()->name;

                    
                    
                    
                    

                }

                $stories = array_unique($stories);
                $operators = array_unique($operators);

                

                foreach ($stories as $store) {

                    $minutosStores[$store] = $minutosStores[$store]/$j[$store];
                    $h = round($minutosStores[$store]/60);
                    $m = $minutosStores[$store]%60;
                    $totalTimeStore[$store] =  str_pad($h,2,'0',STR_PAD_LEFT).":".str_pad($m,2,'0',STR_PAD_LEFT);
                }
                

                foreach ($operators as $operator) {
                    $minutosOperators[$operator] = $minutosOperators[$operator]/$k[$operator];
                    $h = round($minutosOperators[$operator]/60);
                    $m = $minutosOperators[$operator]%60;
                    $totalTimeOperators[$operator] =  str_pad($h,2,'0',STR_PAD_LEFT).":".str_pad($m,2,'0',STR_PAD_LEFT);
                }


                $minutos = $minutos/$i;
                $h = round($minutos/60);
                $m = $minutos%60;
                $totalTime =  str_pad($h,2,'0',STR_PAD_LEFT).":".str_pad($m,2,'0',STR_PAD_LEFT);
                
                

                echo $this->view->render("administrator/slaReport",
                ["id" => "Relatorio SLA | " . SITE,
                "stores" => $stores,
                "storeGroups" => $storeGroups,
                "totalTime" => $totalTime,
                "totalTimeStore" =>$totalTimeStore,
                "totalTimeOperators" => $totalTimeOperators,
                "stories" => $stories,
                "operators" => $operators,
                ]);
    
            } else if ($type == "A") {
                $requests = (new NfReceipt())->find($comparation, $params)->fetch(true);
            }            
        } else {
            echo $this->view->render("administrator/slaIndicators",
            ["id" => "Relatorio SLA | " . SITE,
            "stores" => $stores,
            "storeGroups" => $storeGroups
            ]);
        }        
    }

    public function slaSearch(){

        if ($_POST['store']) {
            $store = $_POST['store'];
        } else {
            $store = 'f';
        }

        if ($_POST['group']) {
            $group = $_POST['group'];
        } else {
            $group = 'f';
        }

        if ($_POST['dataInicial']) {
            $dataInicial = $_POST['dataInicial'];
        } else {
            $dataInicial = 'f';
        }

        if ($_POST['dataFinal']) {
            $dataFinal = $_POST['dataFinal'];
        } else {
            $dataFinal = 'f';
        }

        if ($_POST['type']) {
            $type = $_POST['type'];
        } else {
            $type = 'f';
        }

        $this->router->redirect("/SLA/{$store}/{$group}/{$dataInicial}/{$dataFinal}/{$type}");
    }

    public function NfReceiptRel() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $storeId = $usuario->store_code;

        $criacaoInicial = $_POST['criacaoInicial'];
        $criacaoFinal = $_POST['criacaoFinal'];
        $alteracaoInicial = $_POST['alteracaoInicial'];
        $alteracaoFinal = $_POST['alteracaoFinal'];
        $idOperator = $_POST['idOperator'];
        $storeGroup = $_POST['storeGroup'];
        $store = $_POST['store'];
        $operation = $_POST['operation'];
        $type = $_POST['type'];
        $status = $_POST['status'];
        $params = array();
        $comparation = array();

        if ($type != '') {
            $params['type_id'] = $type;
            $comparation[77] = "type_id = :type_id";
        }

        if ($storeGroup != '') {
            $params['group_id'] = $storeGroup;
            $comparation[999] = "group_id = :group_id";
        }

        if ($operation != '') {
            $params['operation_id'] = $operation;
            $comparation[88] = "operation_id = :operation_id";
        }
        
        if ($idOperator != '') {
            $params['operator_id'] = $idOperator;
            $comparation[2] = "operator_id = :operator_id";
        }
        if ($criacaoInicial != '') {
            $params['criacaoInicial'] = '1' . substr($criacaoInicial, 2, 2) . substr($criacaoInicial, 5, 2) . substr($criacaoInicial, 8, 2);
            $comparation[4] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) >= :criacaoInicial";
        }
        if ($criacaoFinal != '') {
            $params['criacaoFinal'] = '1' . substr($criacaoFinal, 2, 2) . substr($criacaoFinal, 5, 2) . substr($criacaoFinal, 8, 2);
            $comparation[5] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) <= :criacaoFinal";
        }
        if ($alteracaoInicial != '') {
            $params['alteracaoInicial'] = '1' . substr($alteracaoInicial, 2, 2) . substr($alteracaoInicial, 5, 2) . substr($alteracaoInicial, 8, 2);
            $comparation[6] = "concat('1',substr(`updated_at`,3,2),substr(`updated_at`,6,2),substr(`updated_at`,9,2)) >= :alteracaoInicial";
        }
        if ($alteracaoFinal != '') {
            $params['alteracaoFinal'] = '1' . substr($alteracaoFinal, 2, 2) . substr($alteracaoFinal, 5, 2) . substr($alteracaoFinal, 8, 2);
            $comparation[7] = "concat('1',substr(`updated_at`,3,2),substr(`updated_at`,6,2),substr(`updated_at`,9,2)) <= :alteracaoFinal";
        }
        if ($status != '') {
            $params['status_id'] = $status;
            $comparation[8] = "status_id = :status_id";
        }
        if ($store != '') {
            $params['store_id'] = $store;
            $comparation[9] = "store_id = :store_id";
        }

        if ($storeId != 2000){
            $params['store_id'] = $storeId;
            $comparation[9] = "store_id = :store_id";
        }

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);

        if(!isset($comparation)) {
            $comparation = "store_id = :store_id";
        }
        if($params == '') {
            $params = "store_id=$store";
        }
      
       
        
        $requests = (new NfReceipt())->find($comparation,$params)->fetch(true);


        echo $this->view->render("administrator/nfList", ["id" => "Relatorio de Notas | " . SITE,
                "requests" => $requests
        ]);
    }

    public function NfReceiptRelReporte() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $storeId = $usuario->store_code;

        $criacaoInicial = $_POST['criacaoInicial'];
        $criacaoFinal = $_POST['criacaoFinal'];
        $alteracaoInicial = $_POST['alteracaoInicial'];
        $alteracaoFinal = $_POST['alteracaoFinal'];
        $idOperator = $_POST['idOperator'];
        $storeGroup = $_POST['storeGroup'];
        $store = $_POST['store'];
        $operation = $_POST['operation'];
        $type = $_POST['type'];
        $status = $_POST['status'];
        $params = array();
        $comparation = array();

        if ($type != '') {
            $params['type_id'] = $type;
            $comparation[77] = "type_id = :type_id";
        }

        if ($storeGroup != '') {
            $params['group_id'] = $storeGroup;
            $comparation[999] = "group_id = :group_id";
        }

        if ($operation != '') {
            $params['operation_id'] = $operation;
            $comparation[88] = "operation_id = :operation_id";
        }
        
        if ($idOperator != '') {
            $culpado = $idOperator;
        }
        if ($criacaoInicial != '') {
            $params['criacaoInicial'] = '1' . substr($criacaoInicial, 2, 2) . substr($criacaoInicial, 5, 2) . substr($criacaoInicial, 8, 2);
            $comparation[4] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) >= :criacaoInicial";
        }
        if ($criacaoFinal != '') {
            $params['criacaoFinal'] = '1' . substr($criacaoFinal, 2, 2) . substr($criacaoFinal, 5, 2) . substr($criacaoFinal, 8, 2);
            $comparation[5] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) <= :criacaoFinal";
        }
        if ($alteracaoInicial != '') {
            $params['alteracaoInicial'] = '1' . substr($alteracaoInicial, 2, 2) . substr($alteracaoInicial, 5, 2) . substr($alteracaoInicial, 8, 2);
            $comparation[6] = "concat('1',substr(`updated_at`,3,2),substr(`updated_at`,6,2),substr(`updated_at`,9,2)) >= :alteracaoInicial";
        }
        if ($alteracaoFinal != '') {
            $params['alteracaoFinal'] = '1' . substr($alteracaoFinal, 2, 2) . substr($alteracaoFinal, 5, 2) . substr($alteracaoFinal, 8, 2);
            $comparation[7] = "concat('1',substr(`updated_at`,3,2),substr(`updated_at`,6,2),substr(`updated_at`,9,2)) <= :alteracaoFinal";
        }
        if ($status != '') {
            $params['status_id'] = $status;
            $comparation[8] = "status_id = :status_id";
        }
        if ($store != '') {
            $params['store_id'] = $store;
            $comparation[9] = "store_id = :store_id";
        }

        if ($storeId != 2000){
            $params['store_id'] = $storeId;
            $comparation[9] = "store_id = :store_id";
        }

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);

        if(!isset($comparation)) {
            $comparation = "store_id = :store_id";
        }
        if($params == '') {
            $params = "store_id=$store";
        }
      
       
        
        $requests = (new NfReceipt())->find($comparation,$params)->fetch(true);

        echo $this->view->render("administrator/nfReportList", ["id" => "Relatorio de Notas | " . SITE,
                "requests" => $requests,
                "store" => $storeId,
                "culpado" => $culpado,
                "class" => $usuario->class
        ]);
    }

    public function nfVerification() {

        if ($_POST['verification']) {
            $requestCode = $_POST['id'];        
            $criacaoInicial = $_POST['criacaoInicial'];
            $criacaoFinal = $_POST['criacaoFinal'];
            $store = $_POST['store'];
    
            $accessKey = $_POST['accessKey'];
    
            $nfNumber = $_POST['nfNumber'];
    
            $verification = $_POST['verification'];
    
            if ($requestCode != '') {
                $params['id'] = $requestCode;
                $comparation[10] = "id = :id";
            }

            if ($store != '') {
                $params['store_id'] = $store;
                $comparation[10] = "store_id = :store_id";
            }
    
            if ($accessKey != '') {
                $params['access_key'] = $accessKey;
                $comparation[99] = "access_key = :access_key";
            }
    
            if ($nfNumber != '') {
                $params['nf_number'] = $nfNumber;
                $comparation[10] = "nf_number = :nf_number";
            }

            if ($verification == 1) {
                $type = 1;
                $request = new NfReceipt();
                if ($criacaoInicial != '') {
                    $params['criacaoInicial'] = '1' . substr($criacaoInicial, 2, 2) . substr($criacaoInicial, 5, 2) . substr($criacaoInicial, 8, 2);
                    $comparation[4] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) >= :criacaoInicial";
                }
                if ($criacaoFinal != '') {
                    $params['criacaoFinal'] = '1' . substr($criacaoFinal, 2, 2) . substr($criacaoFinal, 5, 2) . substr($criacaoFinal, 8, 2);
                    $comparation[5] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) <= :criacaoFinal";
                }
                $params['status_id'] = '3';
                $comparation[7897] = "status_id = :status_id";
            } else {
                $request = new NfRms();
                if ($criacaoInicial != '') {
                    $params['criacaoInicial'] = '1' . substr($criacaoInicial, 2, 2) . substr($criacaoInicial, 5, 2) . substr($criacaoInicial, 8, 2);
                    $comparation[4] = "date >= :criacaoInicial";
                }
                if ($criacaoFinal != '') {
                    $params['criacaoFinal'] = '1' . substr($criacaoFinal, 2, 2) . substr($criacaoFinal, 5, 2) . substr($criacaoFinal, 8, 2);
                    $comparation[5] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) <= :criacaoFinal";
                }
            }
    
            if ($store != '') {
                $params['store_id'] = $store;
                $comparation[9] = "store_id = :store_id";
            }
            
            $comparation = implode(' and ', $comparation);
            $params = http_build_query($params);
    
            if(!isset($comparation)) {
                $comparation = "store_id = :store_id";
            }
            if($params == '') {
                $params = "store_id=$store";
            }
            
            $nf = $request->find($comparation, $params)->fetch(true);

            echo $this->view->render("administrator/nfVerification", ["id" => "Relatorio de Notas | " . SITE,
            "request" => $nf,
            "type" => $type
            ]);
        } else {
            echo $this->view->render("administrator/nfVerification", ["id" => "Relatorio de Notas | " . SITE,
            ]);
        }
        
    }
    
    public function nfVerificationImport() {
        $upload = new File("storage", "files");
        $files = $_FILES;

        if(!empty($files['csv'])) {
            $file = $files['csv'];
            if(empty($file['type']) || !in_array($file['type'],$upload::isAllowed())){
                $data['sucesso'] = false;
                $data['msg'] = "Falha ao incluir o anexo!";
                echo json_encode($data);
            } else {
                $uploaded = $upload->upload($file,pathinfo($file["name"],PATHINFO_FILENAME));
                $stream = fopen("/bdtecsolucoes.com.br:8090/$uploaded","r");
                $csv = Reader::createFromStream($stream);

                $csv->setDelimiter(",");
                $csv->setHeaderOffset(0);

                $stmt = (new Statement());
                $nfs = $stmt->process($csv);

                foreach ($nfs as $nf) {
                    $nf = (object)$nf;
                    $count = (new NfRms())->find("nf_number = :nf and access_key = :ak and store_id = :si","nf=$nf->NUMERO_NFE&ak=$nf->CHAVE_ACESSO_NFE&si=$nf->LOJA_INCLUSAO")->count();

                    if ($count == 0){
                        $nfRms = new NfRms();
                        $nfRms->nf_number = $nf->NUMERO_NFE;
                        $nfRms->access_key = $nf->CHAVE_ACESSO_NFE;
                        $nfRms->store_id = $nf->LOJA_INCLUSAO;
                        $nfRms->date = $nf->DATA;
                        $nfRmsId = $nfRms->save();
                    }                    
                }
                $data['sucesso'] = true;
                $data['msg'] = "Registros processados com sucesso.";
                echo json_encode($data);

            }
        }
    }
    
    public function updateRegisterAmount() {
        $amount = $_GET['amount'];
        $id = $_GET['id'];
        $rai = new RegisterAmountItems();
        $rai->nf_receipt_id = $id;
        $rai->amount = $amount;
        $raiId = $rai->save();
        $data['sucesso'] = true;
        $data['msg'] = "Quantidade incluida!";
        echo json_encode($data);
    }

    public function desfazReport() {
        $report = (new NfReport())->findById($_POST['id']);
        $report->user_id = 0;
        $reportId = $report->save();
        $data['success'] = true;
        $data['message'] = "Reporte desfeito.";
        echo json_encode($data);
    }

    public function desbloqueio() {
        $id = $_POST['id'];
        $observation = $_POST['observation'];

        $nfReceipt = (new NfReceipt())->findById($id);
        $nfReceipt->error_cost = null;
        $nfReceiptId = $nfReceipt->save();
        $desbloqueio = (new Desbloqueio());
        $desbloqueio->note = $observation;
        $desbloqueioId = $desbloqueio->save();
        $return['sucesso'] = true;
        $return['msg'] = "Nota Desbloqueada!";
        echo json_encode($return);
        return;
    }

    public function registarCadastro() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $operator_id = $usuario->id;
        $referencia = $_POST['referencia'];
        $ean = $_POST['ean'];
        $descricao = $_POST['descricao'];
        $id = $_POST['id'];

        $r = new RegistroCadastro();
        $r->operator_id = $operator_id;
        $r->referencia = $referencia;
        $r->ean = $ean;
        $r->descricao = $descricao;
        $r->nf_receipt_id = $id;
        $rId = $r->save();

    }

    public function registerReport() {
        $date1 = $_POST['date1'];
        $date2 = $_POST['date2'];
        if ($_POST['operator'] != '') {
            $operator = "and operator_id = ".$_POST['operator'];
        } else {
            $operator = '';
        }
        $registers = (new RegistroCadastro())->find("created_at between '$date1' and '$date2' $operator")->fetch(true);
        $i = 1;
        foreach ($registers as $register) {
            $line[$i]["nfId"] = $register->nf_receipt_id;
            $line[$i]["reference"] = $register->referencia;
            $line[$i]["description"] = $register->descricao;
            $line[$i]["ean"] = $register->ean;
            $line[$i]["operator"] = $register->operator()->name;
            $i++;
        }
        echo $this->view->render("administrator/registerReport", ["id" => "Relatorio de cadastros | " . SITE,
                "line" => $line,
                "i" => $i
        ]);
    }
    /**
     * Inserir quantidades.
     */
    public function alteraEstoque() {
        $id = $_GET['id'];
        $nfReceipt = (new NfReceipt())->findById($id);

        if ($nfReceipt) {
            if (strlen($nfReceipt->access_key) == 44) {
                $ag1iensa = $nfReceipt->getAG1IENSA;
                if (!empty($ag1iensa)) {
                    foreach($ag1iensa as $iensa) {
                        $aa2cestq = $iensa->getAA2CESTQ;
                        if ($aa2cestq) {
                            if ($aa2cestq->GET_ESTOQUE < $iensa->ENTSAIC_QUANTI_UN) {
                                $aC = new AuxiliarCancelamento();
                                $aC->store_id = $aa2cestq->GET_COD_LOCAL;
                                $aC->code = $aa2cestq->GET_COD_PRODUTO;
                                $aC->amount = $aa2cestq->GET_ESTOQUE;
                                $aC->amount_nf = $iensa->ENTSAIC_QUANTI_UN;
                                $aC->negative = $aa2cestq->GET_QTD_PEND_VDA;
                                $aC->id_nf_receipt = $id;
                                $aCId = $aC->save();
                                if ($aCId) {
                                    if ($nfReceipt->group_id == 1 or $nfReceipt->group_id == 10) {
                                        $update = new AA2CESTQ();
                                        $update->aa2cestq($aa2cestq->GET_COD_LOCAL,$aa2cestq->GET_COD_PRODUTO,($iensa->ENTSAIC_QUANTI_UN-$aa2cestq->GET_ESTOQUE)+$aa2cestq->GET_ESTOQUE,0,false);    
                                    } else if ($nfReceipt->group_id == 2 or $nfReceipt->group_id == 9) {
                                        $update = new AA2CESTQ2();
                                        $update->aa2cestq($aa2cestq->GET_COD_LOCAL,$aa2cestq->GET_COD_PRODUTO,($iensa->ENTSAIC_QUANTI_UN-$aa2cestq->GET_ESTOQUE)+$aa2cestq->GET_ESTOQUE,0,false);    
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $data['succes'] = false;
            $data['message'] = "Nota não localizada";
            echo json_encode($data);
        }

    }

    /**
     * Segunda parte quando não vai mais lançar o item (item incorreto).
     */
    public function alteraEstoque2() {
        $id = $_GET['id'];
        $nfReceipt = (new NfReceipt())->findById($id);
        $aC = (new AuxiliarCancelamento())->find("id_nf_receipt = $id")->fetch(true);
        if (!empty($aC)) {
            foreach($aC as $a) {
                if ($nfReceipt->group_id == 1 or $nfReceipt->group_id == 10) {
                    $update = new AA2CESTQ();
                    $update->aa2cestq($a->store_id,$a->code,$a->amount_nf-$a->amount,$a->negative+$a->amount_nf,true);
                } else if ($nfReceipt->group_id == 2 or $nfReceipt->group_id == 9) {
                    $update = new AA2CESTQ2();
                    $update->aa2cestq($a->store_id,$a->code,$a->amount_nf-$a->amount,$a->negative+$a->amount_nf,true);
                }

                
            }  
        } else {
            $data['succes'] = false;
            $data['message'] = "Nota não localizada";
            echo json_encode($data);
        }

    }

    /**
     * Segunda parte quando lançou o item.
     */
    public function alteraEstoque3() {
        $id = $_GET['id'];
        $nfReceipt = (new NfReceipt())->findById($id);
        $aC = (new AuxiliarCancelamento())->find("id_nf_receipt = $id")->fetch(true);
        if (!empty($aC)) {
            foreach($aC as $a) {
                if ($nfReceipt->group_id == 1 or $nfReceipt->group_id == 10) {
                    $update = new AA2CESTQ();
                    $update->aa2cestq($a->store_id,$a->code,$a->amount_nf-$a->amount,$a->negative,true);
                } else if ($nfReceipt->group_id == 2 or $nfReceipt->group_id == 9) {
                    $update = new AA2CESTQ2();
                    $update->aa2cestq($a->store_id,$a->code,$a->amount_nf-$a->amount,$a->negative,true);
                    //12-10 = 2
                }

                
            }

        } else {
            $data['succes'] = false;
            $data['message'] = "Nota não localizada";
            echo json_encode($data);
        }

    }

    public function check() {
        $nfReceipt = (new NfReceipt())->findById($_GET['id']);
 
        
        echo '
        <style>
        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;

        }

        table {
            text-align: center;
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }

        td {
            border: 1px solid #555555;
            text-transform: uppercase;
        }

        th {
            border: 1px solid #555555;
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }
        </style>
        <table class="table">
        <thead>
          <tr>
            <th scope="col">Codigo</th>
            <th scope="col">Descrição</th>
            <th scope="col">Custo</th>
            <th scope="col">Preço</th>
            <th scope="col">Data do Preço</th>
            <th scope="col">Margem</th>
            <th scope="col">Quantidade embalagem XML</th>
            <th scope="col">Quantidade embalagem Retaguarda</th>
            <th scope="col">Quantidade*Embalagem Retaguarda</th>
          </tr>
        </thead>
        <tbody>';
        $i = 0;
        foreach($nfReceipt->margemRecebimento() as $registros) {
            $codigoInterno = (new CodigoInterno())->find("store_id = $nfReceipt->store_id and code = ".$registros['codigo'])->count();
            if($nfReceipt->getItemsRms($registros['codigo'])['nota']->prod->qTrib) {
                if (($registros['GET_DT_ULT_FAT'] == 0 && $registros['GIT_TIPO_ETQ'] == 4) or 
                ($nfReceipt->getItemsRms($registros['codigo'])['nota']->prod->qTrib/$nfReceipt->getItemsRms($registros['codigo'])['nota']->prod->qCom != $registros['embalagem'])){
                    echo "<tr style='color:red'>";
                } else if ($registros['GIT_TIPO_ETQ'] == 4 && $codigoInterno == 0) {
                    echo "<tr style='color:purple'>";
                } else {
                    echo "<tr>";
                } 
                echo "<th scope='col'>".$registros['codigo']."</th>
                <th scope='col'>".$registros['descricao']."</th>
                <th scope='col'>".$registros['custo']."</th>
                <th scope='col'>".$registros['preco']."</th>
                <th scope='col'>".$registros['dataPreco']."</th>
                <th scope='col'>".$registros['margem']."</th>

                <th scope='col'>".$nfReceipt->getItemsRms($registros['codigo'])['nota']->prod->qTrib/$nfReceipt->getItemsRms($registros['codigo'])['nota']->prod->qCom."</th>
                <th scope='col'>".$registros['embalagem']."</th>
                <th scope='col'>".$registros['quantidade']*$registros['embalagem']."</th>

                </tr>";
            } else {
                echo "<tr style='color:red'>";
                echo "<th scope='col'>".$registros['codigo']."</th>
                <th scope='col'>".$registros['descricao']."</th>
                <th scope='col'>".$registros['custo']."</th>
                <th scope='col'>".$registros['preco']."</th>
                <th scope='col'>".$registros['dataPreco']."</th>
                <th scope='col'>".$registros['margem']."</th>

                <th scope='col'>-</th>
                <th scope='col'>".$registros['embalagem']."</th>
                <th scope='col'>".$registros['quantidade']*$registros['embalagem']."</th>

                </tr>";
            }
            
            $i++;
        }
        echo '
        </tbody>
        </table>';
    }

    public function codigoInterno() {
        $email = $_COOKIE['login'];
        $user = (new User())->find("email = :email","email=$email")->fetch();
        $codigoInternoConsinco = (new CodigoInternoConsinco())->find()->fetch();
        $codigoInterno = (new CodigoInterno())->find("store_id = $user->store_code")->fetch(true);

        foreach($codigoInterno as $code) {
            $codigo = (new CodigoInterno())->findById($code->id);
            if ($code->description == '') {
                $codigo->description = $code->codigoInternoConsinco()->DESCCOMPLETA;
                $codigoId = $codigo->save();
            }
        }
        

        echo $this->view->render("client/codigoInterno", ["id" => "Codigos Internos Loja - $user->store_code | " . SITE,
        "codigoInterno" => $codigoInterno,
        "codigoInternoConsinco" => $codigoInternoConsinco,
        "storeId" => $user->store_code,
        "read" => true

        ]);
    }

    public function updateCode()
    {
        $id = $_POST['id'];
        $code = $_POST['code'];
        if ($code == '') {
            $nfReceiptCode = (new NfReceiptCodes())->findById($id);
            $destroy = $nfReceiptCode->destroy();
            $data['success'] = true;
            $data['message'] = "Excluido!";
            echo json_encode($data);
        } else {
            $nfReceiptCode = (new NfReceiptCodes())->findById($id);
            $nfReceiptCode->code = $code;
            $nfReceiptCodeId = $nfReceiptCode->save();
            $data['success'] = true;
            $data['message'] = "Alterado!";
            echo json_encode($data);
        }
        
    }
}