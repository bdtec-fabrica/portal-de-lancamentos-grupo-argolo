<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\Call;
use Source\Models\CallAttachment;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\NfRms;
use Source\Models\AA3CNVCC;
use Source\Models\Inventories;
use Source\Models\AA3CNVCC2;
use Source\Models\AA2CTIPO;
use Source\Models\AA2CTIPO3;
use Source\Models\AA2CTIPO2;
use Source\Models\Status;
use Source\Models\Counting;
use Source\Models\StockMovement;
use Source\Models\Session;
use Source\Models\UserClass;
use Source\Models\Inventory;
use Source\Models\Marketing;
use Source\Models\NfType;
use Source\Models\Audit;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SendGrid\Mail\Mail;
use SendGrid;
use Symfony\Component\DomCrawler\Crawler;
use Source\Models\NfReceipt;
use Source\Models\NfReceipt2;
use Source\Models\NfStatus;
use Source\Models\NfAttachment;
use Source\Models\NfDueDate;
use Source\Models\NfOperation;
use Source\Models\NfReceiptLog;
use Source\Models\NfReport;
use Source\Models\NfSefaz;
use Source\Models\ExchangeCode;
use Source\Models\NfEvent;
use Dompdf\Dompdf;
use Source\Models\NfStarted;
use Source\Models\RegisterAmountItems;
use Source\Models\ReasonPendingClient;
use Source\Models\AA1CFISC;
use Source\Models\AA1CFISC2;
use Source\Models\AA1LINHP;
use Source\Models\AA1LINHP2;
use Source\Models\AA3CITEM;
use Source\Models\AA3CITEM2;
use Source\Models\AA3CITEM3;
use Source\Models\AG1IENSA;
use Source\Models\Aa3citemnm;
use Source\Models\AA1AGEDT;
use Source\Models\AG1IENSA2;
use Source\Models\AG1IENSA3;
use Source\Models\AA1CTCON2;
use Source\Models\AA2CESTQ;
use Source\Models\AA2CESTQ2;
use Source\Models\AA2CESTQ3;
use Source\Models\AA3CNVCC3;
use Source\Models\ButcheryRegistration;
use Source\Models\ExchangePackaging;
use Source\Models\RequestScore;
use Source\Models\InventoryItems;
use Source\Models\Itens;
use Source\Models\MarginVerification;
use Source\Models\PostedVerification;
use Source\Models\Itens51;
use Source\Models\DIMPER;
use Source\Models\DIMPER2;
use Source\Models\DIMPER3;
use Source\Models\AGGFLSPROD;
use Source\Models\AGGFLSPROD2;
use Source\Models\AGGFLSPROD3;
use Source\Models\CAPCUPOM;
use Source\Models\CAPCUPOM2;
use Source\Models\AG1PDVPD2;
use Source\Models\AG1PDVPD;
use Source\Models\AA2CPARA;
use Source\Models\AA2CPARA2;
use Source\Models\NotificationsAudit;
use Source\Models\DEVCLICP;
use Source\Models\NFE_CONTROLE;
use Source\Models\NFE_CONTROLE2;
use DateTime;
use Source\Models\AA1CTCON;
use Source\Models\AA1CTCON3;
use Source\Models\AA3CITEM4;
use Source\Models\AA1CTCON4;
use Source\Models\AG1IENSA4;
use Source\Models\MapFamilia;
use Source\Models\AGGFLSPROD4;
use Source\Models\AA1DITEM;
use Source\Models\AA1DITEM2;
use Source\Models\AA1DITEM3;
use Source\Models\AA1DITEM4;
use Source\Models\NCM;
use Source\Models\AA1CFISC3;
use Source\Models\AA1CFISC4;
use Source\Models\AA0LOGIT;
use Source\Models\AA0LOGIT2;
use Source\Models\AA0LOGIT3;
use Source\Models\AA0LOGIT4;
use Source\Models\AA0LOGIT5;
use Source\Models\CadastroExterno;
use Source\Models\NCMALIQUOTA;
use Source\Models\NCMPIS;
use Source\Models\SupplierRegistration;
use Source\Models\AA1FORIT;
use Source\Models\AA1FORIT2;
use Source\Models\AA1FORIT3;
use Source\Models\AA1FORIT4;
use Source\Models\AA3CCEAN;
use Source\Models\AA3CCEAN2;
use Source\Models\AuditoriaIcmsEan;
use Source\Models\AA3CCEAN3;
use Source\Models\AA3CCEAN4;
use Source\Models\AA2CTIPO4;
use League\Csv\Writer;
use PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart;
use Source\Models\InventarioContabil;
use Source\Models\InventarioContabilItens;

class FiscalStock 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }
    /**
     * demonstrar a tela principal de importação
     */
    public function formImportInvContabil() {
        $ic = (new InventarioContabil())->find()->fetch(true);
        echo $this->view->render("fiscalStock/uploadFiscalStock", ["title" => "Importação Inventario Contabil | " . SITE, "ic" => $ic]);
    }
    /**
     * realizar a importação
     */
    public function importInvContabil() {
 
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $store = $_POST['loja'];
        $date = $_POST['data'];
        $ic = new InventarioContabil();
        $ic->user_id = $user->id;
        $ic->store_code = $store;
        $ic->date = $date;
        $icID = $ic->save();


        $upload = new File("storage","csv");
        $files = $_FILES;
        if (!empty($files['file'])) {
            $file = $files['file'];
            $uploaded = $upload->upload($file,pathinfo($file["name"],PATHINFO_FILENAME));
            $stream = fopen($uploaded,"r");
            $csv = Reader::createFromStream($stream);
            $csv->setDelimiter(";");
            $csv->setHeaderOffset(0);

            $stmt = (new Statement());
            $invContabil = $stmt->process($csv);
            foreach ($invContabil as $r) {

                $ici = new InventarioContabilItens();
                $ici->inventario_contabil_id = $ic->id;
                $ici->class_fiscal = str_pad($r['Class. Fiscal'],8,'0',STR_PAD_LEFT);
                $ici->codigo = str_replace('-','',$r['Codigo']);
                $ici->discriminacao = $r['Discriminacao'];
                $ici->quantidade = str_replace(',','.',$r['Quantidade']);
                $ici->un = $r['Und'];
                $ici->unitario = str_replace(',','.',$r['Unitario']);
                $ici->parcial = str_replace(',','.',$r['Parcial']);
                $ici->total = str_replace(',','.',$r['Total']);
                $iciID = $ici->save();
                
            }
            $data['sucesso'] = true;
            $data['msg'] = "Invantario contabil importado! ID #$ic->id";
            echo json_encode($data);
        }
    }
    /**
     * demonstrar  o que foi importado
     */
    public function viewInvContabil($data) {
        $id = $data['id'];
        $invContabil = (new InventarioContabil())->findById($id);
        
        
        echo $this->view->render("fiscalStock/ViewFiscalStockRMS", ["title" => "Relatorio do Inventario Contabil | " . SITE, "ic" => $invContabil]);
    } 
    /**
    * demonstrar o movimento não fiscal
    */
    public function viewMovmentNaoFiscal() {
        $id = $_POST['id'];
        $invContabil = (new InventarioContabil())->findById($id);
        $periodo = " ".inputDateToRMSDate($_POST['date1'])." and ".inputDateToRMSDate($_POST['date2'])." ";
        $base = $_POST['base'];
        if ($base == 1) {
            $ctcon = (new AA1CTCON())->getComprasNaoFiscal();
            $ctcon2 = (new AA1CTCON())->getSaidasNaoFiscal();
            $ag1iensa = (new AG1IENSA())->find("ENTSAIC_SITUACAO != '9' and ESCHC_DATA3 between $periodo and ESCHC_AGENDA3 in ($ctcon) and ESCLC_CODIGO = $invContabil->store_code","","ESITC_CODIGO||ESITC_DIGITO codigo, sum(ENTSAIC_QUANTI_UN) quantidade")->group("ESITC_CODIGO||ESITC_DIGITO")->fetch(true);
            foreach($ag1iensa as $iensa) {
                $quantidadeEntrada[$iensa->CODIGO] = $iensa->QUANTIDADE;
            }
    
            $ag1iensa1 = (new AG1IENSA())->find("ENTSAIC_SITUACAO != '9' and ESCHC_DATA3 between $periodo and ESCHC_AGENDA3 in ($ctcon2) and ESCHLJC_CODIGO3 = $invContabil->store_code","","ESITC_CODIGO||ESITC_DIGITO codigo, sum(ENTSAIC_QUANTI_UN) quantidade")->group("ESITC_CODIGO||ESITC_DIGITO")->fetch(true);
    
            foreach($ag1iensa1 as $iensa) {
                $quantidadeSaida[$iensa->CODIGO] = $iensa->QUANTIDADE;
            }
            $ag1iensa = new AG1IENSA();
        } else if ($base == 3) {
            $ctcon = (new AA1CTCON3())->getComprasNaoFiscal();
            $ctcon2 = (new AA1CTCON3())->getSaidasNaoFiscal();
            $ag1iensa = (new AG1IENSA3())->find("ENTSAIC_SITUACAO != '9' and ESCHC_DATA3 between $periodo and ESCHC_AGENDA3 in ($ctcon) and ESCLC_CODIGO = $invContabil->store_code","","ESITC_CODIGO||ESITC_DIGITO codigo, sum(ENTSAIC_QUANTI_UN) quantidade")->group("ESITC_CODIGO||ESITC_DIGITO")->fetch(true);
            foreach($ag1iensa as $iensa) {
                $quantidadeEntrada[$iensa->CODIGO] = $iensa->QUANTIDADE;
            }
    
            $ag1iensa1 = (new AG1IENSA3())->find("ENTSAIC_SITUACAO != '9' and ESCHC_DATA3 between $periodo and ESCHC_AGENDA3 in ($ctcon2) and ESCHLJC_CODIGO3 = $invContabil->store_code","","ESITC_CODIGO||ESITC_DIGITO codigo, sum(ENTSAIC_QUANTI_UN) quantidade")->group("ESITC_CODIGO||ESITC_DIGITO")->fetch(true);
    
            foreach($ag1iensa1 as $iensa) {
                $quantidadeSaida[$iensa->CODIGO] = $iensa->QUANTIDADE;
            }
            $aa3citem = new Aa3citemnm();
            $ag1iensa = new AG1IENSA3();
        }
        
        
        
        echo $this->view->render("fiscalStock/ViewFiscalStockRMS", ["title" => "Relatorio do Inventario Contabil | " . SITE,"store" => $invContabil->store_code, "periodo" => $periodo, "ag1iensa" => $ag1iensa, "aa3citem" => $aa3citem, "ic" => $invContabil, "entrada" => $quantidadeEntrada, "saida" => $quantidadeSaida]);
    }
    /**
    * realizar o calculo atraves da movimentação e gerar o novo arquivo
    */
    public function processInvContabil() {

    }
    
}