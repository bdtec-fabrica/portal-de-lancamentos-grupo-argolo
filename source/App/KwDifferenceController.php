<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\ExchangeCode;
use Source\Models\ExchangePackaging;
use Source\Models\UserClass;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use SendGrid\Mail\Mail;
use SendGrid;
use Source\Models\KwDifference;

class KwDifferenceController 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function kwDifference() {
        $users = (new User())->find("class = 'A'")->order('id')->fetch(true);
        $stores = (new Store())->find()->fetch(true);
        echo $this->view->render("kwDifference/kwDifference", ["title" => "Acompanhamento resolução diferenças KW | " . SITE, "users" => $users, "stores" => $stores]);  
    }

    public function createKwDifference() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $id = $_COOKIE['id'];
        $operator = $id;
        $store = $_POST['store'];
        $date = $_POST['date'];
        $kwDifference = new KwDifference();
        $kwDifference->client_id = $operator;
        $kwDifference->store_id  = $store;
        $kwDifference->difference_date = $date;
        $kwDifferenceId = $kwDifference->save();

        if (!$kwDifferenceId) {
            $data['sucesso'] = false;
            $data['msg'] = "Falha ao gravar nova Atividade.";
            echo json_encode($data);
        } else {
            $data['sucesso'] = true;
            $data['msg'] = "Atividade salva com sucesso.";
            echo json_encode($data);
        }
    }

    public function readKwDifference() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $id = $_COOKIE['id'];
        if (is_numeric($_POST['operator'])) {
            $operator = $_POST['operator'];
            $array['operator'] = " client_id = $operator ";
        }

        if (is_numeric($_POST['store'])) {
            $store = $_POST['store'];
            $array['operator'] = " store_id = $store ";
        }

        if (is_numeric($_POST['date'])) {
            $date = $_POST['date'];
            $array['date'] = " substr(created_at,1,10) = $date ";
        }
        
        if (isset($array)) {
            $array = implode(' and ', $array); 
        } else {
            $array = '1 < 0';
        }
        
        $kwDifference = (new KwDifference())->find($array)->fetch(true);
        $users = (new User())->find("class = 'A'")->order('id')->fetch(true);
        $stores = (new Store())->find()->fetch(true);
        echo $this->view->render("kwDifference//kwDifference", ["title" => "Parametros de bloqueio | " . SITE,"rows" => $kwDifference, "read" => true, "stores" => $stores, "users" => $users ]);
        
    }

    public function readKwDifferenceOne() {
        if (isset($_GET['id'])){
            $id = $_GET['id'];
            $kwDifference = (new KwDifference())->findById($id);
        } else {
            echo "Registro não encontrado";
        }
        $users = (new User())->find("class = 'A'")->fetch(true);
        echo $this->view->render("kwDifference/kwDifferenceOne", ["title" => "Parametros de bloqueio | " . SITE,"rows" => $kwDifference, "users" => $users]);  
    }

    public function updateKwDifference() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $id = $_COOKIE['id'];
        $lockParameterId = $_POST['id'];
        $operator = $_POST['operator'];
        $status = $_POST['status'];
        $kwDifference = (new KwDifference())->findById($lockParameterId);
        $kwDifference->operator_id = $operator;
        $kwDifference->status = $status;
        $kwDifferenceUpdate = $kwDifference->save();
        
        if (!$kwDifferenceUpdate) {
            $data['sucesso'] = false;
            $data['msg'] = "Falha ao alterar Atividade.";
            echo json_encode($data);
        } else {
            $data['sucesso'] = true;
            $data['msg'] = "Atividade alterada com sucesso.";
            echo json_encode($data);
        }
    }

    public function deleteKwDifference() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $id = $_COOKIE['id'];
        $kwDifferenceId = $_POST['id'];
        $kwDifference = (new KwDifference())->findById($kwDifferenceId);
        $kwDifferenceDestroy = $kwDifference->destroy();
        
        if (!$kwDifferenceDestroy) {
            $data['sucesso'] = false;
            $data['msg'] = "Falha ao deletar Atividade.";
            echo json_encode($data);
        } else {
            $data['sucesso'] = true;
            $data['msg'] = "Atividade deletada com sucesso.";
            echo json_encode($data);
        }
    }
}