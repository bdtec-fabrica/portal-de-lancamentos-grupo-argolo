<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\ExchangeCode;
use Source\Models\ExchangePackaging;
use Source\Models\UserClass;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use SendGrid\Mail\Mail;
use SendGrid;


class Parameters 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function addExchangeCode() {
        $store = $_POST['store'];
        $cnpj = $_POST['cnpj'];
        $sourceCode = $_POST['sourceCode'];
        $destinationCode = $_POST['destinationCode'];

        $exchangeCode = (new ExchangeCode());

        $check = $exchangeCode->find("store_id = :store_id and cnpj = :cnpj and source_code = :source_code and destination_code = :destination_code","store_id=$store&cnpj=$cnpj&source_code=$sourceCode&destination_code=$destinationCode")->count();
        if ($check >  0) {
            $data['sucesso'] = false;
            $data['msg'] = "Parametrização já realizada.";
            echo json_encode($data);
        } else {
            $exchangeCode->store_id = $store;
            $exchangeCode->cnpj = $cnpj;
            $exchangeCode->source_code = $sourceCode;
            $exchangeCode->destination_code = $destinationCode;
            $exchangeCodeId = $exchangeCode->save();

            if ($exchangeCodeId) {
                $data['sucesso'] = true;
                $data['msg'] = "Parametrização realizada com sucesso.";
                echo json_encode($data);
            } else {
                $data['sucesso'] = false;
                $data['msg'] = "Falha ao cadastrar parametrização.";
                echo json_encode($data);
            }
        }
    }

    public function exchangeCode($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");

        } else {
            if ($data['id']) {
                $ids = $data['id'];
                $exchangeCode = (new ExchangeCode());
                // $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
                // $paginator = new Paginator("https://www.bdteclancamentos.bispoedantas.com.br/exchangeCode/$ids?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
                // $paginator->pager($exchangeCode->find("id in ($ids)")->count(), 8, $page, 2);
                $code = $exchangeCode->find("id in ($ids)")->order("store_id,cnpj DESC")->fetch(true);                    
                // $paginatorRender = $paginator->render();
                echo $this->view->render("administrator/exchangeCode", ["title" => "Troca Codigo | " . SITE,
                "exchangeCode" => $code,
                ]);  

            } else {
                $exchangeCode = (new ExchangeCode());
                // $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
                // $paginator = new Paginator("https://www.bdteclancamentos.bispoedantas.com.br/exchangeCode?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
                // $paginator->pager($exchangeCode->find()->count(), 8, $page, 2);
                // $code = $exchangeCode->find()->order("store_id,cnpj DESC")->fetch(true);                    
                // $paginatorRender = $paginator->render();
                echo $this->view->render("administrator/exchangeCode", ["title" => "Troca Codigo | " . SITE,
                "exchangeCode" => $code,
                ]); 
            }
            
        }
    }

    public function searchExchangeCode() {
        $storeId = $_POST['storeId'];
        $cnpj = $_POST['cnpj'];

        if ($storeId != '') {
            $params['store_id'] = $storeId;
            $comparation[10] = "store_id = :store_id";
        }

        if ($cnpj != '') {
            $params['cnpj'] = $cnpj;
            $comparation[99] = "cnpj = :cnpj";
        }

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);

        $exchangeCodes = (new ExchangeCode())->find($comparation, $params)->fetch(true);
        $i = 0;
        foreach($exchangeCodes as $exchangeCode) {
            $i++;
            $id[$i] = $exchangeCode->id;
        }
        if (is_array($id)) {
            $ids = implode(',',$id);
        }
        

        if (!$ids) {
            $ids = -1;
        }

        $this->router->redirect("/exchangeCode/{$ids}");


    }

    public function editExchangeCode() {
        $id = $_POST['id'];
        $sourceCode = $_POST['sourceCode'];
        $destinationCode = $_POST['destinationCode'];
        $exchageCode = (new ExchangeCode())->findById($id);
        $exchageCode->source_code = $sourceCode;
        $exchageCode->destination_code = $destinationCode;
        $exchageCodeId = $exchageCode->save();
        if ($exchageCodeId) {
            $return['success'] = true;
            $return['message'] = "Alteração realziada!";
            echo json_encode($return);
            return;
        } else  {
            $return['success'] = false;
            $return['message'] = "Alteração não realziada!";
            echo json_encode($return);
            return;
        }
    }




    public function deleteExchangeCode() {
        $id = $_POST['id'];
        $exchageCode = (new ExchangeCode())->findById($id);
        $exchageCode->destroy();
        $return['success'] = true;
        $return['message'] = "Exclusão realizada!";
        echo json_encode($return);
        return;
    }
    
    public function packagingCode($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");

        } else {
            if ($data['id']) {
                $ids = $data['id'];
                $packagingCode = (new ExchangePackaging());
                $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
                $paginator = new Paginator(URL_BASE."/packagingCode?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
                $paginator->pager($packagingCode->find()->count(), 8, $page, 2);
                $code = $packagingCode->find("id in ($ids)")->limit($paginator->limit())->offset($paginator->offset())->order("store_id,cnpj DESC")->fetch(true);                    
                $paginatorRender = $paginator->render();
                echo $this->view->render("administrator/packagingCode", ["title" => "Troca Codigo | " . SITE,
                    "packagingCode" => $code,
                    "paginator" => $paginator
                ]);
            } else {
                $packagingCode = (new ExchangePackaging());
                $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
                $paginator = new Paginator(URL_BASE."/packagingCode?page=", "Página", ["Primeira Página", "Primeira"], ["Ultima Página", "Ultima"]);
                $paginator->pager($packagingCode->find()->count(), 8, $page, 2);
                $code = $packagingCode->find()->limit($paginator->limit())->offset($paginator->offset())->order("store_id,cnpj DESC")->fetch(true);                    
                $paginatorRender = $paginator->render();
                echo $this->view->render("administrator/packagingCode", ["title" => "Troca Codigo | " . SITE,
                    "packagingCode" => $code,
                    "paginator" => $paginator
                ]);  
            }
            
        }
    }

    public function searchpackagingCode() {
        $storeId = $_POST['storeId'];
        $cnpj = $_POST['cnpj'];

        if ($storeId != '') {
            $params['store_id'] = $storeId;
            $comparation[10] = "store_id = :store_id";
        }

        if ($cnpj != '') {
            $params['cnpj'] = $cnpj;
            $comparation[99] = "cnpj = :cnpj";
        }

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);

        $exchangeCodes = (new ExchangePackaging())->find($comparation, $params)->fetch(true);
        $i = 0;
        foreach($exchangeCodes as $exchangeCode) {
            $i++;
            $id[$i] = $exchangeCode->id;
        }
        if (is_array($id)) {
            $ids = implode(',',$id);
        }
        

        if (!$ids) {
            $ids = -1;
        }

        $this->router->redirect("/packagingCode/{$ids}");
        
    }

    public function addpackagingCode() {
        $store = $_POST['store'];
        $cnpj = $_POST['cnpj'];
        $code = $_POST['code'];
        $sourcePacking = $_POST['sourcePacking'];
        $destinationPacking = $_POST['destinationPacking'];
        $amount = $_POST['amount'];

        $exchangeCode = (new ExchangePackaging());

        $check = $exchangeCode->find("store_id = :store_id and cnpj = :cnpj and code = :code","store_id=$store&cnpj=$cnpj&code=$code")->count();
        if ($check >  0) {
            $data['success'] = false;
            $data['message'] = "Parametrização já realizada.";
            echo json_encode($data);
        } else {
            $exchangeCode2 = (new ExchangePackaging());
            $exchangeCode2->store_id = $store;
            $exchangeCode2->cnpj = $cnpj;
            $exchangeCode2->code = $code;
            $exchangeCode2->packing_of = $sourcePacking;
            $exchangeCode2->packing_for = $destinationPacking;
            $exchangeCode2->quantity_for = $amount;
            $exchangeCodeId = $exchangeCode2->save();

            if ($exchangeCodeId) {
                $data['success'] = true;
                $data['message'] = "Parametrização realizada com sucesso.";
                echo json_encode($data);
            } else {
                $data['success'] = false;
                $data['message'] = "Falha ao cadastrar parametrização.";
                echo json_encode($data);
            }
        }
    }

    public function editpackagingCode() {
        $id = $_POST['id'];
        $sourceCode = $_POST['sourceCode'];
        $destinationCode = $_POST['destinationCode'];
        $exchageCode = (new ExchangePackaging())->findById($id);
        $exchageCode->source_code = $sourceCode;
        $exchageCode->destination_code = $destinationCode;
        $exchageCodeId = $exchageCode->save();
        if ($exchageCodeId) {
            $return['success'] = true;
            $return['message'] = "Alteração realziada!";
            echo json_encode($return);
            return;
        } else  {
            $return['success'] = false;
            $return['message'] = "Alteração não realziada!";
            echo json_encode($return);
            return;
        }
    }

    public function deletepackagingCode() {
        $id = $_POST['id'];
        $exchageCode = (new ExchangePackaging())->findById($id);
        $exchageCode->destroy();
        $return['success'] = true;
        $return['message'] = "Exclusão realizada!";
        echo json_encode($return);
        return;
    }
}