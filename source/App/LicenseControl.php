<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\Call;
use Source\Models\CallAttachment;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\NfRms;
use Source\Models\AA1CTCON3;
use Source\Models\AA3CNVCC;
use Source\Models\Inventories;
use Source\Models\AA3CNVCC2;
use Source\Models\AA2CTIPO;
use Source\Models\AA2CTIPO3;
use Source\Models\AGGFLSPROD7;
use Source\Models\AA2CTIPO2;
use Source\Models\Status;
use Source\Models\Counting;
use Source\Models\StockMovement;
use Source\Models\Session;
use Source\Models\UserClass;
use Source\Models\Inventory;
use Source\Models\Marketing;
use Source\Models\NfType;
use Source\Models\Audit;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SendGrid\Mail\Mail;
use Source\Models\Aa3citemnm;
use SendGrid;
use Symfony\Component\DomCrawler\Crawler;
use Source\Models\NfReceipt;
use Source\Models\NfReceipt2;
use Source\Models\NfStatus;
use Source\Models\NfAttachment;
use Source\Models\NfDueDate;
use Source\Models\NfOperation;
use Source\Models\NfReceiptLog;
use Source\Models\NfReport;
use Source\Models\NfSefaz;
use Source\Models\ExchangeCode;
use Source\Models\NfEvent;
use Dompdf\Dompdf;
use Source\Models\NfStarted;
use Source\Models\RegisterAmountItems;
use Source\Models\ReasonPendingClient;
use Source\Models\AA1CFISC;
use Source\Models\AA1CFISC2;
use Source\Models\AA1LINHP;
use Source\Models\AA1LINHP2;
use Source\Models\AA3CITEM;
use Source\Models\AA3CITEM2;
use Source\Models\AA3CITEM7;
use Source\Models\AA3CITEM3;
use Source\Models\AG1IENSA;
use Source\Models\AA1AGEDT;
use Source\Models\AG1IENSA2;
use Source\Models\AG1IENSA3;
use Source\Models\AG1IENSA7;
use Source\Models\AA1CTCON2;
use Source\Models\AA2CESTQ;
use Source\Models\AA2CESTQ2;
use Source\Models\AA2CESTQ3;
use Source\Models\AA3CNVCC3;
use Source\Models\ButcheryRegistration;
use Source\Models\ExchangePackaging;
use Source\Models\RequestScore;
use Source\Models\InventoryItems;
use Source\Models\Itens;
use Source\Models\MarginVerification;
use Source\Models\PostedVerification;
use Source\Models\Itens51;
use Source\Models\DIMPER;
use Source\Models\DIMPER2;
use Source\Models\DIMPER3;
use Source\Models\AGGFLSPROD;
use Source\Models\AGGFLSPROD2;
use Source\Models\AGGFLSPROD3;
use Source\Models\CAPCUPOM;
use Source\Models\CAPCUPOM2;
use Source\Models\AG1PDVPD2;
use Source\Models\AG1PDVPD;
use Source\Models\AA2CPARA;
use Source\Models\AA2CPARA2;
use Source\Models\NotificationsAudit;
use Source\Models\DEVCLICP;
use Source\Models\NFE_CONTROLE;
use Source\Models\NFE_CONTROLE2;
use DateTime;
use Source\Models\AA1CTCON;
use Source\Models\AA3CITEM4;
use Source\Models\AA1CTCON4;
use Source\Models\AG1IENSA4;
use Source\Models\MapFamilia;
use Source\Models\AGGFLSPROD4;
use Source\Models\AA1DITEM;
use Source\Models\AA1DITEM2;
use Source\Models\AA1DITEM3;
use Source\Models\AA1DITEM4;
use Source\Models\NCM;
use Source\Models\AA1CFISC3;
use Source\Models\AA1CFISC4;
use Source\Models\AA0LOGIT;
use Source\Models\AA0LOGIT2;
use Source\Models\AA0LOGIT3;
use Source\Models\AA0LOGIT4;
use Source\Models\AA0LOGIT5;
use Source\Models\CadastroExterno;
use Source\Models\NCMALIQUOTA;
use Source\Models\NCMPIS;
use Source\Models\SupplierRegistration;
use Source\Models\AA1FORIT;
use Source\Models\AA1FORIT2;
use Source\Models\AA1FORIT3;
use Source\Models\AA1FORIT4;
use Source\Models\AA3CCEAN;
use Source\Models\AA3CCEAN2;
use Source\Models\AuditoriaIcmsEan;
use Source\Models\AA3CCEAN3;
use Source\Models\AA3CCEAN4;
use Source\Models\AA2CTIPO4;
use League\Csv\Writer;
use PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart;
use Source\Models\License;


class LicenseControl 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function create() {
        $licence = new License();
        $licence->store = $_POST['store'];
        $licence->sistem = $_POST['system'];
        $licence->date = $_POST['date'];
        $licenceId = $licence->save();
        if ($licenceId) {
            $data['message'] = "Licença Cadastrada";
            $data['success'] = true;
            return $data;
        } else {
            $data['message'] = "Licença não Cadastrada";
            $data['success'] = false;
            return $data;
        }
    }

    public function read($data) {
        $array["jid"] = "557199506104@c.us";
        $array["type"] = "text";
        $array["payload"]["message"] = "Integração funcinou !";

        $json = json_encode($array);

        $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($json),
            'Host: ' . strlen($json),
            'User-Agent: PostmanRuntime/7.29.2',
            'Accept: */*',
            'Accept-Encoding: gzip, deflate, br',
            'Connection: keep-alive')                                                                       
        );

        $jsonRet = curl_exec($ch);
        dd($jsonRet);

        // if (!empty($data['id'])) {
        //     $licenses = new License();
        //     $licenses = $licenses->find("id in (".$data['id'].")")->fetch(true);
        // } else {
        //     $licenses = new License();
        //     $licenses = $licenses->find()->fetch(true);
        // }
        
        
        // echo $this->view->render("administrator/licenceControl", 
        // ["id" => "Controle de Licenças | " . SITE ,
        // "licenses" => $licenses
        // ]);
    }

    public function update() {
        $licence = (new License())->findById($_POST['id']);
        $licence->store = $_POST['store'];
        $licence->system = $_POST['system'];
        $licence->date = $_POST['date'];
        $licenceId = $licence->save();

        if ($licenceId) {
            $data['message'] = "Licença Alterada";
            $data['success'] = true;
            echo json_encode($data);
        } else {
            $data['message'] = "Licença não Alterada";
            $data['success'] = false;
            echo json_encode($data);
        }
    }

    public function delete() {
        $licence = (new License())->findById($_POST['id']);
        $licenceId = $licence->destroy();
        if ($licenceId) {
            $data['message'] = "Licença Deletada";
            $data['success'] = true;
            return $data;
        } else {
            $data['message'] = "Licença não Deletada";
            $data['success'] = false;
            return $data;
        }
    }
}