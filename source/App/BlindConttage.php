<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\Status;
use Source\Models\Session;
use Source\Models\UserClass;
use Source\Models\NfType;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SendGrid\Mail\Mail;
use SendGrid;
use Symfony\Component\DomCrawler\Crawler;
use Source\Models\NfReceipt;
use Source\Models\NfStatus;
use Source\Models\NfAttachment;
use Source\Models\NfDueDate;
use Source\Models\NfOperation;
use Source\Models\NfReceiptLog;
use Source\Models\NfSefaz;
use Source\Models\ExchangeCode;
use Source\Models\NfStarted;
use Source\Models\ExchangePackaging;
use Source\Models\CollectionHeader;
use Source\Models\CollectionItems;
use Source\Models\CollectionNfeItems;
use NFePHP\NFe\ToolsNFe;
use NFePHP\Extras\Danfe;
use NFePHP\Common\Files\FilesFolders;

class BlindConttage 
{

    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function newConttage() {
        
        if ($_POST['accessKey'][1]) {
            $xml = simplexml_load_file(__DIR__."/../XML/".$_POST['store'].'/NFe'.$_POST['accessKey'][1].'.xml');
            if ($xml) {
                $cnpj = (string)$xml->NFe->infNFe->emit->CNPJ;
                $razao = $xml->NFe->infNFe->emit->xNome;

                $collectionHeader = new CollectionHeader();
                $collectionHeader->store_id = $_POST['store'];
                $collectionHeader->cnpj = $cnpj;
                $collectionHeader->razao = $razao;
                $collectionHeaderId = $collectionHeader->save();

                if ($collectionHeaderId) {
                    for ($i=1;$i<=count($_POST['accessKey']);$i++){
                       
                        $xml = simplexml_load_file(__DIR__."/../XML/".$_POST['store'].'/NFe'.$_POST['accessKey'][$i].'.xml');
                        $items = $xml->NFe->infNFe->det;
                        foreach ($items as $item) {
                            $collectionItemsCount = (new CollectionItems())->find("collection_header_id = :chid and reference = :ref and description = :dsc","chid=$collectionHeader->id&ref={$item->prod->cProd}&dsc={$item->prod->xProd}");
                            if ($collectionItemsCount->count()>0) {
                                $id = $collectionItemsCount->fetch()->id;
                                $collectionItems = (new CollectionItems())->findById($id);
                                $collectionItems->quantity += $item->prod->qCom;
                                $collectionItemsId = $collectionItems->save();

                                $collectionNfeItems = new CollectionNfeItems();
                                $collectionNfeItems->collection_header_id = $collectionHeader->id;
                                $collectionNfeItems->access_key = $_POST['accessKey'][$i];
                                $collectionNfeItems->reference = $item->prod->cProd;
                                $collectionNfeItems->ean = $item->prod->cEAN;
                                $collectionNfeItems->description = $item->prod->xProd;
                                $collectionNfeItems->packing = $item->prod->uCom;
                                $collectionNfeItems->quantity = $item->prod->qCom;
                                $collectionNfeItemsId = $collectionNfeItems->save();
                            } else {
                                $collectionItems = new CollectionItems();
                                $collectionItems->collection_header_id = $collectionHeader->id;
                                $collectionItems->reference = $item->prod->cProd;
                                $collectionItems->ean = $item->prod->cEAN;
                                $collectionItems->description = $item->prod->xProd;
                                $collectionItems->packing = $item->prod->uCom;
                                $collectionItems->quantity = $item->prod->qCom;
                                $collectionItemsId = $collectionItems->save();

                                $collectionNfeItems = new CollectionNfeItems();
                                $collectionNfeItems->collection_header_id = $collectionHeader->id;
                                $collectionNfeItems->access_key = $_POST['accessKey'][$i];
                                $collectionNfeItems->reference = $item->prod->cProd;
                                $collectionNfeItems->ean = $item->prod->cEAN;
                                $collectionNfeItems->description = $item->prod->xProd;
                                $collectionNfeItems->packing = $item->prod->uCom;
                                $collectionNfeItems->quantity_nfe = $item->prod->qCom;
                                $collectionNfeItemsId = $collectionNfeItems->save();
                            }
                            
                        }

            
                    }
                    $data['success'] = true;
                    $data['message'] = "Conferencia $collectionHeader->id adicionada com success!";
                    echo json_encode($data);
                    die;
                } else {
                    $data['success'] = false;
                    $data['message'] = "Não foi possivel adicionar o cabeçalho!";
                    echo json_encode($data);
                    die;
                }

            } else {
                $data['success'] = false;
                $data['message'] = "Não foi encontrado o XML para essa Chave de acesso e Loja!!";
                echo json_encode($data);
                die;
            }

        } else {
            $data['success'] = false;
            $data['message'] = "Não foi informada a chave de Acesso!!";
            echo json_encode($data);
            die;
        }
    }

    public function startConttage($data) {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        if($class=='O') {
            $id = $data['id'];
            $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRIPPED);
            
            $paginator = new Paginator("https://www.bdtecsolucoes.com.br/blindColletion/{$id}?page=");
            $paginator->pager((new CollectionItems())->find("collection_header_id = :chid and quantity != quantity_found","chid=$id")->count(), 1, $page, 1);
            $collectionItems = (new CollectionItems())->find("collection_header_id = :chid  and quantity != quantity_found","chid=$id")->limit($paginator->limit())->offset($paginator->offset())->fetch(true);
    
            
            echo $this->view->render("operator/startColletion", ["title" => "Conferência Cega | " . SITE, 
            "items" => $collectionItems,
            "id" => $data['id'],
            "paginator" => $paginator
            ]);
        } else {
            $id = $data['id'];
            $collectionItems = (new CollectionItems())->find("collection_header_id = :chid","chid=$id")->fetch(true);
            $collectionHeader = (new CollectionHeader())->findById($id);
            
            echo $this->view->render("client/startColletion", ["title" => "Conferência Cega | " . SITE, 
            "items" => $collectionItems,
            "id" => $data['id'],
            "collectionHeader" => $collectionHeader
            ]);
        }
        

        
    }

    public function getRomaneio($data) {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;

        $id = $data['id'];
        $collectionItems = (new CollectionItems())->find("collection_header_id = :chid","chid=$id")->fetch(true);
        $collectionHeader = (new CollectionHeader())->findById($id);
          
        echo $this->view->render("client/romaneio", ["title" => "Conferência Cega | " . SITE, 
            "items" => $collectionItems,
            "id" => $data['id'],
            "collectionHeader" => $collectionHeader
        ]);      
    }

    public function saveConttage() {
        $id = $_POST['id'];
        $conference = "C";
            $collectionItems = (new CollectionItems())->findById($_POST["code"]);

            
            $collectionItems->ean_found = $_POST["ean_found"];
            $collectionItems->quantity_found = $_POST["quantity_found"];
            $collectionItemsId = $collectionItems->save();


            if ($collectionItems->quantity != $_POST["quantity_found"]) {
                $conference = 'E';
            }


            $collectionNfeItems = (new CollectionNfeItems())->find("collection_header_id = :chid and reference = :ref", "chid=$id&ref=$collectionItems->reference")->fetch(true);
            foreach($collectionNfeItems as $items) {
                $collectionNfeItemsU = (new CollectionNfeItems())->findById($items->id);
                $collectionNfeItemsU->ean_found = $_POST["ean_found"];
                $collectionNfeItemsU->quantity_found = $_POST["quantity_found"];
                $_POST["quantity_found$i"] = $_POST["quantity_found"] - $collectionNfeItemsU->quantity_found;
                $collectionNfeItemsUId = $collectionNfeItemsU->save();
            }

        if ($conference == "E") {
            $collectionHeader = (new CollectionHeader())->findById($id);
            $collectionHeader->status = $conference;
            $collectionHeaderId = $collectionHeader->save();
            $data['success'] = true;
            // $data['message'] = "Conferência salva com sucesso! Quantidades Diveregentes!";
            echo json_encode($data);
        } else {
            // $collectionHeader = (new CollectionHeader())->findById($id);
            // $collectionHeader->status = $conference;
            // $collectionHeaderId = $collectionHeader->save();
            $data['success'] = true;
            // $data['message'] = "Conferência salva com sucesso!! ";
            echo json_encode($data);
        }

        

    }
}