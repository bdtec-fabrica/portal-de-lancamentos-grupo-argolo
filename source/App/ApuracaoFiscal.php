<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\Call;
use Source\Models\CallAttachment;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\NfRms;
use Source\Models\AA3CNVCC;
use Source\Models\AA3CNVCC2;
use Source\Models\AA2CTIPO;
use Source\Models\AA2CTIPO3;
use Source\Models\AA2CTIPO2;
use Source\Models\Status;
use Source\Models\Counting;
use Source\Models\StockMovement;
use Source\Models\Session;
use Source\Models\UserClass;
use Source\Models\Inventory;
use Source\Models\Marketing;
use Source\Models\NfType;
use Source\Models\Audit;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SendGrid\Mail\Mail;
use SendGrid;
use Symfony\Component\DomCrawler\Crawler;
use Source\Models\NfReceipt;
use Source\Models\NfReceipt2;
use Source\Models\NfStatus;
use Source\Models\NfAttachment;
use Source\Models\NfDueDate;
use Source\Models\NfOperation;
use Source\Models\NfReceiptLog;
use Source\Models\NfReport;
use Source\Models\NfSefaz;
use Source\Models\ExchangeCode;
use Source\Models\NfEvent;
use Dompdf\Dompdf;
use Source\Models\NfStarted;
use Source\Models\RegisterAmountItems;
use Source\Models\ReasonPendingClient;
use Source\Models\AA1CFISC;
use Source\Models\AA1CFISC2;
use Source\Models\AA1LINHP;
use Source\Models\AA1LINHP2;
use Source\Models\AA3CITEM;
use Source\Models\AA3CITEM2;
use Source\Models\AA3CITEM3;
use Source\Models\AG1IENSA;
use Source\Models\AG1IENSA2;
use Source\Models\AA2CESTQ;
use Source\Models\AA2CESTQ2;
use Source\Models\AA2CESTQ3;
use Source\Models\AA3CNVCC3;
use Source\Models\ButcheryRegistration;
use Source\Models\ExchangePackaging;
use Source\Models\RequestScore;

class ApuracaoFiscal 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function getApuracao() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
            foreach ($userStoreGroup as $userGroups) {
                $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
            }
            
            if (is_array($groups)){
                $groups = implode(", ",$groups);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                
            }
        $butcheryRegistration = (new ButcheryRegistration())->find()->fetch(true);
        $audit = (new Audit())->find()->fetch(true);
        if ($class == 'A') {
            echo $this->view->render("apuracaoFiscal/apuracao", ["id" => "Apuração | " . SITE,
            "lists" => $butcheryRegistration,
            "apuracoes" => $audit
            ]);
        }
       
    }

    public function getApuracaoSearch() {
        
        $type = $_POST['type'];
        $movimento = $_POST['movimento'];
        $date1 = $_POST['date1'];
        $date2 = $_POST['date2'];
        $storeId = $_POST['storeId'];
        $audit = $_POST['audit'];
        $itensMovimentacao = $_POST['itens-movimentacao'];

        if ($audit > 0) {
           
            $audit = (new Audit())->findById($audit);
        
            
            if ($audit->secao == 0){
                $butcheryRegistration = (new ButcheryRegistration())->find("store_id = :sid and audit_id = :ai","sid=$storeId&ai=$audit->id")->fetch(true);
            } else {
                if ($audit->secao != 0) {
                    $params['GIT_SECAO'] = $audit->secao;
                    $comparation[2] = "GIT_SECAO = :GIT_SECAO";
                }
                if ($audit->grupo != 0) {
                    $params['GIT_GRUPO'] = $audit->grupo;
                    $comparation[3] = "GIT_GRUPO = :GIT_GRUPO";
                }
                if ($audit->subgrupo != 0) {
                    $params['GIT_SUBGRUPO'] = $audit->subgrupo;
                    $comparation[4] = "GIT_SUBGRUPO = :GIT_SUBGRUPO";
                }
                
                $comparation = implode(' and ', $comparation);
                
                $params = http_build_query($params);
                $st = substr($storeId,0,strlen($storeId)-1);

                $store = (new Store())->find("code = :code","code=$st")->fetch();
                
                if ($store->group_id == 1 || $store->group_id == 10) {

                    $butcheryRegistration = (new AA3CITEM())->find($comparation,$params)->fetch(true);

                } elseif ($store->group_id == 2) {
                    
                    $butcheryRegistration = (new AA3CITEM2())->find($comparation,$params)->fetch(true);

                } else {
                     
                    $storeId = substr($storeId,0,strlen($storeId)-1);
                    $butcheryRegistration = (new AA3CITEM3())->find($comparation,$params)->fetch(true);

                }
                
            }
            
            
        } else {
            $butcheryRegistration = (new ButcheryRegistration())->find("store_id = :sid","sid=$storeId")->fetch(true);
        }

        echo $this->view->render("apuracaoFiscal/reportApuracao", ["id" => "Apuração | " . SITE,"butcheryRegistration" => $butcheryRegistration,
        "date1" => $date1, "date2" => $date2, "movimento" => $movimento, "type" => $type, "audit" => $audit, "storeId" => $storeId,
        "itensMovimentacao" => $itensMovimentacao
        ]);
    }

    public function getDates() {
        $store = $_POST['store'];
        $store = substr($store,0,strlen($store)-1);

        $store = (new Store())->find("code = :code","code=$store")->fetch();

        $data = $store->getInventories();

        if ($store->group_id == 1 || $store->group_id == 10) {
            $aa3cnvcc = (new AA3CNVCC())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        } elseif ($store->group_id == 2) {
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        } else {
            $aa3cnvcc = (new AA3CNVCC3())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        }
        foreach($aa3cnvcc as $cnvcc) {
            $data2[$cnvcc->NCC_SECAO] = $cnvcc->NCC_DESCRICAO;
        }
        
        echo json_encode($data);
        
    }

    public function getSecao() {
        $store = $_POST['store'];
        $secao = $_POST['secao'];

        $store = (new Store())->find("code = :code","code=$store")->fetch();
        if($store->group_id == 1 || $store->group_id == 10) {
            $aa3cnvcc = (new AA3CNVCC())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        } elseif($store->group_id == 2) {
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        } else {
            $aa3cnvcc = (new AA3CNVCC3())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        }
       
        foreach($aa3cnvcc as $cnvcc) {
            $data[$cnvcc->NCC_SECAO] = $cnvcc->NCC_DESCRICAO;
        }
        
        echo json_encode($data);
    }

    public function getGrupo() {
        $store = $_POST['store'];
        $secao = $_POST['secao'];

        $store = (new Store())->find("code = :code","code=$store")->fetch();
        if($store->group_id == 1 || $store->group_id == 10) {

            $aa3cnvcc = (new AA3CNVCC())->find("NCC_SECAO = :ns and NCC_GRUPO != :ng and NCC_SUBGRUPO = :nsg","ns=$secao&ng=0&nsg=0")->fetch(true);
        } elseif($store->group_id ==2) {
            
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_SECAO = :ns and NCC_GRUPO != :ng and NCC_SUBGRUPO = :nsg","ns=$secao&ng=0&nsg=0")->fetch(true);
        
        } else {
            
            $aa3cnvcc = (new AA3CNVCC3())->find("NCC_SECAO = :ns and NCC_GRUPO != :ng and NCC_SUBGRUPO = :nsg","ns=$secao&ng=0&nsg=0")->fetch(true);
        }
        
        foreach($aa3cnvcc as $cnvcc) {
            $data[$cnvcc->NCC_GRUPO] = $cnvcc->NCC_DESCRICAO;
        }
        
        echo json_encode($data);
    }

    public function getSubGrupo() {
        $store = $_POST['store'];
        $store = (new Store())->find("code = :code","code=$store")->fetch();
        $secao = $_POST['secao'];
        $grupo = $_POST['grupo'];
        if($store->group_id == 1 || $store->group_id == 10) {
            $aa3cnvcc = (new AA3CNVCC())->find("NCC_SECAO = :ns and NCC_GRUPO = :ng and NCC_SUBGRUPO != :nsg","ns=$secao&ng=$grupo&nsg=0")->fetch(true);
        } elseif($store->group_id ==2) {
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_SECAO = :ns and NCC_GRUPO = :ng and NCC_SUBGRUPO != :nsg","ns=$secao&ng=$grupo&nsg=0")->fetch(true);
        } else {
            $aa3cnvcc = (new AA3CNVCC3())->find("NCC_SECAO = :ns and NCC_GRUPO = :ng and NCC_SUBGRUPO != :nsg","ns=$secao&ng=$grupo&nsg=0")->fetch(true);
        }
        
        foreach($aa3cnvcc as $cnvcc) {
            $data[$cnvcc->NCC_SUBGRUPO] = $cnvcc->NCC_DESCRICAO;
        }
        
        echo json_encode($data);
    }

    public function addItemApuracao() {
        $store = $_POST['store'];
        $code = $_POST['code'];
        $description = $_POST['description'];

        $butcheryRegistration = new ButcheryRegistration();
        $butcheryRegistration->code = $code;
        $butcheryRegistration->store_id = $store;
        $butcheryRegistration->description = $description;
        $butcheryRegistrationId = $butcheryRegistration->save();

        $data['msg'] = "Cadastrado com sucesso!";
        $data['sucesso'] = true;
        echo json_encode($data);


    }

    public function addListApuracao() {
            $store = $_POST["store"];
            $title = $_POST["titulo"];
            $type = $_POST["tipo"];
            $secao = $_POST["secao"];
            $grupo = $_POST["grupo"];
            $subGrupo = $_POST["subgrupo"];

            if ($type == 2) {
                $audit = new Audit();
                $audit->description = $title;
                $audit->secao = $secao+0;
                $audit->grupo = $grupo+0;
                $audit->subgrupo = $subGrupo+0;
                $auditId = $audit->save();
                $data['msg'] = "Cadastrado com sucesso!";
                $data['sucesso'] = true;
                echo json_encode($data);
            } else {
                $audit = new Audit();
                $audit->description = $title;
                $auditId = $audit->save();
                for ($i=1;$i<=count($_POST['code']);$i++){
                    if ($_POST['code'][$i] == '' || $_POST['description'][$i]==''){
                     
                    } else {
                        $rWorksheet = new ButcheryRegistration();
                        $rWorksheet->audit_id = $audit->id;
                        $rWorksheet->store_id = $_POST['store'][$i];
                        $rWorksheet->code = $_POST['code'][$i];
                        $rWorksheet->description = $_POST['description'][$i];
                        $rWorksheetId = $rWorksheet->save();
                    }
                    

                }
                $data['msg'] = "Cadastrado com sucesso!";
                $data['sucesso'] = true;
                echo json_encode($data);
            }
    }
}