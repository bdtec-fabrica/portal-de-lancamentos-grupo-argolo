<?php

namespace Source\App;

use CoffeeCode\DataLayer\Connect2;
use CoffeeCode\DataLayer\Connect3;
use League\Plates\Engine;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\MlfNotaFiscal;
use Source\Models\Store;
use Source\Models\AA3CNVCC;
use Source\Models\Inventories;
use Source\Models\AA3CNVCC2;
use Source\Models\AA2CTIPO;
use Source\Models\AGGFLSPROD7;
use Source\Models\AA2CTIPO2;
use Source\Models\Audit;
use Monolog\Logger;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use Source\Models\Aa3citemnm;
use Source\Models\AG1PDVCC;
use Source\Models\NfReceipt;
use Source\Models\AG1PDVCC2;
use Source\Models\NfReport;
use Source\Models\NfSefaz;
use Source\Models\NfEvent;
use Source\Models\AA1CFISC;
use Source\Models\AG2DETNT2;
use Source\Models\AA1CFISC2;
use Source\Models\AA3CITEM;
use Source\Models\AA3CITEM2;
use Source\Models\AA3CITEM7;
use Source\Models\AA3CITEM3;
use Source\Models\AG1IENSA;
use Source\Models\AA1AGEDT;
use Source\Models\AG1IENSA2;
use Source\Models\AG1IENSA3;
use Source\Models\AG1IENSA7;
use Source\Models\AA1CTCON2;
use Source\Models\AA2CESTQ;
use Source\Models\AA2CESTQ2;
use Source\Models\AA2CESTQ3;
use Source\Models\TABLE;
use Source\Models\ButcheryRegistration;
use Source\Models\InventoryItems;
use Source\Models\Itens;
use Source\Models\MarginVerification;
use Source\Models\PostedVerification;
use Source\Models\DIMPER;
use Source\Models\DIMPER2;
use Source\Models\AGGFLSPROD;
use Source\Models\AGGFLSPROD2;
use Source\Models\AGGFLSPROD3;
use Source\Models\CAPCUPOM;
use Source\Models\CAPCUPOM2;
use Source\Models\AG1PDVPD2;
use Source\Models\AG1PDVPD;
use Source\Models\AA2CPARA;
use Source\Models\AA2CPARA2;
use Source\Models\NotificationsAudit;
use Source\Models\DEVCLICP;
use Source\Models\NFE_CONTROLE;
use Source\Models\NFE_CONTROLE2;
use DateTime;
use Source\Models\AA1CTCON;
use Source\Models\AA3CITEM4;
use Source\Models\AA1CTCON4;
use Source\Models\AG1IENSA4;
use Source\Models\MapFamilia;
use Source\Models\AGGFLSPROD4;
use Source\Models\AA1DITEM;
use Source\Models\AA1DITEM2;
use Source\Models\AA1DITEM4;
use Source\Models\NCM;
use Source\Models\AA1CFISC4;
use Source\Models\AA0LOGIT;
use Source\Models\AA0LOGIT2;
use Source\Models\CadastroExterno;
use Source\Models\NCMALIQUOTA;
use Source\Models\NCMPIS;
use Source\Models\AA1FORIT;
use Source\Models\AA1FORIT2;
use Source\Models\AA1FORIT4;
use Source\Models\AA3CCEAN;
use Source\Models\AA3CCEAN2;
use Source\Models\AA3CCEAN4;
use Source\Models\AA2CTIPO4;
use Source\Models\FiscalStores;
use Source\Models\FiscalPeriod;
use Source\Models\FiscalActivityType;
use Source\Models\FiscalActivity;
use NFePHP\Common\Soap\CurlSoap;
use Source\Models\AG1FENSA;
use Source\Models\AG1FENSA2;
use Source\Models\AA1AGEDT2;
use Source\Models\AA1FFISC;
use Source\Models\AG3VNFCC;
use Source\Models\VIEW_HORTI;
use Source\Models\VIEW_HORTI2;
use Source\Models\AG1CGFAT;
use Source\Models\AG1CGFAT2;
use Source\Models\MultiModels;

class Verifications 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function getApuracao() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
            foreach ($userStoreGroup as $userGroups) {
                $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
            }
            
            if (is_array($groups)){
                $groups = implode(", ",$groups);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                
            }
        $butcheryRegistration = (new ButcheryRegistration())->find()->fetch(true);
        $audit = (new Audit())->find()->fetch(true);
        if ($class == 'OG' || $class == 'OG2') {
            echo $this->view->render("operator/apuracao", ["id" => "Apuração | " . SITE,
            "lists" => $butcheryRegistration,
            "apuracoes" => $audit,
            "groups" => $groups
            ]);
        } else if ($class == 'A') {
            echo $this->view->render("administrator/apuracao", ["id" => "Apuração | " . SITE,
            "lists" => $butcheryRegistration,
            "apuracoes" => $audit
            ]);
        } else if ($class == 'C') {
            $store = (new Store())->find("code = :code","code=$store")->fetch();
            if ($store->group_id == 1 || $store->group_id == 10){
                $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :code","code=$store->code")->fetch();
                $aa2ctipo = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO;
            } else if ($store->group_id == 2) {
                $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = :code","code=$store->code")->fetch();
                $aa2ctipo = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO;
            } else if ($store->group_id == 8 || $store->group_id == 4) {
                // $aa2ctipo = (new AA2CTIPO3())->find("TIP_CODIGO||TIP_DIGITO = :code","code=$store->code")->fetch();
                // $aa2ctipo = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO."n";
                
            }
            echo $this->view->render("client/apuracao", ["id" => "Apuração | " . SITE,
            "lists" => $butcheryRegistration,
            "apuracoes" => $audit,
            "groups" => $groups,
            "store" => $aa2ctipo
            ]);
        }
       
    }

    public function getApuracaoSearch() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        $type = $_POST['type'];
        $movimento = $_POST['movimento'];
        $date1 = $_POST['date1'];
        $date2 = $_POST['date2'];
        $storeId = $_POST['storeId'];
        $storeId = str_replace("n",'',$storeId);
        $audit = $_POST['audit'];
        $itensMovimentacao = $_POST['itens-movimentacao'];

        
        if ($audit > 0) {
            
            $audit = (new Audit())->findById($audit);
        
            
            if ($audit->secao == 0){
                $butcheryRegistration = (new ButcheryRegistration())->find("store_id = :sid and audit_id = :ai","sid=$storeId&ai=$audit->id")->fetch(true);
              
            } else {
                
                if ($audit->secao != 0) {
                    $params['GIT_SECAO'] = $audit->secao;
                    $comparation[2] = "GIT_SECAO = :GIT_SECAO";
                }
                if ($audit->grupo != 0) {
                    $params['GIT_GRUPO'] = $audit->grupo;
                    $comparation[3] = "GIT_GRUPO = :GIT_GRUPO";
                }
                if ($audit->subgrupo != 0) {
                    $params['GIT_SUBGRUPO'] = $audit->subgrupo;
                    $comparation[4] = "GIT_SUBGRUPO = :GIT_SUBGRUPO";
                }
                
                $comparation = implode(' and ', $comparation);
                
                $params = http_build_query($params);
                $st = $storeId;

                $store = (new Store())->find("code = :code","code=$st")->fetch();

                if ($store->group_id == 1 || $store->group_id == 10) {

                    $butcheryRegistration = (new AA3CITEM())->find($comparation,$params)->fetch(true);

                } elseif ($store->group_id == 2) {
                    
                    $butcheryRegistration = (new AA3CITEM2())->find($comparation,$params)->fetch(true);

                } elseif ($store->group_id == 8 || $store->group_id == 4) {
                    // $butcheryRegistration = (new AA3CITEM3())->find($comparation,$params)->fetch(true);
                }
                
            }
            
            
        } else {
            $butcheryRegistration = (new ButcheryRegistration())->find("store_id = :sid","sid=$storeId")->fetch(true);
        }

        if ($class == 'A') {
            echo $this->view->render("administrator/reportApuracao", ["id" => "Apuração | " . SITE,"butcheryRegistration" => $butcheryRegistration,
            "date1" => $date1, "date2" => $date2, "movimento" => $movimento, "type" => $type, "audit" => $audit, "storeId" => $storeId,
            "itensMovimentacao" => $itensMovimentacao
            ]);
        } else if ($class == 'OG'  || $class == 'OG2') {
            echo $this->view->render("operator/reportApuracao", ["id" => "Apuração | " . SITE,"butcheryRegistration" => $butcheryRegistration,
        "date1" => $date1, "date2" => $date2, "movimento" => $movimento, "type" => $type, "audit" => $audit, "storeId" => $storeId,
        "itensMovimentacao" => $itensMovimentacao
        ]);
        } else if ($class == "C" || $class == "C2"){
            echo $this->view->render("client/reportApuracao", ["id" => "Apuração | " . SITE,"butcheryRegistration" => $butcheryRegistration,
            "date1" => $date1, "date2" => $date2, "movimento" => $movimento, "type" => $type, "audit" => $audit, "storeId" => $storeId,
            "itensMovimentacao" => $itensMovimentacao
            ]);
        }
        
    }

    public function getDates() {
        $store = $_POST['store'];
        $store = substr($store,0,strlen($store)-1);
        $storeCount = (new Store())->find("code = :code","code=$store")->count();

        if ($storeCount == 0) {
            $store = substr($store,0,strlen($store)-1);
            $store = (new Store())->find("code = :code","code=$store")->fetch();
        } else {
            $store = (new Store())->find("code = :code","code=$store")->fetch();
        }
        
        $data = $store->getInventories();

        if ($store->group_id == 1 || $store->group_id == 10) {
            $aa3cnvcc = (new AA3CNVCC())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        } elseif ($store->group_id == 2) {
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        } else {
            // $aa3cnvcc = (new AA3CNVCC3())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        }
        foreach($aa3cnvcc as $cnvcc) {
            $data2[$cnvcc->NCC_SECAO] = $cnvcc->NCC_DESCRICAO;
        }
        
        echo json_encode($data);
        
    }

    public function getSecao() {
        
        $store = $_POST['store'];
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        if ($usuario->class == "C") {
            $store = $usuario->store_code;
        }
        $secao = $_POST['secao'];
        
        $storeCount = (new Store())->find("code = :code","code=$store")->count();
        if ($storeCount == 0) {
            $store = substr($store,0,strlen($store)-1);
            $store = (new Store())->find("code = :code","code=$store")->fetch();
        } else {
            $store = (new Store())->find("code = :code","code=$store")->fetch();
        }
        if($store->group_id == 1 || $store->group_id == 10) {
            $aa3cnvcc = (new AA3CNVCC())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        } elseif($store->group_id == 2) {
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        } elseif($store->group_id == 4 || $store->group_id == 8){
            // $aa3cnvcc = (new AA3CNVCC3())->find("NCC_GRUPO = :ng and NCC_SUBGRUPO = :nsg","ng=0&nsg=0")->fetch(true);
        }

       
        foreach($aa3cnvcc as $cnvcc) {
            $data[$cnvcc->NCC_SECAO] = $cnvcc->NCC_DESCRICAO;
        }
        
        echo json_encode($data);
    }

    public function getGrupo() {
        $store = $_POST['store'];
        $secao = $_POST['secao'];
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        if ($usuario->class == "C") {
            $store = $usuario->store_code;
        }

        $storeCount = (new Store())->find("code = :code","code=$store")->count();
        if ($storeCount == 0) {
            $store = substr($store,0,strlen($store)-1);
            $store = (new Store())->find("code = :code","code=$store")->fetch();
        } else {
            $store = (new Store())->find("code = :code","code=$store")->fetch();
        }
        if($store->group_id == 1 || $store->group_id == 10) {

            $aa3cnvcc = (new AA3CNVCC())->find("NCC_SECAO = :ns and NCC_GRUPO != :ng and NCC_SUBGRUPO = :nsg","ns=$secao&ng=0&nsg=0")->fetch(true);
        } elseif($store->group_id ==2) {
            
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_SECAO = :ns and NCC_GRUPO != :ng and NCC_SUBGRUPO = :nsg","ns=$secao&ng=0&nsg=0")->fetch(true);
        
        } else {
            
            // $aa3cnvcc = (new AA3CNVCC3())->find("NCC_SECAO = :ns and NCC_GRUPO != :ng and NCC_SUBGRUPO = :nsg","ns=$secao&ng=0&nsg=0")->fetch(true);
        }
        
        foreach($aa3cnvcc as $cnvcc) {
            $data[$cnvcc->NCC_GRUPO] = $cnvcc->NCC_DESCRICAO;
        }
        
        echo json_encode($data);
    }

    public function getSubGrupo() {
        $store = $_POST['store'];
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        if ($usuario->class == "C") {
            $store = $usuario->store_code;
        }
        $storeCount = (new Store())->find("code = :code","code=$store")->count();
        if ($storeCount == 0) {
            $store = substr($store,0,strlen($store)-1);
            $store = (new Store())->find("code = :code","code=$store")->fetch();
        } else {
            $store = (new Store())->find("code = :code","code=$store")->fetch();
        }
        $secao = $_POST['secao'];
        $grupo = $_POST['grupo'];
        if($store->group_id == 1 || $store->group_id == 10) {
            $aa3cnvcc = (new AA3CNVCC())->find("NCC_SECAO = :ns and NCC_GRUPO = :ng and NCC_SUBGRUPO != :nsg","ns=$secao&ng=$grupo&nsg=0")->fetch(true);
        } elseif($store->group_id ==2) {
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_SECAO = :ns and NCC_GRUPO = :ng and NCC_SUBGRUPO != :nsg","ns=$secao&ng=$grupo&nsg=0")->fetch(true);
        } else {
            // $aa3cnvcc = (new AA3CNVCC3())->find("NCC_SECAO = :ns and NCC_GRUPO = :ng and NCC_SUBGRUPO != :nsg","ns=$secao&ng=$grupo&nsg=0")->fetch(true);
        }
        
        foreach($aa3cnvcc as $cnvcc) {
            $data[$cnvcc->NCC_SUBGRUPO] = $cnvcc->NCC_DESCRICAO;
        }
        
        echo json_encode($data);
    }

    public function addItemApuracao() {
        $store = $_POST['store'];
        $code = $_POST['code'];
        $description = $_POST['description'];

        $butcheryRegistration = new ButcheryRegistration();
        $butcheryRegistration->code = $code;
        $butcheryRegistration->store_id = $store;
        $butcheryRegistration->description = $description;
        $butcheryRegistrationId = $butcheryRegistration->save();

        $data['msg'] = "Cadastrado com sucesso!";
        $data['sucesso'] = true;
        echo json_encode($data);


    }

    public function addListApuracao() {
            $store = $_POST["store1"];
            $title = $_POST["titulo"];
            $type = $_POST["tipo"];
            $secao = $_POST["secao"];
            $grupo = $_POST["grupo"];
            $subGrupo = $_POST["subgrupo"];

            if ($type == 2) {
                $audit = new Audit();
                $audit->description = $title;
                $audit->secao = $secao+0;
                $audit->grupo = $grupo+0;
                $audit->subgrupo = $subGrupo+0;
                $auditId = $audit->save();
                $data['msg'] = "Cadastrado com sucesso!";
                $data['sucesso'] = true;
                echo json_encode($data);
            } else {
                $audit = new Audit();
                $audit->description = $title;
                $auditId = $audit->save();
                for ($i=1;$i<=count($_POST['code']);$i++){
                    if ($_POST['code'][$i] == '' || $_POST['description'][$i]==''){
                     
                    } else {
                        $rWorksheet = new ButcheryRegistration();
                        $rWorksheet->audit_id = $audit->id;
                        $rWorksheet->store_id = $_POST['store'][$i];
                        $rWorksheet->code = $_POST['code'][$i];
                        $rWorksheet->description = $_POST['description'][$i];
                        $rWorksheetId = $rWorksheet->save();
                    }
                    

                }
                $data['msg'] = "Cadastrado com sucesso!";
                $data['sucesso'] = true;
                echo json_encode($data);
            }
    }

    public function addInventory() {
     
        $inventories = new Inventories();
        //`id`, ``, `title`, `list`, `date_inventory`, `created_at`, `updated_at`
        $inventories->store_id = $_POST['store'];
        $inventories->title = $_POST['title'];
        $inventories->list = $_POST['audit'];
        $inventories->date_inventory = $_POST['date'];
        $inventoriesId = $inventories->save();
        $id = $inventories->id;

        $audit = (new Audit())->findById($_POST['audit']);

        if ($audit->secao != 0) {
            $params['GIT_SECAO'] = $audit->secao;
            $comparation[2] = "GIT_SECAO = :GIT_SECAO";
        } else {
            $items = (new ButcheryRegistration())->find("audit_id = :ai","ai=$audit->id")->fetch(true);
        }

        if ($audit->grupo != 0) {
            $params['GIT_GRUPO'] = $audit->grupo;
            $comparation[3] = "GIT_GRUPO = :GIT_GRUPO";
        }
        if ($audit->subgrupo != 0) {
            $params['GIT_SUBGRUPO'] = $audit->subgrupo;
            $comparation[4] = "GIT_SUBGRUPO = :GIT_SUBGRUPO";
        }

        $store = (new Store())->find("code = :code","code=".$_POST['store'])->fetch();

        if (is_array($comparation)){
            $comparation = implode(' and ', $comparation);       
            $params = http_build_query($params);

            if ($store->group_id == 1 || $store->group_id == 10) {
                $items = (new AA3CITEM())->find($comparation,$params)->fetch(true);
            } elseif ($store->group_id == 2) {
                $items = (new AA3CITEM2())->find($comparation,$params)->fetch(true);
            } else {
                // $items = (new AA3CITEM3())->find($comparation,$params)->fetch(true);
            }
        }


        echo $this->view->render("operator/digitaEstoque", ["id" => "Apuração | " . SITE, "items" => $items, "id" => $id]);
        
    }

    public function saveInventory() {

        foreach ($_POST['code'] as $itens) {
            //`inventory_id`, `code`, `description`, `ean`, `amount`
            if ($_POST['amount'.$itens] != ''){
                
                $inventoryItems = new InventoryItems();
                $inventoryItems->inventory_id = $_POST['id'];
                $inventoryItems->code = $itens;
                $inventoryItems->amount = $_POST['amount'.$itens];
                $inventoryItemsId = $inventoryItems->save();
            }
            
        }
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
            foreach ($userStoreGroup as $userGroups) {
                $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
            }
            
            if (is_array($groups)){
                $groups = implode(", ",$groups);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                
            }
        $butcheryRegistration = (new ButcheryRegistration())->find()->fetch(true);
        $audit = (new Audit())->find()->fetch(true);
        if ($class == 'OG' || $class == 'OG2') {
            echo $this->view->render("operator/apuracao", ["id" => "Apuração | " . SITE,
            "lists" => $butcheryRegistration,
            "apuracoes" => $audit,
            "groups" => $groups
            ]);
        } else if ($class == 'A') {
            echo $this->view->render("administrator/apuracao", ["id" => "Apuração | " . SITE,
            "lists" => $butcheryRegistration,
            "apuracoes" => $audit
            ]);
        } else if ($class == 'C') {
            $store = (new Store())->find("code = :code","code=$store")->fetch();
            if ($store->group_id == 1 || $store->group_id == 10){
                $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :code","code=$store->code")->fetch();
                $aa2ctipo = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO;
            } else if ($store->group_id == 2) {
                $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = :code","code=$store->code")->fetch();
                $aa2ctipo = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO;
            } else if ($store->group_id == 8 || $store->group_id == 4) {
                // $aa2ctipo = (new AA2CTIPO3())->find("TIP_CODIGO||TIP_DIGITO = :code","code=$store->code")->fetch();
                // $aa2ctipo = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO."n";
                
            }
            echo $this->view->render("client/apuracao", ["id" => "Apuração | " . SITE,
            "lists" => $butcheryRegistration,
            "apuracoes" => $audit,
            "groups" => $groups,
            "store" => $aa2ctipo
            ]);
        }
        
    }

    public function getItensSemMovimentacao()
    {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        
        if ($class == 'C'){
            echo $this->view->render("client/semMovimentacao", ["id" => "Apuração | " . SITE, "store" => $store]);       
        } else if ($class == 'A'){
            echo $this->view->render("administrator/semMovimentacao", ["id" => "Apuração | " . SITE, "store" => $store]);
        } else if ($class == 'OG2' || $class == 'OG' ){
            $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
            foreach ($userStoreGroup as $userGroups) {
                $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
            }
            
            if (is_array($groups)){
                $groups = implode(", ",$groups);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                
            }
            
            
            echo $this->view->render("operator/semMovimentacao", ["id" => "Apuração | " . SITE, "store" => $store,"groups" => $groups]);
        }
        
    }

    public function getItensSemMovimentacaoSearch()
    {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        $data = $_POST['data1'];
        $data = "1".substr($data,2,2).substr($data,5,2).substr($data,8,2);

        if ($class == 'C') {
            
            $store = (new Store())->find("code = :code","code=$store")->fetch();
            if ($store->group_id == 1 || $store->group_id == 10) {
                $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :code","code=$store->code")->fetch();
                $store = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO;
                $aa2cestq = (new AA2CESTQ())->find("GET_COD_LOCAL = :gcl and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
                rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < :gduf) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
                rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < :gdue) and GET_ESTOQUE > :ge","gcl=$store&gduf=$data&gdue=$data&ge=0")->fetch(true);
            } else if ($store->group_id == 2){
                $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = :code","code=$store->code")->fetch();
                $store = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO;
                $aa2cestq = (new AA2CESTQ2())->find("GET_COD_LOCAL = :gcl and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
            rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < :gduf) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
            rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < :gdue) and GET_ESTOQUE > :ge","gcl=$store&gduf=$data&gdue=$data&ge=0")->fetch(true);
            } else {
                $store = $store->store_code;
                // $aa2cestq = (new AA2CESTQ3())->find("GET_COD_LOCAL = :gcl and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
                // rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < :gduf) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
                // rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < :gdue) and GET_ESTOQUE > :ge","gcl=$store&gduf=$data&gdue=$data&ge=0")->fetch(true);
            }
        } else {
            
            $store = $_POST['store'];
            
            $store = (new Store())->find("code = :code","code=$store")->fetch();

            if ($store->group_id == 1 || $store->group_id == 10) {
                $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :code","code=$store->code")->fetch();
                $store = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO;
                $aa2cestq = (new AA2CESTQ())->find("GET_COD_LOCAL = :gcl and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
                rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < :gduf) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
                rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < :gdue) and GET_ESTOQUE > :ge","gcl=$store&gduf=$data&gdue=$data&ge=0")->fetch(true);
            } else if ($store->group_id == 2){
                $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = :code","code=$store->code")->fetch();
                $store = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO;
                $aa2cestq = (new AA2CESTQ2())->find("GET_COD_LOCAL = :gcl and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
            rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < :gduf) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
            rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < :gdue) and GET_ESTOQUE > :ge","gcl=$store&gduf=$data&gdue=$data&ge=0")->fetch(true);
            } else {
                
                $store = $store->code;
                $aa2cestq = (new AA2CESTQ3())->find("GET_COD_LOCAL = :gcl and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
                rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < :gduf) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
                rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < :gdue) and GET_ESTOQUE > :ge","gcl=$store&gduf=$data&gdue=$data&ge=0")->fetch(true);
            }
        }
        
        if ($class == 'OG2' || $class == 'OG'){
            $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
            foreach ($userStoreGroup as $userGroups) {
                $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
            }
            
            if (is_array($groups)){
                $groups = implode(", ",$groups);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
            }
            echo $this->view->render("operator/semMovimentacao", ["id" => "Apuração | " . SITE,"groups" => $groups, "aa2cestq" => $aa2cestq,"data1" => $data,"store"=> $store]);        
        } else if ($class == 'A'){
            echo $this->view->render("administrator/semMovimentacao", ["id" => "Apuração | " . SITE,"aa2cestq" => $aa2cestq,"data1" => $data,"store"=> $store]);
        } else if ($class == 'C'){
            echo $this->view->render("client/semMovimentacao", ["id" => "Apuração | " . SITE,"aa2cestq" => $aa2cestq,"data1" => $data,"store"=> $store]);
        }
       
    }

    public function duedateAudit() {

        echo $this->view->render("administrator/dueDateAudit", ["id" => "Auditoria de vencimentos | " . SITE
        ]);
    }

    public function dueDateAuditSearch() {
        $initialDate = $_POST['initialDate'];
        $finalDate = $_POST['finalDate'];
        $store = $_POST['store'];
        $nota = $_POST['nota'];
        $divergente = $_POST['divergente'];

        if ($initialDate != '') {
            $params['alteracaoInicial'] = '1' . substr($initialDate, 2, 2) . substr($initialDate, 5, 2) . substr($initialDate, 8, 2);
            $comparation[6] = "FIS_DTA_AGENDA >= :alteracaoInicial";
        }
        if ($finalDate != '') {
            $params['alteracaoFinal'] = '1' . substr($finalDate, 2, 2) . substr($finalDate, 5, 2) . substr($finalDate, 8, 2);
            $comparation[7] = "FIS_DTA_AGENDA <= :alteracaoFinal";
        }
        
        if ($store != '') {
            $params['store_id'] = $store;
            $comparation[9] = "FIS_LOJ_DST = :store_id";
        }

        if ($nota != '') {
            $params['nota'] = $nota;
            $comparation[8] = "FIS_NRO_NOTA = :nota";
        }
        
        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);
        $aa1cfisc = (new AA1CFISC())->find($comparation." and (FIS_OPER = :f1 or FIS_OPER = :f3 or FIS_OPER = :f10 or FIS_OPER = :f11)",$params."&f1=1&f3=3&f10=10&f11=11")->fetch(true);
        $aa1cfisc2 = (new AA1CFISC2())->find($comparation." and (FIS_OPER = :f1 or FIS_OPER = :f3 or FIS_OPER = :f10 or FIS_OPER = :f11)",$params."&f1=1&f3=3&f10=10&f11=11")->fetch(true);
        
        echo $this->view->render("administrator/dueDateAudit", ["id" => "Auditoria de vencimentos | " . SITE,
        "aa1cfisc" => $aa1cfisc,
        "aa1cfisc2" => $aa1cfisc2,
        "divergente" => $divergente
        ]);

    }

    public function getMargens() {

        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
    
        if ($class == 'C') {
            echo $this->view->render("client/marginAudit", ["id" => "Auditoria de margem | " . SITE
            ]);
        } else if ($class == 'A') {
            $users = (new User())->find("class = :class","class=A")->fetch(true);
            echo $this->view->render("administrator/marginAudit", ["id" => "Auditoria de margem | " . SITE,"users" => $users
            ]);
        } else if ($class == 'OG' || $class == 'OG2') {
            $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
            foreach ($userStoreGroup as $userGroups) {
                $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
            }
            
            if (is_array($groups)){
                $groups = implode(", ",$groups);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
            }
            echo $this->view->render("operator/marginAudit", ["id" => "Auditoria de margem | " . SITE,"groups" => $groups
            ]);
        }
        
    }

    public function getMargensSearch() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $storeId = $usuario->store_code;

        $store = $_POST['store'];
        $initialDate = $_POST['initialDate'];
        $finalDate = $_POST['finalDate'];
        $nota = $_POST['nota'];
        $menor = $_POST['menor'];
        $maior = $_POST['maior'];
        $operador = $_POST['operador'];
        
        if($menor == '') {
            $menor = 0;
        }
        if($maior == '') {
            $maior = 0;
        }

        if ($class == 'A') {
            if ($initialDate != '') {
                $params['alteracaoInicial'] = '1' . substr($initialDate, 2, 2) . substr($initialDate, 5, 2) . substr($initialDate, 8, 2);
                $comparation[6] = "ESCHC_DATA3 >= :alteracaoInicial";   
            }
    
            if ($finalDate != '') {
                $params['alteracaoFinal'] = '1' . substr($finalDate, 2, 2) . substr($finalDate, 5, 2) . substr($finalDate, 8, 2);
                $comparation[7] = "ESCHC_DATA3 <= :alteracaoFinal";
               
            }             
    
            if ($nota != '') {
                $params['nota'] = $nota;
                $comparation[8] = "ESCHC_NRO_NOTA = :nota";
            }

            if ($store == '') {
                $comparation = implode(' and ', $comparation);
                $params = http_build_query($params);
                $groups = (new Store())->find()->fetch(true);

                foreach($groups as $group) {
                    if ($group->group_id == 1 || $group->group_id == 10) {
                        $stores1[$group->code] = $group->code;

                    } else if ($group->group_id == 2) {
                        $stores2[$group->code] = $group->code;

                    } else if ($group->group_id == 8 || $group->group_id == 4){
                        $stores3[$group->code] = substr($group->code,0,strlen($group->code)-1);

                    }
                }

                if (is_array($stores1)){
                    foreach($stores1 as $s1) {
                        $comparation1[$s1] = "ESCLC_CODIGO = :store$s1";
                        $params1["store$s1"] = $s1;
                    }
                    $params1 = http_build_query($params1);
                    $comparation1 = implode(" or ",$comparation1);
                    
                    
                    $ag1iensa = (new AG1IENSA())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11) and ($comparation1)"
                    ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9&$params1")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                } else if (is_array($stores2)){
                    foreach($stores2 as $s2) {
                        $comparation2[$s2] = "ESCLC_CODIGO = :store$s2";
                        $params2["store$s2"] = $s2;
                    }
                    $params2 = http_build_query($params2);
                    $comparation2 = implode(" or ",$comparation2);
                    
                    
                    $ag1iensa2 = (new AG1IENSA2())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11) and ($comparation2)"
                    ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9&$params2")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                }else if (is_array($stores3)){
                    foreach($stores3 as $s3) {
                        $comparation3[$s3] = "ESCLC_CODIGO = :store$s3";
                        $params3["store$s3"] = $s3;
                    }
                    $params3 = http_build_query($params3);
                    $comparation3 = implode(" or ",$comparation3);
                }
            } else {
                $storeGroup = (new Store())->find("code = :code","code=$store")->fetch();
                if ($storeGroup->group_id == 1 || $storeGroup->group_id == 10 ) {
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCLC_CODIGO = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                    $ag1iensa = (new AG1IENSA())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11)"
                    ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                } else if ($storeGroup->group_id == 2) {
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCLC_CODIGO = :store_id";

                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);

                    $ag1iensa2 = (new AG1IENSA2())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11)"
                    ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                    

                } else if ($storeGroup->group_id == 8 || $storeGroup->group_id == 4) {
                    $store = substr($store,0,strlen($store)-1);
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCLC_CODIGO = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                }
            }
        } else {
            if($class == 'C') {
                $storeGroup = (new Store())->find("code = :code","code=$storeId")->fetch();
                if ($initialDate != '') {
                    $params['alteracaoInicial'] = '1' . substr($initialDate, 2, 2) . substr($initialDate, 5, 2) . substr($initialDate, 8, 2);
                    $comparation[6] = "ESCHC_DATA3 >= :alteracaoInicial";
                    
                }
        
                if ($finalDate != '') {
                    $params['alteracaoFinal'] = '1' . substr($finalDate, 2, 2) . substr($finalDate, 5, 2) . substr($finalDate, 8, 2);
                    $comparation[7] = "ESCHC_DATA3 <= :alteracaoFinal";
                   
                }             
        
                if ($nota != '') {
                    $params['nota'] = $nota;
                    $comparation[8] = "ESCHC_NRO_NOTA = :nota";
                    
                }
                //checar a loja amarrada 
                if ($storeGroup->group_id == 1 || $storeGroup->group_id == 10 ) {
                    $store = $storeId;
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCLC_CODIGO = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                    $ag1iensa = (new AG1IENSA())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11)"
                    ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                } else if ($storeGroup->group_id == 2) {
                    $store = $storeId;
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCLC_CODIGO = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                    $ag1iensa2 = (new AG1IENSA2())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11)"
                    ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                } else if ($storeGroup->group_id == 8 || $storeGroup->group_id == 4) {
                    $store = substr($storeId,0,strlen($storeId)-1);
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCLC_CODIGO = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                }         
            } else if ($class == 'OG' || $class == 'OG2') {

                if ($initialDate != '') {
                    $params['alteracaoInicial'] = '1' . substr($initialDate, 2, 2) . substr($initialDate, 5, 2) . substr($initialDate, 8, 2);
                    $comparation[6] = "ESCHC_DATA3 >= :alteracaoInicial";
                    
                }
        
                if ($finalDate != '') {
                    $params['alteracaoFinal'] = '1' . substr($finalDate, 2, 2) . substr($finalDate, 5, 2) . substr($finalDate, 8, 2);
                    $comparation[7] = "ESCHC_DATA3 <= :alteracaoFinal";
                   
                }             
        
                if ($nota != '') {
                    $params['nota'] = $nota;
                    $comparation[8] = "ESCHC_NRO_NOTA = :nota";
                    
                }
                
                if ($store == '') {
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                    $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
                    foreach ($userStoreGroup as $userGroups) {
                        $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
                    }
                    if (is_array($groups)){
                        $groups = implode(", ",$groups);
                        $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                    }

                    foreach($groups as $group) {
                        if ($group->group_id == 1 || $group->group_id == 10) {
                            $stores1[$group->code] = substr($group->code,0,strlen($group->code)-1);

                        } else if ($group->group_id == 2) {
                            $stores2[$group->code] = substr($group->code,0,strlen($group->code)-1);

                        } else if ($group->group_id == 8 || $group->group_id == 4){
                            $stores3[$group->code] = substr($group->code,0,strlen($group->code)-1);

                        }
                    }

                    if (is_array($stores1)){
                        foreach($stores1 as $s1) {
                            $comparation1[$s1] = "ESCLC_CODIGO = :store$s1";
                            $params1["store$s1"] = $s1;
                        }
                        $params1 = http_build_query($params1);
                        $comparation1 = implode(" or ",$comparation1);
                        
                        
                        $ag1iensa = (new AG1IENSA())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11) and ($comparation1)"
                        ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9&$params1")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                    } else if (is_array($stores2)){
                        foreach($stores2 as $s2) {
                            $comparation2[$s2] = "ESCLC_CODIGO = :store$s2";
                            $params2["store$s2"] = $s2;
                        }
                        $params2 = http_build_query($params2);
                        $comparation2 = implode(" or ",$comparation2);
                    }else if (is_array($stores3)){
                        foreach($stores3 as $s3) {
                            $comparation3[$s3] = "ESCLC_CODIGO = :store$s3";
                            $params3["store$s3"] = $s3;
                        }
                        $params3 = http_build_query($params3);
                        $comparation3 = implode(" or ",$comparation3);
                    }
                } else {
                    $storeGroup = (new Store())->find("code = :code","code=$store")->fetch();
                    if ($storeGroup->group_id == 1 || $storeGroup->group_id == 10 ) {
                        $params['store_id'] = $store;
                        $comparation[9] = "ESCLC_CODIGO = :store_id";
                        $comparation = implode(' and ', $comparation);
                        $params = http_build_query($params);
                        $ag1iensa = (new AG1IENSA())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11)"
                        ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                    } else if ($storeGroup->group_id == 2) {
                        $params['store_id'] = $store;
                        $comparation[9] = "ESCLC_CODIGO = :store_id";
                        $comparation = implode(' and ', $comparation);
                        $params = http_build_query($params);
                        $ag1iensa2 = (new AG1IENSA2())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag3 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11)"
                        ,$params."&ag1=1&ag3=3&ag10=10&ag11=11&ag9=9")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                    } else if ($storeGroup->group_id == 8 || $storeGroup->group_id == 4) {
                        $store = substr($store,0,strlen($store)-1);
                        $params['store_id'] = $store;
                        $comparation[9] = "ESCLC_CODIGO = :store_id";
                        $comparation = implode(' and ', $comparation);
                        $params = http_build_query($params);
                    }
                }                
            }
        }

        if($class == 'C') {
            echo $this->view->render("client/marginAudit", ["id" => "Auditoria de margem | " . SITE,
            "ag1iensa" => $ag1iensa,
            "ag1iensa2" => $ag1iensa2,
            "ag1iensa3" => $ag1iensa3,
            "maior" => $maior,
            "menor" => $menor,
            "store" => $store
            ]);
        } else if($class == 'OG' || $class == 'OG2') {
            $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);

            foreach ($userStoreGroup as $userGroups) {

                $groups1[$userGroups->store_group_id] = $userGroups->store_group_id;
                
            }

            
            if (is_array($groups1)){
                
                $groups = implode(", ",$groups1);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
            }
            echo $this->view->render("operator/marginAudit", ["id" => "Auditoria de margem | " . SITE,
            "ag1iensa" => $ag1iensa,
            "ag1iensa2" => $ag1iensa2,
            "ag1iensa3" => $ag1iensa3,
            "maior" => $maior,
            "menor" => $menor,
            "store" => $store,
            "groups" => $groups
            ]);
        } else if($class == 'A') {
            $users = (new User())->find("class = :class","class=A")->fetch(true);
     
            echo $this->view->render("administrator/marginAudit", ["id" => "Auditoria de margem | " . SITE,
            "ag1iensa" => $ag1iensa,
            "ag1iensa2" => $ag1iensa2,
            "ag1iensa3" => $ag1iensa3,
            "maior" => $maior,
            "menor" => $menor,
            "users" => $users,
            "operador" => $operador,
            "store" => $store
            ]);
        }
        
    }

    public function getMargens2() {

        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        
        if ($class == 'C') {
            echo $this->view->render("client/marginAudit2", ["id" => "Auditoria de margem | " . SITE
            ]);
        } else if ($class == 'A') {
            $users = (new User())->find("class = :class","class=A")->fetch(true);
            echo $this->view->render("administrator/marginAudit2", ["id" => "Auditoria de margem | " . SITE,"users" => $users
            ]);
        } else if ($class == 'OG' || $class == 'OG2') {
            $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
            foreach ($userStoreGroup as $userGroups) {
                $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
            }
            
            if (is_array($groups)){
                $groups = implode(", ",$groups);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
            }
            echo $this->view->render("operator/marginAudit2", ["id" => "Auditoria de margem | " . SITE,"groups" => $groups
            ]);
        }
        
    }

    public function marginValidate() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;

        if ($class == 'A') {
            if ($usuario->id == 150 || $usuario->id == 152 || $usuario->id == 153 || $usuario->id == 148 || $usuario->id == 180 || $usuario->id == 1) {
                $marginVerification = (new MarginVerification())->find("approved = :approved","approved=N")->fetch(true);
                $user = "ok";
            } else {
                $marginVerification = (new MarginVerification())->find("approved = :approved","approved=N")->fetch(true);
                $user = $usuario->id;
            }
            
            echo $this->view->render("administrator/marginValidade", ["id" => "Validação de margem | " . SITE,"user" => $user,"marginVerification" => $marginVerification
            ]);

        } else {
            $this->router->redirect("/login");
        }
    }

    public function marginValidateUpdate() {
        $marginVerification = (new MarginVerification())->findById($_POST['id']);
        switch($_POST["approved"]){
            case "N":
                $marginVerification->approved = $_POST["approved"];
                $marginVerificationId = $marginVerification->save();
                break;
            case "S":
                $marginVerification->approved = $_POST["approved"];
                $marginVerificationId = $marginVerification->save();
                break;
            case "C":
                $marginVerification->approved = $_POST["approved"];
                $marginVerificationId = $marginVerification->save();
                break;
        }
        if ($marginVerificationId) {
            $data["save"] = "ok";
            echo json_encode($data);
            return;
        } else {
            $data["save"] = "nok";
            echo json_encode($data);
            return;
        }
    }
        
    public function getMargensSearch2() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $storeId = $usuario->store_code;
        
        $store = $_POST['store'];
        
        $initialDate = $_POST['initialDate'];
        $finalDate = $_POST['finalDate'];
        $nota = $_POST['nota'];
        $menor = $_POST['menor'];
        $maior = $_POST['maior'];
        $operador = $_POST['operador'];
        
        if($menor == '') {
            $menor = 0;
        }
        if($maior == '') {
            $maior = 0;
        }
        
        if ($class == 'A') {         
            if ($initialDate != '') {
                $params['alteracaoInicial'] = '1' . substr($initialDate, 2, 2) . substr($initialDate, 5, 2) . substr($initialDate, 8, 2);
                $comparation[6] = "ESCHC_DATA3 >= :alteracaoInicial";
                
            }
        
            if ($finalDate != '') {
                $params['alteracaoFinal'] = '1' . substr($finalDate, 2, 2) . substr($finalDate, 5, 2) . substr($finalDate, 8, 2);
                $comparation[7] = "ESCHC_DATA3 <= :alteracaoFinal";
               
            }             
        
            if ($nota != '') {
                $params['nota'] = $nota;
                $comparation[8] = "ESCHC_NRO_NOTA = :nota";
            }
        
            if ($store == '') {
                
                $comparation = implode(' and ', $comparation);
                $params = http_build_query($params);
                $groups = (new Store())->find()->fetch(true);
        
                foreach($groups as $group) {
                    if ($group->group_id == 1 || $group->group_id == 10) {
                        $stores1[$group->code] = $group->code;
        
                    } else if ($group->group_id == 2) {
                        $stores2[$group->code] = $group->code;
        
                    } else if ($group->group_id == 8 || $group->group_id == 4){
                        $stores3[$group->code] = substr($group->code,0,strlen($group->code)-1);
        
                    }
                }
        
                if (is_array($stores1)){
                    foreach($stores1 as $s1) {
                        $comparation1[$s1] = "ESCHLJC_CODIGO3 = :store$s1";
                        $params1["store$s1"] = $s1;
                    }
                    $params1 = http_build_query($params1);
                    $comparation1 = implode(" or ",$comparation1);
                    
                    
                    $ag1iensa = (new AG1IENSA())->find($comparation." and ESCHC_AGENDA3 = :ag601 and ($comparation1)"
                    ,$params."&ag601=601&$params1")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                    
                } else if (is_array($stores2)){
                    foreach($stores2 as $s2) {
                        $comparation2[$s2] = "ESCHLJC_CODIGO3 = :store$s2";
                        $params2["store$s2"] = $s2;
                    }
                    $params2 = http_build_query($params2);
                    $comparation2 = implode(" or ",$comparation2);
                    
                    
                    $ag1iensa2 = (new AG1IENSA2())->find($comparation." and ESCHC_AGENDA3 = :ag601 and ($comparation2)"
                    ,$params."&ag601=601&$params2")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                }else if (is_array($stores3)){
                    foreach($stores3 as $s3) {
                        $comparation3[$s3] = "ESCHLJC_CODIGO3 = :store$s3";
                        $params3["store$s3"] = $s3;
                    }
                    $params3 = http_build_query($params3);
                    $comparation3 = implode(" or ",$comparation3);
                }
            } else {
                $storeGroup = (new Store())->find("code = :code","code=$store")->fetch();
                if ($storeGroup->group_id == 1 || $storeGroup->group_id == 10 ) {
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                    $ag1iensa = (new AG1IENSA())->find($comparation." and ESCHC_AGENDA3 = :ag601",$params."&ag601=601")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);


                } else if ($storeGroup->group_id == 2) {
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
        
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
        
                    $ag1iensa2 = (new AG1IENSA2())->find($comparation." and ESCHC_AGENDA3 = :ag601"
                    ,$params."&ag601=601")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                    
        
                } else if ($storeGroup->group_id == 8 || $storeGroup->group_id == 4) {
                    $store = substr($store,0,strlen($store)-1);
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                }
            }
        } else {
            if($class == 'C') {
                $storeGroup = (new Store())->find("code = :code","code=$storeId")->fetch();
                if ($initialDate != '') {
                    $params['alteracaoInicial'] = '1' . substr($initialDate, 2, 2) . substr($initialDate, 5, 2) . substr($initialDate, 8, 2);
                    $comparation[6] = "ESCHC_DATA3 >= :alteracaoInicial";
                    
                }
        
                if ($finalDate != '') {
                    $params['alteracaoFinal'] = '1' . substr($finalDate, 2, 2) . substr($finalDate, 5, 2) . substr($finalDate, 8, 2);
                    $comparation[7] = "ESCHC_DATA3 <= :alteracaoFinal";
                   
                }             
        
                if ($nota != '') {
                    $params['nota'] = $nota;
                    $comparation[8] = "ESCHC_NRO_NOTA = :nota";
                    
                }
                //checar a loja amarrada 
                if ($storeGroup->group_id == 1 || $storeGroup->group_id == 10 ) {
                    $store = $storeId;
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                    $ag1iensa = (new AG1IENSA())->find($comparation." and  ESCHC_AGENDA3 = :ag601"
                    ,$params."&ag601=601")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                } else if ($storeGroup->group_id == 2) {
                    $store = $storeId;
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                    $ag1iensa2 = (new AG1IENSA2())->find($comparation." and   ESCHC_AGENDA3 = :ag601"
                    ,$params."&ag601=601")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                } else if ($storeGroup->group_id == 8 || $storeGroup->group_id == 4) {
                    $store = substr($storeId,0,strlen($storeId)-1);
                    $params['store_id'] = $store;
                    $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                }         
            } else if ($class == 'OG' || $class == 'OG2') {
        
                if ($initialDate != '') {
                    $params['alteracaoInicial'] = '1' . substr($initialDate, 2, 2) . substr($initialDate, 5, 2) . substr($initialDate, 8, 2);
                    $comparation[6] = "ESCHC_DATA3 >= :alteracaoInicial";
                    
                }
        
                if ($finalDate != '') {
                    $params['alteracaoFinal'] = '1' . substr($finalDate, 2, 2) . substr($finalDate, 5, 2) . substr($finalDate, 8, 2);
                    $comparation[7] = "ESCHC_DATA3 <= :alteracaoFinal";
                   
                }             
        
                if ($nota != '') {
                    $params['nota'] = $nota;
                    $comparation[8] = "ESCHC_NRO_NOTA = :nota";
                    
                }
                
                if ($store == '') {
                    $comparation = implode(' and ', $comparation);
                    $params = http_build_query($params);
                    $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
                    foreach ($userStoreGroup as $userGroups) {
                        $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
                    }
                    if (is_array($groups)){
                        $groups = implode(", ",$groups);
                        $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                    }
        
                    foreach($groups as $group) {
                        if ($group->group_id == 1 || $group->group_id == 10) {
                            $stores1[$group->code] = substr($group->code,0,strlen($group->code)-1);
        
                        } else if ($group->group_id == 2) {
                            $stores2[$group->code] = substr($group->code,0,strlen($group->code)-1);
        
                        } else if ($group->group_id == 8 || $group->group_id == 4){
                            $stores3[$group->code] = substr($group->code,0,strlen($group->code)-1);
        
                        }
                    }
        
                    if (is_array($stores1)){
                        foreach($stores1 as $s1) {
                            $comparation1[$s1] = "ESCHLJC_CODIGO3 = :store$s1";
                            $params1["store$s1"] = $s1;
                        }
                        $params1 = http_build_query($params1);
                        $comparation1 = implode(" or ",$comparation1);
                        
                        
                        $ag1iensa = (new AG1IENSA())->find($comparation." and ESCHC_AGENDA3 = :ag601 and ($comparation1)"
                        ,$params."&ag601=601&$params1")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                    } else if (is_array($stores2)){
                        foreach($stores2 as $s2) {
                            $comparation2[$s2] = "ESCHLJC_CODIGO3 = :store$s2";
                            $params2["store$s2"] = $s2;
                        }
                        $params2 = http_build_query($params2);
                        $comparation2 = implode(" or ",$comparation2);
                    }else if (is_array($stores3)){
                        foreach($stores3 as $s3) {
                            $comparation3[$s3] = "ESCHLJC_CODIGO3 = :store$s3";
                            $params3["store$s3"] = $s3;
                        }
                        $params3 = http_build_query($params3);
                        $comparation3 = implode(" or ",$comparation3);
                        
                        
                        $ag1iensa3 = (new AG1IENSA3())->find($comparation." and ESCHC_AGENDA3 = :ag601 and ($comparation3)"
                        ,$params."&ag601=601&$params3")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                    }
                } else {
                    $storeGroup = (new Store())->find("code = :code","code=$store")->fetch();
                    if ($storeGroup->group_id == 1 || $storeGroup->group_id == 10 ) {
                        $params['store_id'] = $store;
                        $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
                        $comparation = implode(' and ', $comparation);
                        $params = http_build_query($params);
                        $ag1iensa = (new AG1IENSA())->find($comparation." and ESCHC_AGENDA3 = :ag601"
                        ,$params."&ag601=601")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                    } else if ($storeGroup->group_id == 2) {
                        $params['store_id'] = $store;
                        $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
                        $comparation = implode(' and ', $comparation);
                        $params = http_build_query($params);
                        $ag1iensa2 = (new AG1IENSA2())->find($comparation." and ESCHC_AGENDA3 = :ag601"
                        ,$params."&ag601=601")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
                    } else if ($storeGroup->group_id == 8 || $storeGroup->group_id == 4) {
                        $store = substr($store,0,strlen($store)-1);
                        $params['store_id'] = $store;
                        $comparation[9] = "ESCHLJC_CODIGO3 = :store_id";
                        $comparation = implode(' and ', $comparation);
                        $params = http_build_query($params);
                    }
                }                
            }
        }
        
        if($class == 'C') {
            echo $this->view->render("client/marginAudit2", ["id" => "Auditoria de margem | " . SITE,
            "ag1iensa" => $ag1iensa,
            "ag1iensa2" => $ag1iensa2,
            "ag1iensa3" => $ag1iensa3,
            "maior" => $maior,
            "menor" => $menor,
            "store" => $store
            ]);
        } else if($class == 'OG' || $class == 'OG2') {
            $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
        
            foreach ($userStoreGroup as $userGroups) {
        
                $groups1[$userGroups->store_group_id] = $userGroups->store_group_id;
                
            }
        
            
            if (is_array($groups1)){
                
                $groups = implode(", ",$groups1);
                $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
            }
            echo $this->view->render("operator/marginAudit2", ["id" => "Auditoria de margem | " . SITE,
            "ag1iensa" => $ag1iensa,
            "ag1iensa2" => $ag1iensa2,
            "ag1iensa3" => $ag1iensa3,
            "maior" => $maior,
            "menor" => $menor,
            "store" => $store,
            "groups" => $groups
            ]);
        } else if($class == 'A') {
            $users = (new User())->find("class = :class","class=A")->fetch(true);
        
            echo $this->view->render("administrator/marginAudit2", ["id" => "Auditoria de margem | " . SITE,
            "ag1iensa" => $ag1iensa,
            "ag1iensa2" => $ag1iensa2,
            "ag1iensa3" => $ag1iensa3,
            "maior" => $maior,
            "menor" => $menor,
            "users" => $users,
            "operador" => $operador,
            "store" => $store
            ]);
        }
        
    }

    public function getPostedOrNot() {
        echo $this->view->render("administrator/bookkeepingAudit", ["id" => "Auditoria de Postagens | " . SITE
        ]);
    }

    public function getPostedOrNotSearch() {
        $initialDate = $_POST['initialDate'];
        $finalDate = $_POST['finalDate'];
        $store = $_POST['store'];
        $nota = $_POST['nota'];

        if ($initialDate != '') {
            $params['alteracaoInicial'] = '1' . substr($initialDate, 2, 2) . substr($initialDate, 5, 2) . substr($initialDate, 8, 2);
            $comparation[6] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) >= :alteracaoInicial";
        }
        if ($finalDate != '') {
            $params['alteracaoFinal'] = '1' . substr($finalDate, 2, 2) . substr($finalDate, 5, 2) . substr($finalDate, 8, 2);
            $comparation[7] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) <= :alteracaoFinal";
        }
        
        if ($store != '') {
            $params['store_id'] = $store;
            $comparation[9] = "store_id = :store_id";
        }

        if ($nota != '') {
            $params['nota'] = $nota;
            $comparation[8] = "nf_number = :nota";
        }

        $params['status_id'] = 3;
        $comparation[8] = "status_id = :status_id";

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);

        $nfReceipt = (new NfReceipt())->find($comparation,$params)->fetch(true);
        echo $this->view->render("administrator/bookkeepingAudit", ["id" => "Auditoria de Postagens | " . SITE,
        "nfReceipt" => $nfReceipt
        ]);
    }

    public function webServiceVendasNaoImportadas() {
        $params['alteracaoInicial'] = '1'.date('ymd');
        $comparation[6] = "ESCHC_DATA3 = :alteracaoInicial";

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);
        

        $stores1 = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
        $stores2 = array(83, 117, 119, 120, 91, 20, 21, 84, 86, 95, 102, 23, 93, 101, 18, 49, 94, 10, 108, 110, 111, 115, 122, 150, 151, 152, 153, 154, 155);
        $stores7 = array(1, 2, 3, 4, 5, 6, 7);

        //MB
        if (is_array($stores1)){
            foreach($stores1 as $s1) {
                $comparation1[$s1] = "ESCHLJC_CODIGO3 = :store$s1";
                $params1["store$s1"] = $s1;
            }
            $params1 = http_build_query($params1);
            $comparation1 = implode(" or ",$comparation1);
            $ag1iensa = (new AG1IENSA())->find($comparation." and ESCHC_SECAO3 ! = :ESCHC_SECAO3 and ESCHC_AGENDA3 = :ag601 and ($comparation1)"
            ,$params."&ag601=601&ESCHC_SECAO3=6&$params1","ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3")->group("ESCHLJC_CODIGO3")->fetch(true);

        }
        //BD
        if (is_array($stores2)){
            foreach($stores2 as $s2) {
                $comparation2[$s2] = "ESCHLJC_CODIGO3 = :store$s2";
                $params2["store$s2"] = $s2;
            }
            $params2 = http_build_query($params2);
            $comparation2 = implode(" or ",$comparation2); 
            $ag1iensa2 = (new AG1IENSA2())->find($comparation." and ESCHC_SECAO3 ! = :ESCHC_SECAO3 and ESCHC_AGENDA3 = :ag601 and ($comparation2)"
                         ,$params."&ag601=601&ESCHC_SECAO3=6&$params2","ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3")->group("ESCHLJC_CODIGO3")->fetch(true);
        }

        if (is_array($stores7)){
            foreach($stores7 as $s7) {
                $comparation7[$s7] = "ESCHLJC_CODIGO3 = :store$s7";
                $params7["store$s7"] = $s7;
            }
            $params7 = http_build_query($params7);
            $comparation7 = implode(" or ",$comparation7);
            $ag1iensa7 = (new AG1IENSA7())->find($comparation." and ESCHC_SECAO3 ! = :ESCHC_SECAO3 and ESCHC_AGENDA3 = :ag601 and ($comparation7)"
            ,$params."&ag601=601&ESCHC_SECAO3=6&$params7","ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3")->group("ESCHLJC_CODIGO3")->fetch(true);
        }

        foreach ($ag1iensa as $iensa){
            $key = array_search($iensa->ESCHLJC_CODIGO3,$stores1);
            if ($key!==false){
                unset($stores1[$key]);
            }         
        }

        foreach($stores1 as $notStore) {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = "-598428015";
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            $logger->pushHandler($tele_handler);
            $logger->info("Venda não importada loja $notStore, referente ao dia ".date('d-m-Y')."."); 
            
        }

        foreach ($ag1iensa2 as $iensa2){
            $key = array_search($iensa2->ESCHLJC_CODIGO3,$stores2);
            if ($key!==false){
                unset($stores2[$key]);
            }           
        }

        foreach($stores2 as $notStore2) {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = "-598428015";
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            $logger->pushHandler($tele_handler);
            $logger->info("Venda não importada loja $notStore2, referente ao dia ".date('d-m-Y')."."); 
        }

        foreach ($ag1iensa7 as $iensa7){
            $key = array_search($iensa7->ESCHLJC_CODIGO3,$stores7);
            if ($key!==false){
                unset($stores7[$key]);
            }          
        }

        foreach($stores7 as $notStore) {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = "-598428015";
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            $logger->pushHandler($tele_handler);
            $logger->info("Venda não importada loja $notStore, referente ao dia ".date('d-m-Y')."."); 
            
        }
        
        $notificationsAudit = (new NotificationsAudit())->findById(3);
         $notificationsAudit->last_run = date("Y-m-d H:i:s");
         $notificationsAuditId = $notificationsAudit->save();
    }

    public function webServiceDuoDate() {
        $date = date("2021-10-13");
        $params['create'] = '1' . substr($date, 2, 2) . substr($date, 5, 2) . substr($date, 8, 2);
        $comparation[1] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) = :create";
        $params['update'] = '1' . substr($date, 2, 2) . substr($date, 5, 2) . substr($date, 8, 2);
        $comparation[2] = "concat('1',substr(`updated_at`,3,2),substr(`updated_at`,6,2),substr(`updated_at`,9,2)) = :update";

        $comparation = "(".implode(' or ', $comparation).")"; 
        $params = http_build_query($params);

        $nfReceipt = (new NfReceipt())->find("status_id = :sid and ".$comparation,$params."&sid=3")->fetch(true);

        foreach($nfReceipt as $nf) {
            
            if ($nf->updated) {
                $i = 1;
                $j = 1;

                foreach ($nf->getDueDate as $dueDate) {
                    $portalDueDate[$i] = "1".substr($dueDate->due_date,2,2).substr($dueDate->due_date,5,2).substr($dueDate->due_date,8,2);
                    $i++;
                }
                

                foreach ($nf->updated as $aa1cfisc) {
                    foreach ($aa1cfisc->getDueDate as $dd) {
                        $rmsDueDate[$j] = $dd->FIV_DTA_VENCTO;
                        $j++;
                    }
                }

                
                if(count($portalDueDate) == count($rmsDueDate)) {

                    for ($r=0;$r<$i;$r++) {

                        if ($portalDueDate[$r] != $rmsDueDate[$r]){
                            $error = true;
                            
                        }
                    }
                    
                    if ($error) {
                        $msg = "Loja2  $nf->store_id \n Nota  $nf->nf_number \n Chave de acesso $nf->access_key \n Operador: $nf->getOperator vencimentos portal: ".implode(", ",$portalDueDate)." 
                        s retaguarda: ".implode(", ",$rmsDueDate);
                        // $logger = new Logger("web");
                        // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        // $tele_channel = "-696154966";
                        // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        // $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        // $logger->pushHandler($tele_handler);
                        // $logger->info($msg);

                    }
                } else {
                    $msg = "Loja3  $nf->store_id \n Nota  $nf->nf_number \n Chave de acesso $nf->access_key \n Operador: $nf->getOperator vencimentos portal: ".implode(", ",$portalDueDate)." vencimentos retaguarda: ".implode(", ",$rmsDueDate);

                    // $logger = new Logger("web");
                    // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    // $tele_channel = "-696154966";
                    // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    // $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    // $logger->pushHandler($tele_handler);
                    // $logger->info($msg);
                }
            }
        }
    }

    public function webServicePostedNotUpdated() {
       
        $date = date("Y-m-d");
        $params['create'] = '1' . substr($date, 2, 2) . substr($date, 5, 2) . substr($date, 8, 2);
        $comparation[1] = "concat('1',substr(`created_at`,3,2),substr(`created_at`,6,2),substr(`created_at`,9,2)) = :create";

        $params['update'] = '1' . substr($date, 2, 2) . substr($date, 5, 2) . substr($date, 8, 2);
        $comparation[2] = "concat('1',substr(`updated_at`,3,2),substr(`updated_at`,6,2),substr(`updated_at`,9,2)) = :update";

        $comparation = "(".implode(' or ', $comparation).")";
        $params = http_build_query($params);

        $nfReceipt = (new NfReceipt())->find("store_id != :loja33 and store_id != :loja2 and store_id != :loja1003 and store_id != :loja1001 and store_id != :loja1002 and store_id != :loja270 and (status_id = :sid or status_id = :sid2 or status_id = :sid3) and ".$comparation,$params."&sid3=15&sid2=14&sid=3&loja1001=1001&loja1002=1002&loja2=2&loja33=33&loja1003=1003&loja270=270")->fetch(true);

        foreach($nfReceipt as $nf) {
            

            $postedVerification2 = (new PostedVerification())->find("nf_receipt_id = :nri","nri=$nf->id")->count();
            if ($postedVerification2 == 0) {
                $postedVerification = new PostedVerification();
                $postedVerification->nf_receipt_id = $nf->id;
                $postedVerificationId = $postedVerification->save();

                if (!$nf->updated){
                    
                    $msg1 = "Loja  $nf->store_id \n Nota  $nf->nf_number \n Chave de acesso $nf->access_key \n Operador: $nf->getOperator";

                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-696154966";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info($msg1);
                    if ($nf->group_id == 8) {
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "-619224898";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info($msg1);
                    }

                    if ($nf->getOperatorClass()->telegram) {
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel =$nf->getOperatorClass()->telegram;
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info($msg1);
                    }
                }
            }
        }
    }

    public function importaEstoque() {
        $upload = new File("storage","files");
        $files = $_FILES;
        if(!empty($files["files"])) {
            $file= $files["files"];

            if(empty($file["type"] || !in_array($file["type"],$upload::isAllowed()))) {

            } else {
                $uploaded = $upload->upload($file,pathinfo($file["name"],PATHINFO_FILENAME));

                $stream = fopen("C:/xampp/htdocs/bdtecsolucoes.com.br/".$uploaded,"r");
                
                $csv = Reader::createFromStream($stream);
                
                $csv->setDelimiter(";");
                $csv->setHeaderOffset(null);
                $stmt = (new Statement());
                $items = $stmt->process($csv);
                
                if ($items) {
                    $inventory = new Inventories();
                    $inventory->store_id = $_POST['store'];
                    $inventory->title = "Inventario loja ". $_POST['store'] . " dia " . $_POST['date'] . "(importado)";
                    $inventory->date_inventory = $_POST['date'];
                    $inventoryId = $inventory->save();

                    if ($inventoryId) { 
                        foreach($items as $item) {

                            $inventoryItems = new InventoryItems();
                            $inventoryItems->inventory_id = $inventory->id;
                            $inventoryItems->code = str_replace("-","",$item[0]); 
                            $inventoryItems->amount = $item[1];
                            $inventoryItems->recount = $item[2];

                            $inventoryItemsId = $inventoryItems->save();

                        }
                        $data['msg'] = "Inventario Importado com Sucesso";
                        $data['sucesso'] = true;
                        echo json_encode($data);
                    }
                }

            }
        }
    }

    public function blabla() {
        $mapFamilia = (new MapFamilia())->find("codnbmsh in (08045020)")->fetch(true);

        $dimPer = (new DIMPER())->getIdDt(date("d/m/Y"));
        $aggFlsProd = (new AGGFLSPROD())->find("ID_DT = :ID_DT","ID_DT=$dimPer","CD_FIL,SUM(VL_VDA) VENDA")->group("CD_FIL")->order("CD_FIL")->fetch(true);
        foreach($aggFlsProd as $prod){
            $data[$prod->CD_FIL] = $prod->VENDA;
        }
        function __output_header__( $_dados = array() )
        {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode(
                $_dados
            );
            # por ser a ultima funcao, podemos matar o processo aqui.
            exit;
        }
        $dados = (new MarginVerification())->find("id = :id","id=1792")->fetch();
        __output_header__((array) $data );
    }

    //Serviços
    public function webServiceMargin() {

        $params['alteracaoInicial'] = '1'.date('ymd');
        $comparation[6] = "ESCHC_DATA3 = :alteracaoInicial";

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);
        $groups = (new Store())->find()->fetch(true);


        foreach($groups as $group) {
            if ($group->group_id == 1 || $group->group_id == 10) {
                $stores1[$group->code] = $group->code;

            } else if ($group->group_id == 2) {
                $stores2[$group->code] = $group->code;

            } 
        }

        if (is_array($stores1)){
            foreach($stores1 as $s1) {
                $comparation1[$s1] = "ESCLC_CODIGO = :store$s1";
                $params1["store$s1"] = $s1;
            }
            $params1 = http_build_query($params1);
            $comparation1 = implode(" or ",$comparation1);
            $ag1iensa = (new AG1IENSA())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11) and ($comparation1) and ENTSAIC_SITUACAO != :es"
            ,$params."&ag1=1&ag10=10&ag11=11&ag9=9&$params1&es='9'")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
            
        }
        
        if (is_array($stores2)){
            foreach($stores2 as $s2) {
                $comparation2[$s2] = "ESCLC_CODIGO = :store$s2";
                $params2["store$s2"] = $s2;
            }
            $params2 = http_build_query($params2);
            $comparation2 = implode(" or ",$comparation2); 
            
            $ag1iensa2 = (new AG1IENSA2())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11) and ($comparation2) and ENTSAIC_SITUACAO != :es"
                         ,$params."&ag1=1&&ag10=10&ag11=11&ag9=9&$params2&es='9'")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
        }

        foreach ($ag1iensa as $iensa){
            if ($iensa->getMargin($iensa->ESCLC_CODIGO) <= 0 || $iensa->getMargin($iensa->ESCLC_CODIGO) > 50) {
                if ($iensa->getPrice > 0) {
                    $marginDate = date("Y-m-d");
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code,substr(created_at,1,10)) = :snc","snc=$iensa->ESCLC_CODIGO$iensa->ESCHC_NRO_NOTA3$iensa->ESITC_CODIGO$iensa->ESITC_DIGITO$marginDate")->count();
                    
                    if ($marginVerification == 0) {
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $iensa->ESCLC_CODIGO;
                        $newRegister->nf_number = $iensa->ESCHC_NRO_NOTA3;
                        $newRegister->provider = $iensa->razaoDestino()->TIP_RAZAO_SOCIAL;
                        $newRegister->code = $iensa->ESITC_CODIGO.$iensa->ESITC_DIGITO;
                        $newRegister->description = $iensa->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = $iensa->ENTSAIC_CUS_UN;
                        $newRegister->price = $iensa->getPrice;
                        $newRegister->margin = round($iensa->getMargin($iensa->ESCLC_CODIGO));
                        $newRegister->user = $iensa->getAA1CFISC()->FIS_USUARIO_INC;
                        $newRegisterId =  $newRegister->save();
    
                        $msg1 = "Loja $iensa->ESCLC_CODIGO Mix Bahia\nNota  $iensa->ESCHC_NRO_NOTA3\nFornecedor ".$iensa->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO." - ".$iensa->getItemRegister()->GIT_DESCRICAO."\nCusto $iensa->ENTSAIC_CUS_UN\nPreco: $iensa->getPrice, Data do preço $iensa->getDatePrice\nMargem ".round($iensa->getMargin($iensa->ESCLC_CODIGO))."\nUltima Saida $iensa->ultimaSaida\nOperador ".$iensa->getAA1CFISC()->FIS_USUARIO_INC."\n";
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "@magenmaiormenor";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info($msg1);
                        sleep(5);
                        if ($iensa->getPosted()){
                            if ($iensa->getPosted()->getOperatorClass()->telegram) {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = $iensa->getPosted()->getOperatorClass()->telegram;
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg1);
                                sleep(5);
                            }
                        }
                    } else {
                        $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code,substr(created_at,1,10)) = :snc and approved = :approved and sent = :sent","approved=N&sent=N&snc=$iensa->ESCLC_CODIGO$iensa->ESCHC_NRO_NOTA3$iensa->ESITC_CODIGO$iensa->ESITC_DIGITO$marginDate")->fetch();
                        $start_date = new DateTime($marginVerification->created_at);
                        $since_start = $start_date->diff(new DateTime(date("Y-m-d H:i:s")));

                        if ($since_start->h >= 1 && $iensa->ESCHC_SECAO3 != 5 && $iensa->ESCHC_SECAO3 != 6 && $iensa->ESCHC_SECAO3 != 15  && $iensa->ESCHC_SECAO3.$iensa->ESCHC_SECAO3->ESCHC_GRUPO3 != 81  && $iensa->ESCHC_SECAO3.$iensa->ESCHC_SECAO3->ESCHC_GRUPO3 != 83) {
                            if (telegram($iensa->ESCLC_CODIGO) != '') {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = telegram($iensa->ESCLC_CODIGO);
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info("Loja $iensa->ESCLC_CODIGO Mix Bahia\n Nota  $iensa->ESCHC_NRO_NOTA3\n Fornecedor ".$iensa->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO." - ".$iensa->getItemRegister()->GIT_DESCRICAO."\n custo $iensa->ENTSAIC_CUS_UN preco $iensa->getPrice margem ".round($iensa->getMargin($iensa->ESCLC_CODIGO))." Operador ".$iensa->getAA1CFISC()->FIS_USUARIO_INC."\n\n");
                                sleep(5);
                                $approved = (new MarginVerification())->findById($marginVerification->id);
                                $approved->approved = "S";
                                $approved->sent = "S";
                                $approvedId = $approved->save();
                            }  
                        }
                    }
                }
            }           
        }
        
        foreach ($ag1iensa2 as $iensa2){
            if ($iensa2->getMargin($iensa2->ESCLC_CODIGO) <= 0 || $iensa2->getMargin($iensa2->ESCLC_CODIGO) > 50) {
                if ($iensa2->getPrice > 0) {
                    $marginDate = date("Y-m-d");
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code,substr(created_at,1,10)) = :snc","snc=$iensa2->ESCLC_CODIGO$iensa2->ESCHC_NRO_NOTA3$iensa2->ESITC_CODIGO$iensa2->ESITC_DIGITO$marginDate")->count();
                    if ($marginVerification == 0) {
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $iensa2->ESCLC_CODIGO;
                        $newRegister->nf_number = $iensa2->ESCHC_NRO_NOTA3;
                        $newRegister->provider = $iensa2->razaoDestino()->TIP_RAZAO_SOCIAL;
                        $newRegister->code = $iensa2->ESITC_CODIGO.$iensa2->ESITC_DIGITO;
                        $newRegister->description = $iensa2->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = $iensa2->ENTSAIC_CUS_UN;
                        $newRegister->price = $iensa2->getPrice;
                        $newRegister->margin = round($iensa2->getMargin($iensa2->ESCLC_CODIGO));
                        $newRegister->user = $iensa2->getAA1CFISC()->FIS_USUARIO_INC;
                        $newRegisterId =  $newRegister->save();
    
                        $msg2 = "Loja $iensa2->ESCLC_CODIGO Bispo & Dantas\nNota  $iensa2->ESCHC_NRO_NOTA3\nFornecedor ".$iensa2->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa2->ESITC_CODIGO.'-'.$iensa2->ESITC_DIGITO." - ".$iensa2->getItemRegister()->GIT_DESCRICAO."\nCusto $iensa2->ENTSAIC_CUS_UN\nPreco: $iensa2->getPrice, Data do preço $iensa2->getDatePrice\nMargem ".round($iensa2->getMargin($iensa2->ESCLC_CODIGO))."\nUltima Saida $iensa2->ultimaSaida\nOperador ".$iensa2->getAA1CFISC()->FIS_USUARIO_INC."\n";
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "@magenmaiormenor";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info($msg2);
                        sleep(5);
                        if ($iensa2->getPosted()){
                            if ($iensa2->getPosted()->getOperatorClass()->telegram) {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = $iensa2->getPosted()->getOperatorClass()->telegram;
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg2);
                                sleep(5);
                            }  
                        }
                    } else {
                        $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code,substr(created_at,1,10)) = :snc and approved = :approved and sent = :sent","approved=N&sent=N&snc=$iensa2->ESCLC_CODIGO$iensa2->ESCHC_NRO_NOTA3$iensa2->ESITC_CODIGO$iensa2->ESITC_DIGITO$marginDate")->fetch();
                        $start_date = new DateTime($marginVerification->created_at);
                        $since_start = $start_date->diff(new DateTime(date("Y-m-d H:i:s")));
                        if ($since_start->h >= 1 && $iensa2->ESCHC_SECAO3 != 5 && $iensa2->ESCHC_SECAO3 != 6 && $iensa2->ESCHC_SECAO3 != 15  && $iensa2->ESCHC_SECAO3.$iensa2->ESCHC_SECAO3->ESCHC_GRUPO3 != 81  && $iensa2->ESCHC_SECAO3.$iensa2->ESCHC_SECAO3->ESCHC_GRUPO3 != 83 ) {
                            if (telegram($iensa2->ESCLC_CODIGO) != '') {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = telegram($iensa2->ESCLC_CODIGO);
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info("Loja $iensa2->ESCLC_CODIGO Bispo & Dantas\n Nota  $iensa2->ESCHC_NRO_NOTA3\n Fornecedor ".$iensa2->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa2->ESITC_CODIGO.'-'.$iensa2->ESITC_DIGITO." - ".$iensa2->getItemRegister()->GIT_DESCRICAO."\n custo $iensa2->ENTSAIC_CUS_UN preco $iensa2->getPrice margem ".round($iensa2->getMargin($iensa2->ESCLC_CODIGO))." Operador ".$iensa2->getAA1CFISC()->FIS_USUARIO_INC."\n\n");
                                $approved = (new MarginVerification())->findById($marginVerification->id);
                                $approved->approved = "S";
                                $approved->sent = "S";
                                $approvedId = $approved->save();
                            } 
                        }
                    }
                }
            }            
        }

        $notificationsAudit = (new NotificationsAudit())->findById(1);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function webServiceMarginFora() {
        $params['alteracaoInicial'] = '1'.date('ymd');
        $comparation[6] = "ESCHC_DATA3 = :alteracaoInicial";

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);
        $groups = (new Store())->find()->fetch(true);

        foreach($groups as $group) {
            if ($group->group_id == 1 || $group->group_id == 10) {
                $stores1[$group->code] = $group->code;
            } else if ($group->group_id == 2) {
                $stores2[$group->code] = $group->code;
            } else if ($group->group_id == 4 or $group->goup_id == 8) {
                $stores3[$group->code] = $group->code;
            }
        }

        if (is_array($stores1)){
            foreach($stores1 as $s1) {
                $comparation1[$s1] = "ESCLC_CODIGO != :store$s1";
                $params1["store$s1"] = $s1;
            }
            $params1 = http_build_query($params1);
            $comparation1 = implode(" and ",$comparation1);
            $ag1iensa = (new AG1IENSA())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11) and ($comparation1)"
            ,$params."&ag1=1&ag10=10&ag11=11&ag9=9&$params1")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);


        }
        if (is_array($stores2)){
            foreach($stores2 as $s2) {
                $comparation2[$s2] = "ESCLC_CODIGO != :store$s2";
                $params2["store$s2"] = $s2;
            }
            $params2 = http_build_query($params2);
            $comparation2 = implode(" and ",$comparation2); 
            $ag1iensa2 = (new AG1IENSA2())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11) and ($comparation2)"
                         ,$params."&ag1=1&&ag10=10&ag11=11&ag9=9&$params2")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                         
        }
        if (is_array($stores3)){
            foreach($stores3 as $s3) {
                $s33 = substr($s3,0,strlen($s3)-1);
                $comparation3[$s3] = "ESCLC_CODIGO != :store$s33";
                $params3["store$s33"] = $s33;
            }
            $params3 = http_build_query($params3);
            $comparation3 = implode(" and ",$comparation3); 
            $ag1iensa3 = (new AG1IENSA3())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11) and ($comparation3)"
                         ,$params."&ag1=1&&ag10=10&ag11=11&ag9=9&$params3")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);
                         
        }



        $aa1ctcon = (new AA1CTCON4())->getEntradas1();
        $ag1iensa4 = (new AG1IENSA4())->find($comparation." and ESCHC_AGENDA3 in ($aa1ctcon)"
        ,$params)->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);

        $ag1iensa7 = (new AG1IENSA7())->find($comparation." and (ESCHC_AGENDA3 = :ag9 or ESCHC_AGENDA3 = :ag1 or ESCHC_AGENDA3 = :ag10 or ESCHC_AGENDA3 = :ag11)"
        ,"&ag1=1&&ag10=10&ag11=11&ag9=9&$params3")->order("ESCLC_CODIGO,ESCHC_NRO_NOTA")->fetch(true);

        foreach ($ag1iensa as $iensa){
            if ($iensa->ESCLC_CODIGO == 43) {
                if ($iensa->getMargin($iensa->ESCLC_CODIGO) < 5) {
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$iensa->ESCLC_CODIGO$iensa->ESCHC_NRO_NOTA3$iensa->ESITC_CODIGO$iensa->ESITC_DIGITO")->count();
                    if ($marginVerification == 0) {
                        // echo telegram($iensa->ESCLC_CODIGO);
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $iensa->ESCLC_CODIGO;
                        $newRegister->nf_number = $iensa->ESCHC_NRO_NOTA3;
                        $newRegister->provider = $iensa->razaoDestino()->TIP_RAZAO_SOCIAL;
                        $newRegister->code = $iensa->ESITC_CODIGO.$iensa->ESITC_DIGITO;
                        $newRegister->description = $iensa->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = $iensa->ENTSAIC_CUS_UN;
                        $newRegister->price = $iensa->getPrice;
                        $newRegister->margin = round($iensa->getMargin($iensa->ESCLC_CODIGO));
                        $newRegister->user = $iensa->getAA1CFISC()->FIS_USUARIO_INC;
                        $newRegisterId =  $newRegister->save();
    
                        $msg1 = "Loja $iensa->ESCLC_CODIGO Mix Bahia\nNota  $iensa->ESCHC_NRO_NOTA3\nFornecedor ".$iensa->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO." - ".$iensa->getItemRegister()->GIT_DESCRICAO."\nCusto $iensa->ENTSAIC_CUS_UN\nPreco: $iensa->getPrice, Data do preço $iensa->getDatePrice\nMargem ".round($iensa->getMargin($iensa->ESCLC_CODIGO))."\nUltima Saida $iensa->ultimaSaida\nOperador ".$iensa->getAA1CFISC()->FIS_USUARIO_INC."\n";
                        if ($iensa->ESCLC_CODIGO == 54 || $iensa->ESCLC_CODIGO == 11) {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-1001754067200";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1); 
                        } else if (telegram($iensa->ESCLC_CODIGO) != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegram($iensa->ESCLC_CODIGO);
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                        } else {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-762504349";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1); 
                        }                   
                    } 
                }    
            } else if ($iensa->ESCLC_CODIGO == 74 || $iensa->ESCLC_CODIGO = 75) {
                if ($iensa->getMargin($iensa->ESCLC_CODIGO) < 10) {
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$iensa->ESCLC_CODIGO$iensa->ESCHC_NRO_NOTA3$iensa->ESITC_CODIGO$iensa->ESITC_DIGITO")->count();
                    if ($marginVerification == 0) {
                        // echo telegram($iensa->ESCLC_CODIGO);
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $iensa->ESCLC_CODIGO;
                        $newRegister->nf_number = $iensa->ESCHC_NRO_NOTA3;
                        $newRegister->provider = $iensa->razaoDestino()->TIP_RAZAO_SOCIAL;
                        $newRegister->code = $iensa->ESITC_CODIGO.$iensa->ESITC_DIGITO;
                        $newRegister->description = $iensa->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = $iensa->ENTSAIC_CUS_UN;
                        $newRegister->price = $iensa->getPrice;
                        $newRegister->margin = round($iensa->getMargin($iensa->ESCLC_CODIGO));
                        $newRegister->user = $iensa->getAA1CFISC()->FIS_USUARIO_INC;
                        $newRegisterId =  $newRegister->save();
    
                        $msg1 = "Loja $iensa->ESCLC_CODIGO Mix Bahia\nNota  $iensa->ESCHC_NRO_NOTA3\nFornecedor ".$iensa->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO." - ".$iensa->getItemRegister()->GIT_DESCRICAO."\nCusto $iensa->ENTSAIC_CUS_UN\nPreco: $iensa->getPrice, Data do preço $iensa->getDatePrice\nMargem ".round($iensa->getMargin($iensa->ESCLC_CODIGO))."\nUltima Saida $iensa->ultimaSaida\nOperador ".$iensa->getAA1CFISC()->FIS_USUARIO_INC."\n";
                        if ($iensa->ESCLC_CODIGO == 54 || $iensa->ESCLC_CODIGO == 11) {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-1001754067200";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1); 
                        } else if (telegram($iensa->ESCLC_CODIGO) != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegram($iensa->ESCLC_CODIGO);
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                        } else {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-762504349";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1); 
                        }                   
                    } 
                } 
            } else {
                if ($iensa->ESCHC_SECAO3 != 5 && $iensa->ESCHC_SECAO3 != 6 && $iensa->ESCHC_SECAO3 != 15  && $iensa->ESCHC_SECAO3.$iensa->ESCHC_SECAO3->ESCHC_GRUPO3 != 81  && $iensa->ESCHC_SECAO3.$iensa->ESCHC_SECAO3->ESCHC_GRUPO3 != 83 ) {
                    if ($iensa->getMargin($iensa->ESCLC_CODIGO) < 0) {
                        $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$iensa->ESCLC_CODIGO$iensa->ESCHC_NRO_NOTA3$iensa->ESITC_CODIGO$iensa->ESITC_DIGITO")->count();
                        if ($marginVerification == 0) {
                            // echo telegram($iensa->ESCLC_CODIGO);
                            $newRegister = new MarginVerification();
                            $newRegister->store_id = $iensa->ESCLC_CODIGO;
                            $newRegister->nf_number = $iensa->ESCHC_NRO_NOTA3;
                            $newRegister->provider = $iensa->razaoDestino()->TIP_RAZAO_SOCIAL;
                            $newRegister->code = $iensa->ESITC_CODIGO.$iensa->ESITC_DIGITO;
                            $newRegister->description = $iensa->getItemRegister()->GIT_DESCRICAO;
                            $newRegister->cost = $iensa->ENTSAIC_CUS_UN;
                            $newRegister->price = $iensa->getPrice;
                            $newRegister->margin = round($iensa->getMargin($iensa->ESCLC_CODIGO));
                            $newRegister->user = $iensa->getAA1CFISC()->FIS_USUARIO_INC;
                            $newRegisterId =  $newRegister->save();
        
                            $msg1 = "Loja $iensa->ESCLC_CODIGO Mix Bahia\n Nota  $iensa->ESCHC_NRO_NOTA3\n Fornecedor ".$iensa->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO." - ".$iensa->getItemRegister()->GIT_DESCRICAO."\n custo $iensa->ENTSAIC_CUS_UN preco $iensa->getPrice margem ".round($iensa->getMargin($iensa->ESCLC_CODIGO))." Operador ".$iensa->getAA1CFISC()->FIS_USUARIO_INC."\n\n";
                            if ($iensa->ESCLC_CODIGO == 54 || $iensa->ESCLC_CODIGO == 11) {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = "-1001754067200";
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg1); 
                            } else if (telegram($iensa->ESCLC_CODIGO) != '') {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = telegram($iensa->ESCLC_CODIGO);
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg1);
                            } else {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = "-762504349";
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg1); 
                            }                   
                        } 
                        
                    } 
                }
            }
            
                      
        }

        foreach ($ag1iensa2 as $iensa2){
            if ($iensa2->ESCLC_CODIGO == 49) {
                    if ($iensa2->getMargin($iensa2->ESCLC_CODIGO) < 5) {
                        $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$iensa2->ESCLC_CODIGO$iensa2->ESCHC_NRO_NOTA3$iensa2->ESITC_CODIGO$iensa2->ESITC_DIGITO")->count();
                        if ($marginVerification == 0) {
                            $newRegister = new MarginVerification();
                            $newRegister->store_id = $iensa2->ESCLC_CODIGO;
                            $newRegister->nf_number = $iensa2->ESCHC_NRO_NOTA3;
                            $newRegister->provider = $iensa2->razaoDestino()->TIP_RAZAO_SOCIAL;
                            $newRegister->code = $iensa2->ESITC_CODIGO.$iensa2->ESITC_DIGITO;
                            $newRegister->description = $iensa2->getItemRegister()->GIT_DESCRICAO;
                            $newRegister->cost = $iensa2->ENTSAIC_CUS_UN;
                            $newRegister->price = $iensa2->getPrice;
                            $newRegister->margin = round($iensa2->getMargin($iensa2->ESCLC_CODIGO));
                            $newRegister->user = $iensa2->getAA1CFISC()->FIS_USUARIO_INC;
                            $newRegisterId =  $newRegister->save();
                            $msg2 = "Loja $iensa2->ESCLC_CODIGO Bispo e Dantas\n    Nota  $iensa2->ESCHC_NRO_NOTA3\n    Fornecedor ".$iensa2->razaoDestino()->TIP_RAZAO_SOCIAL."\n    ". $iensa2->ESITC_CODIGO.'-'.$iensa2->ESITC_DIGITO." - ".$iensa2->getItemRegister()->GIT_DESCRICAO."\n    Custo $iensa2->ENTSAIC_CUS_UN\n    Preco: $iensa2->getPrice, Data do preço $iensa2->getDatePrice\n    Margem ".round($iensa2->getMargin($iensa2->ESCLC_CODIGO))."\n    Ultima Saida $iensa2->ultimaSaida\n    Operador ".$iensa2->getAA1CFISC()->FIS_USUARIO_INC."\n";
                            if (telegram($iensa2->ESCLC_CODIGO) != '') {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = telegram($iensa2->ESCLC_CODIGO);
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg2);
                            } else {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = "-621906818";
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg2);
                            }                   
        
                        }
                    } 
            } else {
                if ($iensa2->ESCHC_SECAO3 != 5 && $iensa2->ESCHC_SECAO3 != 6 && $iensa2->ESCHC_SECAO3 != 15  && $iensa2->ESCHC_SECAO3.$iensa2->ESCHC_GRUPO3 != 81  && $iensa2->ESCHC_SECAO3.$iensa2->ESCHC_GRUPO3 != 83 ) {
                    if ($iensa2->getMargin($iensa2->ESCLC_CODIGO) < 0) {
                        $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$iensa2->ESCLC_CODIGO$iensa2->ESCHC_NRO_NOTA3$iensa2->ESITC_CODIGO$iensa2->ESITC_DIGITO")->count();
                        if ($marginVerification == 0) {
                            $newRegister = new MarginVerification();
                            $newRegister->store_id = $iensa2->ESCLC_CODIGO;
                            $newRegister->nf_number = $iensa2->ESCHC_NRO_NOTA3;
                            $newRegister->provider = $iensa2->razaoDestino()->TIP_RAZAO_SOCIAL;
                            $newRegister->code = $iensa2->ESITC_CODIGO.$iensa2->ESITC_DIGITO;
                            $newRegister->description = $iensa2->getItemRegister()->GIT_DESCRICAO;
                            $newRegister->cost = $iensa2->ENTSAIC_CUS_UN;
                            $newRegister->price = $iensa2->getPrice;
                            $newRegister->margin = round($iensa2->getMargin($iensa2->ESCLC_CODIGO));
                            $newRegister->user = $iensa2->getAA1CFISC()->FIS_USUARIO_INC;
                            $newRegisterId =  $newRegister->save();
                            $msg2 = "Loja $iensa2->ESCLC_CODIGO Bispo e Dantas\n Nota  $iensa2->ESCHC_NRO_NOTA3\n Fornecedor ".$iensa2->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa2->ESITC_CODIGO-$iensa2->ESITC_DIGITO." - ".$iensa2->getItemRegister()->GIT_DESCRICAO."\n custo $iensa2->ENTSAIC_CUS_UN preco $iensa2->getPrice margem ".round($iensa2->getMargin($iensa2->ESCLC_CODIGO))." Operador ".$iensa2->getAA1CFISC()->FIS_USUARIO_INC."\n\n";
                            if (telegram($iensa2->ESCLC_CODIGO) != '') {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = telegram($iensa2->ESCLC_CODIGO);
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg2);
                            } else {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = "-621906818";
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info($msg2);
                            }                   
        
                        }
                    } 
                }
            }
            
           
        }

        foreach ($ag1iensa3 as $iensa2){
            if ($iensa2->ESCHC_SECAO3 != 5 && $iensa2->ESCHC_SECAO3 != 6 && $iensa2->ESCHC_SECAO3 != 15  && $iensa2->ESCHC_SECAO3.$iensa2->ESCHC_GRUPO3 != 81  && $iensa2->ESCHC_SECAO3.$iensa2->ESCHC_GRUPO3 != 83 ) {
                if ($iensa2->getMargin($iensa2->ESCLC_CODIGO) < 0) {
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$iensa2->ESCLC_CODIGO$iensa2->ESCHC_NRO_NOTA3$iensa2->ESITC_CODIGO$iensa2->ESITC_DIGITO")->count();
                    if ($marginVerification == 0) {
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $iensa2->ESCLC_CODIGO;
                        $newRegister->nf_number = $iensa2->ESCHC_NRO_NOTA3;
                        $newRegister->provider = $iensa2->razaoDestino()->TIP_RAZAO_SOCIAL;
                        $newRegister->code = $iensa2->ESITC_CODIGO.$iensa2->ESITC_DIGITO;
                        $newRegister->description = $iensa2->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = $iensa2->ENTSAIC_CUS_UN;
                        $newRegister->price = $iensa2->getPrice;
                        $newRegister->margin = round($iensa2->getMargin($iensa2->ESCLC_CODIGO));
                        $newRegister->user = $iensa2->getAA1CFISC()->FIS_USUARIO_INC;
                        $newRegisterId =  $newRegister->save();
                        $msg2 = "Loja $iensa->ESCLC_CODIGO Novo Mix\nNota  $iensa->ESCHC_NRO_NOTA3\nFornecedor ".$iensa->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO." - ".$iensa->getItemRegister()->GIT_DESCRICAO."\nCusto $iensa->ENTSAIC_CUS_UN\nPreco: $iensa->getPrice, Data do preço $iensa->getDatePrice\nMargem ".round($iensa->getMargin($iensa->ESCLC_CODIGO))."\nUltima Saida $iensa->ultimaSaida\nOperador ".$iensa->getAA1CFISC()->FIS_USUARIO_INC."\n";
                        if (telegramNovoMix($iensa2->ESCLC_CODIGO) != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegramNovoMix($iensa2->ESCLC_CODIGO);
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg2);
                            sleep(5);
                        }                   
    
                    }
                } 
            }           
        }

        foreach ($ag1iensa4 as $iensa2) {
            if ($iensa2->ESCHC_SECAO3 != 400 && $iensa2->ESCHC_SECAO3 != 409 && $iensa2->ESCHC_SECAO3 != 403  && $iensa2->ESCHC_SECAO3 != 401) {
                if ($iensa2->getMargin($iensa2->ESCLC_CODIGO) < 0) {
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$iensa2->ESCLC_CODIGO$iensa2->ESCHC_NRO_NOTA3$iensa2->ESITC_CODIGO$iensa2->ESITC_DIGITO")->count();
                    if ($marginVerification == 0) {
                        // echo $iensa2->ESCLC_CODIGO."-".$iensa2->ESCHC_NRO_NOTA3."-".$iensa2->razaoDestino()->TIP_RAZAO_SOCIAL."-".$iensa2->ESITC_CODIGO.$iensa2->ESITC_DIGITO."-".
                        $iensa2->getItemRegister()->GIT_DESCRICAO."-".$iensa2->ENTSAIC_CUS_UN."-".$iensa2->getPrice."-".round($iensa2->getMargin($iensa2->ESCLC_CODIGO))."<br>";

                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $iensa2->ESCLC_CODIGO;
                        $newRegister->nf_number = $iensa2->ESCHC_NRO_NOTA3;
                        $newRegister->provider = $iensa2->razaoDestino()->TIP_RAZAO_SOCIAL;
                        $newRegister->code = $iensa2->ESITC_CODIGO.$iensa2->ESITC_DIGITO;
                        $newRegister->description = $iensa2->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = $iensa2->ENTSAIC_CUS_UN;
                        $newRegister->price = $iensa2->getPrice;
                        $newRegister->margin = round($iensa2->getMargin($iensa2->ESCLC_CODIGO));
                        $newRegister->user = $iensa2->getAA1CFISC()->FIS_USUARIO_INC;
                        $newRegisterId =  $newRegister->save();
                        $msg2 = "Loja $iensa2->ESCLC_CODIGO Unimar\n Nota  $iensa2->ESCHC_NRO_NOTA3\n Fornecedor ".$iensa2->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa2->ESITC_CODIGO.'-'.$iensa2->ESITC_DIGITO." - ".$iensa2->getItemRegister()->GIT_DESCRICAO."\n custo $iensa2->ENTSAIC_CUS_UN preco $iensa2->getPrice margem ".round($iensa2->getMargin($iensa2->ESCLC_CODIGO))." Operador ".$iensa2->getAA1CFISC()->FIS_USUARIO_INC."\n\n";
                        if ('-738674161') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = '-738674161';
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg2);
                            sleep(5);
                        }                   
    
                    }
                } 
            } 
        }

        foreach ($ag1iensa7 as $iensa7){
            if ($iensa7->getMargin($iensa7->ESCLC_CODIGO) < 0) {
                $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$iensa7->ESCLC_CODIGO$iensa7->ESCHC_NRO_NOTA3$iensa7->ESITC_CODIGO$iensa7->ESITC_DIGITO")->count();
                if ($marginVerification == 0) {
                    $newRegister = new MarginVerification();
                    $newRegister->store_id = $iensa7->ESCLC_CODIGO;
                    $newRegister->nf_number = $iensa7->ESCHC_NRO_NOTA3;
                    $newRegister->provider = $iensa7->razaoDestino()->TIP_RAZAO_SOCIAL;
                    $newRegister->code = $iensa7->ESITC_CODIGO.$iensa7->ESITC_DIGITO;
                    $newRegister->description = $iensa7->getItemRegister()->GIT_DESCRICAO;
                    $newRegister->cost = $iensa7->ENTSAIC_CUS_UN;
                    $newRegister->price = $iensa7->getPrice;
                    $newRegister->margin = round($iensa7->getMargin($iensa7->ESCLC_CODIGO));
                    $newRegister->user = $iensa7->getAA1CFISC()->FIS_USUARIO_INC;
                    $newRegisterId =  $newRegister->save();
                    $msg2 = "Loja $iensa7->ESCLC_CODIGO Bem Barato\nNota  $iensa7->ESCHC_NRO_NOTA3\nFornecedor ".$iensa7->razaoDestino()->TIP_RAZAO_SOCIAL."\n". $iensa7->ESITC_CODIGO.'-'.$iensa7->ESITC_DIGITO." - ".$iensa7->getItemRegister()->GIT_DESCRICAO."\nCusto $iensa7->ENTSAIC_CUS_UN\nPreco: $iensa->getPrice, Data do preço $iensa7->getDatePrice\nMargem ".round($iensa7->getMargin($iensa7->ESCLC_CODIGO))."\nUltima Saida $iensa7->ultimaSaida\nOperador ".$iensa7->getAA1CFISC()->FIS_USUARIO_INC."\n";
                    if (telegramNovoMix($iensa7->ESCLC_CODIGO) != '') {
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = '-616090466';
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info($msg2);
                        sleep(5);
                    }                   

                }
            }         
        }
        $notificationsAudit = (new NotificationsAudit())->findById(2);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function webService988x987() {
        $params['alteracaoInicial'] = '1'.date('ymd',strtotime("-1 days"));
        $comparation[6] = "ESCHC_DATA3 = :alteracaoInicial";

        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);

        $stores1 = array( 5, 8, 11, 12, 13, 14, 15, 17, 19, 25, 27, 32, 33, 35,36, 40, 43, 48, 50, 54, 56, 57, 58, 59, 61, 67, 69, 70, 72, 75, 76, 78, 79, 80, 82, 85, 89, 90);
        $stores2 = array(83, 87, 91, 20, 21, 84, 86, 95, 96, 102, 23, 93, 101, 18, 49, 94, 104, 105, 10, 106);


        if (is_array($stores1)){
            foreach($stores1 as $s1) {
                $comparation1[$s1] = "ESCHLJC_CODIGO3 = :store$s1";
                $params1["store$s1"] = $s1;
            }
            $params1 = http_build_query($params1);
            $comparation1 = implode(" or ",$comparation1);
            $ag1iensa = (new AG1IENSA())->find($comparation." and ESCHC_AGENDA3 = :ag and ($comparation1)"
            ,$params."&ag=988&$params1")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
        }

        if (is_array($stores2)){
            foreach($stores2 as $s2) {
                $comparation2[$s2] = "ESCHLJC_CODIGO3 = :store$s2";
                $params2["store$s2"] = $s2;
            }
            $params2 = http_build_query($params2);
            $comparation2 = implode(" or ",$comparation2); 
            $ag1iensa2 = (new AG1IENSA2())->find($comparation." and ESCHC_AGENDA3 = :ag and ($comparation2)"
                         ,$params."&ag=988&$params2")->order("ESCHLJC_CODIGO3,ESCHC_NRO_NOTA")->fetch(true);
        }

        foreach ($ag1iensa as $iensa){
            if ($iensa->agenda987 === false){

                $msg1 = "Loja $iensa->ESCHLJC_CODIGO3 Mix Bahia\n". $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO." - ".$iensa->getItemRegister()->GIT_DESCRICAO."\n quantidade $iensa->ENTSAIC_QUANTI_UN \n  Nota $iensa->ESCHC_NRO_NOTA3 \n  Operador ".$iensa->getAA1CFISC()->FIS_USUARIO_INC."\n\n";
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                if ((new Store())->find("code = :code","code=$iensa->ESCHLJC_CODIGO3")->count() == 0) {
                    $tele_channel = "-640484431";
                } else {
                    $tele_channel = "-781640101";
                }
                
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info($msg1);        
            }           
        }
        foreach ($ag1iensa2 as $iensa2){
            if ($iensa2->agenda987 === false){
                $msg1 = "Loja $iensa2->ESCHLJC_CODIGO3 Base BD\n". $iensa2->ESITC_CODIGO.'-'.$iensa2->ESITC_DIGITO." - ".$iensa2->getItemRegister()->GIT_DESCRICAO."\n quantidade $iensa2->ENTSAIC_QUANTI_UN \n  Nota $iensa2->ESCHC_NRO_NOTA3 \n  Operador ".$iensa2->getAA1CFISC()->FIS_USUARIO_INC."\n\n";
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                if ((new Store())->find("code = :code","code=$iensa2->ESCHLJC_CODIGO3")->count() == 0) {
                    $tele_channel = "-640484431";
                } else {
                    $tele_channel = "-781640101";
                }
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info($msg1);        
            }            
        }
        $notificationsAudit = (new NotificationsAudit())->findById(4);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();

    }

    public function webServiceMarginVendas() {
        $dimPer = (new DIMPER())->getIdDt(date("d/m/Y"));
        $groups = (new Store())->find()->fetch(true);
  
        foreach($groups as $group) {
            if ($group->group_id == 1 || $group->group_id == 10) {
                $stores1[$group->code] = $group->code;

            } else if ($group->group_id == 2) {
                $stores2[$group->code] = $group->code;

            } else if ($group->group_id == 8 || $group->group_id == 4){
                $stores3[$group->code] = substr($group->code,0,strlen($group->code)-1);
            }
        }
        
        // if (is_array($stores1)){
        //     foreach($stores1 as $s1) {
        //         $comparation1[$s1] = "CD_FIL = :store$s1";
        //         $params1["store$s1"] = $s1;
        //     }
        //     $dimPer = (new DIMPER())->getIdDt(date("d/m/Y"));
        //     $params1 = http_build_query($params1);
        //     $comparation1 = implode(" or ",$comparation1);
        //     $aggFlsProd = (new AGGFLSPROD())->find("ID_DT = :ID_DT and $comparation1","ID_DT=$dimPer&".$params1)->order("CD_FIL")->fetch(true);
 
        // }

        if (is_array($stores2)){
            foreach($stores2 as $s2) {
                $comparation2[$s2] = "CD_FIL = :store$s2";
                $params2["store$s2"] = $s2;
            }
            $dimPer = (new DIMPER2())->getIdDt(date("d/m/Y"));
            $params2 = http_build_query($params2);
            $comparation2 = implode(" or ",$comparation2);
            $aggFlsProd2 = (new AGGFLSPROD2())->find("ID_DT = :ID_DT and $comparation2","ID_DT=$dimPer&".$params2)->order("CD_FIL")->fetch(true);
 
        }

        // if (is_array($stores3)){
        //     foreach($stores3 as $s3) {
        //         $comparation3[$s3] = "CD_FIL = :store$s3";
        //         $params3["store$s3"] = $s3;
        //     }
        //     $dimPer = (new DIMPER3())->getIdDt(date("d/m/Y"));
        //     $params3 = http_build_query($params3);
        //     $comparation3 = implode(" or ",$comparation3);
        //     $aggFlsProd3 = (new AGGFLSPROD3())->find("ID_DT = :ID_DT and $comparation3","ID_DT=$dimPer&".$params3)->order("CD_FIL")->fetch(true);
 
        // }
        
        // foreach($aggFlsProd as $prod) {
            
        //     if ($prod->getMargin($prod->CD_FIL) < 0 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO != 5 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 83 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 81) {
        //         $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
        //         if ($marginVerification == 0) {
                    
        //             if ($prod->getItemRegister()->GIT_COD_ITEM) {
        //                 $newRegister = new MarginVerification();
        //                 $newRegister->store_id = $prod->CD_FIL;
        //                 $newRegister->nf_number = date("ymd");
        //                 $newRegister->provider = $prod->CD_FIL;
        //                 $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
        //                 $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
        //                 $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
        //                 $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
        //                 $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
        //                 $newRegister->user = 'venda';
        //                 $newRegisterId =  $newRegister->save();

        //                 $msg1 = "Loja $prod->CD_FIL Mix Bahia\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2).", margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
        //                 if (telegram($prod->CD_FIL) != '') {
        //                     $logger = new Logger("web");
        //                     $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
        //                     $tele_channel = telegram($prod->CD_FIL);
        //                     $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
        //                     $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
        //                     $logger->pushHandler($tele_handler);
        //                     $logger->info($msg1);
        //                     sleep(5);
        //                 } else {
        //                     $logger = new Logger("web");
        //                     $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
        //                     $tele_channel = "-1001718498444";
        //                     $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
        //                     $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
        //                     $logger->pushHandler($tele_handler);
        //                     $logger->info($msg1);
        //                     sleep(5);
        //                 }   
        //             }
        //         }
        //     }
        // }

        foreach($aggFlsProd2 as $prod) {
            "Loja $prod->CD_FIL Base BD\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
            if ($prod->getMargin($prod->CD_FIL) < 0 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO != 5 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 83 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 81) {
                $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
                if ($marginVerification == 0) {
                    if ($prod->getItemRegister()->GIT_COD_ITEM) {
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $prod->CD_FIL;
                        $newRegister->nf_number = date("ymd");
                        $newRegister->provider = $prod->CD_FIL;
                        $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
                        $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
                        $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
                        $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
                        $newRegister->user = 'venda';
                        $newRegisterId =  $newRegister->save();
    
                        $msg1 = "Loja $prod->CD_FIL Base BD\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
    
                        if (telegram($prod->CD_FIL) != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegram($prod->CD_FIL);
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        } else {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-1001718498444";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        }
                    }                   
                }
            }
        }

        // foreach($aggFlsProd3 as $prod) {
            
        //     if ($prod->getMargin($prod->CD_FIL) < 0 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO != 5 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 83 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 81) {
        //         $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
        //         if ($marginVerification == 0) {
        //             $newRegister = new MarginVerification();
        //             $newRegister->store_id = $prod->CD_FIL;
        //             $newRegister->nf_number = date("ymd");
        //             $newRegister->provider = $prod->CD_FIL;
        //             $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
        //             $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
        //             $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
        //             $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
        //             $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
        //             $newRegister->user = 'venda';
        //             $newRegisterId =  $newRegister->save();

        //             $msg1 = "Loja $prod->CD_FIL Novo Mix\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
                    
        //             $logger = new Logger("web");
        //             $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
        //             $storeTele = (new Store())->find("code = :code","code=$prod->CD_FIL".$prod->getAA2CTIPO()->TIP_DIGITO)->fetch();
        //             if ($storeTele->group_id == 8) {
        //                 $tele_channel = "-1001719114997";
        //             } else if($storeTele->group_id == 4) {
        //                 $tele_channel = "-1001682624236";
        //             }
        //             $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
        //             $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
        //             $logger->pushHandler($tele_handler);
        //             if ($prod->getItemRegister()->GIT_COD_ITEM) {
        //                 $logger->info($msg1);
        //                 sleep(5);
        //             }
                   
                   
        //         }
        //     }
        // }
        $notificationsAudit = (new NotificationsAudit())->findById(16);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();

    }

    public function webServiceMarginVendasFora() {
        $dimPer = (new DIMPER())->getIdDt(date("d/m/Y"));
        $groups = (new Store())->find()->fetch(true);
  
        foreach($groups as $group) {
            if ($group->group_id == 1 || $group->group_id == 10) {
                $stores1[$group->code] = $group->code;

            } else if ($group->group_id == 2) {
                $stores2[$group->code] = $group->code;

            }
             else if ($group->group_id == 8 || $group->group_id == 4){
                $stores3[$group->code] = substr($group->code,0,strlen($group->code)-1);
            }
        }
        
        if (is_array($stores1)){
            foreach($stores1 as $s1) {
                $comparation1[$s1] = "CD_FIL != :store$s1";
                $params1["store$s1"] = $s1;
            }
            $params1 = http_build_query($params1);
            $comparation1 = implode(" and ",$comparation1);
            $aggFlsProd = (new AGGFLSPROD())->find("ID_DT = :ID_DT and $comparation1","ID_DT=$dimPer&".$params1)->order("CD_FIL")->fetch(true);
 
        }

        if (is_array($stores2)){
            foreach($stores2 as $s2) {
                $comparation2[$s2] = "CD_FIL != :store$s2";
                $params2["store$s2"] = $s2;
            }
            $params2 = http_build_query($params2);
            $comparation2 = implode(" and ",$comparation2);
            $aggFlsProd2 = (new AGGFLSPROD2())->find("ID_DT = :ID_DT and $comparation2","ID_DT=$dimPer&".$params2)->order("CD_FIL")->fetch(true);
 
        }

        if (is_array($stores3)){
            foreach($stores3 as $s3) {
                $comparation3[$s3] = "CD_FIL != :store$s3";
                $params3["store$s3"] = $s3;
            }
            $params3 = http_build_query($params3);
            $comparation3 = implode(" and ",$comparation3);
            $aggFlsProd3 = (new AGGFLSPROD3())->find("ID_DT = :ID_DT and $comparation3","ID_DT=$dimPer&".$params3)->order("CD_FIL")->fetch(true);
 
        }

        $aggFlsProd4 = (new AGGFLSPROD4())->find("ID_DT = :ID_DT and $comparation3","ID_DT=$dimPer&".$params3)->order("CD_FIL")->fetch(true);
        $aggFlsProd7 = (new AGGFLSPROD7())->find("ID_DT = :ID_DT and $comparation3","ID_DT=$dimPer&".$params3)->order("CD_FIL")->fetch(true);

        foreach($aggFlsProd as $prod) {
            if($prod->CD_FIL == 43) {
                if ($prod->getMargin($prod->CD_FIL) < 5) {
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
                    if ($marginVerification == 0) {
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $prod->CD_FIL;
                        $newRegister->nf_number = date("ymd");
                        $newRegister->provider = $prod->CD_FIL;
                        $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
                        $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
                        $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
                        $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
                        $newRegister->user = 'venda';
                        $newRegisterId =  $newRegister->save();
    
                        $msg1 = "Loja $prod->CD_FIL Mix Bahia\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
                        if (telegram($prod->CD_FIL) != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegram($prod->CD_FIL);
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        } else {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-1001718498444";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        }                   
                    }
                }
            } else {
                if ($prod->getMargin($prod->CD_FIL) < 0 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO != 5 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 83 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 81) {
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
                    if ($marginVerification == 0) {
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $prod->CD_FIL;
                        $newRegister->nf_number = date("ymd");
                        $newRegister->provider = $prod->CD_FIL;
                        $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
                        $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
                        $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
                        $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
                        $newRegister->user = 'venda';
                        $newRegisterId =  $newRegister->save();
    
                        $msg1 = "Loja $prod->CD_FIL Mix Bahia\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
                        if (telegram($prod->CD_FIL) != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegram($prod->CD_FIL);
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        } else {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-1001718498444";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        }                   
                    }
                }
            }

        }

        foreach($aggFlsProd2 as $prod) {
            if ($prod->CD_FIL == 49) {
                if ($prod->getMargin($prod->CD_FIL) < 5) {
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
                    if ($marginVerification == 0) {
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $prod->CD_FIL;
                        $newRegister->nf_number = date("ymd");
                        $newRegister->provider = $prod->CD_FIL;
                        $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
                        $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
                        $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
                        $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
                        $newRegister->user = 'venda';
                        $newRegisterId =  $newRegister->save();
    
                        $msg1 = "Loja $prod->CD_FIL Base BD\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
    
                        if (telegram($prod->CD_FIL) != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegram($prod->CD_FIL);
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        } else {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-1001718498444";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        }
                       
                    }
                }
            } else {
                if ($prod->getMargin($prod->CD_FIL) < 0 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO != 5 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 83 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 81) {
                    $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
                    if ($marginVerification == 0) {
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $prod->CD_FIL;
                        $newRegister->nf_number = date("ymd");
                        $newRegister->provider = $prod->CD_FIL;
                        $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
                        $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
                        $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
                        $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
                        $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
                        $newRegister->user = 'venda';
                        $newRegisterId =  $newRegister->save();
    
                        $msg1 = "Loja $prod->CD_FIL Base BD\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
    
                        if (telegram($prod->CD_FIL) != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegram($prod->CD_FIL);
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        } else {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-1001718498444";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        }
                       
                    }
                }
            }           
        }

        foreach($aggFlsProd3 as $prod) {           
            if ($prod->getMargin($prod->CD_FIL) < 0 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO != 5 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 83 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 81) {
                $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
                if ($marginVerification == 0) {
                    $newRegister = new MarginVerification();
                    $newRegister->store_id = $prod->CD_FIL;
                    $newRegister->nf_number = date("ymd");
                    $newRegister->provider = $prod->CD_FIL;
                    $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
                    $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
                    $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
                    $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
                    $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
                    $newRegister->user = 'venda';
                    $newRegisterId =  $newRegister->save();

                    $msg1 = "Loja $prod->CD_FIL Novo Mix\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
                    
                    if ($prod->CD_FIL == 50 or $prod->CD_FIL == 51 or $prod->CD_FIL == 52 or $prod->CD_FIL == 67){
                        
                    }
                    if ($prod->getItemRegister()->GIT_COD_ITEM > 0) {
                        if (telegramNovoMix($prod->CD_FIL,'S') != '') {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = telegramNovoMix($prod->CD_FIL,'S');
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        } else {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-1001718498444";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info($msg1);
                            sleep(5);
                        }
                    }
                    
                    
                   
                }
            }
        }

        foreach($aggFlsProd4 as $prod) {
            
            if ($prod->getMargin($prod->CD_FIL) < 0 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO != 5 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 83 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 81) {
                $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
                if ($marginVerification == 0) {
                    $newRegister = new MarginVerification();
                    $newRegister->store_id = $prod->CD_FIL;
                    $newRegister->nf_number = date("ymd");
                    $newRegister->provider = $prod->CD_FIL;
                    $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
                    $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
                    $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
                    $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
                    $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
                    $newRegister->user = 'venda';
                    $newRegisterId =  $newRegister->save();

                    $msg1 = "Loja $prod->CD_FIL Unimar\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
                    
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-625313104";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info($msg1);
                    sleep(5);
                    
                   
                }
            }
        }

        foreach($aggFlsProd7 as $prod) {
            
            if ($prod->getMargin($prod->CD_FIL) < 0 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO != 5 && $prod->getItemRegister()->GIT_SECAO != 6 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 83 && $prod->getItemRegister()->GIT_SECAO.$prod->getItemRegister()->GIT_GRUPO != 81) {
                $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code) = :snc","snc=$prod->CD_FIL".date("ymd").$prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO)->count();
                if ($marginVerification == 0) {
                    $newRegister = new MarginVerification();
                    $newRegister->store_id = $prod->CD_FIL;
                    $newRegister->nf_number = date("ymd");
                    $newRegister->provider = $prod->CD_FIL;
                    $newRegister->code = $prod->getItemRegister()->GIT_COD_ITEM.$prod->getItemRegister()->GIT_DIGITO;
                    $newRegister->description = $prod->getItemRegister()->GIT_DESCRICAO;
                    $newRegister->cost = round($prod->VL_CMV/$prod->QTD_VDA,2);
                    $newRegister->price = round($prod->VL_VDA/$prod->QTD_VDA,2);
                    $newRegister->margin = round($prod->getMargin($prod->CD_FIL));
                    $newRegister->user = 'venda';
                    $newRegisterId =  $newRegister->save();

                    $msg1 = "Loja $prod->CD_FIL Bem Barato\n".  $prod->getItemRegister()->GIT_COD_ITEM."-".$prod->getItemRegister()->GIT_DIGITO." - ".$prod->getItemRegister()->GIT_DESCRICAO."\n custo bruto".round($prod->aa2cestq()->GET_CUS_ULT_ENT_BRU/100,2)." custo liquido ".round($prod->VL_CMV/$prod->QTD_VDA,2)." Data do preço: ".$prod->getDatePrice." preco ".round($prod->VL_VDA/$prod->QTD_VDA,2)." Data Ultima entrada ".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),0,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),2,2)."/".substr(str_pad($prod->aa2cestq()->GET_DT_ULT_ENT,6,'0',STR_PAD_LEFT),4,2)." margem ".round($prod->getMargin($prod->CD_FIL))."\n\n";
                    
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-616090466";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info($msg1);
                    sleep(5);
                    
                   
                }
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(17);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();

    }

    public function webServiceCupons() {
        if (date("H") <= 21 && date("H") >= 7 ) {
            $date = "1".date("ymd");
            $hora = date('His',strtotime("-1 Hour"));
            $stores1 = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
            foreach($stores1 as $store) {
                $capCupom  = (new CAPCUPOM())->find("CAP_FILIAL = :CAP_FILIAL and CAP_DATA = :CAP_DATA and CAP_HORA_TRANSACAO >= :CAP_HORA_TRANSACAO",
                                            "CAP_FILIAL=$store&CAP_DATA=$date&CAP_HORA_TRANSACAO=$hora")->fetch(true);
                $i = 0;
                foreach($capCupom as $count) {
                    $i++;
                }
                if ($i == 0) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-622257749";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja $store não encontrada!");
                }
            }
            $stores2 = array(83, 91, 20, 21, 84, 86, 95, 102, 23, 93, 101, 18, 49, 94, 10);
            foreach($stores2 as $store) {
                $capCupom  = (new CAPCUPOM2())->find("CAP_FILIAL = :CAP_FILIAL and CAP_DATA = :CAP_DATA and CAP_HORA_TRANSACAO >= :CAP_HORA_TRANSACAO",
                                            "CAP_FILIAL=$store&CAP_DATA=$date&CAP_HORA_TRANSACAO=$hora")->fetch(true);
                $i = 0;
                foreach($capCupom as $count) {
                    $i++;
                }
                if ($i == 0) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                 $tele_channel = "-622257749";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja $store não encontrada!");
                }
            }
            $notificationsAudit = (new NotificationsAudit())->findById(5);
            $notificationsAudit->last_run = date("Y-m-d H:i:s");
            $notificationsAuditId = $notificationsAudit->save();
        }
        
    }

    public function webServicePromocaoExpirada() {
        $date = "1".date("ymd");
        
         $stores1 = array( 5, 8, 11, 12, 13, 14, 15, 17, 19, 25, 27, 32, 33, 35,36, 40, 43, 48, 50, 54, 56, 57, 58, 59, 61, 67, 69, 70, 72, 75, 76, 78, 79, 80, 81, 82, 85, 89, 90);
         foreach($stores1 as $store) {
            $ag1pdvpd = (new AG1PDVPD())->find("PDV_OFERTA_FIM < :PDV_OFERTA_FIM1 and PDV_OFERTA_FIM != :PDV_OFERTA_FIM and PDV_PRECO_NORMAL != :PDV_PRECO_NORMAL
            and PDV_FILIAL = :PDV_FILIAL","PDV_OFERTA_FIM1=$date&PDV_OFERTA_FIM=0&PDV_PRECO_NORMAL=0&PDV_FILIAL=$store")->fetch(true);

             $i = 0;
             foreach($ag1pdvpd as $count) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-699061268";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja: $count->PDV_FILIAL\n EAN: $count->PDV_CODIGO_EAN13\n Descrição: $count->PDV_DESCRICAO\n Preço normal $count->PDV_PRECO_NORMAL\n Data do Preço Normal: $count->PDV_PRECO_NORMAL_DTA\n Preço de Oferta: $count->PDV_OFERTA_PRECO\n Data do Preço de Oferta: $count->PDV_OFERTA_FIM");
                sleep(5);
            }

         }
         $stores2 = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
         foreach($stores2 as $store) {
            $ag1pdvpd = (new AG1PDVPD2())->find("PDV_OFERTA_FIM < :PDV_OFERTA_FIM1 and PDV_OFERTA_FIM != :PDV_OFERTA_FIM and PDV_PRECO_NORMAL != :PDV_PRECO_NORMAL
            and PDV_FILIAL = :PDV_FILIAL","PDV_OFERTA_FIM1=$date&PDV_OFERTA_FIM=0&PDV_PRECO_NORMAL=0&PDV_FILIAL=$store")->fetch(true);
             $i = 0;
             foreach($ag1pdvpd as $count) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-699061268";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja: $count->PDV_FILIAL\n EAN: $count->PDV_CODIGO_EAN13\n Descrição: $count->PDV_DESCRICAO\n Preço normal $count->PDV_PRECO_NORMAL\n Data do Preço Normal: $count->PDV_PRECO_NORMAL_DTA\n Preço de Oferta: $count->PDV_OFERTA_PRECO\n Data do Preço de Oferta: $count->PDV_OFERTA_FIM");
                sleep(5);
             }
             
         }
         $notificationsAudit = (new NotificationsAudit())->findById(6);
         $notificationsAudit->last_run = date("Y-m-d H:i:s");
         $notificationsAuditId = $notificationsAudit->save();
        
        
    }

    public function webServiceFechamentoSemanal() {
        $date = "1".date('ymd',strtotime("+6 days"));
        $stores1 = array( 5, 8, 11, 12, 13, 14, 15, 17, 19, 25, 27, 32, 33, 35,36, 40, 43, 48, 50, 54, 56, 57, 58, 59, 61, 67, 69, 70, 72, 75, 76, 78, 79, 80, 82, 85, 89, 90);
        asort($stores1);
        foreach($stores1 as $store) {            
            $aa2cpara = (new AA2CPARA())->find("PAR_CODIGO = 31 and substr(PAR_ACESSO,1,6) = 'FECHSE'")->fetch(true);
           
            foreach($aa2cpara as $count) {       
                if(substr($count->PAR_ACESSO,6,4) == $store && $count->PAR_CONTEUDO != $date){
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-666179558";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja $store data do fechamento $count->PAR_CONTEUDO o correto seria $date!");
                    sleep(5);
                }
            }
        }
        $stores2 = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        asort($stores2);
        foreach($stores2 as $store) {            
            $aa2cpara = (new AA2CPARA2())->find("PAR_CODIGO = 31 and substr(PAR_ACESSO,1,6) = 'FECHSE'")->fetch(true);
            foreach($aa2cpara as $count) {       
                if(substr($count->PAR_ACESSO,6,4) == $store && $count->PAR_CONTEUDO != $date){
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-666179558";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja $store data do fechamento $count->PAR_CONTEUDO o correto seria $date!");
                    sleep(5);
                }                
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(7);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();        
    }

    public function webServiceCadastro() {
        $date = "1".date("ymd");
        $aa3citem = (new AA3CITEM2())->find("'1'||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),5)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),3,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),1,2) = :GIT_DAT_ENT_LIN",
                                            "GIT_DAT_ENT_LIN=$date")->fetch(true);

        // dd($aa3citem);
        foreach($aa3citem as $item) {
            if($item->getCountLinha > 7) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-618024799";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("$item->GIT_COD_ITEM - $item->GIT_DIGITO $item->GIT_DESCRICAO Usuário : ".$item->getLog()->LOGIT_USUARIO."!");
                sleep(5);
            } else {
                // echo "$item->GIT_COD_ITEM - $item->GIT_DIGITO $item->GIT_DESCRICAO $item->getCountLinha <br>";
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(8);
         $notificationsAudit->last_run = date("Y-m-d H:i:s");
         $notificationsAuditId = $notificationsAudit->save();
        
    }

    public function webServiceLancamento24Horas() {
        $requests = (new NfReceipt())->find("status_id != :status_id14 and status_id != :status_id16 and status_id != :status_id8 and status_id != :status_id1 and status_id != :status_id2 and status_id != :status_id7",
        "status_id1=3&status_id14=14&status_id7=7&status_id8=8&status_id16=16&status_id2=2")->fetch(true);
        foreach($requests as $request) {
            $dateStart = new \DateTime($request->created_at);
            $dateNow   = new \DateTime(date("Y-m-d H:i:s"));
            $dateDiff = $dateStart->diff($dateNow);
            $horas = $dateDiff->h+($dateDiff->d*24);
            if ($horas > 24) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-608769366";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja: $request->store_id Número: $request->nf_number Aberto em : $request->getCreatedAt Cliente: $request->getClient Chave de acesso: $request->access_key Fornecedor: $request->getFornecedor Tipo: ".$request->getType()."Grupo: $request->clientGroup Quantidade de Itens: $request->items_quantity Alterado em: $request->getUpdatedAt Operador:  $request->getOperator Operação: $request->getOperation Status: $request->getStatus Duração: $horas horas");
                sleep(5);
            }

        }
        $notificationsAudit = (new NotificationsAudit())->findById(9);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function webServiceDevolucaoVenda() {
        $date = "1".date("ym",strtotime("-1 month"));
        $stores1 = array( 5, 8, 11, 12, 13, 14, 15, 17, 19, 25, 27, 32, 33, 35,36, 40, 43, 48, 50, 54, 56, 57, 58, 59, 61, 67, 69, 70, 72, 75, 76, 78, 79, 80, 82, 85, 89, 90);
        foreach ($stores1 as $s1) {
            $devclicp = (new DEVCLICP())->find("NR_SEQ_DEV > :NR_SEQ_DEV and CD_LOJ_EMTT = :CD_LOJ_EMTT and FLAG_SITU = :FLAG_SITU and substr(DT_DEV,0,5) = :DT_DEV1",
                                            "NR_SEQ_DEV=0&CD_LOJ_EMTT=$s1&FLAG_SITU=0&DT_DEV1=$date")->fetch(true);
            foreach($devclicp as $dev) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-1001619458080";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja: $s1\n Data: $dev->DT_DEV \n Valor da devolução: $dev->VLR_TTL_DEV.");
                sleep(5);
            }
        }

        $stores2 = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        foreach ($stores2 as $s2) {
            $devclicp = (new DEVCLICP())->find("NR_SEQ_DEV > :NR_SEQ_DEV and CD_LOJ_EMTT = :CD_LOJ_EMTT and FLAG_SITU = :FLAG_SITU and substr(DT_DEV,0,5) = :DT_DEV1",
                                            "NR_SEQ_DEV=0&CD_LOJ_EMTT=$s2&FLAG_SITU=0&DT_DEV1=$date")->fetch(true);
            foreach($devclicp as $dev) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-1001619458080";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja: $s2\n Data: $dev->DT_DEV \n Valor da devolução: $dev->VLR_TTL_DEV.");
                sleep(5);
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(11);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function webNotasNaoAutorizadas() {
        $date = "1".date("ymd",strtotime("-1 days"));
        $stores1 = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
        foreach ($stores1 as $s1) {
            $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :TIP_CODIGO","TIP_CODIGO=$s1")->fetch();
            $nfe_controle = (new NFE_CONTROLE())->find("SUBSTR(CHAVE_ORIGEM,1,14) = :CHAVE_ORIGEM and CODIGO_SITUACAO != :CODIGO_SITUACAO and dateto_rms7(DATA_PROCESSAMENTO) = :DATA_PROCESSAMENTO",
            "CHAVE_ORIGEM=".str_pad($aa2ctipo->TIP_CGC_CPF,14,'0',STR_PAD_LEFT)."&CODIGO_SITUACAO=6&DATA_PROCESSAMENTO=$date")->fetch(true);
            foreach($nfe_controle as $nfe) {
                if ($nfe->DESCRICAO_SITUACAO != 'NF Cancelada'){                   
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-1001499051648";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja: $aa2ctipo->TIP_CODIGO - $aa2ctipo->TIP_DIGITO $aa2ctipo->TIP_RAZAO\n Chave de acesso: $nfe->CHAVE_ACESSO_NFE \n Status: $nfe->DESCRICAO_SITUACAO.");
                    sleep(5);
                }
            }
        }
        $stores2 = array(83, 117, 118 , 119, 87, 91, 20, 21, 84, 86, 95, 102, 23, 93, 101, 18, 49, 94, 104, 105, 10, 106, 108);
        foreach ($stores2 as $s2) {
            $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = :TIP_CODIGO","TIP_CODIGO=$s2")->fetch();
            $nfe_controle = (new NFE_CONTROLE2())->find("SUBSTR(CHAVE_ORIGEM,1,14) = :CHAVE_ORIGEM and CODIGO_SITUACAO != :CODIGO_SITUACAO and dateto_rms7(DATA_PROCESSAMENTO) = :DATA_PROCESSAMENTO",
            "CHAVE_ORIGEM=".str_pad($aa2ctipo->TIP_CGC_CPF,14,'0',STR_PAD_LEFT)."&CODIGO_SITUACAO=6&DATA_PROCESSAMENTO=$date")->fetch(true);
            foreach($nfe_controle as $nfe) {
                if ($nfe->DESCRICAO_SITUACAO != 'NF Cancelada'){
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-1001499051648";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja: $aa2ctipo->TIP_CODIGO - $aa2ctipo->TIP_DIGITO $aa2ctipo->TIP_RAZAO\n Chave de acesso: $nfe->CHAVE_ACESSO_NFE \n Status: $nfe->DESCRICAO_SITUACAO.");
                    sleep(5);
                }
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(10);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
        
    }

    public function webServiceVendasNegativas() {
        $date = date("dmy",strtotime("-1 month"));
        $stores1 = array( 5, 8, 11, 12, 13, 14, 15, 17, 19, 25, 27, 32, 33, 35,36, 40, 43, 48, 50, 54, 56, 57, 58, 59, 61, 67, 69, 70, 72, 75, 76, 78, 79, 80, 82, 85, 89, 90);
        foreach ($stores1 as $s1) {
            $aa2cestq = (new AA2CESTQ())->find("GET_DT_ULT_FAT = :GET_DT_ULT_FAT  and substr(GET_COD_LOCAL,1,length(GET_COD_LOCAL)-1) = :GET_COD_LOCAL",
                                                "GET_DT_ULT_FAT=$date&GET_COD_LOCAL=$s1")->fetch(true);
           
            foreach($aa2cestq as $dev) {
                if ($dev->GET_QTD_PEND_VDA > 0) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-608618346";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja: $dev->GET_COD_LOCAL\n Data: ".date("d/m/Y")." \n Codigo produto: $dev->GET_COD_PRODUTO - ".$dev->getAA3CITEM()->GIT_DESCRICAO."\n Quantidade da pendencia: ".$dev->GET_QTD_PEND_VDA.".");
                    sleep(5);
                }
                
            }
        }

        $stores2 = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        foreach ($stores2 as $s2) {
            $aa2cestq = (new AA2CESTQ2())->find("GET_DT_ULT_FAT = :GET_DT_ULT_FAT  and substr(GET_COD_LOCAL,1,length(GET_COD_LOCAL)-1) = :GET_COD_LOCAL",
            "GET_DT_ULT_FAT=$date&GET_COD_LOCAL=$s2")->fetch(true);
            foreach($aa2cestq as $dev) {
                if ($dev->GET_QTD_PEND_VDA > 0) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-608618346";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja: $dev->GET_COD_LOCAL\n Data: ".date("d/m/Y")." \n Codigo produto: $dev->GET_COD_PRODUTO - ".$dev->getAA3CITEM()->GIT_DESCRICAO."\n Quantidade da pendencia: ".$dev->GET_QTD_PEND_VDA.".");
                    sleep(5);
                }
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(12);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function consistencia() {
        if (date("H") >= 6) {
            $item = new Itens();
            $date = date("Y-m-d",strtotime("-1 days"));
            $rmsDate = "1".date("ymd",strtotime("-1 days"));
            
            $stores1 = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
            //   43,54,69,72,76,78,79, 85
            foreach($stores1 as $s1) {
                if ($item->selectStore($s1)->find("dataproc = :data1 and estornado = :estornado","data1=$date&estornado=0",'sum(Valor+JuroCheque+JuroCartao) valor')) {
                    $kw[$s1] = $item->selectStore($s1)->find("dataproc = :data1 and estornado = :estornado","data1=$date&estornado=0",'sum(Valor+JuroCheque+JuroCartao) valor');
                    $rms[$s1] = (new AG1IENSA())->find("ESCHC_DATA3 = :data1 and ESCHC_AGENDA3 = :ESCHC_AGENDA3 and ESCHLJC_CODIGO3 = :ESCHLJC_CODIGO3","data1=$rmsDate&ESCHC_AGENDA3=601&ESCHLJC_CODIGO3=$s1","sum(ENTSAIC_QUANTI_UN*ENTSAIC_PRC_UN) valor")->fetch();
                    $kw[$s1] = $kw[$s1]->fetch();
                    if ($kw[$s1]->valor-round($rms[$s1]->VALOR,2) != 0) {
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "-616394161";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("Loja: $s1\n Data:   $date\n KW ".$kw[$s1]->valor." | RMS ".round($rms[$s1]->VALOR,2));
                        sleep(5);
                    }  
                } else {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-616394161";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Falha na conexão Loja $s1");
                    sleep(5);
                }            
            }
            $notificationsAudit = (new NotificationsAudit())->findById(13);
            $notificationsAudit->last_run = date("Y-m-d H:i:s");
            $notificationsAuditId = $notificationsAudit->save();
    
            // echo $this->view->render("administrator/consistencia", ["id" => "Consistencia | " . SITE,"kw" => $kw, "rms" => $rms, "stores1" => $stores1
            // ]);
    
        }
    }
    
    public function webSericeNCMErrado() {
        $ncms = (new NCM())->find()->fetch(true);
        foreach ($ncms as $ncm) {
            $codigoNCM[$ncm->id] = $ncm->ncm;
        }
        $codigoNCM = implode(",",$codigoNCM);
        $aa1ditem = (new AA1DITEM())->find()->fetch(true);

        foreach ($aa1ditem as $item) {
            if (!$item->NCMValidation && $item->AA3CITEM()) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-639331001";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Codigo ".$item->AA3CITEM()->GIT_COD_ITEM."-".$item->AA3CITEM()->GIT_DIGITO." Descrição ".$item->AA3CITEM()->GIT_DESCRICAO." NCM ".$item->DET_CLASS_FIS." MIX BAHIA");
                sleep(5);
            }
        }

        $aa1ditem = (new AA1DITEM2())->find()->fetch(true);
        foreach ($aa1ditem as $item) {
            if (!$item->NCMValidation && $item->AA3CITEM()) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-639331001";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Codigo ".$item->AA3CITEM()->GIT_COD_ITEM."-".$item->AA3CITEM()->GIT_DIGITO." Descrição ".$item->AA3CITEM()->GIT_DESCRICAO." NCM ".$item->DET_CLASS_FIS." BISPO E DANTAS");
                sleep(5);
            }
        }

        $aa1ditem = (new AA1DITEM4())->find()->fetch(true);
        foreach ($aa1ditem as $item) {
            if (!$item->NCMValidation && $item->AA3CITEM()) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-760317700";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Codigo ".$item->AA3CITEM()->GIT_COD_ITEM."-".$item->AA3CITEM()->GIT_DIGITO." Descrição ".$item->AA3CITEM()->GIT_DESCRICAO." NCM ".$item->DET_CLASS_FIS." UNIMAR");
                sleep(5);
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(18);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function webServiceNFeCancelada () {
        $date1 = "1".date("ymd");
        $date2 = "1".date("ymd",strtotime("-1 days"));
        
        $aa1ctcon = (new AA1CTCON())->getEmiteNota();
        
        $aa1cfisc = (new AA1CFISC())->find("FIS_SITUACAO != '9' AND FIS_DTA_AGENDA between $date1 and $date2 and FIS_OPER in ($aa1ctcon)")->fetch(true);
        foreach($aa1cfisc as $nota) {
            
            if(str_contains($nota->getAccessKey()->DESCRICAO_SITUACAO,'Cancelada')){
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-783568397";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Mix Bahia Origem: $nota->FIS_LOJ_ORG - $nota->FIS_DIG_ORG\n Destino : $nota->FIS_LOJ_DST - $nota->FIS_DIG_DST \nNota fiscal : $nota->FIS_NRO_NOTA\nAgenda: $nota->FIS_OPER\nData: $nota->FIS_DTA_AGENDA");
                sleep(5);
            }
            
        }
        $aa1ctcon = (new AA1CTCON2())->getEmiteNota();
        $aa1cfisc = (new AA1CFISC2())->find("FIS_SITUACAO != '9' AND FIS_DTA_AGENDA between $date1 and $date2 and FIS_OPER in ($aa1ctcon)")->fetch(true);
           
        foreach($aa1cfisc as $nota) {

            if(str_contains($nota->getAccessKey()->DESCRICAO_SITUACAO,'Cancelada')){
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-783568397";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Bispo e Dantas Origem: $nota->FIS_LOJ_ORG - $nota->FIS_DIG_ORG\n Destino : $nota->FIS_LOJ_DST - $nota->FIS_DIG_DST \nNota fiscal : $nota->FIS_NRO_NOTA\nAgenda: $nota->FIS_OPER\nData: $nota->FIS_DTA_AGENDA");
                sleep(5);
            }
        }

        // $aa1cfisc = (new AA1CFISC3())->find('FIS_DTA_AGENDA = :FIS_DTA_AGENDA',"FIS_DTA_AGENDA=$date")->fetch(true);
        // foreach($aa1cfisc as $nota) {
            
        //     if(str_contains($nota->getAccessKey()->DESCRICAO_SITUACAO,'ancelada') && $nota->FIS_SITUACAO != 9){
        //         $logger = new Logger("web");
        //         $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
        //         $tele_channel = "-783568397";
        //         $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
        //         $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
        //         $logger->pushHandler($tele_handler);
        //         $logger->info("Novo Mix Origem: $nota->FIS_LOJ_ORG - $nota->FIS_DIG_ORG\n Destino : $nota->FIS_LOJ_DST - $nota->FIS_DIG_DST \nNota fiscal : $nota->FIS_NRO_NOTA\nAgenda: $nota->FIS_OPER\nData: $date");
        //         sleep(5);
        //     }
        // }

        $aa1cfisc = (new AA1CFISC4())->find('FIS_DTA_AGENDA = :FIS_DTA_AGENDA',"FIS_DTA_AGENDA=$date")->fetch(true);
        foreach($aa1cfisc as $nota) {
            
            if(str_contains($nota->getAccessKey()->DESCRICAO_SITUACAO,'ancelada') && $nota->FIS_SITUACAO != 9){
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-783568397";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Unimar: $nota->FIS_LOJ_ORG - $nota->FIS_DIG_ORG\n Destino : $nota->FIS_LOJ_DST - $nota->FIS_DIG_DST \nNota fiscal : $nota->FIS_NRO_NOTA\nAgenda: $nota->FIS_OPER\nData: $date");
                sleep(5);
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(19);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function webServiceCertificado() {
        $dir    = 'C:/xampp/htdocs/bdtecsolucoes.com.br/source/Certs_audit';
        $files1 = scandir($dir);
        foreach ($files1 as $certs) {
            if (str_contains($certs,'.pfx')) {
                $password = substr($certs,strpos($certs,"_")+1,-4);
                $pfxCertPrivado = __DIR__."/../Certs_audit/$certs";
                $pfxContent = file_get_contents($pfxCertPrivado);
        
                if (!openssl_pkcs12_read($pfxContent, $x509certdata, $password)) {

                } else {
                    
                    $CertPriv   = array();
                    $CertPriv   = openssl_x509_parse(openssl_x509_read($x509certdata['cert']));
        
                    $PrivateKey = $x509certdata['pkey'];
        
                    $pub_key = openssl_pkey_get_public($x509certdata['cert']);
                    $keyData = openssl_pkey_get_details($pub_key);
        
                    $PublicKey  = $keyData['key'];
                    $data2 = date("Y-m-d",$CertPriv['validTo_time_t']);

                    $data_inicial = date("Y-m-d");
                    $data_final = $data2;
                    $diferenca = strtotime($data_final) - strtotime($data_inicial);
                    $dias = floor($diferenca / (60 * 60 * 24));
                    echo "<table><tr><th>$certs<th>".openssl_x509_parse(openssl_x509_read($x509certdata['cert']))['subject']['CN']."</th><th>".$data2."</th></tr></table>";
                    if ($dias <= 30) {                       
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "-645577976";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        // echo "Razão : ".openssl_x509_parse(openssl_x509_read($x509certdata['cert']))['subject']['CN']."\nValidade do Certificado: ".$data2."\n Vence em $dias Dias !<br>";
                        $logger->info("Razão : ".openssl_x509_parse(openssl_x509_read($x509certdata['cert']))['subject']['CN']."\nValidade do Certificado: ".$data2."\n Vence em $dias Dias !");
                        sleep(5);
                        // echo "Razão : ".openssl_x509_parse(openssl_x509_read($x509certdata['cert']))['subject']['CN']."\nValidade do Certificado: ".$data2."\n Vence em $dias Dias !<br>";
                    }
                           
                } 
            }
        }
    }

    public function webServiceCadastroUsuario() {
        $date = "1".date("ymd");
        $aa3citem = (new AA3CITEM())->find("'1'||substr(LPAD(GIT_DAT_ENT_LIN,6,'0'),5,2)||substr(LPAD(GIT_DAT_ENT_LIN,6,'0'),3,2)||substr(LPAD(GIT_DAT_ENT_LIN,6,'0'),1,2) = :GIT_DAT_ENT_LIN",
                                            "GIT_DAT_ENT_LIN=$date")->fetch(true);
        foreach ($aa3citem as $item) {
            $aa3cnvcc = (new AA3CNVCC())->find("NCC_DEPARTAMENTO = :GIT_DEPTO and NCC_SECAO = :GIT_SECAO and NCC_GRUPO = :GIT_GRUPO and NCC_SUBGRUPO = :GIT_SUBGRUPO",
            "GIT_DEPTO=$item->GIT_DEPTO&GIT_SECAO=$item->GIT_SECAO&GIT_GRUPO=$item->GIT_GRUPO&GIT_SUBGRUPO=$item->GIT_SUBGRUPO","NCC_DESCRICAO")->fetch();
            $aa0logit = (new AA0LOGIT())->find("LOGIT_COD_ITEM = :LOGIT_COD_ITEM","LOGIT_COD_ITEM=$item->GIT_COD_ITEM")->order("LOGIT_DAT_ALTER||LOGIT_HOR_ALTER DESC")->fetch(true);
            foreach($aa0logit as $logit) {
                $usuario = str_replace(" ","",$logit->LOGIT_ALT_USUARIO);
            }
            $usuarios = array();//"JULIANA","BDPRISCI", "BDMILENA", "BDWARLEN", "ELINALDO", "IURI", "NATALIE", "VITOR", "RBISPO", "MONALISA", "NAYARA", "TAIANA", "VANDER", "BRUNO"
            $cadastro = (new CadastroExterno())->find("code = :code","code=$item->GIT_COD_ITEM$item->GIT_DIGITO")->count();
            if (in_array($usuario,$usuarios,true) == false && $cadastro == 0) {
                $msg = "Usuario |$usuario|  Mix Bahia\nCodigo: $item->GIT_COD_ITEM-$item->GIT_DIGITO\n Descrição: $item->GIT_DESCRICAO\n Fornecedor ".$item->fornecedor()->TIP_CODIGO."-".$item->fornecedor()->TIP_DIGITO." ".$item->fornecedor()->TIP_RAZAO_SOCIAL."\n Figura ICMS $item->GIT_NAT_FISCAL\n PIS/COFINS $item->GIT_CATEGORIA_ANT\n NCM ".$item->aa1ditem()->DET_CLASS_FIS."\n Subgrupo $aa3cnvcc->NCC_DESCRICAO.";
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-625260820";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info($msg);
                $cadastroExterno = new CadastroExterno();
                $cadastroExterno->code = $item->GIT_COD_ITEM.$item->GIT_DIGITO;
                $cadastroExternoId = $cadastroExterno->save();
                sleep(5);
                
            }
        }

        $aa3citem = (new AA3CITEM2())->find("'1'||substr(LPAD(GIT_DAT_ENT_LIN,6,'0'),5,2)||substr(LPAD(GIT_DAT_ENT_LIN,6,'0'),3,2)||substr(LPAD(GIT_DAT_ENT_LIN,6,'0'),1,2) = :GIT_DAT_ENT_LIN",
                                            "GIT_DAT_ENT_LIN=$date")->fetch(true);
        foreach ($aa3citem as $item) {
            // echo $item->GIT_COD_ITEM."<br>";
            $aa3cnvcc = (new AA3CNVCC2())->find("NCC_DEPARTAMENTO = :GIT_DEPTO and NCC_SECAO = :GIT_SECAO and NCC_GRUPO = :GIT_GRUPO and NCC_SUBGRUPO = :GIT_SUBGRUPO",
            "GIT_DEPTO=$item->GIT_DEPTO&GIT_SECAO=$item->GIT_SECAO&GIT_GRUPO=$item->GIT_GRUPO&GIT_SUBGRUPO=$item->GIT_SUBGRUPO","NCC_DESCRICAO")->fetch();
            $aa0logit = (new AA0LOGIT2())->find("LOGIT_COD_ITEM = :LOGIT_COD_ITEM","LOGIT_COD_ITEM=$item->GIT_COD_ITEM")->order("LOGIT_DAT_ALTER||LOGIT_HOR_ALTER DESC")->fetch(true);
            foreach($aa0logit as $logit) {
                $usuario = str_replace(" ","",$logit->LOGIT_ALT_USUARIO);
            }
            $usuarios = array("");//JULIANA","BDPRISCI", "BDMILENA", "WARLEN", "ELINALDO", "IURI", "NATALIE", "VITOR", "RBISPO", "MONALISA", "NAYARA", "TAIANA", "VANDER", "BRUNO
            $cadastro = (new CadastroExterno())->find("code = :code","code=$item->GIT_COD_ITEM$item->GIT_DIGITO")->count();
            if (in_array($usuario,$usuarios,true) == false && $cadastro == 0) {
                $msg = "Usuario |$usuario| Bispo e Dantas\nCodigo: $item->GIT_COD_ITEM-$item->GIT_DIGITO\n Descrição: $item->GIT_DESCRICAO\n Fornecedor ".$item->fornecedor()->TIP_CODIGO."-".$item->fornecedor()->TIP_DIGITO." ".$item->fornecedor()->TIP_RAZAO_SOCIAL."\n Figura ICMS $item->GIT_NAT_FISCAL\n NCM ".$item->aa1ditem()->DET_CLASS_FIS."\n PIS/COFINS $item->GIT_CATEGORIA_ANT\n Subgrupo $aa3cnvcc->NCC_DESCRICAO.";
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-625260820";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info($msg);
                $cadastroExterno = new CadastroExterno();
                $cadastroExterno->code = $item->GIT_COD_ITEM.$item->GIT_DIGITO;
                $cadastroExternoId = $cadastroExterno->save();
                sleep(5);
            }
        }
        $notificationsAudit = (new NotificationsAudit())->findById(20);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }


    public function webServiceAliquota() {
        $aa3citem1 = (new AA3CITEM())->find()->fetch(true);
        $aa3citem2 = (new AA3CITEM2())->find()->fetch(true);
        // $aa3citem3 = (new AA3CITEM3())->find()->fetch(true);
        $aa3citem4 = (new AA3CITEM4())->find()->fetch(true);
        
        foreach ($aa3citem1 as $item) {
            unset($msg);
            $ncmAliquota = (new NCMALIQUOTA())->find("ncm = :ncm and aliquota = :aliquota","aliquota=".$item->getICMSAliquot."&ncm=".$item->aa1ditem()->DET_CLASS_FIS)->count();
            $ncmPis = (new NCMPIS())->find("ncm = :ncm and aliquota = :aliquota","aliquota=".$item->getPISCOFINSAliquot."&ncm=".$item->aa1ditem()->DET_CLASS_FIS)->count();            

            if ($ncmAliquota == 0 and $item->getICMSAliquot != 18) {
                if ($ncmPis == 0 and $item->getPISCOFINSAliquot != 9.25) {
                    //com o ICMS e o pis
                    $msg = "Produto $item->GIT_COD_ITEM - $item->GIT_DIGITO $item->GIT_DESCRICAO NCM ".$item->aa1ditem()->DET_CLASS_FIS." Aliquota ICMS ".$item->getICMSAliquot."Aliquota PIS/COFINS ".$item->getPISCOFINSAliquot ;
                } else {
                    //com o ICMS sem o pis
                    $msg = "Produto $item->GIT_COD_ITEM - $item->GIT_DIGITO $item->GIT_DESCRICAO NCM ".$item->aa1ditem()->DET_CLASS_FIS." Aliquota ICMS ".$item->getICMSAliquot."Aliquota PIS/COFINS 0." ;
                }                
            } else {
                if ($ncmPis == 0 and $item->getPISCOFINSAliquot != 9.25) {
                    //sem ICMS com o PIS
                    $msg = "Produto $item->GIT_COD_ITEM - $item->GIT_DIGITO $item->GIT_DESCRICAO NCM ".$item->aa1ditem()->DET_CLASS_FIS." Aliquota ICMS 0  Aliquota PIS/COFINS ".$item->getPISCOFINSAliquot ;
                }
            }
            if (isset($msg)) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-637524426";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info($msg);
                sleep(5);
            } 
        }

        

        foreach ($aa3citem2 as $item) {
            unset($msg);
            $ncmAliquota = (new NCMALIQUOTA())->find("ncm = :ncm and aliquota = :aliquota","aliquota=".$item->getICMSAliquot."&ncm=".$item->aa1ditem()->DET_CLASS_FIS)->count();
            $ncmPis = (new NCMPIS())->find("ncm = :ncm and aliquota = :aliquota","aliquota=".$item->getPISCOFINSAliquot."&ncm=".$item->aa1ditem()->DET_CLASS_FIS)->count();
            if ($ncmAliquota == 0) {
            if ($ncmAliquota == 0 and $item->getICMSAliquot != 18) {
                if ($ncmPis == 0 and $item->getPISCOFINSAliquot != 9.25) {
                    //com o ICMS e o pis
                    $msg = "Produto $item->GIT_COD_ITEM - $item->GIT_DIGITO $item->GIT_DESCRICAO NCM ".$item->aa1ditem()->DET_CLASS_FIS." Aliquota ICMS ".$item->getICMSAliquot."Aliquota PIS/COFINS ".$item->getPISCOFINSAliquot ;
                } else {
                    //com o ICMS sem o pis
                    $msg = "Produto $item->GIT_COD_ITEM - $item->GIT_DIGITO $item->GIT_DESCRICAO NCM ".$item->aa1ditem()->DET_CLASS_FIS." Aliquota ICMS ".$item->getICMSAliquot."Aliquota PIS/COFINS 0." ;
                }                
            } else {
                if ($ncmPis == 0 and $item->getPISCOFINSAliquot != 9.25) {
                    //sem ICMS com o PIS
                    $msg = "Produto $item->GIT_COD_ITEM - $item->GIT_DIGITO $item->GIT_DESCRICAO NCM ".$item->aa1ditem()->DET_CLASS_FIS." Aliquota ICMS 0 Aliquota PIS/COFINS ".$item->getPISCOFINSAliquot ;
                }
            }
            if (isset($msg)) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-762604287";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info($msg);
            } 
        

            foreach ($aa3citem3 as $item) {
            
            }

            foreach ($aa3citem4 as $item) {
            
            }
            }
        }
    }

    public function webServiceIndicadores() {
        $date1 = '1'.date('ym').'01';
        $date2 = '1'.date('ym').'31';
        $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 = :ESCHC_AGENDA3 and ESCHC_DATA3 between  :ESCHC_DATA1 and :ESCHC_DATA2","ESCHC_DATA2=$date1&ESCHC_DATA1=$date2&ESCHC_AGENDA3=601","ESCHLJC_CODIGO3 store, ROUND(SUM(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN),2) valor")->group("ESCHLJC_CODIGO3")->fetch(true);
        foreach($ag1iensa as $iensa) {

        }



            foreach ($aggFlsProd as $prod) {
                $days[$prod->dimPer()->DT_COMPL] += $prod->VL_VDA;
                $dates[$prod->dimPer()->DT_COMPL] = $prod->dimPer()->DT_COMPL;
                $month += $prod->VL_VDA;
            }
            foreach ($dates as $day) {
                echo "Venda dia ".$day." R$ ".$days['$day']."<br>";
                // $logger = new Logger("web");
                // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                // $tele_channel = telegram($store);
                // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                // $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                // $logger->pushHandler($tele_handler);
                // $logger->info("Venda dia ".$day." R$ ".$days['$day'].".");
                
            }
            echo "Acumulado das Vendas Mês Atual : R$ $month.<br><br>";
            // $logger = new Logger("web");
            // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            // $tele_channel = telegram($store);
            // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            // $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            // $logger->pushHandler($tele_handler);
            // $logger->info("Acumulado das Vendas Mês Atual : R$ $month.");
            
    }

    public function notificationsAudit() {
        $notifications = (new NotificationsAudit())->find()->fetch(true);
        echo $this->view->render("administrator/notificationsPainel", ["id" => "Notificações | " . SITE,
            "notifications" => $notifications
            ]);
    }

    public function usoConsumoComEstoque() {
        $aa3citem1 = (new AA3CITEM())->find("GIT_SECAO = :GIT_SECAO","GIT_SECAO=15")->fetch(true);
        $aa3citem2 = (new AA3CITEM2())->find("GIT_SECAO = :GIT_SECAO","GIT_SECAO=15")->fetch(true);
        $a = 0;
        foreach($aa3citem1 as $item) {
            $a++;
            $aa2cestq = (new AA2CESTQ())->find("substr(GET_COD_LOCAL,0,length(GET_COD_LOCAL)-1) in ( 5, 8, 11, 12, 13, 14, 15, 17, 19, 25, 27, 31, 32, 33, 35, 36, 40, 48, 50, 53, 56 , 57, 58, 59, 61, 70, 75, 80,81) and substr(GET_COD_PRODUTO,0,length(GET_COD_PRODUTO)-1) = $item->GIT_COD_ITEM")->order("GET_COD_LOCAL")->fetch(true);
            foreach ($aa2cestq as $estoque) {
                if ($estoque->GET_ESTOQUE > 0 ) {
                    $store = (new Store())->find("code = :code","code=".substr($estoque->GET_COD_LOCAL,0,strlen($estoque->GET_COD_LOCAL)-1))->count();
                    
                    if ($store == 0 ){

                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = '-697514302';
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("LOJA: $estoque->GET_COD_LOCAL \nITEM: $item->GIT_COD_ITEM-$item->GIT_DIGITO $item->GIT_DESCRICAO\n Estoque: $estoque->GET_ESTOQUE");
                    } else {
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = '-533747624';
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("LOJA: $estoque->GET_COD_LOCAL \nITEM: $item->GIT_COD_ITEM-$item->GIT_DIGITO $item->GIT_DESCRICAO\n Estoque: $estoque->GET_ESTOQUE");
                    }
                    // echo "LOJA: $aa2cestq->GET_COD_LOCAL \nITEM: $item->GIT_COD_ITEM-$item->GIT_DIGITO $item->GIT_DESCRICAO\n Estoque: $aa2cestq->GET_ESTOQUE.<br>";
                    
                }
            }            
        }
        foreach($aa3citem2 as $item) {
            $a++;
            $aa2cestq = (new AA2CESTQ2())->find("substr(GET_COD_LOCAL,0,length(GET_COD_LOCAL)-1) in ( 83, 87, 91, 20, 21, 84, 86, 95, 102, 23, 93, 101, 18, 49, 94, 104, 105, 10, 106) and substr(GET_COD_PRODUTO,0,length(GET_COD_PRODUTO)-1) = $item->GIT_COD_ITEM")->order("GET_COD_LOCAL")->fetch(true);
            foreach ($aa2cestq as $estoque) {
                if ($estoque->GET_ESTOQUE > 0 ) {
                    $store = (new Store())->find("code = :code","code=".substr($estoque->GET_COD_LOCAL,0,strlen($estoque->GET_COD_LOCAL)-1))->count();
                    
                    if ($store == 0 ){

                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = '-697514302';
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("LOJA: $estoque->GET_COD_LOCAL \nITEM: $item->GIT_COD_ITEM-$item->GIT_DIGITO $item->GIT_DESCRICAO\n Estoque: $estoque->GET_ESTOQUE");
                    } else {
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = '-533747624';
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("LOJA: $estoque->GET_COD_LOCAL \nITEM: $item->GIT_COD_ITEM-$item->GIT_DIGITO $item->GIT_DESCRICAO\n Estoque: $estoque->GET_ESTOQUE");
                    }
                    // echo "LOJA: $aa2cestq->GET_COD_LOCAL \nITEM: $item->GIT_COD_ITEM-$item->GIT_DIGITO $item->GIT_DESCRICAO\n Estoque: $aa2cestq->GET_ESTOQUE.<br>";
                    
                }
            }            
        }
    }

    public function manifestationAudit() {
        
        $nfes = (new NfSefaz())->find("TIMESTAMPDIFF(DAY , substr(emission_date,1,10), NOW()) = :emission_date","emission_date=20")->order("store_id")->fetch(true);
        foreach ($nfes as $nfe) {
            if ($nfe->getUF == 'BA' && $nfe->manifestation == '' && !$nfe->getPostedOrNot && $nfe->store_id != 299 && $nfe->store_id != 531 &&  $nfe->store_id != 450) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = '-702441424';
                // telegram($prod->CD_FIL);
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $nfe->store_id\nEmissão $nfe->emission_date\nFornecedor $nfe->getFornecedor\nNúmero da Nota $nfe->nfe_number\n Chave de acesso $nfe->access_key\nEstado $nfe->getUF\nNota fiscal com 20 dias a partir da emissão sem manifestação de operação não realizada e nem postagem no portal.");
                sleep(5);
            }
        }

        $nfes = (new NfSefaz())->find("TIMESTAMPDIFF(DAY , substr(emission_date,1,10), NOW()) = :emission_date","emission_date=35")->order("store_id")->fetch(true);
        foreach ($nfes as $nfe) {
            if ($nfe->getUF != 'BA' && $nfe->manifestation == '' && !$nfe->getPostedOrNot) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = '-602947829';
                // telegram($prod->CD_FIL);
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $nfe->store_id\nEmissão $nfe->emission_date\nFornecedor $nfe->getFornecedor\nNúmero da Nota $nfe->nfe_number\n Chave de acesso $nfe->access_key\nEstado $nfe->getUF\nNota fiscal com 35 dias a partir da emissão sem manifestação de operação não realizada e nem postagem no portal.");
                sleep(5);
            }
        }
    }

    public function nfeUpdateUsers() {
        $users = (new User())->find("class= :class and id in (20000287,298, 152,20000394,20000313,20000395,20000401,20000335,20000216,20000286,309,20000239, 20000147, 20000265, 20000264, 179, 20000268, 193, 205, 153, 286, 181, 20000267, 301,309)","class=A")->fetch(true);
        $nfReceipt = new NfReceipt();
        foreach ($users as $user) {
            $count = $nfReceipt->find("substr(updated_at,12,2) = substr(CURRENT_TIME(),1,2) - 1 and substr(updated_at,1,10) = CURDATE() and status_id = :status_id and operator_id = :operator_id",
            "status_id=3&operator_id=$user->id")->count();
            $countCD = $nfReceipt->find("type_id = :type_id and substr(updated_at,12,2) = substr(CURRENT_TIME(),1,2) - 1 and substr(updated_at,1,10) = CURDATE() and status_id = :status_id and operator_id = :operator_id",
            "status_id=3&operator_id=$user->id&type_id=4")->count();
            $cargoArrival = (new NfEvent())->find("final_user_id = $user->id and situation = 'F' and status_id = 10 and substr(updated_at,12,2) = substr(CURRENT_TIME(),1,2) - 1 and substr(updated_at,1,10) = CURDATE()")->count();
            if ($cargoArrival + $count < 10) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = '-651694898';
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Operador $user->name Atualizou $count Notas sendo $countCD notas do CD e Alocou $cargoArrival notas para o coletor entre".(date("H")-1).":00 e as ".(date("H")-1).":59");
                sleep(5);
            }
            
        }
        

    }

    public function vendasSemUltimaEntrada() {
        $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 in (select rms.dateto_rms7(sysdate - (1 + 2/24)) from dual)")->order("ESCHLJC_CODIGO3")->fetch(true);
        foreach ($ag1iensa as $iensa) {
            if ($iensa->ultimaEntrada == 0 && $iensa->getItemRegister()->GIT_TIPO_PRO != 3) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = '-799323413';
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $iensa->ESCHLJC_CODIGO3\n Produto ".$iensa->getItemRegister()->GIT_COD_ITEM."-".$iensa->getItemRegister()->GIT_DIGITO." ".$iensa->getItemRegister()->GIT_DESCRICAO."\nPreço ".round($iensa->ENTSAIC_PRC_UN,2).".");
                sleep(5);
            }
        }
    }

    public function nfeAutorizada() {
        $nfes = (new NfReceipt())->find("nf_number = 223660 and status_id = :status_id and substr(updated_at,1,10) = CURDATE() and access_key > 10","status_id=3")->fetch(true);
        $ok = 0;
        foreach ($nfes as $nfe) {
           if ($nfe->nfSefaz()->autorized  == "Rejeicao: Evento de Ciencia da Operacao para NFe cancelada ou denegada") {
                $nfeSEFAZ = (new NfSefaz())->find("access_key = :access_key","access_key=$nfe->access_key")->fetch();
                $nfeSEFAZ->autorized = 'C';
                $nfeSEFAZId = $nfeSEFAZ->save();
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = '-793742741';
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja: $nfe->store_id\nNúmero da nota: $nfe->nf_number\n Fornecedor ".$nfe->getFornecedor."\n Chave de Acesso : ". $nfe->nfSefaz()->access_key." CANCELADA.");
                sleep(5);
                $ok = 1;
           } else {
                $nfeSEFAZ = (new NfSefaz())->find("access_key = :access_key","access_key=$nfe->access_key")->fetch();
                $nfeSEFAZ->autorized = 'A';
                $nfeSEFAZId = $nfeSEFAZ->save();
           }
        }
        if ($ok == 0) {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = '-793742741';
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            $logger->pushHandler($tele_handler);
            $logger->info("Verificação realizada as ".date('d/m/Y H:i:s')." nenhum caso encontrado.");
        }
        
    }

    public function cuponsPendetenProtocolo() {
        if (date("H") >= 6) {
            $item = new Itens();
            $date = date("Y-m-d",strtotime("-1 days"));
            $rmsDate = "1".date("ymd",strtotime("-1 days"));
            
            $stores1 = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
            //   43,54,69,72,76,78,79, 85
            foreach($stores1 as $s1) {
                if ($item->selectStoreNfce($s1)->find("protocolo_nfe = '' and dataproc between  CURDATE() - INTERVAL 2 DAY and CURDATE() and nroloja = $s1")) {
                    $kw = $item->selectStore($s1)->find("protocolo_nfe = '' and dataproc between  CURDATE() - INTERVAL 2 DAY and CURDATE() and nroloja = $s1")->fetch(true);
                    foreach ($kw as $nfce) {
                        echo "Loja: $s1\nData do processamento $nfce->DataProc\nPDV $nfce->Pdv\nNúmero do Cupom $nfce->NroCupom\nNúmero Nfce $nfce->numero_nfe\nStatus $nfce->Status\nChave de Acesso $nfce->chave_nfe<br>";
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "-616394161";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("Loja: $s1\nData do processamento $nfce->DataProc\nPDV $nfce->Pdv\nNúmero do Cupom $nfce->NroCupom\nNúmero Nfce $nfce->numero_nfe\nStatus $nfce->Status\nChave de Acesso $nfce->chave_nfe" );
                        sleep(5);
                    } 
                } else {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-616394161";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Falha na conexão Loja $s1");
                    sleep(5);
                }            
            }
            $notificationsAudit = (new NotificationsAudit())->findById(13);
            $notificationsAudit->last_run = date("Y-m-d H:i:s");
            $notificationsAuditId = $notificationsAudit->save();
    
            // echo $this->view->render("administrator/consistencia", ["id" => "Consistencia | " . SITE,"kw" => $kw, "rms" => $rms, "stores1" => $stores1
            // ]);
    
        }
    }

    public function nfAtualizadaSoRetaguarda () {
        $aa1ctcon = (new AA1CTCON())->getEntradas4;

        $stores = (new Store())->find("group_id in (1,10)")->fetch(true);
        foreach($stores as $store) {
            $storeArray[$store->code] = $store->code;
        }
        $store = implode(", ",$storeArray);
        $store = 
        $aa1cfisc = (new AA1CFISC())->find("FIS_USUARIO_INC IN ('HENRIQUE','JULIANA','BDPRISCI', 'BDMILENA', 'BDWARLEN', 'ELINALDO', 'IURI', 'NATALIE', 'VITOR', 'RBISPO', 'MONALISA', 'NAYARA', 'TAIANA', 'VANDER', 'BRUNO') AND FIS_OPER in (1,3,4,5,7,8,9,10,11,12,14,15,16,18,19,20,24,25,27,34,35,38,60,64,65,66,70,72,82,86,87,88,89,95,100,125,128,143,145,149,199,400,430,499,500,700,900) and FIS_LOJ_DST IN (53, 8, 13, 14, 17, 27, 31, 32, 35, 40, 56, 57, 58, 59, 61, 62, 67, 69, 72, 75, 76, 50, 70, 78, 79, 5, 80, 85, 82, 81) AND FIS_ENT_SAI = 'E' and FIS_DTA_AGENDA in (SELECT RMS.DATETO_RMS7(SYSDATE) from dual)")->order("FIS_LOJ_DST")->fetch(true);


        foreach($aa1cfisc as $cfisc) {
 
            if ($cfisc->getAccessKey) {
                       
                if ($cfisc->getAccessKey != $cfisc->getNfReceipt->access_key) {
                    $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "-615410221";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("Loja $cfisc->FIS_LOJ_DST \n Nota fiscal $cfisc->FIS_NRO_NOTA \n Data da agenda $cfisc->FIS_DTA_AGENDA \n Codigo Fornecedor ".$cfisc->getProvider()->TIP_CODIGO.$cfisc->getProvider()->TIP_DIGITO." \n Fornecedor ". $cfisc->getProvider()->TIP_RAZAO_SOCIAL."\n Lançada por $cfisc->FIS_USUARIO_INC NFe não postada.");
                        sleep(5);
                }
                
            }
            
        }
    }

    public function requestLojasImplantadas() {
        $request = (new NfReceipt());
        $requests = $request->find("store_id = 108 and TIMESTAMPDIFF(HOUR , created_at, NOW()) >= 2 and status_id != 3 and status_id != 7 and status_id != 8 and status_id != 16")->fetch(true);


        foreach($requests as $r){
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = "-743817980";
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            $logger->pushHandler($tele_handler);
            $logger->info("Loja $r->store_id \n Nota fiscal $r->nf_number \n Postagem $r->created_at \n Status $r->getStatus \n Operador  $r->getOperator");
            sleep(5);
        }

        
    }

    public function getErrorGerencial() {
        // $aa1tcon = (new AA1CTCON3())->getEntradas4;
        // $aa1cfisc = (new AA1CFISC3())->find("FIS_LOJ_DST in (29,39,45,53,64,65,71,72,73) and '1'||FIS_DT_PROC BETWEEN 1220501 and 1220531 AND FIS_ENT_SAI = 'E' AND FIS_OPER != 499 AND FIS_OPER IN (1,9,11)")->fetch(true);
        // foreach($aa1cfisc as $cfisc) {
        //     if($cfisc->ag1iensa > 0) {
        //         $logger = new Logger("web");
        //         $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
        //         $tele_channel = "-654246728";
        //         $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
        //         $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
        //         $logger->pushHandler($tele_handler);
        //         $logger->info("Loja $cfisc->FIS_LOJ_DST Novo Mix \n Nota fiscal $cfisc->FIS_NRO_NOTA \n Data Agenda $cfisc->dataAgenda \n Agenda Capa $cfisc->FIS_OPER \n Operador $cfisc->FIS_USUARIO_INC");
        //         sleep(5);
        //     }
        // }
        $aa1tcon = (new AA1CTCON())->getEntradas4;
        $aa1cfisc = (new AA1CFISC())->find("'1'||FIS_DT_PROC = (SELECT RMS.DATETO_RMS7(SYSDATE) FROM DUAL) AND FIS_ENT_SAI = 'E' AND FIS_OPER != 499 AND FIS_OPER IN (1,9,11)")->fetch(true);
        foreach($aa1cfisc as $cfisc) {
            if($cfisc->ag1iensa > 0) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-667194338";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $cfisc->FIS_LOJ_DST Mix Bahia \n Nota fiscal $cfisc->FIS_NRO_NOTA \n Data Agenda $cfisc->dataAgenda \n Agenda Capa $cfisc->FIS_OPER \n Operador $cfisc->FIS_USUARIO_INC");
                sleep(5);
            }
        }

        $aa1tcon = (new AA1CTCON2())->getEntradas4;
        $aa1cfisc = (new AA1CFISC2())->find("'1'||FIS_DT_PROC = (SELECT RMS.DATETO_RMS7(SYSDATE) FROM DUAL) AND FIS_ENT_SAI = 'E' AND FIS_OPER != 499 AND FIS_OPER IN (1,9,11)")->fetch(true);
        foreach($aa1cfisc as $cfisc) {
            if($cfisc->ag1iensa > 0) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-667194338";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $cfisc->FIS_LOJ_DST Bispo e Dantas\n Nota fiscal $cfisc->FIS_NRO_NOTA \n Data Agenda $cfisc->dataAgenda \n Agenda Capa $cfisc->FIS_OPER \n Operador $cfisc->FIS_USUARIO_INC");
                sleep(5);
            }
        }
    }

    public function fornecedoresNotasItens() {
        $nfSefaz = (new NFSefaz())->find()->fetch(true);
        foreach ($nfSefaz as $nf){
            if (strlen($nf->cnpj) > 0) {
                $fornecedores[$nf->cnpj] = $nf->fornecedor();
                $notas[$nf->cnpj] += 1;
            }
        }
        echo "<table>
                    <thead>
                        <tr>
                            <th>CNPJ</th>
                            <th>Razão Social</th>
                            <th>Nome Fantasia</th>
                            <th>Quantidade</th>
                        </tr>
                    </thead>";
        foreach ($fornecedores as $fornecedor) {
            
            echo "  <tbody>
                        <tr>
                            <td>$fornecedor->CNPJ</td>
                            <td>$fornecedor->xNome</td>
                            <td>$fornecedor->xFant</td>
                            <td>".$notas[$fornecedor->CNPJ]."<td>
                        </tr>
                    </tbody>";
            

        }
        echo "</table>";
              
    }

    public function transferenceAudito() {
        // 1152/1409
        // 5152/5409
        if (date("d") <= 9) {
            $date1 = "1".date("ym",strtotime("-1 month")).'01';
            $date2 = "1".date('ym',strtotime("-1 month"))."31";
        } else {
            $date1 = "1".date("ym").'01';
            $date2 = "1".date('ym')."31";
        }
        
        
        $aa1cfisc = (new AA1CFISC())->find("FIS_SITUACAO != '9' and FIS_OPER IN (43,46,53,146,191) and FIS_DTA_AGENDA between '$date1' and '$date2'")->fetch(true);
        foreach ($aa1cfisc as $cfisc) {
            $aa1cfisc_  = (new AA1CFISC())->find("FIS_OPER IN (7,13,37,143,145) and FIS_NRO_NOTA = $cfisc->FIS_NRO_NOTA and FIS_LOJ_ORG = $cfisc->FIS_LOJ_ORG")->fetch();
            if (!$aa1cfisc_) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-735673381";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $cfisc->FIS_LOJ_ORG \n Nota fiscal $cfisc->FIS_NRO_NOTA \n Data Agenda $cfisc->dataAgenda \n Agenda $cfisc->FIS_OPER");
                sleep(5);
            }
        }
        $aa1cfisc = (new AA1CFISC2())->find("FIS_SITUACAO != '9' and FIS_LOJ_ORG != 108 and FIS_OPER IN (43,46,53,146,191) and FIS_DTA_AGENDA between '$date1' and '$date2'")->fetch(true);
        foreach ($aa1cfisc as $cfisc) {
            $aa1cfisc_  = (new AA1CFISC2())->find("FIS_OPER IN (7,13,37,143,145) and FIS_NRO_NOTA = $cfisc->FIS_NRO_NOTA and FIS_LOJ_ORG = $cfisc->FIS_LOJ_ORG")->fetch();
            if (!$aa1cfisc_) {

                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-735673381";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $cfisc->FIS_LOJ_ORG \n Nota fiscal $cfisc->FIS_NRO_NOTA \n Data Agenda $cfisc->dataAgenda \n Agenda $cfisc->FIS_OPER");
                sleep(5);
            }
        }
    }

    public function consultaCadastro() {
        $reference = $_POST['reference'];
        $EAN = $_POST['EAN'];
        $CNPJ = $_POST['CNPJ']; 
        if ($reference != '' || $EAN != '' || $CNPJ != '') {
            $i = 1;
            if ($reference != '') {
                $params['reference'] = "FORITE_REFERENCIA like '%$reference%'";
                $params2['reference'] = "GIT_REFERENCIA like '%$reference%'";
            }
            //mb
            if ($CNPJ != '') {
                $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF = $CNPJ")->fetch();
                if ($aa2ctipo) {
                    $params['CNPJ'] = "FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO";
                    $params2['CNPJ'] = "GIT_COD_FOR = $aa2ctipo->TIP_CODIGO";
                } 
            }          
            if ($EAN != '') {
                $aa3ccean = (new AA3CCEAN())->find("EAN_COD_EAN = $EAN")->fetch();
                if ($aa3ccean) {
                    $params['EAN'] = "FORITE_COD_ITEM = ".substr($aa3ccean->EAN_COD_PRO_ALT,0,strlen($aa3ccean->EAN_COD_PRO_ALT)-1);
                    $params2['EAN'] = "GIT_COD_ITEM = ".substr($aa3ccean->EAN_COD_PRO_ALT,0,strlen($aa3ccean->EAN_COD_PRO_ALT)-1);
                }            
            }
            if (is_array($params)) {
                $params = implode(" and ",$params);
                $aa1forit = (new AA1FORIT())->find($params)->fetch(true);


                if (!empty($aa1forit)) {
                    foreach ($aa1forit as $forit) {
                        foreach ($forit->aa3ccean() as $aa3ccean) {
                            $registro['CNPJ'][$i] = $forit->aa2ctipo()->TIP_CGC_CPF;
                            $registro['razao'][$i] = $forit->aa2ctipo()->TIP_RAZAO_SOCIAL;
                            $registro['fantasia'][$i] = $forit->aa2ctipo()->TIP_NOME_FANTASIA;
                            $registro['codigo'][$i] = $forit->aa3citem()->GIT_COD_ITEM.'-'.$forit->aa3citem()->GIT_DIGITO;
                            $registro['referencia'][$i] = $forit->FORITE_REFERENCIA;
                            $registro['descricao'][$i] = $forit->aa3citem()->GIT_DESCRICAO;
                            $registro['embFornecedor'][$i] = $forit->aa3citem()->GIT_EMB_FOR;
                            $registro['embVenda'][$i] = $aa3ccean->EAN_EMB_VENDA;
                            $registro['EAN'][$i] = $aa3ccean->EAN_COD_EAN;
                            $registro['base'][$i] = 'Mix Bahia';
                            $i++;
                        }   
                    }
                } else {
                    $params2 = implode(" and ",$params2);
                    $aa3citem = (new AA3CITEM())->find($params2)->fetch(true);
                    
                    if (is_array($aa3citem)) {
                        foreach ($aa3citem as $citem) {
                            foreach ($citem->aa3ccean() as $aa3ccean) {
                                $registro['CNPJ'][$i] = $citem->aa2ctipo()->TIP_CGC_CPF;
                                $registro['razao'][$i] = $citem->aa2ctipo()->TIP_RAZAO_SOCIAL;
                                $registro['fantasia'][$i] = $citem->aa2ctipo()->TIP_NOME_FANTASIA;
                                $registro['codigo'][$i] = $citem->GIT_COD_ITEM.'-'.$citem->GIT_DIGITO;
                                $registro['referencia'][$i] = $citem->GIT_REFERENCIA;
                                $registro['descricao'][$i] = $citem->GIT_DESCRICAO;
                                $registro['embFornecedor'][$i] = $citem->GIT_EMB_FOR;
                                $registro['embVenda'][$i] = $aa3ccean->EAN_EMB_VENDA;
                                $registro['EAN'][$i] = $aa3ccean->EAN_COD_EAN;
                                $registro['base'][$i] = 'Mix Bahia';
                                $i++;
                            }   
                        }
                    }
                }
            }
            
            $params = null;
            $params2 = null;
            //bd
            if ($CNPJ != '') {
                $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF = $CNPJ")->fetch();
                if ($aa2ctipo) {
                    $params['CNPJ'] = "FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO";
                    $params2['CNPJ'] = "GIT_COD_FOR = $aa2ctipo->TIP_CODIGO";
                } 
            }          
            if ($EAN != '') {
                $aa3ccean = (new AA3CCEAN2())->find("EAN_COD_EAN = $EAN")->fetch();
                if ($aa3ccean) {
                    $params['EAN'] = "FORITE_COD_ITEM = ".substr($aa3ccean->EAN_COD_PRO_ALT,0,strlen($aa3ccean->EAN_COD_PRO_ALT)-1);
                    $params2['EAN'] = "GIT_COD_ITEM = ".substr($aa3ccean->EAN_COD_PRO_ALT,0,strlen($aa3ccean->EAN_COD_PRO_ALT)-1);
                }            
            }

            if (is_array($params)) {
                $params = implode(" and ",$params);
                $aa1forit = (new AA1FORIT2())->find($params)->fetch(true);
                if (!empty($aa1forit)) {
                    foreach ($aa1forit as $forit) {
                        foreach ($forit->aa3ccean() as $aa3ccean) {
                            $registro['CNPJ'][$i] = $forit->aa2ctipo()->TIP_CGC_CPF;
                            $registro['razao'][$i] = $forit->aa2ctipo()->TIP_RAZAO_SOCIAL;
                            $registro['fantasia'][$i] = $forit->aa2ctipo()->TIP_NOME_FANTASIA;
                            $registro['codigo'][$i] = $forit->aa3citem()->GIT_COD_ITEM.'-'.$forit->aa3citem()->GIT_DIGITO;
                            $registro['referencia'][$i] = $forit->FORITE_REFERENCIA;
                            $registro['descricao'][$i] = $forit->aa3citem()->GIT_DESCRICAO;
                            $registro['embFornecedor'][$i] = $forit->aa3citem()->GIT_EMB_FOR;
                            $registro['embVenda'][$i] = $aa3ccean->EAN_EMB_VENDA;
                            $registro['EAN'][$i] = $aa3ccean->EAN_COD_EAN;
                            $registro['base'][$i] = 'Bispo & Dantas';
                            $i++;
                        }   
                    }
                } else {
                    $params2 = implode(" and ",$params2);
                    $aa3citem = (new AA3CITEM2())->find($params2)->fetch(true);
                    
                    if (is_array($aa3citem)) {
                        foreach ($aa3citem as $citem) {
                            foreach ($citem->aa3ccean() as $aa3ccean) {
                                $registro['CNPJ'][$i] = $citem->aa2ctipo()->TIP_CGC_CPF;
                                $registro['razao'][$i] = $citem->aa2ctipo()->TIP_RAZAO_SOCIAL;
                                $registro['fantasia'][$i] = $citem->aa2ctipo()->TIP_NOME_FANTASIA;
                                $registro['codigo'][$i] = $citem->GIT_COD_ITEM.'-'.$citem->GIT_DIGITO;
                                $registro['referencia'][$i] = $citem->GIT_REFERENCIA;
                                $registro['descricao'][$i] = $citem->GIT_DESCRICAO;
                                $registro['embFornecedor'][$i] = $citem->GIT_EMB_FOR;
                                $registro['embVenda'][$i] = $aa3ccean->EAN_EMB_VENDA;
                                $registro['EAN'][$i] = $aa3ccean->EAN_COD_EAN;
                                $registro['base'][$i] = 'Mix Bahia';
                                $i++;
                            }   
                        }
                    }
                }
            }
            
            $params = null;
            $params2 = null;
            // nm
            // if ($CNPJ != '') {
            //     $aa2ctipo = (new AA2CTIPO3())->find("TIP_CGC_CPF = $CNPJ")->fetch();
            //     if ($aa2ctipo) {
            //         $params['CNPJ'] = "FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO";
            //         $params2['CNPJ'] = "GIT_COD_FOR = $aa2ctipo->TIP_CODIGO";
            //     } 
            // }          
            // if ($EAN != '') {
            //     $aa3ccean = (new AA3CCEAN3())->find("EAN_COD_EAN = $EAN")->fetch();
            //     if ($aa3ccean) {
            //         $params['EAN'] = "FORITE_COD_ITEM = ".substr($aa3ccean->EAN_COD_PRO_ALT,0,strlen($aa3ccean->EAN_COD_PRO_ALT)-1);
            //         $params2['EAN'] = "GIT_COD_ITEM = ".substr($aa3ccean->EAN_COD_PRO_ALT,0,strlen($aa3ccean->EAN_COD_PRO_ALT)-1);
            //     }            
            // }
            // if (is_array($params)) {
            //     $params = implode(" and ",$params);
            //     $aa1forit = (new AA1FORIT3())->find($params)->fetch(true);
            //     if (!empty($aa1forit)) {
            //         foreach ($aa1forit as $forit) {
            //             foreach ($forit->aa3ccean() as $aa3ccean) {
            //                 $registro['CNPJ'][$i] = $forit->aa2ctipo()->TIP_CGC_CPF;
            //                 $registro['razao'][$i] = $forit->aa2ctipo()->TIP_RAZAO_SOCIAL;
            //                 $registro['fantasia'][$i] = $forit->aa2ctipo()->TIP_NOME_FANTASIA;
            //                 $registro['codigo'][$i] = $forit->aa3citem()->GIT_COD_ITEM.'-'.$forit->aa3citem()->GIT_DIGITO;
            //                 $registro['referencia'][$i] = $forit->FORITE_REFERENCIA;
            //                 $registro['descricao'][$i] = $forit->aa3citem()->GIT_DESCRICAO;
            //                 $registro['embFornecedor'][$i] = $forit->aa3citem()->GIT_EMB_FOR;
            //                 $registro['embVenda'][$i] = $aa3ccean->EAN_EMB_VENDA;
            //                 $registro['EAN'][$i] = $aa3ccean->EAN_COD_EAN;
            //                 $registro['base'][$i] = 'Novo Mix';
            //                 $i++;
            //             }   
            //         }
            //     } else {
            //         $params2 = implode(" and ",$params2);
            //         $aa3citem = (new AA3CITEM())->find($params2)->fetch(true);
                    
            //         if (is_array($aa3citem)) {
            //             foreach ($aa3citem as $citem) {
            //                 foreach ($citem->aa3ccean() as $aa3ccean) {
            //                     $registro['CNPJ'][$i] = $citem->aa2ctipo()->TIP_CGC_CPF;
            //                     $registro['razao'][$i] = $citem->aa2ctipo()->TIP_RAZAO_SOCIAL;
            //                     $registro['fantasia'][$i] = $citem->aa2ctipo()->TIP_NOME_FANTASIA;
            //                     $registro['codigo'][$i] = $citem->GIT_COD_ITEM.'-'.$citem->GIT_DIGITO;
            //                     $registro['referencia'][$i] = $citem->GIT_REFERENCIA;
            //                     $registro['descricao'][$i] = $citem->GIT_DESCRICAO;
            //                     $registro['embFornecedor'][$i] = $citem->GIT_EMB_FOR;
            //                     $registro['embVenda'][$i] = $aa3ccean->EAN_EMB_VENDA;
            //                     $registro['EAN'][$i] = $aa3ccean->EAN_COD_EAN;
            //                     $registro['base'][$i] = 'Mix Bahia';
            //                     $i++;
            //                 }   
            //             }
            //         }
            //     }
            // }

            $params = null;
            $params2 = null;
            //unimar
            if ($CNPJ != '') {
                $aa2ctipo = (new AA2CTIPO4())->find("TIP_CGC_CPF = $CNPJ")->fetch();
                if ($aa2ctipo) {
                    $params['CNPJ'] = "FORITE_COD_FORN = $aa2ctipo->TIP_CODIGO";
                    $params2['CNPJ'] = "GIT_COD_FOR = $aa2ctipo->TIP_CODIGO";
                } 
            }          
            if ($EAN != '') {
                $aa3ccean = (new AA3CCEAN4())->find("EAN_COD_EAN = $EAN")->fetch();
                if ($aa3ccean) {
                    $params['EAN'] = "FORITE_COD_ITEM = ".substr($aa3ccean->EAN_COD_PRO_ALT,0,strlen($aa3ccean->EAN_COD_PRO_ALT)-1);
                    $params2['EAN'] = "GIT_COD_ITEM = ".substr($aa3ccean->EAN_COD_PRO_ALT,0,strlen($aa3ccean->EAN_COD_PRO_ALT)-1);
                }            
            }
            

            if (is_array($params)) {
                $params = implode(" and ",$params);
                $aa1forit = (new AA1FORIT4())->find($params)->fetch(true);
                if (!empty($aa1forit)) {
                    foreach ($aa1forit as $forit) {
                        foreach ($forit->aa3ccean() as $aa3ccean) {
                            $registro['CNPJ'][$i] = $forit->aa2ctipo()->TIP_CGC_CPF;
                            $registro['razao'][$i] = $forit->aa2ctipo()->TIP_RAZAO_SOCIAL;
                            $registro['fantasia'][$i] = $forit->aa2ctipo()->TIP_NOME_FANTASIA;
                            $registro['codigo'][$i] = $forit->aa3citem()->GIT_COD_ITEM.'-'.$forit->aa3citem()->GIT_DIGITO;
                            $registro['referencia'][$i] = $forit->FORITE_REFERENCIA;
                            $registro['descricao'][$i] = $forit->aa3citem()->GIT_DESCRICAO;
                            $registro['embFornecedor'][$i] = $forit->aa3citem()->GIT_EMB_FOR;
                            $registro['embVenda'][$i] = $aa3ccean->EAN_EMB_VENDA;
                            $registro['EAN'][$i] = $aa3ccean->EAN_COD_EAN;
                            $registro['base'][$i] = 'Unimar';
                            $i++;
                        }   
                    }
                } else {
                    $params2 = implode(" and ",$params2);
                    $aa3citem = (new AA3CITEM4())->find($params2)->fetch(true);
                    
                    if (is_array($aa3citem)) {
                        foreach ($aa3citem as $citem) {
                            foreach ($citem->aa3ccean() as $aa3ccean) {
                                $registro['CNPJ'][$i] = $citem->aa2ctipo()->TIP_CGC_CPF;
                                $registro['razao'][$i] = $citem->aa2ctipo()->TIP_RAZAO_SOCIAL;
                                $registro['fantasia'][$i] = $citem->aa2ctipo()->TIP_NOME_FANTASIA;
                                $registro['codigo'][$i] = $citem->GIT_COD_ITEM.'-'.$citem->GIT_DIGITO;
                                $registro['referencia'][$i] = $citem->GIT_REFERENCIA;
                                $registro['descricao'][$i] = $citem->GIT_DESCRICAO;
                                $registro['embFornecedor'][$i] = $citem->GIT_EMB_FOR;
                                $registro['embVenda'][$i] = $aa3ccean->EAN_EMB_VENDA;
                                $registro['EAN'][$i] = $aa3ccean->EAN_COD_EAN;
                                $registro['base'][$i] = 'Mix Bahia';
                                $i++;
                            }   
                        }
                    }
                }
            }

            
            // $registro = json_encode($registro);
            echo $this->view->render("administrator/cadastroProdutos", ["id" => "Cadastro Produtos | " . SITE, "registros" => $registro, "i" => $i
            ]);
        } else {
            echo $this->view->render("administrator/cadastroProdutos", ["id" => "Cadastro Produtos | " . SITE
            ]);
        }
               
    }

    public function teste() {
        $id = $_GET['id'];
        $nfReceipt = (new NfReceipt())->findById($id);//doce 211312 fruta 211758

        $count = 1;
        foreach ($nfReceipt->getItems as $det) {
            foreach ($nfReceipt->aa2detnt as $aa2detnt) {
                if (in_array($det->prod->cProd, $aa2detnt->references())) {
                    $registros['codigo'][$count] = $det->prod->cProd;
                    $registros['ean'][$count] = $det->prod->cEAN;
                    $registros['descricao'][$count] = $aa2detnt->getItemRegister()->GIT_DESCRICAO;
                    $registros['custoRMS'][$count] = round($aa2detnt->ultimaEntradaAG1IENSA,2);
                    $registros['nFUltimaEntradaAG1IENSA'][$count] = $aa2detnt->nFUltimaEntradaAG1IENSA;
                    $registros['dtUltimaEntradaAG1IENSA'][$count] = $aa2detnt->dtUltimaEntradaAG1IENSA;
                    $registros['custoLancamento'][$count] = $aa2detnt->REN_PRC_UN;
                    $registros['diferencaCustoRMS'][$count] = round($aa2detnt->diferencaValor);
                    $registros['nfAnterior'][$count] = $nfReceipt->notaAnterior($det->prod->cProd);
                    $registros['custoAnterior'][$count] = $nfReceipt->custoAnterior($det->prod->cProd);
                    $registros['custoAtual'][$count] = $nfReceipt->custoAtual($det->prod->cProd);
                    $registros['diferencaCustoPortal'][$count] = round($nfReceipt->diferencaCustoPortal($det->prod->cProd));
                    $count++;
                }else if (in_array($det->prod->cEAN, $aa2detnt->eans())) {
                    $registros['codigo'][$count] = $det->prod->cProd;
                    $registros['ean'][$count] = $det->prod->cEAN;
                    $registros['descricao'][$count] = $aa2detnt->getItemRegister()->GIT_DESCRICAO;
                    $registros['custoRMS'][$count] = round($aa2detnt->ultimaEntradaAG1IENSA,2);
                    $registros['nFUltimaEntradaAG1IENSA'][$count] = $aa2detnt->nFUltimaEntradaAG1IENSA;
                    $registros['dtUltimaEntradaAG1IENSA'][$count] = $aa2detnt->dtUltimaEntradaAG1IENSA;
                    $registros['custoLancamento'][$count] = $aa2detnt->REN_PRC_UN;
                    $registros['diferencaCustoRMS'][$count] = round($aa2detnt->diferencaValor);
                    $registros['nfAnterior'][$count] = $nfReceipt->notaAnterior($det->prod->cProd);
                    $registros['custoAnterior'][$count] = $nfReceipt->custoAnterior($det->prod->cProd);
                    $registros['custoAtual'][$count] = $nfReceipt->custoAtual($det->prod->cProd);
                    $registros['diferencaCustoPortal'][$count] = round($nfReceipt->diferencaCustoPortal($det->prod->cProd));
                    $count++;
                }
                
                
            }
        }
        
        echo $this->view->render("administrator/diferencaCusto", ["id" => "Diferença Custo | " . SITE, "registros" => $registros, "count" => $count
            ]);

    }

    public function teste2() {
        if ($_POST["date1"] && $_POST["date2"]) {
                /**
             * date data do filtro
             */
            $initialDate = $_POST["date1"];
            $finalDate = $_POST["date2"];
             /**
             * date 2 ano anterior ao filtro
             */
            $initialDate2 = date('Y/m/d', strtotime('-1 year', strtotime($initialDate)));
            $finalDate2 = date('Y/m/d', strtotime('-1 year', strtotime($finalDate)));
            /**
             * date 3 mês anterior ao filtro
             */
            $initialDate3 = date('Y/m/d', strtotime('-1 month', strtotime($initialDate)));
            $finalDate3 = date('Y/m/d', strtotime('-1 month', strtotime($finalDate)));
            $date1 = ' '.inputDateToRMSDate($initialDate).' and '.inputDateToRMSDate($finalDate).' ';
            $date2 =  ' '.inputDateToRMSDate($initialDate2).' and '.inputDateToRMSDate($finalDate2).' ';
            $date3 = ' '.inputDateToRMSDate($initialDate3).' and '.inputDateToRMSDate($finalDate3).' ';
            if ($_POST['base'] == 1) {
                $vendaAtual = (new AG1IENSA())->find("ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaAnoPassado = (new AG1IENSA())->find("ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaMePassaoa = (new AG1IENSA())->find("ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
            } else if ($_POST['base'] == 2) {
                $vendaAtual = (new AG1IENSA2())->find("ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaAnoPassado = (new AG1IENSA2())->find("ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaMePassaoa = (new AG1IENSA2())->find("ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
            } else if ($_POST['base'] == 3) {
                $vendaAtual = (new AG1IENSA3())->find("ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaAnoPassado = (new AG1IENSA3())->find("ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaMePassaoa = (new AG1IENSA3())->find("ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
            } else if ($_POST['base'] == 4) {
                $vendaAtual = (new AG1IENSA4())->find("ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaAnoPassado = (new AG1IENSA4())->find("ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaMePassaoa = (new AG1IENSA4())->find("ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
            } else if ($_POST['base'] == 7) {
                $vendaAtual = (new AG1IENSA7())->find("ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaAnoPassado = (new AG1IENSA7())->find("ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
                $vendaMePassaoa = (new AG1IENSA7())->find("ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->order("ESCHLJC_CODIGO3 ASC")->fetch(true);
            }
            foreach ($vendaAtual as $va) {
                $lojas[$va->ESCHLJC_CODIGO3] = $va->ESCHLJC_CODIGO3;
                $valorVenda[$va->ESCHLJC_CODIGO3] = $va->VALOR;
            }
            foreach ($vendaAnoPassado as $va) {
                $lojas[$va->ESCHLJC_CODIGO3] = $va->ESCHLJC_CODIGO3;
                $valorVendaAno[$va->ESCHLJC_CODIGO3] = $va->VALOR;
            }
            foreach ($vendaMePassaoa as $va) {
                $lojas[$va->ESCHLJC_CODIGO3] = $va->ESCHLJC_CODIGO3;
                $valorVendaMes[$va->ESCHLJC_CODIGO3] = $va->VALOR;
            }
            echo $this->view->render("administrator/gerencial", ["id" => "Gerencial | " . SITE, "date1" => $_POST['date1'], "date2" => $_POST['date2'], "lojas" => $lojas, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        } else {
            echo $this->view->render("administrator/gerencial", ["id" => "Gerencial | " . SITE, "lojas" => $lojas, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        }    
    }

    public function teste3() {
        if ($_POST["date1"] && $_POST["date2"]) {
            $aa3cnvcc = new AA3CNVCC();
                /**
             * date data do filtro
             */
            $initialDate = $_POST["date1"];
            $finalDate = $_POST["date2"];
            $loja = $_POST['loja'];
             /**
             * date 2 ano anterior ao filtro
             */
            $initialDate2 = date('Y/m/d', strtotime('-1 year', strtotime($initialDate)));
            $finalDate2 = date('Y/m/d', strtotime('-1 year', strtotime($finalDate)));
            /**
             * date 3 mês anterior ao filtro
             */
            $initialDate3 = date('Y/m/d', strtotime('-1 month', strtotime($initialDate)));
            $finalDate3 = date('Y/m/d', strtotime('-1 month', strtotime($finalDate)));
            $date1 = ' '.inputDateToRMSDate($initialDate).' and '.inputDateToRMSDate($finalDate).' ';
            $date2 =  ' '.inputDateToRMSDate($initialDate2).' and '.inputDateToRMSDate($finalDate2).' ';
            $date3 = ' '.inputDateToRMSDate($initialDate3).' and '.inputDateToRMSDate($finalDate3).' ';
            $vendaAtual = (new AG1IENSA())->find("ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_SECAO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_SECAO3")->order("ESCHC_SECAO3 ASC")->fetch(true);
            $vendaAnoPassado = (new AG1IENSA())->find("ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_SECAO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_SECAO3")->order("ESCHC_SECAO3 ASC")->fetch(true);
            $vendaMePassaoa = (new AG1IENSA())->find("ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_SECAO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_SECAO3")->order("ESCHC_SECAO3 ASC")->fetch(true);
            foreach ($vendaAtual as $va) {
                $secoes[$aa3cnvcc->getSecao($va->ESCHC_SECAO3)] = $aa3cnvcc->getSecao($va->ESCHC_SECAO3);
                $valorVenda[$aa3cnvcc->getSecao($va->ESCHC_SECAO3)] = $va->VALOR;
            }
            foreach ($vendaAnoPassado as $va) {
                $secoes[$aa3cnvcc->getSecao($va->ESCHC_SECAO3)] = $aa3cnvcc->getSecao($va->ESCHC_SECAO3);
                $valorVendaAno[$aa3cnvcc->getSecao($va->ESCHC_SECAO3)] = $va->VALOR;
            }
            foreach ($vendaMePassaoa as $va) {
                $secoes[$aa3cnvcc->getSecao($va->ESCHC_SECAO3)] = $aa3cnvcc->getSecao($va->ESCHC_SECAO3);
                $valorVendaMes[$aa3cnvcc->getSecao($va->ESCHC_SECAO3)] = $va->VALOR;
            }
            echo $this->view->render("administrator/gerencialSecoes", ["id" => "Gerencial | " . SITE, "secoes" => $secoes, "date1" => $_POST['date1'], "date2" => $_POST['date2'], "loja" => $loja, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        } else {
            echo $this->view->render("administrator/gerencialSecoes", ["id" => "Gerencial | " . SITE, "secoes" => $secoes, "loja" => $loja, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        }    
    }   

    public function teste4() {
        if ($_POST["date1"] && $_POST["date2"]) {
            $aa3cnvcc = new AA3CNVCC();
                /**
             * date data do filtro
             */
            $initialDate = $_POST["date1"];
            $finalDate = $_POST["date2"];
            $loja = $_POST['loja'];
            $secao = $_POST['secao'];
            $secaoCode = $aa3cnvcc->getSecao2($secao);
             /**
             * date 2 ano anterior ao filtro
             */
            $initialDate2 = date('Y/m/d', strtotime('-1 year', strtotime($initialDate)));
            $finalDate2 = date('Y/m/d', strtotime('-1 year', strtotime($finalDate)));
            /**
             * date 3 mês anterior ao filtro
             */
            $initialDate3 = date('Y/m/d', strtotime('-1 month', strtotime($initialDate)));
            $finalDate3 = date('Y/m/d', strtotime('-1 month', strtotime($finalDate)));
            $date1 = ' '.inputDateToRMSDate($initialDate).' and '.inputDateToRMSDate($finalDate).' ';
            $date2 =  ' '.inputDateToRMSDate($initialDate2).' and '.inputDateToRMSDate($finalDate2).' ';
            $date3 = ' '.inputDateToRMSDate($initialDate3).' and '.inputDateToRMSDate($finalDate3).' ';
            $vendaAtual = (new AG1IENSA())->find("ESCHC_SECAO3 = $secaoCode and ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_GRUPO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_GRUPO3")->order("ESCHC_GRUPO3 ASC")->fetch(true);
            $vendaAnoPassado = (new AG1IENSA())->find("ESCHC_SECAO3 = $secaoCode and  ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_GRUPO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_GRUPO3")->order("ESCHC_GRUPO3 ASC")->fetch(true);
            $vendaMePassaoa = (new AG1IENSA())->find("ESCHC_SECAO3 = $secaoCode and ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_GRUPO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_GRUPO3")->order("ESCHC_GRUPO3 ASC")->fetch(true);

            foreach ($vendaAtual as $va) {
                $grupos[$aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3)] = $aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3);
                $valorVenda[$aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3)] = $va->VALOR;
            }
            foreach ($vendaAnoPassado as $va) {
                $grupos[$aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3)] = $aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3);
                $valorVendaAno[$aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3)] = $va->VALOR;
            }
            foreach ($vendaMePassaoa as $va) {
                $grupos[$aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3)] = $aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3);
                $valorVendaMes[$aa3cnvcc->getGrupo($secaoCode,$va->ESCHC_GRUPO3)] = $va->VALOR;
            }
            echo $this->view->render("administrator/gerencialGrupo", ["id" => "Gerencial | " . SITE,"secao" => $secao, "grupos" => $grupos, "date1" => $_POST['date1'], "date2" => $_POST['date2'], "loja" => $loja, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        } else {
            echo $this->view->render("administrator/gerencialGrupo", ["id" => "Gerencial | " . SITE,"secao" => $secao, "grupos" => $grupos, "loja" => $loja, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        }    
    } 

    public function teste5() {
        if ($_POST["date1"] && $_POST["date2"]) {
            $aa3cnvcc = new AA3CNVCC();
                /**
             * date data do filtro
             */
            $initialDate = $_POST["date1"];
            $finalDate = $_POST["date2"];
            $loja = $_POST['loja'];
            $secao = $_POST['secao'];
            $secaoCode = $aa3cnvcc->getSecao2($secao);
            $grupo = $_POST['grupo'];
            $grupoCode = $aa3cnvcc->getGrupo2($grupo);
             /**
             * date 2 ano anterior ao filtro
             */
            $initialDate2 = date('Y/m/d', strtotime('-1 year', strtotime($initialDate)));
            $finalDate2 = date('Y/m/d', strtotime('-1 year', strtotime($finalDate)));
            /**
             * date 3 mês anterior ao filtro
             */
            $initialDate3 = date('Y/m/d', strtotime('-1 month', strtotime($initialDate)));
            $finalDate3 = date('Y/m/d', strtotime('-1 month', strtotime($finalDate)));
            $date1 = ' '.inputDateToRMSDate($initialDate).' and '.inputDateToRMSDate($finalDate).' ';
            $date2 =  ' '.inputDateToRMSDate($initialDate2).' and '.inputDateToRMSDate($finalDate2).' ';
            $date3 = ' '.inputDateToRMSDate($initialDate3).' and '.inputDateToRMSDate($finalDate3).' ';
            $vendaAtual = (new AG1IENSA())->find("ESCHC_GRUPO3 = $grupoCode and ESCHC_SECAO3 = $secaoCode and ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_SUBGRUPO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_SUBGRUPO3")->order("ESCHC_SUBGRUPO3 ASC")->fetch(true);
            $vendaAnoPassado = (new AG1IENSA())->find("ESCHC_GRUPO3 = $grupoCode and ESCHC_SECAO3 = $secaoCode and  ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_SUBGRUPO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_SUBGRUPO3")->order("ESCHC_SUBGRUPO3 ASC")->fetch(true);
            $vendaMePassaoa = (new AG1IENSA())->find("ESCHC_GRUPO3 = $grupoCode and ESCHC_SECAO3 = $secaoCode and ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHC_SUBGRUPO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHC_SUBGRUPO3")->order("ESCHC_SUBGRUPO3 ASC")->fetch(true);

            foreach ($vendaAtual as $va) {
                $subGrupos[$aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3)] = $aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3);
                $valorVenda[$aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3)] = $va->VALOR;
            }
            foreach ($vendaAnoPassado as $va) {
                $subGrupos[$aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3)] = $aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3);
                $valorVendaAno[$aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3)] = $va->VALOR;
            }
            foreach ($vendaMePassaoa as $va) {
                $subGrupos[$aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3)] = $aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3);
                $valorVendaMes[$aa3cnvcc->getSubGrupo($secaoCode,$grupoCode,$va->ESCHC_SUBGRUPO3)] = $va->VALOR;
            }
            echo $this->view->render("administrator/gerencialSubGrupo", ["id" => "Gerencial | " . SITE,"subGrupos" => $subGrupos, "secao" => $secao, "grupo" => $grupo, "date1" => $_POST['date1'], "date2" => $_POST['date2'], "loja" => $loja, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        } else {
            echo $this->view->render("administrator/gerencialSubGrupo", ["id" => "Gerencial | " . SITE,"subGrupos" => $subGrupos, "secao" => $secao, "grupo" => $grupo, "loja" => $loja, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        }    
    } 
    
    public function teste6() {

        if ($_POST["date1"] && $_POST["date2"]) {
            $aa3cnvcc = new AA3CNVCC();
                /**
             * date data do filtro
             */
            $initialDate = $_POST["date1"];
            $finalDate = $_POST["date2"];
            $loja = $_POST['loja'];
            $secao = $_POST['secao'];
            $secaoCode = $aa3cnvcc->getSecao2($secao);
            $grupo = $_POST['grupo'];
            $grupoCode = $aa3cnvcc->getGrupo2($grupo);
            $subGrupo = $_POST['subGrupo'];
            $subGrupoCode = $aa3cnvcc->getSubGrupo2($subGrupo);
             /**
             * date 2 ano anterior ao filtro
             */
            $initialDate2 = date('Y/m/d', strtotime('-1 year', strtotime($initialDate)));
            $finalDate2 = date('Y/m/d', strtotime('-1 year', strtotime($finalDate)));
            /**
             * date 3 mês anterior ao filtro
             */
            $initialDate3 = date('Y/m/d', strtotime('-1 month', strtotime($initialDate)));
            $finalDate3 = date('Y/m/d', strtotime('-1 month', strtotime($finalDate)));
            $date1 = ' '.inputDateToRMSDate($initialDate).' and '.inputDateToRMSDate($finalDate).' ';
            $date2 =  ' '.inputDateToRMSDate($initialDate2).' and '.inputDateToRMSDate($finalDate2).' ';
            $date3 = ' '.inputDateToRMSDate($initialDate3).' and '.inputDateToRMSDate($finalDate3).' ';
            $vendaAtual = (new AG1IENSA())->find("ESCHC_SUBGRUPO3 = $subGrupoCode and ESCHC_GRUPO3 = $grupoCode and ESCHC_SECAO3 = $secaoCode and ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESITC_CODIGO,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESITC_CODIGO")->order("ESITC_CODIGO ASC")->fetch(true);
            $vendaAnoPassado = (new AG1IENSA())->find("ESCHC_SUBGRUPO3 = $subGrupoCode and ESCHC_GRUPO3 = $grupoCode and ESCHC_SECAO3 = $secaoCode and  ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESITC_CODIGO,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESITC_CODIGO")->order("ESITC_CODIGO ASC")->fetch(true);
            $vendaMePassaoa = (new AG1IENSA())->find("ESCHC_SUBGRUPO3 = $subGrupoCode and ESCHC_GRUPO3 = $grupoCode and ESCHC_SECAO3 = $secaoCode and ESCHLJC_CODIGO3 = $loja AND ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESITC_CODIGO,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESITC_CODIGO")->order("ESITC_CODIGO ASC")->fetch(true);

            foreach ($vendaAtual as $va) {
                $itens[$va->ESITC_CODIGO] = $va->ESITC_CODIGO;
                $valorVenda[$va->ESITC_CODIGO] = $va->VALOR;
            }
            foreach ($vendaAnoPassado as $va) {
                $itens[$va->ESITC_CODIGO] = $va->ESITC_CODIGO;
                $valorVendaAno[$va->ESITC_CODIGO] = $va->VALOR;
            }
            foreach ($vendaMePassaoa as $va) {
                $itens[$va->ESITC_CODIGO] = $va->ESITC_CODIGO;
                $valorVendaMes[$va->ESITC_CODIGO] = $va->VALOR;
            }
            $aa3citem = new AA3CITEM();
            echo $this->view->render("administrator/gerencialItens", ["id" => "Gerencial | " . SITE,"aa3citem" => $aa3citem, "itens" => $itens,"subGrupos" => $subGrupos, "secao" => $secao, "grupos" => $grupos, "date1" => $_POST['date1'], "date2" => $_POST['date2'], "loja" => $loja, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        } else {
            echo $this->view->render("administrator/gerencialItens", ["id" => "Gerencial | " . SITE,"aa3citem" => $aa3citem, "itens" => $itens,"subGrupos" => $subGrupos, "secao" => $secao, "grupos" => $grupos, "loja" => $loja, "valorVenda" => $valorVenda, "valorVendaAno" => $valorVendaAno, "valorVendaMes" => $valorVendaMes]);
        }    
    } 

    public function gestaoEquipe() {
        $aa1ctcon = (new AA1CTCON())->getComprasFiscal('C');
        if ($_POST['loja'] != null) {
            $loja = $_POST['loja'];
            $arquivo = $_POST['loja'].$_POST['date1'].$_POST['date2'].$_POST['base'].$_POST['tipo'].'.json';
            if (file_exists(__DIR__ . '/' . $arquivo)) {
                $arquivo = file_get_contents(__DIR__."/".$arquivo);
                $relacao = json_decode($arquivo);
            } else {
                
                if ($_POST['base'] == 1 ) {
                    if ($_POST['date1'] && $_POST['date2']) {
                        $loja = $_POST['loja'];
                        $periodo = " ".inputDateToRMSDate($_POST['date1'])." and ".inputDateToRMSDate($_POST['date2'])." ";
                        if ($_POST['tipo'] != '') {
                            $tipo = $_POST['tipo'];
                        } else {
                            $tipo = null;
                        }
            
                        $variacao = (new AG1IENSA())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                        
                        $aa3citem = (new AA3CITEM());
                    }
                    $aa3citem = (new AA3CITEM());
                } else if ($_POST['base'] == 2) {
                    if ($_POST['date1'] && $_POST['date2']) {
                        $loja = $_POST['loja'];
                        $periodo = " ".inputDateToRMSDate($_POST['date1'])." and ".inputDateToRMSDate($_POST['date2'])." ";
                        if ($_POST['tipo'] != '') {
                            $tipo = $_POST['tipo'];
                        } else {
                            $tipo = null;
                        }
    
                        $variacao = (new AG1IENSA2())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                        
                
                        $aa3citem = (new AA3CITEM2());
                    }
                    $aa3citem = (new AA3CITEM2());
                }  
                else if ($_POST['base'] == 3) {
                    if ($_POST['date1'] && $_POST['date2']) {
                        $loja = $_POST['loja'];
                        $periodo = " ".inputDateToRMSDate($_POST['date1'])." and ".inputDateToRMSDate($_POST['date2'])." ";
                        if ($_POST['tipo'] != '') {
                            $tipo = $_POST['tipo'];
                        } else {
                            $tipo = null;
                        }
            
                        $variacao = (new AG1IENSA3())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                        
                        $aa3citem = (new AA3CITEM3());
                    }
                    $aa3citem = (new AA3CITEM3());
                } 
                 else if ($_POST['base'] == 4) {
                    if ($_POST['date1'] && $_POST['date2']) {
                        $loja = $_POST['loja'];
                        $periodo = " ".inputDateToRMSDate($_POST['date1'])." and ".inputDateToRMSDate($_POST['date2'])." ";
                        if ($_POST['tipo'] != '') {
                            $tipo = $_POST['tipo'];
                        } else {
                            $tipo = null;
                        }
    
                        $variacao = (new AG1IENSA4())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                
                        $aa3citem = (new AA3CITEM4());
                    }
                    $aa3citem = (new AA3CITEM2());
                } else if ($_POST['base'] == 7) {
                    if ($_POST['date1'] && $_POST['date2']) {
                        $loja = $_POST['loja'];
                        $periodo = " ".inputDateToRMSDate($_POST['date1'])." and ".inputDateToRMSDate($_POST['date2'])." ";
                        if ($_POST['tipo'] != '') {
                            $tipo = $_POST['tipo'];
                        } else {
                            $tipo = null;
                        }
            
                        $variacao = (new AG1IENSA7())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                        
                        $aa3citem = (new AA3CITEM7());
                    }
            
                    if ($_POST['variacao'] == "") {
                        $diferenca = $_POST['variacao'] = 0;
                    } else {
                        $diferenca = $_POST['variacao'] += 0;
                    }
                    $aa3citem = (new AA3CITEM7());
                }
    
                
                $json = json_encode($variacao);
                $file = fopen(__DIR__ . '/' . $arquivo,'w');
                fwrite($file, $json);
                fclose($file);
                $arquivo = file_get_contents(__DIR__."/".$arquivo);
                $relacao = json_decode($arquivo);

            }
        }            
        
        if ($_POST['variacao'] != '') {
            $diferenca = $_POST['variacao'];
        } else {
            $diferenca = null;
        }

        
        // $aa3citem2 = (new AA3CITEM2());
       
        echo $this->view->render("administrator/variacaoCusto", ["id" => "Cadastro Produtos | " . SITE, "relacao" =>  $relacao, "aa3citem" => $aa3citem, "loja" => $loja, "diferenca" => $diferenca
            ]);
    }

    public function verificarReferenciaDuplicada() {
        $aa3citem  = (new AA3CITEM())->find("TRIM(GIT_REFERENCIA) IS NOT NULL","","GIT_COD_FOR, GIT_REFERENCIA,COUNT(*) QUANTIDADE")->group("GIT_COD_FOR, GIT_REFERENCIA")->order("GIT_REFERENCIA,GIT_COD_FOR")->fetch(true);
        $i = 1;
        foreach($aa3citem as $citem) {

            if ($citem->QUANTIDADE > 1) {
                if ($i == 1) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = '-634452539';
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Referencias principais duplicadas Mix Bahia");
                    $i++;
                }
                $aa3citem2 = (new AA3CITEM())->find("GIT_REFERENCIA = '$citem->GIT_REFERENCIA' and GIT_COD_FOR = $citem->GIT_COD_FOR")->fetch(true);

                foreach ($aa3citem2 as $citem2) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = '-634452539';
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Codigo $citem2->GIT_COD_ITEM - $citem2->GIT_DIGITO $citem2->GIT_DESCRICAO Referencia $citem->GIT_REFERENCIA");
                    sleep(5);
                }
            }
            
        }

        $aa3citem  = (new AA3CITEM2())->find("TRIM(GIT_REFERENCIA) IS NOT NULL","","GIT_COD_FOR, GIT_REFERENCIA,COUNT(*) QUANTIDADE")->group("GIT_COD_FOR, GIT_REFERENCIA")->order("GIT_REFERENCIA,GIT_COD_FOR")->fetch(true);
        $i = 1;
        foreach($aa3citem as $citem) {

            if ($citem->QUANTIDADE > 1) {
                if ($i == 1) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = '-634452539';
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Referencias principais duplicadas Bispo & Dantas");
                    $i++;
                }
                $aa3citem2 = (new AA3CITEM2())->find("GIT_REFERENCIA = '$citem->GIT_REFERENCIA' and GIT_COD_FOR = $citem->GIT_COD_FOR")->fetch(true);

                foreach ($aa3citem2 as $citem2) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = '-634452539';
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Codigo $citem2->GIT_COD_ITEM - $citem2->GIT_DIGITO $citem2->GIT_DESCRICAO Referencia $citem->GIT_REFERENCIA");
                    sleep(5);
                }
            }
            
        }

    }

    public function projeto() {
        /**
         * date data do filtro
         */
        $initialDate = '01-01-2022';
        $finalDate = '31-01-2022';
         /**
         * date 2 ano anterior ao filtro
         */
        $initialDate2 = date('d/m/Y', strtotime('-1 year', strtotime($initialDate)));
        $finalDate2 = date('d/m/Y', strtotime('-1 year', strtotime($finalDate)));
        /**
         * date 3 mês anterior ao filtro
         */
        $initialDate3 = date('d/m/Y', strtotime('-1 month', strtotime($initialDate)));
        $finalDate3 = date('d/m/Y', strtotime('-1 month', strtotime($finalDate)));
        $date1 = ' '.inputDateToRMSDate($initialDate).' and '.inputDateToRMSDate($finalDate).' ';
        
        $date2 =  ' '.inputDateToRMSDate($initialDate2).' and '.inputDateToRMSDate($finalDate2).' ';
        $date3 = ' '.inputDateToRMSDate($initialDate3).' and '.inputDateToRMSDate($finalDate3).' ';
        $vendaAtual = (new AG1IENSA())->find("ESCHC_DATA3 between $date1 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->fetch(true);
        $vendaAnoPassado = (new AG1IENSA())->find("ESCHC_DATA3 between $date2 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->fetch(true);
        $vendaMePassaoa = (new AG1IENSA())->find("ESCHC_DATA3 between $date3 AND ESCHC_AGENDA3 IN (601,42,45)","","ESCHLJC_CODIGO3,sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->group("ESCHLJC_CODIGO3")->fetch(true);
        foreach ($vendaAtual as $va) {
            $lojas[$va->ESCHLJC_CODIGO3] = $va->ESCHLJC_CODIGO3;
            $valorVenda[$va->ESCHLJC_CODIGO3] = $va->VALOR;
        }
        foreach ($vendaAnoPassado as $va) {
            $lojas[$va->ESCHLJC_CODIGO3] = $va->ESCHLJC_CODIGO3;
            $valorVendaAno[$va->ESCHLJC_CODIGO3] = $va->VALOR;
        }
        foreach ($vendaMePassaoa as $va) {
            $lojas[$va->ESCHLJC_CODIGO3] = $va->ESCHLJC_CODIGO3;
            $valorVendaMes[$va->ESCHLJC_CODIGO3] = $va->VALOR;
        }

        
    }

    public function simples() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $store = $usuario->store_code;
        $date1 = inputDateToRMSDate($_POST['date1']);
        $date2 = inputDateToRMSDate($_POST['date2']);
        $nfSefaz = (new NfSefaz())->find("store_id = $store and concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) between $date1 and $date2")->order("id ASC")->fetch(true);
        $i = 0;
        foreach ($nfSefaz as $nf) {
            if ($nf->getSimples) {
                foreach ($nf->getItems() as $itens) {
                    $ncm = (new NCMALIQUOTA());
                    
                    if ($ncm->aliquotaS($itens->prod->NCM) > 0) {
                        
                        $linhas[$i]['referencia'] = $itens->prod->cProd;
                        $linhas[$i]['descricao'] = $itens->prod->xProd;
                        $linhas[$i]['ncm'] = $itens->prod->NCM;
                        $linhas[$i]['cfop'] = $itens->prod->CFOP;
                        if ($linhas[$i]['cfop'] == 5101 || $linhas[$i]['cfop'] == 6101) {
                            $aliquota = 12;
                        } else {
                            $aliquota = 0;
                        }
                        $linhas[$i]['quantidade'] = $itens->prod->qCom;
                        $linhas[$i]['valor'] = $itens->prod->vUnCom;
                        $linhas[$i]['base'] = round($itens->prod->vUnCom * $itens->prod->qCom,2);
                        $linhas[$i]['aliquota'] = $ncm->aliquotaS($itens->prod->NCM) - $aliquota;
                        $linhas[$i]['valorIcms'] = round($linhas[$i]['base'] * ($linhas[$i]['aliquota']/100),2);
                        $linhas[$i]['numero'] = $nf->nfe_number;
                        $linhas[$i]['razao'] = $nf->getFornecedor();
                        $linhas[$i]['emissao'] = $nf->emission_date;
                        $linhas[$i]['chave'] = $nf->access_key;
                        $total += round(($itens->prod->vUnCom * $itens->prod->qCom) * ($ncm->aliquotaS( $linhas[$i]['ncm'])/100),2);
                        $i++;
                    }
                }
            }
        }

        echo $this->view->render("client/simplesNacional", ["id" => "Relatorio Simples Nacional | " . SITE,"store" => $store, "date1" => $_POST['date1'], "date2" => $_POST['date2'], "linhas" =>  $linhas,"total" => $total]);


    }

    public function simplesForm() {
        echo $this->view->render("client/simplesNacionalForm", ["id" => "Relatorio Simples Nacional | " . SITE]);
    }

    public function multaform() {
        echo $this->view->render("client/multaForm", ["id" => "Relatorio de Multas por Falta de Manifestação | " . SITE]);
    }

    public function multa() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $store = $usuario->store_code;
        $date1 = inputDateToRMSDate($_POST['date1']);
        $date2 = inputDateToRMSDate($_POST['date2']);
        $nfSefaz = (new NfSefaz())->find("store_id = $store and concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) between $date1 and $date2")->order("id ASC")->fetch(true);


        $i = 0;
        foreach ($nfSefaz as $nf) {
            $start_date = new DateTime(substr(str_replace("T"," ",$nf->emission_date),0,19));
            $since_start = $start_date->diff(new DateTime(date("Y-m-d H:i:s")));

            foreach ($nf->getLogManifestation() as $log) {
                if ($log->manifestation == '210220' || $log->manifestation == '210240') {
                    $manifestation = true;
                } else {
                    $manifestation = false;
                }
            }
            
            if (!$manifestation) {
                $days = ($since_start->m*30) + ($since_start->y *365) + $since_start->d;
                if ($nf->getUF() == 'BA' && $days > 20) {
                    
                    if ($nf->getAG1IENSA() == false) {
                        $linhas[$i]['numero'] = $nf->nfe_number;
                        $linhas[$i]['razao'] = $nf->getFornecedor();
                        $linhas[$i]['uf'] = $nf->getUF();
                        $linhas[$i]['emissao'] = $nf->emission_date;
                        $linhas[$i]['chave'] = $nf->access_key;
                        $linhas[$i]['dias'] = $days;
                        $linhas[$i]['valor'] = (float) $nf->valorContabil();
                        $linhas[$i]['pagar'] = $nf->valorContabil() * (1/100);
                        $total += $linhas[$i]['pagar'];
                        $i++;
                    }
                } else if ($nf->getUF() != 'BA' && $days > 35) {
                    if ($nf->getAG1IENSA() == false) {
                        $linhas[$i]['numero'] = $nf->nfe_number;
                        $linhas[$i]['razao'] = $nf->getFornecedor();
                        $linhas[$i]['uf'] = $nf->getUF();
                        $linhas[$i]['emissao'] = $nf->emission_date;
                        $linhas[$i]['chave'] = $nf->access_key;
                        $linhas[$i]['dias'] = $days;
                        $linhas[$i]['valor'] = (float) $nf->valorContabil();
                        $linhas[$i]['pagar'] = $nf->valorContabil() * (1/100);
                        $total += $linhas[$i]['pagar'];
                        $i++;
                    } 
                }      
                
            }
        }

        echo $this->view->render("client/multa", ["id" => "Relatorio de Multas por Falta de Manifestação | " . SITE,"store" => $store, "date1" => $_POST['date1'], "date2" => $_POST['date2'], "linhas" =>  $linhas,"total" => $total]);


    }

    public function getJson($data) {
        echo file_get_contents(__DIR__."/".$data['json']);
    }

    // UNIMAR, NOVO MIX e MIX BAHIA, BISPO E DANTAS

    public function importVariacaoCusto($data) {
        $data['date1'] = "2021-01-01";
        $data['date2'] = date("Y-m-d");
        if ($data['loja'] != null) {
            $loja = $data['loja'];
            $arquivo = $data['loja'].$data['date1'].$data['date2'].$data['base'].$data['tipo'].'.json';
            if (file_exists(__DIR__ . '/' . $arquivo)) {
                $arquivo = file_get_contents(__DIR__."/".$arquivo);
                $relacao = json_decode($arquivo);
            } else {
                // Mix Bahia
                if ($data['base'] == 1 ) {
                    if ($data['date1'] && $data['date2']) {
                        $loja = $data['loja'];
                        $periodo = " ".inputDateToRMSDate($data['date1'])." and ".inputDateToRMSDate($data['date2'])." ";
                        if ($data['tipo'] != '') {
                            $tipo = $data['tipo'];
                        } else {
                            $tipo = null;
                        }
            
                        $variacao = (new AG1IENSA())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                        
                        $aa3citem = (new AA3CITEM());
                    }
                    $aa3citem = (new AA3CITEM());

                } else if ($data['base'] == 2) {
                    if ($data['date1'] && $data['date2']) {
                        $loja = $data['loja'];
                        $periodo = " ".inputDateToRMSDate($data['date1'])." and ".inputDateToRMSDate($data['date2'])." ";
                        if ($data['tipo'] != '') {
                            $tipo = $data['tipo'];
                        } else {
                            $tipo = null;
                        }
        
                        $variacao = (new AG1IENSA2())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                
                        $aa3citem = (new AA3CITEM2());
                    }
                    $aa3citem = (new AA3CITEM2());
                }  else if ($data['base'] == 3) {

                    if ($data['date1'] && $data['date2']) {
                        $loja = $data['loja'];
                        $periodo = " ".inputDateToRMSDate($data['date1'])." and ".inputDateToRMSDate($data['date2'])." ";
                        if ($data['tipo'] != '') {
                            $tipo = $data['tipo'];
                        } else {
                            $tipo = null;
                        }
        
                        $variacao = (new AG1IENSA3())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);

                        $aa3citem = (new Aa3citemnm());
                    }
                    $aa3citem = (new Aa3citemnm());
                }  else if ($data['base'] == 4) {
                    if ($data['date1'] && $data['date2']) {
                        $loja = $data['loja'];
                        $periodo = " ".inputDateToRMSDate($data['date1'])." and ".inputDateToRMSDate($data['date2'])." ";
                        if ($data['tipo'] != '') {
                            $tipo = $data['tipo'];
                        } else {
                            $tipo = null;
                        }
        
                        $variacao = (new AG1IENSA4())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                
                        $aa3citem = (new AA3CITEM4());
                    }
                    $aa3citem = (new AA3CITEM2());
                } else if ($data['base'] == 7) {
                    if ($data['date1'] && $data['date2']) {
                        $loja = $data['loja'];
                        $periodo = " ".inputDateToRMSDate($data['date1'])." and ".inputDateToRMSDate($data['date2'])." ";
                        if ($data['tipo'] != '') {
                            $tipo = $data['tipo'];
                        } else {
                            $tipo = null;
                        }
            
                        $variacao = (new AG1IENSA7())->varicaoCusto($produto,$loja, $periodo,$percentualVariacao, $tipo);
                        
                        $aa3citem = (new AA3CITEM7());
                    }
            
                    if ($data['variacao'] == "") {
                        $diferenca = $data['variacao'] = 0;
                    } else {
                        $diferenca = $data['variacao'] += 0;
                    }
                    $aa3citem = (new AA3CITEM7());
                }
        
                
                $json = json_encode($variacao);
                $file = fopen(__DIR__ . '/' . $arquivo,'w');
                fwrite($file, $json);
                fclose($file);
                // echo $json;
                die;
            }
        } 
    }

    public function NFESemChave() {
        $aa1cfisc = (new AA1CFISC())->find("'1'||FIS_DT_PROC between 1220301 and 1220406 and FIS_OPER in (2,10,11,24,12,24,87,7,143,145,149)")->fetch(true);

            foreach ($aa1cfisc as $cfisc) {
                (string) $cnpj = str_pad($cfisc->getAA2CTIPO()->TIP_CGC_CPF,14,'0',STR_PAD_LEFT);
                (string) $cnpj2 = str_pad($cfisc->getAA2CTIPO2()->TIP_CGC_CPF,14,'0',STR_PAD_LEFT);
                (string) $numero = str_pad($cfisc->FIS_NRO_NOTA,9,'0',STR_PAD_LEFT);
                (string) $serie = str_pad(TRIM($cfisc->FIS_SERIE),3,'0',STR_PAD_LEFT);
                (string) $data = $cfisc->FIS_DTA_AGENDA;
                if ($cfisc->FIS_OPER == 2) {
                    // $nfeControle = (new NFE_CONTROLE())->find("DESCRICAO_SITUACAO = 'NF Autorizada' AND SUBSTR(CHAVE_ACESSO_NFE,7,28) = '".$cnpj2.'55'.$serie.$numero."'")->fetch();
                    // if (!$nfeControle) {
                       
                    //     $logger = new Logger("web");
                    //     $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    //     $tele_channel = "-773803646";
                    //     $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    //     $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    //     $logger->pushHandler($tele_handler);
                    //     $logger->info("CNPJ: $cnpj2\nNúmero: $numero\nSerie: $serie\nData: $data\nAgenda: $cfisc->FIS_OPER");
                    //     sleep(5);
                    // }
                } else {
                    $nfeControle = (new NFE_CONTROLE())->find("DESCRICAO_SITUACAO = 'NF Entrada' AND SUBSTR(CHAVE_ACESSO_NFE,7,28) = '".$cnpj.'55'.$serie.$numero."'")->fetch();
                    $nfeControle2 = (new NFE_CONTROLE())->find("DESCRICAO_SITUACAO = 'NF Autorizada' AND CHAVE_ORIGEM = '".$cnpj.$numero.$serie.$data."'")->fetch();
                    
                    if (!$nfeControle && !$nfeControle2) {
                        echo $cnpj2.$numero.$serie.$data."<br>";
                        
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "-773803646";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("CNPJ: $cnpj\nNúmero: $numero\nSerie: $serie\nData: $data\nAgenda: $cfisc->FIS_OPER");
                        sleep(5);
                    }
                }
                
                
                
            }
    }

    public function divergenciaVencimento() {
        $nfReceipt = (new NfReceipt())->find("group_id in (1,10,2) and status_id = 3 and substr(updated_at,1,10) = '".date('Y-m-d')."'")->fetch(true);
        $xml = simplexml_load_file(__DIR__."/../XML/".$nfReceipt->store_id.'/NFe'.$nfReceipt->access_key.'.xml');
        if ($xml) {
            $cnpj = (string)$xml->NFe->infNFe->emit->CNPJ;
            $totalNota = $xml->NFe->infNFe->total->ICMSTot->vNF;
    
            $codigos = $xml->NFe->infNFe->det;
            $compra = false;
            foreach ($codigos as $codigo) {
                if ($codigo->prod->CFOP == 5902 || $codigo->prod->CFOP == 5908) {
                    $outro = true;
                } else if ($codigo->prod->CFOP == 5910 || $codigo->prod->CFOP == 6910) {
                    $bonificacao = true;
                } else if ($codigo->prod->CFOP == 5949 || $codigo->prod->CFOP == 6949) {
                    $type = 10;
                    $outro = true;
                } else {
                    $compra = true;
                }            
            }
            if ($compra == true) {
                foreach ($nfReceipt as $r ) {
                
                    if ($r->checkVencimento()['ok'] === false) {
                        $logger = new Logger("web");
                        $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                        $tele_channel = "-711918658";
                        $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                        $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                        $logger->pushHandler($tele_handler);
                        $logger->info("Nota fiscal número $r->nf_number, loja $r->store_id atualizada $r->updated_At com divergencia de vencimento pelo usuário $r->getOperator. ".$r->checkVencimento['msg']);
                        sleep(5);
                    }
                    
                }
            }
        }        
        
    }

    public function variacaoCustoUltEnt() {
        $requests = (new NfReceipt())->find("substr(updated_at,1,10) = ".date('Y-m-d')." and status_id = 3")->fetch(true);
        foreach ( $requests as $request) {
            if ($request->diferencaVariacaoCusto()['value'] == true) {

            }
        }
    }

    public function semGiro($data) {
        $stores = array($data['store']);
        foreach($stores as $store) {
            $data = inputDateToRMSDate(date('Y-m-d',strtotime("-90 days")));
            $ag1iensa = (new AG1IENSA());
            $store2 = (new Store())->find("code = :code","code=$store")->fetch();
            if (1 == 1) {
                $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :code","code=$store")->fetch();
                $store = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO*1;
                $aa2cestq = (new AA2CESTQ())->find("GET_COD_LOCAL = $store and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
                rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < $data) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
                rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < $data) and GET_ESTOQUE > 0")->fetch(true);
            } else {
                if ($store2->group_id == 1 || $store2->group_id == 10) {
                    $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :code","code=$store2->code")->fetch();
                    $store = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO*1;
                    $aa2cestq = (new AA2CESTQ())->find("GET_COD_LOCAL = $store and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
                    rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < $data) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
                    rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < $data) and GET_ESTOQUE > 0")->fetch(true);
        
                }
            }      
           
            $content = "<table border='1'>
            <caption>Boa Noite!<br><br> Segue planilha de Itens com mais de 90 dias sem giro na loja $store.</caption>
            <tr>
            <th>Loja</th>
            <th>Codigo</th>
            <th>EAN</th>
            <th>Descrição</th>
            <th>Ultima Entrada</th>
            <th>Ultima Saida</th>
            <th>Quantidade Estoque </th>
            <th>Custo Liquido</th>
            <th>Preço</th>
            </tr>";
    
            foreach ($aa2cestq as $linhp) {
                if ($linhp->getAA1LINHP){
                    // $variacao = $ag1iensa->varicaoCusto($linhp->getAA3CITEM()->GIT_COD_ITEM, 74, " 1210101 and 1".date('ymd'), null, null);
                    $content .= "<tr>
                    <td>".$linhp->GET_COD_LOCAL."</td>
                    <td>".$linhp->GET_COD_PRODUTO."</td>
                    <td>".$linhp->getAA3CITEM()->GIT_CODIGO_EAN13."</td>
                    <td>".$linhp->getAA3CITEM()->GIT_DESCRICAO."</td>
                    <td>".$linhp->dtUltEnt."</td>
                    <td>".$linhp->dtUltFat."</td>
                    <td>".str_replace('.',',',$linhp->GET_ESTOQUE)."</td>
                    <td>".str_replace('.',',',$linhp->GET_CUS_ULT_ENT)."</td>
                    <td>".str_replace('.',',',$linhp->getPrice())."</td>
                    </tr>";
                }
            }


            $email = new \SendGrid\Mail\Mail(); 
            $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
            $email->setSubject("Planilha de produtos com 90 dias ou mais sem giro.");
            $email->addTo("jeferson@bispoedantas.com.br", "Jeferson Dantas");
            $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
            $email->addCc("fabioadebrito@gmail.com","Fábio Brito");
            // if ($store == 74) {
            //     $email->addCc("rodrigo@bispoedantas.com.br","Rodrigo Bispo");
            //     $email->addCc("nayara@bispoedantas.com.br","Nayara Braga");
            //     $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
            //     $email->addCc("iuri@bispoedantas.com.br","Iuri");
            // }

            $email->addContent(
                "text/html", $content
            );
            $sendgrid = new \SendGrid("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");
            try {
                $response = $sendgrid->send($email);
                print $response->statusCode() . "\n";
                print_r($response->headers());
                print $response->body() . "\n";
            } catch (Exception $e) {
                echo 'Caught exception: '. $e->getMessage() ."\n";
            }
            $notificationsAudit = (new NotificationsAudit())->findById(21);
            $notificationsAudit->last_run = date("Y-m-d H:i:s");
            $notificationsAuditId = $notificationsAudit->save();
        }
        
    }

    public function informe() {
        $date = date('Y-m-d');
        $date = date('Y-m-d');
        // $date = '2022-09-07';
        $horas = (new NfReceipt())->find("substr(created_at,1,10) = :date and status_id = :status_id","date=$date&status_id=3")->order("updated_at")->fetch(true);
        foreach($horas as $hora) {
            $operators[$hora->getOperator] = $hora->getOperator;
        }
        $request = (new NfReceipt());
        $requests = $request->find("substr(updated_at,1,10) = :hoje and (status_id = :concluido or status_id = :carga)","hoje=$date&concluido=3&carga=14")->fetch(true);
        foreach ($requests as $request) {
            $requestOperatorCompleted[$request->getOperator] += 1;
            $requestOperatorAmountCompleted[$request->getOperator] += $request->getAmountItems;
        }
        $content .= " 
        <caption>Boa Noite!<br><br> Segue Relação de Notas Atualizadas por Usuário.</caption>       
        <div class='container-fluid' style='margin-bottom:5%'>
            <div class='row'>";

        foreach($operators as $operator){
            $content .= "
            <div style='border-style: solid;width: 300px;margin:2%;padding:2%;float:left'>
                <div>
                    <div>
                        <h3>".($requestOperatorCompleted[$operator]+0)." Notas</h3>
                        <p>".$operator."</p>
                        <p>".($requestOperatorAmountCompleted[$operator]+0)." Itens</p>
                    </div>
                </div>
            </div>";
        }

        $content .= "
            </div>
        </div>";
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
        $email->setSubject("Notas Atualizadas por Operadoes.");
        $email->addTo("fabioadebrito@gmail.com", "Fábio Brito");
        $email->addCc("jeferson@bispoedantas.com.br","Jeferson Dantas");
        $email->addCc("rodrigo@bispoedantas.com.br","Rodrigo Bispo");
        $email->addCc("nayara@bispoedantas.com.br","Nayara Braga");
        $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
        $email->addCc("iuri@bispoedantas.com.br","Iuri");
        
        $email->addContent("text/plain", "Funcionando para todos os serviços como o telegram que já temos.");
        $email->addContent(
            "text/html", $content
        );
        $sendgrid = new \SendGrid("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");
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        $notificationsAudit = (new NotificationsAudit())->findById(22);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function informeFiscal() {
        if (date("d") <= 25) {
            $identification = $_COOKIE['login'];
            $user = (new User())->find("email = :login","login=$identification")->fetch();
            $class = $user->class;
            $fiscalPeriod = (new FiscalPeriod())->find()->fetch(true);
            $fiscalTypeActivity = (new FiscalActivityType())->find()->fetch(true);
            $fiscalStores = (new FiscalStores())->find()->fetch(true);
            $user = new User();
            $date = date('mY',strtotime("-1 month"));
            $stores = (new FiscalStores())->find()->order("code")->fetch(true);
            $fiscalStores = (new FiscalStores())->find()->order("code")->fetch(true);
            $activityType = (new FiscalActivityType())->find()->fetch(true);
            $users = $user->find("class = 'A'")->fetch(true);
            $content .= "<table  border='1'>
            <caption>Boa Noite!<br><br> Seguem Pendencias do Fiscal Vigência $date.</caption>
            <tr>
                <th>Loja</th>
                <th>Responsavel</th>
                <th>Arquivo 1º Período</th>
                <th>Inconsistência 1º Período</th>
                <th>Arquivo 2º Período</th>
                <th>Inconsistência 2º Período</th>
                <th>Arquivo 3º Período</th>
                <th>Planilha Estorno de Credito</th>
                <th>Conf. Vendas/NFC-E</th>
                <th>Inconsistência 3º Período</th>
                <th>Fechamento ICMS</th>
                <th>Arquivo ICMS Declaração </th>
                <th>Arquivo PIS/COFINS</th>
            </tr>";
            foreach ($stores as $store){
                if ($store->pendente($store->id, $date) == true){
                    $content.= "
                    <tr>
                    <td><a target='_blank' href='/fiscal/$store->id/$date'>$store->code.' - '.$store->razao</a></td>
                    <td>".$store->user_id."</td>
                    <td>".$store->file1($store->id,$date)."</td>
                    <td>".$store->inconsistence1($store->id,$date)."</td>
                    <td>".$store->file2($store->id,$date)."</td>
                    <td>".$store->inconsistence2($store->id,$date)."</td>
                    <td>".$store->file3($store->id,$date)."</td>
                    <td>".$store->spreadsheet($store->id,$date)."</td>
                    <td>".$store->sailsConference($store->id,$date)."</td>
                    <td>".$store->inconsistence3($store->id,$date)."</td>
                    <td>".$store->ICMSClousure($store->id,$date)."</td>
                    <td>".$store->fileSendedF($store->id,$date)."</td>
                    <td>".$store->fileSended($store->id,$date)."</td>
                </tr>
                    ";
                }
                
            }
            $content .= "</table>";
    
            $fiscalActivity2 = (new FiscalActivity())->find("substr(created_at,1,10) = '".date('Y-m-d',strtotime("-1 days"))."'")->fetch(true);
    
    
            $content .= "<table  border='1'>
            <caption><br><br> Seguem Conclusões Realizadas hoje.</caption>
            <tr>
                <th>Loja</th>
                <th>Responsavel</th>
                <th>Período</th>
                <th>Tipo</th>
                <th>Atividade</th>
                <th>Mês</th>
                <th>Realizado</th>
            </tr>";
            foreach ($fiscalActivity2 as $store){
                $content.= "
                <tr>
                <td>".$store->getStoreCode."</td>
                <td>".$store->getUserName."</td>
                <td>".$store->getPeriodName."</td>
                <td>".$store->getTypeName."</td>
                <td>".$store->getTypeActivityName."</td>
                <td>".$store->month."</td>
                <td>".$store->created_at."</td>
            </tr>
                ";            
            }
            $content .= "</table>";
            $content .= "<table  border='1'>
            <caption>Boa Noite!<br><br> Segue Planilha Completa do Fiscal.</caption>
            <tr>
                <th>Loja</th>
                <th>Arquivo 1º Período</th>
                <th>Inconsistência 1º Período</th>
                <th>Arquivo 2º Período</th>
                <th>Inconsistência 2º Período</th>
                <th>Arquivo 3º Período</th>
                <th>Planilha Estorno de Credito</th>
                <th>Conf. Vendas/NFC-E</th>
                <th>Inconsistência 3º Período</th>
                <th>Fechamento ICMS</th>
                <th>Arquivo ICMS Declaração </th>
                <th>Arquivo PIS/COFINS</th>
            </tr>";
            foreach ($stores as $store){
                $content.= "
                <tr>
                <td><a target='_blank' href='/fiscal/$store->id/$date'>$store->code.' - '.$store->razao</a></td>
                <td>".$store->file1($store->id,$date)."</td>
                <td>".$store->inconsistence1($store->id,$date)."</td>
                <td>".$store->file2($store->id,$date)."</td>
                <td>".$store->inconsistence2($store->id,$date)."</td>
                <td>".$store->file3($store->id,$date)."</td>
                <td>".$store->spreadsheet($store->id,$date)."</td>
                <td>".$store->sailsConference($store->id,$date)."</td>
                <td>".$store->inconsistence3($store->id,$date)."</td>
                <td>".$store->ICMSClousure($store->id,$date)."</td>
                <td>".$store->fileSendedF($store->id,$date)."</td>
                <td>".$store->fileSended($store->id,$date)."</td>
            </tr>
                ";
            }
            $content .= "</table>";
    
            $email = new \SendGrid\Mail\Mail(); 
            $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
            $email->setSubject("Rotinas Fiscais.");
            $email->addTo("fabioadebrito@gmail.com", "Fábio Brito");
            $email->addCc("jeferson@bispoedantas.com.br","Jeferson Dantas");
            $email->addCc("rodrigo@bispoedantas.com.br","Rodrigo Bispo");
            $email->addCc("nayara@bispoedantas.com.br","Nayara Braga");
            $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
            $email->addCc("iuri@bispoedantas.com.br","Iuri");
            
            $email->addContent("text/plain", "Funcionando para todos os serviços como o telegram que já temos.");
            $email->addContent(
                "text/html", $content
            );
            $sendgrid = new \SendGrid("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");
            try {
                $response = $sendgrid->send($email);
                print $response->statusCode() . "\n";
                print_r($response->headers());
                print $response->body() . "\n";
            } catch (Exception $e) {
                echo 'Caught exception: '. $e->getMessage() ."\n";
            }
        }   
        $notificationsAudit = (new NotificationsAudit())->findById(23);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save(); 
    }

    public function informeFiscal2() {
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $class = $user->class;
        $fiscalPeriod = (new FiscalPeriod())->find()->fetch(true);
        $fiscalTypeActivity = (new FiscalActivityType())->find()->fetch(true);
        $fiscalStores = (new FiscalStores())->find()->fetch(true);
        $user = new User();
        $date = date('mY');
        $stores = (new FiscalStores())->find()->order("code")->fetch(true);
        $fiscalStores = (new FiscalStores())->find()->order("code")->fetch(true);
        $activityType = (new FiscalActivityType())->find()->fetch(true);
        $users = $user->find("class = 'A'")->fetch(true);
        $content .= "<table  border='1'>
        <caption>Boa Noite!<br><br> Seguem Pendencias do Fiscal Vigência $date.</caption>
        <tr>
            <th>Loja</th>
            <th>Responsavel</th>
            <th>Arquivo 1º Período</th>
            <th>Inconsistência 1º Período</th>
            <th>Arquivo 2º Período</th>
            <th>Inconsistência 2º Período</th>
            <th>Arquivo 3º Período</th>
            <th>Planilha Estorno de Credito</th>
            <th>Conf. Vendas/NFC-E</th>
            <th>Inconsistência 3º Período</th>
            <th>Fechamento ICMS</th>
            <th>Arquivo ICMS Declaração </th>
            <th>Arquivo PIS/COFINS</th>
        </tr>";
        foreach ($stores as $store){
            if ($store->pendente($store->id, $date) == true){
                $content.= "
                <tr>
                <td><a target='_blank' href='/fiscal/$store->id/$date'>$store->code.' - '.$store->razao</a></td>
                <td>".$store->user_id."</td>
                <td>".$store->file1($store->id,$date)."</td>
                <td>".$store->inconsistence1($store->id,$date)."</td>
                <td>".$store->file2($store->id,$date)."</td>
                <td>".$store->inconsistence2($store->id,$date)."</td>
                <td>".$store->file3($store->id,$date)."</td>
                <td>".$store->spreadsheet($store->id,$date)."</td>
                <td>".$store->sailsConference($store->id,$date)."</td>
                <td>".$store->inconsistence3($store->id,$date)."</td>
                <td>".$store->ICMSClousure($store->id,$date)."</td>
                <td>".$store->fileSendedF($store->id,$date)."</td>
                <td>".$store->fileSended($store->id,$date)."</td>
            </tr>
                ";
            }
            
        }
        $content .= "</table>";

        $fiscalActivity2 = (new FiscalActivity())->find("substr(created_at,1,10) = '".date('Y-m-d',strtotime("-1 days"))."'")->fetch(true);


        $content .= "<table  border='1'>
        <caption><br><br> Seguem Conclusões Realizadas hoje.</caption>
        <tr>
            <th>Loja</th>
            <th>Responsavel</th>
            <th>Período</th>
            <th>Tipo</th>
            <th>Atividade</th>
            <th>Mês</th>
            <th>Realizado</th>
        </tr>";
        foreach ($fiscalActivity2 as $store){
            $content.= "
            <tr>
            <td>".$store->getStoreCode."</td>
            <td>".$store->getUserName."</td>
            <td>".$store->getPeriodName."</td>
            <td>".$store->getTypeName."</td>
            <td>".$store->getTypeActivityName."</td>
            <td>".$store->month."</td>
            <td>".$store->created_at."</td>
        </tr>
            ";            
        }


        $content .= "</table>";
        $content .= "<table  border='1'>
        <caption>Boa Noite!<br><br> Segue Planilha Completa do Fiscal.</caption>
        <tr>
            <th>Loja</th>
            <th>Arquivo 1º Período</th>
            <th>Inconsistência 1º Período</th>
            <th>Arquivo 2º Período</th>
            <th>Inconsistência 2º Período</th>
            <th>Arquivo 3º Período</th>
            <th>Planilha Estorno de Credito</th>
            <th>Conf. Vendas/NFC-E</th>
            <th>Inconsistência 3º Período</th>
            <th>Fechamento ICMS</th>
            <th>Arquivo ICMS Declaração </th>
            <th>Arquivo PIS/COFINS</th>
        </tr>";
        foreach ($stores as $store){
            $content.= "
            <tr>
            <td><a target='_blank' href='/fiscal/$store->id/$date'>$store->code.' - '.$store->razao</a></td>
            <td>".$store->file1($store->id,$date)."</td>
            <td>".$store->inconsistence1($store->id,$date)."</td>
            <td>".$store->file2($store->id,$date)."</td>
            <td>".$store->inconsistence2($store->id,$date)."</td>
            <td>".$store->file3($store->id,$date)."</td>
            <td>".$store->spreadsheet($store->id,$date)."</td>
            <td>".$store->sailsConference($store->id,$date)."</td>
            <td>".$store->inconsistence3($store->id,$date)."</td>
            <td>".$store->ICMSClousure($store->id,$date)."</td>
            <td>".$store->fileSendedF($store->id,$date)."</td>
            <td>".$store->fileSended($store->id,$date)."</td>
        </tr>
            ";
        }
        $content .= "</table>";

        $users = (new User())->find("id in (148,150,20000151,20000052,180,154)")->fetch(true);
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
        $email->setSubject("Rotinas fiscais");
        $email->addTo("fabioadebrito@gmail.com", "Fábio Brito");
        $email->addCc("jeferson@bispoedantas.com.br","Jeferson Dantas");
        $email->addCc("rodrigo@bispoedantas.com.br","Rodrigo Bispo");
        $email->addCc("nayara@bispoedantas.com.br","Nayara Braga");
        $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
        $email->addCc("iuri@bispoedantas.com.br","Iuri");
        
        $email->addContent("text/plain", "Funcionando para todos os serviços como o telegram que já temos.");
        $email->addContent(
            "text/html", $content
        );
        $sendgrid = new \SendGrid("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");
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        $notificationsAudit = (new NotificationsAudit())->findById(23);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save(); 
    
    }

    public function informeNotification() {
        $notifications = (new NotificationsAudit())->find()->fetch(true);

        $content = $this->view->render("administrator/notificationsPainel", ["id" => "Notificações | " . SITE,
        "notifications" => $notifications, "league" => true
        ]);

        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
        $email->setSubject("Notas Atualizadas por Operadoes.");
        $email->addTo("fabioadebrito@gmail.com", "Fábio Brito");
        $email->addCc("jeferson@bispoedantas.com.br","Jeferson Dantas");
        $email->addCc("rodrigo@bispoedantas.com.br","Rodrigo Bispo");
        $email->addCc("nayara@bispoedantas.com.br","Nayara Braga");
        $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
        $email->addCc("iuri@bispoedantas.com.br","Iuri");
        
        $email->addContent("text/plain", "Funcionando para todos os serviços como o telegram que já temos.");
        $email->addContent(
            "text/html", $content
        );
        $sendgrid = new \SendGrid("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");
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        $notificationsAudit = (new NotificationsAudit())->findById(24);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save(); 
  
        
    }

    public function informe1Porcento() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $store = 74;
        $date1 = inputDateToRMSDate("2022-01-01");
        $date2 = inputDateToRMSDate(date("Y-m-d"));
        $nfSefaz = (new NfSefaz())->find("store_id = $store and concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) between $date1 and $date2")->order("id ASC")->fetch(true);
        $i = 0;
        foreach ($nfSefaz as $nf) {
            $start_date = new DateTime(str_replace("T"," ",$nf->emission_date));
            $since_start = $start_date->diff(new DateTime(date("Y-m-d H:i:s")));

            foreach ($nf->getLogManifestation() as $log) {
                if ($log->manifestation == '210220' || $log->manifestation == '210240') {
                    $manifestation = true;
                } else {
                    $manifestation = false;
                }
            }
            if (!$manifestation) {
                $days = ($since_start->m*30) + ($since_start->y *365) + $since_start->d;
                if ($nf->getUF() == 'BA' && $days > 20) {
                    if ($nf->getAA1CFISC() == false) {
                        $linhas[$i]['numero'] = $nf->nfe_number;
                        $linhas[$i]['razao'] = $nf->getFornecedor();
                        $linhas[$i]['uf'] = $nf->getUF();
                        $linhas[$i]['emissao'] = $nf->emission_date;
                        $linhas[$i]['chave'] = $nf->access_key;
                        $linhas[$i]['natureza'] = $nf->natOp;
                        $linhas[$i]['dias'] = $days;
                        $linhas[$i]['valor'] = (float) $nf->valorContabil();
                        $linhas[$i]['pagar'] = $nf->valorContabil() * (1/100);
                        $total += $linhas[$i]['pagar'];
                        $i++;
                    }
                } else if ($nf->getUF() != 'BA' && $days > 35) {
                    if ($nf->getAA1CFISC() == false) {
                        $linhas[$i]['numero'] = $nf->nfe_number;
                        $linhas[$i]['razao'] = $nf->getFornecedor();
                        $linhas[$i]['uf'] = $nf->getUF();
                        $linhas[$i]['emissao'] = $nf->emission_date;
                        $linhas[$i]['chave'] = $nf->access_key;
                        $linhas[$i]['natureza'] = $nf->natOp;
                        $linhas[$i]['dias'] = $days;
                        $linhas[$i]['valor'] = (float) $nf->valorContabil();
                        $linhas[$i]['pagar'] = $nf->valorContabil() * (1/100);
                        $total += $linhas[$i]['pagar'];
                        $i++;
                    } 
                }      
                
            }
        }

        $content =  $this->view->render("client/multa", ["id" => "Relatorio de Multas por Falta de Manifestação | " . SITE,"league" => true, "store" => $store, "date1" => $_POST['date1'], "date2" => $_POST['date2'], "linhas" =>  $linhas,"total" => $total]);
        echodie($content);
        // $dompdf = new Dompdf();
        // $dompdf->loadHtml($content);
        // $dompdf->setPaper("A4");
        // $dompdf->render();
        // $dompdf->stream("file.pdf",["Attachment" => false]);
        // die;
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
        $email->setSubject("Relatorio de Multa 1% sobre notas não manifestadas.");
        $email->addTo("jeferson@bispoedantas.com.br","Jeferson Dantas");
        $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
        $email->addTo("fabioadebrito@gmail.com", "Fábio Brito");
        // $email->addCc("rodrigo@bispoedantas.com.br","Rodrigo Bispo");
        // $email->addCc("nayara@bispoedantas.com.br","Nayara Braga");
        // $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
        // $email->addCc("iuri@bispoedantas.com.br","Iuri");
        
        $email->addContent("text/plain", "Funcionando para todos os serviços como o telegram que já temos.");
        $email->addContent(
            "text/html", $content
        );
        $sendgrid = new \SendGrid("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");
        try {
            $response = $sendgrid->send($email);
                print $response->statusCode() . "\n";
        print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        $notificationsAudit = (new NotificationsAudit())->findById(25);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();
    }

    public function informeNotas() {
        $date = date("Y-m-d");
        $month = date("Y-m");
        $administradores = (new User())->find("class = 'A'")->fetch(true);
        foreach ($administradores as $administrador) {
            $nfReceipts = (new NfReceipt())->find("status_id = 3 and substr(updated_at,1,7) = '$month' and operator_id = $administrador->id")->fetch(true);

            if (!empty($nfReceipts)) {
                foreach ($nfReceipts as $nfReceipt) {
                    if (substr($nfReceipt->updated_at,0,10) == $date) {
                        $quantidadeNotasDia[$nfReceipt->operator_id] += 1;
                        if (is_int($nfReceipt->getAmount)) {
                            $quantidadeItensDia[$nfReceipt->operator_id] += $nfReceipt->getAmount;
                        }
                    }
                    $quantidadeNotasMes[$nfReceipt->operator_id] += 1;
                    if (is_int($nfReceipt->getAmount)) {
                        $quantidadeItensMes[$nfReceipt->operator_id] += $nfReceipt->getAmount;
                    }
                    if ($nfReceipt->type_id == 4) {
                        if (substr($nfReceipt->updated_at,0,10) == $date) {
                            $quantidadeNotasDiaCD[$nfReceipt->operator_id] += 1;
                        }
                        $quantidadeNotasMesCD[$nfReceipt->operator_id] += 1;
                        
                    }
                    
                   
    
                }
            }
            $nfReportMonth[$administrador->id] = (new NfReport())->find("operator_id = $administrador->id and substr(created_at,1,7) = '$month'")->count();
            $nfReportDay[$administrador->id] = (new NfReport())->find("operator_id = $administrador->id and substr(created_at,1,10) = '$date'")->count();
            $operators[$administrador->id] = $administrador->id;
        }

        $content = "<table border='1'>
        <caption>Boa Noite!<br><br> Segue planilha com a relação de notas atualizadas e reportadas por usuário..</caption>
        <tr>
        <th>Operador</th>
        <th>Notas Concluidas Dia</th>
        <th>Itens  Dia</th>
        <th>Notas Concluidas Dia CD</th>
        <th>Notas Concluidas mês</th>
        <th>Itens  mês</th>
        <th>Notas Concluidas mês CD</th>
        <th>Notas Reportadas dia</th>
        <th>Notas Reportadas mês</th>
        <th>Percentual</th>
        </tr>";
        
        foreach ($operators as $operator) {
            $content .= "<tr>
                <td>".((new User())->findById($operator)->name)."</td>
                <td>".$quantidadeNotasDia[$operator]."</td>
                <td>".$quantidadeItensDia[$operator]."</td>
                <td>".$quantidadeNotasDiaCD[$operator]."</td>
                <td>".$quantidadeNotasMes[$operator]."</td>
                <td>".$quantidadeItensMes[$operator]."</td>
                <td>".$quantidadeNotasMesCD[$operator]."</td>
                <td>".$nfReportDay[$operator]."</td>
                <td>".$nfReportMonth[$operator]."</td>";
            if ($quantidadeNotasMes[$operator] > 0) {
                $content .= "<td>".str_replace('.',',',round((($nfReportMonth[$operator]/$quantidadeNotasMes[$operator])*100),2))."</td>
                </tr>";
            } else {
                $content .= "<td>0</td>
                </tr>";
            }
                
        }
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
        $email->setSubject("Notas Atualizadas por Operadoes.");
        $email->addTo("fabioadebrito@gmail.com", "Fábio Brito");
        $email->addCc("jeferson@bispoedantas.com.br","Jeferson Dantas");
        $email->addCc("rodrigo@bispoedantas.com.br","Rodrigo Bispo");
        $email->addCc("nayara@bispoedantas.com.br","Nayara Braga");
        $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
        $email->addCc("iuri@bispoedantas.com.br","Iuri");
        
        $email->addContent("text/plain", "Funcionando para todos os serviços como o telegram que já temos.");
        $email->addContent(
            "text/html", $content
        );
        $sendgrid = new \SendGrid("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");
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        $notificationsAudit = (new NotificationsAudit())->findById(26);
        $notificationsAudit->last_run = date("Y-m-d H:i:s");
        $notificationsAuditId = $notificationsAudit->save();

    }

    public function informeNotas2() {
        $date = date("Y-m-d",strtotime('-1 days'));
        $month = date("Y-m");
        $stores = (new Store())->find()->order("code")->fetch(true);
        foreach ($stores as $store) {
            $nfReceipts = (new NfReceipt())->find("substr(updated_at,1,7) = '$month' and store_id = $store->code")->fetch(true);

            if (!empty($nfReceipts)) {
                foreach ($nfReceipts as $nfReceipt) {
                    if (substr($nfReceipt->created_at,0,10) == $date) {
                        $quantidadeNotasDia[$nfReceipt->store_id] += 1;
                        if (is_int($nfReceipt->getAmount)) {
                            $quantidadeItensDia[$nfReceipt->store_id] += $nfReceipt->getAmount;
                        }
                        if ($nfReceipt->type_id == 4) {
                            $quantidadeNotasDiaCD[$nfReceipt->store_id] += 1;
                            if (is_int($nfReceipt->getAmount)) {
                                $quantidadeItensDiaCD[$nfReceipt->store_id] += $nfReceipt->getAmount;
                            }
                        }
                    }
                    $quantidadeNotasMes[$nfReceipt->store_id] += 1;
                    if (is_int($nfReceipt->getAmount)) {
                        $quantidadeItensMes[$nfReceipt->store_id] += $nfReceipt->getAmount;
                    }

                    if ($nfReceipt->type_id == 4) {
                        $quantidadeNotasMesCD[$nfReceipt->store_id] += 1;
                        if (is_int($nfReceipt->getAmount)) {
                            $quantidadeItensMesCD[$nfReceipt->store_id] += $nfReceipt->getAmount;
                        }
                    }

                    if( $nfReceipt->status_id == 3) {
                        if (substr($nfReceipt->updated_at,0,10) == $date) {
                            $quantidadeNotasDiaAt[$nfReceipt->store_id] += 1;
                            if (is_int($nfReceipt->getAmount)) {
                                $quantidadeItensDiaAt[$nfReceipt->store_id] += $nfReceipt->getAmount;
                            }
                        }
                        $quantidadeNotasMesAt[$nfReceipt->store_id] += 1;
                        if (is_int($nfReceipt->getAmount)) {
                            $quantidadeItensMesAt[$nfReceipt->store_id] += $nfReceipt->getAmount;
                        }
                    } else if ($nfReceipt->status_id != 7 && $nfReceipt->status_id != 8 && $nfReceipt->status_id != 14 && $nfReceipt->status_id != 16) {
                        $quantidadeNotasMesPen[$nfReceipt->store_id] += 1;
                        if (is_int($nfReceipt->getAmount)) {
                            $quantidadeItensMesPen[$nfReceipt->store_id] += $nfReceipt->getAmount;
                        }
                    }
                   
    
                }
            }
            $nfReportMonths = (new NfReport())->find("user_id != 0 and operator_id != 0 and substr(created_at,1,7) = '$month'")->fetch(true);
            if (is_array($nfReportMonths)) {
                
                foreach ($nfReportMonths as $nfReportMonth) {
                    $userForCheck = (new User())->findById($nfReportMonth->user_id);
                    if($userForCheck->class == 'C') {
                        if ($nfReportMonth->getStore == $store->code) {
                            $reportMonth[$store->code] += 1;
                        }
                    }
                }
            }
            $nfReportDays = (new NfReport())->find("user_id != 0 and operator_id != 0 and substr(created_at,1,10) = '$date'")->fetch(true);
            if (is_array($nfReportDays)) {
                foreach ($nfReportDays as $nfReportDay) {
                    $userForCheck = (new User())->findById($nfReportDay->user_id);
                    if($userForCheck->class == 'C') {
                        if ($nfReportDay->getStore == $store->code) {
                            $reportDay[$store->code] += 1;
                        }
                    }
                    
                }
            }
            
            $lojas[$store->code] = $store->code;
        }

        $content = "<table border='1'>
        <caption>Boa Noite!<br><br> Segue planilha com a relação de notas incluidas, atualizadas e reportadas por Loja..$month</caption>
        <tr>
        <th>Loja</th>
        <th>Razão</th>
        <th>Notas Incluidas Dia</th>
        <th>Itens  Dia</th>
        <th>Notas Incluidas mês</th>
        <th>Itens  mês</th>
        <th>Notas Incluidas dia CD</th>
        <th>Itens  Dia CD</th>
        <th>Notas Incluidas mês CD</th>
        <th>Itens  mês CD</th>
        <th>Notas Concluidas Dia</th>
        <th>Itens  Dia</th>
        <th>Notas Concluidas mês</th>
        <th>Itens  mês</th>
        <th>Notas Pendentes</th>
        <th>Itens  Pendentes</th>
        <th>Notas Reportadas dia</th>
        <th>Notas Reportadas mês</th>
        <th>Percentual</th>
        </tr>";
        
        foreach ($lojas as $loja) {
            $content .= "<tr>
                <td>".$loja."</td>
                <td>".((new Store())->find("code = $loja")->fetch()->company_name)."</td>
                <td>".$quantidadeNotasDia[$loja]."</td>
                <td>".$quantidadeItensDia[$loja]."</td>
                <td>".$quantidadeNotasMes[$loja]."</td>
                <td>".$quantidadeItensMes[$loja]."</td>
                <td>".$quantidadeNotasDiaCD[$loja]."</td>
                <td>".$quantidadeItensDiaCD[$loja]."</td>
                <td>".$quantidadeNotasMesCD[$loja]."</td>
                <td>".$quantidadeItensMesCD[$loja]."</td>
                <td>".$quantidadeNotasDiaAt[$loja]."</td>
                <td>".$quantidadeItensDiaAt[$loja]."</td>
                <td>".$quantidadeNotasMesAt[$loja]."</td>
                <td>".$quantidadeItensMesAt[$loja]."</td>
                <td>".$quantidadeNotasMesPen[$loja]."</td>
                <td>".$quantidadeItensMesPen[$loja]."</td>
                <td>".$reportDay[$loja]."</td>
                <td>".$reportMonth[$loja]."</td>";
            if ($quantidadeNotasMes[$loja] > 0) {
                $content .= "<td>".str_replace('.',',',round((($reportMonth[$loja]/$quantidadeNotasMes[$loja])*100),2))."</td>
                </tr>";
            } else {
                $content .= "<td>0</td>
                </tr>";
            }
                
        }

        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
        $email->setSubject("Notas Atualizadas por Operadoes.");
        $email->addTo("fabioadebrito@gmail.com", "Fábio Brito");
        $email->addCc("jeferson@bispoedantas.com.br","Jeferson Dantas");
        $email->addCc("rodrigo@bispoedantas.com.br","Rodrigo Bispo");
        $email->addCc("nayara@bispoedantas.com.br","Nayara Braga");
        $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
        $email->addCc("iuri@bispoedantas.com.br","Iuri");
        
        $email->addContent("text/plain", "Funcionando para todos os serviços como o telegram que já temos.");
        $email->addContent(
            "text/html", $content
        );
        $sendgrid = new \SendGrid("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");
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }

    public function notasFiscalNaoAtualizadas() {
        $data1 = '1'.date("ym",strtotime("-1 month")).'01';
        $data2 = '1'.date("ym",strtotime("-1 month")).'31';
        $agendas = (new AA1CTCON())->getComprasFiscal();
        $aa1agedt = (new AA1AGEDT())->find("DET_FLAG = 1 AND DET_TIPO IN ($agendas) AND DET_DATA BETWEEN $data1 and $data2")->order("DET_LOJA")->fetch(true);
        foreach($aa1agedt as $notas) {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = "-791778147";
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            $logger->pushHandler($tele_handler);
            $logger->info("Nota fiscal número $notas->DET_NOTA, loja $notas->DET_LOJA data agenda ".dateRMSToDate($notas->DET_DATA)." agenda = $notas->DET_TIPO.");
            sleep(5);
            
        }

        $agendas = (new AA1CTCON2())->getComprasFiscal();
        $aa1agedt = (new AA1AGEDT2())->find("DET_FLAG = 1 AND DET_TIPO IN ($agendas) AND DET_DATA BETWEEN $data1 and $data2")->order("DET_LOJA")->fetch(true);
        foreach($aa1agedt as $notas) {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = "-791778147";
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            $logger->pushHandler($tele_handler);
            $logger->info("Nota fiscal número $notas->DET_NOTA, loja $notas->DET_LOJA data agenda ".dateRMSToDate($notas->DET_DATA)." agenda = $notas->DET_TIPO.");
            sleep(5);
            
        }
    }

    public function auditNFEnergia() {
        if (date("d") <= 25) {
            $date = "1".date("ym",strtotime("-1 month"));
            $d = date("m/Y",strtotime("-1 month"));
        } else {
            $date = "1".date("ym");
            $d = date("m/Y");
        }
        
        // Mix Bahia
        $mbs = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
        foreach ($mbs as $mb) {
            $aa1cfisc = (new AA1CFISC())->find("FIS_OPER = 499 and FIS_LOJ_DST = $mb and substr(FIS_DTA_AGENDA,0,5) = $date")->fetch(true);
            $i = 0;
            if (is_array($aa1cfisc)) {
                foreach ($aa1cfisc as $cfisc) {
                    $i++;
                }
            }
            
            if ($i == 0) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-641446203";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $mb MB, não realizou nenhum lançamento de NF de energia no periodo $d");
                // echo "Loja $mb MB, não realizou nenhum lançamento de NF de energia no periodo $d<br>";
                sleep(5);
            }
        }

        // Bispo e Dantas
        $bds = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        foreach($bds as $bd) {
            $aa1cfisc = (new AA1CFISC2())->find("FIS_OPER = 499 and FIS_LOJ_DST = $bd and substr(FIS_DTA_AGENDA,0,5) = $date")->fetch(true);
            $i = 0;
            if (is_array($aa1cfisc)) {
                foreach ($aa1cfisc as $cfisc) {
                    $i++;
                }
            }
            
            if ($i == 0) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-641446203";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $bd BD, não realizou nenhum lançamento de NF de energia no periodo $d");
                // echo "Loja $bd BD, não realizou nenhum lançamento de NF de energia no periodo $d<br>";
                sleep(5);

            }
        }
    }

    public function agenda608e609() {

        $month = $_GET['date1'].' and '.$_GET['date2'];
        $mes = date("m/Y");
        $mbs = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
        foreach ($mbs as $mb) {
            echo "[[[$mb]]]";
            $content = "
                <p>Bom dia,<br><br>
                <br><br>
                Segue relação de itens movimentados nas agendas 608 e 609 no mês ".date('m')."/2022 para emissão das notas fiscais conforme orientação passada anteriormente.
                Os itens da planilha que estão com a agenda 608, vai dar entrada no recebimento na agenda 628 CFOP 1926. ( a série dessa nota fiscal deve ser 99)
                Os itens da planilha que estão com a agenda 609, vai dar saída no faturamento online na agenda 629.( a série dessa nota fiscal sairá 99)
                Em ambas as notas, a emissões devem ser feitas contra o CNPJ da própria Loja.
                E as notas devem ter o mesmo valor total.
                
                A emissão deve ser feita hoje pra que conste no movimento fiscal desse mês
                
                Fizemos alteração nessas duas agendas, e as séries dessas notas fiscais sairão 99.</p>
                <table border='1'>
                <tr>
                <th>Loja</th>
                <th>Agenda</th>
                <th>Codigo</th>
                <th>Descrição</th>
                <th>Quantidade</th>
                </tr>";
            $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 in (608,609) and ESCHC_DATA3 between $month and ESCHLJC_CODIGO3 = $mb","","ESCHLJC_CODIGO3 LOJA, ESCHC_AGENDA3 AGENDA,ESITC_CODIGO CODIGO, ESITC_DIGITO DIGITO, sum(ENTSAIC_QUANTI_UN) QUANTIDADE")->group("ESCHLJC_CODIGO3, ESCHC_AGENDA3,ESITC_CODIGO, ESITC_DIGITO")->order("ESCHC_AGENDA3")->fetch(true);
            foreach ($ag1iensa as $iensa) {
                $content .= "<tr>
                <td>$iensa->LOJA</td>
                <td>$iensa->AGENDA</td>
                <td>$iensa->CODIGO-$iensa->DIGITO</td>
                <td>".(new AA3CITEM())->find("GIT_COD_ITEM = $iensa->CODIGO")->fetch()->GIT_DESCRICAO."</td>
                <td>".str_replace(".",",",round($iensa->QUANTIDADE,2))."</td>
                </tr>";
            }
            $content .= "</table>";

            $fiscalStore = (new FiscalStores())->find("substr(code,1,length(code)-1) = $mb")->fetch();
            $email = new \SendGrid\Mail\Mail(); 
            $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
            $email->setSubject("EMISSÕES FISCAIS ITENS MOVIMENTADOS NAS AGENDAS 608 E 609 Loja $mb");
            $email->addTo($fiscalStore->email, $fiscalStore->razao);
            $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
            $email->addContent(
                "text/html", $content
            );
            $sendgrid = new \SendGrid("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");
            //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
            //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
            try {
                $response = $sendgrid->send($email);
                print $response->statusCode() . "\n";
                print_r($response->headers());
                print $response->body() . "\n";
            } catch (Exception $e) {
                echo 'Caught exception: '. $e->getMessage() ."\n";
            }

            
        }
        
        $bds = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);

        foreach ($bds as $bd) {
            echo "[[[$bd]]]";
            $content = "
                <p>Bom dia,<br><br>

                Segue relação de itens movimentados nas agendas 608 e 609 no mês ".date('m')."/2022 para emissão das notas fiscais conforme orientação passada anteriormente.
                Os itens da planilha que estão com a agenda 608, vai dar entrada no recebimento na agenda 628 CFOP 1926. ( a série dessa nota fiscal deve ser 99)
                Os itens da planilha que estão com a agenda 609, vai dar saída no faturamento online na agenda 629.( a série dessa nota fiscal sairá 99)
                Em ambas as notas, a emissões devem ser feitas contra o CNPJ da própria Loja.
                E as notas devem ter o mesmo valor total.
                
                A emissão deve ser feita hoje pra que conste no movimento fiscal desse mês
                
                Fizemos alteração nessas duas agendas, e as séries dessas notas fiscais sairão 99.</p>
                <table border='1'>
                <tr>
                <th>Loja</th>
                <th>Agenda</th>
                <th>Codigo</th>
                <th>Descrição</th>
                <th>Quantidade</th>
                </tr>";
            $ag1iensa = (new AG1IENSA2())->find("ESCHC_AGENDA3 in (608,609) and ESCHC_DATA3 between $month and ESCHLJC_CODIGO3 = $bd","","ESCHLJC_CODIGO3 LOJA, ESCHC_AGENDA3 AGENDA,ESITC_CODIGO CODIGO, ESITC_DIGITO DIGITO, sum(ENTSAIC_QUANTI_UN) QUANTIDADE")->group("ESCHLJC_CODIGO3, ESCHC_AGENDA3,ESITC_CODIGO, ESITC_DIGITO")->order("ESCHC_AGENDA3")->fetch(true);
            foreach ($ag1iensa as $iensa) {
                $content .= "<tr>
                <td>$iensa->LOJA</td>
                <td>$iensa->AGENDA</td>
                <td>$iensa->CODIGO-$iensa->DIGITO</td>
                <td>".(new AA3CITEM2())->find("GIT_COD_ITEM = $iensa->CODIGO")->fetch()->GIT_DESCRICAO."</td>
                <td>".str_replace(".",",",round($iensa->QUANTIDADE,2))."</td>
                </tr>";
            }
            $content .= "</table>";

            $fiscalStore = (new FiscalStores())->find("substr(code,1,length(code)-1) = $bd")->fetch();
            $email = new \SendGrid\Mail\Mail(); 
            $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
            $email->setSubject("EMISSÕES FISCAIS ITENS MOVIMENTADOS NAS AGENDAS 608 E 609  Loja $bd");
            $email->addTo($fiscalStore->email, $fiscalStore->razao);
            $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
            $email->addContent(
                "text/html", $content
            );
            $sendgrid = new \SendGrid("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");
            try {
                $response = $sendgrid->send($email);
                print $response->statusCode() . "\n";
                print_r($response->headers());
                print $response->body() . "\n";
            } catch (Exception $e) {
                echo 'Caught exception: '. $e->getMessage() ."\n";
            }

        }
        

    }

    public function cfop5926E1926() {
        $day = "1".date("ymd",strtotime("-1 day"));
        $mbs = array( 5, 8, 11, 12, 13, 14, 15, 17, 19, 25, 27, 32, 33, 35,36, 40, 43, 48, 50, 54, 56, 57, 58, 59, 61, 67, 69, 70, 72,  75, 76, 78, 79, 80, 82, 85, 89, 90);
        foreach ($mbs as $mb) {
            $ag1iensa629 = (new AG1IENSA())->find("ESCHC_SER_NOTA3 != 'REC' and ESCHC_AGENDA3 in (629) and ESCHC_DATA3 = $day and ESCHLJC_CODIGO3 = $mb","","replace(cast(sum(ENTSAIC_QUANTI_UN * ENTSAIC_PRC_UN) as decimal(38,2)),'.',',') VALOR")->fetch();
            $ag1iensa628 = (new AG1IENSA())->find("ESCHC_SER_NOTA3 != 'REC' and ESCHC_AGENDA3 in (628) and ESCHC_DATA3 = $day and ESCHLJC_CODIGO3 = $mb","","replace(cast(sum(ENTSAIC_QUANTI_UN * ENTSAIC_PRC_UN) as decimal(38,2)),'.',',') VALOR")->fetch();

            if ($ag1iensa628->VALOR != $ag1iensa629->VALOR) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-870207485";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $mb MB, Faturou $ag1iensa629->VALOR na agenda 629 e $ag1iensa628->VALOR na agenda 628 no dia ".date("d/m/Y",strtotime("-1 day")));
                sleep(5);
            }
            if (!$ag1iensa629 && !$ag1iensa628) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-870207485";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $bd BD não faturou!");
                sleep(5);
            }

        }
        $bds = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        foreach ($bds as $bd) {
            $ag1iensa629 = (new AG1IENSA())->find("ESCHC_SER_NOTA3 != 'REC' and ESCHC_AGENDA3 in (629) and ESCHC_DATA3 = $day and ESCHLJC_CODIGO3 = $bd","","replace(cast(sum(ENTSAIC_QUANTI_UN * ENTSAIC_PRC_UN) as decimal(38,2)),'.',',') VALOR")->fetch();
            $ag1iensa628 = (new AG1IENSA())->find("ESCHC_SER_NOTA3 != 'REC' and ESCHC_AGENDA3 in (628) and ESCHC_DATA3 = $day and ESCHLJC_CODIGO3 = $bd","","replace(cast(sum(ENTSAIC_QUANTI_UN * ENTSAIC_PRC_UN) as decimal(38,2)),'.',',') VALOR")->fetch();
            if ($ag1iensa628->VALOR != $ag1iensa629->VALOR) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-870207485";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $bd BD, Faturou $ag1iensa629->VALOR na agenda 629 e $ag1iensa628->VALOR na agenda 628 no dia ".date("d/m/Y",strtotime("-1 day")));
                sleep(5);
            }
            if (!$ag1iensa629 && !$ag1iensa628) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-870207485";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $bd BD não faturou!");
                sleep(5);
            }
        }
    }

    public function cfop5926E1926b() {
        $day = "1".date("ymd",strtotime("-1 day"));
        $mbs = array( 5, 8, 11, 12, 13, 14, 15, 17, 19, 25, 27, 32, 33, 35,36, 40, 43, 48, 50, 54, 56, 57, 58, 59, 61, 67, 69, 70, 72,  75, 76, 78, 79, 80, 82, 85, 89, 90);
        foreach ($mbs as $mb) {
            $ag1iensa629 = (new AG1IENSA())->find("ESCHC_SER_NOTA3 != 'REC' and ESCHC_AGENDA3 in (56) and ESCHC_DATA3 = $day and ESCHLJC_CODIGO3 = $mb","","replace(cast(sum(ENTSAIC_QUANTI_UN * ENTSAIC_PRC_UN) as decimal(38,2)),'.',',') VALOR")->fetch();
            $ag1iensa628 = (new AG1IENSA())->find("ESCHC_SER_NOTA3 != 'REC' and ESCHC_AGENDA3 in (6) and ESCHC_DATA3 = $day and ESCHLJC_CODIGO3 = $mb","","replace(cast(sum(ENTSAIC_QUANTI_UN * ENTSAIC_PRC_UN) as decimal(38,2)),'.',',') VALOR")->fetch();

            if ($ag1iensa628->VALOR != $ag1iensa629->VALOR) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-658134971";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $mb MB, Faturou $ag1iensa629->VALOR na agenda 56 e $ag1iensa628->VALOR na agenda 6 no dia ".date("d/m/Y",strtotime("-1 day")));
                sleep(5);
            }
            if (!$ag1iensa629 && !$ag1iensa628) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-870207485";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $bd BD não faturou!");
                sleep(5);
            }

        }
        $bds = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        foreach ($bds as $bd) {
            $ag1iensa629 = (new AG1IENSA())->find("ESCHC_SER_NOTA3 != 'REC' and ESCHC_AGENDA3 in (56) and ESCHC_DATA3 = $day and ESCHLJC_CODIGO3 = $bd","","replace(cast(sum(ENTSAIC_QUANTI_UN * ENTSAIC_PRC_UN) as decimal(38,2)),'.',',') VALOR")->fetch();
            $ag1iensa628 = (new AG1IENSA())->find("ESCHC_SER_NOTA3 != 'REC' and ESCHC_AGENDA3 in (6) and ESCHC_DATA3 = $day and ESCHLJC_CODIGO3 = $bd","","replace(cast(sum(ENTSAIC_QUANTI_UN * ENTSAIC_PRC_UN) as decimal(38,2)),'.',',') VALOR")->fetch();
            if ($ag1iensa628->VALOR != $ag1iensa629->VALOR) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-658134971";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $bd BD, Faturou $ag1iensa629->VALOR na agenda 56 e $ag1iensa628->VALOR na agenda 6 no dia ".date("d/m/Y",strtotime("-1 day")));
                sleep(5);
            }
            if (!$ag1iensa629 && !$ag1iensa628) {
                $logger = new Logger("web");
                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                $tele_channel = "-870207485";
                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                $logger->pushHandler($tele_handler);
                $logger->info("Loja $bd BD não faturou!");
                sleep(5);
            }

        }
    }

    public function CFOPS() {
        $mes = "1".date("ym");
        $mbs = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
        $agse = array(1102,1202,1253,1403,1411,1556,1926,2102);
        $agss = array(5949,5927,5926);
        foreach ($mbs as $mb) {
            foreach ($agss as $ag) {
                $ag1fensa = (new AG1FENSA())->find("ENTSAI_CFOP = $ag and substr(ESCH_DATA,0,5) = $mes and ESCHLJ_CODIGO = $mb and ESCH_SER_NOTA != 'PDV'")->fetch(true);
                if (empty($ag1fensa)) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-796296813";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja $mb MB, não escriturou nenhuma nota com o CFOP $ag.");
                    sleep(5);
                }                
            }
            foreach ($agse as $ag) {
                $ag1fensa = (new AG1FENSA())->find("ENTSAI_CFOP = $ag and substr(ESCH_DATA,0,5) = $mes and ESCL_CODIGO = $mb and ESCH_SER_NOTA != 'PDV'")->fetch(true);
                if (empty($ag1fensa)) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-796296813";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja $mb MB, não escriturou nenhuma nota com o CFOP $ag.");
                    sleep(5);
                }
            }
        }
        $bds = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        foreach ($bds as $bd) {
            foreach ($agss as $ag) {
                $ag1fensa = (new AG1FENSA2())->find("ENTSAI_CFOP = $ag and substr(ESCH_DATA,0,5) = $mes and ESCHLJ_CODIGO = $bd and ESCH_SER_NOTA != 'PDV'")->fetch(true);
                if (empty($ag1fensa)) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-796296813";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja $bd BD, não escriturou nenhuma nota com o CFOP $ag.");
                    sleep(5);
                }
                
            }
            foreach ($agse as $ag) {
                $ag1fensa = (new AG1FENSA2())->find("ENTSAI_CFOP = $ag and substr(ESCH_DATA,0,5) = $mes and ESCL_CODIGO = $bd and ESCH_SER_NOTA != 'PDV'")->fetch(true);
                if (empty($ag1fensa)) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-796296813";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Loja $bd BD, não escriturou nenhuma nota com o CFOP $ag.");
                    sleep(5);
                }
            }

        }
    }

    public function automaticManifestarion() {

        $date35 = date("Y-m-d",strtotime("-35 days"));
        // echodie($date35);
        $notas35 = (new NfSefaz())->find("store_id = 74 and substr(emission_date,1,10) = '$date35'")->fetch(true);
        
        foreach($notas35 as $nota35) {
            $manifestation = false;
            foreach ($nota35->getLogManifestation() as $log) {
                if ($log->manifestation == '210220' || $log->manifestation == '210240') {
                    $manifestation = true;
                }
            }
            if($manifestation === false) {
                if ($nota35->getAG1IENSA() == false) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-710877410";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Nota fiscal $nota35->nfe_number da loja $nota35->store_id, emitida $nota35->emission_date 35 dias atras, não escriturada e nem manifestada. Chave de Acesso: $nota35->access_key");
                    sleep(5);
                }
            }

        }

        $date20 = date("Y-m-d",strtotime("-20 days"));
        $notas20 = (new NfSefaz())->find("store_id = 74 and substr(emission_date,1,10) = '$date20'")->fetch(true);
        foreach($notas20 as $nota20) {
            $manifestation = false;
            foreach ($nota35->getLogManifestation() as $log) {
                if ($log->manifestation == '210220' || $log->manifestation == '210240') {
                    $manifestation = true;
                }
            }

            if($nota20->getUF() == 'BA' && $manifestation === false) {
                if ($nota20->getAG1IENSA() == false) {
                    // -710877410
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-710877410";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Nota fiscal $nota20->nfe_number da loja $nota20->store_id, emitida $nota20->emission_date 35 dias atras, não escriturada e nem manifestada. Chave de Acesso: $nota20->access_key");
                    sleep(5);
                }
            }

        }
    }

    public function fornecedoresReports() {
        $nfReports = (new NfReport())->find("created_at between '2022-01-01' and '2022-06-27' and user_id != 0")->fetch(true);
        // dd($nfReports);
        foreach($nfReports as $nfReport) {
            $fornecedores[$nfReport->getNfReceipt()->getFornecedor] = $nfReport->getNfReceipt()->getFornecedor;
            $quantidades[$nfReport->getNfReceipt()->getFornecedor] += 1;
            $cnpj[$nfReport->getNfReceipt()->getFornecedor] = $nfReport->getNfReceipt()->getCnpjFornecedor;

        }
        
        echo "<table>
                <thead>
                    <tr>
                        <th>CNPJ</th>
                        <th>Fornecedor</th>
                        <th>Quantidade Reportada</th>
                    </tr>
                </thead>
                <tbody>
             ";
        foreach ($fornecedores as $fornecedor) {
            echo "<tr>
                <td>".$cnpj[$fornecedor]."</td>
                <td>$fornecedor</td>
                <td>".$quantidades[$fornecedor]."</td>
             </tr>";
        }
        echo "</tbody>
                </table>";

    }

    public function NFCEAudit() {
        $date = inputDateToRMSDate(date("Y-m-d",strtotime("-1 day")));
        $ag1pdvcc2 = (new AG1PDVCC())->find("PDVCC_DTA = $date","","PDVCC_LOJ")->group("PDVCC_LOJ")->order("PDVCC_LOJ")->fetch(true);
        
        foreach ($ag1pdvcc2 as $pdvcc2) {
            $ag1pdvcc = (new AG1PDVCC())->find("PDVCC_DTA = $date and PDVCC_LOJ = $pdvcc2->PDVCC_LOJ")->order("PDVCC_DTA, PDVCC_SER, PDVCC_NTA")->fetch(true);
            


            foreach ($ag1pdvcc as $pdvcc) {
                
                $array[$pdvcc->PDVCC_SER][$pdvcc->PDVCC_NTA] = $pdvcc;
            }
            


            foreach ($array as $data) { //giro das series
 
                $i = 1;
                $K = 0;
                $primeira = true;
                foreach ($data as $number) { //giro das notas
                    $notaAtual = $number->PDVCC_NTA;
                    if ($primeira == true) {
                        $notaAnterior = $number->PDVCC_NTA;
                        $primeira = false;
                    }
                    
                    if ($notaAtual != $notaAnterior + 1) {                           
                            for ($j=$notaAnterior+1;$j<$notaAtual;$j++) {
                                echo "Loja $number->PDVCC_LOJ Número $j, Serie $number->PDVCC_SER chave $number->PDVCC_CHV_NFC dia ".dateRMSToDateI($number->PDVCC_DTA)."<br>";
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = "-713363441";
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info("Loja $number->PDVCC_LOJ Número $j, Serie $number->PDVCC_SER dia ".dateRMSToDateI($number->PDVCC_DTA));
                                sleep(5);
                            }
                            
                            $j = 0;

                             
                            
                    }
                    $notaAnterior = $notaAtual;

                }
                $notaAnterior = null;
                $notaAtual = null;
                $primeira = true;
            }
            $i=0;
            $comparacao = null;
            $array = null;
        }
        die;
        /////////////////////////////////////////

        $ag1pdvcc2 = (new AG1PDVCC2())->find("PDVCC_DTA = $date","","PDVCC_LOJ")->group("PDVCC_LOJ")->order("PDVCC_LOJ")->fetch(true);
        foreach ($ag1pdvcc2 as $pdvcc2) {
            $ag1pdvcc = (new AG1PDVCC2())->find("PDVCC_DTA = $date and PDVCC_LOJ = $pdvcc2->PDVCC_LOJ")->order("PDVCC_DTA, PDVCC_SER, PDVCC_NTA")->fetch(true);
            

            foreach ($ag1pdvcc as $pdvcc) {
                
                $array[$pdvcc->PDVCC_SER][$pdvcc->PDVCC_NTA] = $pdvcc;
            }



            foreach ($array as $data) { //giro das series
 
                $i = 1;
                $K = 0;
                $primeira = true;
                foreach ($data as $number) { //giro das notas
                    $notaAtual = $number->PDVCC_NTA;
                    if ($primeira == true) {
                        $notaAnterior = $number->PDVCC_NTA;
                        $primeira = false;
                    }
                    
                    if ($notaAtual != $notaAnterior + 1) { 
                        if (($notaAtual - $notaAnterior) > 10) {
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-716400287";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info("Loja $number->PDVCC_LOJ Reexportar dia ".dateRMSToDateI($number->PDVCC_DTA));
                            sleep(5);
                        } else {
                            for ($j=$notaAnterior+1;$j<$notaAtual;$j++) {
                                $logger = new Logger("web");
                                $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                $tele_channel = "-716400287";
                                $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                $logger->pushHandler($tele_handler);
                                $logger->info("Loja $number->PDVCC_LOJ Número $j, Serie $number->PDVCC_SER dia ".dateRMSToDateI($number->PDVCC_DTA));
                                sleep(5);
                            }
                            
                            $j = 0;
                        }                
                    }
                    $notaAnterior = $notaAtual;

                }
                $notaAnterior = null;
                $notaAtual = null;
                $primeira = true;
            }
            $i=0;
            $comparacao = null;
            $array = null;
        }
        
    }

    public function margemConsinco() {
        $mapNotaFiscal = (new MlfNotaFiscal())->find("dtaentrada = to_date('".date("d/m/Y")."', 'DD/MM/YYYY') AND nroempresa in (27, 29, 33, 2)")->order("NROEMPRESA,NUMERONF")->fetch(true);
        
        foreach ($mapNotaFiscal as $notaFiscal) {
            foreach ($notaFiscal->mlfNfitem as $mlfNfitem) {
                
                $marginDate = date("Y-m-d");
                $marginVerification = (new MarginVerification())->find("concat(store_id,nf_number,code,substr(created_at,1,10)) = :snc","snc=$notaFiscal->NROEMPRESA$notaFiscal->NUMERONF$mlfNfitem->SEQPRODUTO$marginDate")->count();

                if ($marginVerification == 0) {
                    
                    if ($mlfNfitem->custo > 0) {
                        $custo = $mlfNfitem->custo;
                        $preco = $mlfNfitem->preco->PRECO;
                        if ($preco == 0) {
                            $preco2 = $custo;
                        } else {
                            $preco2 = $preco;
                        }
                        $ICMS = $mlfNfitem->ICMS;
                        $PISCOFINS = $mlfNfitem->pisCofins;
                        $margem = round(100-((($custo/$preco2) * 100)+(($PISCOFINS-($PISCOFINS * ($ICMS/100)))+$ICMS)),2);
                        $newRegister = new MarginVerification();
                        $newRegister->store_id = $notaFiscal->NROEMPRESA;
                        $newRegister->nf_number = $notaFiscal->NUMERONF;
                        $newRegister->provider = $notaFiscal->NROEMPRESA;
                        $newRegister->code = $mlfNfitem->SEQPRODUTO;
                        $newRegister->description = $mlfNfitem->mapProduto()->DESCCOMPLETA;
                        $newRegister->cost = $custo;
                        $newRegister->price = $preco;
                        $newRegister->margin = $margem;
                        $newRegister->user = "CONSINCO";
                        $newRegisterId =  $newRegister->save();


                        if ($margem > 50 || $margem < 0) {
                            switch ($notaFiscal->NROEMPRESA) {
                                case '27':
                                    $logger = new Logger("web");
                                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                    $tele_channel = "-783006692";
                                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                    $logger->pushHandler($tele_handler);
                                    $logger->info("Loja $notaFiscal->NROEMPRESA \n Número da nota $notaFiscal->NUMERONF \n Serie $notaFiscal->SERIENF \n Data da Entrada $notaFiscal->DTAENTRADA \n CFOP $mlfNfitem->CFOP \n Codigo $mlfNfitem->SEQPRODUTO \n Descrição ".$mlfNfitem->mapProduto()->DESCCOMPLETA." \n quantidade $mlfNfitem->QUANTIDADE   \n Custo $custo \n Preço  $preco \n ICMS ".$ICMS." \n PIS/COFINS $PISCOFINS \n Margem $margem.");
                                    sleep(5);
                                    break; 
                                case '29':
                                    $logger = new Logger("web");
                                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                    $tele_channel = "-651359587";
                                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                    $logger->pushHandler($tele_handler);
                                    $logger->info("Loja $notaFiscal->NROEMPRESA \n Número da nota $notaFiscal->NUMERONF \n Serie $notaFiscal->SERIENF \n Data da Entrada $notaFiscal->DTAENTRADA \n CFOP $mlfNfitem->CFOP \n Codigo $mlfNfitem->SEQPRODUTO \n Descrição ".$mlfNfitem->mapProduto()->DESCCOMPLETA." \n quantidade $mlfNfitem->QUANTIDADE   \n Custo $custo \n Preço  $preco \n ICMS ".$ICMS." \n PIS/COFINS $PISCOFINS \n Margem $margem.");
                                    sleep(5);
                                    break; 
                                case '33':
                                    $logger = new Logger("web");
                                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                                    $tele_channel = "-650109861";
                                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                                    $logger->pushHandler($tele_handler);
                                    $logger->info("Loja $notaFiscal->NROEMPRESA \n Número da nota $notaFiscal->NUMERONF \n Serie $notaFiscal->SERIENF \n Data da Entrada $notaFiscal->DTAENTRADA \n CFOP $mlfNfitem->CFOP \n Codigo $mlfNfitem->SEQPRODUTO \n Descrição ".$mlfNfitem->mapProduto()->DESCCOMPLETA." \n quantidade $mlfNfitem->QUANTIDADE   \n Custo $custo \n Preço  $preco \n ICMS ".$ICMS." \n PIS/COFINS $PISCOFINS \n Margem $margem.");
                                    sleep(5);
                                    break;
                                
                            }
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "-772432189";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                            $logger->pushHandler($tele_handler);
                            $logger->info("Loja $notaFiscal->NROEMPRESA \n Número da nota $notaFiscal->NUMERONF \n Serie $notaFiscal->SERIENF \n Data da Entrada $notaFiscal->DTAENTRADA \n CFOP $mlfNfitem->CFOP \n Codigo $mlfNfitem->SEQPRODUTO \n Descrição ".$mlfNfitem->mapProduto()->DESCCOMPLETA." \n quantidade $mlfNfitem->QUANTIDADE   \n Custo $custo \n Preço  $preco \n ICMS ".$ICMS." \n PIS/COFINS $PISCOFINS \n Margem $margem.");
                            sleep(5);
                        }
                    }
                   
                }
                
                
            }
            
        }
    }

    public function icmsAjustado($data) {
        $date = "202206";
        $store = $data["store"];
        define("AA1FFISI", $date, true);
        $lojas = array($store);
        $arquivo = "icmsAjustado/$date-$store.json";
        foreach($lojas as $loja) {
            $aa1ffiscs = (new AA1FFISC())->find("(FISC_DST = $loja AND FISC_T03_ENS = 'E') or (FISC_ORG = $loja AND FISC_T03_ENS = 'S')")->fetch(true);
            foreach($aa1ffiscs as $aa1ffisc) {
                foreach($aa1ffisc->AA1FFISI as $aa1ffisi) {
                    if ($aa1ffisi->FISI_CFO == 1102 or $aa1ffisi->FISI_CFO == 1403 or $aa1ffisi->FISI_CFO == 2102 or $aa1ffisi->FISI_CFO == 2403 or $aa1ffisi->FISI_CFO == 1910 or $aa1ffisi->FISI_CFO == 2910) {
                        $array['COMPRAS'] += $aa1ffisi->FISI_CTB_VAL;
                    } else if ($aa1ffisi->FISI_CFO == 5102 or $aa1ffisi->FISI_CFO == 5405) {
                        $array['FATURAMENTO'] += $aa1ffisi->FISI_CTB_VAL;
                    }

                    if ($aa1ffisi->FISC_T03_ENS == 'E') {
                        $array['BCENTRADA'] += $aa1ffisi->FISI_ICM_BAS;
                        $array['VLENTRADA'] += $aa1ffisi->FISI_ICM_BAS;
                    } else if ($aa1ffisi->FISC_T03_ENS == 'S') {
                        $array['BCSAIDAS'] += $aa1ffisi->FISI_ICM_BAS;
                        $array['VLSAIDAS'] += $aa1ffisi->FISI_ICM_BAS;
                    }
                }
            }
            $ag3vnfccs = (new AG3VNFCC())->find("NFCC_ORG = $loja and NFCC_SIT = '0'")->fetch(true);
    
            foreach($ag3vnfccs as $ag3vnfcc) { //ag3vnfci
                foreach($ag3vnfcc->AG3VNFCI as $ag3vnfci) {
                    if ($ag3vnfci->NFCI_CFO == 5102 or $ag3vnfci->NFCI_CFO == 5405) {
                        $array['FATURAMENTO'] += $ag3vnfci->NFCI_CTB_VAL;
                        $array['BCSAIDAS'] += $aa1ffisi->NFCI_ICM_BAS;
                        $array['VLSAIDAS'] += $aa1ffisi->NFCI_ICM_BAS;
                    }
                }
            }

        }
        $json = json_encode($array);
        $file = fopen(__DIR__ . '/' . $arquivo,'w');
        fwrite($file, $json);
        fclose($file);
        
        
    }

    public function consultaVendas() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $user = $usuario->id;
        $class = $usuario->class;
        $storeId = $usuario->store_code;
        $produto = (new AA3CCEAN())->find("EAN_COD_EAN = '".$_POST['EAN']."'")->fetch();


        $codigo = substr($produto->EAN_COD_PRO_ALT,0,strlen($produto->EAN_COD_PRO_ALT)-1);
        $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM = $codigo")->fetch();
        $dimPer = (new DIMPER())->getIdDt(date("d/m/Y"));
        $aggFlsProd = (new AGGFLSPROD())->find("ID_DT = $dimPer and CD_PROD = $codigo and CD_FIL = $storeId")->fetch();
        $qtdDia = $aggFlsProd->QTD_VDA;
        $ag1iensaMes = (new AG1IENSA())->find("ESCHC_AGENDA3 in (44,601) and ESCHLJC_CODIGO3 = $storeId and ESITC_CODIGO = $codigo and substr(ESCHC_DATA,0,5) = 1".date("ym"),"","sum(ENTSAIC_QUANTI_UN) QUANTIDADE")->fetch();
        $qtdMes = $ag1iensaMes->QUANTIDADE+$qtdDia;
        $ag1iensaAno = (new AG1IENSA())->find("ESCHC_AGENDA3 in (44,601) and ESCHLJC_CODIGO3 = $storeId and ESITC_CODIGO = $codigo and substr(ESCHC_DATA,0,3) = 1".date("y"),"","sum(ENTSAIC_QUANTI_UN) QUANTIDADE")->fetch();
        $qtdAno = $ag1iensaAno->QUANTIDADE+$qtdDia;
        echo $this->view->render("client/consultaVendas", ["id" => "Auditoria de vencimentos | " . SITE,"dia" => $qtdDia,"mes" => $qtdMes, "ano" => $qtdAno, "aa3citem" => $aa3citem
        ]);

    }

    public function diferençaNFCE() {
        $date1 = "1".date('ym',strtotime('-1 month'))."01";
        $date2 = "1".date('ym',strtotime('-1 month'))."31";
        $date = date('Ym',strtotime('-1 month'));
        define("AA1FFISI", $date, true);
        $mbs = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
        // 
        foreach ($mbs as $store) {
            $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 and ESCHC_DATA3 between $date1 and $date2 and ESCHLJC_CODIGO3 = $store","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) valor, ESCHC_DATA3 DATAS")->group("ESCHC_DATA3")->order("ESCHC_DATA3")->fetch(true);
  
            foreach ($ag1iensa as $iensa) {

                if ($iensa->ag3vnfcc($store)->VALOR != $iensa->VALOR) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-743198294";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Diferença na loja $store no dia $iensa->DATAS.");
                    sleep(5);
                }

            }
        }
        $bds = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        foreach ($bds as $store) {
            $ag1iensa = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 and ESCHC_DATA3 between $date1 and $date2 and ESCHLJC_CODIGO3 = $store","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) valor, ESCHC_DATA3 DATAS")->group("ESCHC_DATA3")->order("ESCHC_DATA3")->fetch(true);
  
            foreach ($ag1iensa as $iensa) {

                if ($iensa->ag3vnfcc($store)->VALOR != $iensa->VALOR) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-743198294";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Diferença na loja $store no dia $iensa->DATAS.");
                    sleep(5);
                }

            }
        }

    }

    public function diferencaNFCE2() {
        $date1 = "1".date('ym')."01";
        $date2 = "1".date('ym')."31";
        $date = date('Ym');
        define("AA1FFISI", $date, true);
        $mbs = array(5,8,11,12,13,14,15,17,19,25,33,35,36,40,43,46,48,50,54,56,57,58,59,61,67,69,72,74,75,70,76,78,79,80,82,85,90,89,92,113,114,112);
        // 
        foreach ($mbs as $store) {
            $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 and ESCHC_DATA3 between $date1 and $date2 and ESCHLJC_CODIGO3 = $store","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) valor, ESCHC_DATA3 DATAS")->group("ESCHC_DATA3")->order("ESCHC_DATA3")->fetch(true);
  
            foreach ($ag1iensa as $iensa) {

                if ($iensa->ag3vnfcc($store)->VALOR != $iensa->VALOR) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-743198294";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Diferença na loja $store no dia $iensa->DATAS.");
                    sleep(5);
                }

            }
        }
        $bds = array(10,18,20,21,23,49,83,84,86,91,93,94,95,101,102,108,110,111,115,118);
        foreach ($bds as $store) {
            $ag1iensa = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 and ESCHC_DATA3 between $date1 and $date2 and ESCHLJC_CODIGO3 = $store","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) valor, ESCHC_DATA3 DATAS")->group("ESCHC_DATA3")->order("ESCHC_DATA3")->fetch(true);
  
            foreach ($ag1iensa as $iensa) {

                if ($iensa->ag3vnfcc($store)->VALOR != $iensa->VALOR) {
                    $logger = new Logger("web");
                    $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                    $tele_channel = "-743198294";
                    $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                    $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
                    $logger->pushHandler($tele_handler);
                    $logger->info("Diferença na loja $store no dia $iensa->DATAS.");
                    sleep(5);
                }

            }
        }

    }

    public function testeSOAP() {
        $curl = (new CurlSoap(__DIR__."/../Certs/08816989000118_priKEY.pem",__DIR__."/../Certs/08816989000118_pubKEY.pem",__DIR__."/../Certs/08816989000118_certKEY.pem"));
        $urlservice = "https://dfe-servico.svrs.rs.gov.br/ws/ccgConsGTIN/ccgConsGTIN.asmx?WSDL";
        
        $body =  '<ccgConsGTIN xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/ccgConsGtin">';
        $body .= '<nfeDadosMsg><consGTIN versao="1.00" xmlns="http://www.portalfiscal.inf.br/nfe"><GTIN>17891024134709</GTIN></consGTIN>';
        $body .= '</nfeDadosMsg></ccgConsGTIN>';
    

        $method = 'POST';
        $curl->send2($urlservice, $namespace, $body, $method);
        $resposta = $curl->soapDebug;
        dd($resposta);

    }

    public function rodrigo() {
        $store = 118;
        $nfNumber = 2224;
        $tables = (new TABLE())->find()->fetch(true);
        $sum = (new TABLE())->find("","","sum(CUSTO * QUANTIDADE) VALOR")->fetch();
        $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = $store")->fetch();
        $aa2ctipo2 = (new AA2CTIPO2())->find("TIP_CGC_CPF = 41331360000153")->fetch();
        if (!$aa2ctipo2) {
            $data['success'] = false;
            $data['message'] = "CNPJ não cadastrado.";
            echo json_encode($data);
            die;
        }
        $aa1agedt = new AA1AGEDT2();
        $aa1agedt->addAa1agedt('1'.date("ymd"),$aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO,$nfNumber,$aa2ctipo2->TIP_CODIGO,$sum->VALOR);         
        $i = 1;         
        foreach($tables as $table) {
            if ($i > 998) {
                break;
            }
            $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = $store")->fetch();
            $aa3ccean = (new AA3CCEAN2())->find("EAN_COD_EAN = $table->EAN")->fetch();
            $ag2detnt = new AG2DETNT2();
            $cfop = '1949';
            $ag2detnt->addAg2detnt($cfop,$aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO,str_pad($aa3ccean->aa3citem()->GIT_COD_FOR,7,'0',STR_PAD_RIGHT).str_pad($aa3ccean->EAN_COD_PRO_ALT,8,0,STR_PAD_LEFT),$aa2ctipo2->TIP_CODIGO,$table->CUSTO,'1'.date("ymd"),$nfNumber,$table->QUANTIDADE,$i);
            $i++;
        }
    }

    public function horti() {
        $store = (new Store())->find("code = ".$_GET['store'])->fetch();
        if ($store->group_id == 1) {
            $aa2cestq = (new VIEW_HORTI())->find("(GET_DT_ULT_FAT = 1".date("ym",strtotime("-1 month"))." or GET_DT_ULT_FAT = 1".date("ym").") and substr(GET_COD_LOCAL,0,LENGTH(GET_COD_LOCAL)-1) = ".$_GET['store']." and GIT_SECAO = 5")->order("GIT_DESCRICAO")->fetch(true);
        } else {
            $aa2cestq = (new VIEW_HORTI2())->find("(GET_DT_ULT_FAT = 1".date("ym",strtotime("-1 month"))." or GET_DT_ULT_FAT = 1".date("ym").") and substr(GET_COD_LOCAL,0,LENGTH(GET_COD_LOCAL)-1) = ".$_GET['store']." and GIT_SECAO = 5")->order("GIT_DESCRICAO")->fetch(true);
        }
        

        echo $this->view->render("client/horti", ["id" => "Comercial Horti | " . SITE, "aa2cestq" => $aa2cestq]);
    }

    public function cadastrosUsuarios() {
        $date = '1'.date('ymd');
        $aa3citem = (new AA3CITEM())->find("'1'||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),5,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),3,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),1,2) = $date")->fetch(true);

        foreach($aa3citem as $item) {
            $log = $item->getLog();
            $operadores[$log->LOGIT_USUARIO] = $log->LOGIT_USUARIO;
            $data[$log->LOGIT_USUARIO]['quantidade'] ++;
        }


        $aa3citem2 = (new AA3CITEM2())->find("'1'||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),5,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),3,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),1,2) = $date")->fetch(true);
        foreach($aa3citem2 as $item2) {
            $log = $item2->getLog();
            $operadores[$log->LOGIT_USUARIO] = $log->LOGIT_USUARIO;
            $data[$log->LOGIT_USUARIO]['quantidade'] ++;
        }

        foreach($operadores as $operador) {
            $logger = new Logger("web");
            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
            $tele_channel = '-879780680';
            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
            $tele_handler->setFormatter(new LineFormatter("%message%",null,true));
            $logger->pushHandler($tele_handler);
            $logger->info($operador.' quantidade '.$data[$operador]['quantidade']);
            sleep(5);
        }
    }

    public function test() {
        if ($_GET['base'] == 'BD') {
            $ag1cgfat = (new AG1CGFAT2())->find("","","MAX(GFT_PEDIDO)+1 PEDIDO")->fetch();
            $pedido = $ag1cgfat->PEDIDO;
            $store = $_GET['store'];
            $agenda = 629;
            $stmt = Connect3::getInstance()->prepare("INSERT INTO AG1CGFAT
                (GFT_LOJA,
                GFT_PEDIDO,
                GFT_LOJA_1,
                GFT_ROMANEIO_1,
                GFT_ROTA_1,
                GFT_SUBROTA_1,
                GFT_LOCALIDADE_1,
                GFT_CLIENTE,
                GFT_PEDIDO_1,
                GFT_LOJA_2,
                GFT_ROTA_2,
                GFT_SUBROTA_2,
                GFT_LOCALIDADE_2,
                GFT_ROMANEIO_2,
                GFT_LOJA_3,
                GFT_CLIENTE_2,
                GFT_PEDIDO_2,
                GFT_VENDEDOR,
                GFT_LOJA_ORIGEM,
                GFT_LOJA_VENDA,
                GFT_CGC_CPF,
                GFT_TRANSPORTADORA,
                GFT_CONDICAO,
                GFT_TPO_PAGAMENTO,
                GFT_FRETE,
                GFT_TOTAL_FAT,
                GFT_TOTAL_PED,
                GFT_OBSERVACAO,
                GFT_COD_FAT,
                GFT_DATA,
                GFT_HORA,
                GFT_SUSPENSO,
                GFT_QTD_HORAS,
                GFT_FLAGS,
                GFT_TIPO,
                GFT_ORIGEM,
                GFT_OPCAO,
                GFT_PESO,
                GFT_VOLUME,
                GFT_AGENDA,
                GFT_TIP_LOJ_CLI,
                GFT_LOJ_CONTR_ESTQ,
                GFT_TBC_INTG_07,
                GFT_TBC_INTG_11,
                GFT_TBC_INTG_17,
                GFT_ESTLOC,
                GFT_DESCON_IS)
            VALUES( '$store',
                    '$pedido',
                    '$store',
                    '$pedido',
                    0,
                    0,
                    0,
                    '$store',
                    '$pedido',
                    '$store',
                    0,
                    0,
                    0,
                    '$pedido',
                    '$store',
                    '$store',
                    '$pedido',
                    750069,
                    '$store',
                    '$store',
                    0,
                    0,
                    900,
                    1,
                    1,
                    0,
                    0,
                    '',
                    0,'".
                    date('ymd')."','".
                    date('iH')."',
                    0,
                    0,
                    0,
                    0,
                    4,
                    0,
                    0,
                    0,
                    '$agenda',
                    'L',
                    'S',
                    'N',
                    'T',
                    'N',
                    'N',
                    0)");
            $stmt->execute();
            $aa1agedt = new AA1AGEDT2();
            $aa1agedt->addAa1agedt('1'.date("ymd"),$store,123123,substr($store,0,strlen($store)-1),1000,628,194,99);
            $month = $_GET['data1'].' and '.$_GET['data2'];
            $ag1iensa = (new AG1IENSA2())->find("ESCHC_AGENDA3 in (608,609) and ESCHC_DATA3 between $month and ESCHLJC_CODIGO3 = ".substr($store,0,strlen($store)-1),"","ESCHLJC_CODIGO3, ESCHC_AGENDA3,ESITC_CODIGO, ESITC_DIGITO, sum(ENTSAIC_QUANTI_UN) QUANTIDADE")->group("ESCHLJC_CODIGO3, ESCHC_AGENDA3,ESITC_CODIGO, ESITC_DIGITO")->order("ESCHC_AGENDA3")->fetch(true);
            $i = count($ag1iensa);

            foreach ($ag1iensa as $iensa) {
                if ($iensa->ESCHC_AGENDA3 == 609) {
                    $aa3citem = (new AA3CITEM2())->find("GIT_COD_ITEM = $iensa->ESITC_CODIGO")->fetch();
                    $preco = $iensa->getPrice2;
                    if ($preco == 0) {
                        $preco = 1;
                    }
                    $stmt = Connect3::getInstance()->prepare("INSERT INTO AG1CDFAT
                        (DIG_LOJA,
                        DIG_ROTA,
                        DIG_SUBROTA,
                        DIG_LOCALIDADE,
                        DIG_PEDIDO,
                        DIG_FIGURA,
                        DIG_COD_ITEM,
                        DIG_DIGITO,
                        DIG_GALPAO,
                        DIG_RUA,
                        DIG_NRO,
                        DIG_APT,
                        DIG_LOJA_1,
                        DIG_ROMANEIO_1,
                        DIG_GALPAO_1,
                        DIG_RUA_1,
                        DIG_NRO_1,
                        DIG_APT_1,
                        DIG_COD_ITEM_1,
                        DIG_DIGITO_1,
                        DIG_PEDIDO_1,
                        DIG_LOJA_2,
                        DIG_ROMANEIO_2,
                        DIG_COD_ITEM_2,
                        DIG_DIGITO_2,
                        DIG_GALPAO_2,
                        DIG_RUA_2,
                        DIG_NRO_2,
                        DIG_APT_2,
                        DIG_PEDIDO_2,
                        DIG_QTD_BONI,
                        DIG_QTD_VEND,
                        DIG_QTD_FAT,
                        DIG_TPO_EMB,
                        DIG_QTD_EMB,
                        DIG_DESCONTO,
                        DIG_MOEDA,
                        DIG_PRECO,
                        DIG_PRECO_MOEDA,
                        DIG_NIVEL_PRECO,
                        DIG_NIVEL_PROMO,
                        DIG_ALIQ_ISS,
                        DIG_ALIQ_IR,
                        DIG_ALIQ_AIRE,
                        DIG_ALIQ_ICM,
                        DIG_ICM_FONTE,
                        DIG_BASE_REDUZ,
                        DIG_SUBS_TRIB,
                        DIG_CRED_PRES,
                        DIG_COD_GIA,
                        DIG_SITUACAO,
                        DIG_VAL_PAUTA,
                        DIG_OPERACAO,
                        DIG_VD_CUSTO,
                        DIG_TRANS_SAI_CUSTO,
                        DIG_DEVOLUCAO_CUSTO,
                        DIG_EMB_FOR,
                        DIG_PESO_UN,
                        DIG_VOLU_UN,
                        DIG_COMISSAO,
                        DIG_VAL_BONUS,
                        DIG_NUM_NFF_PDV,
                        DIG_SERIE,
                        DIG_VALOR_IPI,
                        DIG_CONDICAO,
                        DIG_AGENDA,
                        DIG_TIP_LOJ_CLI,
                        DIG_LOJ_CONTR_ESTQ,
                        DIG_TBC_INTG_07,
                        DIG_TBC_INTG_11,
                        DIG_TBC_INTG_17,
                        DIG_ESTLOC,
                        DIG_FLAG,
                        DIG_DATA,
                        DIG_HORA)
                        VALUES (
                        $store,
                        '0',
                        '0',
                        '0',
                        $pedido,
                        $aa3citem->GIT_NAT_FISCAL,
                        $aa3citem->GIT_COD_ITEM,
                        $aa3citem->GIT_DIGITO,
                        '0',
                        '0',
                        '0',
                        '0',
                        $store,
                        $pedido,
                        '0',
                        '0',
                        '0',
                        '0',
                        $aa3citem->GIT_COD_ITEM,
                        $aa3citem->GIT_DIGITO,
                        $pedido,
                        $store,
                        $pedido,
                        $aa3citem->GIT_COD_ITEM,
                        $aa3citem->GIT_DIGITO,
                        '0',
                        '0',
                        '0',
                        '0',
                        $pedido,
                        '0',
                        $iensa->QUANTIDADE,
                        $iensa->QUANTIDADE,
                        '$aa3citem->GIT_TPO_EMB_VENDA',
                        1,
                        0,
                        0,
                        $preco,
                        0,
                        4,
                        4,
                        0,
                        0,
                        6,".
                        $iensa->getICMSAliquot($iensa->ESCHLJC_CODIGO3).",
                        0,
                        0,
                        0,
                        0,
                        0,
                        90,
                        0,
                        3,
                        2,
                        0,
                        0,
                        1,
                        .2,
                        .000468,
                        0,
                        0,
                        0,
                        '    ',
                        0,
                        900,
                        $agenda,
                        'L',
                        'S',
                        'N',
                        'T',
                        'N',
                        'N',
                        0,
                        '1".date('ymd')."','".
                        date('His')."')");
                        
                        $stmt->execute();
                } else if ($iensa->ESCHC_AGENDA3 == 608) {
                    $ag2detnt = new AG2DETNT2();
                    $preco = $iensa->getPrice2;
                    if ($preco == 0) {
                        $preco = 1;
                    }
                    $aa3citem = (new AA3CITEM2())->find("GIT_COD_ITEM = $iensa->ESITC_CODIGO")->fetch();
                    $ag2detnt->addAg2detnt(1926,$store,str_pad($aa3citem->GIT_COD_FOR,7,'0',STR_PAD_RIGHT).str_pad($aa3citem->GIT_COD_ITEM.$aa3citem->GIT_DIGITO,8,0,STR_PAD_LEFT),substr($store,0,strlen($store)-1),$iensa->getPrice2,'1'.date("ymd"),123123,$iensa->QUANTIDADE,$i,628,90);
                }
                
    
            }  
        } else if ($_GET['base'] == 'MB') {
            $ag1cgfat = (new AG1CGFAT())->find("","","MAX(GFT_PEDIDO)+1 PEDIDO")->fetch();
            $pedido = $ag1cgfat->PEDIDO;
            $store = $_GET['store'];
            $agenda = 629;
            $stmt = Connect2::getInstance()->prepare("INSERT INTO AG1CGFAT
                (GFT_LOJA,
                GFT_PEDIDO,
                GFT_LOJA_1,
                GFT_ROMANEIO_1,
                GFT_ROTA_1,
                GFT_SUBROTA_1,
                GFT_LOCALIDADE_1,
                GFT_CLIENTE,
                GFT_PEDIDO_1,
                GFT_LOJA_2,
                GFT_ROTA_2,
                GFT_SUBROTA_2,
                GFT_LOCALIDADE_2,
                GFT_ROMANEIO_2,
                GFT_LOJA_3,
                GFT_CLIENTE_2,
                GFT_PEDIDO_2,
                GFT_VENDEDOR,
                GFT_LOJA_ORIGEM,
                GFT_LOJA_VENDA,
                GFT_CGC_CPF,
                GFT_TRANSPORTADORA,
                GFT_CONDICAO,
                GFT_TPO_PAGAMENTO,
                GFT_FRETE,
                GFT_TOTAL_FAT,
                GFT_TOTAL_PED,
                GFT_OBSERVACAO,
                GFT_COD_FAT,
                GFT_DATA,
                GFT_HORA,
                GFT_SUSPENSO,
                GFT_QTD_HORAS,
                GFT_FLAGS,
                GFT_TIPO,
                GFT_ORIGEM,
                GFT_OPCAO,
                GFT_PESO,
                GFT_VOLUME,
                GFT_AGENDA,
                GFT_TIP_LOJ_CLI,
                GFT_LOJ_CONTR_ESTQ,
                GFT_TBC_INTG_07,
                GFT_TBC_INTG_11,
                GFT_TBC_INTG_17,
                GFT_ESTLOC,
                GFT_DESCON_IS)
                VALUES( '$store',
                    '$pedido',
                    '$store',
                    '$pedido',
                    0,
                    0,
                    0,
                    '$store',
                    '$pedido',
                    '$store',
                    0,
                    0,
                    0,
                    '$pedido',
                    '$store',
                    '$store',
                    '$pedido',
                    750069,
                    '$store',
                    '$store',
                    0,
                    0,
                    900,
                    1,
                    1,
                    0,
                    0,
                    '',
                    0,'".
                    date('ymd')."','".
                    date('iH')."',
                    0,
                    0,
                    0,
                    0,
                    4,
                    0,
                    0,
                    0,
                    '$agenda',
                    'L',
                    'S',
                    'N',
                    'T',
                    'N',
                    'N',
                    0)");
            $stmt->execute();
            $aa1agedt = new AA1AGEDT();
            $aa1agedt->addAa1agedt('1'.date("ymd"),$store,123123,substr($store,0,strlen($store)-1),1000,628,194);
            $month = $_GET['data1'].' and '.$_GET['data2'];
            $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 in (608,609) and ESCHC_DATA3 between $month and ESCHLJC_CODIGO3 = ".substr($store,0,strlen($store)-1),"","ESCHLJC_CODIGO3, ESCHC_AGENDA3,ESITC_CODIGO, ESITC_DIGITO, sum(ENTSAIC_QUANTI_UN) QUANTIDADE")->group("ESCHLJC_CODIGO3, ESCHC_AGENDA3,ESITC_CODIGO, ESITC_DIGITO")->order("ESCHC_AGENDA3")->fetch(true);
            // dd($ag1iensa);
            $i = count($ag1iensa);
            foreach ($ag1iensa as $iensa) {
                if ($iensa->ESCHC_AGENDA3 == 609) {
                    $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM = $iensa->ESITC_CODIGO")->fetch();
                    $preco = $iensa->getPrice2;
                    if ($preco == 0) {
                        $preco = 1;
                    }
 
                    $stmt = Connect2::getInstance()->prepare("INSERT INTO AG1CDFAT
                        (DIG_LOJA,
                        DIG_ROTA,
                        DIG_SUBROTA,
                        DIG_LOCALIDADE,
                        DIG_PEDIDO,
                        DIG_FIGURA,
                        DIG_COD_ITEM,
                        DIG_DIGITO,
                        DIG_GALPAO,
                        DIG_RUA,
                        DIG_NRO,
                        DIG_APT,
                        DIG_LOJA_1,
                        DIG_ROMANEIO_1,
                        DIG_GALPAO_1,
                        DIG_RUA_1,
                        DIG_NRO_1,
                        DIG_APT_1,
                        DIG_COD_ITEM_1,
                        DIG_DIGITO_1,
                        DIG_PEDIDO_1,
                        DIG_LOJA_2,
                        DIG_ROMANEIO_2,
                        DIG_COD_ITEM_2,
                        DIG_DIGITO_2,
                        DIG_GALPAO_2,
                        DIG_RUA_2,
                        DIG_NRO_2,
                        DIG_APT_2,
                        DIG_PEDIDO_2,
                        DIG_QTD_BONI,
                        DIG_QTD_VEND,
                        DIG_QTD_FAT,
                        DIG_TPO_EMB,
                        DIG_QTD_EMB,
                        DIG_DESCONTO,
                        DIG_MOEDA,
                        DIG_PRECO,
                        DIG_PRECO_MOEDA,
                        DIG_NIVEL_PRECO,
                        DIG_NIVEL_PROMO,
                        DIG_ALIQ_ISS,
                        DIG_ALIQ_IR,
                        DIG_ALIQ_AIRE,
                        DIG_ALIQ_ICM,
                        DIG_ICM_FONTE,
                        DIG_BASE_REDUZ,
                        DIG_SUBS_TRIB,
                        DIG_CRED_PRES,
                        DIG_COD_GIA,
                        DIG_SITUACAO,
                        DIG_VAL_PAUTA,
                        DIG_OPERACAO,
                        DIG_VD_CUSTO,
                        DIG_TRANS_SAI_CUSTO,
                        DIG_DEVOLUCAO_CUSTO,
                        DIG_EMB_FOR,
                        DIG_PESO_UN,
                        DIG_VOLU_UN,
                        DIG_COMISSAO,
                        DIG_VAL_BONUS,
                        DIG_NUM_NFF_PDV,
                        DIG_SERIE,
                        DIG_VALOR_IPI,
                        DIG_CONDICAO,
                        DIG_AGENDA,
                        DIG_TIP_LOJ_CLI,
                        DIG_LOJ_CONTR_ESTQ,
                        DIG_TBC_INTG_07,
                        DIG_TBC_INTG_11,
                        DIG_TBC_INTG_17,
                        DIG_ESTLOC,
                        DIG_FLAG,
                        DIG_DATA,
                        DIG_HORA)
                    VALUES (
                        $store,
                        '0',
                        '0',
                        '0',
                        $pedido,
                        $aa3citem->GIT_NAT_FISCAL,
                        $aa3citem->GIT_COD_ITEM,
                        $aa3citem->GIT_DIGITO,
                        '0',
                        '0',
                        '0',
                        '0',
                        $store,
                        $pedido,
                        '0',
                        '0',
                        '0',
                        '0',
                        $aa3citem->GIT_COD_ITEM,
                        $aa3citem->GIT_DIGITO,
                        $pedido,
                        $store,
                        $pedido,
                        $aa3citem->GIT_COD_ITEM,
                        $aa3citem->GIT_DIGITO,
                        '0',
                        '0',
                        '0',
                        '0',
                        $pedido,
                        '0',
                        $iensa->QUANTIDADE,
                        $iensa->QUANTIDADE,
                        '$aa3citem->GIT_TPO_EMB_VENDA',
                        1,
                        0,
                        0,
                        $preco,
                        0,
                        4,
                        4,
                        0,
                        0,
                        6,".
                        $iensa->getICMSAliquot($iensa->ESCHLJC_CODIGO3).",
                        0,
                        0,
                        0,
                        0,
                        0,
                        90,
                        0,
                        3,
                        2,
                        0,
                        0,
                        1,
                        .2,
                        .000468,
                        0,
                        0,
                        0,
                        '    ',
                        0,
                        900,
                        $agenda,
                        'L',
                        'S',
                        'N',
                        'T',
                        'N',
                        'N',
                        0,
                        '1".date('ymd')."','".
                        date('His')."')");
                        
                        $stmt->execute();
                    }
                // } else if ($iensa->ESCHC_AGENDA3 == 608) {
                //     $ag2detnt = new AG2DETNT();
                //     $preco = $iensa->getPrice2;
                //     if ($preco == 0) {
                //         $preco = 1;
                //     }
                //     $aa3citem = (new AA3CITEM())->find("GIT_COD_ITEM = $iensa->ESITC_CODIGO")->fetch();
                //     $ag2detnt->addAg2detnt(1926,$store,str_pad($aa3citem->GIT_COD_FOR,7,'0',STR_PAD_RIGHT).str_pad($aa3citem->GIT_COD_ITEM.$aa3citem->GIT_DIGITO,8,0,STR_PAD_LEFT),substr($store,0,strlen($store)-1),$iensa->getPrice2,'1'.date("ymd"),123123,$iensa->QUANTIDADE,$i,628,90);
                // }
                
    
            }  
        }

    }
    
    public function integracaoNotas() {
        
        
    }
}