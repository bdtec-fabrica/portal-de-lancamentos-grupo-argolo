<?php

namespace Source\App;

use League\Plates\Engine;
use Sabberworm\CSS\Property\Selector;
use Source\Models\NfReceipt;
use Source\Models\Store;
use Source\Models\User;
use Source\Models\NfSefaz;
use Source\Models\AG1IENSA;
use Source\Models\AA2CTIPO;
use Source\Models\AA2CESTQ;
use Source\Models\NCMALIQUOTA;
use DateTime;
use Dompdf\Dompdf;
use Source\Models\CAPCUPOM;
use Source\Models\AGGFLSFIN;
use Source\Models\AA1CTCON;
use Source\Models\AA1CTCON10;
use Source\Models\AG1IENSA2;
use Source\Models\CAPCUPOM2;
use Source\Models\AGGFLSFIN2;
use Source\Models\AA1CTCON2;
use Source\Models\AA3CITEM;
use Source\Models\AA1RTITU;
use Source\Models\AA2CTIPO10;
use Source\Models\AA2CTIPO2;
use Source\Models\AA3CITEM2;
use Source\Models\AG1IENSA10;
use Source\Models\AGGFLSFIN10;
use Source\Models\CadastroConsinco;
use Source\Models\CAPCUPOM10;
use Source\Models\COMPRAS_MARGEM;
use Source\Models\COMPRAS_MARGEM10;
use Source\Models\Daily;
use Source\Models\MapAuditoria;
use Source\Models\MapProduto;
use Source\Models\MlfNotaFiscal;
use Source\Models\MrlLanctoestoque;
use Source\Models\NfReport;
use Source\Models\VENDAS_30_DIAS;
use Source\Models\VENDAS_MARGEM;
use Source\Models\NfReceiptLost;
use Source\Models\StoreContacts;
use Source\Models\VendasComparativo;
use Source\Models\VendasConsinco;
use Source\Models\VIEW_HORTI;
use Source\Models\WhatsappSends;
use stdClass;

class SendsWhatsapp
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");

    }
    /**
    * PDFs
    */
    public function NFnaoEscrituradas() {
        $dompdf = new Dompdf();

        $stores = array($_GET['loja']);
        foreach ($stores as $store1) {
            $store = $store1;
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();

            $date1 = inputDateToRMSDate(date("Y-m-d",strtotime("-90 days")));
            $date2 = inputDateToRMSDate(date("Y-m-d",strtotime("-30 days")));
            $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :code","code=$store")->fetch();
            if ($aa2ctipo->TIP_EMPRESA == 200) {
                $nfSefaz = (new NfSefaz())->find("store_id in (200,201,203) and concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) between $date1 and $date2")->order("id ASC")->fetch(true);
            } else {
                $nfSefaz = (new NfSefaz())->find("store_id = $store and concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) between $date1 and $date2")->order("id ASC")->fetch(true);
            }


            $i = 0;
            $dompdf = new Dompdf();

            foreach ($nfSefaz as $nf) {
                $start_date = new DateTime(str_replace("T"," ",$nf->emission_date));
                $since_start = $start_date->diff(new DateTime(date("Y-m-d H:i:s")));

                foreach ($nf->getLogManifestation() as $log) {
                    if ($log->manifestation == '210220' || $log->manifestation == '210240') {
                        $manifestation = true;
                    } else {
                        $manifestation = false;
                    }
                }
                if (!$manifestation) {
                    $days = ($since_start->m*30) + ($since_start->y *365) + $since_start->d;
                    if ($nf->getUF() == 'BA' && $days > 20) {
                        if ($nf->getStatus() === 1) {
                            if ($nf->valorContabil() > 0) {
                                $linhas[$i]['numero'] = $nf->nfe_number;
                                $linhas[$i]['razao'] = $nf->getFornecedor();
                                $linhas[$i]['uf'] = $nf->getUF();
                                $linhas[$i]['emissao'] = $nf->emission_date;
                                $linhas[$i]['chave'] = $nf->access_key;
                                $linhas[$i]['natureza'] = $nf->natOp;
                                $linhas[$i]['dias'] = $days;
                                $linhas[$i]['valor'] = (float) $nf->valorContabil();
                                $linhas[$i]['pagar'] = $nf->valorContabil() * (1/100);
                                $total += $linhas[$i]['pagar'];
                                $i++;
                            }
                        }
                    } else if ($nf->getUF() != 'BA' && $days > 35) {
                        if ($nf->getStatus() == 1) {
                            if ($nf->valorContabil() > 0) {
                                $linhas[$i]['numero'] = $nf->nfe_number;
                                $linhas[$i]['razao'] = $nf->getFornecedor();
                                $linhas[$i]['uf'] = $nf->getUF();
                                $linhas[$i]['emissao'] = $nf->emission_date;
                                $linhas[$i]['chave'] = $nf->access_key;
                                $linhas[$i]['natureza'] = $nf->natOp;
                                $linhas[$i]['dias'] = $days;
                                $linhas[$i]['valor'] = (float) $nf->valorContabil();
                                $linhas[$i]['pagar'] = $nf->valorContabil() * (1/100);
                                $total += $linhas[$i]['pagar'];
                                $i++;
                            }
                        }
                    }

                }
            }
            if(!empty($linhas)) {
                $content = $this->view->render("layoutPDF/indicadoresNfNaoEscrituradas", ["id" => "Painel NFe | " . SITE , "linhas" => $linhas
            ]);


            // $email = new \SendGrid\Mail\Mail();
            // $email->setFrom("bdtecatende@bispoedantas.com.br", "BDTec");
            // $email->setSubject("Notas não escrituradas Loja $store1->code - $store1->company_name");
            // $email->addTo('jeferson@bispoedantas.com', 'Jeferson');
            // $email->addCc("eduardo@bispoedantas.com.br","Eduardo Morais");
            // $email->addCc("fabioadebrito@gmail.com","Fábio Brito");
            // $email->addContent(
            //     "text/html", $content
            // );
            // $sendgrid = new \SendGrid("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");
            // try {
            //     $response = $sendgrid->send($email);
            //     print $response->statusCode() . "\n";
            //     print_r($response->headers());
            //     print $response->body() . "\n";
            // } catch (Exception $e) {
            //     echo 'Caught exception: '. $e->getMessage() ."\n";
            // }
            $dompdf->loadHtml($content);
            $dompdf->setPaper("A2","landscape");
            $dompdf->render();
            $dompdf->stream("file.pdf",["Attachment" => false]);
            }
        }
    }

    public function ItensSemGiro() {
        $loja = $_GET['loja'];
        $dompdf = new Dompdf();
        $data = inputDateToRMSDate(date('Y-m-d',strtotime("-90 days")));
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = :code","code=$loja")->fetch();
        $store = $aa2ctipo->TIP_CODIGO.$aa2ctipo->TIP_DIGITO*1;
        if ($aa2ctipo->TIP_EMPRESA == 200) {
            $aa2cestq = (new AA2CESTQ())->find("GET_COD_LOCAL in (2003,2011,2038) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
            rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < $data) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
            rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < $data) and GET_ESTOQUE > 0")->fetch(true);
        } else {
            $aa2cestq = (new AA2CESTQ())->find("GET_COD_LOCAL = $store and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) is null or
            rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_FAT)) < $data) and (rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) is null or
            rms.DATETO_RMS7(rms.RMSTO_DATE(GET_DT_ULT_ENT)) < $data) and GET_ESTOQUE > 0")->fetch(true);
        }


        $content = $this->view->render("layoutPDF/indicadoresSemGiro", ["id" => "Painel NFe | " . SITE , "aa2cestq" => $aa2cestq
        ]);

        $dompdf->loadHtml($content);
        $dompdf->setPaper("A4","landscape");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);
    }

    public function simples() {
        $loja = $_GET['loja'];
        $dompdf = new Dompdf();
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
        $date1 = inputDateToRMSDate(date("Y-m",strtotime("-1 month"))."-01");
        $date2 = inputDateToRMSDate(date("Y-m",strtotime("-1 month"))."-31");
        if ($aa2ctipo->TIP_EMPRESA == 200) {
            $nfSefaz = (new NfSefaz())->find("store_id in (200,201,203) and concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) between $date1 and $date2")->order("id ASC")->fetch(true);
        } else {
            $nfSefaz = (new NfSefaz())->find("store_id = $loja and concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) between $date1 and $date2")->order("id ASC")->fetch(true);
        }


        $i = 0;
        foreach ($nfSefaz as $nf) {
            if ($nf->getSimples) {
                foreach ($nf->getItems() as $itens) {
                    $ncm = (new NCMALIQUOTA());
                    if ($ncm->aliquotaS($itens->prod->NCM) > 0) {
                        $aliquota = 0;
                        $linhas[$i]['referencia'] = $itens->prod->cProd;
                        $linhas[$i]['descricao'] = $itens->prod->xProd;
                        $linhas[$i]['ncm'] = $itens->prod->NCM;
                        $linhas[$i]['cfop'] = $itens->prod->CFOP;
                        if ($linhas[$i]['cfop'] == 5101 || $linhas[$i]['cfop'] == 6101) {
                            $aliquota = 12;
                        } else {
                            $aliquota = 0;
                        }
                        $linhas[$i]['quantidade'] = $itens->prod->qCom;
                        $linhas[$i]['valor'] = $itens->prod->vUnCom;
                        $linhas[$i]['base'] = round($itens->prod->vUnCom * $itens->prod->qCom,2);
                        $linhas[$i]['aliquota'] = $ncm->aliquotaS($itens->prod->NCM) - $aliquota;
                        $linhas[$i]['valorIcms'] = round($linhas[$i]['base'] * ($linhas[$i]['aliquota']/100),2);
                        $linhas[$i]['numero'] = $nf->nfe_number;
                        $linhas[$i]['razao'] = $nf->getFornecedor();
                        $linhas[$i]['emissao'] = $nf->emission_date;
                        $linhas[$i]['chave'] = $nf->access_key;
                        $total += round(($itens->prod->vUnCom * $itens->prod->qCom) * ($ncm->aliquotaS( $linhas[$i]['ncm'])/100),2);
                        $i++;
                    }
                }
            }
        }
        $content = "Olá seguem compras realizadas em Simples Nacional: \n\n";
        foreach ($linhas as $linha){
            $content .= "Referencia: ".$linha['referencia']." \n Descrição: ".$linha['descricao']." \n NCM: ".$linha['ncm']." \n CFOP: ".$linha['cfop']." \n Quantidade: ".str_replace(".",",",$linha['quantidade'])." \n Valor: ".str_replace(".",",",$linha['valor'])." \n Base de Calculo: ".str_replace(".",",",$linha['base'])." \n Aliquota: ".$linha['aliquota']." \n Valor de ICMS: ".str_replace(".",",",$linha['valorIcms'])." \n Número NF-e: ".$linha['numero']." \n Razão Social: ".$linha['razao']." \n Emissão: ".$linha['emissao']." \n Chave de Acesso: ".$linha['chave']." \n\n";
        }

        $content = $this->view->render("layoutPDF/indicadoresSimples", ["id" => "Painel NFe | " . SITE , "linhas" => $linhas
        ]);

        $dompdf->loadHtml($content);
        $dompdf->setPaper("A2","landscape");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);




    }

    public function indicatorsPDF() {
        $loja = $_GET['loja'];
        $dompdf = new Dompdf();
        $data1 = "1".date("ymd",strtotime("-1 days"));
        $store = (new Store())->find("code = $loja")->fetch();


        if ($store->group_id == 1) {
            $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
            if ($aa2ctipo->TIP_EMPRESA == 200) {

                $ag1iensa = (new AG1IENSA())->find("ESCHC_AGENDA3 in (41,42,43,44,45,49,79,188,189,190) AND ESCHC_DATA3 = $data1 AND ESCHLJC_CODIGO3 in (200,201,203)")->fetch(true);
                foreach ($ag1iensa as $iensa) {
                    if ($iensa->getMargin2($iensa->ESCHLJC_CODIGO3) < 2) {
                        $registers1[$iensa->ESITC_CODIGO]['codigo'] = $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO;
                        $registers1[$iensa->ESITC_CODIGO]['descricao'] = $iensa->getItemRegister()->GIT_DESCRICAO;
                        $registers1[$iensa->ESITC_CODIGO]['custo'] = $iensa->ENTSAIC_CUS_UN;
                        $registers1[$iensa->ESITC_CODIGO]['preco'] = $iensa->ENTSAIC_PRC_UN;
                        $registers1[$iensa->ESITC_CODIGO]['margem'] = round($iensa->getMargin2($iensa->ESCHLJC_CODIGO3),2);
                    }
                }

                $data2 = "1".date("ymd",strtotime("-1 days"));
                $data3 = "1".date("ymd",strtotime("-366 days"));

                $ag1iensa2 = (new AG1IENSA())->find("ESCHC_AGENDA3  in (41,42,43,44,45,49,79,188,189,190) AND ESCHC_DATA3 = $data2 AND ESCHLJC_CODIGO3 in (200,201,203)","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
                $ag1iensa3 = (new AG1IENSA())->find("ESCHC_AGENDA3  in (41,42,43,44,45,49,79,188,189,190) AND ESCHC_DATA3 = $data3 AND ESCHLJC_CODIGO3 in (200,201,203)","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();

                $data4 = "1".date("ym")."01";
                $data5 = "1".date("ym")."31";
                $ag1iensa4 = (new AG1IENSA())->find("ESCHC_AGENDA3  in (41,42,43,44,45,49,79,188,189,190) AND ESCHC_DATA3 between $data4 and $data5 AND ESCHLJC_CODIGO3 in (200,201,203)","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
                $projecao = ($ag1iensa4->VALOR / date("d",strtotime("-1 days"))) * date("t");

                $data6 = "1".date("ymd",strtotime("-1 days"));

                $capcupom = (new CAPCUPOM())->find("CAP_DATA = $data6 and CAP_FILIAL in (200,201,203)","","count(*) QUANTIDADE, cast(sum(CAP_VLR_CUPOM) / count(*) as decimal(38, 2)) TICKET")->fetch();
                $capcupom2 = (new CAPCUPOM())->find("CAP_DATA BETWEEN $data4 AND $data5 and CAP_FILIAL in (200,201,203)","","count(*) QUANTIDADE, cast(sum(CAP_VLR_CUPOM) / count(*) as decimal(38, 2)) TICKET")->fetch();

                $AGGFLSFIN = (new AGGFLSFIN)->find("ID_DT = (select ID_DT from DIM_PER a where DT_COMPL in (select to_char(to_date(sysdate - (1 + 1 / 24), 'DD-MM-YYYY')) from dual)) and CD_FIL in (200,201,203)")->fetch(true);

                $ag1iensa5 = (new AG1IENSA())->find("ESCHC_AGENDA3 in (41,42,43,44,45,49,79,188,189,190) AND ESCHC_DATA3 = $data2 AND ESCHLJC_CODIGO3 in (200,201,203)")->order("ENTSAIC_QUANTI_UN*ENTSAIC_PRC_UN DESC")->fetch(true);

                foreach ($ag1iensa5 as $iensa) {
                    $registers2[$iensa->ESITC_CODIGO]['codigo'] = $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO;
                    $registers2[$iensa->ESITC_CODIGO]['descricao'] = $iensa->getItemRegister()->GIT_DESCRICAO;
                    $registers2[$iensa->ESITC_CODIGO]['quantidade'] = $iensa->ENTSAIC_QUANTI_UN;
                    $registers2[$iensa->ESITC_CODIGO]['preco'] = $iensa->ENTSAIC_PRC_UN;
                    $registers2[$iensa->ESITC_CODIGO]['margem'] = round($iensa->getMargin2($iensa->ESCHLJC_CODIGO3),2);
                }
                $data7 = "1".date("ym",strtotime("-1 month"))."01";
                $data8 = "1".date("ym",strtotime("-1 month")).date("d");
                $data9 = "1".date("ym",strtotime("-1 year"))."01";
                $data10 ="1".date("ym",strtotime("-1 year")).date("d");

                $aa1ctcon = (new AA1CTCON())->getEntradas2();

                $ag1iensa6 = (new AG1IENSA())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data4 and $data5 AND ESCLC_CODIGO in (200,201,203)","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();//atual
                $ag1iensa7 = (new AG1IENSA())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data7 and $data8 AND ESCLC_CODIGO in (200,201,203)","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();// mes passado
                $ag1iensa72 = (new AG1IENSA())->find("ESCHC_AGENDA3  in (41,42,43,44,45,49,79,188,189,190) AND ESCHC_DATA3 between $data7 and $data8 AND ESCHLJC_CODIGO3 in (200,201,203)","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
                $ag1iensa8 = (new AG1IENSA())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data9 and $data10 AND ESCLC_CODIGO in (200,201,203)","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();// ano passado
                $ag1iensa82 = (new AG1IENSA())->find("ESCHC_AGENDA3  in (41,42,43,44,45,49,79,188,189,190) AND ESCHC_DATA3 between $data9 and $data10 AND ESCHLJC_CODIGO3 in (200,201,203)","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();

                $content =   $this->view->render("layoutPDF/indicadores", ["id" => "Painel NFe | " . SITE ,
                    "registers1" => $registers1,"ag1iensa2" => $ag1iensa2, "ag1iensa3" => $ag1iensa3,"ag1iensa4" => $ag1iensa4, "projecao" => $projecao, "capcupom" => $capcupom, "capcupom2" => $capcupom2, "AGGFLSFIN" => $AGGFLSFIN,
                    "registers2" => $registers2, "ag1iensa6" => $ag1iensa6,"ag1iensa7" => $ag1iensa7,"ag1iensa72" => $ag1iensa72,"ag1iensa8" => $ag1iensa8,"ag1iensa82" => $ag1iensa82,
                ]);


                $dompdf->loadHtml($content);
                $dompdf->setPaper("A4");
                $dompdf->render();
                $dompdf->stream("file.pdf",["Attachment" => false]);
            } else {
                $aa1ctcon = (new AA1CTCON())->getEntradas2();
                $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
                if ($aa2ctipo->TIP_EMPRESA == 200) {
                    $venda = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 42 and ESCHLJC_CODIGO3 in (200,201,203) and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                    $compra = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO in (200,201,203) and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                    $venda2 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 42 and ESCHLJC_CODIGO3 in (200,201,203) and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                    $compra2 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = in (200,201,203) and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
                } else {
                    $venda3 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 601 and ESCHLJC_CODIGO3 = $aa2ctipo->TIP_CODIGO and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                   $compra = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = $aa2ctipo->TIP_CODIGO and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();


                    $venda2 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 601 and ESCHLJC_CODIGO3 = $loja and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                    $compra2 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = $loja and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
                }
                $vendas = (new VENDAS_30_DIAS())->find("ESCHLJC_CODIGO3 = $loja")->fetch(true);

                $ag1iensa = (new VENDAS_MARGEM())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data1 AND ESCHLJC_CODIGO3 = $loja")->fetch(true);

                foreach ($ag1iensa as $iensa) {
                    if ($iensa->MARGEM < 5) {
                        $registers1[$iensa->ESITC_CODIGO]['codigo'] = $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO;
                        $registers1[$iensa->ESITC_CODIGO]['descricao'] = $iensa->GIT_DESCRICAO;
                        $registers1[$iensa->ESITC_CODIGO]['custo'] = $iensa->ENTSAIC_CUS_UN;
                        $registers1[$iensa->ESITC_CODIGO]['preco'] = $iensa->ENTSAIC_PRC_UN;
                        $registers1[$iensa->ESITC_CODIGO]['margem'] = $iensa->MARGEM;
                    }
                }

                $data2 = "1".date("ymd",strtotime("-1 days"));
                $data3 = "1".date("ymd",strtotime("-366 days"));

                $ag1iensa2 = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data2 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
                $ag1iensa3 = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data3 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();

                $data4 = "1".date("ym")."01";
                $data5 = "1".date("ym")."31";
                $ag1iensa4 = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data4 and $data5 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
                $projecao = ($ag1iensa4->VALOR / date("d",strtotime("-1 days"))) * date("t");

                $data6 = "1".date("ymd",strtotime("-1 days"));

                $capcupom = (new CAPCUPOM())->find("CAP_DATA = $data6 and CAP_FILIAL = $loja","","count(*) QUANTIDADE, cast(sum(CAP_VLR_CUPOM) / count(*) as decimal(38, 2)) TICKET")->fetch();
                $capcupom2 = (new CAPCUPOM())->find("CAP_DATA BETWEEN $data4 AND $data5 and CAP_FILIAL = $loja","","count(*) QUANTIDADE, cast(sum(CAP_VLR_CUPOM) / count(*) as decimal(38, 2)) TICKET")->fetch();

                $AGGFLSFIN = (new AGGFLSFIN)->find("ID_DT = (select ID_DT from DIM_PER a where DT_COMPL in (select to_char(to_date(sysdate - (1 + 1 / 24), 'DD-MM-YYYY')) from dual)) and CD_FIL = $loja")->fetch(true);

                $ag1iensa5 = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data2 AND ESCHLJC_CODIGO3 = $loja")->order("ENTSAIC_QUANTI_UN DESC")->fetch(true);

                foreach ($ag1iensa5 as $iensa) {
                    $registers2[$iensa->ESITC_CODIGO]['codigo'] = $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO;
                    $registers2[$iensa->ESITC_CODIGO]['descricao'] = $iensa->getItemRegister()->GIT_DESCRICAO;
                    $registers2[$iensa->ESITC_CODIGO]['quantidade'] = $iensa->ENTSAIC_QUANTI_UN;
                    $registers2[$iensa->ESITC_CODIGO]['preco'] = $iensa->ENTSAIC_PRC_UN;
                    $registers2[$iensa->ESITC_CODIGO]['margem'] = round($iensa->getMargin2($iensa->ESCHLJC_CODIGO3),2);
                }
                $data7 = "1".date("ym",strtotime("-1 month"))."01";
                $data8 = "1".date("ym",strtotime("-1 month")).date("d");
                $data9 = "1".date("ym",strtotime("-1 year"))."01";
                $data10 ="1".date("ym",strtotime("-1 year")).date("d");

                $aa1ctcon = (new AA1CTCON())->getEntradas2();

                $ag1iensa6 = (new AG1IENSA())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data4 and $data5 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();//atual
                $ag1iensa7 = (new AG1IENSA())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data7 and $data8 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();// mes passado
                $ag1iensa72 = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data7 and $data8 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
                $ag1iensa8 = (new AG1IENSA())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data9 and $data10 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();// ano passado
                $ag1iensa82 = (new AG1IENSA())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data9 and $data10 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
                $vendasComparativo = (new VendasComparativo())->find("ESCHLJC_CODIGO3 = $aa2ctipo->TIP_CODIGO")->fetch(true);

                foreach($vendasComparativo as $vendas) {
                    $comparativo[$vendas->DATA_ATUAL]['DATA_ATUAL'] = $vendas->DATA_ATUAL;
                    $comparativo[$vendas->DATA_ATUAL]['VALOR_ATUAL'] = $vendas->VALOR_ATUAL;
                    $comparativo[$vendas->DATA_ATUAL]['DATA_ANTERIOR'] = $vendas->DATA_ANTERIOR;
                    $comparativo[$vendas->DATA_ATUAL]['VALOR_ANTERIOR'] = $vendas->VALOR_ANTERIOR;
                }

                $content =   $this->view->render("layoutPDF/indicadores", ["id" => "Painel NFe | " . SITE ,"vendas" => $vendas,"registers1" => $registers1,"ag1iensa2" => $ag1iensa2, "ag1iensa3" => $ag1iensa3,"ag1iensa4" => $ag1iensa4, "projecao" => $projecao, "capcupom" => $capcupom, "capcupom2" => $capcupom2, "AGGFLSFIN" => $AGGFLSFIN,
                    "comparativo" => $comparativo,"registers2" => $registers2, "ag1iensa6" => $ag1iensa6,"ag1iensa7" => $ag1iensa7,"ag1iensa72" => $ag1iensa72,"ag1iensa8" => $ag1iensa8,"ag1iensa82" => $ag1iensa82,
                    "venda3" => $venda3, "compra" => $compra,"venda2" => $venda2, "compra2" => $compra2
                ]);


                $dompdf->loadHtml($content);
                $dompdf->setPaper("A4");
                $dompdf->render();
                $dompdf->stream("file.pdf",["Attachment" => false]);
            }
        } else if ($store->group_id == 2) {
            $aa2ctipo = (new AA2CTIPO2())->find("TIP_CODIGO = $loja")->fetch();
            $ag1iensa = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data1 AND ESCHLJC_CODIGO3 = $loja")->fetch(true);
            $aa1ctcon = (new AA1CTCON())->getEntradas2();
            $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
            if ($aa2ctipo->TIP_EMPRESA == 200) {
                $venda = (new AG1IENSA2())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 42 and ESCHLJC_CODIGO3 in (200,201,203) and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                $compra = (new AG1IENSA2())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO in (200,201,203) and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                $venda2 = (new AG1IENSA2())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 42 and ESCHLJC_CODIGO3 in (200,201,203) and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                $compra2 = (new AG1IENSA2())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = in (200,201,203) and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
            } else {
                $venda = (new AG1IENSA2())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 601 and ESCHLJC_CODIGO3 = $loja and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
               $compra = (new AG1IENSA2())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = $loja and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();


                $venda2 = (new AG1IENSA2())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 601 and ESCHLJC_CODIGO3 = $loja and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                $compra2 = (new AG1IENSA2())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = $loja and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
            }
            foreach ($ag1iensa as $iensa) {
                if ($iensa->getMargin2($iensa->ESCHLJC_CODIGO3) < 5) {
                    $registers1[$iensa->ESITC_CODIGO]['codigo'] = $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO;
                    $registers1[$iensa->ESITC_CODIGO]['descricao'] = $iensa->getItemRegister()->GIT_DESCRICAO;
                    $registers1[$iensa->ESITC_CODIGO]['custo'] = $iensa->ENTSAIC_CUS_UN;
                    $registers1[$iensa->ESITC_CODIGO]['preco'] = $iensa->ENTSAIC_PRC_UN;
                    $registers1[$iensa->ESITC_CODIGO]['margem'] = round($iensa->getMargin2($iensa->ESCHLJC_CODIGO3),2);
                }
            }

            $data2 = "1".date("ymd",strtotime("-1 days"));
            $data3 = "1".date("ymd",strtotime("-366 days"));

            $ag1iensa2 = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data2 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
            $ag1iensa3 = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data3 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();

            $data4 = "1".date("ym")."01";
            $data5 = "1".date("ym")."31";
            $ag1iensa4 = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data4 and $data5 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
            $projecao = ($ag1iensa4->VALOR / date("d",strtotime("-1 days"))) * date("t");

            $data6 = "1".date("ymd",strtotime("-1 days"));

            $capcupom = (new CAPCUPOM2())->find("CAP_DATA = $data6 and CAP_FILIAL = $loja","","count(*) QUANTIDADE, cast(sum(CAP_VLR_CUPOM) / count(*) as decimal(38, 2)) TICKET")->fetch();
            $capcupom2 = (new CAPCUPOM2())->find("CAP_DATA BETWEEN $data4 AND $data5 and CAP_FILIAL = $loja","","count(*) QUANTIDADE, cast(sum(CAP_VLR_CUPOM) / count(*) as decimal(38, 2)) TICKET")->fetch();

            $AGGFLSFIN = (new AGGFLSFIN2)->find("ID_DT = (select ID_DT from DIM_PER a where DT_COMPL in (select to_char(to_date(sysdate - (1 + 1 / 24), 'DD-MM-YYYY')) from dual)) and CD_FIL = $loja")->fetch(true);

            $ag1iensa5 = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data2 AND ESCHLJC_CODIGO3 = $loja")->order("ENTSAIC_QUANTI_UN DESC")->fetch(true);

            foreach ($ag1iensa5 as $iensa) {
                $registers2[$iensa->ESITC_CODIGO]['codigo'] = $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO;
                $registers2[$iensa->ESITC_CODIGO]['descricao'] = $iensa->getItemRegister()->GIT_DESCRICAO;
                $registers2[$iensa->ESITC_CODIGO]['quantidade'] = $iensa->ENTSAIC_QUANTI_UN;
                $registers2[$iensa->ESITC_CODIGO]['preco'] = $iensa->ENTSAIC_PRC_UN;
                $registers2[$iensa->ESITC_CODIGO]['margem'] = round($iensa->getMargin2($iensa->ESCHLJC_CODIGO3),2);
            }
            $data7 = "1".date("ym",strtotime("-1 month"))."01";
            $data8 = "1".date("ym",strtotime("-1 month")).date("d");
            $data9 = "1".date("ym",strtotime("-1 year"))."01";
            $data10 ="1".date("ym",strtotime("-1 year")).date("d");

            $aa1ctcon = (new AA1CTCON2())->getEntradas2();

            $ag1iensa6 = (new AG1IENSA2())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data4 and $data5 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();//atual
            $ag1iensa7 = (new AG1IENSA2())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data7 and $data8 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();// mes passado
            $ag1iensa72 = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data7 and $data8 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
            $ag1iensa8 = (new AG1IENSA2())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data9 and $data10 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();// ano passado
            $ag1iensa82 = (new AG1IENSA2())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data9 and $data10 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();

            $content =   $this->view->render("layoutPDF/indicadores", ["id" => "Painel NFe | " . SITE ,
                "registers1" => $registers1,"ag1iensa2" => $ag1iensa2, "ag1iensa3" => $ag1iensa3,"ag1iensa4" => $ag1iensa4, "projecao" => $projecao, "capcupom" => $capcupom, "capcupom2" => $capcupom2, "AGGFLSFIN" => $AGGFLSFIN,
                "registers2" => $registers2, "ag1iensa6" => $ag1iensa6,"ag1iensa7" => $ag1iensa7,"ag1iensa72" => $ag1iensa72,"ag1iensa8" => $ag1iensa8,"ag1iensa82" => $ag1iensa82,
                "venda" => $venda, "compra" => $compra,"venda2" => $venda2, "compra2" => $compra2
            ]);


            $dompdf->loadHtml($content);
            $dompdf->setPaper("A4");
            $dompdf->render();
            $dompdf->stream("file.pdf",["Attachment" => false]);
        } else if ($store->group_id == 10) {
            $aa2ctipo = (new AA2CTIPO10())->find("TIP_CODIGO = $loja")->fetch();
            $ag1iensa = (new AG1IENSA10())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data1 AND ESCHLJC_CODIGO3 = $loja")->fetch(true);
            $aa1ctcon = (new AA1CTCON10())->getEntradas2();
            $aa2ctipo = (new AA2CTIPO10())->find("TIP_CODIGO = $loja")->fetch();
            if ($aa2ctipo->TIP_EMPRESA == 200) {
                $venda = (new AG1IENSA10())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 42 and ESCHLJC_CODIGO3 in (200,201,203) and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                $compra = (new AG1IENSA10())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO in (200,201,203) and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                $venda2 = (new AG1IENSA10())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 42 and ESCHLJC_CODIGO3 in (200,201,203) and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                $compra2 = (new AG1IENSA10())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = in (200,201,203) and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
            } else {
                $venda = (new AG1IENSA10())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 601 and ESCHLJC_CODIGO3 = $loja and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
               $compra = (new AG1IENSA10())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = $loja and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();


                $venda2 = (new AG1IENSA10())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 601 and ESCHLJC_CODIGO3 = $loja and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

                $compra2 = (new AG1IENSA10())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = $loja and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
            }
            foreach ($ag1iensa as $iensa) {
                if ($iensa->getMargin2($iensa->ESCHLJC_CODIGO3) < 5) {
                    $registers1[$iensa->ESITC_CODIGO]['codigo'] = $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO;
                    $registers1[$iensa->ESITC_CODIGO]['descricao'] = $iensa->getItemRegister()->GIT_DESCRICAO;
                    $registers1[$iensa->ESITC_CODIGO]['custo'] = $iensa->ENTSAIC_CUS_UN;
                    $registers1[$iensa->ESITC_CODIGO]['preco'] = $iensa->ENTSAIC_PRC_UN;
                    $registers1[$iensa->ESITC_CODIGO]['margem'] = round($iensa->getMargin2($iensa->ESCHLJC_CODIGO3),2);
                }
            }

            $data2 = "1".date("ymd",strtotime("-1 days"));
            $data3 = "1".date("ymd",strtotime("-366 days"));

            $ag1iensa2 = (new AG1IENSA10())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data2 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
            $ag1iensa3 = (new AG1IENSA10())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data3 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();

            $data4 = "1".date("ym")."01";
            $data5 = "1".date("ym")."31";
            $ag1iensa4 = (new AG1IENSA10())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data4 and $data5 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
            $projecao = ($ag1iensa4->VALOR / date("d",strtotime("-1 days"))) * date("t");

            $data6 = "1".date("ymd",strtotime("-1 days"));

            $capcupom = (new CAPCUPOM10())->find("CAP_DATA = $data6 and CAP_FILIAL = $loja","","count(*) QUANTIDADE, cast(sum(CAP_VLR_CUPOM) / count(*) as decimal(38, 2)) TICKET")->fetch();
            $capcupom2 = (new CAPCUPOM10())->find("CAP_DATA BETWEEN $data4 AND $data5 and CAP_FILIAL = $loja","","count(*) QUANTIDADE, cast(sum(CAP_VLR_CUPOM) / count(*) as decimal(38, 2)) TICKET")->fetch();

            $AGGFLSFIN = (new AGGFLSFIN10)->find("ID_DT = (select ID_DT from DIM_PER a where DT_COMPL in (select to_char(to_date(sysdate - (1 + 1 / 24), 'DD-MM-YYYY')) from dual)) and CD_FIL = $loja")->fetch(true);

            $ag1iensa5 = (new AG1IENSA10())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 = $data2 AND ESCHLJC_CODIGO3 = $loja")->order("ENTSAIC_QUANTI_UN DESC")->fetch(true);

            foreach ($ag1iensa5 as $iensa) {
                $registers2[$iensa->ESITC_CODIGO]['codigo'] = $iensa->ESITC_CODIGO.'-'.$iensa->ESITC_DIGITO;
                $registers2[$iensa->ESITC_CODIGO]['descricao'] = $iensa->getItemRegister()->GIT_DESCRICAO;
                $registers2[$iensa->ESITC_CODIGO]['quantidade'] = $iensa->ENTSAIC_QUANTI_UN;
                $registers2[$iensa->ESITC_CODIGO]['preco'] = $iensa->ENTSAIC_PRC_UN;
                $registers2[$iensa->ESITC_CODIGO]['margem'] = round($iensa->getMargin2($iensa->ESCHLJC_CODIGO3),2);
            }
            $data7 = "1".date("ym",strtotime("-1 month"))."01";
            $data8 = "1".date("ym",strtotime("-1 month")).date("d");
            $data9 = "1".date("ym",strtotime("-1 year"))."01";
            $data10 ="1".date("ym",strtotime("-1 year")).date("d");

            $aa1ctcon = (new AA1CTCON10())->getEntradas2();

            $ag1iensa6 = (new AG1IENSA10())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data4 and $data5 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();//atual
            $ag1iensa7 = (new AG1IENSA10())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data7 and $data8 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();// mes passado
            $ag1iensa72 = (new AG1IENSA10())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data7 and $data8 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();
            $ag1iensa8 = (new AG1IENSA10())->find("ESCHC_AGENDA3  in ($aa1ctcon) AND ESCHC_DATA3 between $data9 and $data10 AND ESCLC_CODIGO = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();// ano passado
            $ag1iensa82 = (new AG1IENSA10())->find("ESCHC_AGENDA3 = 601 AND ESCHC_DATA3 between $data9 and $data10 AND ESCHLJC_CODIGO3 = $loja","","sum(ENTSAIC_PRC_UN*ENTSAIC_QUANTI_UN) VALOR")->fetch();

            $content =   $this->view->render("layoutPDF/indicadores", ["id" => "Painel NFe | " . SITE ,
                "registers1" => $registers1,"ag1iensa2" => $ag1iensa2, "ag1iensa3" => $ag1iensa3,"ag1iensa4" => $ag1iensa4, "projecao" => $projecao, "capcupom" => $capcupom, "capcupom2" => $capcupom2, "AGGFLSFIN" => $AGGFLSFIN,
                "registers2" => $registers2, "ag1iensa6" => $ag1iensa6,"ag1iensa7" => $ag1iensa7,"ag1iensa72" => $ag1iensa72,"ag1iensa8" => $ag1iensa8,"ag1iensa82" => $ag1iensa82,
                "venda" => $venda, "compra" => $compra,"venda2" => $venda2, "compra2" => $compra2
            ]);


            $dompdf->loadHtml($content);
            $dompdf->setPaper("A4");
            $dompdf->render();
            $dompdf->stream("file.pdf",["Attachment" => false]);
        } else {
            $loja = substr($_GET['loja'],0,strlen($_GET['loja'])-1);

            $vendas = (new VendasConsinco())->find("TO_DATE(DIA, 'dd/MM/yy') BETWEEN TO_DATE('".date("d/m/Y",strtotime('-1 days'))."', 'dd/MM/yyyy') AND TO_DATE('".date("d/m/Y",strtotime('-1 days'))."', 'dd/MM/yyyy') AND LOJA = $loja")->fetch(true);
            foreach ($vendas as $iensa) {
                if ($iensa->MARGEM < 5) {
                    $registers1[$iensa->CODIGO]['codigo'] = $iensa->CODIGO;
                    $registers1[$iensa->CODIGO]['descricao'] = $iensa->DESCRICAO;
                    $registers1[$iensa->CODIGO]['custo'] = $iensa->CUSTO;
                    $registers1[$iensa->CODIGO]['preco'] = $iensa->PRECO;
                    $registers1[$iensa->CODIGO]['margem'] = $iensa->MARGEM;
                }  
            }

            // $ag1iensa2 = (new VendasConsinco())->find("TO_DATE(DIA, 'dd/MM/yy') BETWEEN TO_DATE('".date("d/m/Y",strtotime('-1 days'))."', 'dd/MM/yyyy') AND TO_DATE('".date("d/m/Y",strtotime('-1 days'))."', 'dd/MM/yyyy') AND LOJA = $loja","","SUM(VALOR_TOTAL) VALOR")->fetch();
            // $ag1iensa3 = (new VendasConsinco())->find("TO_DATE(DIA, 'dd/MM/yy') BETWEEN TO_DATE('".date("d/m/Y",strtotime('-366 days'))."', 'dd/MM/yyyy') AND TO_DATE('".date("d/m/Y",strtotime('-366 days'))."', 'dd/MM/yyyy') AND LOJA = $loja","","SUM(VALOR_TOTAL) VALOR")->fetch();
            // $ag1iensa4 = (new VendasConsinco())->find("TO_DATE(DIA, 'dd/MM/yy') BETWEEN TO_DATE('".date("01/m/Y")."', 'dd/MM/yyyy') AND TO_DATE('".date("31/m/Y")."', 'dd/MM/yyyy') AND LOJA = $loja","","SUM(VALOR_TOTAL) VALOR")->fetch();
            // $projecao = ($ag1iensa4->VALOR / date("d",strtotime("-1 days"))) * date("t");
            $mrlLanctoestoque = (new MrlLanctoestoque())->find("CODGERALOPER IN (22, 810, 811, 851, 858, 800) AND TO_DATE(DTAENTRADASAIDA, 'dd/MM/yy') = TO_DATE('".date("d/m/Y",strtotime('-1 days'))."', 'dd/MM/yyyy') AND NROEMPRESA = $loja","","NRODOCUMENTO,SERIEDOCTO, SUM(QTDLANCTO*VALORVLRNF) VALOR")->group("NRODOCUMENTO, SERIEDOCTO")->fetch(true);
            foreach($mrlLanctoestoque as $cupom) {
                $clientes++;
                $total += $cupom->VALOR;

            }
            $capcupom = new stdClass();
            $capcupom->QUANTIDADE = $clientes;
            if ($clientes > 0) {
                $capcupom->TICKET = round($total /$clientes,2);
            } else {
                $capcupom->TICKET = 0;
            }
            

            $mrlLanctoestoque2 = (new MrlLanctoestoque())->find("CODGERALOPER IN (22, 810, 811, 851, 858, 800) AND TO_DATE(DTAENTRADASAIDA, 'dd/MM/yy') BETWEEN TO_DATE('".date("01/m/Y")."', 'dd/MM/yyyy') AND TO_DATE('".date("31/m/Y")."', 'dd/MM/yyyy') AND NROEMPRESA = $loja","","NRODOCUMENTO,SERIEDOCTO, SUM(QTDLANCTO*VALORVLRNF) VALOR")->group("NRODOCUMENTO, SERIEDOCTO")->fetch(true);
            foreach($mrlLanctoestoque2 as $cupom) {
                $clientes2++;
                $total2 += $cupom->VALOR;

            }
            $capcupom2 = new stdClass();
            $capcupom2->QUANTIDADE = $clientes2;
            if ($clientes2 > 0) {
                $capcupom2->TICKET = round($total2 /$clientes2,2);
            } else {
                $capcupom2->TICKET = 0;
            }
            
            $maisVendidos = (new VendasConsinco())->find("TO_DATE(DIA, 'dd/MM/yy') BETWEEN TO_DATE('".date("d/m/Y",strtotime('-2 days'))."', 'dd/MM/yyyy') AND TO_DATE('".date("d/m/Y",strtotime('-2 days'))."', 'dd/MM/yyyy') AND LOJA = $loja")->order("VALOR_TOTAL DESC")->fetch(true);

            foreach ($maisVendidos as $maisVendido) {
                $registers2[$maisVendido->CODIGO]['codigo'] = $maisVendido->CODIGO;
                $registers2[$maisVendido->CODIGO]['descricao'] = $maisVendido->DESCRICAO;
                $registers2[$maisVendido->CODIGO]['preco'] = $maisVendido->PRECO;
                $registers2[$maisVendido->CODIGO]['quantidade'] = $maisVendido->QUANTIDADE;
                $registers2[$maisVendido->CODIGO]['margem'] = $maisVendido->MARGEM;
            }

            $content =   $this->view->render("layoutPDF/indicadores", ["id" => "Painel NFe | " . SITE ,"vendas" => $vendas,"ag1iensa2" => $ag1iensa2, "projecao" => $projecao, "capcupom" => $capcupom, "capcupom2" => $capcupom2, "registers2" => $registers2

            ]);


            $dompdf->loadHtml($content);
            $dompdf->setPaper("A4");
            $dompdf->render();
            $dompdf->stream("file.pdf",["Attachment" => false]);
        }
    }

    public function indicadoresNegativosPDF() {
       $loja = $_GET['loja'];
        $dompdf = new Dompdf();
        $data1 = "1".date("ymd",strtotime("-1 days"));
        $store = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
        if ($store->TIP_EMPRESA == 200) {
            $aa2cestq = (new AA2CESTQ())->find("GET_QTD_PEND_VDA > 0 and GET_COD_LOCAL in (2003,2011,2038)")->order("GET_DT_ULT_FAT DESC")->fetch(true);
        } else {
            $aa2cestq = (new AA2CESTQ())->find("GET_QTD_PEND_VDA > 0 and GET_COD_LOCAL = $store->TIP_CODIGO$store->TIP_DIGITO")->order("GET_DT_ULT_FAT DESC")->fetch(true);
        }

        foreach ($aa2cestq as $cestq) {
            $registers1[$cestq->GET_COD_PRODUTO]['codigo'] = $cestq->GET_COD_PRODUTO;
            $registers1[$cestq->GET_COD_PRODUTO]['descricao'] = $cestq->getAA3CITEM()->GIT_DESCRICAO;
            $registers1[$cestq->GET_COD_PRODUTO]['dtUltFat'] = $cestq->GET_DT_ULT_FAT;
            $registers1[$cestq->GET_COD_PRODUTO]['quantidade'] = $cestq->GET_QTD_PEND_VDA;
            $registers1[$cestq->GET_COD_PRODUTO]['dtUltEnt'] = $cestq->GET_DT_ULT_ENT;
        }
        $content =   $this->view->render("layoutPDF/indicadoresNegativos", ["id" => "Painel NFe | " . SITE ,
                "registers1" => $registers1
        ]);

        $dompdf->loadHtml($content);
        $dompdf->setPaper("A4");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);
    }

    public function indicadoresCoberturaPDF() {
        $loja = $_GET['loja'];
        $dompdf = new Dompdf();
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
        if ($aa2ctipo->TIP_EMPRESA == 200) {
            $aa2cestq = (new AA2CESTQ())->find("GET_ESTOQUE / ((GET_SEM_1 + GET_SEM_2 + GET_SEM_3 + GET_SEM_4 + GET_SEM_5 + GET_SEM_6 + GET_SEM_7 + GET_SEM_8 + GET_SEM_9 + GET_SEM_10) / 70) < 7 and GET_COD_LOCAL in (2003,2011,2038) and GET_SEM_10 > 0","","AA2CESTQ.*, GET_ESTOQUE / ((GET_SEM_1 + GET_SEM_2 + GET_SEM_3 + GET_SEM_4 + GET_SEM_5 + GET_SEM_6 + GET_SEM_7 + GET_SEM_8 + GET_SEM_9 + GET_SEM_10) / 70) COBERTURA")->fetch(true);
        } else {
            $aa2cestq = (new AA2CESTQ())->find("GET_ESTOQUE / ((GET_SEM_1 + GET_SEM_2 + GET_SEM_3 + GET_SEM_4 + GET_SEM_5 + GET_SEM_6 + GET_SEM_7 + GET_SEM_8 + GET_SEM_9 + GET_SEM_10) / 70) < 7 and substr(GET_COD_LOCAL,0,length(GET_COD_LOCAL)-1) = $loja and GET_SEM_10 > 0","","AA2CESTQ.*, GET_ESTOQUE / ((GET_SEM_1 + GET_SEM_2 + GET_SEM_3 + GET_SEM_4 + GET_SEM_5 + GET_SEM_6 + GET_SEM_7 + GET_SEM_8 + GET_SEM_9 + GET_SEM_10) / 70) COBERTURA")->fetch(true);
        }


        foreach ($aa2cestq as $cestq) {
            $aa3citem = $cestq->getAA3CITEM();
            if ($aa3citem->GIT_SECAO != 5 && $aa3citem->GIT_TIPO_PRO != 3) {
                $registers1[$cestq->GET_COD_PRODUTO]['codigo'] = $cestq->GET_COD_PRODUTO;
                $registers1[$cestq->GET_COD_PRODUTO]['descricao'] = $aa3citem->GIT_DESCRICAO;
                $registers1[$cestq->GET_COD_PRODUTO]['dtUltFat'] = $cestq->GET_DT_ULT_FAT;
                $registers1[$cestq->GET_COD_PRODUTO]['quantidade'] = $cestq->GET_ESTOQUE;
                $registers1[$cestq->GET_COD_PRODUTO]['cobertura'] = $cestq->COBERTURA;
                $registers1[$cestq->GET_COD_PRODUTO]['dtUltEnt'] = $cestq->GET_DT_ULT_ENT;
            }
        }
        $content =   $this->view->render("layoutPDF/indicadoresCobertura", ["id" => "Painel NFe | " . SITE ,
                "registers1" => $registers1
        ]);

        $dompdf->loadHtml($content);
        $dompdf->setPaper("A3");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);
    }

    public function indicadoresICMS() {

    }

    public function indicadoresContasPagar() {
        $loja = $_GET['loja'];
        $dompdf = new Dompdf();
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
        if ($aa2ctipo->TIP_EMPRESA == 200) {
            $aa1rtitu = (new AA1RTITU())->find("DUP_VENC > (select dateto_rms7(sysdate) from dual) and DUP_EMPRESA = $aa2ctipo->TIP_EMPRESA")->order("DUP_VENC_ALT")->fetch(true);
        } else {
            $aa1rtitu = (new AA1RTITU())->find("DUP_VENC > (select dateto_rms7(sysdate) from dual) and DUP_COD_FIL = $loja")->order("DUP_VENC_ALT")->fetch(true);
        }

        $content =   $this->view->render("layoutPDF/indicadoresContasPagar", ["id" => "Painel NFe | " . SITE ,
                "aa1rtitu" => $aa1rtitu
        ]);

        $dompdf->loadHtml($content);
        $dompdf->setPaper("A3");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);

    }

    public function comparativoDesossa() {
        $loja = $_GET['loja'];
        $dompdf = new Dompdf();
        $aa1ctcon = (new AA1CTCON())->getEntradas2();
        $aa2ctipo = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch();
        if ($aa2ctipo->TIP_EMPRESA == 200) {
            $venda = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 42 and ESCHLJC_CODIGO3 in (200,201,203) and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

            $compra = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO in (200,201,203) and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

            $venda2 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 42 and ESCHLJC_CODIGO3 in (200,201,203) and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

            $compra2 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = in (200,201,203) and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
        } else {
            $venda = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 601 and ESCHLJC_CODIGO3 = $loja and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
           $compra = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = $loja and ESCHC_SECAO3 = 6 and ESCHC_GRUPO3 = 7","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();


            $venda2 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 = 601 and ESCHLJC_CODIGO3 = $loja and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();

            $compra2 = (new AG1IENSA())->find("ESCHC_DATA3 BETWEEN (select dateto_rms7(sysdate - 5) from dual) /* -8*/ and (select dateto_rms7(sysdate) from dual) /*-2*/ and ESCHC_AGENDA3 in ($aa1ctcon) and ESCLC_CODIGO = $loja and ESCHC_SECAO3 = 5","","cast(sum(ENTSAIC_PRC_UN * ENTSAIC_QUANTI_UN) as decimal(38, 2)) Valor")->fetch();
        }


        $content =   $this->view->render("layoutPDF/indicadoresComparativoDesossa", ["id" => "Painel NFe | " . SITE ,
                "venda" => $venda, "compra" => $compra,"venda2" => $venda2, "compra2" => $compra2
        ]);

        $dompdf->loadHtml($content);
        $dompdf->setPaper("A3");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);
    }

    public function indicadoresMargemEntrada() {
        if (isset($_GET['operador'])) {
            $user = (new User())->findById($_GET['operador']);
            $user = " and trim(FIS_USUARIO_INC) = '$user->rms_mb'";
        } else {
            $user = '';
        }
        
        if (isset($_GET['loja'])) {
            $store = " and ESCLC_CODIGO = ".$_GET['loja'];
            $storePortal = (new Store())->find("code = ".$_GET['loja'])->fetch();
        } else {
            $store = '';
        }

        if (isset($_GET['numero'])) {
            $numero = " and ESCHC_NRO_NOTA3 = ".$_GET['numero'];
        } else {
            $numero = '';
        }
        
        $dompdf = new Dompdf();
        $date = "1".date('ymd');
        if (URL_BASE == 'http://lancamentosnfe.bdtecsolucoes.com.br:8090') {
            $compras = (new COMPRAS_MARGEM())->find("ESCHC_AGENDA3 not in (4,3,34) AND ESCHC_DATA3 = $date AND (MARGEM < 0 OR MARGEM > 50) $user $store $numero")->order("MARGEM DESC")->fetch(true);
        } else if (URL_BASE == 'http://portalnfe.bdtecsolucoes.com.br:8090') {
            $compras = (new COMPRAS_MARGEM10())->find("ESCHC_AGENDA3 not in (4,3,34) AND ESCHC_DATA3 = $date AND (MARGEM < 0 OR MARGEM > 50) $user $store $numero")->order("MARGEM DESC")->fetch(true);
        }
        
        $portal = new Store();

        $content = $this->view->render("layoutPDF/indicadoresMargemEntrada", ["id" => "Painel NFe | " . SITE , "compras" => $compras, "portal" =>$portal]);
 
        $dompdf->loadHtml($content);
        $dompdf->setPaper("A2");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);

    }

    public function indicadresMargemEntradaConsinco() {
        if ($_GET['operador']) {
            $user = (new User())->findById($_GET['operador']);
            $operador = " AND trim(USULANCTO) = '$user->consinco'";
        }
        $dompdf = new Dompdf();
        $mapNotaFiscal = (new MlfNotaFiscal())->find("dtaentrada = to_date('".date("d/m/Y", strtotime("-4 days"))."', 'DD/MM/YYYY') AND nroempresa in (17,33,17,33,25,27,29,35,36,37,25,27,29,35,36,37) $operador")->fetch(true);


        $i = 1;
        foreach ($mapNotaFiscal as $notaFiscal) {

            foreach ($notaFiscal->mlfNfitem as $mlfNfitem) {

                if ($mlfNfitem->VLRITEM > 0) {
                    $custo = $mlfNfitem->custo;
                    $preco = $mlfNfitem->preco()->PRECO;
                    if ($preco == 0) {
                        $preco2 = $custo;
                    } else {
                        $preco2 = $preco;
                    }
                    $ICMS = $mlfNfitem->ICMS;
                    $PISCOFINS = $mlfNfitem->pisCofins;
                    $margem = round(100-((($custo/$preco2) * 100)+(($PISCOFINS-($PISCOFINS * ($ICMS/100)))+$ICMS)),2);

                    if ($margem > 50 or $margem < 0) {
                        $compras[$i]['ESCLC_CODIGO'] = $notaFiscal->NROEMPRESA;
                        $compras[$i]['ESCHC_NRO_NOTA3'] = $notaFiscal->NUMERONF;
                        $compras[$i]['TIP_CODIGO'] = $notaFiscal->gePessoa()->SEQPESSOA;
                        $compras[$i]['TIP_DIGITO'] = '';
                        $compras[$i]['TIP_RAZAO_SOCIAL'] = $notaFiscal->gePessoa()->NOMERAZAO;
                        $compras[$i]['GIT_COD_ITEM'] = $mlfNfitem->SEQPRODUTO;
                        $compras[$i]['GIT_DIGITO'] = '';
                        $compras[$i]['GIT_DESCRICAO'] = $mlfNfitem->mapProduto()->DESCCOMPLETA;
                        $compras[$i]['ENTSAIC_CUS_UN'] = $custo;
                        $compras[$i]['ENTSAIC_QTD_UN'] = $mlfNfitem->QUANTIDADE;
                        $compras[$i]['PRE_PRECO'] = $preco;
                        $updated_at = new DateTime($mlfNfitem->preco()->UPDATED_AT);
                        $updated_at =  $updated_at->format('d/m/Y');
                        $compras[$i]['PRE_DAT_INICIO'] = $updated_at;
                        $compras[$i]['MARGEM'] = $margem;
                        $compras[$i]['GET_DT_ULT_FAT'] = '';
                        $compras[$i]['FIS_USUARIO_INC'] = $notaFiscal->USULANCTO;
                        $i++;
                    }
                }
            }
        }

        $portal = new Store();
        $content =   $this->view->render("layoutPDF/indicadoresMargemEntrada", ["id" => "Painel NFe | " . SITE , "compras" => $compras, "portal" =>$portal, "consinco" => true]);
        $dompdf->loadHtml($content);
        $dompdf->setPaper("A2");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);
    }

    public function indicadoresNotasLoja() {
        $day = date('Y-m-d');
        $dompdf = new Dompdf();
        $nfReport = (new NfReport())->find("(substr(created_at,1,10) = '$day' or substr(updated_at,1,10) = '$day')")->fetch(true);

        $nfReceipt = (new NfReceipt())->find("(substr(created_at,1,10) = '$day' or substr(updated_at,1,10) = '$day' or status_id in (1,2,5,6,9,17))","","*, TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) duracao")->order("store_id*1")->fetch(true);
        $data = array();
        foreach($nfReceipt as $nf) {
            $data[$nf->store_id]['loja'] = $nf->store_id;
            if (substr($nf->created_at,0,10) == $day) {
                $data[$nf->store_id]['incluida'] += 1;
            }

            if ($nf->status_id == 3) {
                $data[$nf->store_id]['concluida'] += 1;
            }

            if (in_array($nf->status_id, array(1,2,5,6,9,17))) {
                $data[$nf->store_id]['pendente'] += 1;
                $data[$nf->store_id][$nf->status_id] += 1;
                $data[$nf->store_id][$nf->status_id.'duracao'] += $nf->duracao;
            }
        }
        $content =   $this->view->render("layoutPDF/indicadoresNotasLoja", ["id" => "Painel NFe | " . SITE , "data" => $data]);

        $dompdf->loadHtml($content);
        $dompdf->setPaper("A2","landscape");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);


    }

    public function indicadoresOperadoresNotasHora() {
        $dompdf = new Dompdf();
        $nfReceipts = (new NfReceipt())->find("status_id in (3) AND substr(updated_at,1,10) = '".date('Y-m-d')."'","","*, TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) duracao1,TIMESTAMPDIFF(HOUR,updated_at,CURRENT_TIMESTAMP()) duracao ")->fetch(true);
        foreach($nfReceipts as $nfReceipt) {
            $rows[$nfReceipt->operator_id]['operator'] = $nfReceipt->getOperator;
            $rows[$nfReceipt->operator_id]['id'] = $nfReceipt->operator_id;
            $rows[$nfReceipt->operator_id]['quantidade_'.substr($nfReceipt->updated_at,11,2)]++;
            $rows[$nfReceipt->operator_id]['itens_'.substr($nfReceipt->updated_at,11,2)] += $nfReceipt->items_quantity;
        }
        $user = new User();
        $content =  $this->view->render(
            "layoutPDF/indicadoesNotasUsuario",
            ["title" => "Home | " . SITE,
             "rows" => $rows,
             "user" => $user
            ]);
        $dompdf->loadHtml($content);
        $dompdf->setPaper("A1","landscape");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);

    }

    public function indicadoresCheckins() {
        $dompdf = new Dompdf();
        $date1 = date('Y-m-d');
        $date2 = date('Y-m-d');
        if ($_POST['operator']) {
            $operator = " and user_id = '".$_POST['operator']."'";
        }
        $dailies = (new Daily())->find("substr(created_at,1,10) between '$date1' and '$date2' $operator")->order('created_at ASC')->fetch(true);
        foreach($dailies as $daily) {
            $operators[$daily->getUser()->name] = $daily->getUser()->name;
            $dailyOperators[$daily->getUser()->name][$daily->id] = $daily;
        }
        $content = $this->view->render("layoutPDF/indicadoresCheckins", ["title" => "Relatorio Batidas de Ponto | " . SITE, "operators" => $operators, "dailyOperators" => $dailyOperators
        ]);
        $dompdf->loadHtml($content);
        $dompdf->setPaper("A1","landscape");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);
    }

    public function indicadoresCadastroUsuarios() {
        $date = '1'.date('ymd');
        $dompdf = new Dompdf();
        $aa3citem = (new AA3CITEM())->find("'1'||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),5,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),3,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),1,2) = $date")->fetch(true);

        foreach($aa3citem as $item) {
            $log = $item->getLog();
            $rows[$log->LOGIT_USUARIO]['operator'] = $log->LOGIT_USUARIO;
            $rows[$log->LOGIT_USUARIO]['id'] = $log->LOGIT_USUARIO;
            $rows[$log->LOGIT_USUARIO]['quantidade_'.$log->HORA]++;
            $rows[$log->LOGIT_USUARIO]['itens_'.$log->HORA] ++;
        }

        $cadastroConsinco = (new CadastroConsinco())->find("substr(created_at,1,10) = '".date('Y-m-d')."'","","cadastro_consinco.*,substr(created_at,12,2) hora")->fetch(true);

        foreach($cadastroConsinco as $item) {
            $rows[$item->usuario]['operator'] = $item->usuario;
            $rows[$item->usuario]['id'] = $item->usuario;
            $rows[$item->usuario]['quantidade_'.$item->hora]++;
            $rows[$item->usuario]['itens_'.$log->hora] ++;
        }



        $array['tabela'] = "AA3CITEM";
        $date = '1'.date('ymd');
        $array['where'] = "'1'||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),5,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),3,2)||substr(lpad(GIT_DAT_ENT_LIN,6,'0'),1,2) = $date";

        $json = json_encode($array);

        $ch = curl_init('http://indicadores.bdtecsolucoes.com.br:85/bdtecindicadores/acesso');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json))
        );
        $jsonRet = curl_exec($ch);
        $aa3citem2 = json_decode($jsonRet);

        foreach($aa3citem2 as $item2) {
            $rows[$item2->log->LOGIT_USUARIO]['operator'] = $item2->log->LOGIT_USUARIO;
            $rows[$item2->log->LOGIT_USUARIO]['id'] = $item2->log->LOGIT_USUARIO;
            $rows[$item2->log->LOGIT_USUARIO]['quantidade_'.$item2->log->HORA]++;
            $rows[$item2->log->LOGIT_USUARIO]['itens_'.$item2->log->HORA] ++;
        }


        $user = new User();
        $content =  $this->view->render(
            "layoutPDF/indicadoresCadastrosUsuario",
            ["title" => "Home | " . SITE,
             "rows" => $rows,
             "user" => $user
            ]);
        $dompdf->loadHtml($content);
        $dompdf->setPaper("A1","landscape");
        $dompdf->render();
        $dompdf->stream("file.pdf",["Attachment" => false]);
    }
    /**
    * Envios dos PDFs
    */
    public function sendIndicatorsPDF() {
        $stores = (new Store())->find()->fetch(true);
        foreach ($stores as $store) {
            foreach ($store->contacts() as $contato) {
                $array['jid'] = "55$contato->phone@c.us";
                $array['type'] = "document";
                $array['payload']['message'] = URL_BASE."/services-performed/indicadoresPDF?loja=$store->code";
                $array['payload']['legenda'] = " Loja $store->code "."Envio Indicadores Gerais ".date("d-m-Y");
    
                $json = json_encode($array);
    
                $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($json))
                );
                $jsonRet = json_decode(curl_exec($ch));
                $contatos2 .= " $contato->name;";
            }
        }

        echo "<h1>Indicadores enviados para: $contatos2 </h1>";
    }

    public function sendIndicadoresCoberturaPDF() {
        $loja = $_GET['loja'];
        $store = (new Store())->find("code = $loja")->fetch();
        foreach ($store->contacts() as $contato) {
            $array['jid'] = "55$contato->phone@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresCoberturaPDF?loja=$loja";
            $array['payload']['legenda'] = " Loja $loja "."Envio Indicadores Cobertura ".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));
            $contatos2 .= " $contato->name;";
        }
        echo "<h1>Indicadores enviados para: $contatos2 </h1>";
    }

    public function sendIndicadoresNegativosPDF() {
        $loja = $_GET['loja'];
        $store = (new Store())->find("code = $loja")->fetch();
        foreach ($store->contacts() as $contato) {
            $array['jid'] = "55$contato->phone@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresNegativosPDF?loja=$loja";
            $array['payload']['legenda'] = " Loja $loja "."Envio Indicadores Produtos Estoque negativo ".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));
            $contatos2 .= " $contato->name;";
        }
        echo "<h1>Indicadores enviados para: $contatos2 </h1>";
    }

    public function sendSimples() {
        $loja = $_GET['loja'];
        $store = (new Store())->find("code = $loja")->fetch();
        foreach ($store->contacts() as $contato) {
            $array['jid'] = "55$contato->phone@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresSimplesPDF?loja=$loja";
            $array['payload']['legenda'] = " Loja $loja "."Envio Indicadores Notas Simples Nacional ".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));
            $contatos2 .= " $contato->name;";
        }
        echo "<h1>Indicadores enviados para: $contatos2 </h1>";
    }

    public function sendItensSemGiro() {
        $loja = $_GET['loja'];
        $store = (new Store())->find("code = $loja")->fetch();
        foreach ($store->contacts() as $contato) {
            $array['jid'] = "55$contato->phone@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/ItensSemGiro?loja=$loja";
            $array['payload']['legenda'] = " Loja $loja "."Envio Indicadores Sem Giro 90 dias ".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));
            $contatos2 .= " $contato->name;";
        }
        echo "<h1>Indicadores enviados para: $contatos2 </h1>";
    }

    public function sendNFnaoEscrituradas() {
        $loja = $_GET['loja'];
        $store = (new Store())->find("code = $loja")->fetch();
        foreach ($store->contacts() as $contato) {
            $array['jid'] = "55$contato->phone@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/NFnaoEscrituradas?loja=$loja";
            $array['payload']['legenda'] = " Loja $loja "."Envio Indicadores NF-E não escrituradas ".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));
            $contatos2 .= " $contato->name;";
        }
        echo "<h1>Indicadores enviados para: $contatos2 </h1>";
    }

    public function sendComparativoDesossa() {
        $loja = $_GET['loja'];
        $store = (new Store())->find("code = $loja")->fetch();
        foreach ($store->contacts() as $contato) {
            $array['jid'] = "55$contato->phone@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/comparativoDesossa?loja=$loja";
            $array['payload']['legenda'] = " Loja $loja "."Envio Indicadores Compras x Vendas Desossa e Horti ".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));
            $contatos2 .= " $contato->name;";
        }
        echo "<h1>Indicadores enviados para: $contatos2 </h1>";
    }

    public function sendindIcadoresContasPagar() {
        $loja = $_GET['loja'];
        $store = (new Store())->find("code = $loja")->fetch();
        foreach ($store->contacts() as $contato) {
            $array['jid'] = "55$contato->phone@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresContasPagar?loja=$loja";
            $array['payload']['legenda'] = " Loja $loja "."Envio Indicadores Contas a pagar apartir de ".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));
            $contatos2 .= " $contato->name;";
        }
        echo "<h1>Indicadores enviados para: $contatos2 </h1>";
    }

    public function sendIndicadoresMargemEntrada() {
        $contatos = array('7192424238','7199601630','7192201747','7181735580','4796084104','7183808261','7182541131','7187336729','7199506104');
        foreach ($contatos as $contato) {
            $array['jid'] = "55$contato@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresMargemEntrada";
            $array['payload']['legenda'] = "Envio Indicadores Margens MB".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));

            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);
             }
        }
    }

    public function sendIndicadoresNotasLoja() {
        $contatos = array('7192424238','7199601630','7192201747','4796084104','7183808261','7182541131','7187336729','7199506104');
        foreach ($contatos as $contato) {
            $array['jid'] = "55$contato@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresNotasLoja";
            $array['payload']['legenda'] = "Envio Indicadores Notas Lançamento".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));

            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);
             }
        }
    }

    public function sendIndicadoresOperadoresNotasHora() {
        $contatos = array('7192424238','7199506104','7199601630','7192201747','7183808261','7182541131','7191562121');
        foreach ($contatos as $contato) {
            $array['jid'] = "55$contato@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresOperadoresNotasHora";
            $array['payload']['legenda'] = date("d-m-Y")." - Envio Indicadores Notas Lançamento Operadores por Hora";

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));

            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);
             }
        }
    }

    public function sendIndicadoresOperadoresChamadosHora() {
        $contatos = array('7192424238','7199506104','7183808261','7182541131');
        foreach ($contatos as $contato) {
            $array['jid'] = "55$contato@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = "https://bdtecatende.bispoedantas.com.br/services-performed/indicadoresOperadoresNotasHora";
            $array['payload']['legenda'] = date("d-m-Y")." - Envio Indicadores Chamados  Operadores por Hora";

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));

            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);
            }
            var_dump($jsonRet);
        }
    }

    public function sendIndicadoresCheckins() {
        $contatos = array('7199506104','7192424238');
        foreach ($contatos as $contato) {
            $array['jid'] = "55$contato@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresCheckins";
            $array['payload']['legenda'] = date("d-m-Y")." - Envio batimentos de ponto.";

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));

            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);
            }
            var_dump($jsonRet);
        }
    }

    public function sendIndicadoresMargemEntradaConsinco() {
        $contatos = array('7192424238','7181735580','7199601630','7192201747','4796084104','7183808261','7182541131','7187336729','7199506104');
        foreach ($contatos as $contato) {
            $array['jid'] = "55$contato@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = "http://indicadores-redemais.bdtecsolucoes.com.br:8080/bdtecindicadores/margem-entrada";
            $array['payload']['legenda'] = "Envio Indicadores Margens Consinco".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));

            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);
             }
        }
    }

    public function sendIndicadoresCadastroUsuarios() {
        $contatos = array('7192424238','7181735580','7199601630','7192201747','4796084104','7183808261','7182541131','7187336729','7199506104');
        foreach ($contatos as $contato) {
            $array['jid'] = "55$contato@c.us";
            $array['type'] = "document";
            $array['payload']['message'] = URL_BASE."/services-performed/indicadoresCadastroUsuarios";
            $array['payload']['legenda'] = "Envio Indicadores Cadastro Usúario ".date("d-m-Y");

            $json = json_encode($array);

            $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json))
            );
            $jsonRet = json_decode(curl_exec($ch));

            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);
             }
        }
    }
    /**
    * Este metodo dispara os indicadores de cada operador ao bater ponto para sair.
    */
    public function indicadoresOperadoresNotas() {
        $users = (new User())->find("status = 2")->fetch(true);

        if (!empty($users)){
            foreach ($users as $user) {
                /**
                 * Este bloco procura as notas em aberto para sinalizar e alterar.
                 */
                $nfReceipt = (new NfReceipt())->find("operator_id = $user->id and  status_id in (1,2,5,6,9,17)")->fetch(true);
                if (!empty($nfReceipt)){
                    foreach($nfReceipt as $nf) {
                        $mensagem .= "Operador $user->name Saiu e deixou a nota Id: $nf->id \n Stauts: $nf->getStatus \n  Chave: $nf->access_key \n Loja: $nf->store_id \n Incluida $nf->created_at \n Alterada $nf->updated_at \n\n";
                        $nfLost = new NfReceiptLost();
                        $nfLost->id_nf = $nf->id;
                        $nfLost->store_id = $nf->store_id;
                        $nfLost->group_id = $nf->group_id;
                        $nfLost->nf_number = $nf->nf_number;
                        $nfLost->access_key = $nf->access_key;
                        $nfLost->status_id = $nf->status_id;
                        $nfLost->note = $nf->note;
                        $nfLost->client_id = $nf->client_id;
                        $nfLost->operator_id = $nf->operator_id;
                        $nfLost->items_quantity = $nf->items_quantity;
                        $nfLost->type_id = $nf->type_id;
                        $nfLost->operation_id = $nf->operation_id;
                        $nfLost->inclusion_type = $nf->inclusion_type;
                        $nfLost->error_cost = $nf->error_cost;
                        $nfLost->created_at = $nf->created_at;
                        $nfLost->updated_at = $nf->updated_at;

                        $nfLostId = $nfLost->save();

                        $nfUpdate = (new NfReceipt())->findById($nf->id);
                        $nfUpdate->operator_id = 147;
                        $nfUpdateId = $nfUpdate->save();
                    }
                }
                $contatos = array('7192424238','7199601630','7192201747','4796084104','7183808261','7182541131','7187336729');
                foreach ($contatos as $contato) {
                    $array['jid'] = "55$contato@c.us";
                    $array['type'] = "text";
                    $array['payload']['message'] = $mensagem;

                    $json = json_encode($array);

                    $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($json))
                        );
                    $jsonRet = json_decode(curl_exec($ch));

                    if (!curl_errno($ch)) {
                        $info = curl_getinfo($ch);
                    }
                }
                $mensagem = '';
                /**
                 * Este bloco envia as margens dos lançamentos MB do operador
                 */
                $contatos = array($user->tell,'7183808261','7199601630','7192201747','7192424238');

                foreach ($contatos as $contato) {
                    $array['jid'] = "55$contato@c.us";
                    $array['type'] = "document";
                    $array['payload']['message'] = URL_BASE."/services-performed/indicadoresMargemEntrada?operador=$user->id";
                    $array['payload']['legenda'] = "Envio Indicadores Margens MB - $user->name ".date("d-m-Y");
                    $json = json_encode($array);

                    $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($json))
                    );
                    $jsonRet = json_decode(curl_exec($ch));

                    if (!curl_errno($ch)) {
                        $info = curl_getinfo($ch);
                    }
                }
                /**
                 * Este bloco envia as margens dos lançamentos BD do operador
                 */
                $contatos = array($user->tell,'7183808261','7199601630','7192201747','7192424238');

                foreach ($contatos as $contato) {
                    $array['jid'] = "55$contato@c.us";
                    $array['type'] = "document";
                    $array['payload']['message'] = "http://indicadores.bdtecsolucoes.com.br:85/bdtecindicadores/margem-entrada?operador=$user->id";
                    $array['payload']['legenda'] = "Envio Indicadores Margens BD - $user->name ".date("d-m-Y");
                    $json = json_encode($array);

                    $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($json))
                    );
                    $jsonRet = json_decode(curl_exec($ch));

                    if (!curl_errno($ch)) {
                        $info = curl_getinfo($ch);
                    }
                }
                /**
                 * Este bloco envia as margens dos lançamentos CONSINCO do operador
                 */
                $contatos = array($user->tell,'7183808261','7199601630','7192201747','7192424238');

                foreach ($contatos as $contato) {
                    $array['jid'] = "55$contato@c.us";
                    $array['type'] = "document";
                    $array['payload']['message'] = URL_BASE."/services-performed/indicadresMargemEntradaConsinco?operador=$user->id";
                    $array['payload']['legenda'] = "Envio Indicadores Margens Consinco - $user->name ".date("d-m-Y");
                    $json = json_encode($array);

                    $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($json))
                    );
                    $jsonRet = json_decode(curl_exec($ch));

                    if (!curl_errno($ch)) {
                        $info = curl_getinfo($ch);
                    }
                }

                $operador = (new User())->findById($user->id);
                $operador->status = 0;
                $operadorId = $operador->save();
            }
        }
    }

    /**
     * Metodo que busca os cadastros realizados na consinco e insere na tabela cadastros consinco
     */
    public function buscaCadastroConsinco() {
        $mapProduto = (new MapProduto())->find("substr(DTAHORINCLUSAO,0,10) = to_date('".date("d/m/Y")."', 'DD/MM/YYYY')")->fetch(true);

        foreach($mapProduto as $item) {
            $cadastrado = (new CadastroConsinco())->find("SEQPRODUTO = $item->SEQPRODUTO")->count();
            if ($cadastrado == 0) {
                $cadastroConsinco = new CadastroConsinco();
                $cadastroConsinco->seqproduto = $item->SEQPRODUTO;
                $cadastroConsinco->desccompleta = $item->DESCCOMPLETA;
                $cadastroConsinco->usuario = $item->USUARIOINCLUSAO;
                $cadastroConsincoId = $cadastroConsinco->save();
            }
        }
    }

    public function acesso() {
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

        $data = json_decode(file_get_contents('php://input'), true);


        if ($data['tabela'] == 'AA3CITEM') {
             $where = $data['where'];
             $aa3citem = (new AA3CITEM())->find($where)->order("TRIM(GIT_DESCRICAO)")->fetch(true);
             foreach($aa3citem as $row) {
                $info['itens'][] = (array)$row->data;
             }

        } else if ($data['tabela'] == 'HORTI') {
            $where = $data['where'];
            $aa3citem = (new VIEW_HORTI())->find($where)->order("TRIM(GIT_DESCRICAO)")->fetch(true);
            foreach($aa3citem as $row) {
               $info['itens'][] = (array)$row->data;
            }
        }

        echo json_encode($info);
    }

    public function envios() {
        $watsapp = new WhatsappSends();
        $storeContacts = (new StoreContacts())->find()->order("store_id")->fetch(true);
        echo $this->view->render(
            "administrator/envios",
            ["title" => "Envios | " . SITE,
            "watsapp" => $watsapp,
            "storeContacts" => $storeContacts]
        );
    }

    public function indicadoresPendenteCliente() {
        $loja = $_GET['loja'];
        $status = 2;
        if (strlen($loja) > 0) {
            $nfReceipts = (new NfReceipt())->find("store_id = $loja and status_id in ($status)")->fetch(true);
            foreach ($nfReceipts as $nfReceipt) {
                if (substr($nfReceipt->getTimesDuration,0,2) > 0) {
                    $registers1[$nfReceipt->id]['id'] = $nfReceipt->id;
                    $registers1[$nfReceipt->id]['loja'] = $nfReceipt->store_id;
                    $registers1[$nfReceipt->id]['numero'] = $nfReceipt->nf_number;
                    $registers1[$nfReceipt->id]['inclusao'] = $nfReceipt->created_at;
                    $registers1[$nfReceipt->id]['cliente'] = $nfReceipt->getClient;
                    $registers1[$nfReceipt->id]['fornecedor'] = $nfReceipt->getFornecedor;
                    $registers1[$nfReceipt->id]['tipo'] = $nfReceipt->getType;
                    $registers1[$nfReceipt->id]['grupo'] = $nfReceipt->clientGroup;
                    $registers1[$nfReceipt->id]['qtd'] = $nfReceipt->getAmountItems;
                    $registers1[$nfReceipt->id]['alteracao'] = $nfReceipt->updated_at;
                    $registers1[$nfReceipt->id]['operador'] = $nfReceipt->getOperator;
                    $registers1[$nfReceipt->id]['operacao'] = $nfReceipt->getOperation;
                    $registers1[$nfReceipt->id]['status'] = $nfReceipt->getStatus;
    
                    foreach ($nfReceipt->getInteractions as $interaction) {
                        $registers2[$interaction->id][$nfReceipt->id]['user'] = $interaction->user()->name;
                        $registers2[$interaction->id][$nfReceipt->id]['msg'] = $interaction->description;
                        $registers2[$interaction->id][$nfReceipt->id]['anexo'] = $interaction->attachment;
                        $registers2[$interaction->id][$nfReceipt->id]['interactionDate'] = $interaction->getCreatedAt;
                    }
                }
                
            }

            $content = $this->view->render("layoutPDF/pendenteCliente", ["id" => "Relatorio Pendente Cliente | " . SITE ,
            "registers1" => $registers1, "registers2" => $registers2
            ]);

            $dompdf = new Dompdf();
            $dompdf->loadHtml($content);
            $dompdf->setPaper("A2");
            $dompdf->render();
            $dompdf->stream("file.pdf",["Attachment" => false]);
        }
        
    }

    public function sendIndicadoresPendenteCliente() {
        $stores = (new Store())->find()->fetch(true);
        foreach ($stores as $store) {
            $count = (new NfReceipt())->find("store_id = $store->code and status_id = 2 and TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) > 24")->count();
            if ($count > 0) {
                foreach ($store->contactsO() as $contato) {
                    $array['jid'] = "55$contato->phone@c.us";
                    $array['type'] = "document";
                    $array['payload']['message'] = URL_BASE."/services-performed/indicadoresPendenteCliente?loja=$store->code";
                    $array['payload']['legenda'] = " Loja $store->code "."Envio Notas pendente cliente com mais de 24 horas";
        
                    $json = json_encode($array);
        
                    $ch = curl_init('https://s1n1.squidchat.digital:1016/api/v1/sendmessage');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($json))
                    );
                    $jsonRet = json_decode(curl_exec($ch));
                    $contatos2 .= " $contato->name;";
                }
                echo "<h1>Indicadores enviados para: $contatos2 </h1>"; 
                $contatos2 = '';
            }
        }
    }
}