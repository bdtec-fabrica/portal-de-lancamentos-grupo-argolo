<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\FiscalMovement;
use Source\Models\FiscalRegister;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\UserSector;
use Source\Models\Request;
use Source\Models\Call;
use Source\Models\NfReport;
use Source\Models\CallAttachment;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\RequestAttachment;
use Source\Models\Sector;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\Status;
use Source\Models\Criticism;
use Source\Models\Counting;
use Source\Models\StockMovement;
use Source\Models\Session;
use Source\Models\RequestScore;
use Source\Models\UserClass;
use Source\Models\Inventory;
use Source\Models\BlocoH;
use Source\Models\Marketing;
use Source\Models\NfReceipt;
use Source\Models\NfReceipt2;
use Source\Models\NfStatus;
use Source\Models\NfAttachment;
use Source\Models\NfDueDate;
use Source\Models\NfOperation;
use Source\Models\NfReceiptLog;
use Source\Models\NfSefaz;
use Source\Models\Xml;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SendGrid\Mail\Mail;
use SendGrid;
use Symfony\Component\DomCrawler\Crawler;
use DOMDocument;
use Source\Models\AG1IENSA;
use Source\Models\RequestBlock;
use Source\Models\NfEvent;
use Source\Models\DailyClosing;
use Source\Models\NfStarted;
use Source\Models\CollectionHeader;
use Source\Models\NfType;

class Dashboard 
{
    private $view;

    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function home() {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $user = new User();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $stores = $usuario->getStoreGroup2();

            $date = date("Y-m-d");
            if ($class == 'W') {
                $date = date('Y-m-d');
                $month = date('Y-m');
                $request = (new NfReceipt());
                $requests = $request->find("substr(created_at,1,10) = :hoje or substr(updated_at,1,10) = :hoje or (status_id != :concluido and status_id != :carga_finalizada)","hoje=$date&concluido=3&carga_finalizada=14")->fetch(true);
                
                foreach ($requests as $request) {
                    $operators2[$request->getOperator] = $request->getOperator;
                    if (substr($request->created_at,0,10) == $date) {
                        $included += 1;
                        $includedA += $request->getAmountItems;
                        $requestStoreIncluded[$request->store_id] += 1;
                        $requestStoreIncludedA[$request->store_id] += $request->getAmountItems;
                    }

                    if($request->status_id != 3 && $request->status_id != 14) {
                        $pendente += 1;
                        $pendenteA += $request->getAmountItems;
                        $requestStorePendente[$request->store_id] += 1;
                        $requestStorePendenteA[$request->store_id] += $request->getAmountItems;
                    }

                    if ($request->status_id == 1) {
                        $pendingCompany += 1;
                        $pendingCompanyA += $request->getAmountItems;
                        $requestOperationPendenteCompany[$request->operation_id] += 1;
                        $requestOperationAmountPendenteCompany[$request->operation_id] += $request->getAmountItems;
                    }

                    if($request->status_id == 2) {
                        $pendingClient += 1;
                        if ($request->operation_id == 103) {
                            $pendingClientF += 1;
                        }
                        $pendingClientA += $request->getAmountItems;
                        $requestOperatorPendingClient[$request->getOperator] += 1;
                        $requestOperatorAmountPendingClient[$request->getOperator] += $request->getAmountItems;

                    }
                    
                    if($request->status_id == 3 || $request->status_id == 14) {
                        $completed += 1;
                        $completedA += $request->getAmountItems;
                        $requestOperationComleted[$request->operation_id] += 1;
                        $requestOperationAmountCompleted[$request->operation_id] += $request->getAmountItems;
                        $requestOperatorCompleted[$request->getOperator] += 1;
                        $requestOperatorAmountCompleted[$request->getOperator] += $request->getAmountItems;
                        if ($request->type_id != 4) {
                            $requestOperatorCompletedSCd[$request->getOperator] += 1;
                            $requestOperatorAmountCompletedSCd[$request->getOperator] += $request->getAmountItems;
                        }
                        $requestStoreCompleted[$request->store_id] += 1;
                        $requestStoreCompletedA[$request->store_id] += $request->getAmountItems;
                    }
                    
                    if($request->status_id == 4) {
                        $pendingInformation += 1;
                        $pendingInformationA += $request->getAmountItems;
                    }
                    
                    if($request->status_id == 5) {
                        $waitingRelease += 1;
                        $waitingReleaseA += $request->getAmountItems;
                    }
                    
                    if($request->status_id == 6) {
                        $ICMSPending += 1;
                        $ICMSPendingA += $request->getAmountItems;
                    }

                    if($request->status_id == 7) {
                        $canceled += 1;
                        $canceledA += $request->getAmountItems;
                    }

                    if($request->status_id == 8) {
                        $wrongRecipient += 1;
                        $wrongRecipientA += $request->getAmountItems;
                    }
                    
                    if($request->status_id == 9) {
                        $register += 1;
                        $registerA += $request->getAmountItems;
                    }

                    if($request->status_id == 10) {
                        $cargoArrival += 1;
                        $cargoArrivalA += $request->getAmountItems;
                    }

                    if($request->status_id == 11) {
                        $commercialPending += 1;
                        $commercialPendingA += $request->getAmountItems;
                    }

                    if($request->status_id == 12) {
                        $conference += 1;
                        $conferenceA += $request->getAmountItems;
                    }

                    if($request->status_id == 13) {
                        $nfeUpdate += 1;
                        $nfeUpdateA += $request->getAmountItems;
                    }

                    if($request->status_id == 14) {
                        $finishedCargo += 1;
                        $finishedCargoA += $request->getAmountItems;
                    }

                    if($request->status_id == 15) {
                        $price += 1;
                        $priceA += $request->getAmountItems;
                    }
                }
                $operations = (new NfOperation())->find()->fetch(true);

                $nfEvent = (new NfEvent());
                $nfEvents = $nfEvent->find("substr(updated_at,1,10) = :date or substr(created_at,1,10) = :date1","date=$date&date1=$date")->fetch(true);
                $nfEventsMonth = $nfEvent->find("substr(created_at,1,7) = :date","date=$month")->fetch(true);


                foreach ($nfEventsMonth as $nEM) {
                    if ($nEM->status_id == 11){
                        $pendingComercialMonth += 1;
                        if ($pendingComercialMonthIds) {
                            $pendingComercialMonthIds = "$pendingComercialMonthIds,$nEM->id_nf_receipt";
                        } else {
                            $pendingComercialMonthIds = $nEM->id_nf_receipt;
                        }
                        
                    }
                }
                foreach ($nfEvents as $nE) {
                    if ($nE->status_id == 2){
                        $pendingClientEvent += 1;
                    }

                    if ($nE->status_id == 11){
                        $pendingComercial += 1;
                    }

                    if ($nE->status_id == 6){
                        $pendingICMSEvent += 1;
                        $ICMSOperators[$nE->getFinalUser] = $nE->getFinalUser;
                        $amountICMSOperator[$nE->getFinalUser] += 1;
                    }
                    
                    if ($nE->status_id == 9){
                        $registerEvent += 1;
                        $registerOperators[$nE->getFinalUser] = $nE->getFinalUser;
                        $amountRegisterOperator[$nE->getFinalUser] += 1;
                        $amountItemRegisterOperator[$nE->getFinalUser] += $nE->getAmountItems;
                    }
                    
                    if ($nE->status_id == 4){
                        $pendingInformation += 1;
                    }
                }
                $user = new User();

                    $today = date("Y-m-d");
                    $month = date("Y-m");
                    /**
                     * reporte diario Operador
                     */
                    $todayNfReport = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->fetch(true);
                    $todayNfReportCount = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->count();
    
                    if ($todayNfReportCount>0){
                        foreach ($todayNfReport as $tnr) {
                            $todayNfReport1[$tnr->id_request] = $tnr->id_request;
                        }
                        $todayNfReport = implode(",",$todayNfReport1);                    
                    } else {
                        $todayNfReport = '-1';
                    }
    
                    /**
                     * reporte diario Cliente
                     */
                    $clientTodayNfReport = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('C').")")->fetch(true);
                    $clientTodayNfReportCount = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('C').")")->count();
                    if ($clientTodayNfReportCount>0){
                        foreach ($clientTodayNfReport as $tnr) {
                            $todayNfReport2[$tnr->id_request] = $tnr->id_request;
                        }
                        $clientTodayNfReport = implode(",",$todayNfReport2);                    
                    } else {
                        $clientTodayNfReport = '-1';
                    }
                    /**
                     * reporte diario indevido
                     */
                    $todayNfReportIndevido = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->fetch(true);
                    $todayNfReportCountIndevido = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->count();
    
                    if ($todayNfReportCountIndevido>0){
                        foreach ($todayNfReport as $tnr) {
                            $todayNfReportIndevido1[$tnr->id_request] = $tnr->id_request;
                        }
                        if (is_array($todayNfReportIndevido1)) {
                            $todayNfReportIndevido = implode(",",$todayNfReportIndevido1); 
                        }
                                           
                    } else {
                        $todayNfReportIndevido = '-1';
                    }

                    $MonthlyNfReportIndevido = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id = 0")->fetch(true);
                    $MonthlyNfReportCountIndevido = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id = 0")->count();

                    if ($MonthlyNfReportCountIndevido>0){
                        foreach ($MonthlyNfReportIndevido as $tnr) {
                            $todayNfReport3Indevido[$tnr->id_request] = $tnr->id_request;
                        }
                        $MonthlyNfReportIndevido = implode(",",$todayNfReport3Indevido);                    
                    } else {
                        $MonthlyNfReportIndevido = '-1';
                    }
                    /**
                     * reporte diario Operador
                     */
                    $MonthlyNfReport = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('A').")")->fetch(true);
                    $MonthlyNfReportCount = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('A').")")->count();

                    if ($MonthlyNfReportCount>0){
                        foreach ($MonthlyNfReport as $tnr) {
                            $todayNfReport3[$tnr->id_request] = $tnr->id_request;
                        }
                        $MonthlyNfReport = implode(",",$todayNfReport3);                    
                    } else {
                        $MonthlyNfReport = '-1';
                    }
    
                    /**
                     * reporte diario Cliente
                     */
                    $clientMonthlyNfReport = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('C').")")->fetch(true);
                    $clientMonthlyNfReportCount = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('C').")")->count();
                    
                    if ($clientMonthlyNfReport>0){
                        foreach ($clientMonthlyNfReport as $tnr) {
                            $todayNfReport4[$tnr->id_request] = $tnr->id_request;
                        }
                        $clientMonthlyNfReport = implode(",",$todayNfReport4);                    
                    } else {
                        $clientMonthlyNfReport = '-1';
                    }

                $stores = (new Store())->find()->fetch(true);
                $horas = (new NfReceipt())->find("substr(created_at,1,10) = :date and status_id = :status_id","date=$date&status_id=3")->order("updated_at")->fetch(true);

                unset($operators);
                foreach($horas as $hora) {
                    $completedHour[substr($hora->updated_at,11,2)] = substr($hora->updated_at,11,2);
                    $operators[$hora->getOperator] = $hora->getOperator;
                }
                
                if (is_array($completedHour)){
                    $hour = "'".implode("', '",$completedHour)."'";
                }
                
                if ($operators) {
                    foreach ($operators as $operator) {
                        if (is_array($completedHour)){
                            foreach (array_unique($completedHour) as $i) {
                                $user = (new User())->find("name = :name","name=$operator")->fetch()->id;
                                $i2 = str_pad($i,2,'0',STR_PAD_LEFT);
                                $intera = (new NfReceipt())->find("substr(updated_at,1,10) = '$date' and substr(updated_at,12,2) = '$i2' and operator_id = $user")->count();
        
                                $operatorCount[$operator] = $operatorCount[$operator].",".$intera;
                            }
                        }

                        
                        $l[$operator] = $operator;
                        $c[$operator] = sprintf('#%06X', mt_rand(0, 0XFFFFFF));
                    }
                }
                $horas = (new NfReceipt())->find("(substr(created_at,1,10) = :date1 or substr(updated_at,1,10) = :date2)","date2=$date&date1=$date")->order("substr(created_at,11,2)")->fetch(true);
               
                foreach ($horas as $hora){
                    $i++;
                    if(substr($hora->created_at,0,10) == $date) {
                        $times[substr($hora->created_at,11,2)] = substr($hora->created_at,11,2);
                        $includedHour[substr($hora->created_at,11,2)] += 1;
                    }
                    
                    if ($hora->status_id == 3) {
                        $refreshHour[substr($hora->updated_at,11,2)] += 1;
                    } else {
                        $refreshHour[substr($hora->updated_at,11,2)] += 0;
                    }
                }
                
                if (is_array($times)){
                    ksort($times);

                    $time = "'".implode("','",$times)."'";
                    
                }
                
                if (is_array($includedHour)){
                    ksort($includedHour);
                    $includedHour = "'".implode("','",$includedHour)."'";
                }

                if (is_array($refreshHour)){
                    ksort($refreshHour);
                    $refreshHour = "'".implode("','",$refreshHour)."'";
                }

                
                echo $this->view->render(
                    "administrator/home",
                    ["title" => "Home | " . SITE,
                    "operators" => $operators,
                    "operators2" => $operators2,
                    "operatorCounts" => $operatorCount,
                    "register" => $register,
                    "includedHour" => $includedHour,
                    "completedHour" => $refreshHour,
                    "intera" => $intera,
                    "registerA" => $registerA,
                    "wrongRecipient" => $wrongRecipient,
                    "wrongRecipientA" => $wrongRecipientA,
                    "canceled" => $canceled,
                    "canceledA" => $canceledA,
                    "clientMonthlyNfReport" => $clientMonthlyNfReport,
                    "clientMonthlyNfReportCount" => $clientMonthlyNfReportCount,
                    "MonthlyNfReport" => $MonthlyNfReport,
                    "MonthlyNfReportCount" => $MonthlyNfReportCount,
                    "clientTodayNfReport" => $clientTodayNfReport,
                    "clientTodayNfReportCount" => $clientTodayNfReportCount,
                    "todayNfReport" => $todayNfReport,
                    "todayNfReportCount" => $todayNfReportCount,
                    "MonthlyNfReportIndevido" => $MonthlyNfReportIndevido,
                    "MonthlyNfReportCountIndevido" => $MonthlyNfReportCountIndevido,
                    "todayNfReportIndevido" => $todayNfReportIndevido,
                    "todayNfReportCountIndevido" => $todayNfReportCountIndevido,
                    "ICMSPending" => $ICMSPending,
                    "ICMSPendingA" => $ICMSPendingA,
                    "waitingRelease" => $waitingRelease,
                    "waitingReleaseA" => $waitingReleaseA,
                    "pendingInformation" => $pendingInformation,
                    "pendingInformationA" => $pendingInformationA,
                    "completed" => $completed,
                    "completedA" => $completedA,
                    "pendingClient" => $pendingClient,
                    "pendingClientA" => $pendingClientA,
                    "pendingClientF" => $pendingClientF,
                    "pendingCompany" => $pendingCompany,
                    "pendingCompanyA" => $pendingCompanyA,
                    "included" => $included,
                    "includedA" => $includedA,
                    "cargoArrival" => $cargoArrival,
                    "cargoArrivalA" => $cargoArrivalA,
                    "commercialPending" => $commercialPending,
                    "commercialPendingA" => $commercialPendingA,
                    "conference" => $conference,
                    "conferenceA" => $conferenceA,
                    "nfeUpdate" => $nfeUpdate,
                    "nfeUpdateA" => $nfeUpdateA,
                    "finishedCargo" => $finishedCargo,
                    "finishedCargoA" => $finishedCargoA,
                    "price" => $price,
                    "priceA" => $price,
                    "nfEvent" => $nfEvent,
                    "pendingClientEvent" => $pendingClientEvent,
                    "pendingComercial" => $pendingComercial,
                    "pendingComercialMonthIds" => $pendingComercialMonthIds,
                    "pendingComercialMonth" => $pendingComercialMonth,
                    "pendingICMSEvent" => $pendingICMSEvent,
                    "registerEvent" => $registerEvent,
                    "registerOperators" => $registerOperators,
                    "amountRegisterOperator" => $amountRegisterOperator,
                    "amountItemRegisterOperator" => $amountItemRegisterOperator,
                    "pendingInformation" => $pendingInformation,
                    "ICMSOperators" => $ICMSOperators,
                    "amountICMSOperator" => $amountICMSOperator,
                    "operations" => $operations,
                    "requestOperationPendenteCompany" => $requestOperationPendenteCompany,
                    "requestOperationAmountPendenteCompany" => $requestOperationAmountPendenteCompany,
                    "request" => $request,
                    "requestOperationComleted" => $requestOperationComleted,
                    "requestOperationAmountCompleted" => $requestOperationAmountCompleted,
                    "requestOperatorCompleted" => $requestOperatorCompleted,
                    "requestOperatorAmountCompleted" => $requestOperatorAmountCompleted,
                    "requestOperatorPendingClient" => $requestOperatorPendingClient,
                    "requestOperatorAmountPendingClient" => $requestOperatorAmountPendingClient,
                    "requestOperatorCompletedSCd" => $requestOperatorCompletedSCd,
                    "requestOperatorAmountCompletedSCd" => $requestOperatorAmountCompletedSCd,
                    "requestStoreIncluded" => $requestStoreIncluded,
                    "requestStoreIncludedA" => $requestStoreIncludedA,
                    "requestStorePendente" => $requestStorePendente,
                    "requestStorePendenteA" => $requestStorePendenteA,
                    "requestStoreCompleted" => $requestStoreCompleted,
                    "requestStoreCompletedA" => $requestStoreCompletedA,
                    "stores" => $stores,
                      "l" => $l,
                     "c" => $c,
                     "hour" => $hour,
                     "horas" => $time



                    
                     ]);
                
            } else if ($class == 'C') {
                
                $users = (new User())->find("store_code = :sc","sc=$store")->fetch(true);
                foreach($users as $user) {
                    $userId[$user->id] = $user->id;
                }
                $userId = implode(",",$userId);
                $date = date('Y-m-d');
                $request = (new NfReceipt());
                $included = $request->find("substr(created_at,1,10) = :date and store_id = :store","date=$date&store=$store")->count();
                $pendingCompany = $request->find("status_id in (4,5,6,1) and store_id = $store")->count();

                $attendance = $request->find("status_id = :status_id and store_id = :store","status_id=9&store=$store")->count();
                $attendanceAmounts = $request->find("status_id = :status_id and store_id = :store","store=$store&status_id=9")->fetch(true);
                foreach ($attendanceAmounts as $attendanceAmount){
                    $attendanceA += $attendanceAmount->getAmountItems;
                }
                $completed = $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :store","status_id=3&date=$date&store=$store")->count();
                $pendingClient= $request->find("status_id = :status_id and store_id = :store","status_id=2&store=$store")->count();
                $nfEvent = new NfEvent();
                $request = (new NfReceipt());







                $today = date("Y-m-d");
                $month = date("Y-m");

                /**
                 * reporte diario Cliente
                 */
                $clientTodayNfReport = (new NfReport())->find("user_id in ($userId) and substr(created_at,1,10) = '$today'")->fetch(true);
                
                $clientTodayNfReportCount = (new NfReport())->find("user_id in ($userId) and substr(created_at,1,10) = '$today'")->count();
                
                if ($clientTodayNfReportCount>0){
                    foreach ($clientTodayNfReport as $tnr) {
                        $todayNfReport2[$tnr->id_request] = $tnr->id_request;
                    }
                    $clientTodayNfReport = implode(",",$todayNfReport2);                    
                } else {
                    $clientTodayNfReport = '-1';
                }

                /**
                 * reporte diario Cliente
                 */
                $clientMonthlyNfReport = (new NfReport())->find("user_id in ($userId) and  substr(created_at,1,7) = '$month'")->fetch(true);
                $clientMonthlyNfReportCount = (new NfReport())->find("user_id in ($userId) and substr(created_at,1,7) = '$month'")->count();
                
                if ($clientMonthlyNfReport>0){
                    foreach ($clientMonthlyNfReport as $tnr) {
                        $todayNfReport4[$tnr->id_request] = $tnr->id_request;
                    }
                    $clientMonthlyNfReport = implode(",",$todayNfReport4);                    
                } else {
                    $clientMonthlyNfReport = '-1';
                }
               
                
                
                echo $this->view->render(
                    "client/home",
                    ["title" => "Home | " . SITE,
                    "included" => $included,
                    "times" => $time,
                    "clientMonthlyNfReportCount" => $clientMonthlyNfReportCount,
                    "clientMonthlyNfReport" => $clientMonthlyNfReport,
                    "clientTodayNfReportCount" => $clientTodayNfReportCount,
                    "clientTodayNfReport" => $clientTodayNfReport,
                    "pendingCompany" => $pendingCompany,
                    "completed" =>  $completed,
                    "pendingClient" => $pendingClient,
                    "attendance" => $attendance,
                    "attendanceA" => $attendanceA,
                    "nfEvent" => $nfEvent,
                    "request" => $request,
                    "store" => $store]
                );
            } else if ($class == 'O') {
                $collectionHeaders =  (new CollectionHeader())->find("status != :status","status=C")->fetch(true);
                echo $this->view->render(
                    "operator/nfePainel",
                    ["title" => "Painel de Notas | " . SITE,
                    "store" =>$store,
                    "collectionHeaders" => $collectionHeaders
                     ]
                );
            }  else if ($class == 'C2') {
                $request = (new NfReceipt());
                /**
                 * Chegado da carga
                 */
                $cargoArrival += $request->find("status_id = :status_id and store_id = :sid","status_id=10&sid=$store")->count();
                $cargoArrivalAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=10&sid=$store")->fetch(true);
                foreach ($cargoArrivalAmounts as $cargoArrivalAmount){
                    $cargoArrivalA += $cargoArrivalAmount->getAmountItems;
                }
                /**
                 * NFe Lançamento
                 */
                $nfeReceiving += $request->find("status_id = :status_id and store_id = :sid","status_id=11&sid=$store")->count();
                $nfeReceivingAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=11&sid=$store")->fetch(true);
                foreach ($nfeReceivingAmounts as $nfeReceivingAmount){
                    $nfeReceivingA += $nfeReceivingAmount->getAmountItems;
                }
                /**
                 * Pendência Comercial
                 */
                $commercialPending += $request->find("status_id = :status_id and store_id = :sid","status_id=11&sid=$store")->count();
                $commercialPendingAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=11&sid=$store")->fetch(true);
                foreach ($commercialPendingAmounts as $commercialPendingAmount){
                    $commercialPendingA += $commercialPendingAmount->getAmountItems;
                }
                /**
                 * Conferência
                 */
                $conference += $request->find("status_id = :status_id and store_id = :sid","status_id=12&sid=$store")->count();
                $conferenceAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=12&sid=$store")->fetch(true);
                foreach ($conferenceAmounts as $conferenceAmount){
                    $conferenceA += $conferenceAmount->getAmountItems;
                }
                /**
                 * NFe a atualizar
                 */
                $nfeUpdate += $request->find("status_id = :status_id and store_id = :sid","status_id=13&sid=$store")->count();
                $nfeUpdateAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=13&sid=$store")->fetch(true);
                foreach ($nfeUpdateAmounts as $nfeUpdateAmount){
                    $nfeUpdateA += $nfeUpdateAmount->getAmountItems;
                }
                /**
                 * Carga Finalizada
                 */

                $finishedCargo  += $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :sid","status_id=14&date=$date&sid=$store")->count();
               
                $finishedCargoAmounts = $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :sid","status_id=14&date=$date&sid=$store")->fetch(true);
                foreach ($finishedCargoAmounts as $finishedCargoAmount){
                    $finishedCargoA += $finishedCargoAmount->getAmountItems;
                }
                /**
                 * Carga Finalizada
                 */
                $precificar  += $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :sid","status_id=15&date=$date&sid=$store")->count();
                $precificar2 = $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :sid","status_id=15&date=$date&sid=$store")->fetch(true);
                foreach ($precificar2 as $precificar3){
                    $precificarA += $precificar3->getAmountItems;
                }
                /**
                 * Incluida e Finalizada
                 */
                
                $stores = (new NfReceipt())->find("substr(created_at,1,10) = :date and store_id = :sid","date=$date&sid=$store")->group('substr(access_key,7,14)')->order('substr(access_key,7,14)')->fetch(true);
                foreach($stores as $loja) {
                    $storeIncluded["$loja->getFornecedor"] += (new NfReceipt())->find("substr(created_at,1,10) = :data and substr(access_key,7,14) = :cnpj and store_id = :sid","cnpj='$loja->getCnpjFornecedor'&data=$date&sid=$store")->count();

                    $storeIncludedAmounts = (new NfReceipt())->find("substr(created_at,1,10) = :data and substr(access_key,7,14) = :cnpj and store_id = :sid","cnpj='$loja->getCnpjFornecedor'&data=$date&sid=$store")->fetch(true);
                    foreach ($storeIncludedAmounts as $storeIncludedAmount) {              
                        $storeIncludedA["$loja->getFornecedor"] += $storeIncludedAmount->getAmountItems;
                    } 
                    // 7 a 20
                    $storeCompleted["$loja->getFornecedor"] += (new NfReceipt())->find("substr(created_at,1,10) = :data and substr(access_key,7,14) = :cnpj and status_id = :status and store_id = :sid","cnpj='$loja->getCnpjFornecedor'&data=$date&status=6&sid=$store")->count();
                    $storeCompletedAmounts = (new NfReceipt())->find("substr(created_at,1,10) = :data and substr(access_key,7,14) = :cnpj and status_id = :status and store_id = :sid","cnpj='$loja->getCnpjFornecedor'&data=$date&status=6&sid=$store")->fetch(true);
                    foreach ($storeCompletedAmounts as $storeCompletedAmount) {              
                        $storeCompletedA["$loja->getFornecedor"] += $storeCompletedAmount->getAmountItems;
                    }
                }
                $request = (new NfReceipt2());
                /**
                 * Chegado da carga
                 */
                $cargoArrival += $request->find("status_id = :status_id and store_id = :sid","status_id=10&sid=$store")->count();
                $cargoArrivalAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=10&sid=$store")->fetch(true);
                foreach ($cargoArrivalAmounts as $cargoArrivalAmount){
                    $cargoArrivalA += $cargoArrivalAmount->getAmountItems;
                }
                /**
                 * NFe Lançamento
                 */
                $nfeReceiving += $request->find("status_id = :status_id and store_id = :sid","status_id=11&sid=$store")->count();
                $nfeReceivingAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=11&sid=$store")->fetch(true);
                foreach ($nfeReceivingAmounts as $nfeReceivingAmount){
                    $nfeReceivingA += $nfeReceivingAmount->getAmountItems;
                }
                /**
                 * Pendência Comercial
                 */
                $commercialPending += $request->find("status_id = :status_id and store_id = :sid","status_id=11&sid=$store")->count();
                $commercialPendingAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=11&sid=$store")->fetch(true);
                foreach ($commercialPendingAmounts as $commercialPendingAmount){
                    $commercialPendingA += $commercialPendingAmount->getAmountItems;
                }
                /**
                 * Conferência
                 */
                $conference += $request->find("status_id = :status_id and store_id = :sid","status_id=12&sid=$store")->count();
                $conferenceAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=12&sid=$store")->fetch(true);
                foreach ($conferenceAmounts as $conferenceAmount){
                    $conferenceA += $conferenceAmount->getAmountItems;
                }
                /**
                 * NFe a atualizar
                 */
                $nfeUpdate += $request->find("status_id = :status_id and store_id = :sid","status_id=13&sid=$store")->count();
                $nfeUpdateAmounts = $request->find("status_id = :status_id and store_id = :sid","status_id=13&sid=$store")->fetch(true);
                foreach ($nfeUpdateAmounts as $nfeUpdateAmount){
                    $nfeUpdateA += $nfeUpdateAmount->getAmountItems;
                }
                /**
                 * Carga Finalizada
                 */

                $finishedCargo  += $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :sid","status_id=14&date=$date&sid=$store")->count();
               
                $finishedCargoAmounts = $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :sid","status_id=14&date=$date&sid=$store")->fetch(true);
                foreach ($finishedCargoAmounts as $finishedCargoAmount){
                    $finishedCargoA += $finishedCargoAmount->getAmountItems;
                }
                /**
                 * Carga Finalizada
                 */
                $precificar  += $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :sid","status_id=15&date=$date&sid=$store")->count();
                $precificar2 = $request->find("status_id = :status_id and substr(updated_at,1,10) = :date and store_id = :sid","status_id=15&date=$date&sid=$store")->fetch(true);
                foreach ($precificar2 as $precificar3){
                    $precificarA += $precificar3->getAmountItems;
                }
                /**
                 * Incluida e Finalizada
                 */
                
                $stores = (new NfReceipt2())->find("substr(created_at,1,10) = :date and store_id = :sid","date=$date&sid=$store")->group('substr(access_key,7,14)')->order('substr(access_key,7,14)')->fetch(true);
                foreach($stores as $loja) {
                    $storeIncluded["$loja->getFornecedor"] += (new NfReceipt2())->find("substr(created_at,1,10) = :data and substr(access_key,7,14) = :cnpj and store_id = :sid","cnpj='$loja->getCnpjFornecedor'&data=$date&sid=$store")->count();

                    $storeIncludedAmounts += (new NfReceipt2())->find("substr(created_at,1,10) = :data and substr(access_key,7,14) = :cnpj and store_id = :sid","cnpj='$loja->getCnpjFornecedor'&data=$date&sid=$store")->fetch(true);
                    foreach ($storeIncludedAmounts as $storeIncludedAmount) {              
                        $storeIncludedA["$loja->getFornecedor"] += $storeIncludedAmount->getAmountItems;
                    } 
                    // 7 a 20
                    $storeCompleted["$loja->getFornecedor"] += (new NfReceipt2())->find("substr(created_at,1,10) = :data and substr(access_key,7,14) = :cnpj and status_id = :status and store_id = :sid","cnpj='$loja->getCnpjFornecedor'&data=$date&status=6&sid=$store")->count();
                    $storeCompletedAmounts = (new NfReceipt2())->find("substr(created_at,1,10) = :data and substr(access_key,7,14) = :cnpj and status_id = :status and store_id = :sid","cnpj='$loja->getCnpjFornecedor'&data=$date&status=6&sid=$store")->fetch(true);
                    foreach ($storeCompletedAmounts as $storeCompletedAmount) {              
                        $storeCompletedA["$loja->getFornecedor"] += $storeCompletedAmount->getAmountItems;
                    }
                }
                // die;

                /**
                 * duração no status
                 */
                $nfEvent = new NfEvent();
                echo $this->view->render(
                    "client/home2",
                    ["title" => "Home | " . SITE,
                     "request" => $request,
                     "cargoArrival" => $cargoArrival,
                     "cargoArrivalA" => $cargoArrivalA,
                     "nfeReceiving" => $nfeReceiving,
                     "nfeReceivingA" >$nfeReceivingA,
                     "commercialPending" => $commercialPending,
                     "commercialPendingA" => $commercialPendingA,
                     "conference" => $conference,
                     "conferenceA" => $conferenceA,
                     "nfeUpdate" => $nfeUpdate,
                     "nfeUpdateA" => $nfeUpdateA,
                     "finishedCargo" => $finishedCargo,
                     "finishedCargoA" => $finishedCargoA,
                     "nfEvent" => $nfEvent,
                     "storeCompleted" => $storeCompleted,
                     "storeCompletedA" => $storeCompletedA,
                     "storeIncluded" => $storeIncluded,
                     "storeIncludedA" => $storeIncludedA,
                     "stores" => $stores,
                     "precificar" => $precificar,
                     "precificarA" => $precificarA
                     ]
                );
            }  else if ($class == 'OG') {
                $request = (new NfReceipt());
                /**
                 * Chegado da carga
                 */

                $cargoArrival += $request->find("status_id = 10 and store_id in ($stores)")->count();
                $cargoArrivalAmounts = $request->find("status_id = 10 and store_id in ($stores)")->fetch(true);
                foreach ($cargoArrivalAmounts as $cargoArrivalAmount){
                    $cargoArrivalA += $cargoArrivalAmount->getAmountItems;
                }
                /**
                 * NFe Lançamento
                 */
                $nfeReceiving += $request->find("status_id = 11 and store_id in ($stores)")->count();
                $nfeReceivingAmounts = $request->find("status_id = 11 and store_id in ($stores)")->fetch(true);
                foreach ($nfeReceivingAmounts as $nfeReceivingAmount){
                    $nfeReceivingA += $nfeReceivingAmount->getAmountItems;
                }
                /**
                 * Pendência Comercial
                 */
                $commercialPending += $request->find("status_id = 11 and store_id in ($stores)")->count();
                $commercialPendingAmounts = $request->find("status_id = 11 and store_id in ($stores)")->fetch(true);
                foreach ($commercialPendingAmounts as $commercialPendingAmount){
                    $commercialPendingA += $commercialPendingAmount->getAmountItems;
                }
                /**
                 * Conferência
                 */
                $conference += $request->find("status_id = 12 and store_id in ($stores)")->count();
                $conferenceAmounts = $request->find("status_id = 12 and store_id in ($stores)")->fetch(true);
                foreach ($conferenceAmounts as $conferenceAmount){
                    $conferenceA += $conferenceAmount->getAmountItems;
                }
                /**
                 * NFe a atualizar
                 */
                $nfeUpdate += $request->find("status_id = 13 and store_id in ($stores)")->count();
                $nfeUpdateAmounts = $request->find("status_id = 13 and store_id in ($stores)")->fetch(true);
                foreach ($nfeUpdateAmounts as $nfeUpdateAmount){
                    $nfeUpdateA += $nfeUpdateAmount->getAmountItems;
                }
                /**
                 * Carga Finalizada
                 */

                $finishedCargo  += $request->find("status_id = 14 and store_id in ($stores) and substr(updated_at,1,10) = '$date'")->count();
                $finishedCargoAmounts = $request->find("status_id = 14 and store_id in ($stores) and substr(updated_at,1,10) = '$date'")->fetch(true);
                foreach ($finishedCargoAmounts as $finishedCargoAmount){
                    $finishedCargoA += $finishedCargoAmount->getAmountItems;
                }
                /**
                 * Carga Finalizada
                 */
                $precificar  += $request->find("status_id = 15 and store_id in ($stores) and substr(updated_at,1,10) = '$date'")->count();
                $precificar2 = $request->find("status_id = 15 and store_id in ($stores) and substr(updated_at,1,10) = '$date'")->fetch(true);
                foreach ($precificar2 as $precificar3){
                    $precificarA += $precificar3->getAmountItems;
                }
                /**
                 * Incluida e Finalizada
                 */
                
                $stores2 = (new NfReceipt())->find("substr(created_at,1,10) = '$date' and store_id in ($stores)")->group('substr(access_key,7,14)')->order('substr(access_key,7,14)')->fetch(true);
                foreach($stores2 as $loja) {
                   
                    
                    // $storeIncluded[$loja->getFornecedor] += (new NfReceipt())->find("substr(created_at,1,10) = '$date' and store_id in ($stores) and substr(access_key,7,14) = '$loja->getCnpjFornecedor'")->count();

                    $storeIncludedAmounts = (new NfReceipt())->find("substr(created_at,1,10) = '$date' and store_id in ($stores) and substr(access_key,7,14) = '$loja->getCnpjFornecedor'")->fetch(true);
                    foreach ($storeIncludedAmounts as $storeIncludedAmount) {              
                        $storeIncludedA["$loja->getFornecedor"] += $storeIncludedAmount->getAmountItems;
                    } 
                    // 7 a 20
                    $storeCompleted["$loja->getFornecedor"] += (new NfReceipt())->find("substr(created_at,1,10) = '$date' and substr(access_key,7,14) = '$loja->getCnpjFornecedor' and status_id = 14 and store_id in ($stores)")->count();
                    $storeCompletedAmounts = (new NfReceipt())->find("substr(created_at,1,10) = '$date' and substr(access_key,7,14) = '$loja->getCnpjFornecedor' and status_id = 14 and store_id in ($stores)")->fetch(true);
                    foreach ($storeCompletedAmounts as $storeCompletedAmount) {              
                        $storeCompletedA["$loja->getFornecedor"] += $storeCompletedAmount->getAmountItems;
                    }
                }

                $request = (new NfReceipt2());
                /**
                 * Chegado da carga
                 */

                $cargoArrival += $request->find("status_id = 10 and store_id in ($stores)")->count();

                $cargoArrivalAmounts = $request->find("status_id = 10 and store_id in ($stores)")->fetch(true);
                foreach ($cargoArrivalAmounts as $cargoArrivalAmount){
                    $cargoArrivalA += $cargoArrivalAmount->getAmountItems;
                }
                /**
                 * NFe Lançamento
                 */
                $nfeReceiving += $request->find("status_id = 11 and store_id in ($stores)")->count();
                $nfeReceivingAmounts = $request->find("status_id = 11 and store_id in ($stores)")->fetch(true);
                foreach ($nfeReceivingAmounts as $nfeReceivingAmount){
                    $nfeReceivingA += $nfeReceivingAmount->getAmountItems;
                }
                /**
                 * Pendência Comercial
                 */
                $commercialPending += $request->find("status_id = 11 and store_id in ($stores)")->count();
                $commercialPendingAmounts = $request->find("status_id = 11 and store_id in ($stores)")->fetch(true);
                foreach ($commercialPendingAmounts as $commercialPendingAmount){
                    $commercialPendingA += $commercialPendingAmount->getAmountItems;
                }
                /**
                 * Conferência
                 */
                $conference += $request->find("status_id = 12 and store_id in ($stores)")->count();
                $conferenceAmounts = $request->find("status_id = 12 and store_id in ($stores)")->fetch(true);
                foreach ($conferenceAmounts as $conferenceAmount){
                    $conferenceA += $conferenceAmount->getAmountItems;
                }
                /**
                 * NFe a atualizar
                 */
                $nfeUpdate += $request->find("status_id = 13 and store_id in ($stores)")->count();
                $nfeUpdateAmounts = $request->find("status_id = 13 and store_id in ($stores)")->fetch(true);
                foreach ($nfeUpdateAmounts as $nfeUpdateAmount){
                    $nfeUpdateA += $nfeUpdateAmount->getAmountItems;
                }
                /**
                 * Carga Finalizada
                 */

                $finishedCargo  += $request->find("status_id = 14 and store_id in ($stores) and substr(updated_at,1,10) = '$date'")->count();

               
                $finishedCargoAmounts = $request->find("status_id = 14 and store_id in ($stores) and substr(updated_at,1,10) = '$date'")->fetch(true);
                foreach ($finishedCargoAmounts as $finishedCargoAmount){
                    $finishedCargoA += $finishedCargoAmount->getAmountItems;
                }
                /**
                 * Carga Finalizada
                 */
                $precificar  += $request->find("status_id = 15 and store_id in ($stores) and substr(updated_at,1,10) = '$date'")->count();
                $precificar2 = $request->find("status_id = 15 and store_id in ($stores) and substr(updated_at,1,10) = '$date'")->fetch(true);
                foreach ($precificar2 as $precificar3){
                    $precificarA += $precificar3->getAmountItems;
                }
                /**
                 * Incluida e Finalizada
                 */
                
                $stores2 = (new NfReceipt2())->find("substr(created_at,1,10) = '$date' and store_id in ($stores)")->group('substr(access_key,7,14)')->order('substr(access_key,7,14)')->fetch(true);

                foreach($stores2 as $loja) {
                    $storeIncluded["$loja->getFornecedor"] += (new NfReceipt2())->find("store_id in ($stores) and substr(created_at,1,10) = '$date' and substr(access_key,7,14) = '$loja->getCnpjFornecedor'")->count();

                    $storeIncludedAmounts = (new NfReceipt2())->find("store_id in ($stores) and substr(created_at,1,10) = '$date' and substr(access_key,7,14) = '$loja->getCnpjFornecedor'")->fetch(true);
                    foreach ($storeIncludedAmounts as $storeIncludedAmount) {              
                        $storeIncludedA["$loja->getFornecedor"] += $storeIncludedAmount->getAmountItems;
                    } 
                    // 7 a 20
                    $storeCompleted["$loja->getFornecedor"] += (new NfReceipt2())->find("status_id = 14  and store_id in ($stores) and substr(created_at,1,10) = '$date' and substr(access_key,7,14) = '$loja->getCnpjFornecedor'")->count();
                    $storeCompletedAmounts = (new NfReceipt2())->find("status_id = 14 and  store_id in ($stores) and substr(created_at,1,10) = '$date and substr(access_key,7,14) = '$loja->getCnpjFornecedor'")->fetch(true);
                    foreach ($storeCompletedAmounts as $storeCompletedAmount) {              
                        $storeCompletedA["$loja->getFornecedor"] += $storeCompletedAmount->getAmountItems;
                    }
                }
                // die;

                /**
                 * duração no status
                 */
                $nfEvent = new NfEvent();
                echo $this->view->render(
                    "client/home2",
                    ["title" => "Home | " . SITE,
                     "request" => $request,
                     "cargoArrival" => $cargoArrival,
                     "cargoArrivalA" => $cargoArrivalA,
                     "nfeReceiving" => $nfeReceiving,
                     "nfeReceivingA" >$nfeReceivingA,
                     "commercialPending" => $commercialPending,
                     "commercialPendingA" => $commercialPendingA,
                     "conference" => $conference,
                     "conferenceA" => $conferenceA,
                     "nfeUpdate" => $nfeUpdate,
                     "nfeUpdateA" => $nfeUpdateA,
                     "finishedCargo" => $finishedCargo,
                     "finishedCargoA" => $finishedCargoA,
                     "nfEvent" => $nfEvent,
                     "storeCompleted" => $storeCompleted,
                     "storeCompletedA" => $storeCompletedA,
                     "storeIncluded" => $storeIncluded,
                     "storeIncludedA" => $storeIncludedA,
                     "stores" => $stores,
                     "precificar" => $precificar,
                     "precificarA" => $precificarA
                     ]
                );
            } else if ($class == 'A') {
                $requests = (new NfReceipt())->find("operator_id = :operator_id","operator_id=$usuario->id")->fetch(true);
                foreach ($requests as $request) {
                    //quantidade de notas e quantidade de itens dia
                    if (substr($request->created_at,0,10) == date("Y-m-d")) {
                        $qtdAtualDiaria += 1;
                        $qtdAtualDiariaItens += $request->items_quantity;
                    }
                    if (substr($request->updated_at,0,10) == date("Y-m-d") && ($request->status_id == 3 or $request->status_id == 14) ) {
                        $qtdAtualizadaAtualDiaria += 1;
                        $qtdAtualizadaAtualDiariaItens += $request->items_quantity;
                    }

                    switch ($request->status_id) {
                        case 1:
                            $qtdPendenteEmpresaAtualDiaria +=1;
                            $qtdPendenteEmpresaAtualDiariaItens += $request->items_quantity;
                            break;
                        
                        case 2:
                            $qtdPendenteClienteAtualDiaria +=1;
                            $qtdPendenteClienteAtualDiariaItens += $request->items_quantity;
                            break;
                        case 5:
                            $qtdAguardandoLiberacaoAtualDiaria +=1;
                            $qtdAguardandoLiberacaoAtualDiariaItens += $request->items_quantity;
                            break;
                        case 6:
                            $qtdPendenciaIcmsAtualDiaria +=1;
                            $qtdPendenciaIcmsAtualDiariaItens += $request->items_quantity;
                            break;
                        case 9:
                            $qtdCadastroDiaria +=1;
                            $qtdCadastroDiariaItens += $request->items_quantity;
                            break;
                        case 9:
                            $qtdChegadaCargaDiaria +=1;
                            $qtdChegadaCargaDiariaItens += $request->items_quantity;
                            break;
                        case 13:
                            $qtdNFEAAtualizarDiaria +=1;
                            $qtdNFEAAtualizarDiariaItens += $request->items_quantity;
                            break;
                    }
                }
                                    /**
                     * reporte diario Operador
                     */
                    $todayNfReport = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->fetch(true);
                    $todayNfReportCount = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->count();
    
                    if ($todayNfReportCount>0){
                        $todayNfReportCount = 0;
                        foreach ($todayNfReport as $tnr) {
                            if( $tnr->getOperator()->id == $usuario->id) {
                                $todayNfReportCount++;
                                $todayNfReport1[$tnr->id_request] = $tnr->id_request;
                            }                           
                        }
                        $todayNfReport = implode(",",$todayNfReport1);                    
                    } else {
                        $todayNfReport = '-1';
                    }
    
                    /**
                     * reporte diario Cliente
                     */
                    $clientTodayNfReport = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('C').")")->fetch(true);
                    $clientTodayNfReportCount = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('C').")")->count();
                    if ($clientTodayNfReportCount>0){
                        $clientTodayNfReportCount = 0;
                        foreach ($clientTodayNfReport as $tnr) {
                            if( $tnr->getOperator()->id == $usuario->id) {
                                $clientTodayNfReportCount++;
                                $todayNfReport2[$tnr->id_request] = $tnr->id_request;
                            }                            
                        }
                        $clientTodayNfReport = implode(",",$todayNfReport2);                    
                    } else {
                        $clientTodayNfReport = '-1';
                    }
                    /**
                     * reporte diario indevido
                     */
                    $todayNfReportIndevido = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->fetch(true);
                    $todayNfReportCountIndevido = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->count();
    
                    if ($todayNfReportCountIndevido>0){
                        $todayNfReportCountIndevido = 0;
                        foreach ($todayNfReport as $tnr) {
                            if( $tnr->getOperator()->id == $usuario->id) {
                                $todayNfReportIndevido1[$tnr->id_request] = $tnr->id_request;
                                $todayNfReportCountIndevido++;
                            }
                            
                        }
                        if (is_array($todayNfReportIndevido1)) {
                            
                            $todayNfReportIndevido = implode(",",$todayNfReportIndevido1); 
                        }
                                           
                    } else {
                        $todayNfReportIndevido = '-1';
                    }

                    $MonthlyNfReportIndevido = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id = 0")->fetch(true);
                    $MonthlyNfReportCountIndevido = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id = 0")->count();

                    if ($MonthlyNfReportCountIndevido>0){
                        $MonthlyNfReportCountIndevido = 0;
                        foreach ($MonthlyNfReportIndevido as $tnr) {
                            if( $tnr->getOperator()->id == $usuario->id) {
                                $MonthlyNfReportCountIndevido++;
                                $todayNfReport3Indevido[$tnr->id_request] = $tnr->id_request;
                            }
                            
                        }
                        $MonthlyNfReportIndevido = implode(",",$todayNfReport3Indevido);                    
                    } else {
                        $MonthlyNfReportIndevido = '-1';
                    }
                    /**
                     * reporte diario Operador
                     */
                    $MonthlyNfReport = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('A').")")->fetch(true);
                    $MonthlyNfReportCount = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('A').")")->count();

                    if ($MonthlyNfReportCount>0){
                        $MonthlyNfReportCount = 0;
                        foreach ($MonthlyNfReport as $tnr) {
                            if( $tnr->getOperator()->id == $usuario->id) {
                                $MonthlyNfReportCount ++;
                                $todayNfReport3[$tnr->id_request] = $tnr->id_request;
                            }
                            
                        }
                        $MonthlyNfReport = implode(",",$todayNfReport3);                    
                    } else {
                        $MonthlyNfReport = '-1';
                    }
    
                    /**
                     * reporte diario Cliente
                     */
                    $clientMonthlyNfReport = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('C').")")->fetch(true);
                    $clientMonthlyNfReportCount = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('C').")")->count();
                    
                    if ($clientMonthlyNfReport>0){
                        foreach ($clientMonthlyNfReport as $tnr) {
                            $clientMonthlyNfReportCount = 0;
                            if( $tnr->getOperator()->id == $usuario->id) {
                                $clientMonthlyNfReportCount++;
                                $todayNfReport4[$tnr->id_request] = $tnr->id_request;
                            }
                        }
                        $clientMonthlyNfReport = implode(",",$todayNfReport4);                    
                    } else {
                        $clientMonthlyNfReport = '-1';
                    }
                echo $this->view->render(
                    "administrator/homeOld",
                    ["title" => "Home | " . SITE,
                    "usuario" => $usuario->name,
                    "clientMonthlyNfReport" => $clientMonthlyNfReport,
                    "clientMonthlyNfReportCount" => $clientMonthlyNfReportCount,
                    "MonthlyNfReport" => $MonthlyNfReport,
                    "MonthlyNfReportCount" => $MonthlyNfReportCount,
                    "clientTodayNfReport" => $clientTodayNfReport,
                    "clientTodayNfReportCount" => $clientTodayNfReportCount,
                    "todayNfReport" => $todayNfReport,
                    "todayNfReportCount" => $todayNfReportCount,
                    "MonthlyNfReportIndevido" => $MonthlyNfReportIndevido,
                    "MonthlyNfReportCountIndevido" => $MonthlyNfReportCountIndevido,
                    "todayNfReportIndevido" => $todayNfReportIndevido,
                    "todayNfReportCountIndevido" => $todayNfReportCountIndevido,
                    "qtdAtualDiaria" => $qtdAtualDiaria,
                    "qtdAtualDiariaItens" => $qtdAtualDiariaItens,
                    "qtdAtualizadaAtualDiaria" => $qtdAtualizadaAtualDiaria,
                    "qtdAtualizadaAtualDiariaItens" => $qtdAtualizadaAtualDiariaItens,
                    "qtdPendenteEmpresaAtualDiaria" => $qtdPendenteEmpresaAtualDiaria,
                    "qtdPendenteEmpresaAtualDiariaItens" => $qtdPendenteEmpresaAtualDiariaItens,
                    "qtdPendenteClienteAtualDiaria" => $qtdPendenteClienteAtualDiaria,
                    "qtdPendenteClienteAtualDiariaItens" => $qtdPendenteClienteAtualDiariaItens,
                    "qtdAguardandoLiberacaoAtualDiaria" => $qtdAguardandoLiberacaoAtualDiaria,
                    "qtdAguardandoLiberacaoAtualDiariaItens" => $qtdAguardandoLiberacaoAtualDiariaItens,
                    "qtdPendenciaIcmsAtualDiaria" => $qtdPendenciaIcmsAtualDiaria,
                    "qtdPendenciaIcmsAtualDiariaItens" => $qtdPendenciaIcmsAtualDiariaItens,
                    "qtdCadastroDiaria" => $qtdCadastroDiaria,
                    "qtdCadastroDiariaItens" => $qtdCadastroDiariaItens,
                    "qtdChegadaCargaDiaria" => $qtdChegadaCargaDiaria,
                    "qtdChegadaCargaDiariaItens" => $qtdChegadaCargaDiariaItens,
                    "qtdNFEAAtualizarDiaria" => $qtdNFEAAtualizarDiaria,
                    "qtdNFEAAtualizarDiariaItens" => $qtdNFEAAtualizarDiariaItens

                    ]
                );
            }
            
        }
    }   

    public function home2($data) {
        if (!$data) {
            $date = date('Y-m-d');
            $month = date('Y-m');
            $request = (new NfReceipt());
            $requests = $request->find("substr(created_at,1,10) = :hoje or substr(updated_at,1,10) = :hoje or (status_id != :concluido and status_id != :carga_finalizada)","hoje=$date&concluido=3&carga_finalizada=14")->fetch(true);
            
            foreach ($requests as $request) {
                $operators2[$request->getOperator] = $request->getOperator;
                if (substr($request->created_at,0,10) == $date) {
                    $included += 1;
                    $includedA += $request->getAmountItems;
                }

                if ($request->status_id == 1) {
                    $pendingCompany += 1;
                    $pendingCompanyA += $request->getAmountItems;
                }

                if ($request->status_id == 18) {
                    $pendingFiscal += 1;
                    $pendingFiscalA += $request->getAmountItems;
                }

                if($request->status_id == 2) {
                    $pendingClient += 1;
                    $pendingClientA += $request->getAmountItems;
                    if ($request->operation_id == 103) {
                        $pendingClientF += 1;
                    }

                    
                }
                
                if($request->status_id == 3) {
                    $completed += 1;
                    $completedA += $request->getAmountItems;
                }
                
                if($request->status_id == 4) {
                    $pendingInformation += 1;
                    $pendingInformationA += $request->getAmountItems;
                }
                
                if($request->status_id == 5) {
                    $waitingRelease += 1;
                    $waitingReleaseA += $request->getAmountItems;
                }
                
                if($request->status_id == 6) {
                    $ICMSPending += 1;
                    $ICMSPendingA += $request->getAmountItems;
                }

                if($request->status_id == 7) {
                    $canceled += 1;
                    $canceledA += $request->getAmountItems;
                }

                if($request->status_id == 8) {
                    $wrongRecipient += 1;
                    $wrongRecipientA += $request->getAmountItems;
                }
                
                if($request->status_id == 9) {
                    $register += 1;
                    $registerA += $request->getAmountItems;
                }

                if ($request->status_id == 17) {
                    $tarefa += 1;
                    $tarefaA += $request->getAmountItems;
                }
            }
            echo $this->view->render(
                "administrator/home",
                ["title" => "Home | " . SITE,
                "ICMSPending" => $ICMSPending,
                "ICMSPendingA" => $ICMSPendingA,
                "waitingRelease" => $waitingRelease,
                "waitingReleaseA" => $waitingReleaseA,
                "pendingInformation" => $pendingInformation,
                "pendingInformationA" => $pendingInformationA,
                "completed" => $completed,
                "completedA" => $completedA,
                "pendingClient" => $pendingClient,
                "pendingClientA" => $pendingClientA,
                "pendingClientF" => $pendingClientF,
                "pendingCompany" => $pendingCompany,
                "pendingCompanyA" => $pendingCompanyA,
                "pendingFiscal" => $pendingFiscal,
                "pendingFiscalA" => $pendingFiscalA,
                "included" => $included,
                "includedA" => $includedA,
                "register" => $register,
                "registerA" => $registerA,
                "tarefa" => $tarefa,
                "tarefaA" => $tarefaA             
            ]);
        } else if ($data['data'] == 1) {
            $date = date('Y-m-d');
            $month = date('Y-m');
            $request = (new NfReceipt());
            $requests = $request->find("substr(created_at,1,10) = :hoje or substr(updated_at,1,10) = :hoje or (status_id != :concluido and status_id != :carga_finalizada)","hoje=$date&concluido=3&carga_finalizada=14")->fetch(true);
            
            foreach ($requests as $request) {
                if($request->status_id == 10) {
                    $cargoArrival += 1;
                    $cargoArrivalA += $request->getAmountItems;
                }

                if($request->status_id == 11) {
                    $commercialPending += 1;
                    $commercialPendingA += $request->getAmountItems;
                }

                if($request->status_id == 12) {
                    $conference += 1;
                    $conferenceA += $request->getAmountItems;
                }

                if($request->status_id == 13) {
                    $nfeUpdate += 1;
                    $nfeUpdateA += $request->getAmountItems;
                }

                if($request->status_id == 14) {
                    $finishedCargo += 1;
                    $finishedCargoA += $request->getAmountItems;
                }

                if($request->status_id == 15) {
                    $price += 1;
                    $priceA += $request->getAmountItems;
                }
            }
            echo $this->view->render(
                "administrator/home2",
                ["title" => "Home | " . SITE,
                "price" => $price,
                "priceA" => $priceA,
                "finishedCargo" => $finishedCargo,
                "finishedCargoA" => $finishedCargoA,
                "nfeUpdate" => $nfeUpdate,
                "nfeUpdateA" => $nfeUpdateA,
                "conference" => $conference,
                "conferenceA" => $conferenceA,
                "commercialPending" => $commercialPending,
                "commercialPendingA" => $commercialPendingA,
                "pendingCompany" => $pendingCompany,
                "pendingCompanyA" => $pendingCompanyA,
                "cargoArrival" => $cargoArrival,
                "cargoArrivalA" => $cargoArrivalA                           
            ]);
        } else if ($data['data'] == 2) {
            $nfEvent = (new NfEvent());
            echo $this->view->render(
                "administrator/home3",
                ["title" => "Home | " . SITE,
                "nfEvent" => $nfEvent                                       
            ]);
        } else if ($data['data'] == 3) {
            $nfEvent = (new NfEvent());
            echo $this->view->render(
                "administrator/home4",
                ["title" => "Home | " . SITE,
                "nfEvent" => $nfEvent                                       
            ]);
        } else if ($data['data'] == 4) {
            $date = date('Y-m-d');
            $nfEvent = (new NfEvent());
            $nfEvents = $nfEvent->find("substr(updated_at,1,10) = :date or substr(created_at,1,10) = :date1","date=$date&date1=$date")->fetch(true);
            foreach ($nfEvents as $nE) {
                if ($nE->status_id == 2){
                    $pendingClientEvent += 1;
                    $pendenteClientf[$nE->id_nf_receipt] = $nE->id_nf_receipt;                    
                }                

                if ($nE->status_id == 11){
                    $pendingComercial += 1;
                    $pendenteInformacoesId[$nE->id_nf_receipt] = $nE->id_nf_receipt;                    
                }            

                if ($nE->status_id == 6){
                    $pendenteICMSId[$nE->id_nf_receipt] = $nE->id_nf_receipt;
                    $pendingICMSEvent += 1;
                    $ICMSOperators[$nE->getFinalUser] = $nE->getFinalUser;
                    $amountICMSOperator[$nE->getFinalUser] += 1;
                }              
                
                if ($nE->status_id == 9){
                    $cadastroIds[$nE->id_nf_receipt] = $nE->id_nf_receipt;
                    $registerEvent += 1;
                    $registerOperators[$nE->getFinalUser] = $nE->getFinalUser;
                    $amountRegisterOperator[$nE->getFinalUser] += 1;
                    $amountItemRegisterOperator[$nE->getFinalUser] += $nE->getAmountItems;
                }           
                
                if ($nE->status_id == 4){
                    $pendingInformation += 1;
                    $pendenteInformacoesId[$nE->id_nf_receipt] = $nE->id_nf_receipt;   
                }
            }
            if (is_array($pendenteInformacoesId)) {
                $pendenteInformacoesId = implode(",",$pendenteInformacoesId);
            }
            if (is_array($cadastroIds)) {
                $cadastroIds = implode(",",$cadastroIds);
            }
            if (is_array($pendenteICMSId)) {
                $pendenteICMSId = implode(",",$pendenteICMSId);
            }
            if (is_array($pendenteInformacoesId)) {
                $pendenteInformacoesId = implode(",",$pendenteInformacoesId);
            }
            if (is_array($pendenteClientf)) {
                $pendenteClientf = implode(",",$pendenteClientf);
            }
            echo $this->view->render(
                "administrator/home5",
                ["title" => "Home | " . SITE,
                "pendenteInformacoesId" => $pendenteInformacoesId,
                "cadastroIds" => $cadastroIds,
                "pendenteICMSId" => $pendenteICMSId,
                "pendenteInformacoesId" => $pendenteInformacoesId,
                "pendenteClientf" => $pendenteClientf,
                "pendingClientEvent" => $pendingClientEvent,
                "pendingComercial" => $pendingComercial,
                "pendingICMSEvent" => $pendingICMSEvent,
                "amountICMSOperator" => $amountICMSOperator,
                "ICMSOperators" => $ICMSOperators,
                "registerEvent" => $registerEvent,
                "registerOperators" => $registerOperators,
                "amountRegisterOperator" => $amountRegisterOperator,
                "amountItemRegisterOperator" => $amountItemRegisterOperator,
                "pendingInformation" => $pendingInformation
            ]);
        } else if ($data['data'] == 5) {
            $today = date("Y-m-d");
            $month = date("Y-m");
            $user = new User();
            /**
            * reporte diario Operador
            */
            $todayNfReport = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->fetch(true);
            $todayNfReportCount = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('A').")")->count();
            if ($todayNfReportCount>0){
                foreach ($todayNfReport as $tnr) {
                    $todayNfReport1[$tnr->id_request] = $tnr->id_request;
                }
                $todayNfReport = implode(",",$todayNfReport1);                    
            } else {
                $todayNfReport = '-1';
            }

            /**
            * reporte diario Cliente
            */
            $clientTodayNfReport = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('C').")")->fetch(true);
            $clientTodayNfReportCount = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id in (".$user->getUsersForClass('C').")")->count();
            if ($clientTodayNfReportCount>0){
                foreach ($clientTodayNfReport as $tnr) {
                    $todayNfReport2[$tnr->id_request] = $tnr->id_request;
                }
                $clientTodayNfReport = implode(",",$todayNfReport2);                    
            } else {
                $clientTodayNfReport = '-1';
            }
            /**
             * reporte diario indevido
             */
            $todayNfReportIndevido = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id = 0")->fetch(true);
            $todayNfReportCountIndevido = (new NfReport())->find("substr(created_at,1,10) = '$today' and user_id = 0")->count();
            if ($todayNfReportCountIndevido>0){
                foreach ($todayNfReport as $tnr) {
                    $todayNfReportIndevido1[$tnr->id_request] = $tnr->id_request;
                }
                if (is_array($todayNfReportIndevido1)) {
                    $todayNfReportIndevido = implode(",",$todayNfReportIndevido1); 
                }
                                    
            } else {
                $todayNfReportIndevido = '-1';
            }
            $MonthlyNfReportIndevido = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id = 0")->fetch(true);
            $MonthlyNfReportCountIndevido = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id = 0")->count();
            if ($MonthlyNfReportCountIndevido>0){
                foreach ($MonthlyNfReportIndevido as $tnr) {
                    $todayNfReport3Indevido[$tnr->id_request] = $tnr->id_request;
                }
                $MonthlyNfReportIndevido = implode(",",$todayNfReport3Indevido);                    
            } else {
                $MonthlyNfReportIndevido = '-1';
            }
            /**
             * reporte diario Operador
             */
            $MonthlyNfReport = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('A').")")->fetch(true);
            $MonthlyNfReportCount = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('A').")")->count();
            if ($MonthlyNfReportCount>0){
                foreach ($MonthlyNfReport as $tnr) {
                    $todayNfReport3[$tnr->id_request] = $tnr->id_request;
                }
                $MonthlyNfReport = implode(",",$todayNfReport3);                    
            } else {
                $MonthlyNfReport = '-1';
            }
            /**
             * reporte diario Cliente
             */
            $clientMonthlyNfReport = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('C').")")->fetch(true);
            $clientMonthlyNfReportCount = (new NfReport())->find("substr(created_at,1,7) = '$month' and user_id in (".$user->getUsersForClass('C').")")->count();
             
            if ($clientMonthlyNfReport>0){
                foreach ($clientMonthlyNfReport as $tnr) {
                    $todayNfReport4[$tnr->id_request] = $tnr->id_request;
                }
                $clientMonthlyNfReport = implode(",",$todayNfReport4);                    
            } else {
                $clientMonthlyNfReport = '-1';
            }
            echo $this->view->render(
                "administrator/home6",
                ["title" => "Home | " . SITE,
                "todayNfReportCount" => $todayNfReportCount,
                "todayNfReport" => $todayNfReport,
                "MonthlyNfReportCount" => $MonthlyNfReportCount,
                "MonthlyNfReport" => $MonthlyNfReport,
                "clientTodayNfReportCount" => $clientTodayNfReportCount,
                "clientTodayNfReport" => $clientTodayNfReport,
                "clientMonthlyNfReportCount" => $clientMonthlyNfReportCount,
                "clientMonthlyNfReport" => $clientMonthlyNfReport,
                "todayNfReportCountIndevido" => $todayNfReportCountIndevido,
                "todayNfReportIndevido" => $todayNfReportIndevido,
                "MonthlyNfReportCountIndevido" => $MonthlyNfReportCountIndevido,
                "MonthlyNfReportIndevido" => $MonthlyNfReportIndevido

            ]);
        } else if ($data['data'] == 6) {
            $operations = (new NfOperation())->find()->fetch(true);
            $date = date('Y-m-d');
            $month = date('Y-m');
            $request = (new NfReceipt());
            $requests = $request->find("substr(created_at,1,10) = :hoje or substr(updated_at,1,10) = :hoje or (status_id != :concluido and status_id != :carga_finalizada)","hoje=$date&concluido=3&carga_finalizada=14")->fetch(true);
            
            foreach ($requests as $request) {
                if ($request->status_id == 1) {
                    $requestOperationPendenteCompany[$request->operation_id] += 1;
                    $requestOperationAmountPendenteCompany[$request->operation_id] += $request->getAmountItems;
                }
            }
            echo $this->view->render(
                "administrator/home7",
                ["title" => "Home | " . SITE,
                "operations" => $operations,
                "requestOperationPendenteCompany" => $requestOperationPendenteCompany,
                "requestOperationAmountPendenteCompany" => $requestOperationAmountPendenteCompany
            ]);
        } else if ($data['data'] == 7) {
            $request = (new NfReceipt());
            echo $this->view->render(
                "administrator/home8",
                ["title" => "Home | " . SITE,
                "request" => $request                
            ]);
        } else if ($data['data'] == 8) {
            $operations = (new NfOperation())->find()->fetch(true);
            $date = date('Y-m-d');
            $month = date('Y-m');
            $request = (new NfReceipt());
            $requests = $request->find("substr(created_at,1,10) = :hoje or substr(updated_at,1,10) = :hoje or (status_id != :concluido and status_id != :carga_finalizada)","hoje=$date&concluido=3&carga_finalizada=14")->fetch(true);
            
            foreach ($requests as $request) {
                if($request->status_id == 3) {
                    $requestOperationComleted[$request->operation_id] += 1;
                    $requestOperationAmountCompleted[$request->operation_id] += $request->getAmountItems;
                }
            }
            echo $this->view->render(
                "administrator/home9",
                ["title" => "Home | " . SITE,
                "operations" => $operations,
                "requestOperationComleted" => $requestOperationComleted,
                "requestOperationAmountCompleted" => $requestOperationAmountCompleted         
            ]);
        } else if ($data['data'] == 9) {
            $date = date('Y-m-d');
            $horas = (new NfReceipt())->find("substr(created_at,1,10) = :date and status_id = :status_id","date=$date&status_id=3")->order("updated_at")->fetch(true);
            foreach($horas as $hora) {
                $operators[$hora->getOperator] = $hora->getOperator;
            }
            $request = (new NfReceipt());
            $requests = $request->find("substr(updated_at,1,10) = :hoje and (status_id = :concluido or status_id = :carga)","hoje=$date&concluido=3&carga=14")->fetch(true);
            foreach ($requests as $request) {
                $requestOperatorCompleted[$request->getOperator] += 1;
                $requestOperatorAmountCompleted[$request->getOperator] += $request->getAmountItems;
            }
            echo $this->view->render(
                "administrator/home10",
                ["title" => "Home | " . SITE,
                "operators" => $operators,
                "requestOperatorCompleted" => $requestOperatorCompleted,
                "requestOperatorAmountCompleted" => $requestOperatorAmountCompleted         
            ]);
        } else if ($data['data'] == 10) {
            $date = date('Y-m-d');
            $horas = (new NfReceipt())->find("status_id = :status_id","status_id=2")->order("updated_at")->fetch(true);
            foreach($horas as $hora) {
                $operators[$hora->getOperator] = $hora->getOperator;
            }
            $request = (new NfReceipt());
            $requests = $request->find("substr(created_at,1,10) = :hoje or substr(updated_at,1,10) = :hoje or (status_id != :concluido and status_id != :carga_finalizada)","hoje=$date&concluido=3&carga_finalizada=14")->fetch(true);
            foreach ($requests as $request) {
                if($request->status_id == 2) {
                    $requestOperatorPendingClient[$request->getOperator] += 1;
                    $requestOperatorAmountPendingClient[$request->getOperator] += $request->getAmountItems;
    
                }
            }
            echo $this->view->render(
                "administrator/home11",
                ["title" => "Home | " . SITE,
                "operators" => $operators,
                "requestOperatorPendingClient" => $requestOperatorPendingClient,
                "requestOperatorAmountPendingClient" => $requestOperatorAmountPendingClient         
            ]);
           
        } else if ($data['data'] == 11) {
            $date = date('Y-m-d');
            $horas = (new NfReceipt())->find("substr(updated_at,1,10) = :hoje and status_id = :concluido","hoje=$date&concluido=3")->fetch(true);
            foreach($horas as $hora) {
                $operators[$hora->getOperator] = $hora->getOperator;
            }
            $request = new NfReceipt();
            $requests = $request->find("substr(updated_at,1,10) = :hoje and status_id = :concluido","hoje=$date&concluido=3")->fetch(true);
            foreach ($requests as $request) {
                if ($request->type_id != 4) {
                    $requestOperatorCompletedSCd[$request->getOperator] += 1;
                    $requestOperatorAmountCompletedSCd[$request->getOperator] += $request->getAmountItems;
                }
            }
            echo $this->view->render(
                "administrator/home12",
                ["title" => "Home | " . SITE,
                "operators" => $operators,
                "requestOperatorCompletedSCd" => $requestOperatorCompletedSCd,
                "requestOperatorAmountCompletedSCd" => $requestOperatorAmountCompletedSCd         
            ]);
        } else if ($data['data'] == 12) {
            $date = date('Y-m-d');
            $month = date('Y-m');
            $request = (new NfReceipt());
            $requests = $request->find("substr(created_at,1,10) = :hoje or substr(updated_at,1,10) = :hoje or (status_id != :concluido and status_id != :carga_finalizada)","hoje=$date&concluido=3&carga_finalizada=14")->order('store_id')->fetch(true);            
            $stores = (new Store())->find()->order('code')->fetch(true);
            foreach ($requests as $request) {
                if (substr($request->created_at,0,10) == $date) {
                    $requestStoreIncluded[$request->store_id] += 1;
                    $requestStoreIncludedA[$request->store_id] += $request->getAmountItems;
                }
                if($request->status_id == 3 || $request->status_id == 14) {
                    $requestStoreCompleted[$request->store_id] += 1;
                    $requestStoreCompletedA[$request->store_id] += $request->getAmountItems;
                }
                if($request->status_id == 1 || $request->status_id == 2 || $request->status_id == 5 || $request->status_id == 6 || $request->status_id ==  9 || $request->status_id == 10 || $request->status_id == 13) {
                    $requestStorePendente[$request->store_id] += 1;
                    $requestStorePendenteA[$request->store_id] += $request->getAmountItems;
                }
            }
            
            echo $this->view->render(
                "administrator/home13",
                ["title" => "Home | " . SITE,
                "stores" => $stores,
                "requestStoreIncluded" => $requestStoreIncluded,
                "requestStoreIncludedA" => $requestStoreIncludedA,
                "requestStoreCompleted" => $requestStoreCompleted,
                "requestStoreCompletedA" => $requestStoreCompletedA,
                "requestStorePendente" => $requestStorePendente,
                "requestStorePendenteA" => $requestStorePendenteA

            ]);
        } else if ($data['data'] == 13) {
            $date = date('Y-m-d');
            $horas = (new NfReceipt())->find("substr(updated_at,1,10) = :date and status_id = :status_id","date=$date&status_id=3")->order("updated_at")->fetch(true);

            
            foreach($horas as $hora) {
                $completedHour[substr($hora->updated_at,11,2)] = substr($hora->updated_at,11,2);
                $operators[$hora->getOperator] = $hora->getOperator;
            }
            
            if (is_array($completedHour)){
                $hour = "'".implode("', '",$completedHour)."'";
            }            
            if ($operators) {
                foreach ($operators as $operator) {
                    if (is_array($completedHour)){
                        foreach (array_unique($completedHour) as $i) {
                            $user = (new User())->find("name = :name","name=$operator")->fetch()->id;
                            $i2 = str_pad($i,2,'0',STR_PAD_LEFT);
                            $intera = (new NfReceipt())->find("substr(updated_at,1,10) = '$date' and substr(updated_at,12,2) = '$i2' and operator_id = $user")->count();
                            $operatorCount[$operator] = $operatorCount[$operator].",".$intera;
                        }
                    }
                    $l[$operator] = $operator;
                    $c[$operator] = sprintf('#%06X', mt_rand(0, 0XFFFFFF));
                }
            }
            echo $this->view->render(
                "administrator/home14",
                ["title" => "Home | " . SITE,
                "stores" => $stores,
                "l" => $l,
                "c" => $c,
                "operators" => $operators,
                "operatorCounts" => $operatorCount,
                "hour" => $hour

            ]);
        } else if ($data['data'] == 14) {
            $date = date('Y-m-d');
            $horas = (new NfReceipt())->find("(substr(created_at,1,10) = :date1 or substr(updated_at,1,10) = :date2)","date2=$date&date1=$date")->order("substr(created_at,11,2)")->fetch(true);
            foreach ($horas as $hora){
                $i++;
                if(substr($hora->created_at,0,10) == $date) {
                    $times[substr($hora->created_at,11,2)] = substr($hora->created_at,11,2);
                    $includedHour[substr($hora->created_at,11,2)] += 1;
                }
                
                if ($hora->status_id == 3) {
                    $refreshHour[substr($hora->updated_at,11,2)] += 1;
                } else {
                    $refreshHour[substr($hora->updated_at,11,2)] += 0;
                }
            }  
            if (is_array($times)){
                ksort($times);
                $time = "'".implode("','",$times)."'";
                
            }
            if (is_array($includedHour)){
                ksort($includedHour);
                $includedHour = "'".implode("','",$includedHour)."'";
            }
            if (is_array($refreshHour)){
                ksort($refreshHour);
                $refreshHour = "'".implode("','",$refreshHour)."'";
            }
            echo $this->view->render(
                "administrator/home15",
                ["title" => "Home | " . SITE,
                "includedHour" => $includedHour,
                "completedHour" => $refreshHour,
                "horas" => $time
            ]);
        } else if ($data['data'] == 15) {
            $requests = new NfReceipt();
            echo $this->view->render(
                "administrator/home16",
                ["title" => "Home | " . SITE,
                "request" => $requests

            ]);
        } else if ($data['data'] == 16) {
            $requests = new NfReceipt();
            echo $this->view->render(
                "administrator/home17",
                ["title" => "Home | " . SITE,
                "request" => $requests

            ]);
        } else if ($data['data'] == 17) {
            $requests = new NfReceipt();
            echo $this->view->render(
                "administrator/home18",
                ["title" => "Home | " . SITE,
                "request" => $requests

            ]);
        } else if ($data['data'] == 18) {
            $requests = new NfReceipt();
            echo $this->view->render(
                "administrator/home19",
                ["title" => "Home | " . SITE,
                "request" => $requests

            ]);
        } else if ($data['data'] == 19) {
            $requests = new NfReceipt();
            echo $this->view->render(
                "administrator/home20",
                ["title" => "Home | " . SITE,
                "request" => $requests

            ]);
        } else if ($data['data'] == 20) {
            $date = date('Y-m-d');
            $nfEvent = (new NfEvent());
            $nfEvents = $nfEvent->find("substr(updated_at,1,10) = :date and status_id = :status_id and situation = :situation","status_id=10&situation=F&date=$date")->fetch(true);
            foreach ($nfEvents as $nE) {
                if ($nE->final_user_id != 0 and $nE->getFinalUserClass == 'A'){
                    $chegadaCarga[$nE->getFinalUser] += 1;
                    $operators[$nE->getFinalUser] = $nE->getFinalUser;
                    $ids[$nE->getFinalUser] .= $nE->id_nf_receipt.",";
                }
            }

            echo $this->view->render(
                "administrator/home21",
                ["title" => "Home | " . SITE,
                "operators" => $operators,
                "chegadaCarga" => $chegadaCarga,
                "ids" => $ids        
            ]);
        } else if ($data['data'] == 21) {
            $requests = (new NfReceipt())->find(" status_id in (1,4,5,6) and operation_id in (1,11,12)","","TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP) tempo, count(*) quantidade")->group("TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP)")->fetch(true);

            
            foreach ($requests as $request) {
                if ($request->tempo >= 1 && $request->tempo < 2) {
                    $um += $request->quantidade;
                } else if ($request->tempo >= 2 && $request->tempo < 4) {
                    $dois += $request->quantidade;
                } else if ($request->tempo >= 4 && $request->tempo < 6) {
                    $quatro += $request->quantidade;
                } else if ($request->tempo >= 6 && $request->tempo < 8) {
                    $seis += $request->quantidade;
                } else if ($request->tempo >= 8 && $request->tempo < 24) {
                    $oito += $request->quantidade;
                } else if ($request->tempo >= 24) {
                    $vinteQuatro += $request->quantidade;
                }
            }
            
            echo $this->view->render(
                "administrator/home22",
                ["title" => "Home | " . SITE,
                "um" => $um,
                "dois" => $dois,
                "quatro" => $quatro,
                "seis" => $seis,
                "oito" => $oito,
                "vinteQuatro" => $vinteQuatro

            ]);
        } else if ($data['data'] == 22) {
            $nfReceipts = (new NfReceipt())->find("status_id in (1,5,6,9,17,18)","","*, TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) duracao1,TIMESTAMPDIFF(HOUR,updated_at,CURRENT_TIMESTAMP()) duracao ")->fetch(true);
            foreach($nfReceipts as $nfReceipt) {
                $rows[$nfReceipt->operator_id]['operator'] = $nfReceipt->getOperator;
                switch ($nfReceipt->status_id) {
                    case '1':
                        $rows[$nfReceipt->operator_id]['1']++;
                        $rows[$nfReceipt->operator_id]['duracao_1_1'] += $nfReceipt->duracao;
                        $rows[$nfReceipt->operator_id]['duracao_1_2'] += $nfReceipt->duracao1;
                        break;
                    case '5':
                        $rows[$nfReceipt->operator_id]['5']++;
                        $rows[$nfReceipt->operator_id]['duracao_5_1'] += $nfReceipt->duracao;
                        $rows[$nfReceipt->operator_id]['duracao_5_2'] += $nfReceipt->duracao1;
                        break;
                    case '6':
                        $rows[$nfReceipt->operator_id]['6']++;
                        $rows[$nfReceipt->operator_id]['duracao_6_1'] += $nfReceipt->duracao;
                        $rows[$nfReceipt->operator_id]['duracao_6_2'] += $nfReceipt->duracao1;

                    case '9':
                        $rows[$nfReceipt->operator_id]['9']++;
                        $rows[$nfReceipt->operator_id]['duracao_9_1'] += $nfReceipt->duracao;
                        $rows[$nfReceipt->operator_id]['duracao_9_2'] += $nfReceipt->duracao1;
                        break;
                    case '17':
                        $rows[$nfReceipt->operator_id]['17']++;
                        $rows[$nfReceipt->operator_id]['duracao_17_1'] += $nfReceipt->duracao;
                        $rows[$nfReceipt->operator_id]['duracao_17_2'] += $nfReceipt->duracao1;
                        break;
                    case '18':
                        $rows[$nfReceipt->operator_id]['18']++;
                        $rows[$nfReceipt->operator_id]['duracao_18_1'] += $nfReceipt->duracao;
                        $rows[$nfReceipt->operator_id]['duracao_18_2'] += $nfReceipt->duracao1;
                        break;
                }
                
            }
            echo $this->view->render(
                "administrator/home23",
                ["title" => "Home | " . SITE,
                "rows" => $rows
            ]);
        } else if ($data['data'] == 23) {
            $nfReceipts = (new NfReceipt())->find("operator_id = 147 and status_id in (1) and TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) >= 1 and TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) < 2","","*, TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) duracao")->fetch(true);
            foreach($nfReceipts as $request) {
                $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                $content[$request->id]['error_cost'] = $request->error_cost;
                $content[$request->id]['id'] = $request->id;
                $content[$request->id]['store_id'] = $request->store_id;
                $content[$request->id]['nf_number'] = $request->nf_number;
                $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                $content[$request->id]['getClient'] = $request->getClient;
                $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                $content[$request->id]['getType'] = $request->getType;
                $content[$request->id]['clientGroup'] = $request->clientGroup;
                $content[$request->id]['items_quantity'] = $request->items_quantity;
                $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                $content[$request->id]['getOperator'] = $request->getOperator;
                $content[$request->id]['operator'] = $request->operator_id;
                $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                $content[$request->id]['getOperation'] = $request->getOperation;
                $content[$request->id]['operation'] = $request->operation_id;
                $content[$request->id]['status_id'] = $request->status_id;
                $content[$request->id]['getStatus'] = $request->getStatus;
                $content[$request->id]['codigoInternoCD'] = $request->codigoInternoCD();
                // $content[$request->id]['error'] = $request->error;
            }
            $users = new User();
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);

            $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                "users" => $users,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
            ]);
        } else if ($data['data'] == 24) {
            $nfReceipts = (new NfReceipt())->find("operator_id = 147 and status_id in (1) and TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) >= 2 and TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) < 4","","*, TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) duracao")->fetch(true);
            foreach($nfReceipts as $request) {
                $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                $content[$request->id]['error_cost'] = $request->error_cost;
                $content[$request->id]['id'] = $request->id;
                $content[$request->id]['store_id'] = $request->store_id;
                $content[$request->id]['nf_number'] = $request->nf_number;
                $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                $content[$request->id]['getClient'] = $request->getClient;
                $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                $content[$request->id]['getType'] = $request->getType;
                $content[$request->id]['clientGroup'] = $request->clientGroup;
                $content[$request->id]['items_quantity'] = $request->items_quantity;
                $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                $content[$request->id]['getOperator'] = $request->getOperator;
                $content[$request->id]['operator'] = $request->operator_id;
                $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                $content[$request->id]['getOperation'] = $request->getOperation;
                $content[$request->id]['operation'] = $request->operation_id;
                $content[$request->id]['status_id'] = $request->status_id;
                $content[$request->id]['getStatus'] = $request->getStatus;
                $content[$request->id]['codigoInternoCD'] = $request->codigoInternoCD();
                // $content[$request->id]['error'] = $request->error;
            }
            $users = new User();
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);

            $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                "users" => $users,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
            ]);
        } else if ($data['data'] == 25) {
            $nfReceipts = (new NfReceipt())->find("operator_id = 147 and status_id in (1) and TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) >= 4","","*, TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) duracao")->fetch(true);
            foreach($nfReceipts as $request) {
                $content[$request->id]['inclusion_type'] = $request->inclusion_type;
                $content[$request->id]['error_cost'] = $request->error_cost;
                $content[$request->id]['id'] = $request->id;
                $content[$request->id]['store_id'] = $request->store_id;
                $content[$request->id]['nf_number'] = $request->nf_number;
                $content[$request->id]['getCreatedAt'] = $request->getCreatedAt;
                $content[$request->id]['getClient'] = $request->getClient;
                $content[$request->id]['getFornecedor'] = $request->getFornecedor;
                $content[$request->id]['getType'] = $request->getType;
                $content[$request->id]['clientGroup'] = $request->clientGroup;
                $content[$request->id]['items_quantity'] = $request->items_quantity;
                $content[$request->id]['getUpdatedAt'] = $request->getUpdatedAt;
                $content[$request->id]['getOperator'] = $request->getOperator;
                $content[$request->id]['operator'] = $request->operator_id;
                $content[$request->id]['getOperationColor'] = $request->getOperationColor;
                $content[$request->id]['getOperation'] = $request->getOperation;
                $content[$request->id]['operation'] = $request->operation_id;
                $content[$request->id]['status_id'] = $request->status_id;
                $content[$request->id]['getStatus'] = $request->getStatus;
                $content[$request->id]['codigoInternoCD'] = $request->codigoInternoCD();
                // $content[$request->id]['error'] = $request->error;
            }
            $users = new User();
            $status = new NfStatus();
            $status = $status->find()->fetch(true);
            $users = $users->find("class = :class or class = :class2","class=A&class2=C2")->fetch(true);
            $storeGroups = (new StoreGroup())->find()->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            $nfeOperation = (new NfOperation())->find()->fetch(true);

            $content = json_encode($content);
                echo $this->view->render("administrator/requests2", ["id" => "solicitação de lançamentos | " . SITE, "users" => $users,
                "requests" => $content,
                "users" => $users,
                "status" => $status,
                "storeGroups" => $storeGroups,
                "nfeType" => $nfeType,
                "nfeOperation" => $nfeOperation
            ]);
        } else if ($data['data'] == 26) {
            $nfReceipts = (new NfReceipt())->find("status_id in (3) AND substr(updated_at,1,10) = '".date('Y-m-d')."'","","*, TIMESTAMPDIFF(HOUR,created_at,CURRENT_TIMESTAMP()) duracao1,TIMESTAMPDIFF(HOUR,updated_at,CURRENT_TIMESTAMP()) duracao ")->fetch(true);

            foreach($nfReceipts as $nfReceipt) {
                $rows[$nfReceipt->operator_id]['operator'] = $nfReceipt->getOperator;
                $rows[$nfReceipt->operator_id]['id'] = $nfReceipt->operator_id;
                $rows[$nfReceipt->operator_id]['quantidade_'.substr($nfReceipt->updated_at,11,2)]++;
                $rows[$nfReceipt->operator_id]['itens_'.substr($nfReceipt->updated_at,11,2)] += $nfReceipt->items_quantity;
                
            }
            $user = new User();
            echo $this->view->render(
                "administrator/home26",
                ["title" => "Home | " . SITE,
                "rows" => $rows,
                "user" => $user
            ]);
        }
    }

    public function privacidade(){
        echo $this->view->render(
            "client/politicaPrivacidade",
            ["title" => "Home | " . SITE,]);
    }


}