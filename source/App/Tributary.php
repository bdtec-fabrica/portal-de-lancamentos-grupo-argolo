<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\ExchangeCode;
use Source\Models\ExchangePackaging;
use Source\Models\UserClass;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use SendGrid\Mail\Mail;
use SendGrid;
use Source\Models\ConectorConference;
use Source\Models\FiscalActivity;
use Source\Models\FiscalActivityType;
use Source\Models\FiscalPeriod;
use Source\Models\FiscalType;
use Source\Models\FiscalStores;
use Source\Models\FiscalInteraction;
use Source\Models\KwConference;
use League\Csv\Reader;
use League\Csv\Statement;
use Source\Models\NFE_CONTROLE;
use Source\Models\AA2CTIPO;
use Source\Models\NFE_CONTROLE2;
use Source\Models\AA2CTIPO2;

class Tributary 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function fiscalActivityRead($data) {

        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $class = $user->class;
        $fiscalPeriod = (new FiscalPeriod())->find()->fetch(true);
        $fiscalTypeActivity = (new FiscalActivityType())->find()->fetch(true);
        $fiscalStores = (new FiscalStores())->find()->fetch(true);
        if ($class == 'A') {

            if ($data['id']) {
                $user = new User();
                $date = $data['date'];
                $id = $data['id'];
                $stores =  (new FiscalStores())->find("id in ($id)")->fetch(true);
                $fiscalStores = (new FiscalStores())->find()->order("code")->fetch(true);
                $fiscalTypeActivity = (new FiscalActivityType())->find()->fetch(true);
                $users = $user->find("class = 'A'")->fetch(true);
                echo $this->view->render("administrator/fiscalActivity", ["title" => "Atividade Fiscal | " . SITE,
                "periods" => $fiscalPeriod,
                "users" => $users,
                "activityType" => $fiscalTypeActivity,
                "fiscalStores" => $fiscalStores,
                "stores" => $stores,
                "date" => $date]);
            } else {
                $user = new User();
                $date = date("mY");
                $stores = (new FiscalStores())->find()->order("code")->fetch(true);
                $fiscalStores = (new FiscalStores())->find()->order("code")->fetch(true);
                $activityType = (new FiscalActivityType())->find()->fetch(true);
                $users = $user->find("class = 'A'")->fetch(true);
                echo $this->view->render("administrator/fiscalActivity", ["title" => "Atividade Fiscal | " . SITE,
                "periods" => $fiscalPeriod,
                "users" => $users,
                "activityType" => $activityType,
                "fiscalStores" => $fiscalStores,
                "stores" => $stores,
                "date" => $date]);
            }
        }
    }
    
    public function fiscalActivityCreate() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $fiscalActivity = new FiscalActivity();
        $fiscalActivity->user_id = $usuario->id;
        $fiscalActivity->store_id = $_POST['store'];
        $fiscalActivity->period_id = $_POST['period'];
        $fiscalActivity->type_id = $_POST['type'];
        $fiscalActivity->activity_type_id  = $_POST['activityType'];
        $fiscalActivity->month  = $_POST['month'].$_POST['year'];
        $fiscalActivityID = $fiscalActivity->save();
        $data['success'] = true;
        $data['message'] = "Atividade registrada com sucesso!";
        echo json_encode($data);            
    }

    public function fiscalActivityDelete() {
        $id = $_POST['id'];
        $fiscalActivity = (new FiscalActivity())->findById($id);
        $fiscalActivity->destroy();
        $data['success'] = true;
        $data['message'] = "Atividade deletada com sucesso!";
        echo json_encode($data);

    }

    public function fiscalSearch() {
        $date = $_POST['mes']. $_POST['ano'];
        $store = $_POST['store'];
        
        if ($store !='') {
            $params['id'] = $store; 
            $comparation[1] = " id = :id";
        }
        if (is_array($comparation)){
            $comparation = implode(' and ', $comparation);
            $params = http_build_query($params);
        }
        
        $users = (new FiscalStores())->find($comparation,$params)->fetch(true);

 
        if ($users) {
            foreach ($users as $user) {
                if ($_POST['pendencia'] == 's') {
                    
                    if ($user->pendente($user->id, $date) === true) {
                        $id[$user->id] = $user->id;
                    }
                } else {
                    $id[$user->id] = $user->id;
                }
                
            }
            if (is_array($id)) {
                $id = implode(",",$id);
            } else {
                $id = '-1';
            }
            
        } else {
            $id = '-1';
        }
        
        $this->router->redirect("/fiscal/principal/{$id}/{$date}");

    }

    public function addActivity () {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $store_id = $_POST['store_id'];
        $date = $_POST['date'];
        $period_id = $_POST['period_id'];
        $type_id = $_POST['type_id'];
        $activity_type_id = $_POST['activity_type_id'];
        $fiscalActivity = new FiscalActivity();
        $fiscalActivity->store_id = $store_id;
        $fiscalActivity->month = $date;
        $fiscalActivity->period_id = $period_id;
        $fiscalActivity->type_id = $type_id;
        $fiscalActivity->activity_type_id = $activity_type_id;
        $fiscalActivity->user_id = $usuario->id;
        $fiscalActivityId =  $fiscalActivity->save();

        $return['sucesso'] = true;
        $return['msg'] = "Realizado.";
        echo json_encode($return);
        return;
        // store_id =  and month =  and  = 1 and  = 1 and  = 7
    }

    public function addConference () {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $store_id = $_POST['store_id'];
        $date = $_POST['date'];
        $conferenceType = $_POST['conferenceType'];
        if ($conferenceType == 'kw') {
            $kwConference = new KwConference();
            $kwConference->store_id = $store_id;
            $kwConference->date = $date;
            $kwConference->user_id = $usuario->id;
            $kwConferenceId =  $kwConference->save();
        } else {
            $conectorConference = new ConectorConference();
            $conectorConference->store_id = $store_id;
            $conectorConference->date = $date;
            $conectorConference->user_id = $usuario->id;
            $conectorConferenceId =  $conectorConference->save();
        } 

        $return['sucesso'] = true;
        $return['msg'] = "Realizado.";
        echo json_encode($return);
        return;
        // store_id =  and month =  and  = 1 and  = 1 and  = 7
    }


    public function fiscalActivityOnlyRead($data) {
        $store = $data['store'];
        $date = $data['date'];
        $fiscalStores = (new FiscalStores())->findById($store);
        $interactions = new FiscalInteraction();
        $interactions = $interactions->find("fiscal_store_id = :rid and month = :date", "date=$date&rid=$store")->order("id DESC")->fetch(true);
        echo $this->view->render("administrator/fiscalStoreData",["title" => "Atividade Fiscal Loja $store mês $date | ".SITE,
        "store" => $fiscalStores,
        "date" => $date,
        "interactions" => $interactions]);

    }

    public function addInteraction() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $class = $usuario->class;
        $store = $usuario->store_code;
        $name = $usuario->name;
    
        $stoeId = $_POST['store_id'];
        $idUser = $usuario->id;
        $note = $_POST['descricao'];
        $month = $_POST['month'];
    
        if ($note == '') {
            $data['sucesso'] = false;
            $data['msg'] = "Comentario não informado.";
            echo json_encode($data);            
        } else {
    
            $interaction = new FiscalInteraction();
        
            $interaction->fiscal_store_id = $stoeId;
            $interaction->user_id = $idUser;
            $interaction->note = nl2br($note);
            $interaction->month = $month;
            $interactionId = $interaction->save();
    
            if ($interactionId) {
                $id = $interaction->id;                             
                $data['sucesso'] = true;
                $data['msg'] = "Interação enviada com sucesso.";
                echo json_encode($data);
            } else {
                $data['sucesso'] = false;
                $data['msg'] = "Falha ao inserir interação no banco de dados.";
                echo json_encode($data);
    
            }  
    
        }        
    }

    public function NFESefazConference() {
        $loja = $_POST['store'];

        if ($_POST['base'] == 1) {
            $CNPJ = (new AA2CTIPO())->find("TIP_CODIGO = $loja")->fetch()->TIP_CGC_CPF;
        } else {
            $CNPJ = (new AA2CTIPO2())->find("TIP_CODIGO = $loja")->fetch()->TIP_CGC_CPF;
        }
        
        $file = new File("storage","files");

        $date1 = inputDateToRMSDate($_POST['date1']);
        $date2 = inputDateToRMSDate($_POST['date2']);

        if ($_FILES) {
            $upload = $file->upload($_FILES['file'], PATHINFO_FILENAME);
        }
        
        $read = true;
        
        if ($read) {
            $stream = fopen("C:/xampp/htdocs/bdtecsolucoes.com.br/".$upload,"r");
            
            $csv = Reader::createFromStream($stream);
            
            // caso sejá delimitado por ; pode colocar
            $csv->setDelimiter(";");
            // se tiver cabeçalho e eu não quiser que eles sejam chave do array defino null senão defino 0
            $csv->setHeaderOffset(0); 
            $input_bom = $csv->getInputBOM();
            if ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {
                CharsetConverter::addTo($csv, 'utf-16', 'utf-8');
            }
            //->offset(1) para pular a primeira linha e ->limit(1) para limitar os regirstros
            $stmt = (new Statement())->offset(0);
            $users = $stmt->process($csv);
            $i = 1;
            $error = false;
            foreach ($users as $user) {
                $chaves[$i] = substr($user["Chave de Acesso"],1);
                if ($_POST['base'] == '') {
                    $nfeControle = (new NFE_CONTROLE())->find("DESCRICAO_SITUACAO != 'NF Entrada' and CHAVE_ACESSO_NFE = '".substr($user["Chave de Acesso"],1)."'")->fetch();
                } else {
                    $nfeControle = (new NFE_CONTROLE2())->find("DESCRICAO_SITUACAO != 'NF Entrada' and CHAVE_ACESSO_NFE = '".substr($user["Chave de Acesso"],1)."'")->fetch();
                }
                

                if (!$nfeControle) {
                    echo "Não esta no RMS ".substr($user["Numero NF-e"],1)." Valor:  ".$user["Valor (R$)"]."<br>";
                    $error = true;
                    
                } else {
                    if (!mb_strpos($nfeControle->DESCRICAO_SITUACAO,$user["Situacao"])) {
                        echo "Esta no RMS porém com Situação Divergente Número: ".$user["Numero NF-e"]." Valor:  ".$user["Valor (R$)"]." | ".$user["Situacao"]." | $nfeControle->DESCRICAO_SITUACAO <br>";
                        $error = true;
                    }
                }
                $i++;
            }
            
            if ($_POST['base'] == 1) {
                $nfeControles = (new NFE_CONTROLE())->find("DESCRICAO_SITUACAO != 'NF Entrada' and rms.dateto_rms7(DATA_PROCESSAMENTO) between $date1 and $date2 and substr(CHAVE_ACESSO_NFE,7,14) = lpad('$CNPJ',14,'0')")->fetch(true);
            } else {
                $nfeControles = (new NFE_CONTROLE2())->find("DESCRICAO_SITUACAO != 'NF Entrada' and rms.dateto_rms7(DATA_PROCESSAMENTO) between $date1 and $date2 and substr(CHAVE_ACESSO_NFE,7,14) = lpad('$CNPJ',14,'0')")->fetch(true);
            }
            // dd($nfeControles);

            
            
            foreach ($nfeControles as $nfeControle) {
                if (!in_array($nfeControle->CHAVE_ACESSO_NFE,$chaves)) {
                    echo "Esta Nota não esta na SEFAZ ".$nfeControle->CHAVE_ACESSO_NFE."<br>";
                    $error = true;
                }
            }
            
        }
        if ($error === false) {
            echo "Nenhuma divergencia encontrada.";
        }
    }

    
    
}