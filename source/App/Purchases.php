<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\Call;
use Source\Models\CallAttachment;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\NfRms;
use Source\Models\AA1CTCON3;
use Source\Models\AA3CNVCC;
use Source\Models\Inventories;
use Source\Models\AA3CNVCC2;
use Source\Models\AA2CTIPO;
use Source\Models\AA2CTIPO3;
use Source\Models\AGGFLSPROD7;
use Source\Models\AA2CTIPO2;
use Source\Models\Status;
use Source\Models\Counting;
use Source\Models\StockMovement;
use Source\Models\Session;
use Source\Models\UserClass;
use Source\Models\Inventory;
use Source\Models\Marketing;
use Source\Models\NfType;
use Source\Models\Audit;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SendGrid\Mail\Mail;
use Source\Models\Aa3citemnm;
use SendGrid;
use Symfony\Component\DomCrawler\Crawler;
use Source\Models\NfReceipt;
use Source\Models\NfReceipt2;
use Source\Models\NfStatus;
use Source\Models\NfAttachment;
use Source\Models\NfDueDate;
use Source\Models\NfOperation;
use Source\Models\NfReceiptLog;
use Source\Models\NfReport;
use Source\Models\NfSefaz;
use Source\Models\ExchangeCode;
use Source\Models\NfEvent;
use Dompdf\Dompdf;
use Source\Models\NfStarted;
use Source\Models\RegisterAmountItems;
use Source\Models\ReasonPendingClient;
use Source\Models\AA1CFISC;
use Source\Models\AA1CFISC2;
use Source\Models\AA1LINHP;
use Source\Models\AA1LINHP2;
use Source\Models\AA3CITEM;
use Source\Models\AA3CITEM2;
use Source\Models\AA3CITEM7;
use Source\Models\AA3CITEM3;
use Source\Models\AG1IENSA;
use Source\Models\AA1AGEDT;
use Source\Models\AG1IENSA2;
use Source\Models\AG1IENSA3;
use Source\Models\AG1IENSA7;
use Source\Models\AA1CTCON2;
use Source\Models\AA2CESTQ;
use Source\Models\AA2CESTQ2;
use Source\Models\AA2CESTQ3;
use Source\Models\AA3CNVCC3;
use Source\Models\ButcheryRegistration;
use Source\Models\ExchangePackaging;
use Source\Models\RequestScore;
use Source\Models\InventoryItems;
use Source\Models\Itens;
use Source\Models\MarginVerification;
use Source\Models\PostedVerification;
use Source\Models\Itens51;
use Source\Models\DIMPER;
use Source\Models\DIMPER2;
use Source\Models\DIMPER3;
use Source\Models\AGGFLSPROD;
use Source\Models\AGGFLSPROD2;
use Source\Models\AGGFLSPROD3;
use Source\Models\CAPCUPOM;
use Source\Models\CAPCUPOM2;
use Source\Models\AG1PDVPD2;
use Source\Models\AG1PDVPD;
use Source\Models\AA2CPARA;
use Source\Models\AA2CPARA2;
use Source\Models\NotificationsAudit;
use Source\Models\DEVCLICP;
use Source\Models\NFE_CONTROLE;
use Source\Models\NFE_CONTROLE2;
use DateTime;
use Source\Models\AA1CTCON;
use Source\Models\AA3CITEM4;
use Source\Models\AA1CTCON4;
use Source\Models\AG1IENSA4;
use Source\Models\MapFamilia;
use Source\Models\AGGFLSPROD4;
use Source\Models\AA1DITEM;
use Source\Models\AA1DITEM2;
use Source\Models\AA1DITEM3;
use Source\Models\AA1DITEM4;
use Source\Models\NCM;
use Source\Models\AA1CFISC3;
use Source\Models\AA1CFISC4;
use Source\Models\AA0LOGIT;
use Source\Models\AA0LOGIT2;
use Source\Models\AA0LOGIT3;
use Source\Models\AA0LOGIT4;
use Source\Models\AA0LOGIT5;
use Source\Models\CadastroExterno;
use Source\Models\NCMALIQUOTA;
use Source\Models\NCMPIS;
use Source\Models\SupplierRegistration;
use Source\Models\AA1FORIT;
use Source\Models\AA1FORIT2;
use Source\Models\AA1FORIT3;
use Source\Models\AA1FORIT4;
use Source\Models\AA3CCEAN;
use Source\Models\AA3CCEAN2;
use Source\Models\AuditoriaIcmsEan;
use Source\Models\AA3CCEAN3;
use Source\Models\AA3CCEAN4;
use Source\Models\AA2CTIPO4;
use League\Csv\Writer;
use Source\Models\Purchase;
use Source\Models\PurchaseItem;
use PhpOffice\PhpSpreadsheet\Writer\Ods\WriterPart;

class Purchases 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function purshasePrincipalForm() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        // if ($usuario->store_group_id == 1 || $usuario->store_group_id == 10) {
        //     $aa2ctipo = (new AA2CTIPO())->find("TIP_LOJ_CLI = 'F' and TIP_NATUREZA in ('FF', 'FD') and TIP_ESTADO = 'BA'")->fetch(true);
        // } else if ($usuario->store_group_id == 2) {
        //     $aa2ctipo = (new AA2CTIPO2())->find("TIP_LOJ_CLI = 'F' and TIP_NATUREZA in ('FF', 'FD') and TIP_ESTADO = 'BA'")->fetch(true);
        // }
        echo $this->view->render("client/purchasesPrincipalForm",
            [
                "id" => "solicitação de lançamentos | " . SITE,
                "store" => $usuario->store_code,
                 "aa2ctipo" => ''
            ]);
    }

    public function purshaseSecondaryForm() {
        
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $cnpj = $_POST["cnpj"];
        $secao = $_POST["secao"];
        $grupo = $_POST["grupo"];
        $subgrupo = $_POST["subgrupo"];
        $sistematica = $_POST['sistematica'];
        if ($usuario->store_group_id == 1 || $usuario->store_group_id == 10) {
            $aa2ctipo = (new AA2CTIPO())->find("TIP_CGC_CPF = $cnpj")->fetch();
            if ($secao != '') {
                $params['GIT_SECAO'] = $secao;
                $comparation[0] = "GIT_SECAO = :GIT_SECAO";
            }
            if ($grupo != '') {
                $params['GIT_GRUPO'] = $grupo;
                $comparation[1] = "GIT_GRUPO = :GIT_GRUPO";
            }
            if ($subgrupo != '') {
                $params['GIT_SUBGRUPO'] = $subgrupo;
                $comparation[2] = "GIT_SUBGRUPO = :GIT_SUBGRUPO";
            }
            if ($sistematica != '') {
                $params['GIT_SIS_ABAST'] = $sistematica;
                $comparation[2] = "GIT_SIS_ABAST = :GIT_SIS_ABAST";
            }
            if (is_array($comparation)) {
                $comparation = " and ".implode(' and ', $comparation);
                $params = http_build_query($params);
            } else {
                $comparation = '';
                $params = '';
            }
            $aa3citem = (new AA3CITEM())->find($comparation."GIT_COD_FOR = $aa2ctipo->TIP_CODIGO",$params)->fetch(true);

        } else if ($usuario->store_group_id == 2) {
            $aa2ctipo = (new AA2CTIPO2())->find("TIP_CGC_CPF = $cnpj")->fetch();
           
            if ($secao != 'Selecione ...') {
                $params['GIT_SECAO'] = $secao;
                $comparation[0] = "GIT_SECAO = :GIT_SECAO";
            }
            if ($grupo != 'Selecione ...') {
                $params['GIT_GRUPO'] = $grupo;
                $comparation[1] = "GIT_GRUPO = :GIT_GRUPO";
            }
            if ($subgrupo != 'Selecione ...') {
                $params['GIT_SUBGRUPO'] = $subgrupo;
                $comparation[2] = "GIT_SUBGRUPO = :GIT_SUBGRUPO";
            }
            if ($sistematica != '') {
                $params['GIT_SIS_ABAST'] = $sistematica;
                $comparation[2] = "GIT_SIS_ABAST = :GIT_SIS_ABAST";
            }
            if (is_array($comparation)) {
                $comparation = implode(' and ', $comparation)." and ";
                $params = http_build_query($params);
            } else {
                $comparation = '';
                $params = '';
            }
                
            
            
            $aa3citem = (new AA3CITEM2())->find($comparation."GIT_COD_FOR = $aa2ctipo->TIP_CODIGO",$params)->fetch(true);
         
        }
        $i = 0;
        if (is_array($aa3citem)){
            foreach($aa3citem as $citem){
                $registros[$i]['referencia'] = $citem->GIT_REFERENCIA;
                $registros[$i]['ean'] = $citem->GIT_CODIGO_EAN13;
                $registros[$i]['descricao'] = $citem->GIT_DESCRICAO;
                $registros[$i]['codigo'] = $citem->GIT_COD_ITEM;
                $registros[$i]['custo'] = $citem->aa2cestq($usuario->store_code)->GET_CUS_ULT_ENT;
                $registros[$i]['valor'] = $citem->aa2cestq($usuario->store_code)->GET_CUS_ULT_ENT_BRU/100;
                $itens[$citem->GIT_COD_ITEM] = $citem->GIT_COD_ITEM;
                $i++;
            }
            $itens = implode(",",$itens);
        }
        
               
        echo $this->view->render("client/purchasesSecondaryForm",
            [
                "id" => "solicitação de lançamentos | " . SITE,
                "store" => $usuario->store_code,
                 "aa2ctipo" => $aa2ctipo,
                 "aa3citem" => $registros,
                 "itens" => $itens
            ]);
    }

    public function purshaseGeracao() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $cnpj = $_POST["cnpj"];
        $codigos = $_POST['codigos'];
        $razao = $_POST['razao'];
        $purchase = new Purchase();
        $purchase->store_id = $usuario->store_code;
        $purchase->cnpj = $cnpj;
        $purchase->razao = $razao;
        $purchaseID = $purchase->save();
        $codigos = explode(',',$codigos);
        foreach($codigos as $citem){
            if ($_POST["quantidade$citem"] > 0) {
                $purchaseItem = new PurchaseItem;
                $purchaseItem->purchase_id = $purchase->id;
                $purchaseItem->reference = $_POST["referencia$citem"];
                $purchaseItem->ean = $_POST["ean$citem"];
                $purchaseItem->code = $citem;
                $purchaseItem->description = $_POST["descricao$citem"];
                $purchaseItem->cost =  $_POST["custo$citem"];
                $purchaseItem->amount =  $_POST["quantidade$citem"];
                $purchaseItemID = $purchaseItem->save();
                $custoTotal += ( $_POST["custo$citem"]*$_POST["quantidade$citem"]);
            }

        
        }
        $purchase2 = (new Purchase())->findById($purchase->id);
        $purchase2->cost = $custoTotal;
        $purchase2ID = $purchase2->save();


               
        echo $this->view->render("client/purshaseGeracao",
            [
                "id" => "solicitação de lançamentos | " . SITE,
                "purchase" => $purchase2,
                "league" => true
            ]);
    }
}