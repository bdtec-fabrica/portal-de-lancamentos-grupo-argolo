<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\Status;
use Source\Models\Session;
use Source\Models\UserClass;
use Source\Models\NfType;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SendGrid\Mail\Mail;
use SendGrid;
use Symfony\Component\DomCrawler\Crawler;
use Source\Models\NfReceipt;
use Source\Models\NfStatus;
use Source\Models\NfAttachment;
use Source\Models\NfDueDate;
use Source\Models\NfOperation;
use Source\Models\NfReceiptLog;
use Source\Models\NfSefaz;
use Source\Models\SupplierRegistration;
use Source\Models\ProductRegistration;
use Source\Models\ExchangeCode;
use Source\Models\NfStarted;
use Source\Models\ExchangePackaging;
use Source\Models\LogManifestation;
use NFePHP\NFe\ToolsNFe;
use NFePHP\Extras\Danfe;
use NFePHP\NFe\MakeNFe;
use NFePHP\Common\Files\FilesFolders;
use Source\Models\CollectionHeader;
use DateTime;
use DateInterval;
use Source\Models\VIEW_HORTI;
use Source\Models\VIEW_HORTI2;
use Source\Models\VIEW_HORTI10;

class Nfe 
{

    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function NfePdf(){
        //https://bdtecsolucoes.com.br/nfSefaz/pdf/download?key=29210460409075052110550010004189881221290996&store=91
        $chave = $_POST['key'];
        $loja = $_POST['store'];
        $nfe = new ToolsNFe(__DIR__."/../StoreConfig/$loja.json");

        
        $xmlProt = __DIR__."/../XML/".$loja.'/NFe'.$chave.'.xml';

        $pdfDanfe = __DIR__."/../PDF/".$loja.'/NFe'.$chave.'.xml';
        
        $docxml = FilesFolders::readFile($xmlProt);
        $danfe = new Danfe($docxml, 'P', 'A4', $nfe->aConfig['aDocFormat']->pathLogoFile, 'I', '');
        $id = $danfe->montaDANFE();
        $salva = $danfe->printDANFE($pdfDanfe, 'F');
        $abre = $danfe->printDANFE("{$id}-danfe.pdf", 'I');
    }

    public function pastIntoServidor($data) {
        $id = $data["id"];
        $nf = (new NfSefaz())->findById($id);
        $group = (new Store())->find("code = $nf->store_id")->fetch()->group_id;
        
        $ok = pastaImportacaoRMS($nf->store_id,$nf->access_key,$group);
        dd($ok);
        if($ok) {
            echo "<script>window.close();</script:";
        }
       
    }
    //escolhendo a loja que quer consultar
    public function sefazSearch($data){
        // if ($data['store'] == 110) {
        //     echo "<p>Serviço pausado para manutenção.</p>";
        //     die;
        // }
        
        $stores = (new Store())->find("code = ".$data['store'])->order("code")->fetch(true);
      
        foreach ($stores as $store) {

            $nfe = new ToolsNFe(__DIR__."/../StoreConfig/$store->code.json");
            
            echo $store->cStat;
            
            $nsu = $store->nsu;
            $continuar = true;
            if (($store->cStat == '137' || $store->cStat == '589' || $store->cStat == '656') && diferencaHorasTimestamp($store->updated_at,date("Y-m-d H:i:s")) < 90) {
                echo "<h1>Busca pausada</h1>
                        <h5>A Busca esta pausada para evitar bloqueios, Nossos serviços estão alinhados com as diretrizes da SEFAZ.</h5>
                        <h5>Caso queira pode tentar novamente após ".(90-diferencaHorasTimestamp($store->updated_at,date("Y-m-d H:i:s")))." minutos. </h5>";
            } else {

                $numeroConsultas = 0;
                do {
                    if ($numeroConsultas <= 15) {
                        $xml = $nfe->sefazDistDFe('AN', '1', $store->cnpj,'0', $nsu, $aResposta);
                        $numeroConsultas++;
                    
                        if ($aResposta['cStat'] == '137') {
                            //se retornar 137 registre o retorno e salve o horario.
                            $bloquear = (new Store())->findById($store->id);
                            $bloquear->cStat = '137';
                            $bloquarId = $bloquear->save();
                            echo "<h5>".$store->cnpj." - ".$store->company_name." - ".$aResposta['cStat']." - ".$aResposta['xMotivo']."</h5>";
                            $continuar = false;
                            
                        } else if ($aResposta['cStat']=='589') {
                            $desbloquear = (new Store())->findById($store->id);
                            $desbloquear->cStat = '589';
                            $desbloquear->nsu = $nsu;
                            $desbloquearId = $desbloquear->save();
                            echo "<h5>NSU maximo alcançado. Sem mais documentos a consultar.</h5>";
                            $continuar = false;
                        }  else if ($aResposta['cStat']=='109') {
                            //se retornar 109 problema na SEFAZ dispara telegram xMotivo
                            echo "<h5>".$store->cnpj." - ".$store->company_name." - ".$aResposta['cStat']." - ".$aResposta['xMotivo']."</h5>";
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "1247513338";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                            $logger->pushHandler($tele_handler);
                            $logger->info($store->cnpj." - ".$store->company_name." - ".$aResposta['cStat']." - ".$aResposta['xMotivo']);
                            $desbloquear = (new Store())->findById($store->id);
                            $desbloquear->nsu = $nsu;
                            $desbloquearId = $desbloquear->save();
                            $continuar = false;
                        }  else if ($aResposta['cStat'] == '656') {
                            //se retornar 656 consumo indevido dispara telegram xMotivo
                            echo $store->cnpj." - ".$store->company_name." - ".$aResposta['cStat']." - ".$aResposta['xMotivo'];
                            // $logger = new Logger("web");
                            // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            // $tele_channel = "1247513338";
                            // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            // $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                            // $logger->pushHandler($tele_handler);
                            // $logger->info($store->cnpj." - ".$store->company_name." - ".$aResposta['cStat']." - ".$aResposta['xMotivo']);
                            $desbloquear = (new Store())->findById($store->id);
                            $desbloquear->cStat = '656';
                            $desbloquear->nsu = $nsu;
                            $desbloquearId = $desbloquear->save();
                            $continuar = false;
                            
                        } else if ($aResposta['cStat'] == '138') {
                            $desbloquear = (new Store())->findById($store->id);
                            $desbloquear->cStat = '';
                            $desbloquear->nsu = $nsu;
                            $desbloquearId = $desbloquear->save();
                            foreach ($aResposta['aDoc'] as $doc) {
                                if($doc['schema'] =='resNFe_v1.00.xsd'){ // Não tem xml para baixar. Manifestar para baixar posteriormente.
                                    $chave = (simplexml_load_string($doc['doc'])->chNFe);
                                    $numeroNFE = substr($chave,25,9);
                                    $nProt = (simplexml_load_string($doc['doc'])->nProt);
                                    $dataEmissao = (simplexml_load_string($doc['doc'])->dhEmi);
                                    $cnpjEmit = (simplexml_load_string($doc['doc'])->CNPJ);
                                    $xJust = 'bispo&dantas';
                                    $tpEvento = '210210'; //ciencia da operação
                                    $aManifestacao = array();
                                    
                                    $manifestacao = $nfe->sefazManifesta($chave, 1, $xJust, $tpEvento, $aManifestacao); //Reaizar a manifestação.
                
                                    $row = (new NfSefaz())->find("access_key = :access_key","access_key=$chave")->count();
                                    if($row == 0){
                                        $nfeSefaz = (new NFSefaz());
                                        $nfeSefaz->access_key = $chave;
                                        $nfeSefaz->nfe_number = $numeroNFE;
                                        $nfeSefaz->protocol = $nProt;
                                        $nfeSefaz->issuer_cnpj-> $cnpjEmit;
                                        $nfeSefaz->nsu = $nsu;
                                        $nfeSefaz->manifestation = '';
                                        $nfeSefaz->store_id = $store->code;
                                        $nfeSefaz->emission_date = $dataEmissao;
                                        $nfeSefazId = $nfeSefaz->save();
                                        $salvas += 1;
                                    }
                                } else if($doc['schema'] =='procNFe_v4.00.xsd'){ // tem xml pra baixar ...
                                    
                                    $xml = simplexml_load_string($doc['doc']);
                                    $cnpj = $xml->NFe->infNFe->emit->CNPJ;
                                    $company_name = $xml->NFe->infNFe->emit->xNome;
                                    $fantasy_name = $xml->NFe->infNFe->emit->xFant;
                                    $state_registration = $xml->NFe->infNFe->emit->IE;
                                    $public_place = $xml->NFe->infNFe->emit->enderEmit->xLgr;
                                    $number = $xml->NFe->infNFe->emit->enderEmit->nro;
                                    $district = $xml->NFe->infNFe->emit->enderEmit->xBairro;
                                    $county_code = $xml->NFe->infNFe->emit->enderEmit->cMun;
                                    $county = $xml->NFe->infNFe->emit->enderEmit->xMun;
                                    $uf = $xml->NFe->infNFe->emit->enderEmit->UF;
                                    $zip_code = $xml->NFe->infNFe->emit->enderEmit->CEP;
                                    $country_code = $xml->NFe->infNFe->emit->enderEmit->cPais;
                                    $country = $xml->NFe->infNFe->emit->enderEmit->xPais;
                                    $phone = $xml->NFe->infNFe->emit->enderEmit->fone;
                                    $crt = $xml->NFe->infNFe->emit->CRT;
            
                                    $supplierRegistration = new SupplierRegistration();
                                    $productRegistration = new ProductRegistration();
                                    
                                    $countSupplierRegistration = $supplierRegistration->find("cnpj = :cnpj", "cnpj=$cnpj")->count();
                                    if ($countSupplierRegistration == 0) {
                                        $supplierRegistrationAdd = new SupplierRegistration();
                                        $supplierRegistrationAdd->cnpj = $cnpj;
                                        $supplierRegistrationAdd->company_name = $company_name;
                                        $supplierRegistrationAdd->fantasy_name = $fantasy_name;
                                        $supplierRegistrationAdd->state_registration = $state_registration;
                                        $supplierRegistrationAdd->public_place = $public_place;
                                        $supplierRegistrationAdd->number = $number;
                                        $supplierRegistrationAdd->district = $district;
                                        $supplierRegistrationAdd->county_code = $county_code;
                                        $supplierRegistrationAdd->county = $county;
                                        $supplierRegistrationAdd->uf = $uf;
                                        $supplierRegistrationAdd->zip_code = $zip_code;
                                        $supplierRegistrationAdd->country_code = $country_code;
                                        $supplierRegistrationAdd->country = $country;
                                        $supplierRegistrationAdd->phone = $phone;
                                        $supplierRegistrationAdd->crt = $crt;
                                        $supplierRegistrationAddSave = $supplierRegistrationAdd->save();
            
                                    } else {
                                        $supplierRegistrationAdd = (new SupplierRegistration())->find("cnpj = :cnpj", "cnpj=$cnpj")->fetch();
                                    }
                                    foreach($xml->NFe->infNFe->det as $prod){
                                        
                                        $productRegistration->store_id = $store->code;
                                        $productRegistration->reference = $prod->prod->cProd;
                                        $productRegistration->ean = $prod->prod->cEAN;
                                        $productRegistration->description = $prod->prod->xProd;
                                        $productRegistration->ncm = $prod->prod->NCM;
                                        $productRegistration->cest = $prod->prod->CEST;
                                        $productRegistration->packing = $prod->prod->uCom;
                                        $productRegistration->cost = $prod->prod->vUnCom;
                                        $productRegistration->supplier_id = $supplierRegistrationAdd->id;
                                        $productRegistrationId = $productRegistration->save();
                                    }
                                    $chave = $xml->NFe->infNFe['Id'][0];
                                    $dhEmi = $xml->NFe->infNFe->ide->dhEmi;
                                    
                                    if(strlen($chave)==47){                    
                                        $xml1 = $xml->NFe->infNFe;
                                        foreach($xml1->emit as $xml1){
                                            $CNPJ = $xml1->CNPJ;
                                        }
                                        $parametroRow = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$store->code")->count();
                                        #FAZER O TROCA EMBALAGEM AQUI
                                        if($parametroRow > 0){
                                            $exchangePackagings = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$store->code")->fetch(true);
                                            foreach ($exchangePackagings as $assoc) {
                                                foreach( $xml1->NFe->infNFe->det as $xml){
                                                    if($xml->prod->cProd == str_replace('-','',$assoc->code) &&  $xml->prod->uCom == $assoc->packing_of){
                                                        /*
                                                        * Embalagem
                                                        */
                                                        $xml->prod->uCom = $assoc->packing_for;
                                                        $xml->prod->uTrib = $assoc->packing_for;
                                                        /*
                                                         * Custo unitario
                                                         */
                                                        $xml->prod->vUnCom = floatval((floatval($xml->prod->vUnCom)/floatval($xml->prod->qCom))/$assoc->packing_for);
                                                        $xml->prod->vUnTrib = floatval((floatval($xml->prod->vUnTrib)/floatval($xml->prod->qTrib))/$assoc->packing_for);
                                                        /*
                                                         * Quantidade
                                                         */
                                                        $xml->prod->qCom = $xml->prod->qCom/$assoc->packing_for;
                                                        $xml->prod->qTrib = $xml->prod->qTrib/$assoc->packing_for;
                                                        $xml1->asXML('/../XML/'.$store->code.'/'.$chave.'.xml');
                                                    }
                                                }
                                            }
                                        }
                                        $checkRow = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$store->code")->count();
                                        #FAZER O TROCA CODIGO AQUI
                                        if($checkRow > 0){
                                            $exchangeCode = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$store->code")->fetch(true);
                                            foreach($exchangeCode as $assoc) {
                                                foreach( $xml1->det as $xml1){
                                                    if($xml1->prod->cProd == str_replace('-','',$assoc->source_code)){
                                                        $verificacao = true;
                                                        $xml1->prod->cEAN = $assoc->destination_code;
                                                        $xml = $xml->asXML(__DIR__."/../XML/".$store->code.'/'.$chave.'.xml');
                                                    }
                                                }
                                            }
                                            $xml = $xml->asXML(__DIR__."/../XML/".$store->code.'/'.$chave.'.xml');
                                        }else{
                                            $xml = $xml->asXML(__DIR__."/../XML/".$store->code.'/'.$chave.'.xml');
                                        }  
                                        $baixada += 1; 
                                        echo $store->code."-".$chave."-".$store->group_id;
                                        if ($store->group_id == 1 || $store->group_id == 10 || $store->group_id == 2 || $store->group_id == 7) {
                                            $ok = pastaImportacaoRMS($store->code,$chave,$store->group_id);
                                            if ($ok) {
                                                echo "OKOKOK";
                                            }
                                            
                                        }        
                                    }
                                }
                            } 
                            $nsu++;
                        }
                    } else {
                        $continuar = false;
                    }
                } while ($continuar == true);
                echo "<h5>0".$baixada." Notas baixadas.</h5>";
                echo "<h5>0".$salvas." Notas salvas.</h5>";
            }
            
            
        }    
    }

    public function nfePainel($data) {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $class = $usuario->class;
            $store = $usuario->store_code;
            $nfeType = (new NfType())->find()->fetch(true);
                $collectionHeaders =  (new CollectionHeader())->find("status != :status","status=C")->fetch(true);
                $identification = $_COOKIE['login'];
                $usuario = (new User())->find("email = :email","email=$identification")->fetch();
                $class = $usuario->class;
                if ($class == "A") {
                    echo $this->view->render("administrator/nfePainel", ["id" => "Painel NFe | " . SITE,
                    "store" =>$store,
                    "nfeType" => $nfeType ]);
                } else if ($class == "C") {

                    if ($usuario->store_group_id == 1) {
                        $aa2cestq = (new VIEW_HORTI())->find("(GET_DT_ULT_FAT = 1".date("ym",strtotime("-1 month"))." or GET_DT_ULT_FAT = 1".date("ym").") and substr(GET_COD_LOCAL,0,LENGTH(GET_COD_LOCAL)-1) = $store and GIT_SECAO = 5")->order("GIT_DESCRICAO")->fetch(true);
                    } else if ($usuario->store_group_id == 2){
                        $aa2cestq = (new VIEW_HORTI2())->find("(GET_DT_ULT_FAT = 1".date("ym",strtotime("-1 month"))." or GET_DT_ULT_FAT = 1".date("ym").") and substr(GET_COD_LOCAL,0,LENGTH(GET_COD_LOCAL)-1) = $store and GIT_SECAO = 5")->order("GIT_DESCRICAO")->fetch(true);
                    } else if ($usuario->store_group_id == 10){
                        $aa2cestq = (new VIEW_HORTI10())->find("(GET_DT_ULT_FAT = 1".date("ym",strtotime("-1 month"))." or GET_DT_ULT_FAT = 1".date("ym").") and substr(GET_COD_LOCAL,0,LENGTH(GET_COD_LOCAL)-1) = $store and GIT_SECAO = 5")->order("GIT_DESCRICAO")->fetch(true);
                    }
                    

                    echo $this->view->render("client/nfePainel", ["id" => "Painel NFe | " . SITE,
                    "store" =>$store,
                    "nfeType" => $nfeType,
                    "aa2cestq" => $aa2cestq
                ]);
                } else if ($class == "OG" || $class == "OG2") {
                    $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
                    foreach ($userStoreGroup as $userGroups) {
                        $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
                    }
            
                    if (is_array($groups)){
                    $groups = implode(", ",$groups);
                    $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                    }
                    echo $this->view->render("operator/nfePainel2", ["id" => "Painel NFe | " . SITE ,
                        "nfes" => $nfSefaz,
                        "status" => $status,
                        "nfeType" => $nfeType,
                        "store" => $store,
                        "groups" => $groups
                    ]);
                } else if ($class == "O") {
                    echo $this->view->render("operator/nfePainel", ["id" => "Painel NFe | " . SITE,
                    "store" =>$store,
                    "collectionHeaders" => $collectionHeaders,
                    "nfeType" => $nfeType]);
                } else if ($class == "C2") {
                    echo $this->view->render("client/nfePainel", ["id" => "Painel NFe | " . SITE ,
                    "nfes" => $nfSefaz,
                    "c2" => 'c2',
                    "nfeType" => $nfeType,
                    "store" =>$store    
                ]); 
                }
                

                
            
            
        }
    }

    public function NfSearch() {
        if ((!isset($_COOKIE['login']) == true)) {
            $this->router->redirect("/login");
        } else {
            $identification = $_COOKIE['login'];
            $usuario = (new User())->find("email = :email","email=$identification")->fetch();
            $loja = $usuario->store_code;
            $class = $usuario->class;
            $group = $usuario->store_group_id;
            $store = $_POST['store'];
            $cnpj = $_POST['cnpj'];
            $lancada = $_POST['lancada'];
            if ($loja != 2000){
                $store = $loja;
            }
            $id = $_POST['id'];
            $evento = $_POST['evento'];
            $status = $_POST['status'];
            $criacaoInicial = $_POST['criacaoInicial'];
            $criacaoFinal = $_POST['criacaoFinal'];
            $nfNumber = $_POST['nfNumber'];
            $accessKey = $_POST['accessKey'];
            $params = array();
            $comparation = array();
            if ($id != '') {
                $params['id'] = $id;
                $comparation[1] = "id = :id";
            }

            if ($nfNumber != '') {
                $params['nfe_number'] = $nfNumber;
                $comparation[2] = "nfe_number = :nfe_number";
            }

            if ($evento != '') {
                $params['manifestation'] = $evento;
                $comparation[666] = "manifestation = :manifestation";
            }

            if ($accessKey != '') {
                $params['access_key'] = $accessKey;
                $comparation[3] = "access_key = :access_key";
            }

            if ($criacaoInicial != '') {
                $params['criacaoInicial'] = '1' . substr($criacaoInicial, 2, 2) . substr($criacaoInicial, 5, 2) . substr($criacaoInicial, 8, 2);
                $comparation[4] = "concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) >= :criacaoInicial";
            }

            if ($criacaoFinal != '') {
                $params['criacaoFinal'] = '1' . substr($criacaoFinal, 2, 2) . substr($criacaoFinal, 5, 2) . substr($criacaoFinal, 8, 2);
                $comparation[5] = "concat('1',substr(emission_date,3,2),substr(emission_date,6,2),substr(emission_date,9,2)) <= :criacaoFinal";
            }
            
            if ($status != '') {
                $params['status_id'] = $status;
                $comparation[6] = "status_id = :status_id";
            } else {
                $status = 'i';
            }
            
            if ($store != '') {
                $params['store_id'] = $store;
                $comparation[7] = "store_id = :store_id";
            } else {
                if ($class == 'OG' || $class == 'OG2') {
                    $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
                    foreach ($userStoreGroup as $userGroups) {
                        $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
                    }
            
                    if (is_array($groups)){
                        $groups = implode(", ",$groups);
                        $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                        foreach($groups as $group){
                            $params2["store_id$group->code"] = $group->code;
                            $comparation2[$group->code] = "store_id = :store_id$group->code";
                        }
                        $comparation2 = " and (".implode(' or ', $comparation2).")";
                        $params2 = "&".http_build_query($params2);
                        
                    }
                }
            }

            if ($cnpj != '') {
                $params['cnpj'] = $cnpj;
                $comparation[111] = "substr(access_key,7,14) = :cnpj";
            }
            
            
            $comparation = implode(' and ', $comparation);
            $params = http_build_query($params);
            
            $date = new DateTime(date("Y-m-d"));
            $interval = new DateInterval('P1M');
            
            $date->sub($interval);
            $anterior = $date->format('Y-m');
            $atual = date("Y-m");


            if(!isset($comparation)) {
                $comparation = "store_id = :store_id and (substr(emission_date,1,7) = :atual or substr(emission_date,1,7) = :anterior)";
            }

            if($params == '') {
                $params = "store_id=$loja&atual=$atual&anterior=$anterior";
            }
           
            $nfSefaz = (new NfSefaz());
            
            $nfSefaz = $nfSefaz->find($comparation.$comparation2, $params.$params2)->order("id DESC")->fetch(true);
            


            $collectionHeaders =  (new CollectionHeader())->find("status != :status","status=C")->fetch(true);
            $nfeType = (new NfType())->find()->fetch(true);
            if ($class == "A") {
                
                echo $this->view->render("administrator/nfePainel", ["id" => "Painel NFe | " . SITE ,
                    "nfes" => $nfSefaz,
                    "nfeType" => $nfeType,
                    "store" => $store                 
                ]); 
            } else if ($class == "C") {
                $i = 1;
                foreach ($nfSefaz as $nf) {
                    if ($lancada == 2) {
                        if ($nf->getAA1CFISC) {
                            $nfes[$i] = $nf;
                        $i++;
                        }
                    } else if ($lancada == 1) {
                        if (!$nf->getAA1CFISC) {
                            $nfes[$i] = $nf;
                        $i++;
                        }
                    } else {
                        $nfes[$i] = $nf;
                        $i++;
                    }
                    
                    
                }
                echo $this->view->render("client/nfePainel", ["id" => "Painel NFe | " . SITE ,
                "nfes" => $nfes,
                "status" => $status,
                "nfeType" => $nfeType,
                "store" =>$store,
                "lancada" => $lancada
                ]);
                    
            } else if ($class == "O") {
                    echo $this->view->render("operator/nfePainel", ["id" => "Painel NFe | " . SITE,
                    "nfes" => $nfSefaz,
                    "nfeType" => $nfeType,
                    "store" =>$store,
                    "collectionHeaders" => $collectionHeaders]);
            } else if ($class == "C2") {
                echo $this->view->render("client/nfePainel", ["id" => "Painel NFe | " . SITE ,
                "nfes" => $nfSefaz,
                "nfeType" => $nfeType,
                "store" =>$store    
            ]);
            } else if ($class == "OG" || $class == "OG2") {
                    $groups = null;
                    $userStoreGroup = (new UserStoreGroup())->find("user_id = :uid","uid=$usuario->id")->fetch(true);
                    
                    foreach ($userStoreGroup as $userGroups) {
                        $groups[$userGroups->store_group_id] = $userGroups->store_group_id;
                    }
            
                    if (is_array($groups)){
                        
                    $groups = implode(", ",$groups);
                    $groups = (new Store())->find("group_id in ($groups)")->fetch(true);
                    }
                    echo $this->view->render("operator/nfePainel2", ["id" => "Painel NFe | " . SITE ,
                        "nfes" => $nfSefaz,
                        "status" => $status,
                        "nfeType" => $nfeType,
                        "store" => $store,
                        "groups" => $groups
                    ]);
            }
        }
    }

    public function webServiceSearch(){

        $stores = (new Store())->find()->order("code")->fetch(true);
        
        foreach ($stores as $store) {
            $ultNSU = $store->nsu;
            $loja = $store->code;
            $nfe = new ToolsNFe(__DIR__."/../StoreConfig/$store->code.json");
            
            $nfe->setModelo('55');
            $verification = 0;
            $salvaBanco = 0;
            $baixadaPasta = 0;
            $xml = $nfe->sefazDistDFe('AN', '1', $store->cnpj, $ultNSU, '0', $aResposta);        
    
            if($aResposta['cStat']=='138'){//documento localizado  
                foreach ($aResposta['aDoc'] as $doc) {                  
                    if($doc['schema'] == 'resNFe_v1.00.xsd'){ // Não tem xml para baixar. Manifestar para baixar posteriormente.
                        $chave = (simplexml_load_string($doc['doc'])->chNFe);
                        $numeroNFE = substr($chave,25,9);
                        $nProt = (simplexml_load_string($doc['doc'])->nProt);
                        $dataEmissao = (simplexml_load_string($doc['doc'])->dhEmi);
                        $cnpjEmit = (simplexml_load_string($doc['doc'])->CNPJ);
                        $xJust = 'bispo&dantas';
                        $tpEvento = '210210'; //ciencia da operação
                        $aManifestacao = array();
                        
                        $manifestacao = $nfe->sefazManifesta($chave, 1, $xJust, $tpEvento, $aManifestacao); //Reaizar a manifestação.
    
                        $row = (new NfSefaz())->find("access_key = :access_key","access_key=$chave")->count();
                        if($row == 0){
                            $nfeSefaz = (new NFSefaz());
                            $nfeSefaz->access_key = $chave;
                            $nfeSefaz->nfe_number = $numeroNFE;
                            $nfeSefaz->protocol = $nProt;
                            $nfeSefaz->issuer_cnpj-> $cnpjEmit;
                            $nfeSefaz->nsu = $ultNSU;
                            $nfeSefaz->manifestation = '';
                            $nfeSefaz->store_id = $loja;
                            $nfeSefaz->emission_date = $dataEmissao;
                            $nfeSefazId = $nfeSefaz->save();
                            if ($nfeSefazId) {
                                $salvaBanco++;
                                $storeCode = (new Store())->findById($store->id);
                                $storeCode->nsu = $ultNSU;
                                $storeCodeId = $storeCode->save();
                                $ultNSU++;
                            }
                        } else {
                            $storeCode = (new Store())->findById($store->id);
                            $storeCode->nsu = $ultNSU;
                            $storeCodeId = $storeCode->save();
                            $ultNSU++;
                        }
                    } else if($doc['schema'] == 'procNFe_v4.00.xsd'){ // tem xml pra baixar ...
                        $xml = simplexml_load_string($doc['doc']);
                        $cnpj = $xml->NFe->infNFe->emit->CNPJ;
                        $company_name = $xml->NFe->infNFe->emit->xNome;
                        $fantasy_name = $xml->NFe->infNFe->emit->xFant;
                        $state_registration = $xml->NFe->infNFe->emit->IE;
                        $public_place = $xml->NFe->infNFe->emit->enderEmit->xLgr;
                        $number = $xml->NFe->infNFe->emit->enderEmit->nro;
                        $district = $xml->NFe->infNFe->emit->enderEmit->xBairro;
                        $county_code = $xml->NFe->infNFe->emit->enderEmit->cMun;
                        $county = $xml->NFe->infNFe->emit->enderEmit->xMun;
                        $uf = $xml->NFe->infNFe->emit->enderEmit->UF;
                        $zip_code = $xml->NFe->infNFe->emit->enderEmit->CEP;
                        $country_code = $xml->NFe->infNFe->emit->enderEmit->cPais;
                        $country = $xml->NFe->infNFe->emit->enderEmit->xPais;
                        $phone = $xml->NFe->infNFe->emit->enderEmit->fone;
                        $crt = $xml->NFe->infNFe->emit->CRT;

                        $supplierRegistration = new SupplierRegistration();
                        $productRegistration = new ProductRegistration();
                        
                        $countSupplierRegistration = $supplierRegistration->find("cnpj = :cnpj", "cnpj=$cnpj")->count();
                        if ($countSupplierRegistration == 0) {
                            $supplierRegistrationAdd = new SupplierRegistration();
                            $supplierRegistrationAdd->cnpj = $cnpj;
                            $supplierRegistrationAdd->company_name = $company_name;
                            $supplierRegistrationAdd->fantasy_name = $fantasy_name;
                            $supplierRegistrationAdd->state_registration = $state_registration;
                            $supplierRegistrationAdd->public_place = $public_place;
                            $supplierRegistrationAdd->number = $number;
                            $supplierRegistrationAdd->district = $district;
                            $supplierRegistrationAdd->county_code = $county_code;
                            $supplierRegistrationAdd->county = $county;
                            $supplierRegistrationAdd->uf = $uf;
                            $supplierRegistrationAdd->zip_code = $zip_code;
                            $supplierRegistrationAdd->country_code = $country_code;
                            $supplierRegistrationAdd->country = $country;
                            $supplierRegistrationAdd->phone = $phone;
                            $supplierRegistrationAdd->crt = $crt;
                            $supplierRegistrationAddSave = $supplierRegistrationAdd->save();

                        } else {
                            $supplierRegistrationAdd = (new SupplierRegistration())->find("cnpj = :cnpj", "cnpj=$cnpj")->fetch();
                        }
                        foreach($xml->NFe->infNFe->det as $prod){
                            
                            $productRegistration->store_id = $store->code;
                            $productRegistration->reference = $prod->prod->cProd;
                            $productRegistration->ean = $prod->prod->cEAN;
                            $productRegistration->description = $prod->prod->xProd;
                            $productRegistration->ncm = $prod->prod->NCM;
                            $productRegistration->cest = $prod->prod->CEST;
                            $productRegistration->packing = $prod->prod->uCom;
                            $productRegistration->cost = $prod->prod->vUnCom;
                            $productRegistration->supplier_id = $supplierRegistrationAdd->id;
                            $productRegistrationId = $productRegistration->save();
                        }
                        


                        $chave = $xml->NFe->infNFe['Id'][0];
                        $dhEmi = $xml->NFe->infNFe->ide->dhEmi;
                        if(strlen($chave)==47){                    
                            $xml1 = $xml->NFe->infNFe;
                            foreach($xml1->emit as $xml1){
                                $CNPJ = $xml1->CNPJ;
                            }
                            $parametroRow = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
                            #FAZER O TROCA EMBALAGEM AQUI
                            if($parametroRow > 0){
                                $exchangePackagings = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                                foreach ($exchangePackagings as $assoc) {
                                    foreach( $xml1->NFe->infNFe->det as $xml){
                                        if($xml->prod->cProd == str_replace('-','',$assoc->code) &&  $xml->prod->uCom == $assoc->packing_of){
                                            /*
                                            * Embalagem
                                            */
                                            $xml->prod->uCom = $assoc->packing_for;
                                            $xml->prod->uTrib = $assoc->packing_for;
                                            /*
                                             * Custo unitario
                                             */
                                            $xml->prod->vUnCom = floatval((floatval($xml->prod->vUnCom)/floatval($xml->prod->qCom))/$assoc->packing_for);
                                            $xml->prod->vUnTrib = floatval((floatval($xml->prod->vUnTrib)/floatval($xml->prod->qTrib))/$assoc->packing_for);
                                            /*
                                             * Quantidade
                                             */
                                            $xml->prod->qCom = $xml->prod->qCom/$assoc->packing_for;
                                            $xml->prod->qTrib = $xml->prod->qTrib/$assoc->packing_for;
                                            $xml1->asXML('/../XML/'.$loja.'/'.$chave.'.xml');
                                        }
                                    }
                                }
                            }
                            $checkRow = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
                            #FAZER O TROCA CODIGO AQUI
                            if($checkRow > 0){
                                $exchangeCode = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                                foreach($exchangeCode as $assoc) {
                                    foreach( $xml1->det as $xml1){
                                        if($xml1->prod->cProd == str_replace('-','',$assoc->source_code)){
                                            $verificacao = true;
                                            $xml1->prod->cEAN = $assoc->destination_code;
                                            $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                                        }
                                    }
                                }
                                $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                            }else{
                                $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                            }   
                            if ($store->group_id == 1 || $store->group_id == 10) {
                                pastaImportacaoRMS($store->code,$chave);
                            }         
                        }
                        $baixadaPasta ++;
                        
                        $storeCode = (new Store())->findById($store->id);
                        $storeCode->nsu = $ultNSU;
                        $storeCodeId = $storeCode->save();
                        $ultNSU++;
                    } else if($doc['schema'] == 'resEvento_v1.00.xsd'){ //evento
                        $storeCode = (new Store())->findById($store->id);
                        $storeCode->nsu = $ultNSU;
                        $storeCodeId = $storeCode->save();
                        $ultNSU++;
                    } else if($doc['schema']=='procEventoNFe_v1.00.xsd'){ //evento
                        $storeCode = (new Store())->findById($store->id);
                        $storeCode->nsu = $ultNSU;
                        $storeCodeId = $storeCode->save();
                        $ultNSU++;
                    }
                }
            } else if($aResposta['cStat']=='589'){ // Maximo NSU encontrado. Salvar o ultimo Nsu e sair.
                    $storeCode = (new Store())->findById($store->id);
                    $storeCode->nsu = $ultNSU;
                    $storeCodeId = $storeCode->save();
                    $verification = 1;
            } else if($aResposta['cStat']=='137'){// Nenhum arquivo localizado par ao NSU. Gravar o NSU somar                   
                        $storeCode = (new Store())->findById($store->id);
                        $storeCode->nsu = $ultNSU;
                        $storeCodeId = $storeCode->save();
                        $ultNSU++;
                } else if ($aResposta['cStat'] == '109') {
                    die;
                } else if ($aResposta['cStat'] == '656') {
                    echo "$store->code Rejeicao: Consumo Indevido (Deve ser utilizado o ultNSU nas solicitacoes subsequentes. Tente apos 1 hora)<br>";
                }
        }        
    }

    public function nfeReprocess(){
        $store = $_POST['store'];
        $store = (new Store())->find("code = :code","code=$store")->fetch();
        
        $ultNSU = $_POST['nsu'];
        $loja = $store->code;
        $nfe = new ToolsNFe(__DIR__."/../StoreConfig/$store->code.json");

        $nfeSefaz = (new NFSefaz())->find("store_id = :store_id and nsu = :nsu","store_id=$loja&nsu=$ultNSU")->fetch();
        $chaveAcessoNfe = $nfeSefaz->access_key;
        $nfe->setModelo('55');
        $verification = 0;
        $salvaBanco = 0;
        $baixadaPasta = 0;
        do {
            $xml = $nfe->sefazDistDFe('AN', '1', $store->cnpj, '0', $ultNSU, $aResposta);        

            if($aResposta['cStat']=='138'){//documento localizado                    
                if($aResposta['aDoc'][0]['schema']=='procNFe_v4.00.xsd'){ // tem xml pra baixar ...
                    $xml = simplexml_load_string($aResposta['aDoc'][0]['doc']);
                    $chave = $xml->NFe->infNFe['Id'][0];
                    $accessKey = $xml->protNFe->infProt->chNFe;
                    $dhEmi = $xml->NFe->infNFe->ide->dhEmi;
                    if ($accessKey == $chaveAcessoNfe) {
                        if(strlen($chave)==47){                    
                            $xml1 = $xml->NFe->infNFe;
                            foreach($xml1->emit as $cnpj){
                                $CNPJ = $cnpj->CNPJ;
                            }

                            $parametroRow = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
                            #FAZER O TROCA EMBALAGEM AQUI
                            if($parametroRow > 0){
                                $exchangePackagings = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                                foreach ($exchangePackagings as $assoc) {
                                    foreach( $xml1->NFe->infNFe->det as $xml2){
                                        if($xml2->prod->cProd == str_replace('-','',$assoc->code) &&  $xml2->prod->uCom == $assoc->packing_of){
                                            /*
                                            * Embalagem
                                            */
                                            $xml2->prod->uCom = $assoc->packing_for;
                                            $xml2->prod->uTrib = $assoc->packing_for;
                                            /*
                                             * Custo unitario
                                             */
                                            $xml2->prod->vUnCom = floatval((floatval($xml2->prod->vUnCom)/floatval($xml2->prod->qCom))/$assoc->packing_for);
                                            $xml2->prod->vUnTrib = floatval((floatval($xml2->prod->vUnTrib)/floatval($xml2->prod->qTrib))/$assoc->packing_for);
                                            /*
                                             * Quantidade
                                             */
                                            $xml2->prod->qCom = $xml2->prod->qCom/$assoc->packing_for;
                                            $xml2->prod->qTrib = $xml2->prod->qTrib/$assoc->packing_for;
                                            $xml1->asXML('/../XML/'.$loja.'/'.$chave.'.xml');
                                        }
                                    }
                                }
                            }
                            $checkRow = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
                            #FAZER O TROCA CODIGO AQUI
                            if($checkRow > 0){
                                $exchangeCode = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                                foreach($exchangeCode as $assoc) {
                                    foreach( $xml3->det as $xml1){
                                        if($xml3->prod->cProd == str_replace('-','',$assoc->source_code)){
                                            $verificacao = true;
                                            $xml3->prod->cEAN = $assoc->destination_code;
                                            $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                                        }
                                }
                            }
                                $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                        }else{
                            $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                        }            
                    }
                }
                    $baixadaPasta ++;
                    $storeCode = (new Store())->findById($store->id);
                    $storeCode->nsu = $ultNSU;
                    $storeCodeId = $storeCode->save();
                    $ultNSU++;
                } else { //evento
                    $ultNSU++;
                }
            } else if ($aResposta['cStat'] == '109') {
                die;
            } else if ($aResposta['cStat'] == '656') {
                echo "Rejeicao: Consumo Indevido (Deve ser utilizado o ultNSU nas solicitacoes subsequentes. Tente apos 1 hora)";
            } else {
                $ultNSU++;
            }

        }while($accessKey != $chaveAcessoNfe);
        $data['success'] = true;
        $data['message'] = "$baixadaPasta NFe baixadas e $salvaBanco NFe salvas no banco de dados.";
        echo json_encode($data);
    }
    
    public function trocaCodigo(){
        $store = $_POST['store'];
        $key = $_POST['key'];
        $xml = simplexml_load_file(__DIR__."/../XML/".$store.'/NFe'.$key.'.xml');

        $cnpj = $xml->NFe->infNFe->emit->CNPJ;
        $codigos = $xml->NFe->infNFe->det;
        foreach ($codigos as $codigo) {
            $code = $codigo->prod->cProd;
            $count = (new ExchangeCode())->find("store_id = :store_id and cnpj = :cnpj and source_code = :source_code","store_id=$store&cnpj=$cnpj&source_code=$code")->count();

            if ($count > 0) {
                $exchangeCode = (new ExchangeCode())->find("store_id = :store_id and cnpj = :cnpj and source_code = :source_code","store_id=$store&cnpj=$cnpj&source_code=$code")->fetch();
                $codigo->prod->cEAN = str_replace('-','',$exchangeCode->destination_code);
                $codigo->prod->cEANTrib = str_replace('-','',$exchangeCode->destination_code);
                $xml->asXML(__DIR__."/../XML/".$store.'/NFe'.$key.'.xml');
            }
            
        }
        $parametroRow = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$cnpj&store_id=$store")->count();
        #FAZER O TROCA EMBALAGEM AQUI
        if($parametroRow > 0){
            $exchangePackagings = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$cnpj&store_id=$store")->fetch(true);
            foreach ($exchangePackagings as $assoc) {
                foreach( $xml->NFe->infNFe->det as $xml1){
                    if($xml1->prod->cProd == str_replace('-','',$assoc->code) &&  $xml1->prod->uCom == $assoc->packing_of){
                        /*
                          * Embalagem
                        */
                        $xml1->prod->uCom = $assoc->packing_for;
                        $xml1->prod->uTrib = $assoc->packing_for;
                        /*
                         * Custo unitario
                         */
                        $xml1->prod->vUnCom = floatval((floatval($xml1->prod->vUnCom)*floatval($xml1->prod->qCom))/($xml1->prod->qCom*$assoc->quantity_for));
                        $xml1->prod->vUnTrib = floatval((floatval($xml1->prod->vUnTrib)*floatval($xml1->prod->qTrib))/($xml1->prod->qTrib*$assoc->quantity_for));
                        /*
                         * Quantidade
                         */
                        $xml1->prod->qCom = $xml1->prod->qCom*$assoc->quantity_for;
                        $xml1->prod->qTrib = $xml1->prod->qTrib*$assoc->quantity_for;
                        $xml->asXML(__DIR__."/../XML/".$store.'/NFe'.$key.'.xml');
                    }
                }
            }
        }

        $data['success'] = true;
        $data['message'] = "Concluido.";
        echo json_encode($data);

    }

    public function manualTrocaCodigo(){
        $store = $_POST['store'];
        $key = $_POST['key'];
        $xml = simplexml_load_file(__DIR__."/../XML/".$store.'/NFe'.$key.'.xml');

        $cnpj = $xml->NFe->infNFe->emit->CNPJ;
        $codigos = $xml->NFe->infNFe->det;
        foreach ($codigos as $codigo) {
            $code = $codigo->prod->cProd;
            $count = (new ExchangeCode())->find("store_id = :store_id and cnpj = :cnpj and source_code = :source_code","store_id=$store&cnpj=$cnpj&source_code=$code")->count();

            if ($count > 0) {
                $exchangeCode = (new ExchangeCode())->find("store_id = :store_id and cnpj = :cnpj and source_code = :source_code","store_id=$store&cnpj=$cnpj&source_code=$code")->fetch();
                $codigo->prod->cEAN = str_replace('-','',$exchangeCode->destination_code);
                $codigo->prod->cEANTrib = str_replace('-','',$exchangeCode->destination_code);
                $xml->asXML(__DIR__."/../XML/".$store.'/NFe'.$key.'.xml');
            }
            
        }

        $data['success'] = true;
        $data['message'] = "Concluido.";
        echo json_encode($data);

    }

    public function manifestar(){
        
        $store = $_POST['store'];
        $chave = $_POST['key'];
        $xJust = $_POST['xJust'];
        $tpEvento = $_POST['evento'];
        $nfe = new ToolsNFe(__DIR__."/../StoreConfig/$store.json");
        $identification = $_COOKIE['login'];
        $user = (new User())->find("email = :login","login=$identification")->fetch();
        $nfe->sefazManifesta($chave, '1', $xJust , $tpEvento, $aResposta);

        $id = (new NfSefaz())->find("access_key = :access_key","access_key=$chave")->fetch()->id;
        if ($aResposta['evento'][0]['cStat'] != 573) {
            $lm = new LogManifestation();
            $lm->manifestation = $tpEvento;
            $lm->nf_id = $id;
            $lm->user_id = $user->id;
            $lmID = $lm->save();
            $nfSefaz = (new NfSefaz())->findById($id);
            $nfSefaz->manifestation = $tpEvento;
            $nfSefazId = $nfSefaz->save();
            $data['success'] = true;
            $data['message'] = $aResposta['evento'][0]['xMotivo'];
            echo json_encode($data);
        } else {
            $lm = new LogManifestation();
            $lm->manifestation = $tpEvento;
            $lm->nf_id = $id;
            $lm->user_id = 20000327;
            $lmID = $lm->save();
            $data['success'] = false;
            $data['message'] = $aResposta['evento'][0]['xMotivo'];
            echo json_encode($data);
        }
        
       
        
    }
    
    public function buscarNotas() {
        $stores = (new Store())->find()->order("code")->fetch(true);
        foreach ($stores as $store) {

            $nfe = new ToolsNFe(__DIR__."/../StoreConfig/$store->code.json");
            //Verificação se esta rodando todas as lojas
            // echo "<pre>";
            // var_dump($nfe);
            // echo "<pre>";
            // echo "<br>";

            
            $nsu = $store->nsu;
            $continuar = true;
            
            if (($store->cStat == '137' || $store->cStat == '589' || $store->cStat == '656') && diferencaHorasTimestamp($store->updated_at,date("Y-m-d H:i:s")) < 90) {

            } else {

                $numeroConsultas = 0;
                do {
                    if ($numeroConsultas < 20) {
                        $xml = $nfe->sefazDistDFe('AN', '1', $store->cnpj,'0', $nsu, $aResposta);
                        $numeroConsultas++;
                    
                        if ($aResposta['cStat'] == '137') {
                            //se retornar 137 registre o retorno e salve o horario.
                            $bloquear = (new Store())->findById($store->id);
                            $bloquear->cStat = '137';
                            $bloquarId = $bloquear->save();
                            $continuar = false;
                        }  else if ($aResposta['cStat']=='589') {
                            //se retornar 589 grava o NSU no banco e sai.
                            // $logger = new Logger("web");
                            // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            // $tele_channel = "1247513338";
                            // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            // $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                            // $logger->pushHandler($tele_handler);
                            // $logger->info($store->cnpj." - ".$store->company_name." - ".$aResposta['cStat']." - ".$aResposta['xMotivo']);
                            $desbloquear = (new Store())->findById($store->id);
                            $desbloquear->cStat = '589';
                            $desbloquear->nsu = $nsu;
                            $desbloquearId = $desbloquear->save();
                            $continuar = false;
                        }  else if ($aResposta['cStat']=='109') {
                            //se retornar 109 problema na SEFAZ dispara telegram xMotivo 
                            $logger = new Logger("web");
                            $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            $tele_channel = "1247513338";
                            $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                            $logger->pushHandler($tele_handler);
                            $logger->info($store->cnpj." - ".$store->company_name." - ".$aResposta['cStat']." - ".$aResposta['xMotivo']);
                            $desbloquear = (new Store())->findById($store->id);
                            $desbloquear->nsu = $nsu;
                            $desbloquearId = $desbloquear->save();
                            $continuar = false;
                        }  else if ($aResposta['cStat'] == '656') {
                            //se retornar 656 consumo indevido dispara telegram xMotivo
                            // $logger = new Logger("web");
                            // $tele_key = "1694774879:AAEMLKN6Aq93ceOw6py3O2XjZEJALLqqo1w";
                            // $tele_channel = "1247513338";
                            // $tele_handler = new TelegramBotHandler($tele_key,$tele_channel, Logger::INFO);
                            // $tele_handler->setFormatter(new LineFormatter("%level_name%: %message%"));
                            // $logger->pushHandler($tele_handler);
                            // $logger->info($store->cnpj." - ".$store->company_name." - ".$aResposta['cStat']." - ".$aResposta['xMotivo']);
                            $desbloquear = (new Store())->findById($store->id);
                            $desbloquear->cStat = '656';
                            $desbloquear->nsu = $nsu;
                            $desbloquearId = $desbloquear->save();
                            $continuar = false;
                            
                        }  else if ($aResposta['cStat'] == '138') {
                            $desbloquear = (new Store())->findById($store->id);
                            $desbloquear->cStat = '';
                            $desbloquear->nsu = $nsu;
                            $desbloquearId = $desbloquear->save();
                            foreach ($aResposta['aDoc'] as $doc) {
                                if($doc['schema'] =='resNFe_v1.00.xsd'){ // Não tem xml para baixar. Manifestar para baixar posteriormente.
                                    $chave = (simplexml_load_string($doc['doc'])->chNFe);
                                    $numeroNFE = substr($chave,25,9);
                                    $nProt = (simplexml_load_string($doc['doc'])->nProt);
                                    $dataEmissao = (simplexml_load_string($doc['doc'])->dhEmi);
                                    $cnpjEmit = (simplexml_load_string($doc['doc'])->CNPJ);
                                    $xJust = 'bispo&dantas';
                                    $tpEvento = '210210'; //ciencia da operação
                                    $aManifestacao = array();
                                    
                                    $manifestacao = $nfe->sefazManifesta($chave, 1, $xJust, $tpEvento, $aManifestacao); //Reaizar a manifestação.

                                    $row = (new NfSefaz())->find("access_key = :access_key","access_key=$chave")->count();
                                    if($row == 0){
                                        $nfeSefaz = (new NFSefaz());
                                        $nfeSefaz->access_key = $chave;
                                        $nfeSefaz->nfe_number = $numeroNFE;
                                        $nfeSefaz->protocol = $nProt;
                                        $nfeSefaz->issuer_cnpj-> $cnpjEmit;
                                        $nfeSefaz->nsu = $nsu;
                                        $nfeSefaz->manifestation = '';
                                        $nfeSefaz->store_id = $store->code;
                                        $nfeSefaz->emission_date = $dataEmissao;
                                        $nfeSefazId = $nfeSefaz->save();
                                        
                                        $lm = new LogManifestation();
                                        $lm->manifestation = $tpEvento;
                                        $lm->nf_id = $nfeSefaz->id;
                                        $lm->user_id = 147;
                                        $lmID = $lm->save();
                                    }
                                } else if($doc['schema'] =='procNFe_v4.00.xsd'){ // tem xml pra baixar ...
                                    $xml = simplexml_load_string($doc['doc']);
                                    $cnpj = $xml->NFe->infNFe->emit->CNPJ;
                                    $company_name = $xml->NFe->infNFe->emit->xNome;
                                    $fantasy_name = $xml->NFe->infNFe->emit->xFant;
                                    $state_registration = $xml->NFe->infNFe->emit->IE;
                                    $public_place = $xml->NFe->infNFe->emit->enderEmit->xLgr;
                                    $number = $xml->NFe->infNFe->emit->enderEmit->nro;
                                    $district = $xml->NFe->infNFe->emit->enderEmit->xBairro;
                                    $county_code = $xml->NFe->infNFe->emit->enderEmit->cMun;
                                    $county = $xml->NFe->infNFe->emit->enderEmit->xMun;
                                    $uf = $xml->NFe->infNFe->emit->enderEmit->UF;
                                    $zip_code = $xml->NFe->infNFe->emit->enderEmit->CEP;
                                    $country_code = $xml->NFe->infNFe->emit->enderEmit->cPais;
                                    $country = $xml->NFe->infNFe->emit->enderEmit->xPais;
                                    $phone = $xml->NFe->infNFe->emit->enderEmit->fone;
                                    $crt = $xml->NFe->infNFe->emit->CRT;
            
                                    $supplierRegistration = new SupplierRegistration();
                                    $productRegistration = new ProductRegistration();
                                    
                                    $countSupplierRegistration = $supplierRegistration->find("cnpj = :cnpj", "cnpj=$cnpj")->count();
                                    if ($countSupplierRegistration == 0) {
                                        $supplierRegistrationAdd = new SupplierRegistration();
                                        $supplierRegistrationAdd->cnpj = $cnpj;
                                        $supplierRegistrationAdd->company_name = $company_name;
                                        $supplierRegistrationAdd->fantasy_name = $fantasy_name;
                                        $supplierRegistrationAdd->state_registration = $state_registration;
                                        $supplierRegistrationAdd->public_place = $public_place;
                                        $supplierRegistrationAdd->number = $number;
                                        $supplierRegistrationAdd->district = $district;
                                        $supplierRegistrationAdd->county_code = $county_code;
                                        $supplierRegistrationAdd->county = $county;
                                        $supplierRegistrationAdd->uf = $uf;
                                        $supplierRegistrationAdd->zip_code = $zip_code;
                                        $supplierRegistrationAdd->country_code = $country_code;
                                        $supplierRegistrationAdd->country = $country;
                                        $supplierRegistrationAdd->phone = $phone;
                                        $supplierRegistrationAdd->crt = $crt;
                                        $supplierRegistrationAddSave = $supplierRegistrationAdd->save();
            
                                    } else {
                                        $supplierRegistrationAdd = (new SupplierRegistration())->find("cnpj = :cnpj", "cnpj=$cnpj")->fetch();
                                    }
                                    foreach($xml->NFe->infNFe->det as $prod){
                                        
                                        $productRegistration->store_id = $store->code;
                                        $productRegistration->reference = $prod->prod->cProd;
                                        $productRegistration->ean = $prod->prod->cEAN;
                                        $productRegistration->description = $prod->prod->xProd;
                                        $productRegistration->ncm = $prod->prod->NCM;
                                        $productRegistration->cest = $prod->prod->CEST;
                                        $productRegistration->packing = $prod->prod->uCom;
                                        $productRegistration->cost = $prod->prod->vUnCom;
                                        $productRegistration->supplier_id = $supplierRegistrationAdd->id;
                                        $productRegistrationId = $productRegistration->save();
                                    }
                                    $chave = $xml->NFe->infNFe['Id'][0];
                                    $dhEmi = $xml->NFe->infNFe->ide->dhEmi;
                                    if(strlen($chave)==47){                    
                                        $xml1 = $xml->NFe->infNFe;
                                        foreach($xml1->emit as $xml1){
                                            $CNPJ = $xml1->CNPJ;
                                        }
                                        $parametroRow = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$store->code")->count();
                                        #FAZER O TROCA EMBALAGEM AQUI
                                        if($parametroRow > 0){
                                            $exchangePackagings = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$store->code")->fetch(true);
                                            foreach ($exchangePackagings as $assoc) {
                                                foreach( $xml1->NFe->infNFe->det as $xml){
                                                    if($xml->prod->cProd == str_replace('-','',$assoc->code) &&  $xml->prod->uCom == $assoc->packing_of){
                                                        /*
                                                        * Embalagem
                                                        */
                                                        $xml->prod->uCom = $assoc->packing_for;
                                                        $xml->prod->uTrib = $assoc->packing_for;
                                                        /*
                                                         * Custo unitario
                                                         */
                                                        $xml->prod->vUnCom = floatval((floatval($xml->prod->vUnCom)/floatval($xml->prod->qCom))/$assoc->packing_for);
                                                        $xml->prod->vUnTrib = floatval((floatval($xml->prod->vUnTrib)/floatval($xml->prod->qTrib))/$assoc->packing_for);
                                                        /*
                                                         * Quantidade
                                                         */
                                                        $xml->prod->qCom = $xml->prod->qCom/$assoc->packing_for;
                                                        $xml->prod->qTrib = $xml->prod->qTrib/$assoc->packing_for;
                                                        $xml1->asXML('/../XML/'.$store->code.'/'.$chave.'.xml');
                                                    }
                                                }
                                            }
                                        }
                                        $checkRow = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$store->code")->count();
                                        #FAZER O TROCA CODIGO AQUI
                                        if($checkRow > 0){
                                            $exchangeCode = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$store->code")->fetch(true);
                                            foreach($exchangeCode as $assoc) {
                                                foreach( $xml1->det as $xml1){
                                                    if($xml1->prod->cProd == str_replace('-','',$assoc->source_code)){
                                                        $verificacao = true;
                                                        $xml1->prod->cEAN = $assoc->destination_code;
                                                        $xml = $xml->asXML(__DIR__."/../XML/".$store->code.'/'.$chave.'.xml');
                                                    }
                                                }
                                            }
                                            $xml = $xml->asXML(__DIR__."/../XML/".$store->code.'/'.$chave.'.xml');
                                        }else{
                                            $xml = $xml->asXML(__DIR__."/../XML/".$store->code.'/'.$chave.'.xml');
                                        }   
                                        if ($store->group_id == 1 || $store->group_id == 10 || $store->group_id == 2 || $store->group_id == 7) {
                                            $ok = pastaImportacaoRMS($store->code,$chave,$store->group_id);                                            
                                        }         
                                    }
                                }
                            } 
                            $nsu++;
                        }
                        if ($aResposta['maxNSU'] == $nsu) {
                            $continuar = false;
                        }
                    } else {
                        $continuar = false;
                    }
                } while ($continuar == true);
            }
            $relatorioRetorno .= "<p>Loja $store->code executada com sucesso.</p>";
            $lojasExecutadas += 1;
        }
        echo $relatorioRetorno;
        echo "<p>$lojasExecutadas Lojas executadas!</p>";

    }

    public function webServiceSearch2(){
        die;
        $stores = (new Store())->find()->order("code")->fetch(true);
        
        foreach ($stores as $store) {
            $ultNSU = $store->nsu;
            $loja = $store->code;
            $nfe = new ToolsNFe(__DIR__."/../StoreConfig/$store->code.json");
            
            $nfe->setModelo('55');
            $verification = 0;
            $salvaBanco = 0;
            $baixadaPasta = 0;
            $xml = $nfe->sefazDistDFe('AN', '1', $store->cnpj, $ultNSU, '0', $aResposta);        
    
            if($aResposta['cStat']=='138'){//documentos localizado    
                foreach ($aResposta['aDoc'] as $doc) {
                    if($doc['schema'] =='resNFe_v1.00.xsd'){ // Não tem xml para baixar. Manifestar para baixar posteriormente.
                        $chave = (simplexml_load_string($doc['doc'])->chNFe);
                        $numeroNFE = substr($chave,25,9);
                        $nProt = (simplexml_load_string($doc['doc'])->nProt);
                        $dataEmissao = (simplexml_load_string($doc['doc'])->dhEmi);
                        $cnpjEmit = (simplexml_load_string($doc['doc'])->CNPJ);
                        $xJust = 'bispo&dantas';
                        $tpEvento = '210210'; //ciencia da operação
                        $aManifestacao = array();
                        
                        $manifestacao = $nfe->sefazManifesta($chave, 1, $xJust, $tpEvento, $aManifestacao); //Reaizar a manifestação.
    
                        $row = (new NfSefaz())->find("access_key = :access_key","access_key=$chave")->count();
                        if($row == 0){
                            $nfeSefaz = (new NFSefaz());
                            $nfeSefaz->access_key = $chave;
                            $nfeSefaz->nfe_number = $numeroNFE;
                            $nfeSefaz->protocol = $nProt;
                            $nfeSefaz->issuer_cnpj-> $cnpjEmit;
                            $nfeSefaz->nsu = $ultNSU;
                            $nfeSefaz->manifestation = '';
                            $nfeSefaz->store_id = $loja;
                            $nfeSefaz->emission_date = $dataEmissao;
                            $nfeSefazId = $nfeSefaz->save();
                            if ($nfeSefazId) {
                                $salvaBanco++;
                                $storeCode = (new Store())->findById($store->id);
                                $storeCode->nsu = $ultNSU;
                                $storeCodeId = $storeCode->save();
                                $ultNSU++;
                            }
                        } else {
                            $storeCode = (new Store())->findById($store->id);
                            $storeCode->nsu = $ultNSU;
                            $storeCodeId = $storeCode->save();
                            $ultNSU++;
                        }
                    } else if($doc['schema'] =='procNFe_v4.00.xsd'){ // tem xml pra baixar ...
                        $xml = simplexml_load_string($doc['doc']);
                        $cnpj = $xml->NFe->infNFe->emit->CNPJ;
                        $company_name = $xml->NFe->infNFe->emit->xNome;
                        $fantasy_name = $xml->NFe->infNFe->emit->xFant;
                        $state_registration = $xml->NFe->infNFe->emit->IE;
                        $public_place = $xml->NFe->infNFe->emit->enderEmit->xLgr;
                        $number = $xml->NFe->infNFe->emit->enderEmit->nro;
                        $district = $xml->NFe->infNFe->emit->enderEmit->xBairro;
                        $county_code = $xml->NFe->infNFe->emit->enderEmit->cMun;
                        $county = $xml->NFe->infNFe->emit->enderEmit->xMun;
                        $uf = $xml->NFe->infNFe->emit->enderEmit->UF;
                        $zip_code = $xml->NFe->infNFe->emit->enderEmit->CEP;
                        $country_code = $xml->NFe->infNFe->emit->enderEmit->cPais;
                        $country = $xml->NFe->infNFe->emit->enderEmit->xPais;
                        $phone = $xml->NFe->infNFe->emit->enderEmit->fone;
                        $crt = $xml->NFe->infNFe->emit->CRT;

                        $supplierRegistration = new SupplierRegistration();
                        $productRegistration = new ProductRegistration();
                        
                        $countSupplierRegistration = $supplierRegistration->find("cnpj = :cnpj", "cnpj=$cnpj")->count();
                        if ($countSupplierRegistration == 0) {
                            $supplierRegistrationAdd = new SupplierRegistration();
                            $supplierRegistrationAdd->cnpj = $cnpj;
                            $supplierRegistrationAdd->company_name = $company_name;
                            $supplierRegistrationAdd->fantasy_name = $fantasy_name;
                            $supplierRegistrationAdd->state_registration = $state_registration;
                            $supplierRegistrationAdd->public_place = $public_place;
                            $supplierRegistrationAdd->number = $number;
                            $supplierRegistrationAdd->district = $district;
                            $supplierRegistrationAdd->county_code = $county_code;
                            $supplierRegistrationAdd->county = $county;
                            $supplierRegistrationAdd->uf = $uf;
                            $supplierRegistrationAdd->zip_code = $zip_code;
                            $supplierRegistrationAdd->country_code = $country_code;
                            $supplierRegistrationAdd->country = $country;
                            $supplierRegistrationAdd->phone = $phone;
                            $supplierRegistrationAdd->crt = $crt;
                            $supplierRegistrationAddSave = $supplierRegistrationAdd->save();

                        } else {
                            $supplierRegistrationAdd = (new SupplierRegistration())->find("cnpj = :cnpj", "cnpj=$cnpj")->fetch();
                        }
                        foreach($xml->NFe->infNFe->det as $prod){
                            
                            $productRegistration->store_id = $store->code;
                            $productRegistration->reference = $prod->prod->cProd;
                            $productRegistration->ean = $prod->prod->cEAN;
                            $productRegistration->description = $prod->prod->xProd;
                            $productRegistration->ncm = $prod->prod->NCM;
                            $productRegistration->cest = $prod->prod->CEST;
                            $productRegistration->packing = $prod->prod->uCom;
                            $productRegistration->cost = $prod->prod->vUnCom;
                            $productRegistration->supplier_id = $supplierRegistrationAdd->id;
                            $productRegistrationId = $productRegistration->save();
                        }
                        $chave = $xml->NFe->infNFe['Id'][0];
                        $dhEmi = $xml->NFe->infNFe->ide->dhEmi;
                        if(strlen($chave)==47){                    
                            $xml1 = $xml->NFe->infNFe;
                            foreach($xml1->emit as $xml1){
                                $CNPJ = $xml1->CNPJ;
                            }
                            $parametroRow = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
                            #FAZER O TROCA EMBALAGEM AQUI
                            if($parametroRow > 0){
                                $exchangePackagings = (new ExchangePackaging())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                                foreach ($exchangePackagings as $assoc) {
                                    foreach( $xml1->NFe->infNFe->det as $xml){
                                        if($xml->prod->cProd == str_replace('-','',$assoc->code) &&  $xml->prod->uCom == $assoc->packing_of){
                                            /*
                                            * Embalagem
                                            */
                                            $xml->prod->uCom = $assoc->packing_for;
                                            $xml->prod->uTrib = $assoc->packing_for;
                                            /*
                                             * Custo unitario
                                             */
                                            $xml->prod->vUnCom = floatval((floatval($xml->prod->vUnCom)/floatval($xml->prod->qCom))/$assoc->packing_for);
                                            $xml->prod->vUnTrib = floatval((floatval($xml->prod->vUnTrib)/floatval($xml->prod->qTrib))/$assoc->packing_for);
                                            /*
                                             * Quantidade
                                             */
                                            $xml->prod->qCom = $xml->prod->qCom/$assoc->packing_for;
                                            $xml->prod->qTrib = $xml->prod->qTrib/$assoc->packing_for;
                                            $xml1->asXML('/../XML/'.$loja.'/'.$chave.'.xml');
                                        }
                                    }
                                }
                            }
                            $checkRow = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->count();
                            #FAZER O TROCA CODIGO AQUI
                            if($checkRow > 0){
                                $exchangeCode = (new ExchangeCode())->find("cnpj = :cnpj and store_id = :store_id","cnpj=$CNPJ&store_id=$loja")->fetch(true);
                                foreach($exchangeCode as $assoc) {
                                    foreach( $xml1->det as $xml1){
                                        if($xml1->prod->cProd == str_replace('-','',$assoc->source_code)){
                                            $verificacao = true;
                                            $xml1->prod->cEAN = $assoc->destination_code;
                                            $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                                        }
                                    }
                                }
                                $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                            }else{
                                $xml = $xml->asXML(__DIR__."/../XML/".$loja.'/'.$chave.'.xml');
                            }   
                            if ($store->group_id == 1 || $store->group_id == 10) {
                                pastaImportacaoRMS($store->code,$chave);
                            }         
                        }
                        $baixadaPasta ++;
                        $storeCode = (new Store())->findById($store->id);
                        $storeCode->nsu = $ultNSU;
                        $storeCodeId = $storeCode->save();
                        $ultNSU++;
                    } else if($doc['schema'] =='resEvento_v1.00.xsd'){ //evento
                        $storeCode = (new Store())->findById($store->id);
                        $storeCode->nsu = $ultNSU;
                        $storeCodeId = $storeCode->save();
                        $ultNSU++;
                    } else if($doc['schema'] =='procEventoNFe_v1.00.xsd'){ //evento
                        $storeCode = (new Store())->findById($store->id);
                        $storeCode->nsu = $ultNSU;
                        $storeCodeId = $storeCode->save();
                        $ultNSU++;
                    }
                }                

            } else if($aResposta['cStat']=='589'){ // Maximo NSU encontrado. Salvar o ultimo Nsu e sair.
                $storeCode = (new Store())->findById($store->id);
                $storeCode->nsu = $ultNSU;
                $storeCodeId = $storeCode->save();
                $verification = 1;
            } else if($aResposta['cStat']=='137'){// Nenhum arquivo localizado par ao NSU. Gravar o NSU somar                   
                    $storeCode = (new Store())->findById($store->id);
                    $storeCode->nsu = $ultNSU;
                    $storeCodeId = $storeCode->save();
                    $ultNSU++;
            } else if ($aResposta['cStat'] == '109') {
                die;
            } else if ($aResposta['cStat'] == '656') {
                echo "$store->code Rejeicao: Consumo Indevido (Deve ser utilizado o ultNSU nas solicitacoes subsequentes. Tente apos 1 hora<br>";
            }
        }        
    }


    public function nfStart() {
        $identification = $_COOKIE['login'];
        $usuario = (new User())->find("email = :email","email=$identification")->fetch();
        $operator = $usuario->id;
        $accessKey = $_POST['accessKey'];
        $status = 1;

        $nfStarted  = (new NfStarted());

        $check = $nfStarted->find("access_key = :access_key","access_key=$accessKey")->count();

        if ($check >0) {
            $data['success'] = false;
            $data['message'] = "NFe já iniciada.";
            echo json_encode($data);
        } else {
            $nfStarted->access_key = $accessKey;
            $nfStarted->status_id = 1;
            $nfStarted->operator_id = $operator;
            $nfStartedId = $nfStarted->save();

            if($nfStartedId>0) {
                $data['success'] = true;
                $data['message'] = "NFe iniciada com sucesso!";
                echo json_encode($data);
            }
        }



    }
    
    public function started($data){
        if ($data['ids']){
            $ids = $data['ids'];
            $starteds = (new NfStarted())->find("id in ($ids)")->fetch(true);
            $status = (new NfStatus())->find()->fetch(true);
            echo $this->view->render("administrator/started", ["id" => "Painel NFe | " . SITE,
            "starteds" => $starteds,
            "status" => $status
            ]);
        } else {
            $starteds = (new NfStarted())->find()->fetch(true);
            $status = (new NfStatus())->find()->fetch(true);
            echo $this->view->render("administrator/started", ["id" => "Painel NFe | " . SITE,
            "starteds" => $starteds,
            "status" => $status
            ]);
        }

         
    }

    public function note(){
        $id = $_POST['id'];
        $note = $_POST['note'];

        $nfStarted = (new NfStarted())->findById($id);
        $nfStarted->note = $note;
        $nfStartedId = $nfStarted->save();

        if($nfStartedId){
            $return['success'] = true;
            $return['message'] = "Alteração realizada!";
            echo json_encode($return);
            return;
        } else {
            $return['success'] = false;
            $return['message'] = "Alteração não realizada!";
            echo json_encode($return);
            return;
        }
    }

    public function update(){
        $id = $_POST['id'];
        $status= $_POST['status'];
        $nfStarted = (new NfStarted())->findById($id);
        $nfStarted->status_id = $status;
        $nfStartedId = $nfStarted->save();

        if($nfStartedId){
            $return['success'] = true;
            $return['message'] = "Alteração realizada!";
            echo json_encode($return);
            return;
        } else {
            $return['success'] = false;
            $return['message'] = "Alteração não realizada!";
            echo json_encode($return);
            return;
        }
    }

    public function emitirNFCe() {      
        $nfe = new MakeNFe();
        
        $nfeTools = new ToolsNFe(__DIR__."/../StoreConfig/999.json");
        
        //Dados da NFCe - infNFe
        $cUF = '52';
        $cNF = '00000200';
        $natOp = 'Venda ao consumidor';
        $mod = '0';
        $mod  = '65';
        $serie = '1';
        $nNF = '200';
        $dhEmi = date("Y-m-d\TH:i:sP");//Formato: “AAAA-MM-DDThh:mm:ssTZD” (UTC - Universal Coordinated Time).
        $dhSaiEnt = '';//Não informar este campo para a NFC-e.
        $tpNF = '1';
        $idDest = '1';
        $cMunFG = '5208707';
        $tpImp = '4';
        $tpEmis = '1'; //normal
        $tpAmb = '2'; //homolocação
        $finNFe = '1';
        $indFinal = '1';
        $indPres = '1';
        $procEmi = '0';
        $verProc = '4.0.43';
        $dhCont = '';
        $xJust = '';
        
        $ano = date('y', strtotime($dhEmi));
        $mes = date('m', strtotime($dhEmi));
        $cnpj = $nfeTools->aConfig['cnpj'];
        $chave = $nfe->montaChave($cUF, $ano, $mes, $cnpj, $mod, $serie, $nNF, $tpEmis, $cNF);
        $versao = '4.00';
        $resp = $nfe->taginfNFe($chave, $versao);
        
        //digito verificador
        $cDV = substr($chave, -1);
        
        //tag IDE
        $resp = $nfe->tagide(
            $cUF,
            $cNF,
            $natOp,
            $mod,
            $mod,
            $serie,
            $nNF,
            $dhEmi,
            $dhSaiEnt,
            $tpNF,
            $idDest,
            $cMunFG,
            $tpImp,
            $tpEmis,
            $cDV,
            $tpAmb,
            $finNFe,
            $indFinal,
            $indPres,
            $procEmi,
            $verProc,
            $dhCont,
            $xJust
        );
        
        //Dados do emitente
        $CNPJ = $nfeTools->aConfig['cnpj'];
        $CPF = ''; // Utilizado para CPF na nota
        $xNome = $nfeTools->aConfig['razaosocial'];
        $xFant = $nfeTools->aConfig['nomefantasia'];
        $IE = $nfeTools->aConfig['ie'];
        $IEST = ''; //NFC-e não deve informar IE de Substituto Tributário
        $IM = $nfeTools->aConfig['im'];
        $CNAE = $nfeTools->aConfig['cnae'];
        $CRT = $nfeTools->aConfig['regime'];
        $resp = $nfe->tagemit($CNPJ, $CPF, $xNome, $xFant, $IE, $IEST, $IM, $CNAE, $CRT);
        
        //endereço do emitente
        $xLgr = 'Rua 144';
        $nro = '636';
        $xCpl = 'Qd. 50 Lt. 23/24, Sala 6 Res. Mendonça';
        $xBairro = 'Setor Marista';
        $cMun = '5208707';
        $xMun = 'Goiânia';
        $UF = 'GO';
        $CEP = '74170030';
        $cPais = '1058';
        $xPais = 'Brasil';
        $fone = '6241010313';
        $resp = $nfe->tagenderEmit($xLgr, $nro, $xCpl, $xBairro, $cMun, $xMun, $UF, $CEP, $cPais, $xPais, $fone);
        
        //destinatário
        /*$CNPJ = '';
        $CPF = '';
        $idEstrangeiro = '';
        $xNome = '';
        $indIEDest = '9';
        $IE = '';
        $ISUF = '';
        $IM = '';
        $email = '';
        $resp = $nfe->tagdest($CNPJ, $CPF, $idEstrangeiro, $xNome, $indIEDest, $IE, $ISUF, $IM, $email);*/
        
        //Endereço do destinatário
        /*$xLgr = '';
        $nro = '';
        $xCpl = '';
        $xBairro = '';
        $cMun = '';
        $xMun = '';
        $UF = '';
        $CEP = '';
        $cPais = '';
        $xPais = '';
        $fone = '';
        $resp = $nfe->tagenderDest($xLgr, $nro, $xCpl, $xBairro, $cMun, $xMun, $UF, $CEP, $cPais, $xPais, $fone);*/
        
        $nItem = 1;
        $cProd = '142';
        $cEAN = '97899072659522';
        $xProd = 'Chopp Originale 330ml';
        $NCM = '22030000';
        $NVE = '';
        $CEST = '0302300'; // Convênio ICMS 92/15
        $EXTIPI = '';
        $CFOP = '5405'; //CSOSN 500 = 5.405, 5.656 ou 5.667
        $uCom = 'Un';
        $qCom = '2';
        $vUnCom = '6.00';
        $vProd = '12.00';
        $cEANTrib = '';
        $uTrib = 'Un';
        $qTrib = '2';
        $vUnTrib = '6.00';
        $vFrete = '';
        $vSeg = '';
        $vDesc = '';
        $vOutro = '';
        $indTot = '1';
        $xPed = '506';
        $nItemPed = '1';
        $nFCI = '';
        $resp = $nfe->tagprod($nItem, $cProd, $cEAN, $xProd, $NCM, $EXTIPI, $CFOP, $uCom, $qCom, $vUnCom, $vProd, $cEANTrib, $uTrib, $qTrib, $vUnTrib, $vFrete, $vSeg, $vDesc, $vOutro, $indTot, $xPed, $nItemPed, $nFCI);
        
        //imposto
        $nItem = 1;
        $vTotTrib = '0.16';
        $resp = $nfe->tagimposto($nItem, $vTotTrib);
        
        //ICMS
        /*$nItem = 1;
        $orig = '0';
        $cst = '60';//Simples nacional
        $modBC = '3';
        $pRedBC = '';
        $vBC = '12.00'; // = $qTrib * $vUnTrib 
        $pICMS = '19.00'; //17% Alíquota Interna + 2% FECP
        $vICMS = '2.28'; // = $vBC * ( $pICMS / 100 )
        $vICMSDeson = '';
        $motDesICMS = '';
        $modBCST = '';
        $pMVAST = '';
        $pRedBCST = '';
        $vBCST = '';
        $pICMSST = '';
        $vICMSST = '';
        $pDif = '';
        $vICMSDif = '';
        $vICMSOp = '';
        $vBCSTRet = '';
        $vICMSSTRet = '';
        $resp = $nfe->tagICMS($nItem, $orig, $cst, $modBC, $pRedBC, $vBC, $pICMS, $vICMS, $vICMSDeson, $motDesICMS, $modBCST, $pMVAST, $pRedBCST, $vBCST, $pICMSST, $vICMSST, $pDif, $vICMSDif, $vICMSOp, $vBCSTRet, $vICMSSTRet);*/
        
        //ICMSSN - Tributação ICMS pelo Simples Nacional - CSOSN 500
        $nItem = 1;
        $orig = '0';
        $csosn = '500'; //ICMS cobrado anteriormente por substituição tributária (substituído) ou por antecipação
        $modBC = '1';
        $pRedBC = '';
        $vBC = ''; //12.00 = $qTrib * $vUnTrib 
        $pICMS = ''; //19.00 = 17% Alíquota Interna + 2% FECP
        $vICMS = ''; //2.28 = $vBC * ( $pICMS / 100 )
        $pCredSN = '';
        $vCredICMSSN = '';
        $modBCST = '';
        $pMVAST = '';
        $pRedBCST = '';
        $vBCST = '';
        $pICMSST = ''; //27.00 = GO para GO
        $vICMSST = '';
        $vBCSTRet = '12.00'; // Pauta do Chope Claro 1000ml em GO R$ 8,59 x 0.660 Litros
        $vICMSSTRet = '0.96'; // = (Valor da Pauta * Alíquota ICMS ST) - Valor ICMS Próprio
        $resp = $nfe->tagICMSSN($nItem, $orig, $csosn, $modBC, $vBC, $pRedBC, $pICMS, $vICMS, $pCredSN, $vCredICMSSN, $modBCST, $pMVAST, $pRedBCST, $vBCST, $pICMSST, $vICMSST, $vBCSTRet, $vICMSSTRet);
        
        //PIS
        $nItem = 1;
        $cst = '01'; //Operação Tributável (base de cálculo = (valor da operação * alíquota normal) / 100
        $vBC = '4.62';
        $pPIS = '0.65';
        $vPIS = '0.03';
        $qBCProd = '';
        $vAliqProd = '';
        $resp = $nfe->tagPIS($nItem, $cst, $vBC, $pPIS, $vPIS, $qBCProd, $vAliqProd);
        
        //COFINS
        $nItem = 1;
        $cst = '01'; //Operação Tributável (base de cálculo = (valor da operação * alíquota normal) / 100
        $vBC = '4.62';
        $pCOFINS = '3.00';
        $vCOFINS = '0.13';
        $qBCProd = '';
        $vAliqProd = '';
        $resp = $nfe->tagCOFINS($nItem, $cst, $vBC, $pCOFINS, $vCOFINS, $qBCProd, $vAliqProd);
        
        //Inicialização de váriaveis não declaradas...
        $vII = isset($vII) ? $vII : 0;
        $vIPI = isset($vIPI) ? $vIPI : 0;
        $vIOF = isset($vIOF) ? $vIOF : 0;
        $vPIS = isset($vPIS) ? $vPIS : 0;
        $vCOFINS = isset($vCOFINS) ? $vCOFINS : 0;
        $vICMS = isset($vICMS) ? $vICMS : 0;
        $vBCST = isset($vBCST) ? $vBCST : 0;
        $vST = isset($vST) ? $vST : 0;
        $vISS = isset($vISS) ? $vISS : 0;
        
        //total
        $vBC = '0.00';
        $vICMS = '0.00';
        $vICMSDeson = '0.00';
        $vBCST = '0.00';
        $vST = '0.00';
        $vProd = '12.00';
        $vFrete = '0.00';
        $vSeg = '0.00';
        $vDesc = '0.00';
        $vII = '0.00';
        $vIPI = '0.00';
        $vPIS = '0.03';
        $vCOFINS = '0.13';
        $vOutro = '0.00';
        $vNF = number_format($vProd-$vDesc-$vICMSDeson+$vST+$vFrete+$vSeg+$vOutro+$vII+$vIPI, 2, '.', '');
        $vTotTrib = number_format($vICMS+$vST+$vII+$vIPI+$vPIS+$vCOFINS+$vIOF+$vISS, 2, '.', '');
        $resp = $nfe->tagICMSTot($vBC, $vICMS, $vICMSDeson, $vBCST, $vST, $vProd, $vFrete, $vSeg, $vDesc, $vII, $vIPI, $vPIS, $vCOFINS, $vOutro, $vNF, $vTotTrib);
        
        //frete
        $modFrete = '9'; //Sem frete
        $resp = $nfe->tagtransp($modFrete);
        
        //pagamento
        $tPag = '01'; //Dinheiro
        $vPag = '2.00';
        $rest = $nfe->tagpag($tPag, $vPag);
        $tPag = '02'; //Cheque
        $vPag = '10.00';
        $rest = $nfe->tagpag($tPag, $vPag);
        
        // Calculo de carga tributária similar ao IBPT - Lei 12.741/12
        $federal = number_format($vII+$vIPI+$vIOF+$vPIS+$vCOFINS, 2, ',', '.');
        $estadual = number_format($vICMS+$vST, 2, ',', '.');
        $municipal = number_format($vISS, 2, ',', '.');
        $totalT = number_format($federal+$estadual+$municipal, 2, ',', '.');
        $textoIBPT = "Valor Aprox. Tributos R$ {$totalT} - {$federal} Federal, {$estadual} Estadual e {$municipal} Municipal.";
        
        //informações Adicionais
        $infAdFisco = "";
        $infCpl = "Pedido Nº506 - {$textoIBPT}";
        $resp = $nfe->taginfAdic($infAdFisco, $infCpl);
        
        //Monta a NFCe e retorna na tela
        $resp = $nfe->montaNFe();
        if ($resp) {
            $xml = $nfe->getXML();
            // $xml = simplexml_load_string($xml);
            // echodie($xml->asXML());
            //$filename = "/var/www/nfe/homologacao/entradas/{$chave}-nfe.xml"; // Ambiente Linux
            $filename = "D:/xampp/htdocs/GIT-nfephp-org/nfephp/xmls/NF-e/homologacao/entradas/{$chave}-nfe.xml"; // Ambiente Windows
            file_put_contents($filename, $xml);
            chmod($filename, 0777);    
            //Assina (e gera o QR-Code...)
            $xml = $nfeTools->assina($xml);
            //$filename = "/var/www/nfe/homologacao/assinadas/{$chave}-nfe.xml"; // Ambiente Linux
            $filename = "D:/xampp/htdocs/GIT-nfephp-org/nfephp/xmls/NF-e/homologacao/assinadas/{$chave}-nfe.xml"; // Ambiente Windows
            file_put_contents($filename, $xml);
            chmod($filename, 0777);
            if (! $nfeTools->validarXml($xml) || sizeof($nfeTools->errors)) {
                echo "<h3>Eita !?! Tem bicho na linha .... </h3>";
                foreach ($nfeTools->errors as $erro) {
                    if (is_array($erro)) { 
                        foreach ($erro as $err) {
                            echo "$err <br>";
                        }
                    } else {
                        echo "$erro <br>";
                    }
                }
                exit;
            } else {
                header('Content-type: text/xml; charset=UTF-8');
                echo $xml;
            }
        } else {
            header('Content-type: text/html; charset=UTF-8');
            foreach ($nfe->erros as $err) {
                echo 'tag: &lt;'.$err['tag'].'&gt; ---- '.$err['desc'].'<br>';
            }
        }
        
    }
}