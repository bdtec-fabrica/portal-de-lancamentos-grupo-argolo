<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\ExchangeCode;
use Source\Models\ExchangePackaging;
use Source\Models\UserClass;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use SendGrid\Mail\Mail;
use Source\Models\BoningOx;
use Source\Models\OxHistory;
use Source\Models\OxPurchaseHistory;
use Source\Models\OxDeboningHistory;
use Source\Models\BoningOxType;
use SendGrid;


class Desossa 
{
    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    public function dataUpdate() {
        $store = $_POST['store'];
        $files = $_FILES;
        
        if(!empty($files['file'])) {
            $file = $files['file'];

            if(empty($file['type'])) {
                $data['sucesso'] = false;
                $data['msg'] = "Arquivo no formato invalido".$file['type'];
                echo json_encode($data);
            } else {
                $move = move_uploaded_file($_FILES['file']['tmp_name'],"source/XML_DESOSSA/xml.xml");

                if($move){
                    $directory = __DIR__."/../XML_DESOSSA/xml.xml";
                    
                }
            }

        }
        
        $fileUpload = new File("storage", "XML_DESOSSA");

        
        $xml1 = simplexml_load_file($directory);

        $items = $xml1->NFe->infNFe->det;
        
        $count = 0;
        foreach($items as $item) {
            
            $count ++;
            $references[$count] = (string)$item->prod->cProd;
            
        }

        if (is_array($references)){
            $numberRepeatedReferences = array_count_values($references);
        }
        if (is_array($numberRepeatedReferences)){
            $repeatedReferences = array_keys($numberRepeatedReferences,2);
        }
        
        if (is_array($repeatedReferences)){
            $count = count($repeatedReferences);
        }
        
        /**
         * pegando os valores somados dos duplicados (caso haja)
         */
        for($c=0;$count>$c;$c++) {
            foreach($items as $item){
                if(strval($item->prod->cProd)==$repeatedReferences[$c]){
                    $qCom[$c] += $item->prod->qCom;
                    $vProd[$c] += $item->prod->vProd;
                    $xProd[$c]['xProd'] = (string)$item->prod->xProd;
                    $xProd[$c]['CFOP'] = (string)$item->prod->CFOP;
                    $xml1->asXML($directory);
                }  
            }
        }

        for($c=0;$count>$c;$c++){
            foreach($items as $item){
                if($item->prod->cProd == $repeatedReferences[$c]){
                    $dom = dom_import_simplexml($item);
                    $dom->parentNode->removeChild($dom);
                    
                }
            }
            foreach($items as $item){
                if($item->prod->cProd == $repeatedReferences[$c]){
                    $dom = dom_import_simplexml($item);
                    $dom->parentNode->removeChild($dom);
                    
                }
            }
        }

        for($c=0;$count>$c;$c++){
            $obj = $xml1->NFe;
            $obj = $obj->infNFe;
            $obj = $obj->addChild('det');
            $det = $obj->addChild('prod');
            $prod = $det->addChild('cProd',$repeatedReferences[$c]);
            $prod = $det->addChild('cEAN','SEM GTIN');
            $prod = $det->addChild('xProd',$xProd[$c]['xProd']);
            $prod = $det->addChild('NCM','02012090');
            $prod = $det->addChild('CFOP',$xProd[$c]['CFOP']);
            $prod = $det->addChild('uCom','KG');
            $prod = $det->addChild('qCom',$qCom[$c]);
            $prod = $det->addChild('vUnCom',$vProd[$c]/$qCom[$c]);
            $prod = $det->addChild('vProd',$vProd[$c]);
            $prod = $det->addChild('cEANTrib','SEM GTIN');
            $prod = $det->addChild('uTrib','KG');
            $prod = $det->addChild('qTrib',$qCom[$c]);
            $prod = $det->addChild('vUnTrib',$vProd[$c]/$qCom[$c]);
            $prod = $det->addChild('indTot','1');
            $det = $obj->addChild('imposto');
            $imposto = $det->addChild('ICMS');
            $ICMS = $imposto->addChild('ICMS40');
            $ICMS40 = $ICMS->addChild('orig','0');
            $ICMS40 = $ICMS->addChild('CST','40');
            $imposto = $det->addChild('IPI');
            $IPI = $imposto->addChild('cEnq','340');
            $IPI = $imposto->addChild('IPINT');
            $IPINT = $IPI->addChild('CST','52');
            $imposto = $det->addChild('PIS');
            $PIS = $imposto->addChild('PISNT');
            $PISNT = $PIS->addChild('CST','06');
            $imposto = $det->addChild('COFINS');
            $COFINS = $imposto->addChild('COFINSNT');
            $COFINSNT = $COFINS->addChild('CST','06');
            
        }  
        $xml1->asXML($directory);
        $access_key = $xml1->protNFe->infProt->chNFe;
        $issue_date = $xml1->NFe->infNFe->ide->dhEmi;


        $xml = $xml1->NFe->infNFe;
        if (!isset($xml)) {
            $xml = $xml1->infNFe;
        }
        foreach($xml->emit as $emit){
            $CNPJ = (string)$emit->CNPJ;
        }
        $ok = true;
        foreach($xml->det as $det){
            $code = $det->prod->cProd;
            $boningOx = (new BoningOx())->find("reference = :reference and store_id = :store_id and cnpj = :cnpj","reference=$code&cnpj=$CNPJ&store_id=$store")->count();

            if ($boningOx < 1){
                $ok = false;
                $n = "$n $code<br>";                    
            }
        }
        if ($ok == false) {
            $this->router->redirect("/desossa/xml/error/Xml não parametrizado para a(s) referencia(s):  $n.");
        }
        foreach($xml->total as $total){
            $custo = $total->ICMSTot->vProd;
        }
        $boningOxCount = (new BoningOx())->find("store_id = :store_id and cnpj = :cnpj","cnpj=$CNPJ&store_id=$store")->count(true);
        $boningOxs = (new BoningOx())->find("store_id = :store_id and cnpj = :cnpj","cnpj=$CNPJ&store_id=$store")->group("reference")->fetch(true);
        if ($boningOxCount == 0) {
            $this->router->redirect("/desossa/xml/error/Não parametrizado!");
        }
        foreach ($boningOxs as $boningOx) {
            $boning = (new BoningOx());
             
            if($boning->getPercente($CNPJ,$store,$boningOx->reference) >= 99.99){ 
                if($boning->getPercente($CNPJ,$store,$boningOx->reference) <= 100.01){
  
                }else{
                    $this->router->redirect("/desossa/xml/error/Percentual de 100 ultrapassado. Referencia $boningOx->reference com ".$boning->getPercente($CNPJ,$store,$boningOx->reference).".");
                }
            }else{
                $this->router->redirect("/desossa/xml/error/Percentual de 100 não alcançado. Referencia $boningOx->reference com ".$boning->getPercente($CNPJ,$store,$boningOx->reference).".");

            }
        }
        $oxHistory = (new OxHistory());
        $oxHistory->store_id = $store;
        $oxHistory->cnpj = $CNPJ;
        $oxHistory->issue_date = $issue_date;
        $oxHistory->access_key = $access_key;
        $oxHistory->directory = $directory;
        $oxHistoryId = $oxHistory->save();
        // dd($oxHistory->id);


            foreach($items as $item){
                $totalCost1 = 0;
                $totalPrice = 0;
                $oxPurchaseHistory = new OxPurchaseHistory();
                $oxPurchaseHistory->ox_history_id = $oxHistory->id;
                $oxPurchaseHistory->reference = $item->prod->cProd;
                $oxPurchaseHistory->description = $item->prod->xProd;
                $oxPurchaseHistory->cost = $item->prod->vUnCom;
                $oxPurchaseHistory->amount = $item->prod->qCom;
                $oxPurchaseHistoryId = $oxPurchaseHistory->save();
                if ($item->imposto->ICMS->ICMS40->CST == '40') {
                    $icms = 'ICMS40';
                    $cst = '40';
                } else if($item->imposto->ICMS->ICMS60->CST == '60') {
                    $icms = 'ICMS60';
                    $cst = '60';
                } else if($item->imposto->ICMS->ICMS60->CST == '41') {
                    $icms = 'ICMS41';
                    $cst = '41';
                }else if($item->imposto->ICMS->ICMS40->CST == '41') {
                    $icms = 'ICMS41';
                    $cst = '40';
                }else if($item->imposto->ICMS->ICMS51->CST == '51') {
                    $icms = 'ICMS51';
                    $cst = '51';
                }
                else if($item->imposto->ICMS->ICMS90->CST == '90') {
                    $icms = 'ICMS90';
                    $cst = '90';
                }else if($item->imposto->ICMS->ICMSSN500->CSOSN == '500') {
                    $icms = 'ICMSSN500';
                    $cst = '500';
                }else if($item->imposto->ICMS->ICMSSN102->CSOSN == '300') {
                    $icms = 'ICMSSN102';
                    $cst = '300';
                }else if($item->imposto->ICMS->ICMSSN102->CSOSN == '103') {
                    $icms = 'ICMSSN102';
                    $cst = '103';
                } else if ($item->imposto->ICMS->ICMS00->CST == '00') {
                    $icms = 'ICMS00';
                    $cst = '00';
                } else if ($item->imposto->ICMS->ICMS40->CST == '50') {
                    $icms = 'ICMS40';
                    $cst = '50';
                } else if ($item->imposto->ICMS->ICMSSN101->CSOSN == '101') {
                    $icms = 'ICMSSN101';
                    $cst = '101';
                }
                $amount = $item->prod->qCom;
                
                $reference = $item->prod->cProd;
                $cost = $item->prod->vUnCom;
                $ncm = $item->prod->NCM;
                $cfop = $item->prod->CFOP;
                $totalCost1 = (float)$item->prod->qCom * (float)$item->prod->vUnCom;
                $boningOxs = (new BoningOx())->find("reference = :reference and store_id = :store_id and cnpj = :cnpj","reference=$reference&store_id=$store&cnpj=$CNPJ")->fetch(true);
                $totalPrice = 0;
                foreach($boningOxs as $boningOx){
                    $boningOxUpdate = (new BoningOx())->findById($boningOx->id);
                    $boningOxUpdate->amount = $amount * ($boningOxUpdate->percentage/100);
                    $boningOxUpdate->cfop = $cfop;
                    $boningOxUpdate->ncm = $ncm;
                    $boningOxUpdate->icms = $icms;
                    $boningOxUpdate->cst = $cst;
                    $boningOxUpdateId = $boningOxUpdate->save();

                    
                    $totalPrice += $boningOxUpdate->getPrice * $boningOxUpdate->amount;
                    if ($totalPrice == 0) {
                        $description = $boningOxUpdate->description;
                    }

                }
                if ($totalPrice == 0) {
                    $data['sucesso'] = false;
                    $data['msg'] = "Item $description com preço zerado!";
                    echo json_encode($data);


                } else {
                    $margin = 1-$totalCost1/$totalPrice;
                }
                
                
                foreach($boningOxs as $boningOx){
                    $boningOxUpdate = (new BoningOx())->findById($boningOx->id);
                    $boningOxUpdate->margin = $margin;
                    
                    $boningOxUpdate->cost = $boningOxUpdate->getPrice * (1-$margin);
                    $boningOxUpdateId = $boningOxUpdate->save();
                    


                }
            
                $nfReferences[(string)$item->prod->cProd] = $item->prod->cProd;
            }
            
 
            $nfReferences = implode(',',$nfReferences);
            $boningOxs = (new BoningOx())->find("reference in ($nfReferences) and store_id = $store and cnpj = $CNPJ")->order('reference,description')->fetch(true);

            foreach ($boningOxs as $boningOx) {
                $oxDeboningHistory = new OxDeboningHistory();
                $oxDeboningHistory->ox_history_id = $oxHistory->id;
                $oxDeboningHistory->code = $boningOx->code;
                $oxDeboningHistory->type = $boningOx->type;
                $oxDeboningHistory->reference = $boningOx->reference;
                $oxDeboningHistory->description = $boningOx->description;
                $oxDeboningHistory->percentage = $boningOx->percentage;
                $oxDeboningHistory->amount = $boningOx->amount;
                $oxDeboningHistory->cost = $boningOx->cost;
                $oxDeboningHistory->price = $boningOx->getPrice;
                $oxDeboningHistory->margin = $boningOx->margin;
                $oxDeboningHistory->cfop = $boningOx->cfop;
                $oxDeboningHistory->ncm = $boningOx->ncm;
                $oxDeboningHistory->icms = $boningOx->icms;
                $oxDeboningHistory->cst = $boningOx->cst;

                $oxDeboningHistoryId = $oxDeboningHistory->save();

            }

            $this->router->redirect("/desossa/{$oxHistory->id}");
    }

    public function add()
    {
        $store = $_POST['store'];
        $code = $_POST['code'];
        $reference = $_POST['reference'];
        $cnpj = $_POST['cnpj'];
        $description = $_POST['description'];
        $type = $_POST['type'];
        $percentage = $_POST['percentage'];
        $price = $_POST['price'];
        $boningOx = new BoningOx();
        $boningOx->store_id = $store;
        $boningOx->code = $code;
        $boningOx->reference = $reference;
        $boningOx->cnpj = $cnpj;
        $boningOx->description = $description;
        $boningOx->type = $type;
        $boningOx->percentage = $percentage;
        $boningOx->price = $price;
        $boningOxId = $boningOx->save();

        $return['success'] = true;
        $return['message'] = "Salvo!";
        echo json_encode($return);

    }

    public function delete() {
        $id = $_POST['id'];
        $boningOx = (new BoningOx())->findById($id);
        $boningOx->destroy();

        $return['success'] = true;
        $return['message'] = "deletado!";
        echo json_encode($return);
    }

    public function copiar () {
        $ids = $_POST['ids'];
        $boningOxs = (new BoningOx())->find("id in ($ids)")->fetch(true);
        echo $this->view->render("client/desossaCopiar", ["id" => "Desossa | " . SITE,"xml" => "/source/XML_DESOSSA_NOVO/xml.xml",
        "boningOxs" => $boningOxs, "ids" => $ids
            ]);
    }

    public function copiaExec() {
        $ids = $_POST['ids'];
        
        $boningOxs = (new BoningOx())->find("id in ($ids)")->fetch(true);
        foreach ($boningOxs as $boningOx) {
            $newBoningOx = new BoningOx();
            if ($_POST['store'] != '') {
                $newBoningOx->store_id = $_POST['store'];
            } else {
                $newBoningOx->store_id = $boningOx->store_id;
            }
            
            $newBoningOx->code = $boningOx->code;
            $newBoningOx->type = $boningOx->type;
            if ($_POST['reference'] != '') {
                $newBoningOx->reference = $_POST['reference'];
            } else {
                $newBoningOx->reference = $boningOx->reference;
            }
            if ($_POST['cnpj'] != '') {
                $newBoningOx->cnpj = $_POST['cnpj'];
            } else {
                $newBoningOx->cnpj = $boningOx->cnpj;
            }
            
            $newBoningOx->description = $boningOx->description;
            $newBoningOx->percentage = $boningOx->percentage;
            $newBoningOx->amount = $boningOx->amount;
            $newBoningOx->cost = $boningOx->cost;
            $newBoningOx->price = $boningOx->price;
            $newBoningOx->margin = $boningOx->margin;
            $newBoningOx->cfop = $boningOx->cfop;
            $newBoningOx->ncm = $boningOx->ncm;
            $newBoningOx->icms = $boningOx->icms;
            $newBoningOx->cst = $boningOx->cst;
            $newBoningOxId = $newBoningOx->save();
        }
        echo "<script>window.alert('Copia realizada')</script>";
        echo "<script>window.close();</script>";
    }   

    public function desossa($data) {
        $boningOxType = (new BoningOxType())->find()->fetch(true);
        if ($data['ox']){
            $id = (integer)$data['ox'];
            $oxHistory = (new OxHistory())->findById($id);
            echo $this->view->render("client/desossa", ["id" => "Desossa | " . SITE,"id2" => $oxHistory,
            "boningOxType" => $boningOxType
                ]);
        } else if ($data['ok']) {
            echo $this->view->render("client/desossa", ["id" => "Desossa | " . SITE,"xml" => "/source/XML_DESOSSA_NOVO/xml.xml",
            "boningOxType" => $boningOxType
                ]);
        } else if ($data['error']) {
            echo $this->view->render("client/desossa", ["id" => "Desossa | " . SITE,"error" => $data['error'],
            "boningOxType" => $boningOxType
                ]);
        } else if ($data['ids']){
            $ids = $data['ids'];
            $boningOxs = (new BoningOx())->find("id in ($ids)")->fetch(true);
            echo $this->view->render("client/desossa", ["id" => "Desossa | " . SITE,"boningOxs" => $boningOxs,
            "boningOxType" => $boningOxType, "ids" => $ids ]);
        } else {
            // $boningOxs = (new BoningOx())->find()->fetch(true);
            echo $this->view->render("client/desossa", ["id" => "Desossa | " . SITE,"boningOxs" => $boningOxs,
            "boningOxType" => $boningOxType
                ]);
        }
        
      

    }

    public function convertXML() {
       
        $id = $_POST['id'];
    
        $oxHistory = (new OxHistory())->findById($id);
    
        #receber o arquivo
        $uploadFile = $oxHistory->directory;
    
        if (file_exists($uploadFile)) {

            $xml1 = simplexml_load_file($uploadFile);
    
            $xml = $xml1->NFe->infNFe;
            foreach($xml->emit as $emit){
                $CNPJ = $emit->CNPJ;
            }
            $i = 0;
            foreach($xml->det as $det){ 
                $i++;
                $referencia[$i] = $det->prod->cProd;
                
                $referencias = (integer)$referencia[$i];
                $NCM = $det->prod->NCM;
        
                    if ($det->imposto->ICMS->ICMS40->CST == 40) {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMS40->CST;
                        $ICMSS["{$det->prod->cProd}"] = "ICMS40";
                    }else if ($det->imposto->ICMS->ICMS60->CST == 60) {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMS60->CST;
                        $ICMSS["{$det->prod->cProd}"] = "ICMS60";
                    }else if ($det->imposto->ICMS->ICMS41->CST == 41) {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMS41->CST;
                        $ICMSS["{$det->prod->cProd}"] = "ICMS41";
                    }else if ($det->imposto->ICMS->ICMS40->CST == 41) {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMS40->CST;
                        $ICMSS["{$det->prod->cProd}"] = "ICMS40";
                    }else if ($det->imposto->ICMS->ICMS90->CST == 90) {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMS90->CST;
                        $ICMSS["{$det->prod->cProd}"] = "ICMS90";
                    }else if ($det->imposto->ICMS->ICMSSN500->CSOSN == 500) {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMSSN500->CSOSN;
                        $ICMSS["{$det->prod->cProd}"] = "ICMSSN500";
                    }else if ($det->imposto->ICMS->ICMSSN102->CSOSN == 103) {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMSSN102->CSOSN;
                        $ICMSS["{$det->prod->cProd}"] = "ICMSSN102";
                    } else if ($det->prod->cProd->ICMS00->CST = '00') {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMS00->CST;
                        $ICMSS["{$det->prod->cProd}"] = "ICMS00";
                    } else if ($det->prod->cProd->ICMS40->CST = '50') {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMS40->CST;
                        $ICMSS["{$det->prod->cProd}"] = "ICMS40";
                    } else if ($det->prod->cProd->ICMSSN101->CSOSN = '101') {
                        $CST["{$det->prod->cProd}"] = $det->imposto->ICMS->ICMSSN101->CSOSN;
                        $ICMSS["{$det->prod->cProd}"] = "ICMSSN101";
                    }
    
                    if ($_POST['CFOP'] == '5925') {
                        $CFOP["{$det->prod->cProd}"] = '5925';
                    } else {
                        $CFOP["{$det->prod->cProd}"] = $det->prod->CFOP;
                    }
                            
            }
            
        
        
            $l = 0;
            foreach($xml->det as $det){ 
               $l++;
                $codigos = "referencia ".$det->prod->cProd;
                $descricoes = "descricao ".$det->prod->xProd;
                $quantidades = $det->prod->qCom."KG";
                $observacoes = $observacoes.", ".$codigos.", ".$descricoes.", ".$quantidades;
        
            }
            $numeroNFE = $xml->ide->cNF;
            $dataEmi = $xml->ide->dhEmi;
            $dataEmi = substr($dataEmi,8,2)."/".substr($dataEmi,5,2)."/".substr($dataEmi,0,4);
        }
        
            $loja = $_POST['loja'];
            if (is_array($referencia)){
                $j = count($referencia);
            }
            
    
        
            $h = 0;
    
            foreach($oxHistory->getOxDeboningHistory as $piece){
                $array[$h] = $piece;
                $h++;
            }
    
            
            for($i=1;$i<=$j;$i++){
                $ref = $referencia[$i];
            }
            if (isset($cfop)){
                $CNPJ = $xml1->NFe->infNFe->emit->CNPJ;
                unset($xml1->NFe->infNFe->infAdic);
                $xml1->asXML($uploadFile);
                $obj = $xml1->NFe;
                $obj = $obj->infNFe;
                $infNFe = $obj->addChild('infAdic');
                $infAdic = $infNFe->addChild('infCpl',
                "NFe emitida atraves do processo de desagregacao dos itens ".$observacoes.", referente a NFe ".$numeroNFE." , CNJPJ fornecedor ".$CNPJ.", com data de emissao ".$dataEmi.".");
                unset($xml1->NFe->infNFe->emit);
                $xml1->asXML($uploadFile);
                $obj = $obj->addChild('emit');
                $emit = $obj->addChild('CNPJ',$xml1->NFe->infNFe->dest->CNPJ);
                $emit = $obj->addChild('xNome',$xml1->NFe->infNFe->dest->xNome);
                $emit = $obj->addChild('xFant',$xml1->NFe->infNFe->dest->xNome);
                $emit = $obj->addChild('enderEmit');
                $enderEmit = $emit->addChild('xLgr',$xml1->NFe->infNFe->dest->enderDest->xLgr);
                $enderEmit = $emit->addChild('nro',$xml1->NFe->infNFe->dest->enderDest->nro);
                $enderEmit = $emit->addChild('xBairro',$xml1->NFe->infNFe->dest->enderDest->xBairro);
                $enderEmit = $emit->addChild('cMun',$xml1->NFe->infNFe->dest->enderDest->cMun);
                $enderEmit = $emit->addChild('xMun',$xml1->NFe->infNFe->dest->enderDest->xMun);
                $enderEmit = $emit->addChild('UF',$xml1->NFe->infNFe->dest->enderDest->UF);
                $enderEmit = $emit->addChild('CEP',$xml1->NFe->infNFe->dest->enderDest->CEP);
                $enderEmit = $emit->addChild('cPais',$xml1->NFe->infNFe->dest->enderDest->cPais);
                $enderEmit = $emit->addChild('xPais',$xml1->NFe->infNFe->dest->enderDest->xPais);
                $enderEmit = $emit->addChild('fone','7132883557');
                $emit = $obj->addChild('IE',$xml1->NFe->infNFe->dest->IE);
                $emit = $obj->addChild('CRT','3');
                
        
        
        
        
            }
        
            for($i=1;$i<=$j;$i++){
                unset($xml1->NFe->infNFe->det);
                $xml1->asXML($uploadFile);
                
            }
        
            for($k=0;$k<$h;$k++){ 
                $obj = $xml1->NFe;
                $obj = $obj->infNFe;
                $obj = $obj->addChild('det');
                $det = $obj->addChild('prod');
                $prod = $det->addChild('cProd',$array[$k]->code);
                $prod = $det->addChild('cEAN',$array[$k]->code);
                $prod = $det->addChild('xProd',$array[$k]->description);
                $prod = $det->addChild('NCM',$array[$k]->ncm);
                if($_POST['CFOP'] == '5925'){
                    $prod = $det->addChild('CFOP','5925');
                }else{
                    $prod = $det->addChild('CFOP',$array[$k]->cfop);
                }
                $prod = $det->addChild('uCom','KG');
                $prod = $det->addChild('qCom',$array[$k]->amount);
                $prod = $det->addChild('vUnCom',$array[$k]->cost);
                $prod = $det->addChild('vProd',($array[$k]->amount*$array[$k]->cost));
                $prod = $det->addChild('cEANTrib',$array[$k]->code);
                $prod = $det->addChild('uTrib','KG');
                $prod = $det->addChild('qTrib',$array[$k]->amount);
                $prod = $det->addChild('vUnTrib',$array[$k]->cost);
                $prod = $det->addChild('indTot','1');
                $det = $obj->addChild('imposto');
                $imposto = $det->addChild('ICMS');
                $ICMSCST = $array[$k]->icms;
                
                $ICMS = $imposto->addChild("$ICMSCST");
                $ICMS40 = $ICMS->addChild('orig','0');
                $ICMS40 = $ICMS->addChild('CST',$array[$k]->cst);
                $imposto = $det->addChild('IPI');
                $IPI = $imposto->addChild('cEnq','340');
                $IPI = $imposto->addChild('IPINT');
                $IPINT = $IPI->addChild('CST','52');
                $imposto = $det->addChild('PIS');
                $PIS = $imposto->addChild('PISNT');
                $PISNT = $PIS->addChild('CST','06');
                $imposto = $det->addChild('COFINS');
                $COFINS = $imposto->addChild('COFINSNT');
                $COFINSNT = $COFINS->addChild('CST','06'); 
                }
                $new = $uploadFile;
                if($xml1->asXML("source/XML_DESOSSA_NOVO/xml.xml")){
                    $this->router->redirect("/desossa/xml/ok");
                } else {
    
                    echo "imita uma galinha";
                }
    }

    public function searchDesossa() {
        $store = $_POST['store'];
        $code = $_POST['code'];
        $description = $_POST['description'];
        $reference = $_POST['reference'];
        $cnpj = $_POST['cnpj'];
        $type = $_POST['type'];

        if ($store !='') {
            $params['store_id'] = $store; 
            $comparation[1] = " store_id = :store_id";
        }

        if ($type !='') {
            $params['type'] = $type; 
            $comparation[6] = " type = :type";
        }

        if ($code !='') {
            $params['code'] = $code; 
            $comparation[2] = " code = :code";
        }
        if ($description !='') {
            $params['description'] = $description; 
            $comparation[3] = " description like %:description%";
        }
        if ($reference !='') {
            $params['reference'] = $reference; 
            $comparation[4] = " reference = :reference";
        }
        if ($cnpj !='') {
            $params['cnpj'] = $cnpj; 
            $comparation[5] = " cnpj = :cnpj";
        }
        $comparation = implode(' and ', $comparation);
        $params = http_build_query($params);
        $boningOx = (new BoningOx())->find($comparation,$params)->fetch(true);

        if ($boningOx) {
            foreach ($boningOx as $bx) {
                $id[$bx->id] = $bx->id;
            }
            $id = implode(",",$id);
        } else {
            $id = '-1';
        }
        $this->router->redirect("/desossa/search/$id");

    }

    public function desossaEdit() {
        $total = strlen($_GET['data']);
        $traco = strpos($_GET['data'],"-");
        $real = strpos($_GET['data'],"$");
        $id = (integer) substr($_GET['data'],0,$traco);
        $campo = substr($_GET['data'],$traco+1);
        $valor = substr($_GET['data'],$real+1);

        if (substr($campo,0,10) == "percentage") {

            $blocoH = (new BoningOx())->findById($id);
            $blocoH->percentage = (float) $valor;
            $blocoHId = $blocoH->save();

        } else if (substr($campo,0,5) == "price") {
            $blocoH = (new BoningOx())->findById($id);
            $blocoH->price = (float) $valor;
            $blocoHId = $blocoH->save();
        }
        $return['sucesso'] = true;
        $return['msg'] = "Processo Concluido!";
        echo json_encode($return);
    }
    
}