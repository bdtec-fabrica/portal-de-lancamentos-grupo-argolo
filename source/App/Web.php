<?php

namespace Source\App;

use League\Plates\Engine;
use CoffeeCode\Paginator\Paginator;
use CoffeeCode\Router\Router;
use CoffeeCode\Uploader\Image;
use CoffeeCode\Uploader\File;
use Source\Models\FiscalMovement;
use Source\Models\FiscalRegister;
use Source\Models\User;
use Source\Models\UserStoreGroup;
use Source\Models\UserSector;
use Source\Models\Request;
use Source\Models\Call;
use Source\Models\CallAttachment;
use Source\Models\Interaction;
use Source\Models\InteractionAttachment;
use Source\Models\RequestAttachment;
use Source\Models\Sector;
use Source\Models\StoreGroup;
use Source\Models\Store;
use Source\Models\Status;
use Source\Models\Criticism;
use Source\Models\Counting;
use Source\Models\StockMovement;
use Source\Models\Session;
use Source\Models\RequestScore;
use Source\Models\UserClass;
use Source\Models\Inventory;
use Source\Models\BlocoH;
use Source\Models\Marketing;
use Source\Models\Xml;
use Monolog\Logger;
use Monolog\Handler\SendGridHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Formatter\LineFormatter;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SendGrid\Mail\Mail;
use SendGrid;
use Symfony\Component\DomCrawler\Crawler;
use DOMDocument;
use Source\Models\RequestBlock;
use Source\Models\DailyClosing;



class Web 
{
    private $view;

    public function __construct($router) {
        $this->router = $router;
        $this->view = Engine::create(__DIR__ . "/../../themes", "php");
    }

    /**
     * LOGIN
     */
    public function login($data){
        $storeGroup = (new StoreGroup())->find()->fetch(true);
        if ($data){
            echo $this->view->render("web/login", ["title" => "Login | " . SITE,"erro" => "erro", "storeGroups" => $storeGroup]);
        } else {
            echo $this->view->render("web/login", ["title" => "Login | " . SITE, "storeGroups" => $storeGroup]);
        }
        
    }

    public function doLogin() {
        
        $login = addslashes($_POST['login']);
        $password = addslashes($_POST['password']);
        $user = new User();
        $count = $user->find("email = :login", "login=$login")->count();
        if ($count == 1) {
            $params = http_build_query(["login" => $login, "password" => $password]);
            $contador = $user->find("email = :login AND password = :password", $params)->count();
            if ($contador == 1) {
                $usuario = $user->find("email = :login AND password = :password", $params)->fetch();
                setcookie("class", $usuario->class, time()+86400,"/.","bdtecsolucoes.com.br");
                setcookie("id", $usuario->id, time()+86400,"/","bdtecsolucoes.com.br");
                setcookie("login", $usuario->email, time()+86400,"/","bdtecsolucoes.com.br");
                setcookie("store", $usuario->store_code, time()+86400,"/","bdtecsolucoes.com.br");
                $this->router->redirect("/");                
            } else {
                $data = "erro";
                $this->router->redirect("/login/{$data}");
            }
        } else {
            $data = "erro";
            $this->router->redirect("/login/{$data}");
        }
    }

    public function doLogout() {
        setcookie("class", $usuario->class, 1);
        setcookie("id", $usuario->id, 1);
        setcookie("login", $usuario->email, 1);
        setcookie("store", $usuario->store_code, 1);
        $this->router->redirect("/login");
    }

    public function forgotPassword() {
        $email2 = $_POST['email'];

        $user = new User();
        $check = $user->find("email = :email","email={$email2}")->count();

        if ($check >0) {
            $userPassword = $user->find("email = :email","email={$email2}")->fetch();
            $password = $userPassword->password;
            $name = $userPassword->name;
            $email = new Mail();
            $email->setFrom("no-reply@bispoedantas.com.br", "Não responda");
            $email->setSubject("Envio de senha BDTecAtende");
            $email->addTo("$email2", "$name");
            $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
            $email->addContent(
            "text/html", "<h5>Olá $name</h5><p>Esta é a sua senha BDTec Atende:  <strong>$password</strong><p>"
            );
            // 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
            $sendgrid = new SendGrid('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');
            try {
                $response = $sendgrid->send($email);
                // print $response->statusCode() . "\n";
                // print_r($response->headers());
                // print $response->body() . "\n";
            } catch (Exception $e) {
                echo 'Caught exception: '. $e->getMessage() ."\n";
            }

            $return['success'] = true;
            $return['message'] = "Enviado e-mail com a sua senha para $email2.";
            echo json_encode($return);
             
        } else {
            $return['success'] = false;
            $return['message'] = 'Não foi encontrado nenhum cadastro para este e-mail.';
            echo json_encode($return);
        }
    }

    /**
     * ERRO
     */
    public function error($data) {
        echo "<h1>Erro {$data["errcode"]}</h1>";
    }
}