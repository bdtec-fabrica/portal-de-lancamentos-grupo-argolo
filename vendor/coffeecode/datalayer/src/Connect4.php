<?php

namespace CoffeeCode\DataLayer;

use PDO;
use PDOException;

/**
 * Class Connect
 * @package CoffeeCode\DataLayer
 */
class Connect4
{
    /** @var PDO */
    private static $instance;

    /** @var PDOException */
    private static $error;

    /**
     * @return PDO
     */
    public static function getInstance(): ?PDO
    {
        if (empty(self::$instance)) {
            try {
                self::$instance = new PDO(
                    // mysql:host=localhost;dbname=testdb
                    //oci:dbname=//localhost:1521/mydb
                    DATA_LAYER_CONFIG4["driver"] . ":dbname=" . DATA_LAYER_CONFIG4["host"] . ":" . DATA_LAYER_CONFIG4["port"] . "/" . DATA_LAYER_CONFIG4["dbname"],
                    DATA_LAYER_CONFIG4["username"],
                    DATA_LAYER_CONFIG4["passwd"]
                    // ,
                    // DATA_LAYER_CONFIG["options"]
                );
            } catch (PDOException $exception) {
                self::$error = $exception;
            }
        }
        return self::$instance;
    }

    public static function getInstance2(): ?PDO
    {
        if (empty(self::$instance)) {
            try {
                self::$instance = new PDO(
                    DATA_LAYER_CONFIG4["driver"] . ":host=" . DATA_LAYER_CONFIG4["host"] . ";dbname=" . DATA_LAYER_CONFIG4["dbname"] . ";port=" . DATA_LAYER_CONFIG4["port"],
                    DATA_LAYER_CONFIG4["username"],
                    DATA_LAYER_CONFIG4["passwd"],
                    DATA_LAYER_CONFIG4["options"]
                );
            } catch (PDOException $exception) {
                self::$error = $exception;
            }
        }
        return self::$instance;
    }


    /**
     * @return PDOException|null
     */
    public static function getError(): ?PDOException
    {
        return self::$error;
    }

    /**
     * Connect constructor.
     */
    private function __construct()
    {
    }

    /**
     * Connect clone.
     */
    private function __clone()
    {
    }
}
