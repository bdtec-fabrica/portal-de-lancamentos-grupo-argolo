<?php

namespace CoffeeCode\DataLayer;

use PDO;
use PDOException;

/**
 * Class Connect
 * @package CoffeeCode\DataLayer
 */
class Connect752
{
    /** @var PDO */
    private static $instance;

    /** @var PDOException */
    private static $error;

    /**
     * @return PDO
     */
    public static function getInstance(): ?PDO
    {
        if (empty(self::$instance)) {
            try {
                self::$instance = new PDO(
                    DATA_LAYER_CONFIG752["driver"] . ":host=" . DATA_LAYER_CONFIG752["host"] . ";dbname=" . DATA_LAYER_CONFIG752["dbname"] . ";port=" . DATA_LAYER_CONFIG752["port"],
                    DATA_LAYER_CONFIG752["username"],
                    DATA_LAYER_CONFIG752["passwd"],
                    DATA_LAYER_CONFIG752["options"]
                );
            } catch (PDOException $exception) {
                self::$error = $exception;
            }
        }
        return self::$instance;
    }


    /**
     * @return PDOException|null
     */
    public static function getError(): ?PDOException
    {
        return self::$error;
    }

    /**
     * Connect constructor.
     */
    private function __construct()
    {
    }

    /**
     * Connect clone.
     */
    private function __clone()
    {
    }
}
