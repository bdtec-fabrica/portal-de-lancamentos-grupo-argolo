<?php

namespace CoffeeCode\DataLayer;

use PDO;
use PDOException;

/**
 * Class Connect
 * @package CoffeeCode\DataLayer
 */
class Connect2
{
    /** @var PDO */
    private static $instance;

    /** @var PDOException */
    private static $error;

    /**
     * @return PDO
     */
    public static function getInstance($verifique = null): ?PDO
    {
        if ($verifique) {
            if ($verifique == 1) {
                if (empty(self::$instance)) {
                    try {
                        self::$instance = new PDO(
                            // mysql:host=localhost;dbname=testdb
                            //oci:dbname=//localhost:1521/mydb
                            DATA_LAYER_CONFIG2["driver"] . ":dbname=" . DATA_LAYER_CONFIG2["host"] . ":" . DATA_LAYER_CONFIG2["port"] . "/" . DATA_LAYER_CONFIG2["dbname"],
                            DATA_LAYER_CONFIG2["username"],
                            DATA_LAYER_CONFIG2["passwd"]
                            // ,
                            // DATA_LAYER_CONFIG["options"]
                        );
                    } catch (PDOException $exception) {
                        self::$error = $exception;
                    }
                }
            } else if ($verifique == 2) {
                if (empty(self::$instance)) {
                    try {
                        self::$instance = new PDO(
                            // mysql:host=localhost;dbname=testdb
                            //oci:dbname=//localhost:1521/mydb
                            DATA_LAYER_CONFIG3["driver"] . ":dbname=" . DATA_LAYER_CONFIG3["host"] . ":" . DATA_LAYER_CONFIG3["port"] . "/" . DATA_LAYER_CONFIG3["dbname"],
                            DATA_LAYER_CONFIG3["username"],
                            DATA_LAYER_CONFIG3["passwd"]
                            // ,
                            // DATA_LAYER_CONFIG["options"]
                        );
                    } catch (PDOException $exception) {
                        self::$error = $exception;
                    }
                }
            } else if ($verifique == 10) {
                if (empty(self::$instance)) {
                    try {
                        self::$instance = new PDO(
                            // mysql:host=localhost;dbname=testdb
                            //oci:dbname=//localhost:1521/mydb
                            DATA_LAYER_CONFIG10["driver"] . ":dbname=" . DATA_LAYER_CONFIG10["host"] . ":" . DATA_LAYER_CONFIG10["port"] . "/" . DATA_LAYER_CONFIG10["dbname"],
                            DATA_LAYER_CONFIG10["username"],
                            DATA_LAYER_CONFIG10["passwd"]
                            // ,
                            // DATA_LAYER_CONFIG["options"]
                        );
                    } catch (PDOException $exception) {
                        self::$error = $exception;
                    }
                }
            }

        } else {
            if (empty(self::$instance)) {
                try {
                    self::$instance = new PDO(
                        // mysql:host=localhost;dbname=testdb
                        //oci:dbname=//localhost:1521/mydb
                        DATA_LAYER_CONFIG2["driver"] . ":dbname=" . DATA_LAYER_CONFIG2["host"] . ":" . DATA_LAYER_CONFIG2["port"] . "/" . DATA_LAYER_CONFIG2["dbname"],
                        DATA_LAYER_CONFIG2["username"],
                        DATA_LAYER_CONFIG2["passwd"]
                        // ,
                        // DATA_LAYER_CONFIG["options"]
                    );
                } catch (PDOException $exception) {
                    self::$error = $exception;
                }
            }
        }
        if (empty(self::$instance)) {
            try {
                self::$instance = new PDO(
                    // mysql:host=localhost;dbname=testdb
                    //oci:dbname=//localhost:1521/mydb
                    DATA_LAYER_CONFIG2["driver"] . ":dbname=" . DATA_LAYER_CONFIG2["host"] . ":" . DATA_LAYER_CONFIG2["port"] . "/" . DATA_LAYER_CONFIG2["dbname"],
                    DATA_LAYER_CONFIG2["username"],
                    DATA_LAYER_CONFIG2["passwd"]
                    // ,
                    // DATA_LAYER_CONFIG["options"]
                );
            } catch (PDOException $exception) {
                self::$error = $exception;
            }
        }
        return self::$instance;
    }

    public static function getInstance2(): ?PDO
    {
        if (empty(self::$instance)) {
            try {
                self::$instance = new PDO(
                    DATA_LAYER_CONFIG2["driver"] . ":host=" . DATA_LAYER_CONFIG2["host"] . ";dbname=" . DATA_LAYER_CONFIG2["dbname"] . ";port=" . DATA_LAYER_CONFIG2["port"],
                    DATA_LAYER_CONFIG2["username"],
                    DATA_LAYER_CONFIG2["passwd"],
                    DATA_LAYER_CONFIG2["options"]
                );
            } catch (PDOException $exception) {
                self::$error = $exception;
            }
        }
        return self::$instance;
    }


    /**
     * @return PDOException|null
     */
    public static function getError(): ?PDOException
    {
        return self::$error;
    }

    /**
     * Connect constructor.
     */
    private function __construct()
    {
    }

    /**
     * Connect clone.
     */
    private function __clone()
    {
    }
}
