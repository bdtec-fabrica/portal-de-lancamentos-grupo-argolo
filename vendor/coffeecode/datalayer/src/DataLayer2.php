<?php

namespace CoffeeCode\DataLayer;

use Exception;
use PDO;
use PDOException;
use Source\Models\AA2CESTQ;
use stdClass;

/**
 * Class DataLayer2
 * @package CoffeeCode\DataLayer2
 */
abstract class DataLayer2
{
    use CrudTrait2;

    /** @var string $entity database table */
    private $entity;

    /** @var string $primary table primary key field */
    private $primary;

    /** @var array $required table required fields */
    private $required;

    /** @var string $timestamps control created and updated at */
    private $timestamps;

    /** @var string */
    protected $statement;

    /** @var string */
    protected $params;

    /** @var string */
    protected $group;

    /** @var string */
    protected $order;

    /** @var int */
    protected $limit;

    /** @var int */
    protected $offset;

    /** @var \PDOException|null */
    protected $fail;

    /** @var object|null */
    protected $data;
    protected $verifique;
    /**
     * DataLayer2 constructor.
     * @param string $entity
     * @param array $required
     * @param string $primary
     * @param bool $timestamps
     */
    public function __construct(string $entity, array $required, string $primary = 'id', bool $timestamps = true, $verifique = null)
    {
        $this->entity = $entity;
        $this->primary = $primary;
        $this->required = $required;
        $this->timestamps = $timestamps;
        $this->verifique = $verifique;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (empty($this->data)) {
            $this->data = new stdClass();
        }

        $this->data->$name = $value;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data->$name);
    }

    /**
     * @param $name
     * @return string|null
     */
    public function __get($name)
    {
        $method = $this->toCamelCase($name);
        if (method_exists($this, $method)) {
            return $this->$method();
        }

        if (method_exists($this, $name)) {
            return $this->$name();
        }

        return ($this->data->$name ?? null);
    }

    /*
    * @return PDO mode
    */
    public function columns($mode = PDO::FETCH_OBJ)
    {
        $stmt = Connect2::getInstance($this->verifique)->prepare("DESCRIBE {$this->entity}");
        $stmt->execute($this->params);
        return $stmt->fetchAll($mode);
    }


    /**
     * @return object|null
     */
    public function data(): ?object
    {
        return $this->data;
    }

    /**
     * @return PDOException|Exception|null
     */
    public function fail()
    {
        return $this->fail;
    }

    /**
     * @param string|null $terms
     * @param string|null $params
     * @param string $columns
     * @return DataLayer2
     */
    public function find(?string $terms = null, ?string $params = null, string $columns = "*"): DataLayer2
    {
        if ($terms) {
            $this->statement = "SELECT {$columns} FROM {$this->entity} WHERE {$terms}";
            parse_str($params, $this->params);
            return $this;
        }

        $this->statement = "SELECT {$columns} FROM {$this->entity}";
        return $this;
    }

    /**
     * @param int $id
     * @param string $columns
     * @return DataLayer2|null
     */
    public function findById(int $id, string $columns = "*"): ?DataLayer2
    {
        return $this->find("{$this->primary} = :id", "id={$id}", $columns)->fetch();
    }

    /**
     * @param string $column
     * @return DataLayer2|null
     */
    public function group(string $column): ?DataLayer2
    {
        $this->group = " GROUP BY {$column}";
        return $this;
    }

    /**
     * @param string $columnOrder
     * @return DataLayer2|null
     */
    public function order(string $columnOrder): ?DataLayer2
    {
        $this->order = " ORDER BY {$columnOrder}";
        return $this;
    }

    /**
     * @param int $limit
     * @return DataLayer2|null
     */
    public function limit(int $limit): ?DataLayer2
    {
        $this->limit = " LIMIT {$limit}";
        return $this;
    }

    /**
     * @param int $offset
     * @return DataLayer2|null
     */
    public function offset(int $offset): ?DataLayer2
    {
        $this->offset = " OFFSET {$offset}";
        return $this;
    }

    /**
     * @param bool $all
     * @return array|mixed|null
     */
    public function fetch(bool $all = false)
    {
        try {
            if ($this->verifique == 1 ) {
                $stmt = Connect2::getInstance()->prepare($this->statement . $this->group . $this->order . $this->limit . $this->offset);
            } else if ($this->verifique == 2) {
                $stmt = Connect3::getInstance()->prepare($this->statement . $this->group . $this->order . $this->limit . $this->offset);
            } else if ($this->verifique == 10) {
                $stmt = Connect10::getInstance()->prepare($this->statement . $this->group . $this->order . $this->limit . $this->offset);
            } else {
                $stmt = Connect2::getInstance($this->verifique)->prepare($this->statement . $this->group . $this->order . $this->limit . $this->offset);
            }
            
            $stmt->execute($this->params);
            
            
            // if (!$stmt->rowCount()) {
            //     return null;
            // }
            
            if ($all) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, static::class);
            }

            return $stmt->fetchObject(static::class);
        } catch (PDOException $exception) {
            $this->fail = $exception;
            return $exception;
        }
    }

    /**
     * @return int
     */
    public function count(): int
    {
        $stmt = Connect2::getInstance($this->verifique)->prepare($this->statement);
        $stmt->execute($this->params);
        return $stmt->rowCount();
    }

    public function execute2($DET_LOJA,$DET_TIPO,$DET_NOTA,$DET_SERIE,$DET_LOJA_1,$DET_VALOR,$CNPJ)
    {        
        try {     
            
            $loja = substr($DET_LOJA,0,strlen($DET_LOJA)-1);
            $stmt1 = Connect2::getInstance($this->verifique)->prepare(
            "declare
            store1 float;
            store2 string(254);
            i2 string(255);
            begin
            -- Call the procedure
            pc_rcp_nf.proc_dobra_nf_receb($DET_LOJA,1".date("ymd").",$DET_LOJA_1,$DET_TIPO,$DET_NOTA,'1  ','VGRMMENU',0,store1,store2,i2);
            DBMS_OUTPUT.PUT_LINE(i2);
            commit;
            end;");

            $stmt1->execute();

            $stmt2 = Connect2::getInstance($this->verifique)->prepare(
                "declare
                store1 string(254);
                store2 string(254);
                i2     string(255);
              begin
                -- Call the procedure
                PC_RMS_RCP.PROC_GERA_NFE_ENTRADA($loja,
                                                 $DET_LOJA_1,
                                                 $CNPJ,
                                                 $DET_TIPO,
                                                 1".date("ymd").",
                                                 $DET_NOTA,
                                                 '1  ',
                                                 i2,
                                                 'I',
                                                 'RCP',
                                                 store2);
                DBMS_OUTPUT.PUT_LINE(store2);
                commit;
              end;");
    
                $stmt2->execute();

                $stmt3 = Connect2::getInstance($this->verifique)->prepare("INSERT INTO  AA2AFRNT (FRNT_FORN,FRNT_NOTA,FRNT_SERIE) VALUES ($DET_LOJA_1,$DET_NOTA,'1  ');
                commit;");
        
                $stmt3->execute();



        } catch (PDOException $exception) {
            $this->fail = $exception;
            return $this->fail;
        }
    }

    public function aa2cestq($store,$code,$amount,$negative,$type) {
        echo "ALTER TRIGGER TRG_AA2CESTQ DISABLE<br>";
        $stmt = Connect2::getInstance($this->verifique)->prepare("ALTER TRIGGER TRG_AA2CESTQ DISABLE");
        try {
            $stmt->execute();
        } catch (PDOException $pdo) {
            dd($pdo);
        }
        /**
         * Se $type === true quer dizer que estamos no segundo passo que é remover do estoque a quantidade
         * que foi acrescentada para o cancelamento.
         */
        if ($type === true) { 
            $aa2cestq = (new AA2CESTQ())->find("GET_COD_PRODUTO = $code and GET_COD_LOCAL = $store")->fetch();
            /**
             * Se o estoque após o relançamento ainda for zero (0) o item não estara incluso no relançamento da nota.
             */
            if ($aa2cestq->GET_ESTOQUE == 0) {
                /**
                 * Se havia estoque negativo antes do processo iremos informar o estoque zerado e somar a quantidade
                 * que foi adicionada para cancelamento ao estoque negativo
                 */
                if ($negative > 0) {
                    $stmt = Connect2::getInstance($this->verifique)->prepare("UPDATE AA2CESTQ SET GET_ESTOQUE = 0, GET_QTD_PEND_VDA = $amount+$negative WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store");
                    echo "UPDATE AA2CESTQ SET GET_ESTOQUE = 0, GET_QTD_PEND_VDA = $amount+$negative WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store<br>";
                /**
                 * Se não havia estoque negativo antes do processo iremos informar o estoque zerado e a quantidade
                 * que foi adicionada para cancelamento será o estoque negativo
                 */
                } else {
                    $stmt = Connect2::getInstance($this->verifique)->prepare("UPDATE AA2CESTQ SET GET_ESTOQUE = 0, GET_QTD_PEND_VDA = $amount WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store");
                    echo "UPDATE AA2CESTQ SET GET_ESTOQUE = 0, GET_QTD_PEND_VDA = $amount WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store<br>";
                }  
            /**
             * Se o estoque após relançamento for > 0 quer dizer que o item foi relançado.
             */              
            } else if ($aa2cestq->GET_ESTOQUE > 0) {
                /**
                 * Caso antes do processo o produto tenha estoque negativo vamos verificar se essa quantidade é maior que o 
                 * estoque pós relançamento
                 */
                if ($negative > 0) {
                    /**
                     * Se a quantidade de estoque for menor que a quantidade que foi acrescentada para
                     * cancelamento + o estoque negativo anterior vamos zerar o estoque e colocar a quantidade
                     * restante ($amount + $negative - GET_ESTOQUE) no valor negativo
                     */
                    if ($aa2cestq->GET_ESTOQUE < $amount+$negative) {
                        $stmt = Connect2::getInstance($this->verifique)->prepare("UPDATE AA2CESTQ SET GET_ESTOQUE = 0, GET_QTD_PEND_VDA = ($amount  + $negative) - GET_ESTOQUE WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store");
                        echo "UPDATE AA2CESTQ SET GET_ESTOQUE = 0, GET_QTD_PEND_VDA = ($amount  + $negative) - GET_ESTOQUE WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store<br>";
                    /**
                     * Se a quantidade de estoque for maior ou igual a soma do estoque negativo + a quantidade acrescentada
                     * para o cancelamento iremos colocar no estoque o valor restante (GET_ESTOQUE - $amount + $negative) e 
                     * zerar o estoque negativo
                     */
                    } else {
                        $stmt = Connect2::getInstance($this->verifique)->prepare("UPDATE AA2CESTQ SET GET_ESTOQUE = GET_ESTOQUE - ($amount + $negative), GET_QTD_PEND_VDA = 0 WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store");
                        echo "UPDATE AA2CESTQ SET GET_ESTOQUE = GET_ESTOQUE - ($amount + $negative), GET_QTD_PEND_VDA = 0 WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store<br>";
                    }                  
                } else {
                /**
                 * Caso antes do processo não haja quantidade negativa vamos verificar se a quatidade que foi 
                 * acrescentada para cencelamento é maior que a quantidade de estoque atual
                 */
                    if ($amount > $aa2cestq->GET_ESTOQUE) {
                    /**
                     * Se a quantidade acrescentada para o cancelamento for maior que a quantidade de estoque atual
                     * vamos zerar o estoque atual e colocar o restante nno estoque negativo
                     */
                        $stmt = Connect2::getInstance($this->verifique)->prepare("UPDATE AA2CESTQ SET GET_ESTOQUE = 0, GET_QTD_PEND_VDA = $amount - GET_ESTOQUE WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store");
                        echo "UPDATE AA2CESTQ SET GET_ESTOQUE = 0, GET_QTD_PEND_VDA = $amount - GET_ESTOQUE WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store<br>";
                    } else {
                    /**
                     * Se a quantidade acrescentada para cancelamento for menor ou igual a quantidade de estoque atual
                     * vamos subtrai-la do estoque atual
                     */
                        $stmt = Connect2::getInstance($this->verifique)->prepare("UPDATE AA2CESTQ SET GET_ESTOQUE = GET_ESTOQUE - $amount, GET_QTD_PEND_VDA = 0 WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store");
                        echo "UPDATE AA2CESTQ SET GET_ESTOQUE = GET_ESTOQUE - $amount, GET_QTD_PEND_VDA = 0 WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store<br>";
                    }
                }
            }   
        } else {
        /**
         * Se $type !== true quer dizer que estamos no primeiro passo, só precisamos informar o que foi passado com parametro
         */
            $stmt = Connect2::getInstance($this->verifique)->prepare("UPDATE AA2CESTQ SET GET_ESTOQUE = $amount, GET_QTD_PEND_VDA = $negative WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store");
            echo "UPDATE AA2CESTQ SET GET_ESTOQUE = $amount, GET_QTD_PEND_VDA = $negative WHERE GET_COD_PRODUTO = $code AND GET_COD_LOCAL = $store<br>";
        }
        //UPDATE
        
        
        try {
            $stmt->execute();
        } catch (PDOException $pdo) {
            dd($pdo);
        }
        //HABILITAR TRG_AA2CESTQ
        echo "ALTER TRIGGER TRG_AA2CESTQ ENABLE<br>";
        $stmt = Connect2::getInstance($this->verifique)->prepare("ALTER TRIGGER TRG_AA2CESTQ ENABLE");
        try {
            $stmt->execute();
        } catch (PDOException $pdo) {
            dd($pdo);
        }
           
    }

    public function addAa1agedt($date,$DET_LOJA,$nfNumber,$store,$totalCost,$agenda,$cfop,$serie)
    {   
        /**
         * AA1AGEDT Capa externa da nota
         * AG1LGNFI Número da nota Capa interna nota
         * AG2CAPNT Cama interna nota
         */
        $totalCost = str_replace(',','.',$totalCost);
        $stmt = Connect2::getInstance($this->verifique)->prepare("INSERT INTO AA1AGEDT (DET_TRANSMIT,DET_INTG_CP,DET_INTG_RC,DET_DATA,DET_LOJA,DET_TIPO,DET_NOTA,DET_SERIE,DET_LOJA_1,DET_VALOR,DET_FLAG,DET_ROM,DET_DIG,DET_CRIT,DET_INTG_LF ,DET_INTG_DB ,DET_SECAO,DET_ERRO,DET_RATEIO ,DET_LIBERA ,DET_DIVERGE ,DET_FLAG_RAT ,DET_PORTARIA ,RMSRSV_001) VALUES 
        (0,' ',' ',$date,$DET_LOJA,$agenda,$nfNumber,'$serie ',$store,$totalCost,1,0,0,0,0,0,0,0,0,' ',' ',' ',' ','D')");
        try {
            $stmt->execute();
        } catch (PDOException $exception) {
            $this->fail = $exception;
            return $this->fail;
        }
        

        $date2 = date('ymd');
        $date3 = date('dmy');        
        $stmt = Connect2::getInstance($this->verifique)->prepare("INSERT INTO AG1LGNFI
        (NFI_DESTINO,
        NFI_DATA_AGENDA,
        NFI_ORIGEM,
        NFI_AGENDA,
        NFI_NOTA,
        NFI_SERIE,
        NFI_DATA_ATU,
        NFI_HORA_ATU,
        NFI_USUARIO,
        NFI_TRANSP_SAIDA,
        NFI_TRANSP_PLACA,
        NFI_TRANSP_UF,
        NFI_NF_EDI,
        NFI_DATA_EXCLUSAO,
        NFI_HORA_EXCLUSAO,
        NFI_AGENDA_EXCLUSAO,
        NFI_DIRETORIO_XML,
        NFI_ARQUIVO_XML,
        NFI_QTDE_VOLUME,
        NFI_FLAG_FFI,
        NFI_INS_EST,
        NFI_NUM_FAT,
        NFI_CHAVE_NFE,
        NFI_CHAVE_NFE_FRT,
        NFI_ESTORNO,
        NFI_OPE_PRESENCIAL,
        NFI_FINALIDADE,
        NFI_DATA_SELO,
        NFI_TIPO_XML,
        NFI_VOL_ESP,
        NFI_VOL_MARCA,
        NFI_VOL_NUMERACAO,
        NFI_NOTA_XML,
        NFI_FRT_CMUN_ENV,
        NFI_FRT_CMUN_FIM,
        NFI_FRT_CNPJ_EMIT,
        NFI_FRT_CNPJ_DEST,
        NFI_FRT_CNPJ_REME,
        NFI_IMAGEM_NF,
        NFI_CHAVE_NFS)
        VALUES( ".substr($DET_LOJA,0,strlen($DET_LOJA)-1).",
         $date,
         $store,
         $agenda,
         $nfNumber,
        '$serie ',
         0,
         0,
        '201.FBRI',
         0,
        '',
        '',
        'MANU-RCP-VGRMMANU-SC',
         0,
         0,
         0,
        '',
        '',
         0,
        '',
        'ISENTO',
        $nfNumber,
        '',
        '',
        0,
        0,
        0,
         0,
        'NFE000',
        '                    ',
        '                    ',
        '                    ',
        $nfNumber,
        0,
        0,
        0,
        0,
        0,
        '                                                                                                                                                                                                                                                               ',
        '                                                  ')");
        $stmt->execute();

        $stmt = Connect2::getInstance($this->verifique)->prepare("INSERT INTO AG2CAPNT
        (NOT_DESTINO,
        NOT_DTA_AGENDA,
        NOT_DISTRIB,
        NOT_TPO_AGENDA,
        NOT_NOTA,
        NOT_SERIE,
        NOT_EMISSAO,
        NOT_DATA_BASE,
        NOT_COD_FISCAL,
        NOT_COMPRADOR,
        NOT_NPED_1,
        NOT_DPED_1,
        NOT_CND_PAGTO_1,
        NOT_NPED_2,
        NOT_DPED_2,
        NOT_CND_PAGTO_2,
        NOT_NPED_3,
        NOT_DPED_3,
        NOT_CND_PAGTO_3,
        NOT_NPED_4,
        NOT_DPED_4,
        NOT_CND_PAGTO_4,
        NOT_NPED_5,
        NOT_DPED_5,
        NOT_CND_PAGTO_5,
        NOT_DVEN_1,
        NOT_FVEN_1,
        NOT_DVEN_2,
        NOT_FVEN_2,
        NOT_DVEN_3,
        NOT_FVEN_3,
        NOT_DVEN_4,
        NOT_FVEN_4,
        NOT_DVEN_5,
        NOT_FVEN_5,
        NOT_SECAO,
        NOT_TPO_PGTO,
        NOT_ESTADO,
        NOT_CRED_PRESUM_TOT,
        NOT_COMPLE,
        NOT_COMPLE_T,
        NOT_DESCON,
        NOT_DESCON_T,
        NOT_ADESC_F,
        NOT_CFO_1,
        NOT_FIGURA_1,
        NOT_OPERACAO_1,
        NOT_VLR_CONT_1,
        NOT_BASE_CALC_1,
        NOT_PERC_ICM_1,
        NOT_VLR_ICM_1,
        NOT_VLR_ICM_F_FF_1,
        NOT_ISENTO_1,
        NOT_PVV_FF_1,
        NOT_CRED_PRESUM_1,
        NOT_PERC_IPI_1,
        NOT_VLR_IPI_FF_1,
        NOT_CFO_2,
        NOT_FIGURA_2,
        NOT_OPERACAO_2,
        NOT_VLR_CONT_2,
        NOT_BASE_CALC_2,
        NOT_PERC_ICM_2,
        NOT_VLR_ICM_2,
        NOT_VLR_ICM_F_FF_2,
        NOT_ISENTO_2,
        NOT_PVV_FF_2,
        NOT_CRED_PRESUM_2,
        NOT_PERC_IPI_2,
        NOT_VLR_IPI_FF_2,
        NOT_CFO_3,
        NOT_FIGURA_3,
        NOT_OPERACAO_3,
        NOT_VLR_CONT_3,
        NOT_BASE_CALC_3,
        NOT_PERC_ICM_3,
        NOT_VLR_ICM_3,
        NOT_VLR_ICM_F_FF_3,
        NOT_ISENTO_3,
        NOT_PVV_FF_3,
        NOT_CRED_PRESUM_3,
        NOT_PERC_IPI_3,
        NOT_VLR_IPI_FF_3,
        NOT_CFO_4,
        NOT_FIGURA_4,
        NOT_OPERACAO_4,
        NOT_VLR_CONT_4,
        NOT_BASE_CALC_4,
        NOT_PERC_ICM_4,
        NOT_VLR_ICM_4,
        NOT_VLR_ICM_F_FF_4,
        NOT_ISENTO_4,
        NOT_PVV_FF_4,
        NOT_CRED_PRESUM_4,
        NOT_PERC_IPI_4,
        NOT_VLR_IPI_FF_4,
        NOT_CFO_5,
        NOT_FIGURA_5,
        NOT_OPERACAO_5,
        NOT_VLR_CONT_5,
        NOT_BASE_CALC_5,
        NOT_PERC_ICM_5,
        NOT_VLR_ICM_5,
        NOT_VLR_ICM_F_FF_5,
        NOT_ISENTO_5,
        NOT_PVV_FF_5,
        NOT_CRED_PRESUM_5,
        NOT_PERC_IPI_5,
        NOT_VLR_IPI_FF_5,
        NOT_VLR_CONT_INF,
        NOT_BASE_INF,
        NOT_VLR_ICM_INF,
        NOT_VLR_ICMF_INF,
        NOT_TOT_ISENTO,
        NOT_ISENTO_INF,
        NOT_PVV_INF,
        NOT_VLR_IPI_INF,
        NOT_BASE_IRF,
        NOT_ALIQ_IRF,
        NOT_IRF,
        NOT_ICMS_FRONT,
        NOT_BASE_ISS,
        NOT_CGC_CPF,
        NOT_ALIQ_ISS,
        NOT_VLR_ISS,
        NOT_RATEIO_NF,
        NOT_TRANSP,
        NOT_CONHEC,
        NOT_CONHEC_SERIE,
        NOT_EMI_CONHEC,
        NOT_DT_APANHA,
        NOT_VLR_FRETE,
        NOT_BAS_C_FRETE,
        NOT_ALIQ_ICM,
        NOT_VLR_ICM_FRETE,
        NOT_AGENDA_FRET,
        NOT_COD_FISC_T,
        NOT_OPE_TRANSP,
        NOT_ADESC_T,
        NOT_VLRDIF_1,
        NOT_VLRDIF_2,
        NOT_VLRDIF_3,
        NOT_VLRDIF_4,
        NOT_VLRDIF_5,
        NOT_VLRDIF_6,
        NOT_VLRDIF_7,
        NOT_VLRDIF_8,
        NOT_VLRDIF_9,
        NOT_VLRDIF_10,
        NOT_DTA_PROC,
        NOT_LOTE,
        NOT_ATL_NTA,
        NOT_LIBERACAO,
        NOT_DIG,
        NOT_LIBERA,
        NOT_INFORME,
        NOT_DESC_F,
        NOT_ESP_NF,
        NOT_AUTONOMIA,
        NOT_LOJ_CLI,
        NOT_FLAG_PEDIDO,
        NOT_FLAG_PROC,
        NOT_VLR_COMIS,
        NOT_VLR_BONUS,
        NOT_VENDEDOR,
        NOT_LOJA_VENDA,
        NOT_ESTADO_TRANSP,
        NOT_COD_FAT,
        NOT_SITUACAO,
        NOT_TPO_DOC,
        NOT_OBS,
        NOT_NUM_SEQ,
        NOT_FILLER,
        NOT_IMP_IMPORT_INF,
        NOT_PIS_INF,
        NOT_COFINS_INF,
        NOT_VLR_PRODS_INF,
        NOT_OUTRAS_INF)
        VALUES( $DET_LOJA,
         $date,
         $store,
         $agenda,
         $nfNumber,
        '$serie ',
         $date2,
         0,
         $cfop,
         2,
         0,
         0,
         1,
         0,
         0,
        0,
         0,
         0,
        0,
         0,
         0,
        0,
         0,
         0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
         0,
        ' ',
        'BA',
         0,
         0,
         0,
         0,
         0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        $totalCost,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
        0,
        0,
        0,
        0,
        0,
         10838648000195,
        0,
        0,
        0,
         0,
         0,
        '   ',
         0,
         $date2,
         0,
         0,
        0,
         0,
         0,
         0,
        0,
        0,
        0,
        0,
        0,
         0,
        0,
        0,
        0,
        0,
         0,
         0,
         $date3,
        0,
        0,
        0,
        0,
         0,
         0,
        0,
        '   ',
        ' ',
        ' ',
        0,
        0,
        0,
        0,
         0,
         0,
        '  ',
         0,
        ' ',
        'NF',
        '',
         1,
        0,
        0,
         0,
         0,
         0,
        0)");
        $stmt->execute();   
    }

    public function addAg2detnt($cfop,$REN_DESTINO,$REN_ITEM,$store,$cost,$date,$nfNumber,$amount,$i,$agenda,$cst)
    {   
        (float)$cost = str_replace(",",".",$cost);
        (float)$amount = str_replace(",",'.',$amount);
        $stmt = Connect2::getInstance($this->verifique)->prepare("INSERT INTO AG2DETNT
        (REN_DESTINO,
        REN_DTA_AGENDA,
        REN_DISTRIB,
        REN_TPO_AGENDA,
        REN_NOTA,
        REN_SERIE,
        REN_SEQ,
        REN_ITEM,
        REN_AGE_ITEM,
        REN_CFO_ITEM,
        REN_DESTINO_ALT,
        REN_DTA_AGENDA_ALT,
        REN_DISTRIB_ALT,
        REN_TPO_AGENDA_ALT,
        REN_NOTA_ALT,
        REN_SERIE_ALT,
        REN_ITEM_ALT,
        REN_AGE_ITEM_ALT,
        REN_CFO_ITEM_ALT,
        REN_MARGEM_CAD,
        REN_PRC_VEN_CAD,
        REN_CENTRO_CUSTO,
        REN_DATA_VALIDADE,
        REN_COND_PGTO,
        REN_VENDEDOR,
        REN_LOC_FISICA,
        REN_SECAO,
        REN_FIGURA,
        REN_OPERACAO,
        REN_CUSTO_FOR,
        REN_COMISSAO,
        REN_CUS_UN,
        REN_CUS_UN_N,
        REN_CUS_INV,
        REN_PESO_UN,
        REN_QTD_PED,
        REN_QTD_FAT,
        REN_QTD_REC,
        REN_ESTQ_ANT,
        REN_NCND,
        REN_NPED,
        REN_DPED,
        REN_DTA_PEDIDO,
        REN_QUANTI_PED,
        REN_BASE_PVV,
        REN_PERC_DESC,
        REN_PERC_IPI,
        REN_VALOR_IPI,
        REN_TPO_EMB,
        REN_EMB,
        REN_PRC_UN,
        REN_PRC_EMB,
        REN_VLR_DESC,
        REN_DESP_ACES,
        REN_PRC_TOT,
        REN_BASE_IPI,
        REN_IPI_CAL,
        REN_BASE_CALC,
        REN_ALIQ_ICM,
        REN_VLR_ICM,
        REN_VLR_PAUTA,
        REN_BASE_REDUZ,
        REN_ISENTO,
        REN_VLR_ICM_CRED_PRESUM,
        REN_VLR_ICM_VENDA,
        REN_PERC_STR,
        REN_PERC_ICMF,
        REN_PVV,
        REN_VLR_ICM_FONTE,
        REN_DESP_ACES_IS,
        REN_COMPL_IS,
        REN_COMPL_TB,
        REN_BONIFICACAO,
        REN_BONUS,
        REN_AJT_CUS_UAQ,
        REN_FUNRURAL,
        REN_VLRDIF,
        REN_FIGURA_FRT,
        REN_OPERACAO_FRT,
        REN_FRETE_FRT,
        REN_PERC_ICMS_FRT,
        REN_PERC_REDUZ_FRT,
        REN_VLR_ICM_FRT,
        REN_SITUACAO,
        REN_FLAG_CONV,
        REN_TBC_INTG_07,
        REN_TBC_INTG_08,
        REN_TBC_INTG_11,
        REN_TBC_INTG_17,
        REN_PRECO_INF,
        REN_SIT_TRIB,
        REN_NIVEL_PRECO,
        REN_RATEIO,
        REN_LIBERA,
        REN_DESCR_INF,
        REN_TPO_EMB_INF,
        REN_DSC_INF,
        REN_PRC_INF,
        REN_VLDAC_INF,
        REN_VLTOT_INF,
        REN_BASE_IPI_INF,
        REN_IPI_CAL_INF,
        REN_IPI_VAL_INF,
        REN_IPI_PRC_INF,
        REN_BASE_CALC_INF,
        REN_ALIQ_ICM_INF,
        REN_VLR_ICM_INF,
        REN_VLR_PAUTA_INF,
        REN_BASE_REDUZ_INF,
        REN_ISENTO_INF,
        REN_MVA_ST_INF,
        REN_RDB_ST_INF,
        REN_PRC_ST_INF,
        REN_BAS_ST_INF,
        REN_VLR_ST_INF,
        REN_CST_XML,
        REN_DESC_INF,
        REN_PIS_CALC,
        REN_COFINS_CALC,
        REN_PIS_INF,
        REN_COFINS_INF,
        REN_EMB_INF,
        REN_ERROS,
        REN_VLTOT_CALC,
        REN_INF_ADIC,
        REN_FRETE_FRT_INF,
        REN_PRC_TOT_INF,
        REN_COMPL_IS_INF,
        REN_COMPL_TB_INF,
        REN_IMP_IMP_INF,
        REN_BAS_ST_REC_INF,
        REN_VLR_ST_REC_INF,
        REN_PROCEDENCIA,
        REN_CATEGORIA_ANT,
        REN_VLR_INDENIZACAO,
        REN_DESC_CALC)
        VALUES( $REN_DESTINO,
        $date,
        $store,
         $agenda,
         $nfNumber,
        '1  ',
         $i,
         $REN_ITEM,
         $agenda,
         $cfop,
         $REN_DESTINO,
         $date,
         $store,
         $agenda,
         $nfNumber,
        '1  ',
        $REN_ITEM,
        $agenda,
         $cfop,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
         $cst,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
         $amount,
         $amount,
         0,
        0,
         0,
         0,
         140322,
         121214,
        0,
         0,
        0,
        0,
        'UN',
         1,
        0,
        ".($cost/$amount).",
         0,
         0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        ' ',
        ' ',
        ' ',
        ' ',
        ' ',
        ' ',
         1,
         $cst,
        0,
        0,
         0,
        '',
        '      ',
         0,
        ".($cost/$amount).",
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
         0,
        0,
        0,
        0,
        0,
        0,
        '                    ',
        0,
        '',
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
         0)");
        
         $stmt->execute();

    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        $primary = $this->primary;
        $id = null;

        try {
            if (!$this->required()) {
                throw new Exception("Preencha os campos necessários");
            }

            /** Update */
            if (!empty($this->data->$primary)) {
                $id = $this->data->$primary;
                $this->update($this->safe(), "{$this->primary} = :id", "id={$id}");
            }

            /** Create */
            if (empty($this->data->$primary)) {
                $id = $this->create($this->safe());
            }

            if (!$id) {
                return false;
            }

            // $this->data = $this->findById($id)->data();
            return true;
        } catch (Exception $exception) {
            $this->fail = $exception;
            return false;
        }
    }

    public function update2($code,$where): bool
    {
        $stmt = Connect2::getInstance($this->verifique)->prepare("UPDATE AG2DETNT SET $code WHERE $where");
        return $stmt->execute();
    }

    /**
     * @return bool
     */

    public function destroy2($where): bool
    {
        $stmt = Connect2::getInstance($this->verifique)->prepare("DELETE AG2DETNT WHERE $where");
        return $stmt->execute();
    }


    public function destroy(): bool
    {
        $primary = $this->primary;
        $id = $this->data->$primary;

        if (empty($id)) {
            return false;
        }

        return $this->delete("{$this->primary} = :id", "id={$id}");
    }

    /**
     * @return bool
     */
    protected function required(): bool
    {
        $data = (array)$this->data();
        foreach ($this->required as $field) {
            if (empty($data[$field])) {
                if(!is_int($data[$field])){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @return array|null
     */
    protected function safe(): ?array
    {
        $safe = (array)$this->data;
        unset($safe[$this->primary]);
        return $safe;
    }


    /**
     * @param string $string
     * @return string
     */
    protected function toCamelCase(string $string): string
    {
        $camelCase = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
        $camelCase[0] = strtolower($camelCase[0]);
        return $camelCase;
    }
}
