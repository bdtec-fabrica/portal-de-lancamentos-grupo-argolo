<?php



require __DIR__."/vendor/autoload.php";

use CoffeeCode\Router\Router;
use Source\App\Verifications;


$router = new Router(URL_BASE);

/*

 * Controller

 */

$router->namespace("Source\App");

/*

 * WEB

 * home

 */

$router->group(null);

$router->get("/","Dashboard:home");

$router->get("/home2","Dashboard:home2");

$router->get("/home2/{data}","Dashboard:home2");

$router->get("/privacidade","Dashboard:privacidade");

$router->get("/dashboard/","Dashboard:DashBoards");

$router->post("/dashboard/","Dashboard:DashBoards");

$router->post("/doLogin","Web:doLogin");

$router->get("/login","Web:login");

$router->post("/login/forgotPassword","Web:forgotPassword");

$router->get("/login/{data}","Web:login");

$router->get("/logout","Web:doLogout");

/*

 * ADMIN

 * home

 */

$router->group(null);

$router->get("/usuarios","Admin:users");

$router->post("/updateWorkLoad","Admin:updateParameterWeek");

$router->post("/addParameter", "Admin:addParameter");

$router->post("/deleteParameter", "Admin:deleteParameter");

$router->get("/lockParameter","Admin:lockParameter","lockParameter");
$router->post("/lockParameter/create","Admin:createLockParameter","createLockParameter");
$router->post("/lockParameter/read","Admin:readLockParameter","readLockParameter");
$router->post("/lockParameter/update","Admin:updateLockParameter","updateLockParameter");
$router->post("/lockParameter/delete","Admin:deleteLockParameter","deleteLockParameter");


$router->get("/daily","Admin:addCheckIn");

$router->get("/viewCheckIns","Admin:viewCheckIns");

$router->post("/searchViewCheckIns","Admin:searchViewCheckIns");

$router->post("/reportCheckIns","Admin:reportCheckIns");

$router->get("/usuarios/{id}","Admin:users");

$router->post("/usuarios/usersSearch","Admin:usersSearch");

$router->post("/usuarios/storeGroupsParameters","Admin:storeGroupsParameters");

$router->post("/usuarios/userSector","Admin:userSector");

$router->post("/usuarios/userEdit","Admin:userEdit");

$router->post("/usuarios/usersUpdatePassword","Admin:usersUpdatePassword");

$router->post("/usuarios/usersAdd","Admin:usersAdd");

$router->group(null);
$router->get("/register","Attendance:updateRegisterAmount");

/*

 * WEB

 * request

 */

$router->group(null);

$router->get("/lojas","Admin:stores");
$router->post("/lojas","Admin:stores");
$router->get("/lojas/contacts", "Admin:storeContacts");
$router->post("/lojas/contacts/add", "Admin:storeContactsAdd");
$router->post("/lojas/contacts/delete", "Admin:storeContactsDelete");
$router->post("/lojas/contacts/update", "Admin:storeContactsUpdate");

$router->post("/lojas/update/cert","Admin:updateCert");
$router->post("/lojas/update/store","Admin:updateStore");
$router->post("/lojas/{id}","Admin:stores");




$router->group(null);

$router->get("/request","Attendance:request");

$router->get("/request2","Attendance:request2");

$router->get("/request/check/","Attendance:check");

$router->post("/request/update","Attendance:requestUpdate");

$router->post("/request/validate","Attendance:requestValid");

$router->post("/request/searchRequest","Attendance:searchNfReceipt");

$router->post("/request/registerReport","Attendance:registerReport");

$router->get("/request/{id}","Attendance:request");

$router->get("/request/storeStatus/{store}/{status}","Attendance:requestStoreStatus");

$router->get("/request/pendente/{operation}","Attendance:requestPendente");

$router->get("/request/atualizada/{operation}","Attendance:requestAtualizada");

$router->get("/request/cliente/{operator}","Attendance:requestClientOperator");

$router->get("/request/atualizada/{operator}/{cd}","Attendance:requestAtualizadaOperator");

$router->get("/request/dashboard/{status}","Attendance:getRequest");

$router->get("/request/","Attendance:request");

$router->post("/request/addRequest","Attendance:addNfReceipt");

$router->post("/request/editRequest","Attendance:editNfReceipt");

$router->post("/request/addRequestAuto","Attendance:addNfReceiptAuto");

$router->post("/request/deleteRequest","Attendance:deleteNfReceipt");

$router->get("/request/interacao/{id}","Attendance:interactions");

$router->post("/request/interacao/add","Attendance:addInteraction");

$router->post("/request/nfReport/add","Attendance:addReport");

$router->post("/request/trocaCodigo/nfe","Nfe:trocaCodigo");

$router->post("/request/manifestacao/nfe","Nfe:manifestar");

$router->post("/request/reprocessar/nfe","Nfe:nfeReprocess");

$router->get("/request/verification","Attendance:nfVerification");

$router->post("/request/verification","Attendance:nfVerification");

$router->post("/request/verification/import","Attendance:nfVerificationImport");

$router->get("/request/verification/transfer/{id}","Nfe:pastIntoServidor");

$router->group(null);

$router->get("/exchangeCode","Parameters:exchangeCode");
$router->get("/exchangeCode/{id}","Parameters:exchangeCode");
$router->post("/exchangeCode/search","Parameters:searchExchangeCode");
$router->post("/exchangeCode/edit","Parameters:editExchangeCode");
$router->post("/exchangeCode/add","Parameters:addExchangeCode");
$router->post("/exchangeCode/delete","Parameters:deleteExchangeCode");


$router->group(null);

$router->get("/packagingCode","Parameters:packagingCode");
$router->get("/packagingCode/{id}","Parameters:packagingCode");
$router->post("/packagingCode/search","Parameters:searchpackagingCode");
$router->post("/packagingCode/add","Parameters:addpackagingCode");
$router->post("/packagingCode/edit","Parameters:editpackagingCode");
$router->post("/packagingCode/delete","Parameters:deletepackagingCode");

$router->group(null);

$router->get("/nfSefaz","Nfe:nfePainel");
$router->get("/nfSefaz/websearch/","Nfe:buscarNotas");
$router->get("/nfSefaz/websearch2/","Nfe:buscarNotas");
$router->post("/nfSefaz/search","Nfe:NfSearch");
$router->get("/nfSefaz/search","Nfe:NfSearch");
$router->get("/nfSefaz/sefazSearch/{store}","Nfe:sefazSearch");
$router->get("/nfSefaz/{ids}","Nfe:nfePainel");
$router->post("/nfSefaz/pdf/download","Nfe:NfePdf");

$router->group(null);
$router->get("/simples", "Verifications:simplesForm");
$router->post("/simples", "Verifications:simples");

$router->group(null);
$router->get("/multa", "Verifications:multaForm");
$router->post("/multa", "Verifications:multa");

$router->group(null);

$router->post("/relatorioNotas", "Attendance:NfReceiptRel");
$router->post("/relatorioNotasReportadas", "Attendance:NfReceiptRelReporte");
$router->post("/start","Nfe:nfStart");
$router->post("/start/note","Nfe:note");
$router->post("/start/update","Nfe:update");
$router->get("/iniciadas","Nfe:started");
$router->get("/iniciadas/{ids}","Nfe:started");
$router->post("/desbloqueio","Attendance:desbloqueio");

$router->group(null);
$router->post("/blindColletion","BlindConttage:newConttage");
$router->get("/blindColletion/{id}","BlindConttage:startConttage");
$router->get("/romaneio/{id}","BlindConttage:getRomaneio");
$router->post("/blindColletion/save","BlindConttage:saveConttage");

$router->group(null);
$router->get("/auditoria-vencimento","Verifications:duedateAudit");
$router->post("/auditoria-vencimento/search","Verifications:duedateAuditSearch");

$router->group(null);
$router->get("/auditoria-margem","Verifications:getMargens");
$router->post("/auditoria-margem/search","Verifications:getMargensSearch");

$router->group(null);
$router->get("/auditoria-margem2","Verifications:getMargens2");
$router->post("/auditoria-margem2/search","Verifications:getMargensSearch2");

$router->group(null);
$router->get("/auditoria-lancamento","Verifications:getPostedOrNot");
$router->post("/auditoria-lancamento/search","Verifications:getPostedOrNotSearch");

$router->group(null);
$router->get("/dashboard","Dashboard:home2");

$router->group(null);
$router->get("/desossa","Desossa:desossa");
$router->post("/desossa/add","Desossa:add");
$router->post("/desossa/copiar","Desossa:copiar");
$router->post("/desossa/copiaExec","Desossa:copiaExec");
$router->post("/desossa/delete","Desossa:delete");
$router->get("/desossa/edit/edit","Desossa:desossaEdit");
$router->post("/desossa/search","Desossa:searchDesossa");
$router->get("/desossa/search/{ids}","Desossa:desossa");
$router->get("/desossa/{ox}","Desossa:desossa");
$router->post("/desossa/update","Desossa:dataUpdate");
$router->post("/desossa/convert","Desossa:convertXML");
$router->get("/desossa/xml/{ok}","Desossa:desossa");
$router->get("/desossa/xml/error/{error}","Desossa:desossa");

$router->group(null);
$router->get("/apuracao","Verifications:getApuracao");
$router->post("/apuracao/report","Verifications:getApuracaoSearch");
$router->post("/apuracao/dates","Verifications:getDates");
$router->post("/apuracao/add","Verifications:addItemApuracao");
$router->post("/apuracao/addList","Verifications:addListApuracao");
$router->post("/apuracao/secao","Verifications:getSecao");
$router->post("/apuracao/grupo","Verifications:getGrupo");
$router->post("/apuracao/subgrupo","Verifications:getSubGrupo");
$router->post("/apuracao/inventario","Verifications:addInventory");
$router->post("/apuracao/salvaInventario","Verifications:saveInventory");
$router->post("/apuracao/importar","Verifications:importaEstoque");

$router->group(null);
$router->get("/apuracao-fiscal","ApuracaoFiscal:getApuracao");
$router->post("/apuracao-fiscal/report","ApuracaoFiscal:getApuracaoSearch");
$router->post("/apuracao-fiscal/dates","ApuracaoFiscal:getDates");
$router->post("/apuracao-fiscal/add","ApuracaoFiscal:addItemApuracao");
$router->post("/apuracao-fiscal/addList","ApuracaoFiscal:addListApuracao");
$router->post("/apuracao-fiscal/secao","ApuracaoFiscal:getSecao");
$router->post("/apuracao-fiscal/grupo","ApuracaoFiscal:getGrupo");
$router->post("/apuracao-fiscal/subgrupo","ApuracaoFiscal:getSubGrupo");


$router->group(null);
$router->get("/movimentacao","Verifications:getItensSemMovimentacao");
$router->post("/movimentacao/report","Verifications:getItensSemMovimentacaoSearch");


$router->group(null);
$router->get("/margin/webservice","Verifications:webServiceMargin");
$router->get("/margin/webservice2","Verifications:webServiceMarginFora");
$router->get("/margin/webservice4","Verifications:webService988x987");
$router->get("/margin/webservice5","Verifications:webServiceCupons");
$router->get("/margin/webservice6","Verifications:webServicePromocaoExpirada");
$router->get("/margin/webservice7","Verifications:webServiceVendasNaoImportadas");
$router->get("/margin/webservice8","Verifications:webServiceFechamentoSemanal");
$router->get("/margin/webservice9","Verifications:webServiceCadastro");
$router->get("/margin/webservice10","Verifications:webServiceLancamento24Horas");
$router->get("/margin/webservice12","Verifications:webServiceMarginVendas");
$router->get("/margin/webservice13","Verifications:webServiceDevolucaoVenda");
$router->get("/margin/webservice14","Verifications:webNotasNaoAutorizadas");
$router->get("/margin/webservice15","Verifications:webServiceVendasNegativas");
$router->get("/margin/webservice16","Verifications:consistencia");
$router->get("/margin/webservice17","Verifications:webServiceMarginVendasFora");
$router->get("/margin/webservice18","Verifications:webSericeNCMErrado");
$router->get("/margin/webservice19","Verifications:webServiceNFeCancelada");
$router->get("/margin/webservice20","Verifications:webServiceCertificado");
$router->get("/margin/webservice21","Verifications:webServiceCadastroUsuario");
$router->get("/margin/webservice22","Verifications:webServiceIndicadores");
$router->get("/margin/webservice23","Verifications:webServiceAliquota");
$router->get("/margin/webservice24","Verifications:usoConsumoComEstoque");
$router->get("/margin/webservice25","Verifications:manifestationAudit");
$router->get("/margin/webservice26","Verifications:nfeUpdateUsers");
$router->get("/margin/webservice27","Verifications:vendasSemUltimaEntrada");
$router->get("/margin/webservice28","Verifications:nfeAutorizada");
$router->get("/margin/webservice29","Verifications:cuponsPendetenProtocolo");
$router->get("/margin/webservice30","Verifications:nfAtualizadaSoRetaguarda");
$router->get("/margin/webservice31","Verifications:requestLojasImplantadas");
$router->get("/margin/webservice32","Verifications:getErrorGerencial");
$router->get("/margin/webservice33","Verifications:fornecedoresNotasItens");
$router->get("/margin/webservice34","Verifications:transferenceAudito");
$router->get("/margin/webservice35","Verifications:consultaCadastro");
$router->post("/margin/webservice35","Verifications:consultaCadastro");
$router->get("/margin/webservice36","Verifications:teste");// Gerencial BDTEC
$router->get("/margin/webservice37","Verifications:teste2");// Gerencial BDTEC
$router->post("/margin/webservice37","Verifications:teste2");// Gerencial BDTEC
$router->post("/margin/webservice40","Verifications:teste3");// Gerencial BDTEC
$router->post("/margin/webservice41","Verifications:teste4");// Gerencial BDTEC
$router->post("/margin/webservice42","Verifications:teste5");// Gerencial BDTEC
$router->post("/margin/webservice43","Verifications:teste6");// Gerencial BDTEC
$router->get("/margin/webservice38","Verifications:gestaoEquipe");
$router->post("/margin/webservice38","Verifications:gestaoEquipe");
$router->get("/margin/webservice50","Verifications:NFESemChave");
$router->get("/margin/webservice51","Verifications:divergenciaVencimento");
$router->get("/margin/webservice52/{store}","Verifications:semGiro");
$router->get("/margin/webservice53","Verifications:informe");
$router->get("/margin/webservice54","Verifications:informeFiscal");
$router->Get("/margin/webservice55","Verifications:informeNotification");
$router->Get("/margin/webservice56","Verifications:informe1Porcento");
$router->Get("/margin/webservice57","Verifications:informeNotas");
$router->Get("/margin/webservice58","Verifications:informeNotas2");//
$router->Get("/margin/webservice59","Verifications:notasFiscalNaoAtualizadas");//
$router->Get("/margin/webservice60","Verifications:informeFiscal2");//
$router->Get("/margin/webservice61","Verifications:auditNFEnergia");//
$router->Get("/margin/webservice62","Verifications:agenda608e609");//
$router->Get("/margin/webservice64","Verifications:CFOPS");//
$router->Get("/margin/webservice65","Verifications:automaticManifestarion");//
$router->Get("/margin/webservice66","Verifications:fornecedoresReports");//
$router->Get("/margin/webservice67","Verifications:NFCEAudit");//margemConsinco
$router->Get("/margin/webservice68","Verifications:margemConsinco");//
$router->Get("/margin/webservice69","Nfe:emitirNFCe");//
$router->Get("/margin/webservice70/{store}","Verifications:icmsAjustado");//
$router->Get("/margin/webservice71","Verifications:consultaVendas");//
$router->post("/margin/webservice72","Verifications:consultaVendas");//
$router->get("/margin/webservice73","Verifications:diferencaNFCE");
$router->get("/margin/webservice74","Verifications:diferencaNFCE2");
$router->get("/margin/webservice75","Verifications:testeSOAP");
$router->get("/margin/webservice76","Verifications:rodrigo");
$router->get("/margin/webservice77","Verifications:horti");
$router->get("/margin/webservice78","Verifications:test");
$router->get("/margin/webservice79","Verifications:cadastrosUsuarios");



$router->get("/margin/notificationsAudit","Verifications:notificationsAudit");
$router->get("/margin/webservice39","Verifications:verificarReferenciaDuplicada");
$router->get("/margin/validate","Verifications:marginValidate");
$router->post("/margin/validate/update","Verifications:marginValidateUpdate");

$router->group(null);
$router->get("/fiscal-stock","FiscalStock:formImportInvContabil");
$router->post("/fiscal-stock/import","FiscalStock:importInvContabil");
$router->get("/fiscal-stock/view/{id}","FiscalStock:viewInvContabil");
$router->post("/fiscal-stock/view/comparacao","FiscalStock:viewMovmentNaoFiscal");

$router->group(null);
$router->post("/registrarCadastro","Attendance:registarCadastro");

$router->group(null);
$router->post("/updatecode","Attendance:updateCode");

$router->group(null);
$router->get("/posted/webservice","Verifications:webServicePostedNotUpdated");

$router->group(null);
$router->get("/duedate/webservice","Verifications:webServiceDuoDate");

$router->group(null);
$router->post("/desfazreport","Attendance:desfazReport");

$router->group(null);
$router->get("/blabla","Verifications:blabla");


$router->group(null);
$router->get("/fiscal","Tributary:fiscalActivityRead");
$router->get("/fiscal/principal/{id}/{date}","Tributary:fiscalActivityRead");
$router->get("/fiscal/{store}/{date}","Tributary:fiscalActivityOnlyRead");
$router->post("/fiscal/create","Tributary:fiscalActivityCreate");
$router->post("/fiscal/search","Tributary:fiscalSearch");
$router->post("/fiscal/add","Tributary:addActivity");
$router->post("/fiscal/conferenceAdd","Tributary:addConference");
$router->post("/fiscal/conferenceNFESEFAZ","Tributary:NFESefazConference");

// $router->post("/fiscal/update","Tributary:fiscalActivityUpdate");
$router->post("/fiscal/delete","Tributary:fiscalActivityDelete");
$router->post("/fiscal/interacoes/add","Tributary:addInteraction");

$router->group(null);
$router->get("/json/{json}","Verifications:getJson");
$router->get("/json/{loja}/{base}/{tipo}","Verifications:importVariacaoCusto");


$router->group(null);
$router->get("/purchasePrincipalForm","Purchases:purshasePrincipalForm");
$router->post("/purshaseSecondaryForm","Purchases:purshaseSecondaryForm");
$router->post("/purshaseGeracao","Purchases:purshaseGeracao");


$router->group(null);
$router->get("/controle-licencas","LicenseControl:read");
$router->post("/controle-licencas/adicionar","LicenseControl:create");
$router->post("/controle-licencas/alterar","LicenseControl:update");
$router->post("/controle-licencas/deletar","LicenseControl:delete");
$router->post("/controle-licencas/search","LicenseControl:search");


$router->group(null);
$router->post("/services-performed","SendsWhatsapp:servicesPerformed");
$router->get("/services-performed/envios", "SendsWhatsapp:envios");
$router->get("/services-performed/NFnaoEscrituradas","SendsWhatsapp:NFnaoEscrituradas");
$router->get("/services-performed/ItensSemGiro","SendsWhatsapp:ItensSemGiro");
$router->get("/services-performed/indicadoresPDF","SendsWhatsapp:indicatorsPDF");
$router->get("/services-performed/indicadoresNegativosPDF","SendsWhatsapp:indicadoresNegativosPDF");
$router->get("/services-performed/indicadoresCoberturaPDF","SendsWhatsapp:indicadoresCoberturaPDF");
$router->get("/services-performed/indicadoresSimplesPDF","SendsWhatsapp:simples");
$router->get("/services-performed/comparativoDesossa","SendsWhatsapp:comparativoDesossa");
$router->get("/services-performed/indicadoresContasPagar","SendsWhatsapp:indicadoresContasPagar");
$router->get("/services-performed/indicadoresMargemEntrada","SendsWhatsapp:indicadoresMargemEntrada");
$router->get("/services-performed/indicadresMargemEntradaConsinco","SendsWhatsapp:indicadresMargemEntradaConsinco");
$router->get("/services-performed/indicadoresCheckins","SendsWhatsapp:indicadoresCheckins");
$router->get("/services-performed/indicadoresNotasLoja","SendsWhatsapp:indicadoresNotasLoja");
$router->get("/services-performed/indicadoresOperadoresNotas","SendsWhatsapp:indicadoresOperadoresNotas");
$router->get("/services-performed/indicadoresOperadoresNotasHora","SendsWhatsapp:indicadoresOperadoresNotasHora");
$router->get("/services-performed/indicadoresCadastroUsuarios","SendsWhatsapp:indicadoresCadastroUsuarios");
$router->get("/services-performed/indicadoresPendenteCliente", "SendsWhatsapp:indicadoresPendenteCliente");



$router->get("/services-performed/sendIndicadoresPDF","SendsWhatsapp:sendIndicatorsPDF");
$router->get("/services-performed/sendIndicadoresCoberturaPDF","SendsWhatsapp:sendIndicadoresCoberturaPDF");
$router->get("/services-performed/sendIndicadoresNegativosPDF","SendsWhatsapp:sendIndicadoresNegativosPDF");
$router->get("/services-performed/sendSimples","SendsWhatsapp:sendSimples");
$router->get("/services-performed/sendItensSemGiro","SendsWhatsapp:sendItensSemGiro");
$router->get("/services-performed/sendNFnaoEscrituradas","SendsWhatsapp:sendNFnaoEscrituradas");
$router->get("/services-performed/sendComparativoDesossa","SendsWhatsapp:sendComparativoDesossa");
$router->get("/services-performed/sendindIcadoresContasPagar","SendsWhatsapp:sendindIcadoresContasPagar");
$router->get("/services-performed/sendIndicadoresMargemEntrada","SendsWhatsapp:sendIndicadoresMargemEntrada");
$router->get("/services-performed/sendIndicadoresMargemEntradaConsinco","SendsWhatsapp:sendIndicadoresMargemEntradaConsinco");
$router->get("/services-performed/sendIndicadoresNotasLoja","SendsWhatsapp:sendIndicadoresNotasLoja");
$router->get("/services-performed/sendIndicadoresOperadoresNotasHora","SendsWhatsapp:sendIndicadoresOperadoresNotasHora");
$router->get("/services-performed/sendIndicadoresOperadoresChamadosHora","SendsWhatsapp:sendIndicadoresOperadoresChamadosHora");
$router->get("/services-performed/sendIndicadoresCheckins","SendsWhatsapp:sendIndicadoresCheckins");
$router->get("/services-performed/sendIndicadoresCadastroUsuarios","SendsWhatsapp:sendIndicadoresCadastroUsuarios");
$router->get("/services-performed/buscaCadastroConsinco","SendsWhatsapp:buscaCadastroConsinco");
$router->get("/services-performed/sendIndicadoresPendenteCliente","SendsWhatsapp:sendIndicadoresPendenteCliente");


$router->group(null);
$router->get("/alteraEstoque", "Attendance:alteraEstoque");
$router->get("/alteraEstoque2", "Attendance:alteraEstoque2");
$router->get("/alteraEstoque3", "Attendance:alteraEstoque3");

$router->group(null);
$router->get("/codigointerno","Attendance:codigoInterno");

$router->group(null);
$router->post("/acesso","SendsWhatsapp:acesso");


$router->group(null);
$router->get("/kwDifference","KwDifferenceController:kwDifference","kwDifference");
$router->post("/kwDifference/create","KwDifferenceController:createKwDifference","createKwDifference");
$router->post("/kwDifference/read","KwDifferenceController:readKwDifference","readKwDifference");
$router->get("/kwDifference/readOne","KwDifferenceController:readKwDifferenceOne","readKwDifferenceOne");
$router->post("/kwDifference/update","KwDifferenceController:updateKwDifference","updateKwDifference");
$router->post("/kwDifference/delete","KwDifferenceController:deleteKwDifference","deleteKwDifference");
/*
 * ERROS
 */

$router->group("ooops");

$router->get("/{errcode}","Web:error");

$router->dispatch();

if($router->error()){

    $router->redirect("/ooops/{$router->error()}");

}