<?php 
session_start();
$v->layout("administrator/adm_theme");
?>
<!-- <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script> -->
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Diferenças KW x RMS</h1>
        <p class="lead">Diferença KW x RMS - Registro de conclusão</p>
    </div>
</div>
<div class="container">
    <form action="/kwDifference/read" method="POST" class="row g-4">
        <div class="col-auto">
            <label for="staticEmail2" class="visually-hidden">Operador</label>
            <select name="operator" class="form-select" aria-label="Default select example">
                <option selected disabled>Selecione Operador ...</option>
                <?php foreach($users as $user): ?>
                <option value="<?=$user->id?>"><?=$user->name?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-auto">
            <label for="staticEmail2" class="visually-hidden">Loja</label>
            <input class="form-control" name="store" type="number" placeholder="Loja">
        </div>
        <div class="col-auto">
            <label for="staticEmail2" class="visually-hidden">Data</label>
            <input class="form-control" name="date" type="date">
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-success mb-3">Buscar</button>
        </div>
        <div class="col-auto">
            <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal"
                data-bs-target="#adicionar">Adicionar
                +</button>
        </div>
    </form>
</div>
<?php if ($read === true): ?>
<div class="table-responsive">
    <table class="table align-middle">
        <thead>
            <tr>
                <th>Id</th>
                <th>Cliente</th>
                <th>Operador</th>
                <th>Loja</th>
                <th>Status</th>
                <th>Data Diferença</th>
                <th>Data Notificação</th>
                <th>Remover</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($rows as $row): ?>
            <tr>
                <td><a href="<?=URL_BASE?>/kwDifference/readOne?id=<?=$row->id?>"><?=$row->id?></a></td>
                <td><?=$row->getClient()->name?></td>
                <td><?=$row->getOperator()->name?></td>
                <td><?=$row->store_id?></td>
                <td><?=$row->status?></td>
                <td><?=$row->difference_date?></td>
                <td><?=$row->updated_at?></td>
                <td>
                    <form name="delete<?=$row->id?>">
                        <input type="hidden" name="id" value="<?=$row->id?>">
                        <button type="submit" class="btn btn-danger">X</button>
                    </form>
                </td>
            </tr>
            <script>
                $('form[name="delete<?=$row->id?>"]').submit(function (event) {
                    event.preventDefault();

                    $.ajax({
                        url: '/kwDifference/delete',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.sucesso === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.msg,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.msg,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
<div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Adicionar Atividade</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form name="adicionar">
                <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input class="form-control" name="store" type="number">
                    </div>
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Data Diferença:</label>
                        <input class="form-control" name="date" type="date">
                    </div>
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Print da diferença:</label>
                        <input class="form-control" name="date" type="file">
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('form[name="adicionar"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/kwDifference/create',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.sucesso === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        timer: 600,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });
</script>