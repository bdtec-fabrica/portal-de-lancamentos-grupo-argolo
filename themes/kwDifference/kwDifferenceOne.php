<?php 
session_start();
$v->layout("administrator/adm_theme");
?>
<div class="jumbotron jumbotron-fluid">
  <div class="container text-center">
    <h1 class="display-4">Diferenças KW x RMS</h1>
    <p class="lead">Diferença KW x RMS - Registro de conclusão</p>
  </div>
</div>
<div class="container" style="width:600px">
  <form name="update">
    <div class="mb-3 row">
      <label for="staticEmail" class="col-sm-2 col-form-label">Id</label>
      <div class="col-sm-10">
        <input type="text" readonly class="form-control-plaintext" id="staticEmail" name="id" value="<?=$rows->id?>">
      </div>
    </div>
    <div class="mb-3 row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Loja</label>
      <div class="col-sm-10">
        <input type="number" readonly class="form-control" name="store" value="<?=$rows->store_id?>">
      </div>
    </div>
    <div class="mb-3 row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Data Diferença</label>
      <div class="col-sm-10">
        <input type="text" readonly class="form-control" name="date" value="<?=$rows->difference_date?>">
      </div>
    </div>
    <div class="mb-3 row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Cliente</label>
      <div class="col-sm-10">
        <input type="text" readonly class="form-control" name="client" value="<?=$rows->getClient()->name?>">
      </div>
    </div>
    <div class="mb-3 row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Operador</label>
      <div class="col-sm-10">
        <select name="operator" class="form-control">
          <option value="<?=$rows->operator_id?>"><?=$rows->getOperator()->name?></option>
          <?php foreach($users as $operator): ?>
          <option value="<?=$operator->id?>"><?=$operator->name?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="mb-3 row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Status</label>
      <div class="col-sm-10">
        <select name="status" class="form-control">
          <option value="D">Divergente</option>
          <option value="R">Reprocessando</option>
          <option value="C">Corrigido</option>
        </select>
      </div>
    </div>
    <div class="mb-3 row">
      <div class="col-sm-10">
        <button class="btn btn-success" type="submit">Gravar</button>
      </div>
    </div>
  </form>
</div>

<div class="d-flex flex-column align-items-stretch flex-shrink-0 bg-white">
  <a href="/" class="d-flex align-items-center flex-shrink-0 p-3 link-dark text-decoration-none border-bottom">
    <svg class="bi pe-none me-2" width="30" height="24">
      <use xlink:href="#bootstrap" /></svg>
    <span class="fs-5 fw-semibold">Interações: </span>
  </a>
  <div class="list-group list-group-flush border-bottom scrollarea">
    <?php foreach($rows->getAttachment as $attachment): ?>
    <a class="list-group-item list-group-item-action py-3 lh-sm">
      <div class="d-flex w-100 align-items-center justify-content-between">
        <strong class="mb-1"><?=$attachment->getUser()->name?></strong>
        <small><?=$attachment->created_at?></small>
      </div>
      <div class="col-10 mb-1 small">            <img style="width:350px;margin-bottom:1%;"
                src="<?="<?=URL_BASE?>/$attachment->attachment"?>" class="card-img-top"
                alt="..."></div>
    </a>
    <?php endforeach; ?>
  </div>
</div>
<script>
  $('form[name="update"]').submit(function (event) {
    event.preventDefault();

    $.ajax({
      url: '/kwDifference/update',
      type: 'post',
      data: $(this).serialize(),
      dataType: 'json',
      success: function (response) {

        if (response.sucesso === true) {
          swal({
            title: "Bispo & Dantas",
            text: response.msg,
            timer: 20000,
            icon: "success",
            showCancelButton: false,
            showConfirmButton: false,
            type: "success"
          });
          
        } else {
          swal({
            title: "Bispo & Dantas",
            text: response.msg,
            icon: "error",
            showCancelButton: false,
            showConfirmButton: false,
            type: "danger"
          });
        }

      }
    })
  });
</script>