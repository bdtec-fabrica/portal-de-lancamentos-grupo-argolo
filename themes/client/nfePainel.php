<?php
$v->layout("client/_theme");
?>

<main class="main_content">
    <style>
        .campo {
            font-size: 70%;
        }

    </style>
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Painel Nota Fiscal Eletronica BDTec#</h1>
                <p class="lead"></p>
            </div>
        </div>
        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
            data-bs-toggle="collapse" data-bs-target="#multiCollapseExample1" aria-expanded="false"
            aria-controls="multiCollapseExample1">Filtro
            ↴</button>
    </div>
    <div class="collapse multi-collapse container" id="multiCollapseExample1">
        <form action="/nfSefaz/search" method="post">
            <div class="row">
                <div class="form-group col-md-1">
                    <span>#Codigo</span>
                    <input name="id" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Número</span>
                    <input name="nfNumber" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>CPF</span>
                    <input name="cnpj" class="form-control" type="text">
                </div>
                <div class="form-group col-md-5">
                    <span>Chave Acesso</span>
                    <input name="accessKey" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Emissão inicial</span>
                    <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Emissão final</span>
                    <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Status</span>
                    <select name="status" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <option value="0">Não Postada</option>
                        <option value="2">Postada</option>
                    </select>
                </div>
                <div class="form-group col-md-1">
                    <span>Loja</span>
                    <input class="form-control" name="store">
                </div>
                <div class="form-group col-md-3">
                    <span>Manifestação no portal</span>
                    <select class="form-control" name="evento">
                        <option value='' selected diabled>Selecione ...</option>
                        <option value="210200">Confirmação da Operação</option>
                        <option value="210210">Ciência da Operação</option>
                        <option value="210220">Desconhecimento da Operação</option>
                        <option value="210240">Operação não Realizada</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <span>Lançadas no RMS</span>
                    <select class="form-control" name="lancada">]
                        <option value='' selected diabled>Selecione ...</option>
                        <option value="1">Não</option>
                        <option value='2'>Sim</option>
                    </select>
                </div>

            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <button class='btn btn-primary' onclick='openWin()'>Busca Sefaz</button>
    <div class="container" style="margin-bottom:3%;margin-top:3%">
        <?php if (!$c2):?>
        <button type="button" class="btn btn-secondary btn-lg" data-bs-toggle="modal"
            data-bs-target="#adicionar">Postagem Manual</button>
        <?php endif;?>
    </div>
    <button class="btn btn-outline-success" style="margin:2%" data-bs-toggle="modal"
        data-bs-target="#exampleModal">Adicionar Notas
        Conferência Cega</button>
    <script language='javascript' type='text/javascript'>
        function openWin() {
            myWindow = window.open('<?=URL_BASE?>/nfSefaz/sefazSearch/<?=$store?>',
                '_blank', 'width=300, height=300');
        }
    </script>
    <button type="button" style="margin:2%" class="btn btn-primary" data-bs-toggle="modal"
        data-bs-target="#pendentColletion">Conferências</button>

    <div class="modal fade" id="pendentColletion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Conferências</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#Id</th>
                                    <th>Loja</th>
                                    <th>Razão Social do Fornecedor</th>
                                    <th>Inclusão</th>
                                    <th>Romaneio</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($collectionHeaders as $collectionHeader): 
                              
                            ?>
                                <tr <?php if ($collectionHeader->getStatus == 'E'):
                                ?> class="bg-danger" <?php elseif($collectionHeader->getStatus == 'P'):
                                ?> class="bg-warning" <?php elseif($collectionHeader->getStatus == 'C'):
                                ?> class="bg-success" <?php endif;
                                ?>>
                                    <td><a href="/blindColletion/<?=$collectionHeader->id?>"
                                            target="blank"><?=$collectionHeader->id?></a>
                                    </td>
                                    <td><?=$collectionHeader->store_id?></td>
                                    <td><?=$collectionHeader->razao?></td>
                                    <td><?=$collectionHeader->created_at?></td>
                                    <td><a href="/romaneio/<?=$collectionHeader->id?>" target="blank">Romaneio</a></td>
                                </tr>
                                <?php 
                        endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Postagem Manual</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="formUpload" id="formUpload" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Número do documento:</label>
                        <input required name="nfNumber" type="number" class="form-control" id="recipient-name">
                        <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                            id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                        <input name="accessKey" type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                        <select name="type" required class="form-control target" id="target">
                            <option value="1">Avulsa</option>
                            <option value="8">Emissão Própria</option>
                        </select>
                    </div>

                    <div class="form-group" id="planilha-cadastro" style="display:none">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">CPF:</label>
                            <input name="cnpj" id="cad" type="text" class="form-control" maxlength="11" disabled>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Valor Total:</label>
                            <input name="totalCost" type="number" class="form-control totalCost" value="0" id="cad" disabled>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="campo">Codigo Interno</th>
                                    <th class="campo" style="width: 200px;">Descrição</th>
                                    <th class="campo">Quantidade</th>
                                    <th class="campo">Custo Atual</th>
                                    <th class="campo">Custo Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="linhas" class="linhas">
                                <?php 
                                $count = 1;
                                foreach ($aa2cestq as $cestq): ?>
                                <tr>
                                    <td><input readonly id="cad" value="<?=$cestq->GIT_COD_ITEM?>"
                                            class="form-control campo" name="ean[<?=$count?>]" disabled></td>
                                    <td><input value="<?=trim($cestq->GIT_DESCRICAO)?>" class="form-control campo"
                                            disabled></td>
                                    <td><input id="quantidade<?=$cestq->GIT_COD_ITEM?>" class="form-control campo" name="amount[<?=$count?>]"></td>
                                    <td><input id="custo<?=$cestq->GIT_COD_ITEM?>" onblur="calcular<?=$cestq->GIT_COD_ITEM?>()"class="form-control campo" ></td>
                                    <td><input id="custoT<?=$cestq->GIT_COD_ITEM?>" onblur="tcalcular<?=$cestq->GIT_COD_ITEM?>();tGeralCalcular<?=$cestq->GIT_COD_ITEM?>()" class="form-control campo" type="number" name="cost[<?=$count?>]"></td>
                                    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                            data-bs-target="#exampleModal<?=$cestq->GIT_COD_ITEM?>">+</button>
                                    </td>
                                </tr>
                                <script>
                                    let quantidade<?=$cestq->GIT_COD_ITEM?> = document.getElementById('quantidade<?=$cestq->GIT_COD_ITEM?>').value.replace(',','.');
                                    let custo<?=$cestq->GIT_COD_ITEM?> = document.getElementById('custo<?=$cestq->GIT_COD_ITEM?>').value.replace(',','.');
                                    let custoT<?=$cestq->GIT_COD_ITEM?> = document.getElementById('custoT<?=$cestq->GIT_COD_ITEM?>').value.replace(',','.');
                                    console.log(quantidade<?=$cestq->GIT_COD_ITEM?>);

                                    function  calcular<?=$cestq->GIT_COD_ITEM?>() {
                                        if (document.getElementById('quantidade<?=$cestq->GIT_COD_ITEM?>').value > 0 &&  document.getElementById('custo<?=$cestq->GIT_COD_ITEM?>').value > 0) {
                                            let custo = document.getElementById('quantidade<?=$cestq->GIT_COD_ITEM?>').value *  document.getElementById('custo<?=$cestq->GIT_COD_ITEM?>').value;
                                            document.getElementById('custoT<?=$cestq->GIT_COD_ITEM?>').value = custo;
                                        }   
                                    }

                                    function  tcalcular<?=$cestq->GIT_COD_ITEM?>() {
                                        if (document.getElementById('quantidade<?=$cestq->GIT_COD_ITEM?>').value > 0 &&  document.getElementById('custoT<?=$cestq->GIT_COD_ITEM?>').value > 0) {
                                            let custo = document.getElementById('custoT<?=$cestq->GIT_COD_ITEM?>').value /  document.getElementById('quantidade<?=$cestq->GIT_COD_ITEM?>').value;
                                            document.getElementById('custo<?=$cestq->GIT_COD_ITEM?>').value = custo;
                                        }   
                                    }

                                    function tGeralCalcular<?=$cestq->GIT_COD_ITEM?>() {
                                        let totalGeral = document.getElementsByName('totalCost');
                                        let custoTotal = document.getElementById('custoT<?=$cestq->GIT_COD_ITEM?>'); 
                                        if (parseFloat(custoTotal.value)){
                                            totalGeral[0].value = parseFloat(totalGeral[0].value) + parseFloat(custoTotal.value);
                                            console.log(totalGeral[0].value);
                                        }
                                        
                                    }
                                </script>
                                <div class="modal fade" id="exampleModal<?=$cestq->GIT_COD_ITEM?>" tabindex="-1"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Detalhamento
                                                    <?=$cestq->GIT_COD_ITEM?> - <?=trim($cestq->GIT_DESCRICAO)?></h5>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row row-cols-2">
                                                    <div class="col">Codigo Interno</div>
                                                    <div class="col"><input readonly id="cad" value="<?=$cestq->GIT_COD_ITEM?>" class="form-control campo" disabled></div>
                                                    <div class="col">Descrição</div>
                                                    <div class="col"><input value="<?=trim($cestq->GIT_DESCRICAO)?>"class="form-control campo" disabled></div>
                                                    <div class="col">Quantidade</div>
                                                    <div class="col"><input id="cad" class="form-control campo" disabled></div>
                                                    <div class="col">Embalagem</div>
                                                    <div class="col"><input id="cad" class="form-control campo" disabled></div>
                                                    <div class="col">Custo Anterior</div>
                                                    <div class="col"><input value="<?=str_replace(".",",",round($cestq->GET_CUS_ULT_ENT,2))?>" class="form-control campo" disabled></div>
                                                    <div class="col">Custo Atual</div>
                                                    <div class="col"><input id="cad" class="form-control campo"
                                                            disabled></div>
                                                    <div class="col">Custo Total</div>
                                                    <div class="col"><input id="cad" type="number" class="form-control campo"
                                                            disabled></div>
                                                    <div class="col">Preço atual</div>
                                                    <div class="col"><input
                                                            value="<?=str_replace(".",",",round($cestq->PRE_PRECO,2))?>"
                                                            class="form-control campo" disabled></div>
                                                    <div class="col">Margem Atual</div>
                                                    <div class="col"><input
                                                            value="<?=str_replace(".",",",round($cestq->MARGEM,2))?>"
                                                            class="form-control campo" disabled></div>
                                                    <div class="col">Margem Desejada</div>
                                                    <div class="col"><input id="cad" class="form-control campo"
                                                            disabled></div>
                                                    <div class="col">Preço Calculado</div>
                                                    <div class="col"><input id="cad" class="form-control campo"
                                                            disabled></div>
                                                    <div class="col">Estoque</div>
                                                    <div class="col"><input
                                                            value="<?=str_replace(".",",",round($cestq->GET_ESTOQUE,2))?>"
                                                            class="form-control campo" disabled></div>
                                                    <div class="col">Saida Media</div>
                                                    <div class="col"><input
                                                            value="<?=str_replace(".",",",round($cestq->SAIDA_MEDIA,2))?>"
                                                            class="form-control campo" disabled></div>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                    $count++;
                                    endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                        <script>
                            $("#target").change(function () {
                                var select = document.getElementById('target');
                                var value = select.options[select.selectedIndex].value;
                                if (value == 8) {
                                    $("input[id='cad']").prop('required', true);
                                    $("input[id='cad']").prop('disabled', false);
                                    $("#planilha-cadastro").css("display", "");

                                } else {
                                    $("input[id='cad']").prop('required', false);
                                    $("input[id='cad']").prop('disabled', true);
                                    $("#planilha-cadastro").css("display", "none");

                                }

                                if (value == 1 || value == 7 || value == 18 || value == 17 || value == 2 ||
                                    value == 6) {
                                    if ($("#emergencial").length != 0) {

                                    } else {
                                        $('#criticidade').append(
                                            '<option id="emergencial" value="4">Emergencial</option>');
                                    }

                                } else {
                                    $('#emergencial').remove();
                                }

                            });
                            $("#custo<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $( "#custoT<?=$cestq->GIT_COD_ITEM?>" ).val( ($( "#quantidade<?=$cestq->GIT_COD_ITEM?>" ).val() * $( "#embalagem<?=$cestq->GIT_COD_ITEM?>" ).val() )* $( "#custo<?=$cestq->GIT_COD_ITEM?>" ).val() );
                                $( "#2custo<?=$cestq->GIT_COD_ITEM?>").val($( "#custo<?=$cestq->GIT_COD_ITEM?>").val());
                            });
                            $("#custoT<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $( "#custo<?=$cestq->GIT_COD_ITEM?>" ).val( $( "#custoT<?=$cestq->GIT_COD_ITEM?>" ).val() / ($( "#quantidade<?=$cestq->GIT_COD_ITEM?>" ).val() / $( "#embalagem<?=$cestq->GIT_COD_ITEM?>" ).val()));
                                $( "#2custoT<?=$cestq->GIT_COD_ITEM?>").val($( "#custoT<?=$cestq->GIT_COD_ITEM?>").val());
                            });
                            $("#quantidade<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#2quantidade<?=$cestq->GIT_COD_ITEM?>").val($("#quantidade<?=$cestq->GIT_COD_ITEM?>").val());
                            });
                            $("#margemCalculada<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#precoCalculado<?=$cestq->GIT_COD_ITEM?>").val(($("#2custo<?=$cestq->GIT_COD_ITEM?>").val() /((100 - $("#margemCalculada<?=$cestq->GIT_COD_ITEM?>").val() - <?=$cestq->TFIS_ALIQ_ICM?> - <?=$cestq->PIS_COFINS?>)/100)));
                            });
                            
                            $("#precoCalculado<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#margemCalculada<?=$cestq->GIT_COD_ITEM?>").val((100-(($("#2custo<?=$cestq->GIT_COD_ITEM?>").val()/$("#precoCalculado<?=$cestq->GIT_COD_ITEM?>").val())*100))+((<?=$cestq->PIS_COFINS?>-(<?=$cestq->PIS_COFINS?>*(<?=$cestq->TFIS_ALIQ_ICM?>/100)))+<?=$cestq->TFIS_ALIQ_ICM?>));
                            })
                        </script>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Quantidade de itens</label>
                            <input required name="itemsQuantity" type="number" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">1º Vencimento</label>
                            <input required name="dueDate1" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">2º Vencimento</label>
                            <input required name="dueDate2" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">3º Vencimento</label>
                            <input required name="dueDate3" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">4º Vencimento</label>
                            <input required name="dueDate4" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">5º Vencimento</label>
                            <input required name="dueDate5" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">6º Vencimento</label>
                            <input required name="dueDate6" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Observações:</label>
                            <textarea required name="note" class="form-control" id="message-text"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Prioridade:</label>
                            <select class="form-select" name="nfeOperation">
                                <option value="99">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                                <input name="files[]" multiple type="file" class="form-control" id="inputGroupFile01">
                            </div>
                        </div>
                        <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                            id="porcentagem">0%</span>
                        <div id="resposta" style="width:100%"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#btnEnviar').click(function () {
                $('#formUpload').ajaxForm({
                    uploadProgress: function (event, position, total, percentComplete) {
                        $('progress').attr('value', percentComplete);
                        $('#porcentagem').html(percentComplete + '%');
                    },
                    success: function (data) {
                        $('progress').attr('value', '100');
                        $('#porcentagem').html('100%');
                        if (data.success == true) {
                            $('#resposta').html(
                                '<div class="alert alert-success" role="alert">' + data
                                .message + '</div>');
                            document.location.reload(true);
                        } else {
                            $('#resposta').html(
                                '<div class="alert alert-danger" role="alert">' + data
                                .message + '</div>');
                        }
                    },
                    error: function () {
                        $('#resposta').html(
                            '<div class="alert alert-danger" role="alert">Erro ao enviar requisição!!!</div>'
                        );
                    },
                    dataType: 'json',
                    url: '/request/addRequest',
                    resetForm: true
                }).submit();
            })
        });                
    </script>
    <script language='javascript' type='text/javascript'>
        function openWin() {
            myWindow = window.open('<?=URL_BASE?>/nfSefaz/sefazSearch/<?=$store?>',
                '_blank', 'width=300, height=300');
        }
    </script>
    <?php if ($paginator): ?>
    <h5 style="margin-left:2%;margin-top:2%">Página <?= $paginator->page(); ?> de <?= $paginator->pages(); ?> </h5>
    <?php endif; ?>


    <div style="font-size: small;">
        <table class="table table-hover sortable">
            <thead>
                <tr>
                    <th>Loja</th>
                    <th>Número</th>
                    <th>Operação</th>
                    <th>Ultima manifestação</th>
                    <th>Razão Social</th>
                    <th>Emissão</th>
                    <th>Chave de Acesso</th>
                    <th>Valor Contabil</th>
                    <th>Visualizar</th>
                    <th>Postar</th>
                    <th>Manifestar</th>
                </tr>
            </thead>
            <tbody>

                <?php
            if ($nfes):
            foreach ($nfes as $nfe):
                
                if($status == 2):
                    if($nfe->getStatus == 2):
                ?>
                <tr <?php 
                    $badManifestation = false;
                    foreach ($nfe->getLogManifestation() as $log):
                        if ($log->manifestation == 210220 or $log->manifestation == 210240):
                            $badManifestation = true;
                        else:
                            $badManifestation = false;
                        endif;
                    endforeach;
                        if ($badManifestation):?> style="background:red" <?php elseif ($nfe->retorno()): ?>
                    style="background:purple" <?php elseif ($nfe->getStatus == 2): ?> style="background:green"
                    <?php endif;?>>
                    <option value="210220">Desconhecimento da Operação</option>
                    <option value="210240">Operação não Realizada</option>
                    <td><?= $nfe->store_id?></td>
                    <td><?= $nfe->nfe_number?></td>
                    <td data-bs-toggle="tooltip" data-bs-placement="top" title="<?=$nfe->retorno()?>">
                        <?=substr($nfe->natop,0,10)?></td>
                    <td><?= $nfe->getmanifestation?></td>
                    <td><?= $nfe->getFornecedor?></td>
                    <td><?= $nfe->emission_date?></td>
                    <td><?= $nfe->access_key?></td>
                    <td><?= $nfe->valorcontabil?></td>

                    <td><button <?php if ($nfe->getStatus == 2):
                ?> disabled <?php endif;  ?> type="button" data-bs-placement="top" title="Visualizar"
                            class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#visualizar<?=$nfe->id?>"
                            data-whatever="@mdo"><i class="ion ion-eye"></i></button>
                        <div class="modal fade" id="visualizar<?=$nfe->id?>" tabindex="-1"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $nfe->nfe_number?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $nfe->access_key?>">
                                        </div>

                                        <?php if (file_exists(__DIR__."/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml")): ?>
                                        <div style="margin: 2%;">
                                            <div class="row border">
                                                <div class="col border">
                                                    Evento
                                                </div>
                                                <div class="col border">
                                                    Usuário
                                                </div>
                                                <div class="col border">
                                                    Data
                                                </div>
                                            </div>
                                            <?php foreach ($nfe->getLogManifestation() as $log):?>
                                            <div class="row border">
                                                <div class="col border">
                                                    <?= $log->getManifestationName()?>
                                                </div>
                                                <div class="col border">
                                                    <?= $log->getName()->name?>
                                                </div>
                                                <div class="col border">
                                                    <?= $log->created_at?>
                                                </div>
                                            </div>
                                            <?php endforeach;?>
                                        </div>
                                        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#multiCollapseExample<?=$nfe->id?>" aria-expanded="false"
                                            aria-controls="multiCollapseExample1">Itens
                                            ↴</button>

                                    </div>
                                    <div class="collapse multi-collapse container"
                                        id="multiCollapseExample<?=$nfe->id?>">
                                        <div class="form-group">
                                            <div class="container">
                                                <?php foreach ($nfe->getItems as $det): ?>
                                                <div class="row border">
                                                    <div class="col border">
                                                        <?= $det->prod->cProd?>
                                                    </div>
                                                    <div class="col border">
                                                        <?= $det->prod->cEAN?>
                                                    </div>
                                                    <div class="col border">
                                                        <?= $det->prod->xProd?>
                                                    </div>
                                                </div> <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row container">
                                            <a download style="text-decoration:none;margin-bottom:5%"
                                                href="<?="/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml"?>"><button
                                                    class="btn btn-outline-success">XML <i
                                                        class="ion ion-android-download"></i></button></a>
                                            <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                                <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                                <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                                <a>
                                                    <button style="margin-bottom:5%" type="submit"
                                                        class="btn btn-outline-warning">PDF <i
                                                            class="ion ion-android-download"></i></button></a>
                                            </form>
                                            <form name="reprocessar<?=$nfe->id?>">
                                                <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                                <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                                <a>
                                                    <button type="submit" class="btn btn-outline-info">Troca Codigo<i
                                                            class="ion ion-android-download"></i></button></a>
                                            </form>
                                            <script>
                                                $('form[name="reprocessar<?=$nfe->id?>"]').submit(function (event) {
                                                    event.preventDefault();
                                                    $.ajax({
                                                        url: '/request/trocaCodigo/nfe',
                                                        type: 'post',
                                                        data: $(this).serialize(),
                                                        dataType: 'json',
                                                        success: function (response) {
                                                            if (response.success === true) {
                                                                swal({
                                                                    title: "Bispo & Dantas",
                                                                    text: response.message,
                                                                    timer: 20000,
                                                                    icon: "success",
                                                                    showCancelButton: false,
                                                                    showConfirmButton: false,
                                                                    type: "success"
                                                                });
                                                            } else {
                                                                swal({
                                                                    title: "Bispo & Dantas",
                                                                    text: response.message,
                                                                    icon: "error",
                                                                    showCancelButton: false,
                                                                    showConfirmButton: false,
                                                                    type: "danger"
                                                                });
                                                            }
                                                        }
                                                    })
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <?php endif; ?>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Voltar</button>
                                </div>
                            </div>
                        </div>
    </div>

    </td>
    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap">Postar</button>
        <div class="modal fade" id="exampleModal<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Postar NFE9</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="adicionar<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do
                                    documento:</label>
                                <input required readonly name="nfNumber" value="<?= $nfe->nfe_number?>" type="number"
                                    class="form-control" id="recipient-name">
                                <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="accessKey" type="text" readonly value="<?= $nfe->access_key?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de
                                    documento:</label>
                                <select class="form-select" name="type" required>
                                    <option value="">Selecione um tipo</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div cl ass="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 1:</label>
                                <input name="dueDate1" id="vencimento1" onblur="validadata()" type="date"
                                    value="<?=$nfe->getDueDate[0]->dVenc?>" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 2:</label>
                                <input name="dueDate2" type="date" value="<?= $nfe->getDueDate[1]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 3:</label>
                                <input name="dueDate3" type="date" value="<?= $nfe->getDueDate[2]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 4:</label>
                                <input name="dueDate4" type="date" value="<?= $nfe->getDueDate[3]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 5:</label>
                                <input name="dueDate5" type="date" value="<?= $nfe->getDueDate[3]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 6:</label>
                                <input name="dueDate6" type="date" value="<?= $nfe->getDueDate[3]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Informar Codigos Interno:</label>
                                <div class="row g-3" id="internalCode<?=$nfe->id?>">
                                    <div class="col-auto">
                                        <input type="text" class="form-control" name="code[0]">
                                    </div>
                                    <div class="col-auto">
                                        <a class="btn btn-secondary" id="add<?=$nfe->id?>">+<a>
                                    </div>
                                </div>                           
                            </div>
                            <script>
                                i = 1;   
                                $( "#add<?=$nfe->id?>" ).click(function() {
                                    $( "#internalCode<?=$nfe->id?>" ).append( "<div class='row g-3' id='hello<?=$nfe->id?>" + i + "'>"+
                                    "<div class='col-auto'>" +
                                    "<input type='text' class='form-control' name='code[" + i + "]'>" +
                                    "</div>" +
                                    "<div class='col-auto'>"+
                                    "<a class='btn btn-primary' onclick='$( `#hello<?=$nfe->id?>" + i + "` ).remove();'>-</a>" +
                                    "</div>" +
                                    "</div>" );            
                                    i++;
                                });
                            </script>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Observações6:</label>
                                <textarea name="note" class="form-control" id="message-text"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                <select class="form-select" name="nfeOperation">
                                    <option value="99">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                </div>
            </div>
            <script>
                $('form[name="adicionar<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    // document.getElementById('blanket').style.display = 'block';document.getElementById('aguarde').style.display = 'block';
                    $.ajax({
                        url: '/request/addRequestAuto',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    <td>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#manifestar<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap" style="font-size:80%">Manifestar</button>
        <div class="modal fade" id="manifestar<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Manifestar NFE</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="manifestacao<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Codigo Loja</label>
                                <input class="form-control" value="<?=$nfe->store_id?>" name="store" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Chave de Acesso</label>
                                <input class="form-control" value="<?=$nfe->access_key?>" name="key" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Tipo de Evento</label>
                                <select class="form-control" id="select<?=$nfe->id?>" name="evento"
                                    onchange="atribuir<?=$nfe->id?>()">
                                    <option value='' selected diabled>Selecione ...</option>
                                    <option value="210200">Confirmação da Operação</option>
                                    <option value="210210">Ciência da Operação</option>
                                    <option value="210220">Desconhecimento da Operação</option>
                                    <option value="210240">Operação não Realizada</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Justificativa</label>
                                <textarea name="xJust" class="form-control" id="text<?=$nfe->id?>" rows="3" required
                                    readonly></textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                    <script>
                        function atribuir<?=$nfe->id?>(){
                            var select = document.getElementById('select<?=$nfe->id?>');
                            var value = select.options[select.selectedIndex].value;

                            if (value == 210200) {
                                document.getElementById('text<?=$nfe->id?>').value =
                                    "Nota Fiscal Escriturada com Sucesso";
                            } else if (value == 210210) {
                                document.getElementById('text<?=$nfe->id?>').value =
                                    "Empresa ciente da operacao desta NFe";
                            } else if (value == 210220) {
                                document.getElementById('text<?=$nfe->id?>').value =
                                    "Empresa desconhece esta operacao desta NFe";
                            } else if (value == 210240) {
                                document.getElementById('text<?=$nfe->id?>').value =
                                    "A operação referente a esta NFe não foi realizada";
                            }

                        }
                    </script>
                </div>
            </div>
            <script>
                $('form[name="manifestacao<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/request/manifestacao/nfe',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    </tr>
    <?php       endif;
            elseif($status == 0):
                if($nfe->getStatus != 2):
                    ?>
    <tr <?php 
                    foreach ($nfe->getLogManifestation() as $log):
                        if ($log->manifestation == 210220 or $log->manifestation == 210240):
                            $badManifestation = true;
                        else:
                            $badManifestation = false;
                        endif;
                    endforeach;
                        if ($badManifestation):?> style="background:red" <?php elseif ($nfe->retorno()): ?>
        style="background:purple" <?php elseif ($nfe->getStatus == 2): ?> style="background:green" <?php endif;?>>

        <td><?= $nfe->store_id?></td>
        <td><?= $nfe->nfe_number?></td>
        <td data-bs-toggle="tooltip" data-bs-placement="top" title="<?=$nfe->retorno()?>"><?= substr($nfe->natop,0,10)?>
        </td>
        <td><?= $nfe->getmanifestation?></td>
        <td><?= $nfe->getFornecedor?></td>
        <td><?= $nfe->emission_date?></td>
        <td><?= $nfe->access_key?></td>
        <td><?= $nfe->valorcontabil?></td>

        <td><button type="button" data-bs-placement="top" title="Visualizar" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#visualizar<?=$nfe->id?>"
                data-whatever="@mdo"><i class="ion ion-eye"></i></button>
            <div class="modal fade" id="visualizar<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do
                                    documento:</label>
                                <input name="title" disabled type="text" class="form-control" id="recipient-name"
                                    value="<?= $nfe->nfe_number?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="title" disabled type="text" class="form-control" id="recipient-name"
                                    value="<?= $nfe->access_key?>">
                            </div>

                            <?php if (file_exists(__DIR__."/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml")): ?>
                            <div style="margin: 2%;">
                                <div class="row border">
                                    <div class="col border">
                                        Evento
                                    </div>
                                    <div class="col border">
                                        Usuário
                                    </div>
                                    <div class="col border">
                                        Data
                                    </div>
                                </div>
                                <?php foreach ($nfe->getLogManifestation() as $log):?>
                                <div class="row border">
                                    <div class="col border">
                                        <?= $log->getManifestationName()?>
                                    </div>
                                    <div class="col border">
                                        <?= $log->getName()->name?>
                                    </div>
                                    <div class="col border">
                                        <?= $log->created_at?>
                                    </div>
                                </div>
                                <?php endforeach;?>
                            </div>
                            <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
                                data-bs-toggle="collapse" data-bs-target="#multiCollapseExample<?=$nfe->id?>"
                                aria-expanded="false" aria-controls="multiCollapseExample1">Itens
                                ↴</button>

                        </div>
                        <div class="collapse multi-collapse container" id="multiCollapseExample<?=$nfe->id?>">
                            <div class="form-group">
                                <div class="container">
                                    <?php foreach ($nfe->getItems as $det): ?>
                                    <div class="row border">
                                        <div class="col border">
                                            <?= $det->prod->cProd?>
                                        </div>
                                        <div class="col border">
                                            <?= $det->prod->cEAN?>
                                        </div>
                                        <div class="col border">
                                            <?= $det->prod->xProd?>
                                        </div>
                                    </div> <?php endforeach; ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row container">
                                <a download style="text-decoration:none;margin-bottom:5%"
                                    href="<?="/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml"?>"><button
                                        class="btn btn-outline-success">XML <i
                                            class="ion ion-android-download"></i></button></a>
                                <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                    <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                    <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                    <a>
                                        <button style="margin-bottom:5%" type="submit"
                                            class="btn btn-outline-warning">PDF <i
                                                class="ion ion-android-download"></i></button></a>
                                </form>
                                <form name="reprocessar<?=$nfe->id?>">
                                    <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                    <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                    <a>
                                        <button type="submit" class="btn btn-outline-info">Troca Codigo<i
                                                class="ion ion-android-download"></i></button></a>
                                </form>
                                <script>
                                    $('form[name="reprocessar<?=$nfe->id?>"]').submit(function (event) {
                                        event.preventDefault();
                                        $.ajax({
                                            url: '/request/trocaCodigo/nfe',
                                            type: 'post',
                                            data: $(this).serialize(),
                                            dataType: 'json',
                                            success: function (response) {
                                                if (response.success === true) {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        timer: 20000,
                                                        icon: "success",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "success"
                                                    });
                                                } else {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        icon: "error",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "danger"
                                                    });
                                                }
                                            }
                                        })
                                    });
                                </script>
                            </div>
                        </div>
                        <?php endif; ?>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Voltar</button>
                    </div>
                </div>
            </div>
            </div>

        </td>
        <td><button type="button" class="btn btn-primary" data-bs-toggle="modal"
                data-bs-target="#exampleModal<?=$nfe->id?>" data-bs-whatever="@getbootstrap">Postar</button>

            <div class="modal fade" id="exampleModal<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Postar NFE8</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form name="adicionar<?=$nfe->id?>">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Número do
                                        documento:</label>
                                    <input required readonly name="nfNumber" value="<?= $nfe->nfe_number?>"
                                        type="number" class="form-control" id="recipient-name">
                                    <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                    <input name="accessKey" type="text" readonly value="<?= $nfe->access_key?>"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Tipo de
                                        documento:</label>
                                    <select class="form-select" name="type" required>
                                        <option value="">Selecione um tipo</option>
                                        <?php foreach ($nfeType as $type): ?>
                                        <option value="<?=$type->id?>"><?=$type->name?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">

                                    <label for="recipient-name" class="col-form-label">Vencimento 1:</label>
                                    <input name="dueDate1" type="date" value="<?=$nfe->getDueDate[0]->dVenc?>"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 2:</label>
                                    <input name="dueDate2" type="date" value="<?= $nfe->getDueDate[1]->dVenc?>"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 3:</label>
                                    <input name="dueDate3" type="date" value="<?= $nfe->getDueDate[2]->dVenc?>"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 4:</label>
                                    <input name="dueDate4" type="date" value="<?= $nfe->getDueDate[3]->dVenc?>"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 5:</label>
                                    <input name="dueDate5" type="date" value="<?= $nfe->getDueDate[4]->dVenc?>"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 6:</label>
                                    <input name="dueDate6" type="date" value="<?= $nfe->getDueDate[5]->dVenc?>"
                                        class="form-control" id="recipient-name">
                                </div>
                                
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Observações:</label>
                                    <textarea name="note" class="form-control" id="message-text"></textarea>
                                </div>
                                <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Informar Codigos Interno:</label>
                                <div class="row g-3" id="internalCode<?=$nfe->id?>">
                                    <div class="col-auto">
                                        <input type="text" class="form-control" name="code[0]">
                                    </div>
                                    <div class="col-auto">
                                        <a class="btn btn-secondary" id="add<?=$nfe->id?>">+<a>
                                    </div>
                                </div>                           
                            </div>
                            <script>
                                i = 1;   
                                $( "#add<?=$nfe->id?>" ).click(function() {
                                    $( "#internalCode<?=$nfe->id?>" ).append( "<div class='row g-3' id='hello<?=$nfe->id?>" + i + "'>"+
                                    "<div class='col-auto'>" +
                                    "<input type='text' class='form-control' name='code[" + i + "]'>" +
                                    "</div>" +
                                    "<div class='col-auto'>"+
                                    "<a class='btn btn-primary' onclick='$( `#hello<?=$nfe->id?>" + i + "` ).remove();'>-</a>" +
                                    "</div>" +
                                    "</div>" );            
                                    i++;
                                });
                            </script>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                    <select class="form-select" name="nfeOperation">
                                        <option value="99">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                        </div>
                        </form>
                    </div>
                </div>
                <script>
                    $('form[name="adicionar<?=$nfe->id?>"]').submit(function (event) {
                        event.preventDefault();
                        // document.getElementById('blanket').style.display = 'block';document.getElementById('aguarde').style.display = 'block';
                        $.ajax({
                            url: '/request/addRequestAuto',
                            type: 'post',
                            data: $(this).serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success === true) {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        timer: 600,
                                        icon: "success",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "success"
                                    });
                                    document.location.reload(true);
                                } else {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        icon: "error",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "danger"
                                    });
                                }

                            }
                        })
                    });
                </script>
        </td>
        <td>
            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                data-bs-target="#manifestar<?=$nfe->id?>" data-bs-whatever="@getbootstrap">Manifestar</button>
            <div class="modal fade" id="manifestar<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Manifestar NFE</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form name="manifestacao<?=$nfe->id?>">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Codigo Loja</label>
                                    <input class="form-control" value="<?=$nfe->store_id?>" name="store" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Chave de Acesso</label>
                                    <input class="form-control" value="<?=$nfe->access_key?>" name="key" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Tipo de Evento</label>
                                    <select class="form-control" id="select<?=$nfe->id?>" name="evento"
                                        onchange="atribuir<?=$nfe->id?>()">
                                        <option value='' selected diabled>Selecione ...</option>
                                        <option value="210200">Confirmação da Operação</option>
                                        <option value="210210">Ciência da Operação</option>
                                        <option value="210220">Desconhecimento da Operação</option>
                                        <option value="210240">Operação não Realizada</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Justificativa</label>
                                    <textarea name="xJust" class="form-control" id="text<?=$nfe->id?>" rows="3" required
                                        readonly></textarea>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                        </div>
                        </form>
                        <script>
                            function atribuir<?=$nfe->id?>(){
                                var select = document.getElementById('select<?=$nfe->id?>');
                                var value = select.options[select.selectedIndex].value;

                                if (value == 210200) {
                                    document.getElementById('text<?=$nfe->id?>').value =
                                        "Nota Fiscal Escriturada com Sucesso";
                                } else if (value == 210210) {
                                    document.getElementById('text<?=$nfe->id?>').value =
                                        "Empresa ciente da operacao desta NFe";
                                } else if (value == 210220) {
                                    document.getElementById('text<?=$nfe->id?>').value =
                                        "Empresa desconhece esta operacao desta NFe";
                                } else if (value == 210240) {
                                    document.getElementById('text<?=$nfe->id?>').value =
                                        "A operação referente a esta NFe não foi realizada";
                                }

                            }
                        </script>
                    </div>
                </div>
                <script>
                    $('form[name="manifestacao<?=$nfe->id?>"]').submit(function (event) {
                        event.preventDefault();
                        $.ajax({
                            url: '/request/manifestacao/nfe',
                            type: 'post',
                            data: $(this).serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success === true) {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        timer: 600,
                                        icon: "success",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "success"
                                    });
                                    document.location.reload(true);
                                } else {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        icon: "error",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "danger"
                                    });
                                }

                            }
                        })
                    });
                </script>
        </td>
    </tr>


    <?php
                    endif;
                else:
                    ?>
    <tr <?php 
                    foreach ($nfe->getLogManifestation() as $log):
                        if ($log->manifestation == 210220 or $log->manifestation == 210240):
                            $badManifestation = true;
                        else:
                            $badManifestation = false;
                        endif;
                    endforeach;
                        if ($badManifestation):?> style="background:red" <?php elseif ($nfe->retorno()): ?>
        style="background:purple" <?php elseif ($nfe->getStatus == 2): ?> style="background:green" <?php endif;?>>

        <td><?= $nfe->store_id?></td>
        <td><?= $nfe->nfe_number?></td>
        <td data-bs-toggle="tooltip" data-bs-placement="top" title="<?=$nfe->retorno()?>"><?= substr($nfe->natop,0,10)?>
        </td>
        <td><?= $nfe->getmanifestation?>
        <td><?= $nfe->getFornecedor?></td>
        <td><?= $nfe->emission_date?></td>
        <td><?= $nfe->access_key?></td>
        <td><?= $nfe->valorcontabil?></td>

        <td><button <?php if ($nfe->getStatus == 2):
                ?> disabled <?php endif;  ?> type="button" data-bs-placement="top" title="Visualizar"
                class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#visualizar<?=$nfe->id?>"
                data-whatever="@mdo"><i class="ion ion-eye"></i></button>
            <div class="modal fade" id="visualizar<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do
                                    documento:</label>
                                <input name="title" disabled type="text" class="form-control" id="recipient-name"
                                    value="<?= $nfe->nfe_number?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="title" disabled type="text" class="form-control" id="recipient-name"
                                    value="<?= $nfe->access_key?>">
                            </div>

                            <?php if (file_exists(__DIR__."/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml")): ?>
                            <div style="margin: 2%;">
                                <div class="row border">
                                    <div class="col border">
                                        Evento
                                    </div>
                                    <div class="col border">
                                        Usuário
                                    </div>
                                    <div class="col border">
                                        Data
                                    </div>
                                </div>
                                <?php foreach ($nfe->getLogManifestation() as $log):?>
                                <div class="row border">
                                    <div class="col border">
                                        <?= $log->getManifestationName()?>
                                    </div>
                                    <div class="col border">
                                        <?= $log->getName()->name?>
                                    </div>
                                    <div class="col border">
                                        <?= $log->created_at?>
                                    </div>
                                </div>
                                <?php endforeach;?>
                            </div>
                            <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
                                data-bs-toggle="collapse" data-bs-target="#multiCollapseExample<?=$nfe->id?>"
                                aria-expanded="false" aria-controls="multiCollapseExample1">Itens
                                ↴</button>

                        </div>
                        <div class="collapse multi-collapse container" id="multiCollapseExample<?=$nfe->id?>">
                            <div class="form-group">
                                <div class="container">
                                    <?php foreach ($nfe->getItems as $det): ?>
                                    <div class="row border">
                                        <div class="col border">
                                            <?= $det->prod->cProd?>
                                        </div>
                                        <div class="col border">
                                            <?= $det->prod->cEAN?>
                                        </div>
                                        <div class="col border">
                                            <?= $det->prod->xProd?>
                                        </div>
                                    </div> <?php endforeach; ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row container">
                                <a download style="text-decoration:none;margin-bottom:5%"
                                    href="<?="/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml"?>"><button
                                        class="btn btn-outline-success">XML <i
                                            class="ion ion-android-download"></i></button></a>
                                <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                    <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                    <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                    <a>
                                        <button style="margin-bottom:5%" type="submit"
                                            class="btn btn-outline-warning">PDF <i
                                                class="ion ion-android-download"></i></button></a>
                                </form>
                                <form name="reprocessar<?=$nfe->id?>">
                                    <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                    <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                    <a>
                                        <button type="submit" class="btn btn-outline-info">Troca Codigo<i
                                                class="ion ion-android-download"></i></button></a>
                                </form>
                                <script>
                                    $('form[name="reprocessar<?=$nfe->id?>"]').submit(function (event) {
                                        event.preventDefault();
                                        $.ajax({
                                            url: '/request/trocaCodigo/nfe',
                                            type: 'post',
                                            data: $(this).serialize(),
                                            dataType: 'json',
                                            success: function (response) {
                                                if (response.success === true) {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        timer: 20000,
                                                        icon: "success",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "success"
                                                    });
                                                } else {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        icon: "error",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "danger"
                                                    });
                                                }
                                            }
                                        })
                                    });
                                </script>
                            </div>
                        </div>
                        <?php endif; ?>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Voltar</button>
                    </div>
                </div>
            </div>
            </div>

        </td>
        <td><button type="button" class="btn btn-primary" data-bs-toggle="modal"
                data-bs-target="#exampleModal<?=$nfe->id?>" data-bs-whatever="@getbootstrap">Postar</button>
            <div class="modal fade" id="exampleModal<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Postar NFE</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form name="adicionar<?=$nfe->id?>">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Número do
                                        documento:</label>
                                    <input required readonly name="nfNumber" value="<?= $nfe->nfe_number?>"
                                        type="number" class="form-control" id="recipient-name">
                                    <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                    <input name="accessKey" type="text" readonly value="<?= $nfe->access_key?>"
                                        class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Tipo de
                                        documento:</label>
                                    <select class="form-select" name="type" required>
                                        <option value="">Selecione um tipo</option>
                                        <?php foreach ($nfeType as $type): ?>
                                        <option value="<?=$type->id?>"><?=$type->name?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <script>
                                        function validadataa<?=$nfe->id?>(){
                                            var data = document.getElementById("vencimento1<?=$nfe->id?>")
                                                .value; // pega o valor do input

                                            if (data == null) {
                                                return true;
                                            }
                                            data = data.replace(/\//g,
                                                "-"); // substitui eventuais barras (ex. IE) "/" por hífen "-"
                                            var data_array = data.split("-"); // quebra a data em array


                                            // para o IE onde será inserido no formato dd/MM/yyyy
                                            if (data_array[0].length != 4) {
                                                data = data_array[2] + "-" + data_array[1] + "-" + data_array[
                                                    0]; // remonto a data no formato yyyy/MM/dd
                                            }


                                            // comparo as datas e calculo a idade
                                            var hoje = new Date();
                                            var nasc = new Date(data);
                                            var nasc2 = nasc;
                                            var idade = nasc.getFullYear() - hoje.getFullYear();
                                            var m = hoje.getMonth() - nasc.getMonth();
                                            if (m < 0 || (m === 0 && hoje.getDate() < nasc.getDate())) idade--;

                                            if (idade > 1 || idade < -1 || (nasc2.length > 10 && nasc2 !=
                                                    'Invalid Date')) {

                                                return false;
                                            } else {
                                                return true;
                                            }
                                        }

                                        function validadatab<?=$nfe->id?>(){
                                            var data = document.getElementById("vencimento2<?=$nfe->id?>")
                                                .value; // pega o valor do input

                                            if (data == null) {
                                                return true;
                                            }
                                            data = data.replace(/\//g,
                                                "-"); // substitui eventuais barras (ex. IE) "/" por hífen "-"
                                            var data_array = data.split("-"); // quebra a data em array


                                            // para o IE onde será inserido no formato dd/MM/yyyy
                                            if (data_array[0].length != 4) {
                                                data = data_array[2] + "-" + data_array[1] + "-" + data_array[
                                                    0]; // remonto a data no formato yyyy/MM/dd
                                            }


                                            // comparo as datas e calculo a idade
                                            var hoje = new Date();
                                            var nasc = new Date(data);
                                            var nasc2 = nasc;
                                            var idade = nasc.getFullYear() - hoje.getFullYear();
                                            var m = hoje.getMonth() - nasc.getMonth();
                                            if (m < 0 || (m === 0 && hoje.getDate() < nasc.getDate())) idade--;
                                            if (idade > 1 || idade < -1 || (nasc2.length > 10 && nasc2 !=
                                                    'Invalid Date')) {
                                                return false;
                                            } else {
                                                return true;
                                            }
                                        }

                                        function validadatac<?=$nfe->id?>(){
                                            var data = document.getElementById("vencimento3<?=$nfe->id?>")
                                                .value; // pega o valor do input

                                            if (data == null) {
                                                return true;
                                            }
                                            data = data.replace(/\//g,
                                                "-"); // substitui eventuais barras (ex. IE) "/" por hífen "-"
                                            var data_array = data.split("-"); // quebra a data em array


                                            // para o IE onde será inserido no formato dd/MM/yyyy
                                            if (data_array[0].length != 4) {
                                                data = data_array[2] + "-" + data_array[1] + "-" + data_array[
                                                    0]; // remonto a data no formato yyyy/MM/dd
                                            }


                                            // comparo as datas e calculo a idade
                                            var hoje = new Date();
                                            var nasc = new Date(data);
                                            var nasc2 = nasc;
                                            var idade = nasc.getFullYear() - hoje.getFullYear();
                                            var m = hoje.getMonth() - nasc.getMonth();
                                            if (m < 0 || (m === 0 && hoje.getDate() < nasc.getDate())) idade--;
                                            if (idade > 1 || idade < -1 || (nasc2.length > 10 && nasc2 !=
                                                    'Invalid Date')) {
                                                return false;
                                            } else {
                                                return true;
                                            }
                                        }
                                    </script>
                                    <label for="recipient-name" class="col-form-label">Vencimento 1:</label>
                                    <input name="dueDate1" type="date" id="vencimento1<?=$nfe->id?>" min="2022-01-01"
                                        value="<?=$nfe->getDueDate[0]->dVenc?>" class="form-control"
                                        id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 2:</label>
                                    <input name="dueDate2" type="date" id="vencimento2<?=$nfe->id?>" min="2022-01-01"
                                        value="<?= $nfe->getDueDate[1]->dVenc?>" class="form-control"
                                        id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 3:</label>
                                    <input name="dueDate3" type="date" id="vencimento3<?=$nfe->id?>" min="2022-01-01"
                                        value="<?= $nfe->getDueDate[2]->dVenc?>" class="form-control"
                                        id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 4:</label>
                                    <input name="dueDate5" type="date" id="vencimento4<?=$nfe->id?>" min="2022-01-01"
                                        value="<?= $nfe->getDueDate[3]->dVenc?>" class="form-control"
                                        id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 5:</label>
                                    <input name="dueDate5" type="date" id="vencimento5<?=$nfe->id?>" min="2022-01-01"
                                        value="<?= $nfe->getDueDate[4]->dVenc?>" class="form-control"
                                        id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Vencimento 6:</label>
                                    <input name="dueDate6" type="date" id="vencimento6<?=$nfe->id?>" min="2022-01-01"
                                        value="<?= $nfe->getDueDate[5]->dVenc?>" class="form-control"
                                        id="recipient-name">
                                </div>
                                <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Informar Codigos Interno:</label>
                                <div class="row g-3" id="internalCode<?=$nfe->id?>">
                                    <div class="col-auto">
                                        <input type="text" class="form-control" name="code[0]">
                                    </div>
                                    <div class="col-auto">
                                        <a class="btn btn-secondary" id="add<?=$nfe->id?>">+<a>
                                    </div>
                                </div>                           
                            </div>
                            <script>
                                i = 1;   
                                $( "#add<?=$nfe->id?>" ).click(function() {
                                    $( "#internalCode<?=$nfe->id?>" ).append( "<div class='row g-3' id='hello<?=$nfe->id?>" + i + "'>"+
                                    "<div class='col-auto'>" +
                                    "<input type='text' class='form-control' name='code[" + i + "]'>" +
                                    "</div>" +
                                    "<div class='col-auto'>"+
                                    "<a class='btn btn-primary' onclick='$( `#hello<?=$nfe->id?>" + i + "` ).remove();'>-</a>" +
                                    "</div>" +
                                    "</div>" );            
                                    i++;
                                });
                            </script>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Observações:</label>
                                    <textarea name="note" class="form-control" id="message-text"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                    <select class="form-select" name="nfeOperation">
                                        <option value="99">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                        </div>
                        </form>
                    </div>
                </div>
                <script>
                    vencimento = true;
                    $('form[name="adicionar<?=$nfe->id?>"]').submit(function (event) {
                        event.preventDefault();
                        if (validadataa<?=$nfe->id?>() == false) {
                            alert("Primeiro vencimento com data invalido!");
                            vencimento = false;

                        }
                        if (validadatab<?=$nfe->id?>() == false) {
                            alert("Segundo vencimento com data invalido!");
                            vencimento = false;

                        }
                        if (validadatac<?=$nfe->id?>() == false) {
                            alert("Terceiro vencimento com data invalido!");
                            vencimento = false;

                        }
                        if (vencimento != false) {
                            $.ajax({
                                url: '/request/addRequestAuto',
                                type: 'post',
                                data: $(this).serialize(),
                                dataType: 'json',
                                success: function (response) {
                                    if (response.success === true) {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            timer: 600,
                                            icon: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "success"
                                        });
                                        document.location.reload(true);
                                    } else {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            icon: "error",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "danger"
                                        });
                                    }

                                }
                            })
                        }

                    });
                </script>
        </td>
        <td>
            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                data-bs-target="#manifestar<?=$nfe->id?>" data-bs-whatever="@getbootstrap">Manifestar</button>
            <div class="modal fade" id="manifestar<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Manifestar NFE</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form name="manifestacao<?=$nfe->id?>">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Codigo Loja</label>
                                    <input class="form-control" value="<?=$nfe->store_id?>" name="store" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Chave de Acesso</label>
                                    <input class="form-control" value="<?=$nfe->access_key?>" name="key" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Tipo de Evento</label>
                                    <select class="form-control" id="select<?=$nfe->id?>" name="evento"
                                        onchange="atribuir<?=$nfe->id?>()">
                                        <option value='' selected diabled>Selecione ...</option>
                                        <option value="210200">Confirmação da Operação</option>
                                        <option value="210210">Ciência da Operação</option>
                                        <option value="210220">Desconhecimento da Operação</option>
                                        <option value="210240">Operação não Realizada</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Justificativa</label>
                                    <textarea name="xJust" class="form-control" id="text<?=$nfe->id?>" rows="3" required
                                        readonly></textarea>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                        </div>
                        </form>
                        <script>
                            function atribuir<?=$nfe->id?>() {
                                var select = document.getElementById('select<?=$nfe->id?>');
                                var value = select.options[select.selectedIndex].value;

                                if (value == 210200) {
                                    document.getElementById('text<?=$nfe->id?>').value =
                                        "Nota Fiscal Escriturada com Sucesso";
                                } else if (value == 210210) {
                                    document.getElementById('text<?=$nfe->id?>').value =
                                        "Empresa ciente da operacao desta NFe";
                                } else if (value == 210220) {
                                    document.getElementById('text<?=$nfe->id?>').value =
                                        "Empresa desconhece esta operacao desta NFe";
                                } else if (value == 210240) {
                                    document.getElementById('text<?=$nfe->id?>').value =
                                        "A operação referente a esta NFe não foi realizada";
                                }

                            }
                        </script>
                    </div>
                </div>
                <script>
                    $('form[name="manifestacao<?=$nfe->id?>"]').submit(function (event) {
                        event.preventDefault();
                        $.ajax({
                            url: '/request/manifestacao/nfe',
                            type: 'post',
                            data: $(this).serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success === true) {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        timer: 600,
                                        icon: "success",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "success"
                                    });
                                    document.location.reload(true);
                                } else {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        icon: "error",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "danger"
                                    });
                                }

                            }
                        })
                    });
                </script>
        </td>
    </tr>
    <?php       endif;
            endforeach;
            endif;
            ?>
    </tbody>
    </table>
    </div>
    <?php if ($paginator):
    ?>
    <div style="margin-left:2%;margin-bottom:5%"><?= $paginator->render();?><div>
            <?php endif;
    ?>
</main>