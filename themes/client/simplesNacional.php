<?php 
session_start();
$v->layout("client/_theme");

?>
<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Levantamento de compras sem pegar crédito ICMS (simples nacional) <?=$store?></h1>
                <p class="lead">Periodo <?=$date1?> até <?=$date2?></p>
            </div>
        </div>
    </div>
    <div class="alert alert-danger" role="alert">
        ICMS PERDIDO <?= number_format($total,2,",",".")?>
    </div>
    <table class="table sortable" style="font-size: 80%">
        <thead>
            <tr>
                <th scope="col">Referencia</th>
                <th scope="col">Descrição</th>
                <th scope="col">NCM</th>
                <th scope="col">CFOP</th>
                <th scope="col">Quantidade</th>
                <th scope="col">Valor</th>
                <th scope="col">Base de Calculo</th>
                <th scope="col">Aliquota</th>
                <th scope="col">Valor ICMS</th>
                <th scope="col">Número NFe</th>
                <th scope="col">Razão Social</th>
                <th scope="col">Data da Emissão</th>
                <th scope="col">Chave de Acesso</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($linhas as $linha): ?>
            <tr>
                <td><?=$linha['referencia']?></td>
                <td><?=$linha['descricao']?></td>
                <td><?=$linha['ncm']?></td>
                <td><?=$linha['cfop']?></td>
                <td><?=str_replace(".",",",$linha['quantidade'])?></td>
                <td><?=str_replace(".",",",$linha['valor'])?></td>
                <td><?=str_replace(".",",",$linha['base'])?></td>
                <td><?=$linha['aliquota']?></td>
                <td><?=str_replace(".",",",$linha['valorIcms'])?></td>
                <td><?=$linha['numero']?></td>
                <td><?=$linha['razao']?></td>
                <td><?=$linha['emissao']?></td>
                <td><?=$linha['chave']?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</main>