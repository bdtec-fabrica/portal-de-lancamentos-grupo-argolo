<?php 
session_start();
$v->layout("client/_theme");
?>
<ul class="nav justify-content-center" style="padding:2%">
    <li class="nav-item" style="margin:2%">
        <button class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#adicionar"
            data-bs-whatever="@mdo">+Converter</button>
    </li>
</ul>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Desossa</h1>
        <p class="lead">Desagregação das peças de carne das partes do Boi</p>
    </div>
</div>
<div class="container alert-secondary" style="margin-top:2%">

    <form action="/desossa/search" method="post">
        <div class="row">
            <div class="form-group col-md-1">
                <span>Loja</span>
                <input name="store" class="form-control" type="text">
            </div>
            <div class="form-group col-md-2">
                <span>CNPJ</span>
                <input name="cnpj" class="form-control" type="text">
            </div>
            <div class="form-group col-md-2">
                <span>Código</span>
                <input name="code" class="form-control" type="text">
            </div>
            <div class="form-group col-md-2">
                <span>Referencia</span>
                <input name="reference" class="form-control" type="text">
            </div>
            <div class="form-group col-md-5">
                <span>Descrição</span>
                <input id="desc" name="description" class="form-control" type="text">
            </div>
            <div class="form-group col-md-5">
                <span>Tipo</span>
                <select class="form-control col-md-5" name="type">
                    <?php foreach($boningOxType as $type):
                    ?>
                    <option value="<?=$type->code?>"><?=$type->name?></option>
                    <?php endforeach;
                    ?>
                </select>
            </div>
            <div class="form-group col-md-1">
                <span style="visibility:hidden">Filtrar</span>
                <button type="submit" class="btn btn-secondary">Filtrar</button>
            </div>
        </div>
    </form>
    <button type="button" class="btn btn-primary btn-lg" style="margin:2%" data-bs-toggle="modal"
        data-bs-target="#novo" data-bs-whatever="@mdo">+Adicionar</button>
</div>
<div class="modal fade" id="novo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nova Peça</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="novo">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" id="store" class="form-control" name="store" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Codigo:</label>
                        <input type="text" id="code" class="form-control" name="code" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Referencia:</label>
                        <input type="text" id="reference" class="form-control" name="reference" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">CNPJ:</label>
                        <input type="text" id="cnpj" class="form-control" name="cnpj" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Descrição:</label>
                        <input type="text" id="description" class="form-control" name="description" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tipo:</label>
                        <select class="form-control" name="type">
                        <?php foreach($boningOxType as $type):
                    ?>
                    <option value="<?=$type->code?>"><?=$type->name?></option>
                    <?php endforeach;
                    ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Percentual:</label>
                        <input type="text" id="percentage" class="form-control" name="percentage" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Preço:</label>
                        <input type="text" id="price" class="form-control" name="price" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-success">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('form[name="novo"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/desossa/add',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.success === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.message,
                        timer: 600,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.message,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });
</script>
<?php if($boningOxs):?>
<div class="tabela container">
    <table class="table table-dark table-striped">
        <thead class="thead-dark">
            <tr>
                <th>Loja</th>
                <th>Codigo</th>
                <th>Referência</th>
                <th>CNPJ</th>
                <th>Descrição</th>
                <th>Tipo</th>
                <th>Percentual</th>
                <th>Preço</th>
                <th>Excluir</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($boningOxs as $boningOx):?>
            <tr>
                <form id="form" method="get">
                    <th><?=$boningOx->store_id?></th>
                    <th><?=$boningOx->code?></th>
                    <th><?=$boningOx->reference?></th>
                    <th><?=$boningOx->cnpj?></th>
                    <th><?=$boningOx->description?></th>
                    <th><?=$boningOx->getTypeName?></th>
                    <td id="<?=$boningOx->id."-percentage"?>"><?=$boningOx->percentage?></td>
                    <td id="<?=$boningOx->id."-price"?>"><?=$boningOx->price?></td>
                    
                </form>
                <td><form name="delete<?=$boningOx->id?>">
                        <input type="hidden" value="<?=$boningOx->id?>" name="id">
                        <button class="btn btn-danger" type="submit">Excluir</button>
            </form></td>
                <script>
                    $('form[name="delete<?=$boningOx->id?>"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/desossa/delete',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.success === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.message,
                        timer: 600,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.message,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });
    </script>
            </tr>
            <?php $totalPercentage += $boningOx->percentage;
        endforeach;  ?>
        </tbody>
    </table>
    <?php if (round($totalPercentage) == 100): ?>
        <form target="_blank" action="/desossa/copiar" method="POST">
            <input type="hidden" value="<?=$ids?>" name="ids">
        <button type="submit" class="btn btn-primary" style="margin:2%">Copiar</button>
        </form>
    
    <?php endif; ?>
    <div class="alert alert-dark" role="alert">Total Percentual: <?=round($totalPercentage)?></div>
</div>
<script>
    
    $(function () {
        $("td").dblclick(function () {
            var conteudoOriginal = $(this).text();
            var id = $(this).attr("id");

            $(this).addClass("celulaEmEdicao");
            $(this).html("<input type='text' name='" + id + "' class='form-control' value='" +
                conteudoOriginal + "' />");
            $(this).children().first().focus();

            $(this).children().first().keypress(function (e) {
                if (e.which == 13) {

                    var novoConteudo = $(this).val();
                    $(this).parent().text(novoConteudo);
                    $(document).ready(function () {
                        $('#form').ajaxForm({

                            success: function (data) {
                                if (data.sucesso == true) {
                                    $(this).parent().removeClass(
                                        "celulaEmEdicao");
                                    document.location.reload(true);
                                }
                            },
                            dataType: 'json',
                            url: '/desossa/edit/edit?data=' + id + '$' + novoConteudo,
                            resetForm: true
                        }).submit();
                    })

                }
            });

            $(this).children().first().blur(function () {


                $(this).parent().text(conteudoOriginal);
                $(this).parent().removeClass("celulaEmEdicao");

            });
        });
    });
    
</script>
<?php endif;?>
<?php if ($id2):
?>
<div class="tabela container">
    <div class="tabela">
        <h3 style="text-align:center">Informações do XML<br></h3>
    </div>
    <table class="table table-dark table-striped">
        <thead class="thead-dark">
            <tr>
                <th>Referencia</th>
                <th>Descricão</th>
                <th>Qtd</th>
                <th>Valor Unitario</th>
                <th>Valor Total</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($id2->getOxPurchaseHistory() as $xml):?>
            <tr>
                <td><?=$xml->reference?></td>
                <td><?=$xml->description?></td>
                <td><?=$xml->amount?></td>
                <td><?=$xml->cost?></td>
                <td><?=$xml->amount*$xml->cost?></td>
            </tr>
            <?php endforeach;  ?>
        </tbody>
    </table>
</div>
<!-- <div class="tabela container" style="margin-top:5%">
    <div class="tabela">
        <h3 style="text-align:center">Informações Gerais do XML<br></h3>
    </div>
    <table class="table table-dark table-striped">
        <thead class="thead-dark">
            <th>Custo Total</th>
            <th>Preço Total</th>
            <th>Margem</th>
        </thead>
        <tbody>
            <tr>
            <tr>
        </tbody>
    </table>
</div> -->
<div class="tabela container" style="margin-top:5%;margin-bottom:5%">
    <div class="tabela">
        <h3 style="text-align:center">Informações do XML<br></h3>
    </div>
    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th>Referencia</th>
                <th>Código</th>
                <th>Descricao</th>
                <th>Percentual</th>
                <th>Quantidade</th>
                <th>Custo</th>
                <th>Preco</th>
                <th>Margem</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($id2->getOxDeboningHistory() as $xml):?>
            <tr>
                <td><?=$xml->reference?></td>
                <td><?=$xml->code?></td>
                <td><?=$xml->description?></td>
                <td><?=$xml->percentage?></td>
                <td><?=$xml->amount?></td>
                <td><?=$xml->cost?></td>
                <td><?=$xml->getPrice?></td>
                <td><?php if ($xml->getPrice > 0):
                    echo 100-($xml->cost/$xml->getPrice)*100;
                endif;?></td>
            </tr>
            <?php endforeach;  ?>
        </tbody>
    </table>
    <form action="/desossa/convert" method="post">
        <input type="hidden" name="id" value="<?=$id2->id?>" />
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="5925" id="flexCheckDefault" name="CFOP">
            <label class="form-check-label" for="flexCheckDefault">
                5925?
            </label>
        </div>
        
        <button class="btn btn-success" type="submit">Gerar</button>
    </form>

    <?php elseif($xml):
    ?>
    <div class="alert alert-success container"><a href="<?="/../../source/XML_DESOSSA_NOVO/xml.xml"?>" download>XML gerado
            com sucesso</a></div>
    <?php elseif($error):
    ?>
    <div class="alert alert-danger container"><a><?=$error?></a></div>
    <?php elseif($items):
    ?>
    <?php endif;
    ?>



    <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Importar XML</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="/desossa/update" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Loja:</label>
                            <input type="text" id="store" class="form-control" name="store" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="file" type="file" accept=".xml" class="form-control" id="inputGroupFile01">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-success">Salvar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script>
        $('form[name="adicionar"]').submit(function (event) {
            event.preventDefault();

            $.ajax({
                url: '/usuarios/usersAdd',
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function (response) {

                    if (response.success === true) {
                        swal({
                            title: "Bispo & Dantas",
                            text: response.message,
                            timer: 600,
                            icon: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            type: "success"
                        });
                        document.location.reload(true);
                    } else {
                        swal({
                            title: "Bispo & Dantas",
                            text: response.message,
                            icon: "error",
                            showCancelButton: false,
                            showConfirmButton: false,
                            type: "danger"
                        });
                    }

                }
            })
        });
    </script>