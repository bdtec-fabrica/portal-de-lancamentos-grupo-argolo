<link rel="stylesheet" href="<?= url("/themes/css/bootstrap.min.css");?>">
  <link rel="stylesheet" href="<?= url("/themes/css/nav.css");?>">
  <script src="<?= url("/themes/js/jquery-3.5.1.min.js");?>"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
    integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
    integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous">
  </script>
  <script src="<?= url("/themes/js/jquery.form.min.js");?>"></script>
  <script src="<?= url("/themes/js/sweetalert.min.js");?>"></script>
  <script src="<?= url("/themes/js/nav.js");?>"></script>
  <script src="<?= url("/themes/js/sortable.js");?>"></script>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Pedido de Compra</h1>
        <p class="lead">CNPJ: <?=$purchase->cnpj?></p>
        <p class="lead">Razão Social : <?=$purchase->razao?></p>
    </div>
</div>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Referencia</th>
            <th scope="col">EAN Principal</th>
            <th scope="col">Descrição</th>
            <th scope="col">Custo liquido</th>
            <th scope="col">Custo bruto</th>
        </tr>
    </thead>
    <tbody>

        <?php 
        foreach($purchase->purchaseItems as $item):
        ?>
        <tr>
            <th scope="row"><?=$item->reference?></th>
            <td><?=$item->ean?></td>
            <td><?=$item->description?></td>
            <td><?=$item->cost?></td>
            <td><?=$item->amount?></td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
    <tfoot>
        <tr>
            <th>Total do Pedido R$ <?=$purchase->cost?></th>
        </tr>
    </tfoot>
</table>