<?php 
session_start();
$v->layout("client/_theme");
?>
<div class = "container" >
<form action="/desossa/copiaExec" method="post">
        <div class="row">
            <div class="form-group col-md-1">
                <span>Loja</span>
                <input name="store" class="form-control" type="text">
            </div>
            <div class="form-group col-md-2">
                <span>CNPJ</span>
                <input name="cnpj" class="form-control" type="text">
            </div>
            <div class="form-group col-md-2">
                <span>Referencia</span>
                <input name="reference" class="form-control" type="text">
                <input type="hidden" name="ids" value="<?=$ids?>">
            </div>
            <div class="form-group col-md-1">
                <span style="visibility:hidden">Copiar</span>
                <button type="submit" class="btn btn-secondary">Copiar</button>
            </div>
        </div>
    </form>
</div>

<div class="tabela container">
    <table class="table table-dark table-striped">
        <thead class="thead-dark">
            <tr>
                <th>Loja</th>
                <th>Codigo</th>
                <th>Referência</th>
                <th>CNPJ</th>
                <th>Descrição</th>
                <th>Tipo</th>
                <th>Percentual</th>
                <th>Preço</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($boningOxs as $boningOx):?>
            <tr>
                <th><?=$boningOx->store_id?></th>
                <th><?=$boningOx->code?></th>
                <th><?=$boningOx->reference?></th>
                <th><?=$boningOx->cnpj?></th>
                <th><?=$boningOx->description?></th>
                <th><?=$boningOx->getTypeName?></th>
                <td id="<?=$boningOx->id."-percentage"?>"><?=$boningOx->percentage?></td>
                <td id="<?=$boningOx->id."-price"?>"><?=$boningOx->price?></td>
            </tr>
            <?php $totalPercentage += $boningOx->percentage;
        endforeach;  ?>
        </tbody>
    </table>
    <div class="alert alert-dark" role="alert">Total Percentual: <?=round($totalPercentage)?></div>
</div>