<?php 
session_start();
$v->layout("client/_theme");

?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Resultados</h1>
        <p class="lead">Apuração do resultado dos produtos</p>
    </div>
</div>
<table class="table table-hover sortable" style="text-align:right">
    <?php 

if ($audit != '0,00'):
 
    if ($type == 3):
        ?>
    <thead>
        <tr>
            <th style="min-width:25px">Loja</th>
            <th style="min-width:25px">Código</th>
            <th style="min-width:25px">Descrição</th>
            <th style="min-width:25px">Estoque Inicial</th>
            <th style="min-width:25px">Estoque Inicial(R$)</th>
            <th style="min-width:25px">Entradas</th>
            <th style="min-width:25px">Entradas(R$)</th>
            <th style="min-width:25px">Saidas</th>
            <th style="min-width:25px">Saidas(R$)</th>
            <th style="min-width:25px">Estoque Final</th>
            <th style="min-width:25px">Estoque Final(R$)</th>
            <th style="min-width:25px">Resultado</th>
            <th style="min-width:25px">Resultado(R$)</th>
        </tr>
    </thead>
    <tbody>
        <?php 
    foreach ($butcheryRegistration as $code):
        if($itensMovimentacao == 1) :
            if($code->getInventories($date1,$storeId) != '0,00' || $code->getPurchases($date1,$date2,$movimento,$storeId) != '0,00' || $code->getSales($date1,$date2,$movimento,$storeId) != '0,00'
            || $code->getInventories($date2,$storeId) != '0,00' ):
        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventories($date1,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date1,$storeId); ?></td>
            <td><?=$code->getPurchases($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getPurchasesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSales($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSalesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventories($date2,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date2,$storeId); ?></td>
            <td><?=$code->result($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->resultCost($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php
            endif;
        else:
        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventories($date1,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date1,$storeId); ?></td>
            <td><?=$code->getPurchases($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getPurchasesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSales($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSalesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventories($date2,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date2,$storeId); ?></td>
            <td><?=$code->result($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->resultCost($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php
        endif;

    endforeach; ?>
    <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><?=$totalInventoryInitial?></td>
            <td><?= $totalPurchases ?></td>
            <td><?=$totalSales?></td>
            <td><?=$totalInventoryFinal ?></td>
            <td><?=$totalResult?></td>
        </tr>
    </tbody>
    <?php
    else:
        ?>
    <thead>
        <tr>
            <th style="min-width:25px">Loja</th>
            <th style="min-width:25px">Código</th>
            <th style="min-width:25px">Descrição</th>
            <th style="min-width:25px">Estoque Inicial</th>
            <th style="min-width:25px">Entradas</th>
            <th style="min-width:25px">Saidas</th>
            <th style="min-width:25px">Estoque Final</th>
            <th style="min-width:25px">Resultado</th>
        </tr>
    </thead>
    <tbody>
        <?php 
    foreach ($butcheryRegistration as $code):
        if ($type == 1):
            if($itensMovimentacao == 1) :
                if($code->getInventories($date1,$storeId) != '0,00' || $code->getPurchases($date1,$date2,$movimento,$storeId) != '0,00' || $code->getSales($date1,$date2,$movimento,$storeId) != '0,00'
                    || $code->getInventories($date2,$storeId) != '0,00' ):

                    

        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventories($date1,$storeId)?></td>
            <td><?=$code->getPurchases($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSales($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventories($date2,$storeId); ?></td>
            <td><?=$code->result($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php
        $totalInventoryInitial += $code->getInventories($date1,$storeId);
        $totalInventoryFinal += $code->getInventories($date2,$storeId);
        $totalPurchases += $code->getPurchases($date1,$date2,$movimento,$storeId);
        $totalSales += $code->getSales($date1,$date2,$movimento,$storeId);
        $totalResult += $code->result($date1,$date2,$movimento,$storeId);
                endif;
            else:
        ?>
        <tr>
            <td><?=$storeId?>"</td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventories($date1,$storeId)?></td>
            <td><?=$code->getPurchases($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSales($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventories($date2,$storeId); ?></td>
            <td><?=$code->result($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php 
            $totalInventoryInitial += $code->getInventories($date1,$storeId);
            $totalInventoryFinal += $code->getInventories($date2,$storeId);
            $totalPurchases += $code->getPurchases($date1,$date2,$movimento,$storeId);
            $totalSales += $code->getSales($date1,$date2,$movimento,$storeId);
            $totalResult += $code->result($date1,$date2,$movimento,$storeId);
                endif;
        elseif ($type == 2):
            if($itensMovimentacao == 1) :
                if($code->getInventories($date1,$storeId) != '0,00' || $code->getPurchases($date1,$date2,$movimento,$storeId) != '0,00' || $code->getSales($date1,$date2,$movimento,$storeId) != '0,00'
                    || $code->getInventories($date2,$storeId) != '0,00' ):

        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventoriesCost($date1,$storeId)?></td>
            <td><?=$code->getPurchasesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSalesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date2,$storeId); ?></td>
            <td><?=$code->resultCost($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php
                endif;
            else:
        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventoriesCost($date1,$storeId)?></td>
            <td><?=$code->getPurchasesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSalesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date2,$storeId); ?></td>
            <td><?=$code->resultCost($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php endif;
        endif;
    endforeach; ?>
    <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><?=$totalInventoryInitial?></td>
            <td><?= $totalPurchases ?></td>
            <td><?=$totalSales?></td>
            <td><?=$totalInventoryFinal ?></td>
            <td><?=$totalResult?></td>
        </tr>
    </tbody>
    <?php
    endif;
    else:
    if ($type == 3):
        ?>
    <thead>
        <tr>
            <th style="min-width:25px">Loja</th>
            <th style="min-width:25px">Código</th>
            <th style="min-width:25px">Descrição</th>
            <th style="min-width:25px">Estoque Inicial</th>
            <th style="min-width:25px">Estoque Inicial(R$)</th>
            <th style="min-width:25px">Entradas</th>
            <th style="min-width:25px">Entradas(R$)</th>
            <th style="min-width:25px">Saidas</th>
            <th style="min-width:25px">Saidas(R$)</th>
            <th style="min-width:25px">Estoque Final</th>
            <th style="min-width:25px">Estoque Final(R$)</th>
            <th style="min-width:25px">Resultado</th>
            <th style="min-width:25px">Resultado(R$)</th>
        </tr>
    </thead>
    <tbody>
        <?php 
    foreach ($butcheryRegistration as $code):
        if($itensMovimentacao == 1) :
            if($code->getInventories($date1,$storeId) != '0,00' || $code->getPurchases($date1,$date2,$movimento,$storeId) != '0,00' || $code->getSales($date1,$date2,$movimento,$storeId) != '0,00'
            || $code->getInventories($date2,$storeId) != '0,00' ):
        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventories($date1,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date1,$storeId); ?></td>
            <td><?=$code->getPurchases($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getPurchasesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSales($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSalesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventories($date2,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date2,$storeId); ?></td>
            <td><?=$code->result($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->resultCost($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php
            endif;
        else:
        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventories($date1,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date1,$storeId); ?></td>
            <td><?=$code->getPurchases($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getPurchasesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSales($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSalesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventories($date2,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date2,$storeId); ?></td>
            <td><?=$code->result($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->resultCost($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php
        endif;
    endforeach; ?>
    <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><?=$totalInventoryInitial?></td>
            <td><?= $totalPurchases ?></td>
            <td><?=$totalSales?></td>
            <td><?=$totalInventoryFinal ?></td>
            <td><?=$totalResult?></td>
        </tr>
    </tbody>
    <?php
    else:
        ?>
    <thead>
        <tr>
            <th style="min-width:25px">Loja</th>
            <th style="min-width:25px">Código</th>
            <th style="min-width:25px">Descrição</th>
            <th style="min-width:25px">Estoque Inicial</th>
            <th style="min-width:25px">Entradas</th>
            <th style="min-width:25px">Saidas</th>
            <th style="min-width:25px">Estoque Final</th>
            <th style="min-width:25px">Resultado</th>
        </tr>
    </thead>
    <tbody>
        <?php 
    foreach ($butcheryRegistration as $code):
        if ($type == 1):
            if($itensMovimentacao == 1) :
                if($code->getInventories($date1,$storeId) != '0,00' || $code->getPurchases($date1,$date2,$movimento,$storeId) != '0,00' || $code->getSales($date1,$date2,$movimento,$storeId) != '0,00'
                    || $code->getInventories($date2,$storeId) != '0,00' ):

        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventories($date1,$storeId)?></td>
            <td><?=$code->getPurchases($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSales($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventories($date2,$storeId); ?></td>
            <td><?=$code->result($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php

        $totalInventoryInitial += $code->getInventories($date1,$storeId);
        $totalInventoryFinal += $code->getInventories($date2,$storeId);
        $totalPurchases += $code->getPurchases($date1,$date2,$movimento,$storeId);
        $totalSales += $code->getSales($date1,$date2,$movimento,$storeId);
        $totalResult += $code->result($date1,$date2,$movimento,$storeId);
                endif;
            else:
        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventories($date1,$storeId)?></td>
            <td><?=$code->getPurchases($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSales($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventories($date2,$storeId); ?></td>
            <td><?=$code->result($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php
        $totalInventoryInitial += $code->getInventories($date1,$storeId);
        $totalInventoryFinal += $code->getInventories($date2,$storeId);
        $totalPurchases += $code->getPurchases($date1,$date2,$movimento,$storeId);
        $totalSales += $code->getSales($date1,$date2,$movimento,$storeId);
        $totalResult += $code->result($date1,$date2,$movimento,$storeId);
            endif;
        elseif ($type == 2):
            if($itensMovimentacao == 1) :
                if($code->getInventories($date1,$storeId) != '0,00' || $code->getPurchases($date1,$date2,$movimento,$storeId) != '0,00' || $code->getSales($date1,$date2,$movimento,$storeId) != '0,00'
                    || $code->getInventories($date2,$storeId) != '0,00' ):

        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventoriesCost($date1,$storeId)?></td>
            <td><?=$code->getPurchasesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSalesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date2,$storeId); ?></td>
            <td><?=$code->resultCost($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php
                endif;
            else:
        ?>
        <tr>
            <td><?=$storeId?></td>
            <td><?=$code->code; ?></td>
            <td><?=$code->description; ?></td>
            <td><?=$code->getInventoriesCost($date1,$storeId)?></td>
            <td><?=$code->getPurchasesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getSalesCost($date1,$date2,$movimento,$storeId); ?></td>
            <td><?=$code->getInventoriesCost($date2,$storeId); ?></td>
            <td><?=$code->resultCost($date1,$date2,$movimento,$storeId); ?></td>
        </tr>
        <?php endif;
        endif;
    endforeach; ?>
    <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><?=$totalInventoryInitial?></td>
            <td><?= $totalPurchases ?></td>
            <td><?=$totalSales?></td>
            <td><?=$totalInventoryFinal ?></td>
            <td><?=$totalResult?></td>
        </tr>
    </tbody>
    <?php
    endif;
endif;
    ?>

</table>