<?php 
$v->layout("client/_theme");
?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Lançamentos</h1>
        <p class="lead">Interações e Feedbacks.</p>
    </div>
</div>

<div style="margin-left: 2%;margin-right:2%">
    <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="adicionar" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="adicionar">Adicionar interação</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="formUpload" id="formUpload" method="post">
                        <input name="idRequest" value="<?=$id?>" type="hidden">
                        <input name="idUser" value="<?=$_SESSION['id']?>" type="hidden">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="files[]" multiple type="file" class="form-control" id="inputGroupFile01" accept="audio/*;capture=microphone">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Observação:</label>
                            <textarea name="descricao" required class="form-control" id="message-text"></textarea>
                        </div>
                        <div class="col-12">
                            <!-- <div class="form-check">
                                <input name="check" class="form-check-input" type="checkbox" value="1"
                                    id="invalidCheck">
                                <label class="form-check-label" for="invalidCheck">
                                    Marcar como concluido ?
                                </label>
                            </div> -->
                        </div>
                </div>
                <div class="modal-footer">
                    <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                        id="porcentagem">0%</span>
                    <div id="resposta" style="width:100%"></div>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#btnEnviar').click(function () {
                $('#formUpload').ajaxForm({
                    uploadProgress: function (event, position, total, percentComplete) {
                        $('progress').attr('value', percentComplete);
                        $('#porcentagem').html(percentComplete + '%');
                    },
                    success: function (data) {
                        $('progress').attr('value', '100');
                        $('#porcentagem').html('100%');
                        if (data.sucesso == true) {
                            $('#resposta').html('<a>' + data.msg + '</a>');
                            document.location.reload(true);
                        } else {
                            $('#resposta').html(data.msg);
                        }

                    },
                    error: function () {
                        $('#resposta').html('Erro ao enviar requisição!!!');
                    },
                    dataType: 'json',
                    url: '/request/interacao/add',
                    resetForm: true
                }).submit();
            })
        })
    </script>
</div>
<div class="" style="margin-top:5%">

    <div class="card" style="margin-left:2%;margin-right:2%;margin-bottom:2%">
        <div class="card-header">
            <h3><?="#".$request->id." - NF: ".$request->nf_number;?></h3>
            <?php
             if($request->access_key):
            echo "<h4>Chave de Acesso: ".$request->access_key."</h4>";
            endif;
        ?>
        
            <h5>
                <?= $request->userAdministrator()->name." - <a style='font-size:70%;color:#5199FF'>".$request->getCreatedAtInt?></a>

            </h5>
            
        </div>
        <div class="card">
                <div class="card-header">
                Codigos Internos
                </div>
                <?php foreach ($request->getCodes() as $code): ?>
                <div style="margin: 1%">
                <?=$code->code?>
                </div>
                <?php endforeach; ?>    
            </div>
        <div style="margin:1%">
        
            <p><?=$request->note?></p>
            
            <?php
            

            if (is_array($request->getAttachment)):
            foreach($request->getAttachment as $file):?>
            <a download href="<?=URL_BASE.'/'.$file->file_dir?>" class="card-link"><?=$file->getName."📎"?></a>
            
            <?php endforeach;
            endif;
                ?>
            <div class="card">
                <div class="card-header">
                Codigos Internos
                </div>
                <?php foreach ($request->getCodes() as $code): ?>
                <div style="margin: 1%">
                <?=$code->code?>
                </div>
                <?php endforeach; ?>    
            </div>
        </div>
    </div>
    
    <div style="margin-left:2%;margin-right:2%;">
        <button style="margin-top:2%" type="button" class="btn btn-secondary btn-lg" data-bs-toggle="modal"
            data-bs-target="#adicionar">Adicionar
            +</button>
        <?php
        if ($interactions):
            ?>
        <?php
        else:
            ?>
        <h5 style="margin-left:43%;margin-bottom:5%">Sem Interações.</h5>
        <?php
        endif;
        ?>

        <?php 
        if(is_array($interactions)):
        foreach ($interactions as $interaction):
            ?>

        <div class="card" style="margin-left:2%;margin-right:2%;margin-bottom:2%">
            <div class="card-header">
                <?=$interaction->user()->getLogin." <a style='font-size:70%;color:#5199FF'>".$interaction->getCreatedAt?></a>
            </div>
            <div class="card-body">
                <h5 class="card-title"></h5>
                <p class="card-text"><?=nl2br($interaction->description)?></p>
            </div>
            <?php 
            if (is_array($interaction->imageAttachment)):
                foreach ($interaction->imageAttachment as $attachment):
                    
            ?>
            <img style="width:350px;margin-bottom:1%;" src="<?=url($attachment->name)?>" class="card-img-top" alt="...">
            <?php
                endforeach;
                endif;
            ?>
            <div class="card-body">
                <?php 
                if (is_array($interaction->imageAttachment)):
                foreach ($interaction->imageAttachment as $attachment):
                ?>
                <a download href="<?=url($attachment->name)?>" class="card-link">Imagem📎</a>
                <?php
                endforeach;
                endif;
                if (is_array($interaction->fileAttachment)):
                foreach ($interaction->fileAttachment as $file):
                ?>
                <a download href="<?=url($file->name)?>" class="card-link"><?=$file->getName."📎"?></a>
                <?php
                endforeach;
            endif;
            if (is_array($interaction->audioAttachment)):
                foreach ($interaction->audioAttachment as $audio):
                    ?>
                    <audio controls>
                        <source src="<?=url($audio->name)?>" type="audio/ogg">
                        Your browser does not support the audio element.
                    </audio>
                    <?php
                endforeach;
            endif;
            ?>
            </div>
        </div>
        <?php 
        endforeach;
    endif;
        ?>
            </div>
        </div>
    </div>