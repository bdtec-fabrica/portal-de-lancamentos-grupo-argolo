<?php 
$v->layout("client/_theme");
?>
<script>
  setTimeout(function () {
  window.location.reload(1);
}, 60000);
</script>
<main class="main_content">
    <form action="/dashboard" method="post">
        <!-- <div class="input-group input-group-sm container" style="width:500px;margin-top:2%">
  <span class="input-group-text">Período</span>
  <input type="date" name="date1" aria-label="First name" class="form-control">
  <input type="date" name="date2" aria-label="Last name" class="form-control">
  <button type="submit" class="input-group-text btn btn-outline-success"><i class="ion ion-android-refresh"></i></button>
</div> -->
    </form>
    <div class="container">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-3">
                    <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento lançamentos diarios</h1>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="margin-bottom:5%">
            <div class="row">
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$cargoArrival?></h3>
                            <p>Chegada Carga</p>
                            <p><?=$cargoArrivalA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-add"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/10"
                            class="small-box-footer">Mais Informações <i class="ion ion-android-add"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$commercialPending?><sup style="font-size: 20px"></sup></h3>
                            <p>Pendente Comercial</p>
                            <p><?=$commercialPendingA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/11"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$conference?></h3>
                            <p style="font-size:95%">Conferência</p>
                            <p><?=$conferenceA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-alert"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/12"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-alert"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$nfeUpdate?></h3>
                            <p>NFe a Atualizar</p>
                            <p><?=$nfeUpdateA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-refresh"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/13"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-refresh"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-indigo">
                        <div class="inner">
                            <h3><?=$precificar?></h3>
                            <p>A precificar</p>
                            <p><?=$precificarA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-add"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/15"
                            class="small-box-footer">Mais Informações <i class="ion ion-android-add"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3><?=$finishedCargo?></h3>
                            <p>Carga Finalizada</p>
                            <p><?=$finishedCargoA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/14"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-person"></i></a>
                    </div>
                </div>

            </div>
        </div>
        <!-- chegada carga 1-->
        <div class="container">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-3">
                        <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Chegada Carga Horas</h1>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$request->getAmountCC1?><sup style="font-size: 20px"></sup></h3>
                            <p>1 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCC1?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$request->getAmountCC2?><sup style="font-size: 20px"></sup></h3>
                            <p>2 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCC2?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$request->getAmountCC4?><sup style="font-size: 20px"></sup></h3>
                            <p>4 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCC4?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$request->getAmountCC6?><sup style="font-size: 20px"></sup></h3>
                            <p>6 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCC6?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$request->getAmountCC8?><sup style="font-size: 20px"></sup></h3>
                            <p>8 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCC8?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$request->getAmountCC24?><sup style="font-size: 20px"></sup></h3>
                            <p>24 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCC24?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pendência Comercial-->
        <div class="container">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-3">
                        <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Pendência Comercial Horas
                        </h1>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$request->getAmountPCO1?><sup style="font-size: 20px"></sup></h3>
                            <p>1 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestPCO1?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$request->getAmountPCO2?><sup style="font-size: 20px"></sup></h3>
                            <p>2 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestPCO2?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$request->getAmountPCO4?><sup style="font-size: 20px"></sup></h3>
                            <p>4 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestPCO4?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$request->getAmountPCO6?><sup style="font-size: 20px"></sup></h3>
                            <p>6 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestPCO6?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$request->getAmountPCO8?><sup style="font-size: 20px"></sup></h3>
                            <p>8 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestPCO8?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$request->getAmountPCO24?><sup style="font-size: 20px"></sup></h3>
                            <p>24 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestPCO24?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Conferência-->
        <div class="container">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-3">
                        <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Conferência Horas</h1>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$request->getAmountCR1?><sup style="font-size: 20px"></sup></h3>
                            <p>1 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCR1?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$request->getAmountCR2?><sup style="font-size: 20px"></sup></h3>
                            <p>2 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCR2?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$request->getAmountCR4?><sup style="font-size: 20px"></sup></h3>
                            <p>4 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCR4?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$request->getAmountCR6?><sup style="font-size: 20px"></sup></h3>
                            <p>6 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCR6?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$request->getAmountCR8?><sup style="font-size: 20px"></sup></h3>
                            <p>8 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCR8?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$request->getAmountCR24?><sup style="font-size: 20px"></sup></h3>
                            <p>24 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestCR24?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- NFe a atualizar-->
        <div class="container">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-3">
                        <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento NFe a Atualizar Horas</h1>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$request->getAmountAA1?><sup style="font-size: 20px"></sup></h3>
                            <p>1 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestAA1?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$request->getAmountAA2?><sup style="font-size: 20px"></sup></h3>
                            <p>2 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestAA2?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$request->getAmountAA4?><sup style="font-size: 20px"></sup></h3>
                            <p>4 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestAA4?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$request->getAmountAA6?><sup style="font-size: 20px"></sup></h3>
                            <p>6 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestAA6?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$request->getAmountAA8?><sup style="font-size: 20px"></sup></h3>
                            <p>8 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestAA8?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$request->getAmountAA24?><sup style="font-size: 20px"></sup></h3>
                            <p>24 horas ou mais</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$request->getRequestAA24?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-3">
                        <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Media de Tempo Status Dia
                        </h1>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(10)?><sup style="font-size: 20px"></sup></h3>
                            <p>Chegada Carga</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(10)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(11)?><sup style="font-size: 20px"></sup></h3>
                            <p>NFe Lançamento</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(11)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(12)?><sup style="font-size: 20px"></sup></h3>
                            <p>Pendente Comercial</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(12)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(13)?><sup style="font-size: 20px"></sup></h3>
                            <p>Conferência</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(13)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(14)?><sup style="font-size: 20px"></sup></h3>
                            <p>NFe a Atualizar</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(14)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
            </div>
            <section class="col-lg-12 connectedSortable">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-chart-pie mr-1" style="text-align:center"></i>
                            Processo de Recebimento de Carga por Fornecedor
                        </h3>
                        <div class="card-tools">
                            <ul class="nav nav-pills ml-auto">
                                <li class="nav-item">
                                </li>
                            </ul>
                        </div>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <?php
                    foreach ($stores as $store):
                    ?>
                            <div class="col-lg-2 col-6">
                                <!-- small box -->
                                <div class="small-box bg-info">
                                    <div class="inner">
                                        <h5><?=$store->getFornecedor?></h5>
                                        <p><?=$storeIncluded["$store->getFornecedor"]?> Chamado(os) Aberto(os)</p>
                                        <p><?=$storeIncludedA["$store->getFornecedor"]+0?> Itens</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-pie-graph"></i>
                                    </div>
                                    <a href="<?=URL_BASE?>/request/storeStatus/<?=$store->store_id?>/1"
                                        class="small-box-footer">Info. <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-lg-2 col-6">
                                <!-- small box -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h5><?=$store->getFornecedor?></h5>
                                        <p><?=$storeCompleted["$store->getFornecedor"]?> Chamado(os) Concluido(os)</p>
                                        <p><?=$storeCompletedA["$store->getFornecedor"]+0?> Itens</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-pie-graph"></i>
                                    </div>
                                    <a href="<?=URL_BASE?>/request/storeStatus/<?=$store->store_id?>/3"
                                        class="small-box-footer">Info. <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <?php
                    endforeach;
                    ?>
            </section>
        </div>