<?php 
$v->layout("client/_theme");
?>
<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Painel Lançamentos BDTec</h1>
                <p class="lead"></p>
            </div>
        </div>
        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
            data-bs-toggle="collapse" data-bs-target="#multiCollapseExample1" aria-expanded="false"
            aria-controls="multiCollapseExample1">Filtro
            ↴</button>

    </div>
    <div class="collapse multi-collapse container" id="multiCollapseExample1">
        <form action="/request/searchRequest" method="post">
            <div class="row">
                <div class="form-group col-md-1">
                    <span>#Codigo</span>
                    <input name="id" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Número</span>
                    <input name="nfNumber" class="form-control" type="text">
                </div>
                <div class="form-group col-md-5">
                    <span>Chave Acesso</span>
                    <input name="accessKey" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Postagem inicial</span>
                    <input name="criacaoInicial" required style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Postagem final</span>
                    <input name="criacaoFinal" required style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Alteração inicial</span>
                    <input name="alteracaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Alteração final</span>
                    <input name="alteracaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Operador</span>
                    <select name="idOperator" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($users as $user):
                            ?>
                        <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <span>Status</span>
                    <select name="status" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($status as $statuss):
                            ?>
                        <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <span>Grupo</span>
                    <select name="storeGroup" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($storeGroups as $storeGroup):
                            ?>
                        <option value="<?= $storeGroup->id ?>"><?= $storeGroup->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <div style="margin-left: 2%;padding:2%;">
    <div class="container" style="margin-bottom:3%">
            <button type="button" class="btn btn-info btn-lg" data-bs-toggle="modal"
            data-bs-target="#report">Relatorio de Notas <i class="ion ion-document-text"></i></button>
            <button type="button" class="btn btn-danger btn-lg" data-bs-toggle="modal"
                data-bs-target="#report2">Relatorio de Reportes <i class="ion ion-document-text"></i></button>
                <div class="modal fade" id="report2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Relatorio de Notas Reportadas</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/relatorioNotasReportadas" target="_blank" method="post">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Criação inicial:</label>
                                <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Criação final:</label>
                                <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Alteração inicial:</label>
                                <input name="alteracaoInicial" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Alteração Final:</label>
                                <input name="alteracaoFinal" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                                <select class="form-select" name="type">
                                    <option value="">Selecione ...</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Operador</label>
                                <select name="idOperator" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todos</option>
                                    <?php
                        foreach ($users as $user):
                            ?>
                                    <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Status</label>
                                <select name="status" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todos</option>
                                    <?php
                        foreach ($status as $statuss):
                            ?>
                                    <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Loja</label>
                                <input class="form-control" name="store">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" id="btnEnviar" class="btn btn-primary">Enviar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
            <div class="modal fade" id="report" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Relatorio de Notas Postadas</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/relatorioNotas" target="_blank" method="post">
                        <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Criação inicial:</label>
                    <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group">
                <label for="recipient-name" class="col-form-label">Criação final:</label>
                    <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group">
                <label for="recipient-name" class="col-form-label">Alteração inicial:</label>
                    <input name="alteracaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Alteração Final:</label>
                    <input name="alteracaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                                <select class="form-select" name="type">
                                    <option value="">Selecione ...</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Operador</label>
                    <select name="idOperator" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($users as $user):
                            ?>
                        <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group">
                <label for="recipient-name" class="col-form-label">Status</label>
                    <select name="status" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($status as $statuss):
                            ?>
                        <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group">
                <label for="recipient-name" class="col-form-label">Grupo</label>
                    <select name="storeGroup" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($storeGroups as $storeGroup):
                            ?>
                        <option value="<?= $storeGroup->id ?>"><?= $storeGroup->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group">
                <label for="recipient-name" class="col-form-label">Operação</label>
                    <select name="operation" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todas</option>
                        <?php
                        foreach ($nfeOperation as $nfeOperations):
                            if($nfeOperations->id != 12):
                            ?>
                        <option value="<?= $nfeOperations->id ?>"><?= $nfeOperations->name ?></option>
                        <?php
                        endif;
                        endforeach;
                    ?>
                    </select>
                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Postagem Manual</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="formUpload" id="formUpload" method="post">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do documento:</label>
                                <input required name="nfNumber" type="number" class="form-control" id="recipient-name">
                                <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="accessKey" type="text" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                                <select class="form-select" name="type" required>
                                    <option value="1">Avulsa</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Quantidade de itens</label>
                                <input required name="itemsQuantity" type="number" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">1º Vencimento</label>
                                <input required name="dueDate1" type="date"  min="2022-01-01"  class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">2º Vencimento</label>
                                <input required name="dueDate2" type="date"  min="2022-01-01"  class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">3º Vencimento</label>
                                <input required name="dueDate3" type="date"  min="2022-01-01"  class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Observações:</label>
                                <textarea required name="note" class="form-control" id="message-text"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                <select class="form-select" name="nfeOperation">
                                    <option value="99">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                                    <input name="files[]" multiple type="file" class="form-control"
                                        id="inputGroupFile01">
                                </div>
                            </div>
                            <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                                id="porcentagem">0%</span>
                            <div id="resposta" style="width:100%"></div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#btnEnviar').click(function () {
                    $('#formUpload').ajaxForm({
                        uploadProgress: function (event, position, total, percentComplete) {
                            $('progress').attr('value', percentComplete);
                            $('#porcentagem').html(percentComplete + '%');
                        },
                        success: function (data) {
                            $('progress').attr('value', '100');
                            $('#porcentagem').html('100%');
                            if (data.sucesso == true) {
                                $('#resposta').html('<div class="alert alert-success" role="alert">' + data.msg + '</div>');
                                document.location.reload(true);
                            } else {
                                $('#resposta').html('<div class="alert alert-danger" role="alert">' + data.msg + '</div>');
                            }
                        },
                        error: function () {
                            $('#resposta').html('<div class="alert alert-danger" role="alert">Erro ao enviar requisição!!!</div>');
                        },
                        dataType: 'json',
                        url: '/request/addRequest',
                        
                    }).submit();
                })
            })
        </script>


        <!-- <h5 style="margin-left:2%;margin-top:2%">Página < ?= $paginator->page(); ?> de < ?= $paginator->pages(); ?> </h5> -->



        <table class="table dt-responsive table-hover nowrap w-100 sortable" style="font-size:80%">
            <thead class="">
                <tr>
                    <th>#</th>
                    <th>Loja</th>
                    <th>Número</th>
                    <th>Inclusão</th>
                    <th>Cliente</th>
                    <!-- <th>Chave de Acesso</th> -->
                    <th>Fornecedor</th>
                    <th>Tipo</th>
                    <th>Grupo</th>
                    <th>Qtd</th>
                    <th>Alteração</th>
                    <th>Operador</th>
                    <th>Status</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>
                <?php
         if($requests):
            /**@var $request Request */
            foreach ($requests as $request):
                // if ($request->status == $status || $status == 3):
                    ?>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("a<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("b<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("c<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("d<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("e<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("f<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("g<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("h<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("h<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("i<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("j<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("k<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>

                <tr
                <?php if ($request->operation_id == 1) : 
                        echo "style='background-color:$request->getOperationColor'";
                endif;
                ?>
                >
                    <th scope="row"><?=$request->id?></th>
                    <td id="a<?=$request->id?>"><?= $request->store_id?></td>
                    <td id="b<?=$request->id?>"><?= $request->nf_number?></td>
                    <td id="c<?=$request->id?>"><?= $request->getCreatedAt?></td>
                    <td id="d<?=$request->id?>"><?= $request->getClient?></td>
                    <!-- <td><?= $request->access_key?></td> -->
                    <td id="e<?=$request->id?>"><?= $request->getFornecedor?></td>
                    <td id="f<?=$request->id?>"><?= $request->getType()?></td>
                    <td id="g<?=$request->id?>"><?= $request->clientGroup?></td>
                    <td id="h<?=$request->id?>"><?= $request->items_quantity?></td>
                    <td id="i<?=$request->id?>"><?= $request->getUpdatedAt?></td>
                    <td id="j<?=$request->id?>"><?= $request->userOperator?></td>
                    <td id="k<?=$request->id?>"><?= $request->getStatus?></td>
                    <td>
                    <div class="btn-group me-2" role="group" aria-label="First group">
                    <?php if ($request->status_id == 3 || $request->status_id == 14): ?>
                    <button type="button" data-bs-placement="top" title="Reporte" class="btn btn-outline-danger" data-bs-toggle="modal"
                                     data-bs-target="#reporte<?=$request->id?>"
                                    data-whatever="@mdo"><i class="ion ion-alert"></i></button>
                    <?php endif; ?>
                    <button type="button"  data-bs-placement="top" title="Visualizar" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#visualizar<?=$request->id?>"
                                    data-whatever="@mdo"><i class="ion ion-eye"></i></button>
                    <button type="button" data-bs-placement="top" title="Editar" class="btn btn-warning" data-bs-toggle="modal"
                                    data-bs-target="#editar<?=$request->id?>" data-whatever="@mdo"><i class="ion ion-edit"></i></button>
                    <button type="button" data-bs-placement="top" title="Log" class="btn btn-secondary" data-bs-toggle="modal"
                                    data-bs-target="#log<?= $request->id?>" data-whatever="@mdo"><i class="ion ion-ios-time-outline"></i></button>
                    <form name="excluir<?=$request->id?>">
                        <input type="hidden" name="id" value="<?=$request->id?>">
                    <!-- <button type="submit" data-bs-placement="top" title="Excluir" class="btn btn-danger"><i class="ion ion-ios-trash"></i></button> -->

                    <button class="btn" type="button"><a href="https://wa.me/55<?=$request->getOperatorClass()->tell?>?text= Cliente: <?= $request->getClientClass()->getLogin?>, NF: <?= $request->nf_number?>, Loja:  <?= $request->store_id?>, Grupo: <?=$request->clientGroup?>  http://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/<?=$request->id?>"><i class="ion ion-social-whatsapp"></i></a></button>
                    </div>
                    
                    
                        <script>
                                    $('form[name="excluir<?=$request->id?>"]').submit(function (event) {
                                        event.preventDefault();
                                        var x;
                                        var r = confirm(
                                            "Tem certeza que deseja excluir a Chamado: '<?= $request->title?>'"
                                            );
                                        if (r == true) {
                                            $.ajax({
                                                url: '/request/deleteRequest',
                                                type: 'post',
                                                data: $(this).serialize(),
                                                dataType: 'json',
                                                success: function (response) {

                                                    if (response.success === true) {
                                                        swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            timer: 20000,
                                                            icon: "success",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "success"
                                                        });

                                                    } else {
                                                        swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            icon: "error",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "danger"
                                                        });
                                                    }

                                                }
                                            })
                                            document.location.reload(true);
                                        }
                                    });
                                </script>
                    </form>
                    </td>
                    <div class="modal fade " id="reporte<?=$request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Reportar NF <?=$request->nf_number?></h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form name="reporte<?=$request->id?>">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Categoria:</label>
                                        <select class="form-control" name="type">
                                            <option value="1">Erro lançamento</option>
                                            <option value="2">Vencimento</option>
                                            <option value="3">Código interno</option>
                                            <option value="4">Não Atualizada</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                            <label for="message-text" class="col-form-label">Menssagem:</label>
                                            <textarea name="note" class="form-control" id="message-text"></textarea>
                                            <input type="hidden" name="idRequest" value="<?=$request->id?>">
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="editar<?=$request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Alterar Postagem</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form name="editar<?=$request->id?>">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="nfNumber" required type="text" class="form-control" id="recipient-name"
                                                value="<?= $request->nf_number?>">
                                            <input name="id" type="hidden" value="<?= $request->id?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="accessKey" type="text" class="form-control" id="recipient-name"
                                                value="<?= $request->access_key?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Operador:</label>
                                            <select name="operatorId" required class="custom-select" id="inputGroupSelect01" disabled>
                                                <option selected value="<?=$request->operator_id?>">
                                                    <?=$request->getOperator?></option>
                                                <?php 
                                                foreach ($users as $user):
                                            ?>
                                                <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                                                <?php
                                                endforeach;
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Status:</label>
                                            <select name="status" required class="custom-select" id="inputGroupSelect01" disabled>
                                                <option value="<?=$request->status_id?>"><?=$request->getStatus?>
                                                </option>
                                                <?php
                                                foreach ($status as $statuss):
                                            ?>
                                                <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                                                <?php
                                                endforeach;
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Quantidade de
                                                itens:</label>
                                            <input name="itemsQuantity" required type="number" class="form-control"
                                                id="recipient-name" value="<?= $request->items_quantity?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Tipo de
                                                documento:</label>
                                            <select class="form-select" required name="type">
                                            <option value="<?=$request->type_id?>"><?=$request->getType?>
                                                </option>
                                                <?php foreach ($nfeType as $type): ?>
                                                <option value="<?=$type->id?>"><?=$type->name?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Operação:</label>
                                            <select class="form-select" required name="operationId">
                                            <option value="<?=$request->operation_id?>"><?=$request->getOperation?>
                                                </option>
                                                <?php foreach ($nfeOperation as $operation): 
                                                    if($operation->id != 12):?>
                                                <option value="<?=$operation->id?>"><?=$operation->name?></option>
                                                <?php endif;
                                            endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" required class="col-form-label">Descrição:</label>
                                            <textarea name="note" class="form-control"
                                                id="message-text"><?= str_replace("<br />","",$request->note) ?></textarea>
                                        </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade " id="log<?= $request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Log de alteração Chamado
                                        <?=$request->id?></h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="row align-items-start" style="background-color:#adb5bd">
                                            <div class="col">
                                                Usúario
                                            </div>
                                            <div class="col">
                                                Alteração
                                            </div>
                                            <div class="col">
                                                Data e Hora
                                            </div>
                                        </div>
                                        <?php if (is_array($request->getLogs())):
                                    foreach ($request->getLogs() as $log):?>
                                    <div class="row align-items-start">
                                        <div class="col">
                                            <?=$log->getUser()->getLogin;?>
                                        </div>
                                        <div class="col">
                                            <?=$log->description?>
                                        </div>
                                        <div class="col">
                                            <?=$log->getCreatedAt?>
                                        </div>
                                    </div>
                                    <?php endforeach;
                                    endif;?>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Fechar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="visualizar<?=$request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $request->nf_number?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $request->access_key?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Observações:</label>
                                            <textarea name="description" disabled class="form-control"
                                                id="message-text"><?= $request->note ?></textarea>
                                        </div>
                                        <?php $i = 1;
                                        if ($request->getDueDate):
                                         foreach ($request->getDueDate as $dueDate): ?>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Vencimento <?=$i?>:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $dueDate->due_date?>">
                                        </div>
                                        
                                        <?php endforeach;
                                        endif; ?>
                                    <?=__DIR__?>
                                        <?php if (file_exists(__DIR__."/../../source/XML/$request->store_id/NFe$request->access_key.xml")): ?>
                                        <div class="form-group">
                                        <div class="row">
                                        <a download style="text-decoration:none;margin-bottom:5%" href="<?="/../../source/XML/$request->store_id/NFe$request->access_key.xml"?>"><button class="btn btn-outline-success">XML <i class="ion ion-android-download"></i></button></a>
                                            <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                                <input type="hidden" name="key" value="<?=$request->access_key?>">
                                                <input type="hidden" name="store" value="<?=$request->store_id?>">
                                                <a>
                                                <button type="submit" class="btn btn-outline-warning">PDF <i class="ion ion-android-download"></i></button></a>
                                            </form>
                                        </div>
                                        </div>
                                        <?php endif; ?>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Voltar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                    $('form[name="reporte<?=$request->id?>"]').submit(function (event) {
                            event.preventDefault();

                            $.ajax({
                                url: '/request/nfReport/add',
                                type: 'post',
                                data: $(this).serialize(),
                                dataType: 'json',
                                success: function (response) {

                                    if (response.sucesso === true) {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.msg,
                                            timer: 20000,
                                            icon: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "success"
                                        });
                                        document.location.reload(true);
                                    } else {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.msg,
                                            icon: "error",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "danger"
                                        });
                                    }

                                }
                            })
                        });
                        $('form[name="editar<?=$request->id?>"]').submit(function (event) {
                            event.preventDefault();

                            $.ajax({
                                url: '/request/editRequest',
                                type: 'post',
                                data: $(this).serialize(),
                                dataType: 'json',
                                success: function (response) {

                                    if (response.success === true) {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            timer: 20000,
                                            icon: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "success"
                                        });
                                        document.location.reload(true);
                                    } else {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            icon: "error",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "danger"
                                        });
                                    }

                                }
                            })
                        });
                    </script>
                
                <?php 
            // endif;
            endforeach;
        endif;?>
        <?php
        if($requests2):
            /**@var $request Request */
            foreach ($requests2 as $request):
                // if ($request->status == $status || $status == 3):
                    ?>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("a<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("b<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("c<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("d<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("e<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("f<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("g<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("h<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("h<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("i<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("j<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>
                <script>
                    // Função para mudar o conteúdo de t2
                    function interaction<?=$request->id?>() {
                        window.open("/request/interacao/<?=$request->id?>");
                    }

                    // Função para adicionar uma espera de evento em t
                    function load() {
                        var el = document.getElementById("k<?=$request->id?>");
                        el.addEventListener("click", interaction<?=$request->id?> , false);
                    }

                    document.addEventListener("DOMContentLoaded", load, false);
                </script>

                <tr
                <?php if ($request->operation_id == 1) : 
                        echo "style='background-color:$request->getOperationColor'";
                endif;
                ?>
                >
                    <th scope="row"><?=$request->id?></th>
                    <td id="a<?=$request->id?>"><?= $request->store_id?></td>
                    <td id="b<?=$request->id?>"><?= $request->nf_number?></td>
                    <td id="c<?=$request->id?>"><?= $request->getCreatedAt?></td>
                    <td id="d<?=$request->id?>"><?= $request->getClient?></td>
                    <!-- <td><?= $request->access_key?></td> -->
                    <td id="e<?=$request->id?>"><?= $request->getFornecedor?></td>
                    <td id="f<?=$request->id?>"><?= $request->getType()?></td>
                    <td id="g<?=$request->id?>"><?= $request->clientGroup?></td>
                    <td id="h<?=$request->id?>"><?= $request->items_quantity?></td>
                    <td id="i<?=$request->id?>"><?= $request->getUpdatedAt?></td>
                    <td id="j<?=$request->id?>"><?= $request->userOperator?></td>
                    <td id="k<?=$request->id?>"><?= $request->getStatus?></td>
                    <td>
                    <div class="btn-group me-2" role="group" aria-label="First group">
                    <?php if ($request->status_id == 3): ?>
                    <button type="button" data-bs-placement="top" title="Reporte" class="btn btn-outline-danger" data-bs-toggle="modal"
                                     data-bs-target="#reporte<?=$request->id?>"
                                    data-whatever="@mdo"><i class="ion ion-alert"></i></button>
                    <?php endif; ?>
                    <button type="button"  data-bs-placement="top" title="Visualizar" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#visualizar<?=$request->id?>"
                                    data-whatever="@mdo"><i class="ion ion-eye"></i></button>
                    <button type="button" data-bs-placement="top" title="Editar" class="btn btn-warning" data-bs-toggle="modal"
                                    data-bs-target="#editar<?=$request->id?>" data-whatever="@mdo"><i class="ion ion-edit"></i></button>
                    <button type="button" data-bs-placement="top" title="Log" class="btn btn-secondary" data-bs-toggle="modal"
                                    data-bs-target="#log<?= $request->id?>" data-whatever="@mdo"><i class="ion ion-ios-time-outline"></i></button>
                    <form name="excluir<?=$request->id?>">
                        <input type="hidden" name="id" value="<?=$request->id?>">
                    <button type="submit" data-bs-placement="top" title="Excluir" class="btn btn-danger"><i class="ion ion-ios-trash"></i></button>
                    </div>
                    
                    
                        <script>
                                    $('form[name="excluir<?=$request->id?>"]').submit(function (event) {
                                        event.preventDefault();
                                        var x;
                                        var r = confirm(
                                            "Tem certeza que deseja excluir a Chamado: '<?= $request->title?>'"
                                            );
                                        if (r == true) {
                                            $.ajax({
                                                url: '/request/deleteRequest',
                                                type: 'post',
                                                data: $(this).serialize(),
                                                dataType: 'json',
                                                success: function (response) {

                                                    if (response.success === true) {
                                                        swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            timer: 20000,
                                                            icon: "success",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "success"
                                                        });

                                                    } else {
                                                        swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            icon: "error",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "danger"
                                                        });
                                                    }

                                                }
                                            })
                                            document.location.reload(true);
                                        }
                                    });
                                </script>
                    </form>
                    </td>
                    <div class="modal fade " id="reporte<?=$request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Reportar NF <?=$request->nf_number?></h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form name="reporte<?=$request->id?>">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Categoria:</label>
                                        <select class="form-control" name="type">
                                            <option value="1">Erro lançamento</option>
                                            <option value="2">Vencimento</option>
                                            <option value="3">Código interno</option>
                                            <option value="4">Não Atualizada</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                            <label for="message-text" class="col-form-label">Menssagem:</label>
                                            <textarea name="note" class="form-control" id="message-text"></textarea>
                                            <input type="hidden" name="idRequest" value="<?=$request->id?>">
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="editar<?=$request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Alterar Postagem</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form name="editar<?=$request->id?>">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="nfNumber" required type="text" class="form-control" id="recipient-name"
                                                value="<?= $request->nf_number?>">
                                            <input name="id" type="hidden" value="<?= $request->id?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="accessKey" type="text" class="form-control" id="recipient-name"
                                                value="<?= $request->access_key?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Operador:</label>
                                            <select name="operatorId" required class="custom-select" id="inputGroupSelect01" disabled>
                                                <option selected value="<?=$request->operator_id?>">
                                                    <?=$request->getOperator?></option>
                                                <?php 
                                                foreach ($users as $user):
                                            ?>
                                                <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                                                <?php
                                                endforeach;
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Status:</label>
                                            <select name="status" required class="custom-select" id="inputGroupSelect01" disabled>
                                                <option value="<?=$request->status_id?>"><?=$request->getStatus?>
                                                </option>
                                                <?php
                                                foreach ($status as $statuss):
                                            ?>
                                                <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                                                <?php
                                                endforeach;
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Quantidade de
                                                itens:</label>
                                            <input name="itemsQuantity" required type="number" class="form-control"
                                                id="recipient-name" value="<?= $request->items_quantity?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Tipo de
                                                documento:</label>
                                            <select class="form-select" required name="type">
                                            <option value="<?=$request->type_id?>"><?=$request->getType?>
                                                </option>
                                                <?php foreach ($nfeType as $type): ?>
                                                <option value="<?=$type->id?>"><?=$type->name?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Operação:</label>
                                            <select class="form-select" required name="operationId">
                                            <option value="<?=$request->operation_id?>"><?=$request->getOperation?>
                                                </option>
                                                <?php foreach ($nfeOperation as $operation): 
                                                    if($operation->id != 12):?>
                                                <option value="<?=$operation->id?>"><?=$operation->name?></option>
                                                <?php endif;
                                            endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" required class="col-form-label">Descrição:</label>
                                            <textarea name="note" class="form-control"
                                                id="message-text"><?= str_replace("<br />","",$request->note) ?></textarea>
                                        </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade " id="log<?= $request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Log de alteração Chamado
                                        <?=$request->id?></h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="row align-items-start" style="background-color:#adb5bd">
                                            <div class="col">
                                                Usúario
                                            </div>
                                            <div class="col">
                                                Alteração
                                            </div>
                                            <div class="col">
                                                Data e Hora
                                            </div>
                                        </div>
                                        <?php if (is_array($request->getLogs())):
                                    foreach ($request->getLogs() as $log):?>
                                    <div class="row align-items-start">
                                        <div class="col">
                                            <?=$log->getUser()->getLogin;?>
                                        </div>
                                        <div class="col">
                                            <?=$log->description?>
                                        </div>
                                        <div class="col">
                                            <?=$log->getCreatedAt?>
                                        </div>
                                    </div>
                                    <?php endforeach;
                                    endif;?>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Fechar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="visualizar<?=$request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $request->nf_number?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $request->access_key?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Observações:</label>
                                            <textarea name="description" disabled class="form-control"
                                                id="message-text"><?= $request->note ?></textarea>
                                        </div>
                                        <?php $i = 1;
                                        if ($request->getDueDate):
                                         foreach ($request->getDueDate as $dueDate): ?>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Vencimento <?=$i?>:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $dueDate->due_date?>">
                                        </div>
                                        
                                        <?php endforeach;
                                        endif; ?>
                                    <?=__DIR__?>
                                        <?php if (file_exists(__DIR__."/../../source/XML/$request->store_id/NFe$request->access_key.xml")): ?>
                                        <div class="form-group">
                                        <div class="row">
                                        <a download style="text-decoration:none;margin-bottom:5%" href="<?="/../../source/XML/$request->store_id/NFe$request->access_key.xml"?>"><button class="btn btn-outline-success">XML <i class="ion ion-android-download"></i></button></a>
                                            <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                                <input type="hidden" name="key" value="<?=$request->access_key?>">
                                                <input type="hidden" name="store" value="<?=$request->store_id?>">
                                                <a>
                                                <button type="submit" class="btn btn-outline-warning">PDF <i class="ion ion-android-download"></i></button></a>
                                            </form>
                                        </div>
                                        </div>
                                        <?php endif; ?>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Voltar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                    $('form[name="reporte<?=$request->id?>"]').submit(function (event) {
                            event.preventDefault();

                            $.ajax({
                                url: '/request/nfReport/add',
                                type: 'post',
                                data: $(this).serialize(),
                                dataType: 'json',
                                success: function (response) {

                                    if (response.sucesso === true) {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.msg,
                                            timer: 20000,
                                            icon: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "success"
                                        });
                                        document.location.reload(true);
                                    } else {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.msg,
                                            icon: "error",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "danger"
                                        });
                                    }

                                }
                            })
                        });
                        $('form[name="editar<?=$request->id?>"]').submit(function (event) {
                            event.preventDefault();

                            $.ajax({
                                url: '/request/editRequest',
                                type: 'post',
                                data: $(this).serialize(),
                                dataType: 'json',
                                success: function (response) {

                                    if (response.success === true) {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            timer: 20000,
                                            icon: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "success"
                                        });
                                        document.location.reload(true);
                                    } else {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            icon: "error",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "danger"
                                        });
                                    }

                                }
                            })
                        });
                    </script>
                
                <?php 
            // endif;
            endforeach;
        endif;?>
            </tbody>
        </table>

        <!-- <div style="margin-left:2%;margin-bottom:5%">< ?= $paginator->render();?><div> -->
            </div>
</main>