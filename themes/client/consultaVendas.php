<?php 
session_start();
$v->layout("client/_theme");
?>
<div class="container">
    <div class="pricing-header p-3 pb-md-4 mx-auto text-center">
        <h1 class="display-5 fw-normal">Acompanhamento do resultado dos produtos</h1>
        <p class="fs-5 text-muted">Acompanhemtno da venda dos podutos no dia, mês e ano atual.</p>
    </div>
    <form action="/margin/webservice72" method="POST">
        <div class="row">
            <div class="col">
            </div>
            <div class="col">
                <input style="width: 300px" name="EAN" type="text" class="form-control" placeholder="Codigo de barras"
                    aria-label="Recipient's username">
            </div>
            <div class="col">
            </div>
        </div>
        <div class="row" style="margin:2%">
            <div class="col">
            </div>
            <div class="col">
                <button class="btn btn-outline-secondary" type="submit">Buscar</button>
            </div>
            <div class="col">
            </div>
        </div>
    </form>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Codigo Interno</th>
                <th scope="col">Descrição</th>
                <th scope="col">Venda dia Atual</th>
                <th scope="col">Venda mês Atual</th>
                <th scope="col">Venda ano Atual</th>
            </tr>
        </thead>
        <tbody id="corpo">
            <tr id="linha">
                <td><?=$aa3citem->GIT_COD_ITEM.'-'.$aa3citem->GIT_DIGITO?></td>
                <td><?=$aa3citem->GIT_DESCRICAO?></td>
                <td><?=$dia?></td>
                <td><?=$mes?></td>
                <td><?=$ano?></td>
            </tr>
        </tbody>
    </table>
</div>
<script>
    $('form[name="buscar"]').submit(function (event) {
        event.preventDefault();
        $("#linha").remove();
        $("#corpo").append(
            "<tr id='linha'><th scope='row'></th><td></td><td>Não Encontrado</td><td></td><td></td><td></td></tr>"
        );
        $.ajax({
            url: '/desfazreport',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response.success === true) {
                    $("#corpo").append(
                        "<tr id='linha'><th scope='row'>7897395040307</th><td>240818</td><td>CERVEJA PILSEN ITAIPAVA LT 269ML</td><td>22</td><td>222</td><td>2222</td></tr>"
                    )
                } else {
                    $("#corpo").append("<tr id='linha'>Não encontrado!</tr>")
                }
            }
        })
    });
</script>