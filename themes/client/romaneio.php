<?php 
session_start();
$v->layout("operator/operator_theme");
?>
<div class="jumbotron jumbotron-fluid">
  <div class="container text-center">
    <h1 class="display-4">Relatório  de Conferência</h1>
    <p class="lead"><?=$collectionHeader->razao?></p>
  </div>
</div>

  <table class="table table-striped table-secondary table-bordered">
    <tr>
      <thead>
        <th>Referência</th>
        <th>EAN NFe</th>
        <th>Descrição NFe</th>
        <th>Quantidade Embalagem Encontrada</th>
        <th>EAN NFe</th>
        <th>EAN Encontrado</th>
        <th>Quantidade Encontrada</th>
        <th>Vencimento</th>
      </thead>
    </tr>
    <?php
foreach($items as $item):
?>
    <tr>
      <tbody>
        <td><?=$item->reference?></td>
        <td><?=$item->ean?></td>
        <td><?=$item->description?></td>
        <td></td>
        <td><?=$item->ean?></td>
        <td></td>
        <td></td>
        <td></td>
      </tbody>
    </tr>
    <?php
  endforeach;
?>

  </table>




</div>