<?php

use Source\Models\AA2CTIPO;

$v->layout("client/_theme");
// dd($aa3citem);
?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Digitação dos itens de Compra</h1>
        <p class="lead">CNPJ: <?=$aa2ctipo->TIP_CGC_CPF?> - <?=$aa2ctipo->TIP_RAZAO_SOCIAL?></p>
        <p class="lead">Razão Social : <?=$aa2ctipo->TIP_RAZAO_SOCIAL?></p>
    </div>
</div>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Referencia</th>
            <th scope="col">EAN Principal</th>
            <th scope="col">Descrição</th>
            <th scope="col">Custo liquido</th>
            <th scope="col">Custo bruto</th>
            <th scope="col">Quantidade</th>
        </tr>
    </thead>
    <tbody>
        <form  method="POST" action="/purshaseGeracao">
            <input name="codigos" value="<?=$itens?>" type="hidden">
            <input name="cnpj" value="<?=$aa2ctipo->TIP_CGC_CPF?>" type="hidden">
            <input name="razao" value="<?=$aa2ctipo->TIP_RAZAO_SOCIAL?>" type="hidden">
        <?php 
    if (is_array($aa3citem)):
        foreach($aa3citem as $citem):
        ?>
        <tr>
            <th scope="row"><?=$citem['referencia']?></th>
            <td><?=$citem['ean']?></td>
            <td><?=$citem['descricao']?></td>
            <td><?=$citem['custo']?></td>
            <input type="hidden" name="referencia<?=$citem['codigo']?>" value="<?=$citem['referencia']?>">
            <input type="hidden" name="ean<?=$citem['codigo']?>" value="<?=$citem['ean']?>">
            <input type="hidden" name="descricao<?=$citem['codigo']?>" value="<?=$citem['descricao']?>">
            <input type="hidden" name="custo<?=$citem['codigo']?>" value="<?=$citem['custo']?>">
            <td><input name="valor<?=$citem['codigo']?>" value="<?=$citem['valor']?>"></td>
            <td><input name="quantidade<?=$citem['codigo']?>"></td>
        </tr>
        <?php
        endforeach;
    endif;
        ?>
        <button type="submit" class="btn btn-success">Enviar</button>
        </form>
    </tbody>
</table>