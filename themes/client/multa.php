<?php 
if ($league != true):
session_start();

$v->layout("client/_theme");
endif;

?>
<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Notas Passiveis de Multa 1%  <?=$store?></h1>
                <p class="lead">Periodo <?=$date1?> até <?=$date2?></p>
            </div>
        </div>
    </div>
    <div class="alert alert-danger" role="alert">
        VALOR A PAGAR <?= number_format($total,2,",",".")?>
    </div>
    <table class="table sortable" style="font-size: 80%">
        <thead>
            <tr>
                <th scope="col">Número NFe</th>
                <th scope="col">Razão</th>
                <th scope="col">UF</th>
                <th scope="col">Emissão</th>
                <th scope="col">Chave de Acesso</th>
                <th scope="col">Natureza</th>
                <th scope="col">Dias</th>
                <th scope="col">Valor</th>
                <th scope="col">A Pagar</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($linhas as $linha): ?>
            <tr>
                <td><?=$linha['numero']?></td>
                <td><?=$linha['razao']?></td>
                <td><?=$linha['uf']?></td>
                <td><?=$linha['emissao']?></td>
                <td><?=$linha['chave']?></td>
                <td><?=$linha['natureza']?></td>
                <td><?=$linha['dias']?></td>
                <td><?=number_format($linha['valor'],2,",",".")?></td>
                <td><?=number_format($linha['pagar'],2,",",".")?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</main>