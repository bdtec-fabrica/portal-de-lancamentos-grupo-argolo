<?php 
session_start();
$v->layout("client/_theme");
?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Apuração de resultados</h1>
        <p class="lead">Apuração dos resultados de produtos atraves dos dados do ERP</p>
    </div>
</div>
<div class="d-grid gap-3 col-7 mx-auto">
    <button type="button" class="btn btn-outline-primary btn-lg" data-bs-toggle="modal"
        data-bs-target="#adicionar2">Adicionar Lista de itens para apuração de resultado</button>
    <button type="button" class="btn btn-outline-primary btn-lg" data-bs-toggle="modal"
        data-bs-target="#adicionar">Adicione itens aleatorios para apuração de resultado</button>
    <button type="button" class="btn btn-outline-primary btn-lg" data-bs-toggle="modal"
        data-bs-target="#inventario">Registre seu inventário</button>
    <button type="button" class="btn btn-outline-primary btn-lg" data-bs-toggle="modal" data-bs-target="#apurar">Realize
        apuração de resultado</button>
</div>
<div class="container">
    <div class="table-responsive-lg">
        <table class="table table-hover sortable">
            <thead>
                <tr>
                    <th style="min-width:25px">Loja</th>
                    <th style="min-width:25px">Codigo</th>
                    <th style="min-width:25px">Descrição</th>
                </tr>
            </thead>
            <tbody>
                <?php 
    foreach ($lists as $list):
        ?>
                <tr>
                    <td><?=$list->store_id; ?></td>
                    <td><?=$list->code; ?></td>
                    <td><?=$list->description; ?></td>
                </tr>
                <?php
    endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- Modais -->
<div class="modal fade" id="adicionar2" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog  modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar Lista de Apuração</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="buscar2" method="post">
                    <input name="store" type="hidden" id="store2" value="<?=$store?>">
                    <input name="secao" type="hidden" id="secao2">
                    <input name="grupo" type="hidden" id="grupo2">
                </form>
                <form name="addList">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input required name="store1" type="text" class="form-control" id="store" value="<?=$store?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Titulo:</label>
                        <input required name="titulo" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tipo de Cadastro:</label>
                        <select required name="tipo" class="form-control alvo" id="alvo">
                            <option selected disabled>Selecione ...</option>
                            <option value="1">Item a Item</option>
                            <option value="2">Mercadologica</option>
                        </select>
                    </div>
                    <div class="form-group" id="planilha-cadastro" style="display:none">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Loja</th>
                                    <th>Codigo</th>
                                    <th>Descrição</th>
                                </tr>
                            </thead>
                            <tbody id="linhas" class="linhas">
                                <tr>
                                    <td><input id="cad" class="form-control" name="store[1]" disabled></td>
                                    <td><input id="cad" class="form-control" name="code[1]" disabled></td>
                                    <td><input id="cad" class="form-control" name="description[1]" disabled></td>
                                    <td><button type="button" class="btn btn-secondary" id="add-campo"> + </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group" id="planilha-cadastro2" style="display:none">
                        <label for="recipient-name" class="col-form-label">Seção:</label>
                        <select id="cad2" name="secao" class="form-control">
                            <option disabled selected>Selecione ...</option>
                        </select>
                    </div>
                    <div class="form-group" id="planilha-cadastro3" style="display:none">
                        <label for="recipient-name" class="col-form-label">Grupo:</label>
                        <select id="cad3" name="grupo" class="form-control">
                            <option disabled selected>Selecione ...</option>
                        </select>
                    </div>
                    <div class="form-group" id="planilha-cadastro4" style="display:none">
                        <label for="recipient-name" class="col-form-label">SubGrupo:</label>
                        <select id="cad4" name="subgrupo" class="form-control">
                            <option disabled selected>Selecione ...</option>
                        </select>

                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
            </form>


        </div>
    </div>
</div>
<div class="modal fade" id="adicionar" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="adicionar">Adicionar produto a apuração</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="formUpload">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Loja:</label>
                        <input type="number" name="store" class="form-control" value="<?=$store?>">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Codigo:</label>
                        <input type="number" name="code" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Descrição:</label>
                        <input type="text" name="description" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="inventario" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog  modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Realização de contagem de estoque</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="addList">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input required name="date" type="date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Data:</label>
                        <input required name="date" type="date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Lista:</label>
                        <select class="form-control" name="audit">
                            <option disabled selected>Selecione a Apuração ...</option>
                            <?php
                        foreach($apuracoes as $audit): ?>
                            <option value="<?=$audit->id?>"><?=$audit->description?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="apurar" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog  modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Apuração de resultados</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="buscar" id="buscar" method="post">
                    <input name="store" id="target2" type="hidden" value="<?=$store?>">
                </form>
                <form method="post" action="/apuracao/report">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input name="storeId" id="target" type="text" class="form-control valor" value="<?=$store?>"
                            readonly>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Data inicial:</label>
                        <select class="form-control" id="select1" name="date1" required>
                            <option disabled selected>Data Inicial ...</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Data final:</label>
                        <select class="form-control" id="select2" name="date2" required>
                            <option disabled selected>Data Final ...</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Lista:</label>
                        <select class="form-control" name="audit">
                            <option disabled selected>Selecione a Apuração ...</option>
                            <?php
                        foreach($apuracoes as $audit): ?>
                            <option value="<?=$audit->id?>"><?=$audit->description?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-check" style="margin-bottom:5%">
                        <input class="form-check-input" name="itens-movimentacao" type="checkbox" value="1"
                            id="flexCheckChecked">
                        <label class="form-check-label" for="flexCheckChecked">
                            Apenas itens com movimentação
                        </label>
                    </div>
                    <div style="margin-bottom:5%">
                        <span>Forma de apuração</span>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="movimento" id="flexRadioDefault1"
                                value="1" checked>
                            <label class="form-check-label" for="flexRadioDefault1">
                                Agendas com movimentação
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="movimento" id="flexRadioDefault2"
                                value="2">
                            <label class="form-check-label" for="flexRadioDefault2">
                                Agendas de Compra e Venda
                            </label>
                        </div>
                    </div>
                    <div style="margin-bottom:5%">
                        <span>Exibição</span>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="type" id="flexRadioDefault1" value="1"
                                checked>
                            <label class="form-check-label" for="flexRadioDefault1">
                                Apuração Quantitativa
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="type" value="2" id="flexRadioDefault2">
                            <label class="form-check-label" for="flexRadioDefault2">
                                Apuração Financeira
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="type" value="3" id="flexRadioDefault2">
                            <label class="form-check-label" for="flexRadioDefault2">
                                Apuração Geral
                            </label>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /Modais -->
<script>
    $("#alvo").change(function () {
        var select = document.getElementById('alvo');
        var value = select.options[select.selectedIndex].value;
        if (value == 1) {
            $("input[id='cad']").prop('required', true);
            $("input[id='cad']").prop('disabled', false);
            $("#planilha-cadastro").css("display", "");

            $("input[id='cad2']").prop('required', false);
            $("input[id='cad2']").prop('disabled', true);

            $("input[id='cad3']").prop('disabled', true);

            $("input[id='cad4']").prop('disabled', true);
            $("#planilha-cadastro2").css("display", "none");
            $("#planilha-cadastro3").css("display", "none");
            $("#planilha-cadastro4").css("display", "none");

        } else if (value == 2) {
            $("input[id='cad2']").prop('required', true);
            $("input[id='cad2']").prop('disabled', false);
            $("input[id='cad3']").prop('disabled', false);
            $("input[id='cad4']").prop('disabled', false);
            $("#planilha-cadastro2").css("display", "");
            $("#planilha-cadastro3").css("display", "");
            $("#planilha-cadastro4").css("display", "");

            $("input[id='cad']").prop('required', false);
            $("input[id='cad']").prop('disabled', true);
            $("#planilha-cadastro").css("display", "none");

        }

        if (value == 1 || value == 7 || value == 18 || value == 17 || value == 2 || value ==
            6) {
            if ($("#emergencial").length != 0) {

            } else {
                $('#criticidade').append(
                    '<option id="emergencial" value="4">Emergencial</option>');
            }

        } else {
            $('#emergencial').remove();
        }

    });
    var cont = 1;
    $("#add-campo").click(function () {
        cont++;
        $("#linhas").append('<tr id="campo' + cont +
            '"><td><input id="cad" class="form-control" name="store[' + cont +
            ']" required></td><td><input id="cad" class="form-control" name="code[' + cont +
            ']" required></td><td><input id="cad" class="form-control" name="description[' +
            cont +
            ']" required><td><button type="button" id="' + cont +
            '" class="btn btn-outline-danger btn-apagar"> - </button></td></tr>');
    });

    $("#linhas").on("click", ".btn-apagar", function () {
        var button_id = $(this).attr("id");
        $('#campo' + button_id).remove();
    });
    $(document).ready(function () {
        $('#btnEnviar').click(function () {
            $("#btnEnviar").prop('disabled', true);
            $('#formUpload').ajaxForm({
                uploadProgress: function (event, position, total,
                    percentComplete) {
                    $('progress').attr('value', percentComplete);
                    $('#porcentagem').html(percentComplete + '%');
                },
                success: function (data) {
                    $('progress').attr('value', '100');
                    $('#porcentagem').html('100%');
                    if (data.sucesso == true) {
                        $('#resposta').html('<a>' + data.msg + '</a>');
                        document.location.reload(true);
                    } else {
                        $("#btnEnviar").prop('disabled', false);
                        $('#resposta').html(
                            '<div class="alert alert-danger d-flex align-items-center" role="alert">' +
                            '<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>' +
                            '<div>' + data.msg + '</div></div>');
                    }
                },
                error: function () {
                    $("#btnEnviar").prop('disabled', false);
                    $('#resposta').html('Erro ao enviar requisição!!!');
                },
                dataType: 'json',
                url: '/request/addRequest'
            }).submit();
        })
    });

    $('form[name="formUpload"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/apuracao/add',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.sucesso === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        timer: 20000,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });

    $('form[name="addList"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/apuracao/addList',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.sucesso === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        timer: 20000,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });

    $(document).ready(function () {
        $("#select1").focus(function () {
            $("option[id='optiona']").remove();
            $("option[id='optionb']").remove();
            $('#buscar').ajaxForm({
                success: function (data) {
                    $.each(data, function (index, value) {
                        $("#select1").append($("<option id='optiona' value='1" +
                            value + "'>" + value.substr(4, 2) + "/" +
                            value
                            .substr(2, 2) + "/" + value.substr(0, 2) +
                            " " + index +
                            "</option>"));
                        $("#select2").append($("<option id='optionb' value='1" +
                            value + "'>" + value.substr(4, 2) + "/" +
                            value
                            .substr(2, 2) + "/" + value.substr(0, 2) +
                            " " + index +
                            "</option>"));
                    });
                },
                dataType: 'json',
                url: '/apuracao/dates',
                resetForm: true
            }).submit();
        })
    })

    $('#store').keyup(function () {
        $('#store2').val($(this).val());
    });
    $(document).ready(function () {
        $("select[name='tipo']").blur(function () {
            $("option[id='option1']").remove();
            $("option[id='option2']").remove();
            $("option[id='option3']").remove();
            $('#buscar').ajaxForm({
                success: function (data) {

                    $.each(data, function (index, value) {
                        $("select[name='secao']").append($(
                            "<option id='option1' value='" + index +
                            "'>" + value + "</option>"));
                    });
                },
                dataType: 'json',
                url: '/apuracao/secao',
                resetForm: true
            }).submit();
        })
        $("select[name='secao']").blur(function () {
            $('#secao2').val($(this).val());
            $("option[id='option2']").remove();
            $("option[id='option3']").remove();
            $('#buscar2').ajaxForm({
                success: function (data) {

                    $.each(data, function (index, value) {
                        $("select[name='grupo']").append($(
                            "<option id='option2' value='" + index +
                            "'>" + value + "</option>"));
                    });
                },
                dataType: 'json',
                url: '/apuracao/grupo',
                resetForm: true
            }).submit();
        })
        $("select[name='grupo']").blur(function () {
            $("option[id='option3']").remove();
            $('#grupo2').val($(this).val());
            $('#buscar2').ajaxForm({
                success: function (data) {

                    $.each(data, function (index, value) {
                        $("select[name='subgrupo']").append($(
                            "<option id='option3' value='" + index +
                            "'>" + value + "</option>"));
                    });
                },
                dataType: 'json',
                url: '/apuracao/subgrupo',
                resetForm: true
            }).submit();
        })
    });
</script>