<?php 
session_start();
$v->layout("client/_theme");

?>
<div class="container">
    <div class="bg-light p-5" style="margin-bottom:5%">
        <div class="container text-center">
            <h1 class="display-4">Notas Passiveis de Multa 1% </h1>
            <p class="lead"></p>
        </div>
    </div>
</div>
<div class="card-body position-absolute top-50 start-50 translate-middle" style="width: 300px;">
<form target="_blank" action="/multa" method="POST">
    <div class="mb-3">
        <label for="recipient-name" class="col-form-label">Emissão inicial:</label>
        <input type="date" name="date1" class="form-control">
    </div>
    <div class="mb-3">
        <label for="recipient-name" class="col-form-label">Emissão Final:</label>
        <input type="date" name="date2" class="form-control">
    </div>
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
    <button type="submit" class="btn btn-primary">Confirmar</button>
</form>
</div>