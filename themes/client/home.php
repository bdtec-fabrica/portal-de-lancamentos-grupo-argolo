<?php 
$v->layout("client/_theme");

?>
<main class="main_content">
    <div class="container">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-3">

                    <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento lançamentos diarios</h1>

                    <div class="col-sm-6">
                        <!-- <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active">Dashboard Chamados</li>
                        </ol> -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <div class="container-fluid" style="margin-bottom:5%">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?=$included?></h3>

                            <p>Incluidos</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-add"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/i"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-add"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$attendance?></h3>

                            <p>Cadastro</p>
                            <p><?=$attendanceA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-play"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/9"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-ios-play"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3><?=$completed?><sup style="font-size: 20px"></sup></h3>
                            <p>Concluídos</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/3"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$pendingCompany?></h3>

                            <p style="font-size:95%">Pendente Empresa</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-alert"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/1,4,5,6"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-alert"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$pendingClient?></h3>

                            <p>Pendente Cliente</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-person"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/2"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-person"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!--marvelous-->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-3">
                    <h1 class="m-0 text-dark" style="text-align:center">Duração media status diario (lançamentos
                        diretos)</h1>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="margin-bottom:5%">
            <div class="row">
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(1)?><sup style="font-size: 20px"></sup></h3>
                            <p>Pendente Empresa</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(1)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(2)?><sup style="font-size: 20px"></sup></h3>
                            <p>Pendente Cliente</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(2)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-indigo">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(4)?><sup style="font-size: 20px"></sup></h3>
                            <p>Pendente Informações</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(4)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-black">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(9)?><sup style="font-size: 20px"></sup></h3>
                            <p>Cadastro</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(9)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?=$nfEvent->getAverageDuration(5)?><sup style="font-size: 20px"></sup></h3>
                            <p>Pendente ICMS</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDuration(6)?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!--marvelous-->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-3">
                    <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Reportados</h1>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="margin-bottom:5%">
            <div class="row">
                <div class="col-lg-2 col-6">
                    <div class="small-box">
                        <div class="inner">
                            <h3><?=$clientTodayNfReportCount?></h3>
                            <p style="font-size:70%">NF reportadas hoje</p>
                            <p></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion ion-alert"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$clientTodayNfReport?>"
                            class="small-box-footer">Mais Informações <i class="ion ion ion-alert"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box">
                        <div class="inner">
                            <h3><?=$clientMonthlyNfReportCount?></h3>
                            <p style="font-size:70%">NF reportadas Este Mês</p>
                            <p></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion ion-alert"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$clientMonthlyNfReport?>"
                            class="small-box-footer">Mais Informações <i class="ion ion ion-alert"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- <section class="col-lg-12 connectedSortable ui-sortable">
            <div class="card">
                <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title">Incluidas e Concluidas por hora</h3>
                        <a href="javascript:void(0);">View Report</a> 
                    </div>
                </div>
                <div class="card-body">
                    <div class="d-flex">       
                    </div>
                    <div class="position-relative mb-4">
                        <canvas id="visitors-chart2" height="300"></canvas>
                    </div>

                    <div class="d-flex flex-row justify-content-end">
                        <span class="mr-2" style="font-size:80%">
                            <i style="background-color:#1771F1" class="fas fa-square"></i>Incluidas
                        </span>
                        <span class="mr-2" style="font-size:80%">
                            <i style="background-color:#41B619" class="fas fa-square"></i>Concluidas
                        </span>
                    </div>
                </div>
            </div>
        </section> -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-3">
                    <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Pendente Cliente Dias</h1>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC0Store($store)?><sup style="font-size: 20px"></sup></h3>
                        <p>Menos de 1 dia</p>
                        <p><?=$request->getItemsIds($request->getNfPC0)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC0?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC1Store($store)?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 1 dia</p>
                        <p><?=$request->getItemsIds($request->getNfPC1)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC1?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC2Store($store)?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 2 dias</p>
                        <p><?=$request->getItemsIds($request->getNfPC2)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC2?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC4Store($store)?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 4 dias</p>
                        <p><?=$request->getItemsIds($request->getNfPC4)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC4?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC7Store($store)?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 7 dias</p>
                        <p><?=$request->getItemsIds($request->getNfPC7)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC7?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC10Store($store)?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 10 dias</p>
                        <p><?=$request->getItemsIds($request->getNfPC10)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC10?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>
        </div>
        <!-- <script>
    $(function () {
      'use strict'

      var ticksStyle = {
        fontColor: '#495057',
        fontStyle: 'bold'
      }

      var mode = 'index'
      var intersect = true

      var $visitorsChart = $('#visitors-chart2')
          var visitorsChart = new Chart($visitorsChart, {
            data: {
            
              labels: [<?=$times?>],
              datasets: [
                {
                type: 'line',
                data: [<?=$includedHour?>],
                backgroundColor: 'transparent',
                borderColor: '#1771F1',
                pointBorderColor: '#1771F1',
                pointBackgroundColor: '#1771F1',
                pointRadius: '5',
                pointHoverRadius: '5',
                fill: false
              },
              {
                type: 'line',
                data: [<?=$completedHour?>],
                backgroundColor: 'transparent',
                borderColor: '#41B619',
                pointBorderColor: '#41B619',
                pointBackgroundColor: '#41B619',
                pointRadius: '5',
                pointHoverRadius: '5',
                fill: false
              }
            ]
            },
            options: {
              maintainAspectRatio: false,
              tooltips: {
                mode: mode,
                intersect: intersect
              },
              hover: {
                mode: mode,
                intersect: intersect
              },
              legend: {
                display: false
              },
              scales: {
                yAxes: [{
                  gridLines: {
                    display: true,
                    lineWidth: '4px',
                    color: 'rgba(0, 0, 0, .2)',
                    zeroLineColor: 'transparent'
                  },
                  ticks: $.extend({
                    beginAtZero: true,
                    suggestedMax: 10
                  }, ticksStyle)
                }],
                xAxes: [{
                  display: true,
                  gridLines: {
                    display: true
                  },
                  ticks: ticksStyle
                }]
              }
            }
          })
        })
  </script> -->


  