<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title; ?></title>
    <link rel="stylesheet" href="<?= url("/themes/css/horti.css");?>">
    <link rel="stylesheet" href="<?= url("/themes/css/bootstrap.min.css");?>">
    <script src="<?= url("/themes/js/bootstrap.min.js");?>"></script>
    <script src="<?= url("/themes/js/jquery-3.5.1.min.js");?>"></script>
    <script src="<?= url("/themes/js/popper.min.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.min.js");?>"></script>
    <script src="<?= url("/themes/js/jquery-ui.min.js");?>"></script>
    <script src="<?= url("/themes/js/bootstrap.bundle.min.js");?>"></script>
    <script src="<?= url("/themes/js/Chart.min.js");?>"></script>
    <script src="<?= url("/themes/js/sparkline.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.vmap.min.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.vmap.usa.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.knob.min.js");?>"></script>
    <script src="<?= url("/themes/js/moment.min.js");?>"></script>
    <script src="<?= url("/themes/js/daterangepicker.js");?>"></script>
    <script src="<?= url("/themes/js/tempusdominus-bootstrap-4.min.js");?>"></script>
    <script src="<?= url("/themes/js/summernote-bs4.min.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.overlayScrollbars.min.js");?>"></script>
    <script src="<?= url("/themes/js/adminlte.js");?>"></script>
    <script src="<?= url("/themes/js/dashboard.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.form.min.js");?>"></script>
    <script src="<?= url("/themes/js/sweetalert.min.js");?>"></script>
    <script src="<?= url("/themes/js/nav.js");?>"></script>
    <script src="<?= url("/themes/js/sortable.js");?>"></script>
    <script src="<?= url("/themes/js/demo.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.flot.resize.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.flot.pie.js");?>"></script>

</head>

<body>
    <div class="jumbotron jumbotron-fluid">
        <div class="container text-center">
            <h1 class="display-4">Hortifrut </h1>
            <p class="lead">Registro dos pedidos e precificação do Horti.</p>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <form name="formUpload" id="formUpload" method="post">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="campo">Codigo Interno</th>
                            <th class="campo" style="width: 200px;">Descrição</th>
                            <th class="campo">Embalagem</th>
                            <th class="campo">Quantidade</th>
                            <th class="campo">Custo Atual</th>
                            <th class="campo">Custo Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach ($aa2cestq as $cestq): ?>
                        <tr>
                            <td><?=$cestq->GIT_COD_ITEM?><input value="<?=$cestq->GIT_COD_ITEM?>" type="hidden"
                                    name="ean[<?=$cestq->GIT_COD_ITEM?>]"></td>
                            <td><?=trim($cestq->GIT_DESCRICAO)?></td>
                            <td><input id="embalagem<?=$cestq->GIT_COD_ITEM?>" value="1" class="form-control campo"></td>
                            <td><input id="quantidade<?=$cestq->GIT_COD_ITEM?>" class="form-control campo" name="amount[<?=$i?>]"></td>
                            <td><input id="custo<?=$cestq->GIT_COD_ITEM?>" class="form-control campo"></td>
                            <td><input id="custoT<?=$cestq->GIT_COD_ITEM?>" class="form-control campo" name="cost[<?=$i?>]">
                            </td>
                            <td><button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal<?=$cestq->GIT_COD_ITEM?>">+</button>
                            </td>
                        </tr>
                        <div class="modal fade" id="exampleModal<?=$cestq->GIT_COD_ITEM?>" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                        <span aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="row row-cols-2">
                                                <div class="col">Codigo Interno</div>
                                                <div class="col"><input readonly id="cad"
                                                        value="<?=$cestq->GIT_COD_ITEM?>" class="form-control campo"
                                                        name="ean[<?=$i?>]"></div>
                                                <div class="col">Descrição</div>
                                                <div class="col"><input value="<?=trim($cestq->GIT_DESCRICAO)?>"
                                                        class="form-control campo"></div>
                                                <div class="col">Quantidade</div>
                                                <div class="col"><input id="2quantidade<?=$cestq->GIT_COD_ITEM?>" class="form-control campo"
                                                        name="amount[<?=$i?>]"></div>
                                                <div class="col">Embalagem</div>
                                                <div class="col"><input id="2embalagem<?=$cestq->GIT_COD_ITEM?>" id="cad" value="1" class="form-control campo" name="packing[<?=$i?>]"></div>
                                                <div class="col">Custo Anterior</div>
                                                <div class="col"><input value="<?=str_replace(".",",",round($cestq->GET_CUS_ULT_ENT,2))?>"
                                                        class="form-control campo"></div>
                                                <div class="col">Custo Atual</div>
                                                <div class="col"><input id="2custo<?=$cestq->GIT_COD_ITEM?>" class="form-control campo">
                                                </div>
                                                <div class="col">Custo Total</div>
                                                <div class="col"><input id="2custoT<?=$cestq->GIT_COD_ITEM?>" class="form-control campo"
                                                        name="cost[<?=$i?>]">
                                                </div>
                                                <div class="col">Preço atual</div>
                                                <div class="col"><input value="<?=str_replace(".",",",round($cestq->PRE_PRECO,2))?>" class="form-control campo"></div>
                                                <div class="col">Margem Atual</div>
                                                <div class="col"><input
                                                        value="<?=str_replace(".",",",round($cestq->MARGEM,2))?>"
                                                        class="form-control campo"></div>
                                                <div class="col">Margem Desejada</div>
                                                <div class="col"><input id="margemCalculada<?=$cestq->GIT_COD_ITEM?>" class="form-control campo">
                                                </div>
                                                <div class="col">Preço Calculado</div>
                                                <div class="col"><input id="precoCalculado<?=$cestq->GIT_COD_ITEM?>" class="form-control campo">
                                                </div>
                                                <div class="col">Estoque</div>
                                                <div class="col"><input
                                                        value="<?=str_replace(".",",",round($cestq->GET_ESTOQUE,2))?>"
                                                        class="form-control campo"></div>
                                                <div class="col">Saida Media</div>
                                                <div class="col"><input
                                                        value="<?=str_replace(".",",",round($cestq->SAIDA_MEDIA,2))?>"
                                                        class="form-control campo"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $("#custo<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $( "#custoT<?=$cestq->GIT_COD_ITEM?>" ).val( ($( "#quantidade<?=$cestq->GIT_COD_ITEM?>" ).val() * $( "#embalagem<?=$cestq->GIT_COD_ITEM?>" ).val() )* $( "#custo<?=$cestq->GIT_COD_ITEM?>" ).val() );
                                $( "#2custo<?=$cestq->GIT_COD_ITEM?>").val($( "#custo<?=$cestq->GIT_COD_ITEM?>").val());
                            });
                            $("#2custo<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $( "#custo<?=$cestq->GIT_COD_ITEM?>").val($( "#2custo<?=$cestq->GIT_COD_ITEM?>").val());
                                $( "#custoT<?=$cestq->GIT_COD_ITEM?>" ).val( ($( "#quantidade<?=$cestq->GIT_COD_ITEM?>" ).val() * $( "#embalagem<?=$cestq->GIT_COD_ITEM?>" ).val() )* $( "#custo<?=$cestq->GIT_COD_ITEM?>" ).val() );
                                
                            });
                            $("#custoT<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $( "#custo<?=$cestq->GIT_COD_ITEM?>" ).val( $( "#custoT<?=$cestq->GIT_COD_ITEM?>" ).val() / ($( "#quantidade<?=$cestq->GIT_COD_ITEM?>" ).val() * $( "#embalagem<?=$cestq->GIT_COD_ITEM?>" ).val()));
                                $( "#2custoT<?=$cestq->GIT_COD_ITEM?>").val($( "#custoT<?=$cestq->GIT_COD_ITEM?>").val());
                            });

                            $("#2custoT<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $( "#custo<?=$cestq->GIT_COD_ITEM?>" ).val( $( "#2custoT<?=$cestq->GIT_COD_ITEM?>" ).val() / ($( "#2quantidade<?=$cestq->GIT_COD_ITEM?>" ).val() * $( "#2embalagem<?=$cestq->GIT_COD_ITEM?>" ).val()));
                                $( "#custoT<?=$cestq->GIT_COD_ITEM?>").val($( "#2custoT<?=$cestq->GIT_COD_ITEM?>").val());
                            });

                            $("#quantidade<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#2quantidade<?=$cestq->GIT_COD_ITEM?>").val($("#quantidade<?=$cestq->GIT_COD_ITEM?>").val());
                            });
                            $("#2quantidade<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#quantidade<?=$cestq->GIT_COD_ITEM?>").val($("#2quantidade<?=$cestq->GIT_COD_ITEM?>").val());
                            });

                            $("#margemCalculada<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#precoCalculado<?=$cestq->GIT_COD_ITEM?>").val(($("#2custo<?=$cestq->GIT_COD_ITEM?>").val() /((100 - $("#margemCalculada<?=$cestq->GIT_COD_ITEM?>").val() - <?=$cestq->TFIS_ALIQ_ICM?> - <?=$cestq->PIS_COFINS?>)/100)));
                            });
                            
                            $("#precoCalculado<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#margemCalculada<?=$cestq->GIT_COD_ITEM?>").val((100-(($("#2custo<?=$cestq->GIT_COD_ITEM?>").val()/$("#precoCalculado<?=$cestq->GIT_COD_ITEM?>").val())*100))+((<?=$cestq->PIS_COFINS?>-(<?=$cestq->PIS_COFINS?>*(<?=$cestq->TFIS_ALIQ_ICM?>/100)))+<?=$cestq->TFIS_ALIQ_ICM?>));
                            });

                            $("#embalagem<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#2embalagem").val($("#embalagem<?=$cestq->GIT_COD_ITEM?>").val());
                                $("#custo<?=$cestq->GIT_COD_ITEM?>").val($("#custoT<?=$cestq->GIT_COD_ITEM?>").val()/($("#embalagem<?=$cestq->GIT_COD_ITEM?>").val() * $("#quantidade<?=$cestq->GIT_COD_ITEM?>").val()));
                            });
                            $("#2embalagem<?=$cestq->GIT_COD_ITEM?>").blur(function () {
                                $("#embalagem").val($("#2embalagem<?=$cestq->GIT_COD_ITEM?>").val());
                            });
                        </script>
                        <?php 
                        $i++;
                    endforeach; ?>
                    </tbody>
                </table>
        </div>
    </div>
</body>

</html>