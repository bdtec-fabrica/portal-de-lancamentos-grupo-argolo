<?php 
session_start();
$v->layout("client/_theme");
?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Codigos Internos</h1>
        <p class="lead">Códigos Internos praticados pela loja</p>
    </div>
</div>
<div class="container">
    <form action="" method="POST" class="row g-4">
        <div class="col-auto">
            <label for="staticEmail2" class="visually-hidden">Loja</label>
            <input class="form-control" name="store" type="number" placeholder="Loja">
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-success mb-3">Buscar</button>
        </div>
        <div class="col-auto">
            <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal"
                data-bs-target="#adicionar">Adicionar
                +</button>
        </div>
    </form>
</div>
<?php if ($read === true): ?>
<div class="table-responsive">
    <table class="table align-middle">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Descrição</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($codigoInterno as $codigo): ?>
            <tr>
                <td><?=$codigo->code?></td>
                <td><?=$codigo->description?></td>
                <td>
                    <form name="delete<?=$row->id?>">
                        <input type="hidden" name="id" value="<?=$row->id?>">
                        <button type="submit" class="btn btn-danger">X</button>
                    </form>
                </td>
            </tr>
            <script>
                $('form[name="delete<?=$row->id?>"]').submit(function (event) {
                    event.preventDefault();

                    $.ajax({
                        url: '/kwDifference/delete',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.sucesso === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.msg,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.msg,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
<div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Adicionar Atividade</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form name="adicionar">
                <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input class="form-control" name="store" type="number">
                    </div>
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Codigo</label>
                        <input class="form-control" name="code" type="number">
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('form[name="adicionar"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/kwDifference/create',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.sucesso === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        timer: 600,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });
</script>