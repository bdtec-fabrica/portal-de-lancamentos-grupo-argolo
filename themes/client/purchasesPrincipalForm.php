<?php
$v->layout("client/_theme");
?>
<div class="position-absolute top-50 start-50 translate-middle" style="margin-top: 5%;">

    <div class="container text-center">
        <h3 class="display-4">Realizar Pedido de Compra</h3>
    </div>
    <form id="buscar2" method="post">
        <input name="store" type="hidden" id="store2">
        <input name="secao" type="hidden" id="secao2">
        <input name="grupo" type="hidden" id="grupo2">
    </form>
    <form name="buscar" id="buscar2" method="post">
        <input value="<?=$store?>" name="store" id="target2" type="hidden">
        <input name="secao" type="hidden" id="secao2">
        <input name="grupo" type="hidden" id="grupo2">
    </form>
    <form class=" row g-3" method="POST" action="/purshaseSecondaryForm">
        <div class="col-md-12">
            <label for="inputEmail4" class="form-label">Fornecedor</label>
            <input value="<?=$store?>" name="store" type="hidden" class="form-control" id="store">
            <input name="cnpj" type="number" class="form-control" placeholder="CNPJ" required>
            <!-- <select class="form-control alvo" id="alvo">
                <option selected disabled>Selectione ...</option>
                <option value="2">2</option>
                <?php foreach($aa2ctipo as $ctipo):  ?>
                <option>
                    <?=$ctipo->TIP_RAZAO_SOCIAL.' '.$ctipo->TIP_CODIGO.'-'.$ctipo->TIP_DIGITO.' '.$ctipo->TIP_CGC_CPF?>
                </option>
                <?php endforeach; ?>
            </select> -->
        </div>
        <div class="form-group" class="col-md-12">
            <label for="recipient-name" class="col-form-label">Seção:</label>
            <select id="cad2" name="secao" class="form-control">
                <option selected>Selecione ...</option>
            </select>
        </div>
        <div class="form-group" id="planilha-cadastro3" class="col-md-12">
            <label for="recipient-name" class="col-form-label">Grupo:</label>
            <select id="cad3" name="grupo" class="form-control">
                <option selected>Selecione ...</option>
            </select>
        </div>
        <div class="form-group" id="planilha-cadastro4" class="col-md-12">
            <label for="recipient-name" class="col-form-label">SubGrupo:</label>
            <select id="cad4" name="subgrupo" class="form-control">
                <option selected>Selecione ...</option>
            </select>

        </div>
        <div class="col-md-6">
            <label for="inputCity" class="form-label">Sistematica</label>
            <input type="text" name="sistematica" class="form-control" id="inputCity">
        </div>
        <div class="col-12">
            <button type="submit" style="margin-bottom:5%;" class="btn btn-primary">Confirmar</button>
        </div>
    </form>
    
</div>
<script>
    $('#target').keyup(function () {
        $('#target2').val($(this).val());
    });
    $(document).ready(function () {
        $("#target").blur(function () {
            $("option[id='optiona']").remove();
            $("option[id='optionb']").remove();
            $('#buscar').ajaxForm({
                success: function (data) {
                    $.each(data, function (index, value) {
                        $("#select1").append($("<option id='optiona' value='1" +
                            value + "'>" + value.substr(4, 2) + "/" +
                            value
                            .substr(2, 2) + "/" + value.substr(0, 2) +
                            " " + index +
                            "</option>"));
                        $("#select2").append($("<option id='optionb' value='1" +
                            value + "'>" + value.substr(4, 2) + "/" +
                            value
                            .substr(2, 2) + "/" + value.substr(0, 2) +
                            " " + index +
                            "</option>"));
                    });
                },
                dataType: 'json',
                url: '/apuracao/dates',
                resetForm: true
            }).submit();
        })
    })

    $('#store').keyup(function () {
        $('#store2').val($(this).val());
    });

    $(document).ready(function () {
        $("option[id='option1']").remove();
        $("option[id='option2']").remove();
        $("option[id='option3']").remove();
        $('#buscar2').ajaxForm({
            success: function (data) {

                $.each(data, function (index, value) {
                    $("select[name='secao']").append($(
                        "<option id='option1' value='" + index +
                        "'>" + value + "</option>"));
                });
            },
            dataType: 'json',
            url: '/apuracao/secao',
            resetForm: true
        }).submit();
        $("select[name='secao']").blur(function () {
            $('#secao2').val($(this).val());
            $("option[id='option2']").remove();
            $("option[id='option3']").remove();
            $('#buscar2').ajaxForm({
                success: function (data) {

                    $.each(data, function (index, value) {
                        $("select[name='grupo']").append($(
                            "<option id='option2' value='" + index +
                            "'>" + value + "</option>"));
                    });
                },
                dataType: 'json',
                url: '/apuracao/grupo',
                resetForm: true
            }).submit();
        })
        $("select[name='grupo']").blur(function () {
            $("option[id='option3']").remove();
            $('#grupo2').val($(this).val());
            $('#buscar2').ajaxForm({
                success: function (data) {

                    $.each(data, function (index, value) {
                        $("select[name='subgrupo']").append($(
                            "<option id='option3' value='" + index +
                            "'>" + value + "</option>"));
                    });
                },
                dataType: 'json',
                url: '/apuracao/subgrupo',
                resetForm: true
            }).submit();
        })
    });

    $('form[name="formUpload"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/apuracao/add',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.sucesso === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        timer: 20000,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });

    $('form[name="addList"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/apuracao/addList',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.sucesso === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        timer: 20000,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });

    $("#alvo").change(function () {
        var select = document.getElementById('alvo');
        var value = select.options[select.selectedIndex].value;
        $("input[id='cad2']").prop('required', true);
        $("input[id='cad2']").prop('disabled', false);
        $("input[id='cad3']").prop('disabled', false);
        $("input[id='cad4']").prop('disabled', false);
        $("#planilha-cadastro2").css("display", "");
        $("#planilha-cadastro3").css("display", "");
        $("#planilha-cadastro4").css("display", "");

        $("input[id='cad']").prop('required', false);
        $("input[id='cad']").prop('disabled', true);
        $("#planilha-cadastro").css("display", "none");

        if (value == 1 || value == 7 || value == 18 || value == 17 || value == 2 || value ==
            6) {
            if ($("#emergencial").length != 0) {

            } else {
                $('#criticidade').append(
                    '<option id="emergencial" value="4">Emergencial</option>');
            }

        } else {
            $('#emergencial').remove();
        }

    });
    var cont = 1;
    $("#add-campo").click(function () {
        cont++;
        $("#linhas").append('<tr id="campo' + cont +
            '"><td><input id="cad" class="form-control" name="store[' + cont +
            ']" required></td><td><input id="cad" class="form-control" name="code[' + cont +
            ']" required></td><td><input id="cad" class="form-control" name="description[' +
            cont +
            ']" required><td><button type="button" id="' + cont +
            '" class="btn btn-outline-danger btn-apagar"> - </button></td></tr>');
    });

    $("#linhas").on("click", ".btn-apagar", function () {
        var button_id = $(this).attr("id");
        $('#campo' + button_id).remove();
    });
    $(document).ready(function () {
        $('#btnEnviar').click(function () {
            $("#btnEnviar").prop('disabled', true);
            $('#formUpload').ajaxForm({
                uploadProgress: function (event, position, total,
                    percentComplete) {
                    $('progress').attr('value', percentComplete);
                    $('#porcentagem').html(percentComplete + '%');
                },
                success: function (data) {
                    $('progress').attr('value', '100');
                    $('#porcentagem').html('100%');
                    if (data.sucesso == true) {
                        $('#resposta').html('<a>' + data.msg + '</a>');
                        document.location.reload(true);
                    } else {
                        $("#btnEnviar").prop('disabled', false);
                        $('#resposta').html(
                            '<div class="alert alert-danger d-flex align-items-center" role="alert">' +
                            '<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>' +
                            '<div>' + data.msg + '</div></div>');
                    }
                },
                error: function () {
                    $("#btnEnviar").prop('disabled', false);
                    $('#resposta').html('Erro ao enviar requisição!!!');
                },
                dataType: 'json',
                url: '/request/addRequest'
            }).submit();
        })
    })
</script>