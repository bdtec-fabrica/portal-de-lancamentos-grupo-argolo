</html>
<!doctype html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= $title; ?></title>
  <link rel="stylesheet" href="<?= url("/themes/css/bootstrap.min.css");?>">
  <link rel="stylesheet" href="<?= url("/themes/css/nav.css");?>">
  <script src="<?= url("/themes/js/jquery-3.5.1.min.js");?>"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
    integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
    integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous">
  </script>
  <script src="<?= url("/themes/js/jquery.form.min.js");?>"></script>
  <script src="<?= url("/themes/js/sweetalert.min.js");?>"></script>
  <script src="<?= url("/themes/js/nav.js");?>"></script>
  <script src="<?= url("/themes/js/sortable.js");?>"></script>
  <style>
    #telaLogin {
      width: 350px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      box-shadow: 1px 5px 30px rgb(195, 195, 195);
      opacity: 0.9
    }
  </style>
</head>

<body style="background-image:url('<?=url("/themes/images/bd.jpeg")?>');background-position: center;">
  <div class="card" id="telaLogin">
    <div class="card-body">
      <form action="/doLogin" method="post">
        <div class="form-group">
          <label>Login</label>
          <input name="login" type="email" class="form-control" aria-describedby="emailHelp"
            placeholder="Informe seu Login">
        </div>
        <div class="form-group">
          <label>Senha</label>
          <input name="password" type="password" class="form-control" id="exampleInputPassword1"
            placeholder="Insira sua senha">
        </div>
        <?php 
            if(isset($erro)):
        ?>
        <div id="erro" class="alert alert-danger" role="alert" style="margin-top:2%;margin-bottom:2%">Login e/ou senha
          invalido.</div>
        <?php
            endif;
        ?>
        <div style="margin-left:35%;margin-top:5%">
          <button type="submit" class="btn btn-outline-success btn-lg btn-block">Entrar</button>
        </div>
      </form>
    </div>
    <div class="card container" style="padding:2%;text-align:center ">
      <p>Novo por aqui ? <button class="btn btn-outline-info" style="margin-left:2%" data-bs-toggle="modal"
          data-bs-target="#singUp">Cadastre-se</button></p>
    </div>
    <div class="card container" style="padding:2%;text-align:center ">
      <p>Esqueceu a senha ? <button class="btn btn-outline-warning" style="margin-left:2%" data-bs-toggle="modal"
          data-bs-target="#forgotPassword">Enviar senha</button></p>
    </div>
  </div>
  <div class="modal fade" id="forgotPassword" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Enviar senha por e-mail</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form name="forgotPassword">
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Email:</label>
              <input type="email" id="login" class="form-control" name="email" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-success">Enviar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="singUp" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastre-se</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form name="adicionar">
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Loja:</label>
              <input type="number" id="store" min="1"  class="form-control" name="store" required>
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Nome Completo</label>
              <input type="text" class="form-control" name="name" required>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Email:</label>
              <input type="email" class="form-control" name="login" required>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Telefone:</label>
              <input type="tel" placeholder="(xx)xxxxx-xxxx" class="form-control" name="tel" pattern="[0-9]{11}" required>
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Grupo</label>
              <select name="storeGroup" class="form-control" required placeholder="Selecione ...">
              <option value="" disabled selected hidden>Selecione ...</option>
                <?php foreach ($storeGroups as $storeGroup):
                if ($storeGroup->store == 'N'):
                  ?>
                <option value="<?=$storeGroup->id?>"><?=$storeGroup->name?></option>
                <?php 
                endif;
                endforeach;
                ?>
              </select>
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Senha</label>
              <input type="password" class="form-control" name="password" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-success">Salvar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script>
    $('form[name="adicionar"]').submit(function (event) {
      event.preventDefault();

      $.ajax({
        url: '/usuarios/usersAdd',
        type: 'post',
        data: $(this).serialize(),
        dataType: 'json',
        success: function (response) {

          if (response.success === true) {
            swal({
              title: "Bispo & Dantas",
              text: response.message,
              timer: 1200,
              icon: "success",
              showCancelButton: false,
              showConfirmButton: false,
              type: "success"
            });
            document.location.reload(true);
            
          } else {
            swal({
              title: "Bispo & Dantas",
              text: response.message,
              icon: "error",
              showCancelButton: false,
              showConfirmButton: false,
              type: "danger"
            });
          }

        }
      })
    });
    $('form[name="forgotPassword"]').submit(function (event) {
      event.preventDefault();

      $.ajax({
        url: '/login/forgotPassword',
        type: 'post',
        data: $(this).serialize(),
        dataType: 'json',
        success: function (response) {

          if (response.success === true) {
            swal({
              title: "Bispo & Dantas",
              text: response.message,
              timer: 1200,
              icon: "success",
              showCancelButton: false,
              showConfirmButton: false,
              type: "success"
            });
            
          } else {
            swal({
              title: "Bispo & Dantas",
              text: response.message,
              icon: "error",
              showCancelButton: false,
              showConfirmButton: false,
              type: "danger"
            });
          }

        }
      })
    });
  </script>

</body>

</html>