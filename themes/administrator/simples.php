<?php 
$v->layout("administrator/adm_theme");

?>

<table class="table dt-responsive table-hover nowrap w-100 sortable" style="font-size:75%">
    <thead class="">
        <tr>
            <th>Referencia</th>
            <th>Descrição</th>
            <th>NCM</th>
            <th>CFOP</th>
            <th>Quantidade</th>
            <th>Valor</th>
            <th>Base de calculo</th>
            <th>Aliquota</th>
            <th>Valor ICMS</th>
            <th>Número Nota</th>
            <th>Razão Social</th>
            <th>Emissão</th>
            <th>Chave de Acesso</th>

        </tr>
    </thead>
    <tbody>

        <?php 
            if ($nfs):
            foreach ($nfs as $nf):
                if ($nf->getSimples()):

                    foreach ($nf->getItems() as $item):
                        
                            
                            
                            

            ?>
        <tr <?php if ($nf->getStatus == 2):
                ?> style="background:green" <?php endif; 
                ?>>
            <td><?=$item->prod->cProd?></td>
            <td><?=$item->prod->xProd?></td>
            <td><?=$item->prod->NCM?></td>
            <td><?=$item->prod->CFOP?></td>
            <td><?=$item->prod->qCom?></td>
            <td><?=$item->prod->vUnCom?></td>
            <td><?=$item->prod->vProd?></td>
            <td><?=$ncmAliquota->aliquotaS($item->prod->NCM)?></td>
            <?php if ($ncmAliquota->aliquotaS($item->prod->NCM) > 0):
                $vlTotal += $item->prod->vProd; 
                $vlIcms += $item->prod->vProd * ($ncmAliquota->aliquotaS($item->prod->NCM)/100);
                if ($item->prod->CFOP == 5101):
                    $vlIcmsNota += $item->prod->vProd * (12/100);
                endif;?>
            <td><?=$item->prod->vProd * ($ncmAliquota->aliquotaS($item->prod->NCM)/100)?></td>
            <?php else: ?>
                <td><?=$item->prod->vProd * $ncmAliquota->aliquotaS($item->prod->NCM)?></td>
            <?php endif; ?>
            <td><?= $nf->nfe_number?></td>
            <td><?= $nf->getFornecedor?></td>
            <td><?= $nf->emission_date?></td>
            <td><?= $nf->access_key?></td>

            <td>

        </tr>
        <?php 

            endforeach;
            endif;
            endforeach;
            endif;
            ?>
    </tbody>
</table>
<table class="table dt-responsive table-hover nowrap w-100 sortable" style="font-size:75%">
    <thead class="">
        <tr>
            <th>Valor Total Dos produtos</th>
            <th>Valor ICMS</th>
            <th>Valor ICMS Nota</th>
            <th>Valor de ICMS não Creditado</th>
            

        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=round($vlTotal,2)?></td>
            <td><?=round($vlIcms,2)?></td>
            <td><?=round($vlIcmsNota,2)?></td>
            <td><?=round($vlIcms - $vlIcmsNota,2)?></td>
        </tr>
    </tbody>
</table>