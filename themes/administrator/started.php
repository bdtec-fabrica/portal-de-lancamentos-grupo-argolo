<?php 
$v->layout("administrator/adm_theme");
?>
<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Painel Antecipação de Lançamentos BDTec</h1>
                <p class="lead"></p>
            </div>
        </div>
        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
            data-bs-toggle="collapse" data-bs-target="#multiCollapseExample1" aria-expanded="false"
            aria-controls="multiCollapseExample1">Filtro
            ↴</button>

    </div>
    <div class="collapse multi-collapse container" id="multiCollapseExample1">
        <form action="/request/searchRequest" method="post">
            <div class="row">
                <div class="form-group col-md-1">
                    <span>#Codigo</span>
                    <input name="id" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Número</span>
                    <input name="nfNumber" class="form-control" type="text">
                </div>
                <div class="form-group col-md-5">
                    <span>Chave Acesso</span>
                    <input name="accessKey" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação inicial</span>
                    <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação final</span>
                    <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Alteração inicial</span>
                    <input name="alteracaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Alteração final</span>
                    <input name="alteracaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Operador</span>
                    <select name="idOperator" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($users as $user):
                            ?>
                        <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <span>Status</span>
                    <select name="status" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($status as $statuss):
                            ?>
                        <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-1">
                    <span>Loja</span>
                    <input class="form-control" name="store">
                </div>
                <div class="form-group col-md-2">
                    <span>Grupo</span>
                    <select name="storeGroup" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($storeGroups as $storeGroup):
                            ?>
                        <option value="<?= $storeGroup->id ?>"><?= $storeGroup->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <span>Operação</span>
                    <select name="operation" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todas</option>
                        <?php
                        foreach ($nfeOperation as $nfeOperations):
                            ?>
                        <option value="<?= $nfeOperations->id ?>"><?= $nfeOperations->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <span>Tipo</span>
                    <select name="type" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($nfeType as $nfeTypes):
                            ?>
                        <option value="<?= $nfeTypes->id ?>"><?= $nfeTypes->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <div class="table-responsive-lg">
        <table class="table dt-responsive table-hover nowrap w-100 sortable" style="font-size:60%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Loja</th>
                    <th>Número</th>
                    <th>Inclusão</th>
                    <th>Fornecedor</th>
                    <th>Chave de acesso</th>
                    <th>Qtd</th>
                    <th>Alteração</th>
                    <th style="min-width:150px">Status</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>
            <?php if($starteds):
                  foreach ($starteds as $started):
            ?>
                <tr>
                    <th scope="row"><?=$started->id?></th>
                    <td><?= $started->getStore?></td>
                    <td><?= $started->getNfNumber?></td>
                    <td><?= $started->getCreatedAt?></td>
                    <td><?= $started->getFornecedor?></td>
                    <td><?= $started->access_key?></td>
                    <td><?= $started->itemsAmount?></td>
                    <td><?= $started->getUpdatedAt?></td>
                    <td>
                    <form name="update<?=$started->id?>">
                        <div class="input-group">
                        <input type="hidden" name="id" value="<?=$started->id?>">
                        <select class="form-select" id="inputGroupSelect04" aria-label="Example select with button addon" name="status" style="font-size:90%">
                        <option value="<?=$started->status_id?>"><?= $started->getStatus?></option>
                        <?php
                            foreach ($status as $statuss):
                            ?>
                        <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                        <?php
                            endforeach;
                        ?>
                        </select> 
                        </div>
                   
                        </td>
                        <td>
                        <div class="btn-group me-2" role="group" aria-label="First group">
                    <button class="btn btn-success" type="submit"><i class="ion ion-checkmark-circled"></i></button>
                    <script>
                                    $('form[name="update<?=$started->id?>"]').submit(function (event) {
                                            event.preventDefault();
                                            $.ajax({
                                                url: '/start/update',
                                                type: 'post',
                                                data: $(this).serialize(),
                                                dataType: 'json',
                                                success: function (response) {
                                                    if (response.success === true) {
                                                        swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            timer: 200,
                                                            icon: "success",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "success"
                                                        });
                                                        
                                                    } else {
                                                        swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            icon: "error",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "danger"
                                                        });
                                                    }
                                                }
                                            })
                                        });
                                </script>
                    </form>
                    <button type="button" data-bs-placement="top" title="Editar" class="btn btn-warning" data-bs-toggle="modal"
                        data-bs-target="#editar<?=$started->id?>" data-whatever="@mdo"><i class="ion ion-edit"></i></button>
                                    <div class="modal fade" id="editar<?=$started->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Adicionar Obervação</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="editar<?=$started->id?>">
                                    <div class="form-group">
                                        <label for="message-text" required class="col-form-label">Descrição:</label>
                                        <input type="hidden" name="id" value="<?=$started->id?>">
                                        <textarea name="note" class="form-control"
                                        id="message-text"><?= str_replace("<br />","",$started->note) ?></textarea>
                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                </form>
                                <script>
                                    $('form[name="editar<?=$started->id?>"]').submit(function (event) {
                                        event.preventDefault();
                                        $.ajax({
                                            url: '/start/note',
                                            type: 'post',
                                            data: $(this).serialize(),
                                            dataType: 'json',
                                            success: function (response) {

                                                if (response.success === true) {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        timer: 20000,
                                                        icon: "success",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "success"
                                                    });
                                                    document.location.reload(true);
                                                } else {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        icon: "error",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "danger"
                                                    });
                                                }
                                            }
                                        })
                                    });
                                </script>
                                    </div>
                            </div>
                        </div>
                    </div>
                    </td>
                </tr>
            <?php 
                  endforeach;
                  endif;
                  ?>
            </tbody>
        </table>
    </div>
</main>
