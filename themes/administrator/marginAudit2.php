<?php 
session_start();
$v->layout("administrator/adm_theme");

?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Auditoria de Margens Vendas</h1>
        <p class="lead">Demonstrativo das margens geradas a partir das vendas.</p>
    </div>
</div>
<div class="container" style="margin-bottom:5%">
    <form method="post" action="/auditoria-margem2/search" class="row gx-3 gy-2 align-items-center"
        style="margin-bottom:2%">
        <div class="col-sm-2">
            <label for="specificSizeInputName">Loja</label>
            <input name="store" type="text" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Data agenda inicial</label>
            <input name="initialDate" type="date" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Data agenda final</label>
            <input name="finalDate" type="date" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Margem ></label>
            <input type="number" name="maior" class="form-control">
        </div>

        <div class="col-sm-2">
            <label for="specificSizeInputName">Margem <</label>
            <input type="number" name="menor" class="form-control">
        </div>

        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th>Loja</th>
            <th>Nota</th>
            <th>Codigo</th>
            <th>Descrição</th>
            <th>Qtd</th>
            <th>Emb</th>
            <th>Custo Bruto (R$)</th>
            <th>Custo Liquido (R$)</th>
            <th>ICMS (%)</th>
            <th>PIS/COFINS (%)</th>
            <th>Margem (%)</th>
            <th>Preço (R$)</th>
            
        </tr>
    </thead>
    <tbody>
            <?php
        foreach ($ag1iensa as $movment):
            if($movment->validateStore2):
                if($movment->getMargin2($movment->ESCHLJC_CODIGO3) >= $maior || $movment->getMargin2($movment->ESCHLJC_CODIGO3) <= $menor):
                    if ($operador == ''):
               ?>
                        <tr>
                            <td><?=$movment->ESCHLJC_CODIGO3?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=$movment->ENTSAIC_QUANTI_UN/$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCHLJC_CODIGO3),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin2($movment->ESCHLJC_CODIGO3))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice2,2))?></td>
                            <?php if ($movment->getAA1CFISC()):
                            ?>
                            
                            <?php endif?>
                            
                        </tr>
               <?php  
                    else:
                        if ($movment->getAA1CFISC()):
                        if($operador == $movment->getAA1CFISC()->getNfReceipt()->operator_id):
                            ?>
                        <tr>
                            <td><?=$movment->ESCHLJC_CODIGO3?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=$movment->ENTSAIC_QUANTI_UN/$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCHLJC_CODIGO3),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin2($movment->ESCHLJC_CODIGO3))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice2,2))?></td>
                            <?php if ($movment->getAA1CFISC()):
                            ?>
                            
                            <?php endif?>
                        </tr>
               <?php 
                        endif;
                    endif;
                    endif;               
               endif;               
            else:
               
            endif;
            
        endforeach;
        ?>
                    <?php
        foreach ($ag1iensa2 as $movment):
            
            if($movment->validateStore2):
                
                if($movment->getMargin2($movment->ESCHLJC_CODIGO3) >= $maior || $movment->getMargin2($movment->ESCHLJC_CODIGO3) <= $menor):
                    
                    if ($operador == ''):
               ?>
                        <tr>
                            <td><?=$movment->ESCHLJC_CODIGO3?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=$movment->ENTSAIC_QUANTI_UN/$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCHLJC_CODIGO3),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin2($movment->ESCHLJC_CODIGO3))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice2,2))?></td>
                            <?php if ($movment->getAA1CFISC()):
                            ?>
                            
                            <?php endif?>
                            
                        </tr>
               <?php  
                    else:
                        if ($movment->getAA1CFISC()):
                        if($operador == $movment->getAA1CFISC()->getNfReceipt()->operator_id):
                            ?>
                        <tr>
                            <td><?=$movment->ESCHLJC_CODIGO3?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=$movment->ENTSAIC_QUANTI_UN/$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCHLJC_CODIGO3),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin2($movment->ESCHLJC_CODIGO3))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice2,2))?></td>
                            <?php if ($movment->getAA1CFISC()):
                            ?>
                            
                            <?php endif?>
                        </tr>
               <?php 
                        endif;
                    endif;
                    endif;               
               endif;               
            else:
               
            endif;
            
        endforeach;
        ?>
                    <?php
        foreach ($ag1iensa3 as $movment):
            if($movment->validateStore2):
                if($movment->getMargin2($movment->ESCHLJC_CODIGO3) >= $maior || $movment->getMargin2($movment->ESCHLJC_CODIGO3) <= $menor):
                    if ($operador == ''):
               ?>
                        <tr>
                            <td><?=$movment->ESCHLJC_CODIGO3."-".$movment->ESCLC_DIGITO?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=$movment->ENTSAIC_QUANTI_UN/$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCHLJC_CODIGO3),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin2($movment->ESCHLJC_CODIGO3))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice2,2))?></td>
                            <?php if ($movment->getAA1CFISC()):
                            ?>
                            
                            <?php endif?>
                            
                        </tr>
               <?php  
                    else:
                        if ($movment->getAA1CFISC()):
                        if($operador == $movment->getAA1CFISC()->getNfReceipt()->operator_id):
                            ?>
                        <tr>
                            <td><?=$movment->ESCHLJC_CODIGO3."-".$movment->ESCLC_DIGITO?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=$movment->ENTSAIC_QUANTI_UN/$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=$movment->ENTSAIC_BASE_EMB?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCHLJC_CODIGO3),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin2($movment->ESCHLJC_CODIGO3))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice2,2))?></td>
                            <?php if ($movment->getAA1CFISC()):
                            ?>
                            
                            <?php endif?>
                        </tr>
               <?php 
                        endif;
                    endif;
                    endif;               
               endif;               
            else:
               
            endif;
            
        endforeach;
        ?>

    </tbody>
</table>