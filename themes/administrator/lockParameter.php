<?php 
session_start();
$v->layout("administrator/adm_theme");
?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Usuários</h1>
        <p class="lead">Usuários legados de atendimento.bispoedantas.com.br</p>
    </div>
</div>
<div class="container">
    <form action="/lockParameter/read" method="POST" class="row g-4">
        <div class="col-auto">
            <label for="staticEmail2" class="visually-hidden">Operador</label>
            <select name="type" class="form-select" aria-label="Default select example">
                <option selected disabled>Selecione ...</option>
                <option value="O">Operador</option>
                <option value="F">Fornecedor</option>
                <option value="L">Loja</option>
            </select>
        </div>
        <div class="col-auto">
            <label for="inputPassword2" class="visually-hidden">Valor</label>
            <input type="number" name="value" class="form-control" placeholder="Valor">
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-success mb-3">Buscar</button>
        </div>
        <div class="col-auto">
            <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal"
                data-bs-target="#adicionar">Adicionar
                +</button>
        </div>
    </form>
</div>
<?php if ($read === true): ?>
<div class="table-responsive">
    <table class="table align-middle">
        <thead>
            <tr>
                <th>Tipo</th>
                <th>Valor</th>
                <th>Obsevação</th>
                <th>Criado em</th>
                <th>Alterado em</th>
                <th>Remover</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($rows as $row): ?>
            <tr>
                <td><?=$row->type?></td>
                <td><?=$row->parameter_id?></td>
                <td><?=$row->note?></td>
                <td><?=$row->created_at?></td>
                <td><?=$row->updated_at?></td>
                <td>
                    <form name="delete<?=$row->id?>">
                        <input type="hidden" name="id" value="<?=$row->id?>">
                        <button type="submit" class="btn btn-danger">X</button>
                    </form>
                </td>
            </tr>
            <script>
                $('form[name="delete<?=$row->id?>"]').submit(function (event) {
                    event.preventDefault();

                    $.ajax({
                        url: '/lockParameter/delete',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.sucesso === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.msg,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.msg,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif; ?>
<div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Adicionar Parâmetro</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form name="adicionar">
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Tipo:</label>
                        <select name="type" class="form-select" name="type">
                            <option selected disabled>Selecione ...</option>
                            <option value="O">Operador</option>
                            <option value="F">Fornecedor</option>
                            <option value="L">Loja</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Valor:</label>
                        <input type="number" class="form-control" name="value">
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('form[name="adicionar"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/lockParameter/create',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.sucesso === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        timer: 600,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.msg,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });
</script>