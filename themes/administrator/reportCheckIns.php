<?php 
$v->layout("administrator/adm_theme");

?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Relatório de batimento de ponto</h1>
        <p class="lead"></a></p>
    </div>
</div>
<div>
    <?php
    foreach($operators as $operator):
        ?>
    <div class="container">
    <table class="table table-bordered">
        <h4><?=$operator?></h4>
        <tr>
            <th style="width:300px">Dia</th>
            <?php
            $count = 1;
            $a = array(1,2,3,4);
        foreach($a as $daily):
        ?>
            <th><?=$count?>ª Batida</th>
            <?php
            $count++;
        endforeach;
        ?>
            <th>Quantidade de Horas Trabalhadas
        </tr>
        </tr>
        <?php
        foreach($dailyOperators[$operator] as $daily):
        ?>
        <tr>
            <td><?=  substr($daily->created_at,0,10)?></td>
            <?php
        $calc = 1;
        foreach($daily->getCheckIns as $checkIn):
        ?>
            <td><?= substr($checkIn->created_at,11) ?></td>
            <?php
        $calc++;
        endforeach;
        for($calc=$calc;$calc<=4;$calc++):
        ?>
            <td></td>
            <?php
        endfor;
        ?>
            <td>
                <?php 
                
                if ($soma == null):
                    $soma = $daily->countHours($daily->user_id,substr($daily->created_at,0,10));
                    
                else:
                    
                    $soma = somarHoras($daily->countHours($daily->user_id,substr($daily->created_at,0,10)),$soma);
                    
                endif;
                echo $daily->countHours($daily->user_id,substr($daily->created_at,0,10))?>
            </td>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
        <tr><th><?=$soma?></th></tr>
    </table>
    </div>
    <?php
    $soma = null;
    endforeach;
    ?>