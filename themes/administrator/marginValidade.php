<?php 
session_start();
$v->layout("administrator/adm_theme");
?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Validação de Margens</h1>
        <p class="lead">Esta ferramenta foi criada para que sejam feitas as validações das notificações de margens
            possivelmente divergentes e envio para os proprietarios dos estabelecimentos. </p>
    </div>
</div>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <p class="lead">Lista de notificações</p>
    </div>
</div>
<div class="alert alert-danger" role="alert">
  Ao selecionar o status da aprovação a alteração será realizada. Atenção! Ao selecionar Sim para aprovada, a margem supostamente divergente será enviada para o(s) resposavel(eis) da loja.
  Ao Selecionar corrigida, a margem incorreta será enviada para os administradores.
</div>
<div class="table-responsive-lg">
    <table class="table dt-responsive table-hover sortable">
        <thead>
            <tr>
                <th>Loja</th>
                <th>NFE</th>
                <th>Fornecedor</th>
                <th>Código</th>
                <th>Descrição</th>
                <th>Custo</th>
                <th>Preço(R$)</th>
                <th>Margem(%)</th>
                <th>Operador</th>
                <th style="min-width: 150px;">Aprovada</th>
                <th>Enviada</th>
                <th>Criação</th>
                <th>Alteração</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($marginVerification as $notification): ?>
            <tr id="tr<?=$notification->id?>">
                <td><?=$notification->store_id?></td>
                <td><?=$notification->nf_number?></td>
                <td><?=$notification->provider?></td>
                <td><?=$notification->code?></td>
                <td><?=$notification->description?></td>
                <td><?=$notification->cost?></td>
                <td><?=$notification->price?></td>
                <td><?=$notification->margin?></td>
                <td><?=$notification->user?></td>
                <td>
                    <form id="formApproved<?=$notification->id?>" method="post">
                        <input type="hidden" name="id" value="<?=$notification->id?>">
                        <select class="form-control" name="approved" id="approved<?=$notification->id?>">
                            <option readonly disabled selected value="<?=$notification->approved?>">
                                <?=$notification->getApprovedName?></option>
                            <option value="C">Corrigido</option>
                            <option value="N">Não</option>
                            <option value="S">Sim</option>
                        </select>
                    </form>
                    <script>
                        $(document).ready(function () {
                            $("#approved<?=$notification->id?>").blur(function () {
                                $('#formApproved<?=$notification->id?>').ajaxForm({
                                    success: function (data) {
                                        if (data.save == "ok") {
                                            // $('#tr<?=$notification->id?>').remove();
                                        } else {
                                            
                                        }

                                    },
                                    dataType: 'json',
                                    url: '/margin/validate/update'
                                }).submit();
                            })
                        });
                    </script>
                </td>
                <td><?=$notification->sent?></td>
                <td><?=$notification->created_at?></td>
                <td><?=$notification->updated_at?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>