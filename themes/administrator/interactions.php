<?php

use Source\Models\User;

$v->layout("administrator/adm_theme");
?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Lançamentos</h1>
        <p class="lead">Interações e Feedbacks.</p>
    </div>
</div>

<div style="margin-left: 2%;margin-right:2%">
    <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="adicionar" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="adicionar">Adicionar interação</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="formUpload" id="formUpload" method="post">
                        <input name="idRequest" value="<?=$id?>" type="hidden">
                        <input name="idUser" value="<?=$_SESSION['id']?>" type="hidden">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="files[]" multiple type="file" class="form-control" id="inputGroupFile01"
                                accept="audio/*;capture=microphone">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Observação:</label>
                            <textarea name="descricao" required class="form-control" id="message-text"></textarea>
                        </div>
                        <div class="col-12">
                        <?php if (1 == 2): ?>
                            <div class="form-check">
                                <input name="check" class="form-check-input" type="checkbox" value="1"
                                    id="invalidCheck">
                                <label class="form-check-label" for="invalidCheck">
                                    Marcar como concluido ?
                                </label>
                            </div>
                            <?php endif;?>
                        </div>
                </div>
                <div class="modal-footer">
                    <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                        id="porcentagem">0%</span>
                    <div id="resposta" style="width:100%"></div>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#btnEnviar').click(function () {
                $('#formUpload').ajaxForm({
                    uploadProgress: function (event, position, total, percentComplete) {
                        $('progress').attr('value', percentComplete);
                        $('#porcentagem').html(percentComplete + '%');
                    },
                    success: function (data) {
                        $('progress').attr('value', '100');
                        $('#porcentagem').html('100%');
                        if (data.sucesso == true) {
                            $('#resposta').html('<a>' + data.msg + '</a>');
                            document.location.reload(true);
                        } else {
                            $('#resposta').html(data.msg);
                        }

                    },
                    error: function () {
                        $('#resposta').html('Erro ao enviar requisição!!!');
                    },
                    dataType: 'json',
                    url: '/request/interacao/add',
                    resetForm: true
                }).submit();
            })
        })
    </script>
</div>
<div class="" style="margin-top:5%">
    <div class="card" style="margin-left:2%;margin-right:2%;margin-bottom:2%">
        <div class="card-header">
            <?php
            if ($request->getReportData):
                ?>
            <form name="report">
                <input type="hidden" name="id" value="<?=$request->getReportData()->id?>">
                <?php if ($_COOKIE['id'] == 1 || $_COOKIE['id'] == 150): ?>
                <button class="btn btn-success" type="submit">Report Indevido</button>
                <?php endif; ?>
            </form>
            <?php
            endif;
            ?>
            <script>
                $('form[name="report"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/desfazreport',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 20000,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }
                        }
                    })
                });
            </script>
            <h3><?="#".$request->id." - NF: ".$request->nf_number;?></h3>
            <?php
             if($request->access_key):
            echo "<h4>Chave de Acesso: ".$request->access_key."</h4>";
            endif;
            echo $request->getNotaFiscalRetaguarda();
        ?>
            <h5>
                <?= $request->userAdministrator()->name." - <a style='font-size:70%;color:#5199FF'>".$request->getCreatedAtInt?></a>
            </h5>
            <div class="row">
                <div class="container">
                    <div class="row row-cols-6">
                        <div class="col"><a download style="text-decoration:none;margin-bottom:5%"
                                href="<?="/../../source/XML/$request->store_id/NFe$request->access_key.xml"?>"><button
                                    class="btn btn-outline-success">XML <i
                                        class="ion ion-android-download"></i></button></a></div>
                        <div class="col">
                            <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                <input type="hidden" name="key" value="<?=$request->access_key?>">
                                <input type="hidden" name="store" value="<?=$request->store_id?>">
                                <a>
                                    <button style="margin-bottom:5%" type="submit" class="btn btn-outline-warning">PDF
                                        <i class="ion ion-android-download"></i></button></a>
                            </form>
                        </div>
                        <div class="col"><?php if ($request->status_id): ?>
                            <button type="button" data-bs-placement="top" title="Reporte" class="btn btn-outline-danger"
                                data-bs-toggle="modal" data-bs-target="#reporte<?=$request->id?>"
                                data-whatever="@mdo">Reportar</button>
                            <?php endif; ?></div>
                        <div class="col"><?php if ($request->group_id == 1 || $request->group_id == 2): ?>
                            <a target="_blank" class="btn btn-danger"
                                href="<?=URL_BASE?>/margin/webservice36?id=<?=$request->id?>">Checar
                                Custos</a>
                            <?php endif; ?></div>
                        <div class="col">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Desbloquear NFe</button>
                        </div>
                        <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id']  or $_COOKIE['id'] == 178 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 20000151): ?>
                        <div class="col">
                        <button type="button" data-bs-placement="top" title="Editar" class="btn btn-warning" data-bs-toggle="modal"
                                    data-bs-target="#editar<?=$request->id?>" data-whatever="@mdo"><i class="ion ion-edit"></i></button>

                        </div>
                        <?php endif;?>
                        <div class="modal fade" id="editar<?=$request->id?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alterar Postagem</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form name="editar<?=$request->id?>">
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Número do documento:</label>
                                                <input name="nfNumber" required type="text" class="form-control" id="recipient-name" value="<?= $request->nf_number?>">
                                                <input name="id" type="hidden" value="<?= $request->id?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                                <input name="accessKey" type="text" class="form-control" id="recipient-name" value="<?= $request->access_key?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Operador:</label>
                                                <select name="operatorId" required class="custom-select" id="inputGroupSelect01" 
                                                <?php $user = (new User())->findById($_COOKIE['id']); ?>
                                                <?php if ($user->libera != 'S'): ?>
                                                disabled
                                                <?php endif; ?>
                                                >
                                                    <option selected value="<?=$request->operator_id?>"><?=$request->getOperator?></option>
                                                    <?php 
                                                    foreach ($users as $user):
                                                    ?>
                                                    <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Status:</label>
                                                <select name="status" required class="custom-select" id="inputGroupSelect01" disabled>
                                                    <option value="<?=$request->status_id?>"><?=$request->getStatus?></option>
                                                    <?php
                                                    foreach ($status as $statuss):
                                                    ?>
                                                    <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Quantidade de itens:</label>
                                                <input name="itemsQuantity" required type="number" class="form-control" id="recipient-name" value="<?= $request->items_quantity?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                                                <select class="form-select" required name="type">
                                                    <option value="<?=$request->type_id?>"><?=$request->getType?></option>
                                                    <?php foreach ($nfeType as $type): ?>
                                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Operação:</label>
                                                <select class="form-select" required name="operationId">
                                                    <option value="<?=$request->operation_id?>"><?=$request->getOperation?></option>
                                                    <?php foreach ($nfeOperation as $operation): 
                                                    if($operation->id != 12):?>
                                                    <option value="<?=$operation->id?>"><?=$operation->name?></option>
                                                    <?php endif;
                                                    endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" required class="col-form-label">Descrição:</label>
                                                <textarea name="note" class="form-control"
                                                id="message-text"><?= str_replace("<br />","",$request->note) ?></textarea>
                                            </div>
                                            <?php $i=1;?>
                                            <?php 
                                            foreach ($request->getDueDate as $dueDate):
                                            ?>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Vencimento <?=$i?>:</label>
                                                <input name="dueDate<?=$i?>" type="text" class="form-control" id="recipient-name"
                                                value="<?=$dueDate->due_date?>">
                                            </div>
                                            <?php
                                            $i++;
                                            endforeach;?>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-primary">Confirmar</button>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Desbloqueio NFe</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                    <?php if ($_COOKIE['id'] == 150 || $_COOKIE['id'] == 152 || $_COOKIE['id'] == 175): ?>
                                        <form name="desbloqueio">
                                            <input type="hidden" name="id" value="<?=$request->id?>">
                                            <div class="mb-3">
                                                <label for="message-text" class="col-form-label">Message:</label>
                                                <textarea required class="form-control" id="message-text"></textarea>
                                            </div>
                                            <?php endif; ?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary">Desbloquear</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade " id="reporte<?=$request->id?>" tabindex="-1"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Reportar NF
                                            <?=$request->nf_number?></h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form name="reporte<?=$request->id?>">
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Categoria:</label>
                                                <select class="form-control" name="type">
                                                    <option value="1">Erro lançamento</option>
                                                    <option value="2">Vencimento</option>
                                                    <option value="3">Código interno</option>
                                                    <option value="4">Não Atualizada</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Menssagem:</label>
                                                <textarea name="note" class="form-control" id="message-text"></textarea>
                                                <input type="hidden" name="idRequest" value="<?=$request->id?>">
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary">Confirmar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                                                <script>
                            $('form[name="desbloqueio"]').submit(function (event) {
                                event.preventDefault();

                                $.ajax({
                                    url: '/desbloqueio',
                                    type: 'post',
                                    data: $(this).serialize(),
                                    dataType: 'json',
                                    success: function (response) {

                                        if (response.sucesso === true) {
                                            swal({
                                                title: "Bispo & Dantas",
                                                text: response.msg,
                                                timer: 20000,
                                                icon: "success",
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                type: "success"
                                            });
                                            document.location.reload(true);
                                        } else {
                                            swal({
                                                title: "Bispo & Dantas",
                                                text: response.msg,
                                                icon: "error",
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                type: "danger"
                                            });
                                        }

                                    }
                                })
                            });
                        </script>
                        <script>
                        $('form[name="editar<?=$request->id?>"]').submit(function (event) {
                            event.preventDefault();

                            $.ajax({
                                url: '/request/editRequest',
                                type: 'post',
                                data: $(this).serialize(),
                                dataType: 'json',
                                success: function (response) {

                                    if (response.success === true) {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            timer: 20000,
                                            icon: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "success"
                                        });
                                        document.location.reload(true);
                                    } else {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            icon: "error",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "danger"
                                        });
                                    }

                                }
                            })
                        });
                            $('form[name="reporte<?=$request->id?>"]').submit(function (event) {
                                event.preventDefault();

                                $.ajax({
                                    url: '/request/nfReport/add',
                                    type: 'post',
                                    data: $(this).serialize(),
                                    dataType: 'json',
                                    success: function (response) {

                                        if (response.sucesso === true) {
                                            swal({
                                                title: "Bispo & Dantas",
                                                text: response.msg,
                                                timer: 20000,
                                                icon: "success",
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                type: "success"
                                            });
                                            document.location.reload(true);
                                        } else {
                                            swal({
                                                title: "Bispo & Dantas",
                                                text: response.msg,
                                                icon: "error",
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                type: "danger"
                                            });
                                        }

                                    }
                                })
                            });
                        </script>
                        <div class="col">
                        <button type="button" data-bs-placement="top" title="Log" class="btn btn-secondary"
                                data-bs-toggle="modal" data-bs-target="#log<?= $request->id?>" data-whatever="@mdo"><i
                                    class="ion ion-ios-time-outline"></i></button>
                        </div>
                        <div class="modal fade" id="log<?= $request->id?>" tabindex="-1"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xl">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Log de alteração Chamado
                                            <?=$request->id?></h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="row align-items-start" style="background-color:#adb5bd">
                                                <div class="col">
                                                    Usúario
                                                </div>
                                                <div class="col">
                                                    Alteração
                                                </div>
                                                <div class="col">
                                                    Data e Hora
                                                </div>
                                            </div>
                                            <?php if (is_array($request->getLogs())):
                                        foreach ($request->getLogs() as $log):?>
                                            <div class="row align-items-start">
                                                <div class="col">
                                                    <?=$log->getUser()->getLogin;?>
                                                </div>
                                                <div class="col">
                                                    <?=$log->description?>
                                                </div>
                                                <div class="col">
                                                    <?=$log->getCreatedAt?>
                                                </div>
                                            </div>
                                            <?php endforeach;
                                    endif;?>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Fechar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                                </div>
                        <div class="col">
                            <button class="btn btn-success" type="button"><a style="text-decoration: none"
                                    href="https://wa.me/55<?=$request->getClientClass()->tell?>?text= Operador: <?= $request->getOperator?>, NF: <?= $request->nf_number?>, Loja:  <?= $request->store_id?>, Grupo: <?=$request->clientGroup?>  http://lancamentosnfe.bdtecsolucoes.com.br:8090/request/interacao/<?=$request->id?>"><i
                                        class="ion ion-social-whatsapp"></i></a></button>
                        </div>
                        <div class="col">
                            <form name="reprocessar<?=$request->id?>">
                                <input type="hidden" name="key" value="<?=$request->access_key?>">
                                <input type="hidden" name="store" value="<?=$request->store_id?>">
                                <a>
                                <button type="submit" class="btn btn-outline-info">Troca Codigo<i
                                            class="ion ion-android-download"></i></button></a>
                            </form>
                            <script>
                                $('form[name="reprocessar<?=$request->id?>"]').submit(function (event) {
                                    event.preventDefault();
                                    $.ajax({
                                        url: '/request/trocaCodigo/nfe',
                                        type: 'post',
                                        data: $(this).serialize(),
                                        dataType: 'json',
                                        success: function (response) {
                                            if (response.success === true) {
                                                swal({
                                                    title: "Bispo & Dantas",
                                                    text: response.message,
                                                    timer: 20000,
                                                    icon: "success",
                                                    showCancelButton: false,
                                                    showConfirmButton: false,
                                                    type: "success"
                                                });
                                            } else {
                                                swal({
                                                    title: "Bispo & Dantas",
                                                    text: response.message,
                                                    icon: "error",
                                                    showCancelButton: false,
                                                    showConfirmButton: false,
                                                    type: "danger"
                                                });
                                            }
                                        }
                                    })
                                });
                            </script>
                        </div><button class="btn btn-outline-danger"><a target="blank" href="/request/verification/transfer/<?=$request->nfSefaz()->id?>">Enviar XML para pasta</a></button>
                        <button type="button" style="margin-left:2%"  data-bs-placement="top" title="Visualizar" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#visualizar<?=$request->id?>"
                                    data-whatever="@mdo"><i class="ion ion-eye"></i></button>
                                    <div class="modal fade" id="visualizar<?=$request->id?>" tabindex="-1"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $request->nf_number?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $request->access_key?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Observações:</label>
                                            <textarea name="description" disabled class="form-control"
                                                id="message-text"><?= $request->note ?></textarea>
                                        </div>
                                        <?php $i = 1;
                                        if ($request->getDueDate):
                                         foreach ($request->getDueDate as $dueDate): ?>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Vencimento <?=$i?>:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $dueDate->due_date?>">
                                        </div>
                                        
                                        <?php endforeach;
                                        endif; ?>
                                    <?=__DIR__?>
                                        <?php if (file_exists(__DIR__."/../../source/XML/$request->store_id/NFe$request->access_key.xml")): ?>
                                        <div class="form-group">
                                        <div class="row">
                                        <a download style="text-decoration:none;margin-bottom:5%" href="<?="/../../source/XML/$request->store_id/NFe$request->access_key.xml"?>"><button class="btn btn-outline-success">XML <i class="ion ion-android-download"></i></button></a>
                                            <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                                <input type="hidden" name="key" value="<?=$request->access_key?>">
                                                <input type="hidden" name="store" value="<?=$request->store_id?>">
                                                <a>
                                                <button type="submit" class="btn btn-outline-warning">PDF <i class="ion ion-android-download"></i></button></a>
                                            </form>
                                        </div>
                                        </div>
                                        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
            data-bs-toggle="collapse" data-bs-target="#multiCollapseExample<?=$request->id?>" aria-expanded="false"
            aria-controls="multiCollapseExample1">Itens
            ↴</button>

    </div>
    <div class="collapse multi-collapse container" id="multiCollapseExample<?=$request->id?>">    
                                        <div class="form-group">
                                        <div class="container">
                                                    <?php foreach ($request->getItems as $det): ?>
                                            <div class="row border">
                                                <div class="col border">
                                                    <?= $det->prod->cProd?>
                                                </div>
                                                <div class="col border">
                                                    <?= $det->prod->cEAN?>
                                                </div>
                                                <div class="col border">
                                                    <?= $det->prod->xProd?>
                                                </div>
                                                <div class="col border">
                                                    <form name="update<?=$request->id?>">
                                                        <input type="hidden" name="id" value="<?=$request->id?>">
                                                        <input type="hidden" name="referencia" value="<?=$det->prod->cProd?>">
                                                        <input type="hidden" name="ean" value="<?=$det->prod->cEAN?>">
                                                        <input type="hidden" name="descricao" value="<?=$det->prod->xProd?>">
                                                        
                                                        <button id="tra<?=$request->id?>" class="btn btn-danger" <?php if($request->getItemCadastro($det->prod->cProd)): ?> disabled <?php endif?> type="submit" >!Cadastro</button>
                                                    </form>
                                                    <script>

                                                        $('form[name="update<?=$request->id?>"]').submit(function (event) {
                                                            event.preventDefault();
                                                            var send = $("#tra<?=$request->id?>");
                                                            send.attr('disabled', 'disabled');
                                                            $.ajax({
                                                                url: '/registrarCadastro',
                                                                type: 'post',
                                                                data: $(this).serialize(),
                                                                dataType: 'json',
                                                                success: function (response) {

                                                                    if (response.success === true) {
                                                                        swal({
                                                                            title: "Bispo & Dantas",
                                                                            text: response.message,
                                                                            timer: 600,
                                                                            icon: "success",
                                                                            showCancelButton: false,
                                                                            showConfirmButton: false,
                                                                            type: "success"
                                                                        });
                                                                        document.location.reload(true);
                                                                    } else {
                                                                        swal({
                                                                            title: "Bispo & Dantas",
                                                                            text: response.message,
                                                                            icon: "error",
                                                                            showCancelButton: false,
                                                                            showConfirmButton: false,
                                                                            type: "danger"
                                                                        });
                                                                    }

                                                                }
                                                            })
                                                        }); 
                                                    </script>                                                   
                                                </div>
                                            </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <?php endforeach; ?>
                                        </div>
                                        </div>
                                        
                                        <?php endif; ?>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Voltar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="col">
                            <?php if ($_COOKIE['id'] == 150): ?>
                            <form name="excluir<?=$request->id?>">
                                <input type="hidden" name="id" value="<?=$request->id?>">
                                <button type="submit" data-bs-placement="top" title="Excluir"
                                    class="btn btn-outline-danger">Excluir</button>
                            </form>
                            
                            <script>
                                $('form[name="excluir<?=$request->id?>"]').submit(function (event) {
                                    event.preventDefault();
                                    var x;
                                    var r = confirm(
                                        "Excluir o lançamento ?"
                                    );
                                    if (r == true) {
                                        $.ajax({
                                            url: '/request/deleteRequest',
                                            type: 'post',
                                            data: $(this).serialize(),
                                            dataType: 'json',
                                            success: function (response) {

                                                if (response.success === true) {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        timer: 20000,
                                                        icon: "success",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "success"
                                                    });

                                                } else {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        icon: "error",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "danger"
                                                    });
                                                }

                                            }
                                        })
                                    }
                                });
                            </script>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                
                <?php 

                if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 150): ?>
                    <div class="row" style="margin-top: 5%;">
                        <div class="col">
                            <a class="btn btn-danger container" target="_blank" href="<?=URL_BASE?>/alteraEstoque?id=<?=$request->id?>">1º Passo</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-danger container" target="_blank" href="<?=URL_BASE?>/alteraEstoque2?id=<?=$request->id?>">2º Passo (Item Incorreto)</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-danger container" target="_blank" href="<?=URL_BASE?>/alteraEstoque3?id=<?=$request->id?>">2º Passo (Quantidade Incorreta)</a>
                        </div>
                    </div>
                        <?php endif; ?>
                    <div class="row" style="margin-top: 5%;">
                        <div class="col">
                            <!-- <a class="btn btn-danger container" target="_blank" href="<?=URL_BASE?>/">1º Passo</a> -->
                        </div>
                    </div>    
            </div>
            <div class="card">
                <div class="card-header">
                Codigos Internos
                </div>
                <?php foreach ($request->getCodes() as $code): ?>
                <div style="margin: 1%">
                <form name="updatecode<?= $code->id ?>">
                    <div class="row">
                        <div class="col">
                            <input class="form-control" name="code" value="<?=$code->code?>">
                            <input type="hidden" name="id" value="<?=$code->id?>">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-success">✓</button>
                        </div>
                    </div>
                </form>
                <script>
                $('form[name="updatecode<?= $code->id ?>"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/updatecode',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 20000,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }
                        }
                    })
                });
                </script>

                </div>
                <?php endforeach; ?>    
            </div>
            <div class="row row-cols-2" style="padding-top:2%;padding-bottom:2%;width:30%">
                <?php foreach ($request->getDueDate as $dueDate): ?>
                <div class="col">
                    <h6>Vencimento : </h6>
                </div>
                <div class="col">
                    <h6><?= $dueDate->due_date?>
                </div>
                </h6>
                <?php endforeach; ?>

            </div>

        </div>

        <div style="margin:1%">
            <h5>Observações: </h5>
            <p><?=$request->note?></p>
            <?php
            

            if (is_array($request->getAttachment)):
            foreach($request->getAttachment as $file):?>
            <a download href="<?=URL_BASE.'/'.$file->file_dir?>"
                class="card-link"><?=$file->getName."📎"?></a>

            <?php endforeach;
            endif;
                ?>
        </div>
    </div>
    <div style="margin-left:2%;margin-right:2%;">
    <?php if ($_COOKIE['id'] == $request->operator_id):?>
        <button style="margin-top:2%" type="button" class="btn btn-secondary btn-lg" data-bs-toggle="modal"
            data-bs-target="#adicionar">Adicionar
            +</button>
        <?php
        endif;
        if ($interactions):
            ?>
        <?php
        else:
            ?>
        <h5 style="margin-left:43%;margin-bottom:5%">Sem Interações.</h5>
        <?php
        endif;
        ?>

        <?php 
        if(is_array($interactions)):
        foreach ($interactions as $interaction):
            ?>
        <div class="card" style="margin-left:2%;margin-right:2%;margin-bottom:2%">
            <div class="card-header">
                <?=$interaction->user()->getLogin." <a style='font-size:70%;color:#5199FF'>".$interaction->getCreatedAt?></a>
            </div>
            <div class="card-body">
                <h5 class="card-title"></h5>
                <p class="card-text"><?=nl2br($interaction->description)?></p>
            </div>
            <?php 
            if (is_array($interaction->imageAttachment)):
                foreach ($interaction->imageAttachment as $attachment):
                    
            ?>
            <img style="width:350px;margin-bottom:1%;"
                src="<?=URL_BASE.'/'.$attachment->name?>" class="card-img-top"
                alt="...">
            <?php
                endforeach;
                endif;
            ?>
            <div class="card-body">
                <?php 
                if (is_array($interaction->imageAttachment)):
                foreach ($interaction->imageAttachment as $attachment):
                ?>
                <a download href="<?="<?=URL_BASE?>/$attachment->name"?>"
                    class="card-link">Imagem📎</a>
                <?php
                endforeach;
                endif;
                if (is_array($interaction->fileAttachment)):
                foreach ($interaction->fileAttachment as $file):
                ?>
                <a download href="<?=URL_BASE.'/'.$file->name?>"
                    class="card-link"><?=$file->getName."📎"?></a>
                <?php
                endforeach;
            endif;
            if (is_array($interaction->audioAttachment)):
                foreach ($interaction->audioAttachment as $audio):
                    ?>
                <audio controls>
                    <source src="<?="<?=URL_BASE?>/$audio->name"?>" type="audio/ogg">
                    Your browser does not support the audio element.
                </audio>
                <?php
                endforeach;
            endif;
            ?>
            </div>
        </div>

        <?php 
        endforeach;
    endif;
        ?>
    </div>
</div>
</div>