<?php 
$v->layout("administrator/adm_theme");

?>
<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 60000);
</script>
<main class="main_content">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="/home2">Acompanhamento diario (lançamentos
                    diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  active" aria-current="page" href="/home2/1">Acompanhamento diario (lançamentos com
                    confêrencia)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/2">Duração media status diario (lançamentos
                    diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/3">Duração media status diario (lançamentos com
                    conferência)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/4">Situações Pontuais (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/5">Acompanhamento Reportados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/6">Acompanhamento Pendente Empresa (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/7">Acompanhamento Chegada Carga Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/8">Acompanhamento Atualizadas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/9">Atualizadas por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/10">Pendente cliente por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/11">Atualizadas por Operador Sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/12">Atendimento Solicitações de lançamento Por Loja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/13">Atualizações por hora / usúario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/14">Incluidas e Concluidas por hora</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/15">Acompanhamento Pendente Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/16">Acompanhamento Pendente Cliente Dias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/17">Acompanhamento Pendência Comercial Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/18">Acompanhamento Conferência Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/19">Acompanhamento NFe a Atualizar Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/20">Alocadas por Usuario sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/21">Pendentes Prioridades Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/22">Pendentes Usuário Media</a>
            </li>
            <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 180 or $_COOKIE['id'] == 20000151): ?>
            <li class="nav-item">
                <a class="nav-link" href="/home2/26">Notas Atualizadas Operador Hora</a>
            </li>
            <?php endif; ?>
        </ul>
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-3">
                    <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento diario (lançamentos com
                        confêrencia)</h1>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="margin-bottom:5%">
            <div class="row">
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3><?=$cargoArrival+0?></h3>
                            <p>Chegada Carga</p>
                            <p><?=$cargoArrivalA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-add"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/10"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-add"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$commercialPending+0?><sup style="font-size: 20px"></sup></h3>
                            <p>Pendente Comercial</p>
                            <p><?=$commercialPendingA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/11"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-checkmark"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$conference+0?></h3>
                            <p style="font-size:95%">Conferência</p>
                            <p><?=$conferenceA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-alert"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/12"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-alert"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?=$nfeUpdate+0?></h3>
                            <p>NFe a Atualizar</p>
                            <p><?=$nfeUpdateA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-refresh"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/13"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-refresh"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6">
                    <div class="small-box bg-indigo">
                        <div class="inner">
                            <h3><?=$price+0?></h3>
                            <p>A precificar</p>
                            <p><?=$priceA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-add"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/15"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-add"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3><?=$finishedCargo+0?></h3>
                            <p>Carga Finalizada</p>
                            <p><?=$finishedCargoA+0?> Itens</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-checkmark"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/dashboard/14"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-person"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>