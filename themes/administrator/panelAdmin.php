<?php 
$v->layout("administrator/adm_theme");
?>
<!-- <script>
  setTimeout(function () {
  window.location.reload(1);
}, 60000);
</script> -->
<main class="main_content">
  <form action="/dashboard" method="post">
    <div class="input-group input-group-sm container" style="width:500px;margin-top:2%">
      <span class="input-group-text">Período</span>
      <input type="date" name="date1" aria-label="First name" class="form-control">
      <input type="date" name="date2" aria-label="Last name" class="form-control">
      <button type="submit" class="input-group-text btn btn-outline-success"><i
          class="ion ion-android-refresh"></i></button>
    </div>
  </form>
  <div class="container">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-3">
          <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento lançamentos</h1>
          <div class="col-sm-6">
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid" style="margin-bottom:5%">
      <div class="row">
        <div class="col-lg-4 col-6">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=$included?></h3>
              <p>Incluidos</p>
              <p><?=$includedA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-add"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/i" class="small-box-footer">Mais
              Informações <i class="ion ion-android-add"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?=$completed?><sup style="font-size: 20px"></sup></h3>
              <p>Concluídos</p>
              <p><?=$completedA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-checkmark"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/3" class="small-box-footer">Mais
              Informações <i class="ion ion-checkmark"></i></a>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
    <div class="container">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-3">
                <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Media de Tempo Status</h1>
                <div class="col-sm-6">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3><?=$nfEvent->getAverageDurationPeriod(1,$initial,$final)?><sup style="font-size: 20px"></sup></h3>
                    <p>Pendente Empresa</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDurationPeriod(1,$initial,$final)?>"
                    class="small-box-footer">Mais
                    Informações <i class="ion ion-checkmark"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3><?=$nfEvent->getAverageDurationPeriod(2,$initial,$final)?><sup style="font-size: 20px"></sup></h3>
                    <p>Pendente Cliente</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDurationPeriod(2,$initial,$final)?>"
                    class="small-box-footer">Mais
                    Informações <i class="ion ion-checkmark"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-indigo">
                <div class="inner">
                    <h3><?=$nfEvent->getAverageDurationPeriod(4,$initial,$final)?><sup style="font-size: 20px"></sup></h3>
                    <p>Pendente Informações</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDurationPeriod(4,$initial,$final)?>"
                    class="small-box-footer">Mais
                    Informações <i class="ion ion-checkmark"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-black">
                <div class="inner">
                    <h3><?=$nfEvent->getAverageDurationPeriod(9,$initial,$final)?><sup style="font-size: 20px"></sup></h3>
                    <p>Cadastro</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDurationPeriod(9,$initial,$final)?>"
                    class="small-box-footer">Mais
                    Informações <i class="ion ion-checkmark"></i></a>
            </div>
        </div>

        <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?=$nfEvent->getAverageDurationPeriod(5,$initial,$final)?><sup style="font-size: 20px"></sup></h3>
                    <p>Pendente ICMS</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$nfEvent->getIdsDurationPeriod(6,$initial,$final)?>"
                    class="small-box-footer">Mais
                    Informações <i class="ion ion-checkmark"></i></a>
            </div>
        </div>
    </div>
    <div class="content-header">
    <div class="container-fluid">
        <div class="row mb-3">
            <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Situações Pontuais</h1>
            <div class="col-sm-6">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-bottom:5%">
    <div class="row">
        <div class="col-lg-2 col-6">
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3><?=$pendenteCliente?></h3>
                    <p>Pendente Cliente</p>
                    <!-- <p><?=$includedA+0?> Itens</p> -->
                </div>
                <div class="icon">
                    <i class="ion ion-android-add"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$pendenteClientf?>" class="small-box-footer">Mais
                    Informações <i class="ion ion-android-add"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box bg-indigo">
                <div class="inner">
                    <h3><?=$reabertura?></h3>

                    <p>Reabertura</p>
                    <!-- <p><?=$attendanceA+0?> Itens</p> -->
                </div>
                <div class="icon">
                    <i class="ion ion-ios-play"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$reaberturaf?>" class="small-box-footer">Mais
                    Informações <i class="ion ion-ios-play"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?=$pendenteICMS?></h3>

                    <p>Pendente ICMS</p>
                    <p>Usuários: 
                    <?php foreach ($pendenteICMSUser as $piu):
                    ?>
                    <p><?=$user->getName($piu)." - ".$piuc1[$piu]?></p>
                    <?php endforeach; 
                    ?>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-play"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$pendenteICMSId?>" class="small-box-footer">Mais
                    Informações <i class="ion ion-ios-play"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box bg-dark">
                <div class="inner">
                    <h3><?=$cadastro?></h3>

                    <p>Cadastro</p>
                    <p>Usuários: 
                    <?php foreach ($cadastroUser as $piu):
                    ?>
                    <p><?=$user->getName($piu)." - ".$cuc[$piu]." tickets"?></p>
                    <?php endforeach; 
                    ?>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-play"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$cadastroIds?>" class="small-box-footer">Mais
                    Informações <i class="ion ion-ios-play"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box bg-secondary">
                <div class="inner">
                    <h3><?=$pendenteInformacoes?></h3>

                    <p>Pendente Informações</p>
                    <?php foreach ($pendenteInformacoesUser as $piu):
                    ?>
                    <p></p>
                    <?php endforeach; 
                    ?>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-play"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$pendenteInformacoesId?>" class="small-box-footer">Mais
                    Informações <i class="ion ion-ios-play"></i></a>
            </div>
        </div>
    </div>
</div><!-- /.container-fluid -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-3">
          <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Atualizadas</h1>
          <div class="col-sm-6">
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid" style="margin-bottom:5%">
      <div class="row">
        <?php foreach($operations as $operation):
          ?>
        <div class="col-lg-2 col-6">
          <div class="small-box" style="background:<?=$operation->color?>">
            <div class="inner">
              <h3><?=$nfAtualizada[$operation->id]?></h3>
              <p style="font-size:70%"><?=$operation->name?></p>
              <p><?=$nfAtualizadaA[$operation->id]+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-checkmark"></i>
            </div>
            <a href="<?=URL_BASE?>/request/atualizada/<?=$operation->id?>"
              class="small-box-footer">Mais Informações <i class="ion ion-checkmark"></i></a>
          </div>
        </div>
        <?php endforeach;
          ?>
      </div>
    </div>


    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-3">
          <h1 class="m-0 text-dark" style="text-align:center">Atualizadas por Operador</h1>
          <div class="col-sm-6">
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid" style="margin-bottom:5%">
      <div class="row">
        <?php foreach($userAtualizadas as $userAtualizadas):
          ?>
        <div class="col-lg-2 col-6">
          <div class="small-box">
            <div class="inner">
              <h3><?=$userRequestAtualizada[$userAtualizadas->getOperator]?></h3>
              <p style="font-size:70%"><?=$userAtualizadas->getOperator?></p>
              <p style="font-size:70%"><?=$userRquestItemsAmount[$userAtualizadas->getOperator]?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-checkmark"></i>
            </div>
            <a href="<?=URL_BASE?>/request/atualizada/<?=$userAtualizadas->operator_id?>/1"
              class="small-box-footer">Mais Informações <i class="ion ion-checkmark"></i></a>
          </div>
        </div>
        <?php endforeach;
          ?>
      </div>
    </div>
    

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-3">
          <h1 class="m-0 text-dark" style="text-align:center">Atualizadas por Operador Sem CD</h1>
          <div class="col-sm-6">
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid" style="margin-bottom:5%">
      <div class="row">
        <?php foreach($userAtualizadasScd as $userAtualizadaScd):
          ?>
        <div class="col-lg-2 col-6">
          <div class="small-box">
            <div class="inner">
              <h3><?=$userRequestAtualizadaScd[$userAtualizadaScd->getOperator]?></h3>
              <p style="font-size:70%"><?=$userAtualizadaScd->getOperator?></p>
              <p style="font-size:70%"><?=$userRquestItemsAmountScd[$userAtualizadaScd->getOperator]?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-checkmark"></i>
            </div>
            <a href="<?=URL_BASE?>/request/atualizada/<?=$userAtualizadaScd->operator_id?>/2"
              class="small-box-footer">Mais Informações <i class="ion ion-checkmark"></i></a>
          </div>
        </div>
        <?php endforeach;
          ?>
      </div>
    </div>
    <section class="col-lg-12 connectedSortable">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            <i class="fas fa-chart-pie mr-1" style="text-align:center"></i>
            Atendimento Chamados Por Loja
          </h3>
          <div class="card-tools">
            <ul class="nav nav-pills ml-auto">
              <li class="nav-item">
              </li>
            </ul>
          </div>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <?php
                    foreach ($stores as $store):
                    ?>
            <div class="col-lg-2 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h4>Loja <?=$store->store_id?></h4>
                  <p><?=$storeIncluded[$store->store_id]?> Chamado(os) Aberto(os)</p>
                  <p><?=$storeIncludedA[$store->store_id]+0?> Itens</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="<?=URL_BASE?>/request/storeStatus/<?=$store->store_id?>/1"
                  class="small-box-footer">Info. <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-2 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h4>Loja <?=$store->store_id?></h4>
                  <p><?=$storeCompleted[$store->store_id]?> Chamado(os) Concluido(os)</p>
                  <p><?=$storeCompletedA[$store->store_id]+0?> Itens</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="<?=URL_BASE?>/request/storeStatus/<?=$store->store_id?>/3"
                  class="small-box-footer">Info. <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <?php
                    endforeach;
                    ?>
    </section>

    <script>
      $(function () {
        'use strict'

        var ticksStyle = {
          fontColor: '#495057',
          fontStyle: 'bold'
        }

        var mode = 'index'
        var intersect = true

        var $visitorsChart = $('#visitors-chart')
        var visitorsChart = new Chart($visitorsChart, {
          data: {
            labels: [ <?=$hour?>],
            datasets: [<?php foreach($l as $ls) :?>{
              type: 'line',
              data: [<?php echo substr($operatorCounts[$ls], 1)?>],
              backgroundColor: 'transparent',
              borderColor: '<?=$c[$ls]?>',
              pointBorderColor: '<?=$c[$ls]?>',
              pointBackgroundColor: '<?=$c[$ls]?>',
              pointRadius: '5',
              pointHoverRadius: '5',

              fill: false
              // pointHoverBackgroundColor: '#007bff',
              // pointHoverBorderColor    : '#007bff'
            },<?php endforeach;?>]
          },
          options: {
            maintainAspectRatio: false,
            tooltips: {
              mode: mode,
              intersect: intersect
            },
            hover: {
              mode: mode,
              intersect: intersect
            },
            legend: {
              display: false
            },
            scales: {
              yAxes: [{
                // display: false,
                gridLines: {
                  display: true,
                  lineWidth: '4px',
                  color: 'rgba(0, 0, 0, .2)',
                  zeroLineColor: 'transparent'
                },
                ticks: $.extend({
                  beginAtZero: true,
                  suggestedMax: 10
                }, ticksStyle)
              }],
              xAxes: [{
                display: true,
                gridLines: {
                  display: true
                },
                ticks: ticksStyle
              }]
            }
          }
        })
      })
    </script>
    <section class="col-lg-12 connectedSortable ui-sortable">
      <div class="card">
        <div class="card-header border-0">
          <div class="d-flex justify-content-between">
            <h3 class="card-title">Atualizações por hora / usúario</h3>
            <!-- <a href="javascript:void(0);">View Report</a> -->
          </div>
        </div>
        <div class="card-body">
          <div class="d-flex">
            <!-- <p class="d-flex flex-column">
                    <span class="text-bold text-lg">820</span>
                    <span>Visitors Over Time</span>
                  </p> -->
            <!-- <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-success">
                      <i class="fas fa-arrow-up"></i> 12.5%
                    </span>
                    <span class="text-muted">Since last week</span>
                  </p> -->
          </div>
          <!-- /.d-flex -->

          <div class="position-relative mb-4">
            <canvas id="visitors-chart" height="300"></canvas>
          </div>

          <div class="d-flex flex-row justify-content-end">
            <?php foreach ($l as $lf):
                ?>
            <span class="mr-2" style="font-size:80%">
              <i style="background-color:<?=$c[$lf]?>" class="fas fa-square"></i> <?=$lf?>
            </span>
            <?php endforeach;
                ?>

          </div>
        </div>
      </div>
    </section>
    