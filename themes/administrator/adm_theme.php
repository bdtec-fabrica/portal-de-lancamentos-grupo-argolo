<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $title; ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= url("/themes/css/all.min.css");?>">
  <link rel="stylesheet" href="<?= url("/themes/css/nav.css");?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= url("/themes/css/OverlayScrollbars.min.css");?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= url("/themes/css/adminlte.min.css");?>">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= url("/themes/css/bootstrap.min.css");?>">
  <script src="<?= url("/themes/js/jquery-3.5.1.min.js");?>"></script>
  <script src="<?= url("/themes/js/jquery-3.6.0.js");?>"></script>
  <script src="<?= url("/themes/js/popper.min.js");?>"></script>
  <script src="<?= url("/themes/js/bootstrap.min.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.min.js");?>"></script>
  <script src="<?= url("/themes/js/jquery-ui.min.js");?>"></script>
  <script src="<?= url("/themes/js/bootstrap.bundle.min.js");?>"></script>
  <script src="<?= url("/themes/js/Chart.min.js");?>"></script>
  <script src="<?= url("/themes/js/sparkline.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.vmap.min.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.vmap.usa.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.knob.min.js");?>"></script>
  <script src="<?= url("/themes/js/moment.min.js");?>"></script>
  <script src="<?= url("/themes/js/daterangepicker.js");?>"></script>
  <script src="<?= url("/themes/js/tempusdominus-bootstrap-4.min.js");?>"></script>
  <script src="<?= url("/themes/js/summernote-bs4.min.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.overlayScrollbars.min.js");?>"></script>
  <script src="<?= url("/themes/js/adminlte.js");?>"></script>
  <script src="<?= url("/themes/js/dashboard.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.form.min.js");?>"></script>
  <script src="<?= url("/themes/js/sweetalert.min.js");?>"></script>
  <script src="<?= url("/themes/js/nav.js");?>"></script>
  <script src="<?= url("/themes/js/sortable.js");?>"></script>
  <script src="<?= url("/themes/js/demo.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.flot.resize.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.flot.pie.js");?>"></script>
  <script src="https://requirejs.org/docs/release/2.3.5/minified/require.js"></script>
 
  <style>
    ::-webkit-scrollbar {
      width: 5px;
      height: 5px;
    }

    /* aqui é para personalizar o fundo da barra, neste caso estou colocando um fundo cinza escuro*/
    ::-webkit-scrollbar-track {
      background: #333;
    }

    /* aqui é a alça da barra, que demonstra a altura que você está na página
    estou colocando uma cor azul clara nela*/
    ::-webkit-scrollbar-thumb {
      background: #939598;
    }
  </style>
</head>

<body class="sidebar-mini layout-fixed sidebar-collapse">
  <!-- Site wrapper -->
  <div class="wrapper" >
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-dark navbar-indigo">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button">≡</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="/" class="nav-link">Inicio</a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
          <a href="/logout" class="nav-link">Sair</a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->

    <aside class="main-sidebar sidebar-dark-primary elevation-4" style="overflow:auto">
      <!-- Brand Logo -->

      <a href="/" class="brand-link" style="text-decoration:none">
        <img src="<?= url("/themes/images/bd.jpeg");?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
          style="opacity: .8">
        <span class="brand-text font-weight-light">BDTec Lançamentos</span>
      </a>
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li class="nav-header">Ferramentas do Administrador</li>

              <li class="nav-item">
                <a href="/usuarios" class="nav-link">
                  <i class="ion ion-ios-person-outline"></i>
                  <p>
                    Usuários
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lojas" class="nav-link">
                  <i class="ion ion-bag"></i>
                  <p>
                    Lojas
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/lojas/contacts" class="nav-link">
                  <i class="ion ion-bag"></i>
                  <p>
                    Contatos Lojas
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/exchangeCode" class="nav-link">
                  <i class="ion ion-ios-settings-strong"></i>
                  <p>
                    Troca Codigo
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/packagingCode" class="nav-link">
                  <i class="ion ion-ios-settings-strong"></i>
                  <p>
                    Troca Embalagem
                  </p>
                </a>
              </li>
              <li class="nav-header">Lançamentos de Notas</li>

              <li class="nav-item">
                <a href="/request" class="nav-link">
                  <i class="ion ion-document"></i>
                  <p>
                    Lançamentos
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/request/verification" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Auditoria de postagem

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/iniciadas" class="nav-link">
                  <i class="ion ion-ios-navigate"></i>
                  <p>
                    NFe Iniciadas

                  </p>
                </a>
              </li>
              <li class="nav-header">Consultas Notas Fiscais</li>

              <li class="nav-item">
                <a href="/nfSefaz" class="nav-link">
                  <i class="ion ion-android-list"></i>
                  <p>
                    Painel NFe

                  </p>
                </a>
              </li>

              <li class="nav-header">Auditorias</li>

              <li class="nav-item">
                <a href="/auditoria-margem" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Auditoria de Margens

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/margin/validate" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Aprovação de Margens

                  </p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="/auditoria-margem2" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Audit margens Vendas

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/auditoria-vencimento" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Auditoria de Vencimento

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/auditoria-lancamento" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Auditoria de Lançamentos

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/dashboard/" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Dashboards

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/desossa" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Desossa

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/apuracao" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Apuração

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/movimentacao" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Itens sem Giro

                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/apuracao-fiscal" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Apuração Fiscal
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/margin/notificationsAudit" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Painel de Notificações
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/margin/webservice35" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Consulta Cadastro
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/fiscal" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Processos Fiscais
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/margin/webservice38" class="nav-link">
                  <i class="ion ion-ios-compose"></i>
                  <p>
                    Variação de Custo
                  </p>
                </a>
              </li>
              
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content">

        <main class="main_content">
          <?=$v->section("content"); ?>
        </main>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <!-- <b>Version</b> 3.1.0 -->
      </div>
      <!-- <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved. -->
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->
</body>

</html>