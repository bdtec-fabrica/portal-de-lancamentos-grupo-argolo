<?php 
$v->layout("administrator/adm_theme");

?>
<script>
  setTimeout(function () {
    window.location.reload(1);
  }, 60000);
</script>
<main class="main_content">
  <div class="container">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link  active" aria-current="page" href="/home2">Acompanhamento diario (lançamentos diretos)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/1">Acompanhamento diario (lançamentos com
          confêrencia)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/2">Duração media status diario (lançamentos
          diretos)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/3">Duração media status diario (lançamentos com
          conferência)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/4">Situações Pontuais (lançamento direto)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/5">Acompanhamento Reportados</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/6">Acompanhamento Pendente Empresa (lançamento direto)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/7">Acompanhamento Chegada Carga Horas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/8">Acompanhamento Atualizadas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/9">Atualizadas por Operador</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/10">Pendente cliente por Operador</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/11">Atualizadas por Operador Sem CD</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/12">Atendimento Solicitações de lançamento Por Loja</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/13">Atualizações por hora / usúario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/14">Incluidas e Concluidas por hora</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/15">Acompanhamento Pendente Horas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/16">Acompanhamento Pendente Cliente Dias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/17">Acompanhamento Pendência Comercial Horas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/18">Acompanhamento Conferência Horas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/19">Acompanhamento NFe a Atualizar Horas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/home2/20">Alocadas por Usuario sem CD</a>
      </li>
      <li class="nav-item">
        <a class="nav-link"  href="/home2/21">Pendentes Prioridades Horas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" aria-current="page" href="/home2/22">Pendentes Usuário Media</a>
      </li>
      <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 180 or $_COOKIE['id'] == 20000151): ?>
      <li class="nav-item">
        <a class="nav-link" href="/home2/26">Notas Atualizadas Operador Hora</a>
      </li>
      <?php endif; ?>
    </ul>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-3">
          <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento diario (lançamentos diretos)</h1>
          <div class="col-sm-6">
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid" style="margin-bottom:5%">
      <div class="row">
        <div class="col-lg-2 col-6">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=$included+0?></h3>
              <p>Incluidos</p>
              <p><?=$includedA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-add"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/i" class="small-box-footer">Mais
              Informações <i class="ion ion-android-add"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <div class="small-box bg-secondary">
            <div class="inner">
              <h3><?=$register+0?></h3>

              <p>Cadastro</p>
              <p><?=$register+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-play"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/9" class="small-box-footer">Mais
              Informações <i class="ion ion-ios-play"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?=$completed+0?><sup style="font-size: 20px"></sup></h3>
              <p>Concluídos</p>
              <p><?=$completedA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-checkmark"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/3" class="small-box-footer">Mais
              Informações <i class="ion ion-checkmark"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <div class="small-box bg-warning">
            <div class="inner">
              <h3><?=$pendingCompany+0?></h3>
              <p style="font-size:95%">Pendente Empresa</p>
              <p><?=$pendingCompanyA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-alert"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/1" class="small-box-footer">Mais
              Informações <i class="ion ion-alert"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <div class="small-box">
            <div class="inner">
              <h3><?=$waitingRelease+0?></h3>
              <p style="font-size:95%">Aguardando Liberação</p>
              <p><?=$waitingReleaseA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-alert"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/5" class="small-box-footer">Mais
              Informações <i class="ion ion-alert"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?=$pendingClient+0?></h3>
              <p>Pendente Cliente</p>
              <p><?=$pendingClientA+0?> Itens</p>
              <p><?=$pendingClientF+0?> Fora de Linha</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-person"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/2" class="small-box-footer">Mais
              Informações <i class="ion ion-android-person"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <div class="small-box bg-cyan">
            <div class="inner">
              <h3><?=$ICMSPending+0?></h3>

              <p style="font-size:95%">Pendente ICMS</p>
              <p><?=$ICMSPendingA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-calculator"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/6" class="small-box-footer">Mais
              Informações <i class="ion ion-alert"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <div class="small-box bg-cyan">
            <div class="inner">
              <h3><?=$tarefa+0?></h3>

              <p style="font-size:95%">Tarefa</p>
              <p><?=$tarefaA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-calculator"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/17" class="small-box-footer">Mais
              Informações <i class="ion ion-alert"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-6">
          <div class="small-box bg-warning">
            <div class="inner">
              <h3><?=$pendingFiscal+0?></h3>
              <p style="font-size:95%">Pendente Fiscal</p>
              <p><?=$pendingFiscalA+0?> Itens</p>
            </div>
            <div class="icon">
              <i class="ion ion-alert"></i>
            </div>
            <a href="<?=URL_BASE?>/request/dashboard/18" class="small-box-footer">Mais
              Informações <i class="ion ion-alert"></i></a>
          </div>
        </div>
      </div>
    </div>

  </div>