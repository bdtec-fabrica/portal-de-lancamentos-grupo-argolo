<?php 
$v->layout("operator/_theme");
?>
<div class="bg-light p-5" style="margin-bottom:5%">
    <div class="container text-center">
        <h1 class="display-4">Bloco H <?=$year?></h1>
        <p class="lead">Estoque calculado atraves do processamento das entradas e saidas.</p>
    </div>
</div>
<button style="margin:2%" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"
    data-bs-whatever="@mdo">Totais por mercadologica</button>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Totais por Tributação
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Bloco H <?=$year?></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <table class="table sortable">
        <thead>
        <tr>
        <th>Tributação</th>
        <th> Valor Total</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($taxation as $tax):
        ?>
        <tr>
        <th><?=$tax?></th>
        <?php setlocale(LC_MONETARY, 'pt_BR'); ?>
        <td><?=money_format('%.2n',str_replace("-","",$totalIcms[$tax]))?></td>
        </tr>
        <?php 
        endforeach;
        ?>
        </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="padding:2%">
            <div class="modal-header">
                <h5 class="modal-title">Totais por Mercadologica</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="post" action="/teste">
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr><th>Quantidade Total: </th><td><?=$amount?></td><th>Quantidade Total: </th><td><?=$amount?></td></tr>
                    <tr><th>Custo Total: </th><td><?=str_replace(".",",",round($cost,2))?></td><th>Custo Total: </th><td><input class="form-control" name="total" type="text"></td></tr>
                </table>
            </div>

            <table class="table sortable table-striped">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Descrição</th>
                        <th>Quantidade</th>
                        <th>Custo</th>
                        <th>Percentual</th>
                    </tr>
                </thead>
                <tbody>
                
                    <?php 
            foreach ($marketings as $marketing):
                if($marketingCode[$marketing->marketing] && $marketingAmount[$marketing->marketing]>0):
            ?>
                    <tr>
                        <td><?=$marketingCode[$marketing->marketing]?></td>
                        <td><?=$marketingTitle[$marketing->marketing]?></td>
                        <td><?=str_replace(".",",",round($marketingAmount[$marketing->marketing],3))?></td>
                        <td><?=str_replace(".",",",round($marketingCost[$marketing->marketing],2))?></td>
                        <td><input class="form-control" type="number" name="<?=$marketingCode[$marketing->marketing]?>"></td>
                        
                    </tr>
                    <?php
                endif;
            endforeach;
            ?>  
                <input type="hidden" name="store" value="<?=$store?>">
                <input type="hidden" name ="year" value="<?=$year2?>">
                <input type="hidden" name="type" value="<?=$type?>">
                
                </tbody>
            </table>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Send message</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive-lg" style="padding-left:2%;padding-right:4%">
    <table class="table dt-responsive table-light table-striped nowrap w-100 sortable">
        <thead>
            <tr>
            <?php if ($amount<0):
            ?>
                <th>Quantidade total estoque Negativo:</th>
                <th>Custo Total Estoque Negativo:</th>
            <?php else:
            ?>
                <th>Quantidade total estoque Positivo:</th>
                <th>Custo Total Estoque Positivo:</th>
            <?php endif;
            ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?=number_format($amount,3,',','.')?></td>
                <?php setlocale(LC_MONETARY, 'pt_BR'); ?>
                <td><?=money_format('%.2n',str_replace("-","",$cost))?></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="table-responsive-lg" style="padding-left:2%;padding-right:4%">

    <table class="table dt-responsive table-light table-striped nowrap w-100 sortable">
        <thead>
            <tr>
                <th>Loja</th>
                <th>Codigo</th>
                <th>Descrição</th>
                <th>Quantidade</th>
                <th>Custo</th>
                <th>Custo Total</th>
                <!-- <th>Grupo</th> -->
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach ($countings as $counting):
        ?>
            <tr>
                <td><?=$counting->store_code?></td>
                <td><?=$counting->code?></td>
                <td><?=$counting->description?></td>
                <td><?=$counting->getInventoryQuantity($type)?></td>
                <td><?=$counting->getInventoryCost($type)?></td>
                <td><?=$counting->getInventoryTotalCost($type)?></td>
                <!-- <td><?=$counting->getGroup()->description?></td> -->
            </tr>
            <?php
            endforeach;
        ?>
        </tbody>
    </table>