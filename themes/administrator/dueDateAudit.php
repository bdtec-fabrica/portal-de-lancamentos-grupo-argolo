<?php 
session_start();
$v->layout("administrator/adm_theme");

?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Auditoria de Vencimentos</h1>
        <p class="lead">Batimento dos dados de vencimento entre portal e retaguarda.</p>
    </div>
</div>
<div class="container" style="margin-bottom:5%">
    <form method="post" action="/auditoria-vencimento/search" class="row gx-3 gy-2 align-items-center"
        style="margin-bottom:2%">
        <div class="col-sm-2">
            <label for="specificSizeInputName">Loja</label>
            <input name="store" type="text" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Nota Fiscal</label>
            <input name="nota" type="text" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Data agenda inicial</label>
            <input name="initialDate" type="date" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Data agenda final</label>
            <input name="finalDate" type="date" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Diveregente</label>
            <select name="divergente" class="form-control">
                <option value="1">Sim</option>
                <option value='0'>Não</option>
            </select>
        </div>

        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th style="min-width:25px">Loja</th>
            <th style="min-width:25px">Número da NF</th>
            <th style="min-width:25px">Atualização Retaguarda</th>
            <th style="min-width:25px">Atualização Portal</th>
            <th style="min-width:25px">Vencimento 01 Portal</th>
            <th style="min-width:25px">Vencimento 02 Portal</th>
            <th style="min-width:25px">Vencimento 03 Portal</th>
            <th style="min-width:25px">Vencimento 01 Retaguarda</th>
            <th style="min-width:25px">Vencimento 02 Retaguarda</th>
            <th style="min-width:25px">Vencimento 03 Retaguarda</th>
            <th style="min-width:25px">Usuário</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if ($aa1cfisc):
                foreach($aa1cfisc as $cfisc) :
                    if ($divergente == '1'):
                        $amountFis = $cfisc->getDueDateAmount();
                        if ($cfisc->getNfReceipt()){
                            $amountPort = $cfisc->getNfReceipt()->getDueDateAmount();
                        } else {
                            $amountPort = 0;
                        }
                        if ($amountFis != $amountPort):
                        ?>
                        <tr>
                        <td><?=$cfisc->FIS_LOJ_DST?></td>
                        <td><?=$cfisc->FIS_NRO_NOTA?></td>
                        <td><?=$cfisc->FIS_DTA_AGENDA?></td>
                    <?php
                    if($cfisc->getNfReceipt()):// se estiver lançada
                        
                        $nfReceipt = $cfisc->getNfReceipt();
                        ?>
                        <td><?=substr($nfReceipt->updated_at,0,10)//nota?></td> 
                        <?php
                        if($nfReceipt->getDueDate() != null):
                            $i=0;
                            foreach($nfReceipt->getDueDate() as $vencimento):
                                $i++;
                            ?>
                                <td><?=$vencimento->due_date?></td>
                            <?php
                            endforeach;  
                            if ($i < 3):
                                for ($i=$i;$i<3;$i++):
                                    ?>
                                    <td>---</td>
                                    <?php
                                endfor;
                            endif;  
                        else:
                            ?>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <?php
                        endif;
                    else: // se não estiver 
                        ?>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        <?php
                    endif;
                    foreach($cfisc->getDueDate() as $venc) :
                        ?>
                        <td><?=$venc->FIV_DTA_VENCTO?></td>
                        <?php
                    endforeach;
                    $i = 1;
                    foreach($cfisc->getDueDate() as $venc) :
                        $i++;
                        ?>
                        <td><?=$venc->FIV_DTA_VENCTO?></td>
                        <?php
                    endforeach;
                    if ($i < 3):
                        for ($i=$i;$i<3;$i++):
                            ?>
                            <td>---</td>
                            <?php
                        endfor;
                    endif;  
                    ?>
                    
                    <td><?=$cfisc->FIS_USUARIO_INC?></td>
                    </tr>
                    <?php
                    else:
                    endif;
                else:

                    ?>
                                            <tr>
                        <td><?=$cfisc->FIS_LOJ_DST?></td>
                        <td><?=$cfisc->FIS_NRO_NOTA?></td>
                        <td><?=$cfisc->FIS_DTA_AGENDA?></td>
                    <?php
                    if($cfisc->getNfReceipt()):// se estiver lançada
                        
                        $nfReceipt = $cfisc->getNfReceipt();
                        ?>
                        <td><?=substr($nfReceipt->updated_at,0,10)//nota?></td> 
                        <?php
                        if($nfReceipt->getDueDate() != null):
                            $i=0;
                            foreach($nfReceipt->getDueDate() as $vencimento):
                                $i++;
                            ?>
                                <td><?=$vencimento->due_date?></td>
                            <?php
                            endforeach;  
                            if ($i < 3):
                                for ($i=$i;$i<3;$i++):
                                    ?>
                                    <td>---</td>
                                    <?php
                                endfor;
                            endif;  
                        else:
                            ?>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <?php
                        endif;
                    else: // se não estiver 
                        ?>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        <?php
                    endif;
                    $i = 0;
                    foreach($cfisc->getDueDate() as $venc) :
                        $i++;
                        ?>
                        <td><?=$venc->FIV_DTA_VENCTO?></td>
                        <?php
                    endforeach;
                    if ($i < 3):
                        for ($i=$i;$i<3;$i++):
                            ?>
                            <td>---</td>
                            <?php
                        endfor;
                    endif;  
                    ?>
                    <td><?=$cfisc->FIS_USUARIO_INC?></td>
                    </tr>
                    <?php
                endif;
                endforeach;
            endif;
        ?>
        <?php 
            if ($aa1cfisc2):
                foreach($aa1cfisc2 as $cfisc) :
                    if ($divergente == '1'):
                        $amountFis = $cfisc->getDueDateAmount();
                        if ($cfisc->getNfReceipt()){
                            $amountPort = $cfisc->getNfReceipt()->getDueDateAmount();
                        } else {
                            $amountPort = 0;
                        }
                        if ($amountFis != $amountPort):
                        ?>
                        <tr>
                        <td><?=$cfisc->FIS_LOJ_DST?></td>
                        <td><?=$cfisc->FIS_NRO_NOTA?></td>
                        <td><?=$cfisc->FIS_DTA_AGENDA?></td>
                    <?php
                    if($cfisc->getNfReceipt()):// se estiver lançada
                        
                        $nfReceipt = $cfisc->getNfReceipt();
                        ?>
                        <td><?=substr($nfReceipt->updated_at,0,10)//nota?></td> 
                        <?php
                        if($nfReceipt->getDueDate() != null):
                            $i=0;
                            foreach($nfReceipt->getDueDate() as $vencimento):
                                $i++;
                            ?>
                                <td><?=$vencimento->due_date?></td>
                            <?php
                            endforeach;  
                            if ($i < 3):
                                for ($i=$i;$i<3;$i++):
                                    ?>
                                    <td>---</td>
                                    <?php
                                endfor;
                            endif;  
                        else:
                            ?>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <?php
                        endif;
                    else: // se não estiver 
                        ?>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        <?php
                    endif;
                    foreach($cfisc->getDueDate() as $venc) :
                        ?>
                        <td><?=$venc->FIV_DTA_VENCTO?></td>
                        <?php
                    endforeach;
                    $i = 1;
                    foreach($cfisc->getDueDate() as $venc) :
                        $i++;
                        ?>
                        <td><?=$venc->FIV_DTA_VENCTO?></td>
                        <?php
                    endforeach;
                    if ($i < 3):
                        for ($i=$i;$i<3;$i++):
                            ?>
                            <td>---</td>
                            <?php
                        endfor;
                    endif;  
                    ?>
                    
                    <td><?=$cfisc->FIS_USUARIO_INC?></td>
                    </tr>
                    <?php
                    else:
                    endif;
                else:

                    ?>
                                            <tr>
                        <td><?=$cfisc->FIS_LOJ_DST?></td>
                        <td><?=$cfisc->FIS_NRO_NOTA?></td>
                        <td><?=$cfisc->FIS_DTA_AGENDA?></td>
                    <?php
                    if($cfisc->getNfReceipt()):// se estiver lançada
                        
                        $nfReceipt = $cfisc->getNfReceipt();
                        ?>
                        <td><?=substr($nfReceipt->updated_at,0,10)//nota?></td> 
                        <?php
                        if($nfReceipt->getDueDate() != null):
                            $i=0;
                            foreach($nfReceipt->getDueDate() as $vencimento):
                                $i++;
                            ?>
                                <td><?=$vencimento->due_date?></td>
                            <?php
                            endforeach;  
                            if ($i < 3):
                                for ($i=$i;$i<3;$i++):
                                    ?>
                                    <td>---</td>
                                    <?php
                                endfor;
                            endif;  
                        else:
                            ?>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <?php
                        endif;
                    else: // se não estiver 
                        ?>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                            <td>---</td>
                        <?php
                    endif;
                    $i = 0;
                    foreach($cfisc->getDueDate() as $venc) :
                        $i++;
                        ?>
                        <td><?=$venc->FIV_DTA_VENCTO?></td>
                        <?php
                    endforeach;
                    if ($i < 3):
                        for ($i=$i;$i<3;$i++):
                            ?>
                            <td>---</td>
                            <?php
                        endfor;
                    endif;  
                    ?>
                    <td><?=$cfisc->FIS_USUARIO_INC?></td>
                    </tr>
                    <?php
                endif;
                endforeach;
            endif;
        ?>
        
           
            
            
            
            <td></td>
            <td></td>
            <td></td>
        </tr>
        
    </tbody>
</table>