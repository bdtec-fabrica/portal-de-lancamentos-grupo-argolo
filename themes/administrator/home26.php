<?php 
$v->layout("administrator/adm_theme");

?>
<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 60000);
</script>
<main class="main_content">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="/home2">Acompanhamento diario (lançamentos diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/1">Acompanhamento diario (lançamentos com
                    confêrencia)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/2">Duração media status diario (lançamentos
                    diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/3">Duração media status diario (lançamentos com
                    conferência)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/4">Situações Pontuais (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/5">Acompanhamento Reportados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/6">Acompanhamento Pendente Empresa (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/7">Acompanhamento Chegada Carga Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/8">Acompanhamento Atualizadas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/9">Atualizadas por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/10">Pendente cliente por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/11">Atualizadas por Operador Sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/12">Atendimento Solicitações de lançamento Por Loja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/13">Atualizações por hora / usúario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/14">Incluidas e Concluidas por hora</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/15">Acompanhamento Pendente Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/16">Acompanhamento Pendente Cliente Dias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/17">Acompanhamento Pendência Comercial Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/18">Acompanhamento Conferência Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/19">Acompanhamento NFe a Atualizar Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/20">Alocadas por Usuario sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/home2/21">Pendentes Prioridades Horas</a>
            </li>
            <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 180 or $_COOKIE['id'] == 20000151): ?>
            <li class="nav-item">
                <a class="nav-link" href="/home2/22">Pendente Usúario Media</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/home2/26">Notas Atualizadas Operador Hora</a>
            </li>
            <?php endif; ?>
        </ul>

        <div class="table-responsive">
                <table class="table table-striped table-bordered table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Operador</th>
                            <th scope="col">Entrada</th>
                            <th scope="col">Almoço</th>
                            <th scope="col">Retorno</th>
                            <th scope="col">Saida</th>
                            <th scope="col" colspan="2">06:00 Notas/Itens</th>
                            <th scope="col" colspan="2">07:00 Notas/Itens</th>
                            <th scope="col" colspan="2">08:00 Notas/Itens</th>
                            <th scope="col" colspan="2">09:00 Notas/Itens</th>
                            <th scope="col" colspan="2">10:00 Notas/Itens</th>
                            <th scope="col" colspan="2">11:00 Notas/Itens</th>
                            <th scope="col" colspan="2">12:00 Notas/Itens</th>
                            <th scope="col" colspan="2">13:00 Notas/Itens</th>
                            <th scope="col" colspan="2">14:00 Notas/Itens</th>
                            <th scope="col" colspan="2">15:00 Notas/Itens</th>
                            <th scope="col" colspan="2">16:00 Notas/Itens</th>
                            <th scope="col" colspan="2">17:00 Notas/Itens</th>
                            <th scope="col" colspan="2">18:00 Notas/Itens</th>
                            <th scope="col" colspan="2">19:00 Notas/Itens</th>
                            <th scope="col" colspan="2">20:00 Notas/Itens</th>
                            <th scope="col" colspan="2">21:00 Notas/Itens</th>
                            <th scope="col" colspan="2">22:00 Notas/Itens</th>
                            <th scope="col" colspan="2">23:00 Notas/Itens</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($rows as $row): ?>
                        <tr>
                            <th scope="row"><?=$row['operator']?></th>
                            <td><?=$user->getCheckIns(date('Y-m-d'),$row['id'])[1]?></td>
                            <td><?=$user->getCheckIns(date('Y-m-d'),$row['id'])[2]?></td>
                            <td><?=$user->getCheckIns(date('Y-m-d'),$row['id'])[3]?></td>
                            <td><?=$user->getCheckIns(date('Y-m-d'),$row['id'])[4]?></td>
                            <td><?=$row['quantidade_06']?></td>
                            <td><?=$row['itens_06']?></td>
                            <td><?=$row['quantidade_07']?></td>
                            <td><?=$row['itens_07']?></td>
                            <td><?=$row['quantidade_08']?></td>
                            <td><?=$row['itens_08']?></td>
                            <td><?=$row['quantidade_09']?></td>
                            <td><?=$row['itens_09']?></td>
                            <td><?=$row['quantidade_10']?></td>
                            <td><?=$row['itens_10']?></td>
                            <td><?=$row['quantidade_11']?></td>
                            <td><?=$row['itens_11']?></td>
                            <td><?=$row['quantidade_12']?></td>
                            <td><?=$row['itens_12']?></td>
                            <td><?=$row['quantidade_13']?></td>
                            <td><?=$row['itens_13']?></td>
                            <td><?=$row['quantidade_14']?></td>
                            <td><?=$row['itens_14']?></td>
                            <td><?=$row['quantidade_15']?></td>
                            <td><?=$row['itens_15']?></td>
                            <td><?=$row['quantidade_16']?></td>
                            <td><?=$row['itens_16']?></td>
                            <td><?=$row['quantidade_17']?></td>
                            <td><?=$row['itens_17']?></td>
                            <td><?=$row['quantidade_18']?></td>
                            <td><?=$row['itens_18']?></td>
                            <td><?=$row['quantidade_19']?></td>
                            <td><?=$row['itens_19']?></td>
                            <td><?=$row['quantidade_20']?></td>
                            <td><?=$row['itens_20']?></td>
                            <td><?=$row['quantidade_21']?></td>
                            <td><?=$row['itens_21']?></td>
                            <td><?=$row['quantidade_22']?></td>
                            <td><?=$row['itens_22']?></td>
                            <td><?=$row['quantidade_23']?></td>
                            <td><?=$row['itens_23']?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
        </div>
    </div>