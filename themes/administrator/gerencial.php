<?php 
$v->layout("administrator/adm_theme");

?>
<div class="bg-light p-5" style="margin-bottom:5%">
    <div class="container text-center">
        <h1 class="display-4">Gerencial estrategico BDTec</h1>
        <?php if ($date1):?>
        <p class="lead"><?=$date1.' à '.$date2?></p>
        <?php endif;?>
    </div>
</div>
<div class="container-lg">
    <div class="card-header">
        Periodo das vendas
    </div>
    <div class="card-body">
        <form action="/margin/webservice37" method="post">
            <div class="row">
                <div class="form-group col-md-2">
                    <span>Criação inicial</span>
                    <input name="date1" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação final</span>
                    <input name="date2" style="font-size:80%" class="form-control" type="date">
                </div>

            </div>
            <div class="form-group col-md-2">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <select name="base" class="form-control">
                <option selected disabled>Selecione a BASE...</option>
                <option value="1">Mix Bahia</option>
                <option value="2">Bispo e Dantas</option>
                <option value="3">Novo Mix</option>
                <option value="4">Unimar</option>
                <option value="7">Bem Barato</option>
            </select>
        </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
            
        </form>
    </div>


</div>

<div class="container-lg">
    <table class="table table-hover sortable" id="table">
        <thead>
            <tr>
                <th scope="col">Loja</th>
                <th scope="col">Venda Atual</th>
                <th scope="col">Venda Mês Anterior</th>
                <th scope="col">Percentual alcançado mês anterior</th>
                <th scope="col">Venda Ano Anterior</th>
                <th scope="col">Percentual alcançado ano anterior</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($lojas as $loja):
            // if($valorVenda[$loja] > 0):?>
            <div
            >
            <tr <?php if (((i0($valorVenda[$loja],$valorVendaMes[$loja])-$valorVendaMes[$loja])/i0($valorVenda[$loja],$valorVendaMes[$loja]))*100 < 0 && ((i0($valorVenda[$loja],$valorVendaAno[$loja])-$valorVendaAno[$loja])/i0($valorVenda[$loja],$valorVendaAno[$loja]))*100 < 0):?>
                class="table-danger"
                <?php elseif(((i0($valorVenda[$loja],$valorVendaMes[$loja])-$valorVendaMes[$loja])/i0($valorVenda[$loja],$valorVendaMes[$loja]))*100 < 0 || ((i0($valorVenda[$loja],$valorVendaAno[$loja])-$valorVendaAno[$loja])/i0($valorVenda[$loja],$valorVendaAno[$loja]))*100 < 0): ?>
                class="table-warning" <?php else: ?> class="table-success"<?php endif;?>>
                <form method="POST" action="/margin/webservice40">
                    <input type="hidden" name="loja" value="<?=$loja?>">
                    <input type="hidden" name="date1" value="<?=$date1?>">
                    <input type="hidden" name="date2" value="<?=$date2?>">
                    <th scope="row"><?=$loja?></th>
                    <td><?=number_format($valorVenda[$loja],2,",",".")?></td>
                    <td><?=number_format($valorVendaMes[$loja],2,",",".")?></td>
                    <td><?=number_format(((i0($valorVenda[$loja],$valorVendaMes[$loja])-$valorVendaMes[$loja])/i0($valorVenda[$loja],$valorVendaMes[$loja]))*100,2,",",".")?>%
                        <?php if(((i0($valorVenda[$loja],$valorVendaMes[$loja])-$valorVendaMes[$loja])/i0($valorVenda[$loja],$valorVendaMes[$loja]))*100 >0):?>
                        ↑
                        <?php else: ?>
                        ↓
                        <?php endif;?>
                    </td>
                    <td><?=number_format($valorVendaAno[$loja],2,",",".")?></td>
                    <td><?=number_format(((i0($valorVenda[$loja],$valorVendaAno[$loja])-$valorVendaAno[$loja])/i0($valorVenda[$loja],$valorVendaAno[$loja]))*100,2,",",".")?>%
                        <?php if(((i0($valorVenda[$loja],$valorVendaAno[$loja])-$valorVendaAno[$loja])/i0($valorVenda[$loja],$valorVendaAno[$loja]))*100 >0):?>
                        ↑
                        <?php else: ?>
                        ↓
                        <?php endif;?>
                    </td>
                    <td><button type="submit" class="btn btn-primary">+</button></td>
                </form>
            </tr>
            </div>
            <?php 
            $valorVendaTotal += $valorVenda[$loja];
            $valorVendaMesTotal += $valorVendaMes[$loja];
            $valorVendaAnoTotal += $valorVendaAno[$loja];
        // endif;
        endforeach; ?>
            <tr>
                <th scope="col">Total</th>
                <th scope="col"><?=number_format($valorVendaTotal,2,",",".")?></th>
                <th scope="col"><?=number_format($valorVendaMesTotal,2,",",".")?></th>
                <th scope="col"></th>
                <th scope="col"><?=number_format($valorVendaAnoTotal,2,",",".")?></th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
        </tbody>
    </table>
    <form>
        <input type="button" value="Voltar" onClick="history.go(-1)">
        <input type="button" value="Avançar" onCLick="history.forward()">
        <input type="button" value="Atualizar" onClick="history.go(0)">
    </form>
</div>
