<?php 
$v->layout("administrator/adm_theme");
?>
<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Painel de verificação de Lançamentos BDTec</h1>
                <p class="lead"></p>
            </div>
        </div>
        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
            data-bs-toggle="collapse" data-bs-target="#multiCollapseExample1" aria-expanded="false"
            aria-controls="multiCollapseExample1">Filtro
            ↴</button>

    <div class="collapse multi-collapse container" id="multiCollapseExample1">
        <form action="/request/verification" method="post">
            <div class="row">
                <div class="form-group col-md-1">
                    <span>#Codigo</span>
                    <input name="id" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Número</span>
                    <input name="nfNumber" class="form-control" type="text">
                </div>
                <div class="form-group col-md-5">
                    <span>Chave Acesso</span>
                    <input name="accessKey" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação inicial</span>
                    <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação final</span>
                    <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-1">
                    <span>Loja</span>
                    <input class="form-control" name="store">
                </div>
                <div class="form-group col-md-3">
                    <span>Verificação</span>
                    <select class="form-control" name="verification">
                    <option value="1">Não esta no RMS</option>
                    <option value="2">Não esta no Portal</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <div class="container" style="margin-bottom:2%">
        <button type="button" class="btn btn-secondary btn-lg" data-bs-toggle="modal"
            data-bs-target="#adicionar">Adicionar+</button>
    </div>
    <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Importação manual notas retaguarda</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="formUpload" id="formUpload" method="post">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" for="inputGroupFile01">Arquivo CSV</span>
                                    <input name="csv" multiple type="file" class="form-control"
                                        id="inputGroupFile01">
                                </div>
                            </div>
                            <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                                id="porcentagem">0%</span>
                            <div id="resposta" style="width:100%"></div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#btnEnviar').click(function () {
                $('#formUpload').ajaxForm({
                    uploadProgress: function (event, position, total, percentComplete) {
                        $('progress').attr('value', percentComplete);
                        $('#porcentagem').html(percentComplete + '%');
                    },
                    success: function (data) {
                        $('progress').attr('value', '100');
                        $('#porcentagem').html('100%');
                        if (data.sucesso == true) {
                            $('#resposta').html('<div class="alert alert-success" role="alert">' + data.msg + '</div>');
                            document.location.reload(true);
                        } else {
                            $('#resposta').html('<div class="alert alert-danger" role="alert">' + data.msg + '</div>');
                        }
                    },
                    error: function () {
                        $('#resposta').html('<div class="alert alert-danger" role="alert">Erro ao enviar requisição!!!</div>');
                    },
                    dataType: 'json',
                    url: '/request/verification/import',
                    resetForm: true
                }).submit();
            })
        })
    </script>
    <div class="table-responsive-lg">
        <table class="table dt-responsive table-hover nowrap w-100 sortable" style="font-size:60%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Loja</th>
                    <th>Número</th>
                    <th>Inclusão</th>
                    <th>Fornecedor</th>
                    <th>Chave de acesso</th>
                </tr>
            </thead>
            <tbody>
            <?php if($request):
                    
                  foreach ($request as  $request):
                    if($type == 1):
                        if($request->inRMS == 0):
            ?>
                <tr>
                    <th scope="row"><?=$request->id?></th>
                    <td><?= $request->store_id?></td>
                    <td><?= $request->nf_number?></td>
                    <td><?= $request->getCreatedAt?></td>
                    <td><?= $request->getFornecedor?></td>
                    <td><?= $request->access_key?></td>
                    <td><?= $request->inRMS?></td>
                </tr>
            <?php 
                        endif;
                else:
                    if($request->inPortal == 0):
                ?>
                <tr>
                    <th scope="row"><?=$request->id?></th>
                    <td><?= $request->store_id?></td>
                    <td><?= $request->nf_number?></td>
                    <td><?= $request->getCreatedAt?></td>
                    <td><?= $request->getFornecedor?></td>
                    <td><?= $request->access_key?></td>
                    <td><?= $request->inPortal?></td>
                </tr>

                    <?php
                    endif;
                endif;
                  endforeach;
                  endif;
                  ?>
            </tbody>
        </table>
    </div>
</main>
