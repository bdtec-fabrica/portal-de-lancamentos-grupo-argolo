<?php 
session_start();
$v->layout("administrator/adm_theme");
?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Consistencia KW x RMS</h1>
        <p class="lead"></p>
    </div>
</div>
<div class="container" style="margin-bottom:5%">
    <form method="post" action="/usuarios/usersSearch" class="row gx-3 gy-2 align-items-center"
        style="margin-bottom:2%">
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeInputName">Loja</label>
            <input name="store" type="text" class="form-control" id="specificSizeInputName" placeholder="Loja">
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th style="min-width:25px">Loja</th>
            <th style="min-width:25px">KW</th>
            <th style="min-width:25px">RMS</th>
        </tr>
    </thead>
    <tbody>
        <?php 
    foreach ($users as $user):
        ?>
        <tr>
            <td><?=$user->name; ?></td>
            <td><?=$user->store_code; ?></td>
            <td><?=$user->email; ?></td>
            <td><?=$user->class; ?></td>
            <td></td>
        </tr>
        <?php
    endforeach; ?>
    </tbody>
</table>