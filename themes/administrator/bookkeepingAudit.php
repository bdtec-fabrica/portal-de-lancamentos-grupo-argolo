<?php 
session_start();
$v->layout("administrator/adm_theme");

?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Auditoria de Postagem</h1>
        <p class="lead">Confirmação de escrituração no retaguarda.</p>
    </div>
</div>
<div class="container" style="margin-bottom:5%">
    <form method="post" action="/auditoria-lancamento/search" class="row gx-3 gy-2 align-items-center"
        style="margin-bottom:2%">
        <div class="col-sm-2">
            <label for="specificSizeInputName">Loja</label>
            <input name="store" type="text" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Nota Fiscal</label>
            <input name="nota" type="text" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Data agenda inicial</label>
            <input name="initialDate" required type="date" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Data agenda final</label>
            <input name="finalDate" required type="date" class="form-control" id="specificSizeInputName">
        </div>
        <!-- <div class="col-sm-2">
            <label for="specificSizeInputName">Margem ></label>
            <input type="number" name="maior" class="form-control">
        </div>

        <div class="col-sm-2">
            <label for="specificSizeInputName">Margem <</label>
            <input type="number" name="menor" class="form-control">
        </div> -->

        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th>Loja</th>
            <th>Nota</th>
            <th>Usuário</th>
            <th>Escriturada</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($nfReceipt):
            foreach($nfReceipt as $nR):
                ?>
                <tr>
                <td><?=$nR->store_id?></td>
                <td><?=$nR->nf_number?></td>
                <td><?=$nR->getOperator?></td>
                <td><?=$nR->getAA1CFISC?></td>
            </tr>
                <?php
            endforeach;
        endif;
        ?>       
    </tbody>
</table>