<?php 
$v->layout("administrator/adm_theme");
?>
<div class="container" style="margin-top:5%">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <?php 
        $count = 1;
        foreach ($stores as $store): ?>
        <li class="nav-item" role="presentation">
            <button style="width: 150px;" class="nav-link<?php if ($count == 1): ?> active <?php endif;?>" id="pills-home-tab"
                data-bs-toggle="pill" data-bs-target="#loja<?=$store->id?>" type="button" role="tab"
                aria-controls="pills-home" aria-selected="true">Loja <?=$store->code?></button>
        </li>
        <?php 
        $count++;
        endforeach ?>
    </ul>

    <div class="tab-content" id="pills-tabContent">
        <?php 
        $count = 1;
        foreach($stores as $store): ?>
        <div class="painel<?=$store->id?> tab-pane fade show<?php if($count == 1):?> active <?php endif;?>" id="loja<?=$store->id?>"
            role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
            <div class="p-5 mb-4 bg-light rounded-3">
                <div class="container-fluid py-5">
                    <h1 class="display-5 fw-bold"><?= $store->code ?> - <?= $store->company_name ?></h1>
                    <p class="col-md-8 fs-4">Lista de contatos para os envios das notificações.</p>
                    <button class="btn btn-primary btn-lg" id="adicionar<?=$store->id?>" type="button">Adicionar Contato
                        +</button>
                        <a target="_blank" href="<?=URL_BASE?>/services-performed/sendIndicadoresPDF?loja=<?=$store->code?>">Enviar Indicadores</a>
                </div>
            </div>
            <?php foreach($store->contacts as $contacts) :  ?>
            <form id="form<?=$store->id?>1" method="post">
                <div class="row">
                    <div class="col">
                        <input type="text" id="<?=$store->id?>1" value="<?=$contacts->name?>" name="name" class="form-control name" placeholder="Nome" aria-label="First name">
                    </div>
                    <div class="col">
                        <input type="text" id="<?=$store->id?>1" value="<?=$contacts->type?>" name="type" class="form-control name" placeholder="Nome" aria-label="First name">
                    </div>
                    <div class="col">
                        <input type="text" id="<?=$store->id?>1"value="<?=$contacts->phone?>" name="phone" class="form-control phone" placeholder="Telefone"
                            aria-label="Last name">
                        <input type="hidden" value="<?=$store->id?>" name="store">
                        <input type="hidden" value="<?=$contacts->id?>" name="id">
                    </div>
                    <div class="col">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <button type="button" id="<?=$store->id?>1" class="btn btn-danger">Excluir</button>
                            <button type="button" id="<?=$store->id?>1" class="btn btn-warning">Alterar</button>
                        </div>
                        
                    </div>
                </div>
            </form>
            <?php endforeach; ?>
        </div>
        <script>
            i = 999999;
            $("#adicionar<?=$store->id?>").click(function () {
                $('.painel<?=$store->id?>').append("<form  method='post' class='alert alert-warning' id='form<?=$store->id?>" + i + "'>" +
                    "<div class='row'>" +
                    "<div class='col'>" +
                    "<input type='text' id='<?=$store->id?>" + i +
                    "' class='form-control name' name='name' placeholder='Nome' aria-label='First name'>" +
                    "</div>" +
                    "<div class='col'>" +
                    "<input type='text' name='phone' id='<?=$store->id?>" + i +
                    "' class='form-control phone' placeholder='Telefone' aria-label='Last name'>" +
                    "</div>" +
                    "<div class='col'>" +
                    "<input type='text' name='type' id='<?=$store->id?>" + i +
                    "' class='form-control type' placeholder='Tipo' aria-label='Tipo'>" +
                    "</div>" +
                    "<input type='hidden' value='<?=$store->id?>' name='store'>"+
                    "</div>" +
                    "<div class='col'>" +
                    "<div class='btn-group' role='group' aria-label='Basic mixed styles example'>" +
                    "<button type='button' id='<?=$store->id?>" + i + "' class='btn btn-danger'>Excluir</button>" +
                    "<button type='button' id='<?=$store->id?>" + i + "' class='btn btn-success add<?=$store->id?>" + i +
                    "'>Adicionar</button>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</form>"
                );
                i++;
            });
            $(".painel<?=$store->id?>").on("focus", ".name", function () {
                var button_id = $(this).attr("id");
                $('#form' + button_id).addClass('alert alert-warning');
            })
            $(".painel<?=$store->id?>").on("focus", ".phone", function () {
                var button_id = $(this).attr("id");
                $('#form' + button_id).addClass('alert alert-warning');
            })
            $(".painel<?=$store->id?>").on("click", ".btn-danger", function () {
                var button_id = $(this).attr("id");
                if (confirm("Tem certeza que deseja excluir?")) {
                    $('#form' + button_id).ajaxForm({
                    success: function (data) {
                        if (data.sucesso == true) {
                            $('#form' + button_id).remove();
                            alert(data.msg);
                        } else {
                            alert('#form' + button_id + " formulario não enviado!");
                        }

                    },
                    error: function () {
                        alert('#form' + button_id + 'Erro ao enviar requisição!');
                    },
                    dataType: 'json',
                    url: '/lojas/contacts/delete',
                }).submit();
                }
            });

            $(".painel<?=$store->id?>").on("click", ".btn-success", function () {
                var button_id = $(this).attr("id");
                $('#form' + button_id).ajaxForm({
                    success: function (data) {

                        if (data.sucesso == true) {
                            $('#form' + button_id).removeClass('alert alert-warning');
                            $('#form' + button_id).addClass('alert alert-success');
                            $('#form' + button_id).prepend( "<input type='hidden' name='id' value='"+data.id+"'>" );
                            $('.add' + button_id).remove();
                            alert("Contato Salvo!");
                        } else {
                            alert('#form' + button_id + " formulario não enviado!");
                        }

                    },
                    error: function () {
                        alert('#form' + button_id + 'Erro ao enviar requisição!');
                    },
                    dataType: 'json',
                    url: '/lojas/contacts/add',
                }).submit();
            });
            $(".painel<?=$store->id?>").on("click", ".btn-warning", function () {
                var button_id = $(this).attr("id");
                $('#form' + button_id).attr('action', 'alterar');
                $('#form' + button_id).ajaxForm({
                    success: function (data) {
                        if (data.sucesso == true) {
                            $('#form' + button_id).removeClass('alert alert-warning');
                            $('#form' + button_id).addClass('alert alert-success');
                            alert("Contato alterado!");
                        } else {
                            alert('#form' + button_id + " formulario não enviado!");
                        }

                    },
                    error: function () {
                        alert('#form' + button_id + 'Erro ao enviar requisição!');
                    },
                    dataType: 'json',
                    url: '/lojas/contacts/update',
                }).submit();

            });
        </script>
        <?php 
        $count++;
    endforeach; ?>
    </div>
</div>
