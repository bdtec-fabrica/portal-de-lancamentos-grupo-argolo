
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
<script>
    $.ajax({
        url: 'http://localhost:8080/NumDocumentos',
        method: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: '{"jid":"557192424238@c.us","type":"text","payload":{"message":"1234 TESTE"}}'
    }).done(function (res) {
        console.log(res);
    })
</script>
<?php 
die;
session_start();
$v->layout("administrator/adm_theme");

?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Controle de Licenças</h1>
    </div>
</div>
<div class="container">
    <div class="row">
        <form name="formUpload" action="/controle-licencas/search" method="POST"></form>
        <div class="col">
            <div class="mb-3">
                <input placeholder="Loja" id="store1" type="text" class="form-control" name="store">
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <input placeholder="Systema" id="system1" type="text" class="form-control" name="system">
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <input type="date" id="date1" class="form-control" name="date">
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
        </form>
        <div class="col">
            <div class="mb-3">
                <form name="formAdicionar">
                    <input type="hidden" name="store" id="store2">
                    <input type="hidden" name="system" id="system2">
                    <input type="hidden" name="date" id="date2">
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </form>
                <script>
                    $("#store1").blur(function () {
                        $("#store2").val($("#store1").val())
                    });
                    $('form[name="formAdicionar"]').submit(function (event) {
                        event.preventDefault();

                        $.ajax({
                            url: '/controle-licencas/adicionar',
                            type: 'post',
                            data: $(this).serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success === true) {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        timer: 20000,
                                        icon: "success",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "success"
                                    });
                                    document.location.reload(true);
                                } else {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        icon: "error",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "danger"
                                    });
                                }
                            }
                        })
                    });
                </script>

            </div>
        </div>
    </div>
</div>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Loja</th>
                <th scope="col">Sistema</th>
                <th scope="col">Validade</th>
                <th scope="col">Atualizado em</th>
                <th scope="col">Opções</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($licenses as $license):?>
            <tr>
                <th scope="row"><?=$license->store?></th>
                <td><?=$license->system?></td>
                <td><?=$license->date?></td>
                <td><?=$license->created_at?></td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <form name="excluir<?=$license->id?>">
                            <input type="hidden" name="id" value="<?=$license->id?>">
                            <button type="submit" class="btn btn-danger">Deletar</button>
                        </form>
                        <button type="button" class="btn btn-warning" data-bs-toggle="modal"
                            data-bs-target="#alterar<?=$license->id?>">Alterar</button>
                    </div>
                </td>
            </tr>

            <div class="modal fade" id="alterar<?=$license->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Alterar Licença</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form name="formAlterar<?=$license->id?>">
                                <div class="mb-3">
                                    <label for="recipient-name" class="col-form-label">Loja:</label>
                                    <input type="hidden" value="<?=$license->id?>" name="id">
                                    <input type="text" value="<?=$license->store?>" name="store" class="form-control"
                                        id="recipient-name">
                                </div>
                                <div class="mb-3">
                                    <label for="recipient-name" class="col-form-label">Sistema:</label>
                                    <input type="text" value="<?=$license->system?>" name="system" class="form-control"
                                        id="recipient-name">
                                </div>
                                <div class="mb-3">
                                    <label for="recipient-name" class="col-form-label">Vencimento:</label>
                                    <input type="date" value="<?=$license->date?>" name="date" class="form-control"
                                        id="recipient-name">
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $('form[name="excluir<?=$license->id?>"]').submit(function (event) {
                    event.preventDefault();
                    var x;
                    var r = confirm(
                        "Excluir o lançamento ?"
                    );
                    if (r == true) {
                        $.ajax({
                            url: '/controle-licencas/deletar',
                            type: 'post',
                            data: $(this).serialize(),
                            dataType: 'json',
                            success: function (response) {

                                if (response.success === true) {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        timer: 20000,
                                        icon: "success",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "success"
                                    });

                                } else {
                                    swal({
                                        title: "Bispo & Dantas",
                                        text: response.message,
                                        icon: "error",
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        type: "danger"
                                    });
                                }

                            }
                        })
                    }
                });
            </script>
            <script>
                $('form[name="formAlterar<?=$license->id?>"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/controle-licencas/alterar',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 20000,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }
                        }
                    })
                });
            </script>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>