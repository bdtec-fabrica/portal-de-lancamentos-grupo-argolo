<?php 
$v->layout("administrator/adm_theme");

?>
<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 60000);
</script>
<main class="main_content">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="/home2">Acompanhamento diario (lançamentos diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/1">Acompanhamento diario (lançamentos com
                    confêrencia)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/2">Duração media status diario (lançamentos
                    diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/3">Duração media status diario (lançamentos com
                    conferência)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/4">Situações Pontuais (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/5">Acompanhamento Reportados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/6">Acompanhamento Pendente Empresa (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/7">Acompanhamento Chegada Carga Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/8">Acompanhamento Atualizadas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/9">Atualizadas por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/10">Pendente cliente por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/11">Atualizadas por Operador Sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/12">Atendimento Solicitações de lançamento Por Loja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/13">Atualizações por hora / usúario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/14">Incluidas e Concluidas por hora</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/15">Acompanhamento Pendente Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/16">Acompanhamento Pendente Cliente
                    Dias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/17">Acompanhamento Pendência Comercial Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/18">Acompanhamento Conferência Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/19">Acompanhamento NFe a Atualizar Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  active" aria-current="page" href="/home2/20">Alocadas por Usuario sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/21">Pendentes Prioridades Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/22">Pendentes Usuário Media</a>
            </li>
            <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 180 or $_COOKIE['id'] == 20000151): ?>
            <li class="nav-item">
                <a class="nav-link" href="/home2/26">Notas Atualizadas Operador Hora</a>
            </li>
            <?php endif; ?>
        </ul>
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-3">
                    <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Pendente Cliente Dias</h1>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC0?><sup style="font-size: 20px"></sup></h3>
                        <p>Menos de 1 dia</p>
                        <p><?=$request->getItemsIds($request->getNfPC0)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC0?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC1?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 1 dia</p>
                        <p><?=$request->getItemsIds($request->getNfPC1)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC1?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC2?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 2 dias</p>
                        <p><?=$request->getItemsIds($request->getNfPC2)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC2?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC4?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 4 dias</p>
                        <p><?=$request->getItemsIds($request->getNfPC4)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC4?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC7?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 7 dias</p>
                        <p><?=$request->getItemsIds($request->getNfPC7)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC7?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$request->getAmountPC10?><sup style="font-size: 20px"></sup></h3>
                        <p>Mais de 10 dias</p>
                        <p><?=$request->getItemsIds($request->getNfPC10)+0?> Itens</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-checkmark"></i>
                    </div>
                    <a href="<?=URL_BASE?>/request/<?=$request->getNfPC10?>"
                        class="small-box-footer">Mais
                        Informações <i class="ion ion-checkmark"></i></a>
                </div>
            </div>
        </div>
    </div>