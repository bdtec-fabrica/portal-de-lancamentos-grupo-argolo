<?php 
session_start();
$v->layout("client/_theme");

?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Cadastros</h1>
        <p class="lead">Relatorio de cadastros por usuário</p>
    </div>
</div>
<table class="table table-hover sortable" style="text-align:right">
    <thead>
        <tr>
            <th style="min-width:25px">ID da Nota</th>
            <th style="min-width:25px">Referencia</th>
            <th style="min-width:25px">Descrição</th>
            <th style="min-width:25px">EAN</th>
            <th style="min-width:25px">Operador</th>
        </tr>
    </thead>
    <tbody>
        <?php 
    for($j=0;$j<$i;$j++):
        ?>
        <tr>
            <td><?=$line[$j]["nfId"]?></td>
            <td><?=$line[$j]["reference"]?></td>
            <td><?=$line[$j]["description"]?></td>
            <td><?=$line[$j]["ean"]?></td>
            <td><?=$line[$j]["operator"]?></td>
        </tr>
        <?php
    endfor; ?>
    </tbody>
</table>
    