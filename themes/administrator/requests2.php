<?php

use Sabberworm\CSS\Value\URL;

$v->layout("administrator/adm_theme");
?>

<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Painel Lançamentos BDTec</h1>
                <p class="lead"></p>
            </div>
        </div>
        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
            data-bs-toggle="collapse" data-bs-target="#multiCollapseExample1" aria-expanded="false"
            aria-controls="multiCollapseExample1">Filtro
            ↴</button>

    </div>
    <div class="collapse multi-collapse container" id="multiCollapseExample1">
        <form action="/request/searchRequest" method="post">
            <div class="row">
                <div class="form-group col-md-1">
                    <span>#Codigo</span>
                    <input name="id" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Número</span>
                    <input name="nfNumber" class="form-control" type="text">
                </div>
                <div class="form-group col-md-5">
                    <span>Chave Acesso</span>
                    <input name="accessKey" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação inicial</span>
                    <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação final</span>
                    <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Alteração inicial</span>
                    <input name="alteracaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Alteração final</span>
                    <input name="alteracaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Operador</span>
                    <select name="idOperator" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($users as $user):
                            ?>
                        <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <span>Status</span>
                    <select name="status" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($status as $statuss):
                            ?>
                        <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-1">
                    <span>Loja</span>
                    <input class="form-control" name="store">
                </div>
                <div class="form-group col-md-2">
                    <span>Grupo</span>
                    <select name="storeGroup" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($storeGroups as $storeGroup):
                            ?>
                        <option value="<?= $storeGroup->id ?>"><?= $storeGroup->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <span>Operação</span>
                    <select name="operation" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todas</option>
                        <?php
                        foreach ($nfeOperation as $nfeOperations):
                            ?>
                        <option value="<?= $nfeOperations->id ?>"><?= $nfeOperations->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <span>Tipo</span>
                    <select name="type" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($nfeType as $nfeTypes):
                            ?>
                        <option value="<?= $nfeTypes->id ?>"><?= $nfeTypes->name ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <div>
        <div class="container" style="margin-bottom:3%">
            <button type="button" class="btn btn-secondary btn-lg" data-bs-toggle="modal"
                data-bs-target="#adicionar">Adicionar+</button>
            <button type="button" class="btn btn-info btn-lg" data-bs-toggle="modal" data-bs-target="#report">Relatorio
                de Notas <i class="ion ion-document-text"></i></button>
            <button type="button" class="btn btn-danger btn-lg" data-bs-toggle="modal"
                data-bs-target="#report2">Relatorio de Reportes <i class="ion ion-document-text"></i></button>
                <button type="button" class="btn btn-primary btn-lg" data-bs-toggle="modal" data-bs-target="#exampleModal">Relatorio de Cadastro</button>
            <a class="btn btn-lg btn-danger" href="<?=URL_BASE?>/home2/23">1+</a>
            <a class="btn btn-lg btn-danger" href="<?=URL_BASE?>/home2/24">2+</a>
            <a class="btn btn-lg btn-danger" href="<?=URL_BASE?>/home2/25">4+</a>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Relatorio de Cadastros</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/request/registerReport" target="_blank">
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Data inicial:</label>
                            <input type="date" name="date1" class="form-control" id="recipient-name">
                        </div>
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Data final:</label>
                            <input type="date" name="date2" class="form-control" id="recipient-name">
                        </div>
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Data Oerador:</label>
                            <select name="operador" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <?php
                        foreach ($users as $user):
                            ?>
                        <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                        <?php
                        endforeach;
                    ?>
                    </select>
                        </div>
                        
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send message</button>
                    </form>
            </div>
        </div>
                    </div>
                    </div>
        <div class="modal fade" id="report2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Relatorio de Notas Reportadas</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/relatorioNotasReportadas" target="_blank" method="post">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Criação inicial:</label>
                                <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Criação final:</label>
                                <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Alteração inicial:</label>
                                <input name="alteracaoInicial" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Alteração Final:</label>
                                <input name="alteracaoFinal" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                                <select class="form-select" name="type">
                                    <option value="">Selecione ...</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Operador</label>
                                <select name="idOperator" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todos</option>
                                    <?php
                        foreach ($users as $user):
                            ?>
                                    <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Status</label>
                                <select name="status" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todos</option>
                                    <?php
                        foreach ($status as $statuss):
                            ?>
                                    <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Loja</label>
                                <input class="form-control" name="store">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Grupo</label>
                                <select name="storeGroup" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todos</option>
                                    <?php
                        foreach ($storeGroups as $storeGroup):
                            ?>
                                    <option value="<?= $storeGroup->id ?>"><?= $storeGroup->name ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Operação</label>
                                <select name="operation" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todas</option>
                                    <?php
                        foreach ($nfeOperation as $nfeOperations):
                            ?>
                                    <option value="<?= $nfeOperations->id ?>"><?= $nfeOperations->name ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" id="btnEnviar" class="btn btn-primary">Enviar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="report" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Relatorio de Notas Postadas</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/relatorioNotas" target="_blank" method="post">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Criação inicial:</label>
                                <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Criação final:</label>
                                <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Alteração inicial:</label>
                                <input name="alteracaoInicial" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Alteração Final:</label>
                                <input name="alteracaoFinal" style="font-size:80%" class="form-control" type="date">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                                <select class="form-select" name="type">
                                    <option value="">Selecione ...</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Operador</label>
                                <select name="idOperator" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todos</option>
                                    <?php
                        foreach ($users as $user):
                            ?>
                                    <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Status</label>
                                <select name="status" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todos</option>
                                    <?php
                        foreach ($status as $statuss):
                            ?>
                                    <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Loja</label>
                                <input class="form-control" name="store">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Grupo</label>
                                <select name="storeGroup" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todos</option>
                                    <?php
                        foreach ($storeGroups as $storeGroup):
                            ?>
                                    <option value="<?= $storeGroup->id ?>"><?= $storeGroup->name ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Operação</label>
                                <select name="operation" class="form-control" id="inputGroupSelect01">
                                    <option selected disabled>Todas</option>
                                    <?php
                        foreach ($nfeOperation as $nfeOperations):
                            ?>
                                    <option value="<?= $nfeOperations->id ?>"><?= $nfeOperations->name ?></option>
                                    <?php
                        endforeach;
                    ?>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" id="btnEnviar" class="btn btn-primary">Enviar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Postagem Manual</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="formUpload" id="formUpload" method="post">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do documento:</label>
                                <input required name="nfNumber" type="number" class="form-control" id="recipient-name">
                                <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="accessKey" type="text" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                                <select class="form-select" name="type" required>
                                    <option value="">Selecione ...</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Quantidade de itens</label>
                                <input required name="itemsQuantity" type="number" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">1º Vencimento</label>
                                <input required name="dueDate1" type="date" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">2º Vencimento</label>
                                <input required name="dueDate2" type="date" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">3º Vencimento</label>
                                <input required name="dueDate3" type="date" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Observações:</label>
                                <textarea required name="note" class="form-control" id="message-text"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                <select class="form-select" name="nfeOperation">
                                    <option value="99">Não</option>
                                    <option value="2">Sim</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                                    <input name="files[]" multiple type="file" class="form-control"
                                        id="inputGroupFile01">
                                </div>
                            </div>
                            <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                                id="porcentagem">0%</span>
                            <div id="resposta" style="width:100%"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#btnEnviar').click(function () {
                    $('#formUpload').ajaxForm({
                        uploadProgress: function (event, position, total, percentComplete) {
                            $('progress').attr('value', percentComplete);
                            $('#porcentagem').html(percentComplete + '%');
                        },
                        success: function (data) {
                            $('progress').attr('value', '100');
                            $('#porcentagem').html('100%');
                            if (data.sucesso == true) {
                                $('#resposta').html(
                                    '<div class="alert alert-success" role="alert">' +
                                    data.msg + '</div>');
                                document.location.reload(true);
                            } else {
                                $('#resposta').html(
                                    '<div class="alert alert-danger" role="alert">' +
                                    data.msg + '</div>');
                            }
                        },
                        error: function () {
                            $('#resposta').html(
                                '<div class="alert alert-danger" role="alert">Erro ao enviar requisição!!!</div>'
                            );
                        },
                        dataType: 'json',
                        url: '/request/addRequest',
                        resetForm: true
                    }).submit();
                })
            })
        </script>
        <div class="table-responsive-lg" style="padding:2%">
            <table class="table dt-responsive table-hover nowrap w-100 sortable" style="font-size:80%">
                <thead class="">
                    <tr>
                        <th>#</th>
                        <th>Loja</th>
                        <th>Número</th>
                        <th>Inclusão</th>
                        <th>Cliente</th>
                        <th>Fornecedor</th>
                        <th>Tipo</th>
                        <th>Grupo</th>
                        <th>Qtd</th>
                        <th>Alteração</th>
                        <th style="min-width:170px">Operador</th>
                        <th style="min-width:170px">Operação</th>
                        <th style="min-width:150px">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $requests = json_decode($requests, true);
                    $k = 0;
                    foreach ($requests as $request):
                        $k++;
                    ?>
                    <tr class="alert" style="background:<?=$request['getOperationColor']?>">
                        <td><a href="/request/interacao/<?=$request['id']?>" target="_blank"
                                style="text-decoration:none"><button class="btn"><?=$request['id']?></button></a></td>
                        <td><?=$request['store_id']?></td>
                        <td><?=$request['nf_number']?>
                        <?php if ($request['error_cost'] == 'E' && $request['clientGroup'] == 'Mix Bahia' ): ?>
                            <i class="ion ion-alert"></i>

                        <?php endif; ?>
                        </td>
                        <td><?=$request['getCreatedAt']?></td>
                        <td><?=$request['getClient']?></td>
                        <td><?=$request['getFornecedor']." - ".$request['codigoInternoCD']?></td>
                        <td><?=$request['getType']?></td>
                        <td><?=$request['clientGroup']?></td>
                        <td><?=$request['items_quantity']?></td>
                        <td><?=$request['getUpdatedAt']?></td>
                        <td>
                            <form name="update<?=$request['id']?>">
                            <?php if ($request['operator'] == 147): ?>
                                <div class="input-group">
                                    <input type="hidden" name="id" value="<?=$request['id']?>">
                                    <select class="form-select" id="inputGroupSelect04"
                                        aria-label="Example select with button addon" name="operator"
                                        style="font-size:90%">
                                        <option value="<?=$request['operator']?>"><?=$request['getOperator']?></option>
                                        <?php 
                                        foreach ($users as $user):
                                        ?>
                                        <option value="<?= $user->id ?>"><?= $user->getLogin() ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            <?php else: ?>
                                <input type="hidden" name="id" value="<?=$request['id']?>">
                                <?=$request['getOperator']?>
                            <?php endif;?>
                        </td>
                        <td><?=$request['getOperation']?></td>
                        <td>
                            <div class="input-group">
                                <select class="form-select aquii<?=$request['id']?>" id="aquii<?=$request['id']?>"
                                    aria-label="Example select with button addon" id="statusUpdate" name="status"
                                    style="font-size:90%">
                                    <option value="<?=$request['status_id']?>"><?=$request['getStatus']?></option>
                                    <?php
                                                foreach ($status as $statuss):
                                                    if ($statuss->id != 2 && $statuss->id != 7 && $statuss->id != 8 && $statuss->id != 9 && $statuss->id != 11 && $statuss->id != 15):
                                            ?>
                                    <option value="<?= $statuss->id ?>"><?= $statuss->name ?></option>
                                    <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                    <option value="11">Pendente Comercial</option>
                                    <option value="2">Pendente Cliente</option>
                                    <option value="9">Cadastro</option>
                                    <option value="7">Cancelada</option>
                                </select>
                            </div>

                        </td>
                        <td>
                            <button class="btn btn-success" type="submit"><i
                                    class="ion ion-checkmark-circled"></i></button>
                            </form>
                            <script>
                                function a<?=$request['id']?>(){
                                    window.open('<?=URL_BASE?>/request/check/?id=<?=$request['id']?>',"Margem","width=640,height=640");
                                }
                            </script>
                            
                            <button class="btn btn-primary" onclick="a<?=$request['id']?>()">#</button>                                              
                            
                        </td>
                    </tr>
                    <script>
                        $('form[name="update<?=$request['id']?>"]').submit(function (event) {
                            event.preventDefault();                            
                            if ($("#aquii<?=$request['id']?>").val() == 9) {
                                swal({
                                    text: 'Indique a quantidade de itens para cadastro".',
                                    content: "input",
                                    button: {
                                        text: "Enviar",
                                        closeModal: false,
                                    },
                                })
                                .then(name => {
                                    if (!name || name == 0) throw null;
                                        var xhttp = new XMLHttpRequest();
                                        xhttp.open("GET", 'http://lancamentosnfe.bdtecsolucoes.com.br:8090/register?amount='+name+'&id=<?=$request['id']?>',false);
                                        xhttp.send();
                                                    
                                })
                                .then(results => {
                                    return results.json();
                                })
                                .then(json => {
                                    const movie = json.results[0];
                                    if (!movie) {
                                        return swal("No movie was found!");
                                    }
                                    const name = movie.trackName;
                                    const imageURL = movie.artworkUrl100;
                                    swal({
                                        title: "Top result:",
                                        text: name,
                                        icon: imageURL,
                                    });
                                })
                                .catch(err => {
                                    if (err) {
                                        swal("OK!", "Quantidade Registrada.", "success");
                                        $.ajax({
                                            url: '/request/update',
                                            type: 'post',
                                            data: $(this).serialize(),
                                            dataType: 'json',
                                            success: function (response) {
                                                if (response.success === true) {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        icon: "success",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "success"
                                                    });
                                                } else {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        icon: "error",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "danger"
                                                    });
                                                }
                                            }
                                        })
                                    } else {
                                        swal.stopLoading();
                                        swal.close();
                                    }
                                });
                            } else if ($("#aquii<?=$request['id']?>").val() == 3){
                                swal({
                                    title: "Margens anormais, deseja atualizar?",
                                    text: "<?=$request['AG2DETNT2']?>!",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                })
                                .then((willDelete) => {
                                    if (willDelete) {
                                        $.ajax({
                                            url: '/request/update',
                                            type: 'post',
                                            data: $(this).serialize(),
                                            dataType: 'json',
                                            success: function (response) {
                                                if (response.success === true) {
                                                    swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            icon: "success",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "success"
                                                        });
                                                } else {
                                                    swal({
                                                        title: "Bispo & Dantas",
                                                        text: response.message,
                                                        icon: "error",
                                                        showCancelButton: false,
                                                        showConfirmButton: false,
                                                        type: "danger"
                                                    });
                                                }
                                            }
                                        })
                                    }
                                });

                            } else {
                                $.ajax({
                                    url: '/request/update',
                                    type: 'post',
                                    data: $(this).serialize(),
                                    dataType: 'json',
                                    success: function (response) {
                                        if (response.success === true) {
                                            swal({
                                                title: "Bispo & Dantas"+$("#aquii<?=$request['id']?>").val(),
                                                text: response.message,
                                                icon: "success",
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                type: "success"
                                            });
                                        } else {
                                            swal({
                                                title: "Bispo & Dantas",
                                                text: response.message,
                                                icon: "error",
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                type: "danger"
                                            });
                                        }
                                    }
                                })
                            }
                        })                   

                    </script>
                    <?php endforeach;
                    ?>
                </tbody>
            </table>
        </div>
        <div class="alert alert-dark" role="alert">
            <strong>Total de notas:</strong> <?=$k?> <strong>Total de itens:</strong> <?=$items?>
        </div>
    </div>
</main>