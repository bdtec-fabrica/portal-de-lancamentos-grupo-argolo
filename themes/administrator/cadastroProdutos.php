<?php 
session_start();
$v->layout("administrator/adm_theme");

?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Cadastro de Produtos</h1>
        <p class="lead">Cadastro de Produtos do RMS, diversas bases.</p>
    </div>
</div>
<div class="container">
    <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button" data-bs-toggle="collapse"
        data-bs-target="#multiCollapseExample1" aria-expanded="false" aria-controls="multiCollapseExample1">Filtro
        ↴</button>

</div>
<div class="collapse multi-collapse container" id="multiCollapseExample1">
    <form action="/margin/webservice35" method="post">
        <div class="row">
            <div class="form-group col-md-1">
                <span>Referencia</span>
                <input name="reference" class="form-control" type="text">
            </div>
            <div class="form-group col-md-2">
                <span>CNPJ</span>
                <input name="CNPJ" class="form-control" type="text">
            </div>
            <div class="form-group col-md-2">
                <span>EAN</span>
                <input name="EAN" class="form-control" type="text">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-1">
                <button type="submit" class="btn btn-primary">Filtrar</button>
            </div>
        </div>
    </form>
</div>
<div>


    <table  class="table table-hover" style="width:100%">
        <thead>
            <tr>
                <th scope="col">CNPJ</th>
                <th scope="col">Razão Social</th>
                <th scope="col">Nome Fantasia</th>
                <th scope="col">Codigo RMS</th>
                <th scope="col">Referencia</th>
                <th scope="col">Descrição</th>
                <th scope="col">Embalagem Fornecedor</th>
                <th scope="col">Embalagem EAN</th>
                <th scope="col">EAN</th>
                <th scope="col">Base</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                for($j=1;$j<$i;$j++) :
                    ?>
            <tr>
                <td><?=$registros['CNPJ'][$j]?></td>
                <td><?=$registros['razao'][$j]?></td>
                <td><?=$registros['fantasia'][$j]?></td>
                <td><?=$registros['codigo'][$j]?></td>
                <td><?=$registros['referencia'][$j];?></td>
                <td><?=$registros['descricao'][$j]?></td>
                <td><?=$registros['embFornecedor'][$j]?></td>
                <td><?=$registros['embVenda'][$j]?></td>
                <td><?=$registros['EAN'][$j]?></td>
                <td><?=$registros['base'][$j]?></td>
            </tr>
            <?php
                endfor;
            
            ?>
        </tbody>
    </table>
</div>