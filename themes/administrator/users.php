<?php 
session_start();
$v->layout("administrator/adm_theme");
?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Usuários</h1>
        <p class="lead">Usuários legados de atendimento.bispoedantas.com.br</p>
    </div>
</div>
<div style="margin:2%">
    <button type="button" class="btn btn-primary btn-lg" style="margin-left:2%" data-bs-toggle="modal"
        data-bs-target="#adicionar" data-bs-whatever="@mdo">+Adicionar</button>
    <div class="container" style="margin-bottom:5%">
        <form method="post" action="/usuarios/usersSearch" class="row gx-3 gy-2 align-items-center"
            style="margin-bottom:2%">
            <div class="col-sm-2">
                <label class="visually-hidden" for="specificSizeInputName">Loja</label>
                <input name="store" type="text" class="form-control" id="specificSizeInputName" placeholder="Loja">
            </div>
            <div class="col-sm-3">
                <label class="visually-hidden" for="specificSizeSelect">Preference</label>
                <select name="group" class="form-select" id="specificSizeSelect">
                    <option selected value=''>Grupo Economico ...</option>
                    <?php foreach ($storeGroups as $storeGroup):
                ?>
                    <option value="<?=$storeGroup->id?>"><?=$storeGroup->name?></option>
                    <?php endforeach;
                ?>
                </select>
            </div>
            <div class="col-sm-3">
                <label class="visually-hidden" for="specificSizeSelect">Preference</label>
                <select name="class" class="form-select" id="specificSizeSelect">
                    <option selected value=''>Classes de Usúario ...</option>
                    <?php foreach ($userClass as $userClasss):
                ?>
                    <option value="<?=$userClasss->type?>"><?=$userClasss->description?></option>
                    <?php endforeach;
                ?>
                </select>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">Filtrar</button>
            </div>
        </form>
    </div>
    <?php if($users): ?>
    <div style="margin-left:0%;margin-top:2%">
        <h5>Página <?= $paginator->page(); ?> de <?= $paginator->pages(); ?> </h5>
    </div>
    <table class="table table-hover sortable">
        <thead>
            <tr>
                <th style="min-width:25px">ID</th>
                <th style="min-width:25px">Nome</th>
                <th style="min-width:25px">Loja</th>
                <th style="min-width:25px">Email</th>
                <th style="min-width:25px">Classe</th>
                <th style="min-width:25px">Opções</th>
            </tr>
        </thead>
        <tbody>
            <?php 
    foreach ($users as $user):
        ?>
            <tr>
                <td><?=$user->id; ?></td>
                <td><?=$user->name; ?></td>
                <td><?=$user->store_code; ?></td>
                <td><?=$user->email; ?></td>
                <td><?=$user->class; ?></td>
                <td>
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Opções
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                                data-bs-target="#alteraSenha<?=$user->id?>" data-bs-whatever="<?=$user->id?>">Alterar
                                Senha</button>
                            <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                                data-bs-target="#editarCadastro<?=$user->id?>" data-bs-whatever="<?=$user->id?>">Editar
                                Cadastro</button>
                            <?php if ($user->class = 'A'): ?>
                            <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                                data-bs-target="#parametrosLancamentos<?=$user->id?>" data-bs-whatever="<?=$user->id?>">
                                Parametros de Lançamento</button>
                            <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                                data-bs-target="#cargaHoraria<?=$user->id?>" data-bs-whatever="<?=$user->id?>">Registrar
                                Carga Horaria</button>
                            <?php
                            endif; 
                            if ($user->class == "OG" || $user->class == "OG2"):
                                ?>
                            <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                                data-bs-target="#storeGroup<?=$user->id?>" data-bs-whatever="<?=$user->id?>">Parametros
                                de Grupo</button>
                            <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                                data-bs-target="#userSector<?=$user->id?>" data-bs-whatever="<?=$user->id?>">Parametros
                                de Setor</button>
                            <?php endif;
                                ?>
                        </div>
                    </div>
                </td>
            </tr>
            <div class="modal fade" id="parametrosLancamentos<?=$user->id?>" tabindex="-1"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Parametrosde lançamento</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="container card" style="padding:2%">
                                <div class="row">

                                    <div class="col">
                                        <label for="exampleInputEmail1" class="form-label">Parametro</label>
                                    </div>
                                    <div class="col">
                                        <label for="exampleInputEmail1" class="form-label">Conteúdo</label>
                                    </div>
                                    <div class="col">
                                        <label for="exampleInputEmail1" class="form-label"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <form name="parametersOperator<?=$user->id?>">
                                        <div class="col">
                                            <select class="form-control" name="parametro" id="parametro">
                                                <option selected disabled>Selecione ...</option>
                                                <?php foreach ($parameters as $parameter): ?>
                                                <option value="<?=$parameter->id?>"><?=$parameter->name?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <input name="user" value="<?=$user->id?>" type="hidden">
                                            <input type="text" class="form-control" id="conteudo" name="content"
                                                aria-describedby="emailHelp">
                                        </div>
                                        <div class="col">
                                            <button type="submit" id="btnEnviar<?=$user->id?>"
                                                class="btn btn-success">Salvar</button>
                                        </div>
                                    </form>
                                    
                                    <script>
                                        $('#btnEnviar<?=$user->id?>').click(function () {
                                            $("#corpo").append("<div class='row'><div class='col'>" + $(
                                                    '#parametro')
                                                .find(":selected").text() + "</div><div class='col'>" + $(
                                                    '#conteudo').val() + "</div></div>");
                                        });
                                        $('form[name="parametersOperator<?=$user->id?>"]').submit(function (event) {
                                            event.preventDefault();
                                            $.ajax({
                                                url: '/addParameter',
                                                type: 'post',
                                                data: $(this).serialize(),
                                                dataType: 'json',
                                                success: function (response) {

                                                    if (response.success === true) {
                                                        swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            timer: 600,
                                                            icon: "success",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "success"
                                                        });

                                                    } else {
                                                        swal({
                                                            title: "Bispo & Dantas",
                                                            text: response.message,
                                                            icon: "error",
                                                            showCancelButton: false,
                                                            showConfirmButton: false,
                                                            type: "danger"
                                                        });
                                                    }
                                                }
                                            })
                                        });
                                    </script>

                                </div>
                            </div>
                            <div class="container card" style="padding:2%">
                                <div class="row " id="corpo">
                                    <div class="col card">
                                        Parametro
                                    </div>
                                    <div class="col card">
                                        Conteúdo
                                    </div>
                                </div>
                                <?php foreach($user->parameters() as $parameter):?>
                                <div class="row excluir<?=$parameter->id?>" id="corpo" style="padding:1%">
                                    <div class="col">
                                        <?=$parameter->parameterName()?>
                                    </div>
                                    <div class="col">
                                        <?=$parameter->content?>
                                    </div>
                                    <div class="col">
                                        <form name="excluirParametro<?=$parameter->id?>">
                                            <input type="hidden" value="<?=$parameter->id?>" name="id">
                                            <button type="submit" class="btn btn-danger">Excluir</button>
                                        </form>
                                        <script>
                                            $('form[name="excluirParametro<?=$parameter->id?>"]').submit(function (event) {
                                                event.preventDefault();
                                                $.ajax({
                                                    url: '/deleteParameter',
                                                    type: 'post',
                                                    data: $(this).serialize(),
                                                    dataType: 'json',
                                                    success: function (response) {

                                                        if (response.success === true) {
                                                            swal({
                                                                title: "Bispo & Dantas",
                                                                text: response.message,
                                                                timer: 600,
                                                                icon: "success",
                                                                showCancelButton: false,
                                                                showConfirmButton: false,
                                                                type: "success"
                                                            });
                                                            $(".excluir<?=$parameter->id?>").remove();
                                                        } else {
                                                            swal({
                                                                title: "Bispo & Dantas",
                                                                text: response.message,
                                                                icon: "error",
                                                                showCancelButton: false,
                                                                showConfirmButton: false,
                                                                type: "danger"
                                                            });
                                                        }
                                                    }
                                                })
                                            });
                                            
                                        </script>
                                    </div>

                                </div>
                                <?php endforeach; ?>
                            </div>
                            <button class="btn btn-primary" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Codigos dos Tipos,Operações e Grupos Economicos
                            </button>
                            <div class="collapse" id="collapseExample">
                                <div class="row">
                                    <div class="card" style="width: 15rem;">
                                        <div class="card-header">
                                            Tipos
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">1 Avulsa</li>
                                            <li class="list-group-item">2 Compra</li>
                                            <li class="list-group-item">3 Bonificação</li>
                                            <li class="list-group-item">4 CDS</li>
                                            <li class="list-group-item">5 Definir</li>
                                            <li class="list-group-item">6 Horti</li>
                                            <li class="list-group-item">7 Outros</li>
                                            <li class="list-group-item">8 Emissão Própria</li>
                                            <li class="list-group-item">9 Boi (lançamento manual)</li>
                                            <li class="list-group-item">10 Outros Sem Vencimento</li>
                                        </ul>
                                    </div>
                                    <div class="card" style="width: 15rem;">
                                        <div class="card-header">
                                            Operações
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">1 Prioridade</li>
                                            <li class="list-group-item">2 Fáceis</li>
                                            <li class="list-group-item">5 Manual</li>
                                            <li class="list-group-item">6 Media</li>
                                            <li class="list-group-item">7 CD</li>
                                            <li class="list-group-item">8 Dificeis</li>
                                            <li class="list-group-item">9 Parametrizada</li>
                                            <li class="list-group-item">10 Parametrizada/Prioridade</li>
                                            <li class="list-group-item">11 Reportado Cliente</li>
                                            <li class="list-group-item">12 Reportado Operador</li>
                                            <li class="list-group-item">99 Normal</li>
                                            <li class="list-group-item">100 Lançamento sem coletor</li>
                                            <li class="list-group-item">101 Lançamento com coletor</li>
                                            <li class="list-group-item">102 Desbloqueada</li>
                                            <li class="list-group-item">103 Fora de Linha</li>
                                        </ul>
                                    </div>
                                    <div class="card" style="width: 15rem;">
                                        <div class="card-header">
                                            Grupos Economicos
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">1 Mix Bahia</li>
                                            <li class="list-group-item">2 Base BD</li>
                                            <li class="list-group-item">3 Rede Mais</li>
                                            <li class="list-group-item">4 Novo Mix</li>
                                            <li class="list-group-item">5 Unimar</li>
                                            <li class="list-group-item">6 Cesta do Povo</li>
                                            <li class="list-group-item">8 Novo Mix Camaçari</li>
                                            <li class="list-group-item">9 VR</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="cargaHoraria<?=$user->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Cadastrar Carga Horaria</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="semana1<?=$user->id?>-tab" data-bs-toggle="tab"
                                    data-bs-target="#semana1<?=$user->id?>" type="button" role="tab"
                                    aria-controls="semana1<?=$user->id?>" aria-selected="true">Semana 1</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="semana2<?=$user->id?>-tab" data-bs-toggle="tab"
                                    data-bs-target="#semana2<?=$user->id?>" type="button" role="tab"
                                    aria-controls="semana2<?=$user->id?>" aria-selected="false">Semana
                                    2</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="semana3<?=$user->id?>-tab" data-bs-toggle="tab"
                                    data-bs-target="#semana3<?=$user->id?>" type="button" role="tab"
                                    aria-controls="semana3<?=$user->id?>" aria-selected="false">Semana
                                    3</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="semana4<?=$user->id?>-tab" data-bs-toggle="tab"
                                    data-bs-target="#semana4<?=$user->id?>" type="button" role="tab"
                                    aria-controls="semana4<?=$user->id?>" aria-selected="false">Semana
                                    4</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <?php 
                            foreach($parameterWeek as $pW):
                            ?>
                            <div class="tab-pane fade<?php if ($pW->id == 1):?> show active <?php endif;?>"
                                id="semana<?=$pW->id.$user->id?>" role="tabpanel"
                                aria-labelledby="semana<?=$pW->id.$user->id?>-tab">
                                <div class="modal-body">
                                    <form name="updateWorkload<?=$pW->id.$user->id?>">
                                        <input name="week" value="<?=$pW->id?>" type="hidden">
                                        <input name="user" value="<?=$user->id?>" type="hidden">
                                        <?php
                                        foreach ($parameterDayWeek as $pdw):
                                            $parameter = $parameterWorkload->parameter($user->id,$pW->id,$pdw->id);
                                        ?>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox"
                                                id="inlineCheckbox<?=$pdw->id.$pW->id.$user->id?>" value="1" checked>
                                            <label class="form-check-label"
                                                for="inlineCheckbox1"><?=$pdw->name?></label>
                                        </div>
                                        <div class="mb-3">
                                            <div class="container">
                                                <div class="row row-cols-1 row-cols-lg-5 g-1 g-lg-3">
                                                    <div class="col">
                                                        <label for="exampleInputEmail1" class="form-label">HORARIO
                                                            INICIAL</label>
                                                    </div>
                                                    <div class="col">
                                                        <label for="exampleInputEmail1" class="form-label">INTERVALO
                                                            INICIAL </label>
                                                    </div>
                                                    <div class="col">
                                                        <label for="exampleInputEmail1" class="form-label">INTERVALO
                                                            FINAL</label>
                                                    </div>
                                                    <div class="col">
                                                        <label for="exampleInputEmail1" class="form-label">
                                                            HORARIO FINAL</label>
                                                    </div>
                                                    <div class="col">
                                                        <label for="exampleInputEmail1" class="form-label">
                                                            TOTAL DIA</label>
                                                    </div>
                                                    <div class="col">
                                                        <input type="text" class="form-control"
                                                            id="horaInical<?=$pdw->id.$pW->id.$user->id?>"
                                                            name="horaInical<?=$pdw->id.$pW->id.$user->id?>"
                                                            aria-describedby="emailHelp"
                                                            value="<?=$parameter->start_time?>">
                                                    </div>
                                                    <div class="col">
                                                        <input type="text" class="form-control"
                                                            id="intervaloInicial<?=$pdw->id.$pW->id.$user->id?>"
                                                            name="intervaloInicial<?=$pdw->id.$pW->id.$user->id?>"
                                                            aria-describedby="emailHelp"
                                                            value="<?=$parameter->start_break?>">
                                                    </div>
                                                    <div class="col">
                                                        <input type="text" class="form-control"
                                                            id="intervaloFinal<?=$pdw->id.$pW->id.$user->id?>"
                                                            name="intervaloFinal<?=$pdw->id.$pW->id.$user->id?>"
                                                            aria-describedby="emailHelp"
                                                            value="<?=$parameter->end_break?>">
                                                    </div>
                                                    <div class="col">
                                                        <input type="text" class="form-control"
                                                            id="horaFinal<?=$pdw->id.$pW->id.$user->id?>"
                                                            name="horaFinal<?=$pdw->id.$pW->id.$user->id?>"
                                                            aria-describedby="emailHelp"
                                                            value="<?=$parameter->end_time?>">
                                                    </div>
                                                    <div class="col">
                                                        <input type="text" class="form-control"
                                                            id="totalDia<?=$pdw->id.$pW->id.$user->id?>"
                                                            aria-describedby="emailHelp" value="<?php if ($parameter->start_time):
                                                            echo somarHoras((subtrairHoras($parameter->start_break,$parameter->start_time)),(subtrairHoras($parameter->end_time,$parameter->end_break)));
                                                                endif;?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                    </form>
                                </div>
                            </div>
                            <script>
                                $('form[name="updateWorkload<?=$pW->id.$user->id?>"]').submit(function (event) {
                                    event.preventDefault();
                                    $.ajax({
                                        url: '/updateWorkLoad',
                                        type: 'post',
                                        data: $(this).serialize(),
                                        dataType: 'json',
                                        success: function (response) {

                                            if (response.success === true) {
                                                swal({
                                                    title: "Bispo & Dantas",
                                                    text: response.message,
                                                    timer: 600,
                                                    icon: "success",
                                                    showCancelButton: false,
                                                    showConfirmButton: false,
                                                    type: "success"
                                                });

                                            } else {
                                                swal({
                                                    title: "Bispo & Dantas",
                                                    text: response.message,
                                                    icon: "error",
                                                    showCancelButton: false,
                                                    showConfirmButton: false,
                                                    type: "danger"
                                                });
                                            }
                                        }
                                    })
                                });
                            </script>
                            <?php endforeach; ?>

                        </div>

                    </div>
                </div>
            </div>
            <?php
            foreach (array(1,2,3,4) as $i):
            ?>
            <script>
                $(document).ready(function () {
                    if ($("#inlineCheckbox1<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical1<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial1<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal1<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal1<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia1<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical1<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial1<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal1<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal1<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia1<?=$i.$user->id?>").prop("disabled", false);
                    }

                    if ($("#inlineCheckbox2<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical2<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial2<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal2<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal2<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia2<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical2<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial2<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal2<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal2<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia2<?=$i.$user->id?>").prop("disabled", false);
                    }

                    if ($("#inlineCheckbox3<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical3<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial3<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal3<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal3<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia3<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical3<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial3<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal3<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal3<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia3<?=$i.$user->id?>").prop("disabled", false);
                    }

                    if ($("#inlineCheckbox4<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical4<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial4<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal4<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal4<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia4<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical4<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial4<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal4<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal4<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia4<?=$i.$user->id?>").prop("disabled", false);
                    }

                    if ($("#inlineCheckbox5<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical5<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial5<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal5<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal5<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia5<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical5<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial5<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal5<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal5<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia5<?=$i.$user->id?>").prop("disabled", false);
                    }

                    if ($("#inlineCheckbox6<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical6<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial6<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal6<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal6<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia6<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical6<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial6<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal6<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal6<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia6<?=$i.$user->id?>").prop("disabled", false);
                    }

                    if ($("#inlineCheckbox7<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical7<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial7<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal7<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal7<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia7<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical7<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial7<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal7<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal7<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia7<?=$i.$user->id?>").prop("disabled", false);
                    }
                });
                $("#inlineCheckbox1<?=$i.$user->id?>").click(function () {

                    if ($("#inlineCheckbox1<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical1<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial1<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal1<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal1<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia1<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical1<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial1<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal1<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal1<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia1<?=$i.$user->id?>").prop("disabled", false);
                    }


                });
                $("#inlineCheckbox2<?=$i.$user->id?>").click(function () {

                    if ($("#inlineCheckbox2<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical2<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial2<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal2<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal2<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia2<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical2<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial2<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal2<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal2<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia2<?=$i.$user->id?>").prop("disabled", false);
                    }


                });
                $("#inlineCheckbox3<?=$i.$user->id?>").click(function () {

                    if ($("#inlineCheckbox3<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical3<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial3<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal3<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal3<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia3<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical3<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial3<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal3<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal3<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia3<?=$i.$user->id?>").prop("disabled", false);
                    }


                });
                $("#inlineCheckbox4<?=$i.$user->id?>").click(function () {

                    if ($("#inlineCheckbox4<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical4<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial4<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal4<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal4<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia4<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical4<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial4<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal4<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal4<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia4<?=$i.$user->id?>").prop("disabled", false);
                    }


                });
                $("#inlineCheckbox5<?=$i.$user->id?>").click(function () {

                    if ($("#inlineCheckbox5<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical5<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial5<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal5<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal5<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia5<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical5<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial5<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal5<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal5<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia5<?=$i.$user->id?>").prop("disabled", false);
                    }


                });
                $("#inlineCheckbox6<?=$i.$user->id?>").click(function () {

                    if ($("#inlineCheckbox6<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical6<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial6<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal6<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal6<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia6<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical6<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial6<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal6<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal6<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia6<?=$i.$user->id?>").prop("disabled", false);
                    }


                });
                $("#inlineCheckbox7<?=$i.$user->id?>").click(function () {

                    if ($("#inlineCheckbox7<?=$i.$user->id?>").prop('checked') == false) {
                        $("#horaInical7<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloInicial7<?=$i.$user->id?>").prop("disabled", true);
                        $("#intervaloFinal7<?=$i.$user->id?>").prop("disabled", true);
                        $("#horaFinal7<?=$i.$user->id?>").prop("disabled", true);
                        $("#totalDia7<?=$i.$user->id?>").prop("disabled", true);
                    } else {
                        $("#horaInical7<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloInicial7<?=$i.$user->id?>").prop("disabled", false);
                        $("#intervaloFinal7<?=$i.$user->id?>").prop("disabled", false);
                        $("#horaFinal7<?=$i.$user->id?>").prop("disabled", false);
                        $("#totalDia7<?=$i.$user->id?>").prop("disabled", false);
                    }


                });
            </script>

            <?php
            endforeach;
            ?>
            <div class="modal fade" id="userSector<?=$user->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Parametros de Setor - <?=$user->getLogin?>
                            </h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="userSector<?=$user->id?>">
                                <div class="form-group">
                                    <input type="hidden" name="id" id="id" value="<?=$user->id?>">
                                </div>
                                <div class="form-group">
                                    <?php foreach ($sectors as $sector):
                                    if ($user->validateSector($sector->id)):
                                        $check = "checked";
                                    else:
                                        $check = "";
                                    endif;
                                    ?>
                                    <div class="form-check">
                                        <input class="form-check-input" name="<?=$sector->id?>" type="checkbox"
                                            value="1" id="flexCheckDefault" <?=$check?>>
                                        <label class="form-check-label" for="flexCheckDefault">
                                            <?=$sector->name?>
                                        </label>
                                    </div>
                                    <?php endforeach;
                                    ?>
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-success">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="storeGroup<?=$user->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Parametros de Grupo - <?=$user->getLogin?>
                            </h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="storeGroup<?=$user->id?>">
                                <div class="form-group">
                                    <input type="hidden" name="id" id="id" value="<?=$user->id?>">
                                </div>
                                <div class="form-group">
                                    <?php foreach ($storeGroups as $storeGroup):
                                    if ($user->validateGroup($storeGroup->id)):
                                        $check = "checked";
                                    else:
                                        $check = "";
                                    endif;
                                    ?>
                                    <div class="form-check">
                                        <input class="form-check-input" name="<?=$storeGroup->id?>" type="checkbox"
                                            value="1" id="flexCheckDefault" <?=$check?>>
                                        <label class="form-check-label" for="flexCheckDefault">
                                            <?=$storeGroup->name?>
                                        </label>
                                    </div>
                                    <?php endforeach;
                                    ?>
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-success">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editarCadastro<?=$user->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="editar<?=$user->id?>">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Nome:</label>
                                    <input type="text" id="current-password" class="form-control" name="name"
                                        autocomplete="on" id="recipient-name" value="<?=$user->name?>">
                                    <input type="hidden" name="id" id="id" value="<?=$user->id?>">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Email:</label>
                                    <input type="email" value="<?=$user->email?>" class="form-control" name="email"
                                        autocomplete="on">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Telefone:</label>
                                    <input type="tel" id="login" value="<?=$user->tell?>" class="form-control"
                                        name="tel" pattern="[0-9]{11}" required>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Loja:</label>
                                    <input type="text" value="<?=$user->store_code?>" class="form-control"
                                        name="storeCode" autocomplete="on">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Admissão:</label>
                                    <input type="date" value="<?=$user->admissao?>" class="form-control" name="admissao"
                                        autocomplete="on">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Realoca NF:</label>
                                    <select class="form-control" name="libera" id="">
                                        <option value="<?=$user->libera?>"><?=$user->libera?></option>
                                        <?php if ($user->libera == "N"): ?>
                                        <option value="S">S</option>
                                        <?php else: ?>
                                        <option value="N">N</option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Grupo:</label>
                                    <select name="group" class="form-control" id="inputGroupSelect01">
                                        <option value="<?=$user->store_group_id?>"><?=$user->getGroup()->name?>
                                        </option>
                                        <?php
                                            foreach ($storeGroups as $storeGroup):
                                                ?>
                                        <option value="<?=$storeGroup->id?>"><?=$storeGroup->name?></option>
                                        <?php 
                                            endforeach;
                                            ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Tipo de Usúario</label>
                                    <select name="class" class="form-control" id="inputGroupSelect01">
                                        <option value="<?=$user->class?>"><?=$user->getUserClass()->description?>
                                        </option>
                                        <?php
                                            foreach ($userClass as $userClasss):
                                                ?>
                                        <option value="<?=$userClasss->type?>"><?=$userClasss->description?></option>
                                        <?php 
                                            endforeach;
                                            ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" name="whatsapp" type="checkbox" value="T"
                                            id="flexCheckDefault" <?php if ($user->whatsapp == "T"):
                                                echo "checked";
                                               endif;
                                        ?>>
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Atendimento Whatsapp
                                        </label>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-success">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="alteraSenha<?=$user->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nova Senha</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="alterar<?=$user->id?>">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Senha antual:</label>
                                    <input type="password" id="current-password" class="form-control"
                                        name="current-password" autocomplete="on">
                                    <input type="hidden" name="id" value="<?=$user->id?>">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Nova senha:</label>
                                    <input type="password" class="form-control" name="new-password" autocomplete="on">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Repita Nova senha:</label>
                                    <input type="password" class="form-control" name="repeat-new-password"
                                        autocomplete="on">
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $('form[name="userSector<?=$user->id?>"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/usuarios/userSector',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }
                        }
                    })
                });
                $('form[name="editar<?=$user->id?>"]').submit(function (event) {
                    event.preventDefault();

                    $.ajax({
                        url: '/usuarios/userEdit',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });

                $('form[name="storeGroup<?=$user->id?>"]').submit(function (event) {
                    event.preventDefault();

                    $.ajax({
                        url: '/usuarios/storeGroupsParameters',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
                $('form[name="alterar<?=$user->id?>"]').submit(function (event) {
                    event.preventDefault();

                    $.ajax({
                        url: '/usuarios/usersUpdatePassword',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>

            <?php
    endforeach; ?>
        </tbody>
    </table>
    <div style="margin-bottom:5%">
        <?= $paginator->render(); ?>
    </div>
    <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Novo Usuário</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="adicionar">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Loja:</label>
                            <input type="text" id="store" class="form-control" name="store" required>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nome Completo</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Email:</label>
                            <input type="email" id="login" class="form-control" name="login" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Telefone:</label>
                            <input type="tel" id="login" placeholder="(xx)xxxxx-xxxx" class="form-control" name="tel"
                                pattern="[0-9]{11}" required>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Grupo</label>
                            <select name="storeGroup" class="form-control" required placeholder="Selecione ...">
                                <option value="" disabled selected hidden>Selecione ...</option>
                                <?php foreach ($storeGroups as $storeGroup):
                if ($storeGroup->store == 'N'):
                  ?>
                                <option value="<?=$storeGroup->id?>"><?=$storeGroup->name?></option>
                                <?php 
                endif;
                endforeach;
                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Senha</label>
                            <input type="password" class="form-control" name="password" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-success">Salvar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php
endif; ?>

    <script>
        $('form[name="adicionar"]').submit(function (event) {
            event.preventDefault();

            $.ajax({
                url: '/usuarios/usersAdd',
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function (response) {

                    if (response.success === true) {
                        swal({
                            title: "Bispo & Dantas",
                            text: response.message,
                            timer: 600,
                            icon: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            type: "success"
                        });
                        document.location.reload(true);
                    } else {
                        swal({
                            title: "Bispo & Dantas",
                            text: response.message,
                            icon: "error",
                            showCancelButton: false,
                            showConfirmButton: false,
                            type: "danger"
                        });
                    }

                }
            })
        });

        $('#alteraSenha').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Botão que acionou o modal
            var recipient = button.data('whatever') // Extrai informação dos atributos data-bs-*
            // Se necessário, você pode iniciar uma requisição AJAX aqui e, então, fazer a atualização em um callback.
            // Atualiza o conteúdo do modal. Nós vamos usar jQuery, aqui. No entanto, você poderia usar uma biblioteca de data binding ou outros métodos.
            var modal = $(this)
            modal.find('.modal-body input#id').val(recipient)
        });
    </script>