<?php 
session_start();
$v->layout("administrator/adm_theme");

?>

<table class='table'>
    <thead>
        <tr>
            <th>Referencia</th>
            <th>EAN</th>
            <th>Descrição</th>
            <th>Valor Anterior RMS</th>
            <th>NF Valor Anterior RMS</th>
            <th>Data Valor Anterior RMS</th>
            <th>Valor da Nota RMS</th>
            <th>% Diferença Valor RMS</th>
            <th>Número da Nota</th>
            <th>Valor Anterior Portal</th>
            <th>Valor da Nota Portal</th>
            <th>% Diferença valor Portal</th>
        </tr>
    </thead>
    <tbody>
        <?php
         for ($i=1;$i<$count;$i++) :
            ?>
        <tr>
            <td><?=$registros['codigo'][$i]?></td>
            <td><?=$registros['ean'][$i]?></td>
            <td><?=$registros['descricao'][$i]?></td>
            <td><?=$registros['custoRMS'][$i]?></td>
            <td><?=$registros['dtUltimaEntradaAG1IENSA'][$i]?></td>
            <td><?=$registros['custoLancamento'][$i]?></td>
            <td><?=$registros['nFUltimaEntradaAG1IENSA'][$i]?></td>
            <td class='table-dark'
                <?php if($registros['diferencaCustoRMS'][$i] <>  $registros['diferencaCustoPortal'][$i]): ?>
                    style="color:red"
                <?php endif; ?>
            ><?=$registros['diferencaCustoRMS'][$i]?></td>
            <td><?=$registros['nfAnterior'][$i]?></td>
            <td><?=$registros['custoAnterior'][$i]?></td>
            <td><?=$registros['custoAtual'][$i]?></td>
            <td class='table-dark'<?php if($registros['diferencaCustoRMS'][$i] <>  $registros['diferencaCustoPortal'][$i]): ?>
                    style="color:red"
                <?php endif; ?>
            ><?=$registros['diferencaCustoPortal'][$i]?></td>
        </tr>
        <?php endfor;
    ?>
    </tbody>
</table>