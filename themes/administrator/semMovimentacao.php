<?php 
session_start();
$v->layout("administrator/adm_theme");
?>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<main class="main_content">
  <div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
      <h1 class="display-4">Itens sem Movimentação</h1>
      <p class="lead">Itens na linha da loja e sem movimentação.</p>
    </div>
  </div>
  <div class="container" style="margin-bottom:5%">
    <form name="buscar" id="buscar" method="post">
      <input name="store" id="target2" type="hidden">
    </form>
    <div class="row">
      <form method="post" action="/movimentacao/report" class="row gx-3 gy-2 align-items-center"
        style="margin-bottom:2%">
        <div class="row g-3">
          <div class="col-sm-2">
            <input name="store" id="target" type="text" class="form-control valor" id="specificSizeInputName"
              placeholder="Loja" data-bs-toggle="tooltip" data-bs-html="true" title="Lojas Novo Mix utilizar com digito as demais utilizar sem digito.">
          </div>
          <div class="col-sm-2">
            <input name="data1" type="date" class="form-control">
          </div>
          <div class="col-sm-4">
            <h5 style="color:red">Para Lojas Novo Mix utilizar codigo com digito, Para demais utilizar codigo sem digito.</h5>
          </div>
        </div>
        <div class="col-auto">
          <button type="submit" class="btn btn-primary">Buscar</button>
        </div>

      </form>
    </div>
  </div>
  <table class="table table-hover sortable" style="text-align:justify">
    <thead>
      <tr>
        <th >Loja</th>
        <th >Codigo</th>
        <th >EAN</th>
        <th >Descrição</th>
        <th >Ultima Entrada</th>
        <th >Ultima Saida</th>
        <th >Quantidade Estoque </th>
        <th >Custo Liquido</th>
        <th >Preço</th>
      </tr>
    </thead>
    <tbody>
      <?php 
    foreach ($aa2cestq as $linhp):
        if ($linhp->getAA1LINHP):
        ?>
      <tr class="item">
        <td><?=$linhp->GET_COD_LOCAL?></td>
        <td><?=$linhp->GET_COD_PRODUTO?></td>
        <td><?=$linhp->getAA3CITEM()->GIT_CODIGO_EAN13?></td>
        <td><?=$linhp->getAA3CITEM()->GIT_DESCRICAO?></td>
        <td><?=$linhp->dtUltEnt?></td>
        <td><?=$linhp->dtUltFat?></td>
        <td><?=str_replace('.',',',$linhp->GET_ESTOQUE)?></td>
        <td><?=str_replace('.',',',$linhp->GET_CUS_ULT_ENT)?></td>
        <td><?=str_replace('.',',',$linhp->getPrice())?></td>
      </tr>
      <?php
        endif;
    endforeach; ?>
    </tbody>
  </table>

  <script>
    // População da lista
    const data = Array.from({
      length: 50
    }).map(
      (_, i) => `<item"> ${i + 1}`
    );
    /* aqui ultilizamos o Array.from(metodo) para transformar ArrayLike ou nodeList numa Array.
e o map irá fzer um loop para cada item do array retornando o posicinamento de 1 á 100 */

    // Controles
    const html = {
      get(element) {
        return document.querySelector(element);
      },
    };

    let perPage = 5;
    const state = {
      page: 1, //pagina atual
      perPage,
      totalPage: Math.ceil(data.length /
      perPage), //temos 100 posições dividido pelo quantidade de elementos por pagina,q no caso é 5,sendo assim teremos 20 paginas.
      maxVisibleButtons: 5,
    };
    const controls = {
      next() {
        state.page++;

        const lastPage = state.page > state.totalPage;
        if (lastPage) {
          state.page--;
        }
      },
      prev() {
        state.page--;

        if (state.page < 1) {
          state.page++;
        }
      },
      goTo(page) {
        if (page < 1) {
          page = 1;
        }

        state.page = +page;
        if (page > state.totalPage) {
          state.page = state.totalPage;
        }
      },

      //createListeners() sua funcionalidade  ira procurar um elemento que foi clicado e irá adicionar um evento nele
      createListeners() {
        html.get(".first").addEventListener("click", () => {
          controls.goTo(1);
          update();
        });
        html.get(".last").addEventListener("click", () => {
          controls.goTo(state.totalPage);
          update();
        });
        html.get(".next").addEventListener("click", () => {
          controls.next();
          update();
        });
        html.get(".prev").addEventListener("click", () => {
          controls.prev();
          update();
        });
      },
    };


    const buttons = {
      element: html.get("#paginate .numbers"),
      create(number) {
        const button = document.createElement("div");

        button.innerHTML = number;

        if (state.page === number) {
          button.classList.add("active");
        }

        button.addEventListener("click", (event) => {
          const page = event.target.innerText

          controls.goTo(page);
          update();
        });

        buttons.element.appendChild(button);
      },
      update() {
        buttons.element.innerHTML = "";
        const {
          maxLeft,
          maxRight
        } = buttons.calculatemaxVisible();
        for (let page = maxLeft; page <= maxRight; page++) {
          buttons.create(page);
        }
      },
      calculatemaxVisible() {
        const {
          maxVisibleButtons
        } = state;
        let maxLeft = state.page - Math.floor(maxVisibleButtons / 2);
        let maxRight = state.page + Math.floor(maxVisibleButtons / 2);
        if (maxLeft < 1) {
          maxLeft = 1;
          maxRight = maxVisibleButtons;
        }

        if (maxRight > state.totalPage) {
          maxLeft = state.totalPage - (maxVisibleButtons - 1);
          maxRight = state.totalPage;

          if (maxRight < 1) maxLeft = 1
        }
        return {
          maxLeft,
          maxRight
        };
      },
    };

    function update() {
      list.update();
      buttons.update();
    }

    function init() {
      update();
      controls.createListeners();
    }

    init();
  </script>
</main>