<?php 
$v->layout("operator/_theme");
?>
<div class="bg-light p-5" style="margin-bottom:5%">
    <div class="container text-center">
        <h1 class="display-4">Bloco H</h1>
        <p class="lead">Importação, edição e geração do Bloco H.</p>
    </div>
</div>
<div class="container" style="margin-bottom:5%">
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add">+ Incluir
        Estoque</button>
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#gerar">+ Gerar Bloco
        H</button>
</div>
<div class="container">
    <form class="row gy-2 gx-3 align-items-center" method="post" action="/blocoH/search">
        <div class="col-auto">
            <label class="visually-hidden" for="autoSizingInput">Loja</label>
            <input type="text" name="store" class="form-control" id="autoSizingInput" placeholder="Loja" value="<?=$store?>">
        </div>
        <div class="col-auto">
            <label class="visually-hidden" for="autoSizingInput">Ano</label>
            <input type="number" name="year" class="form-control" id="autoSizingInput" placeholder="Ano" value="<?=$year?>">
        </div>
        <div class="col-auto">
            <label class="visually-hidden" for="autoSizingInput">Codigo</label>
            <input type="number" name="code" class="form-control" id="autoSizingInput" placeholder="Codigo">
        </div>
        <div class="col-auto" style="margin-right:10%">
            <button type="submit" class="btn btn-primary">Procurar</button>
        </div>
    </form>

</div>
<div class="modal fade" id="gerar" tabindex="-1" aria-labelledby="gerarLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="gerarLabel">Gerar Bloco H</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form name="formGerar" id="formGerar" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input required name="store" type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input required name="year" type="number" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Conta :</label>
                        <input required name="cta" type="number" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Gerar Custo :</label>
                        <select required name="cost" class="form-control">
                        <option value="S">Sim</option>
                        <option value="N">Não</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin-top:2%">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="file" accept=".txt" type="file" class="form-control" id="inputGroupFile01">
                        </div>
                    </div>
                    <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                        id="porcentagemGerar">0%</span>
                    <div id="respostaGerar" style="width:100%"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" id="btnGerar" class="btn btn-primary">Enviar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#btnGerar').click(function () {
            $('#formGerar').ajaxForm({
                uploadProgress: function (event, position, total, percentComplete) {
                    $('progress').attr('value', percentComplete);
                    $('#porcentagemGerar').html(percentComplete + '%');
                },
                success: function (data) {
                    $('progress').attr('value', '100');
                    $('#porcentagemGerar').html('100%');
                    if (data.sucesso == true) {
                        $('#respostaGerar').html('<div class="alert bg-success" role="alert">' +
                            data.msg + '</div>');
                        document.location.reload(true);
                    } else {
                        $('#respostaGerar').html('<div class="alert bg-danger" role="alert">' +
                            data.msg + '</div>');
                    }
                },
                error: function () {
                    $('#respostaGerar').html(
                        '<div class="alert bg-danger" role="alert">Não foi possivel realizar o upload.</div>'
                    );
                },
                dataType: 'json',
                url: '/blocoH/gerar',
                resetForm: true
            }).submit();
        })
    });
</script>
<div class="modal fade" id="add" tabindex="-1" aria-labelledby="addLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addLabel">Gravar Estoque</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form name="formUpload" id="formUpload" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input required name="store" type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input required name="year" type="number" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group" style="margin-top:2%">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="file" accept=".csv" type="file" class="form-control" id="inputGroupFile01">
                        </div>
                    </div>
                    <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                        id="porcentagem">0%</span>
                    <div id="resposta" style="width:100%"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#btnEnviar').click(function () {
            $('#formUpload').ajaxForm({
                uploadProgress: function (event, position, total, percentComplete) {
                    $('progress').attr('value', percentComplete);
                    $('#porcentagem').html(percentComplete + '%');
                },
                success: function (data) {
                    $('progress').attr('value', '100');
                    $('#porcentagem').html('100%');
                    if (data.sucesso == true) {
                        $('#resposta').html('<div class="alert bg-success" role="alert">' +
                            data.msg + '</div>');
                        // document.location.reload(true);
                    } else {
                        $('#resposta').html('<div class="alert bg-danger" role="alert">' +
                            data.msg + '</div>');
                    }
                },
                error: function () {
                    $('#resposta').html(
                        '<div class="alert bg-danger" role="alert">Não foi possivel realizar o upload.</div>'
                    );
                },
                dataType: 'json',
                url: '/blocoH/add',
                resetForm: true
            }).submit();
        })
    });
</script>


<?php if ($paginator): 
 ?>
<div class="container" style="margin-top:2%;margin-bottom:2%">
    <h5 style="margin-left:2%;">Página <?= $paginator->page(); ?> de <?= $paginator->pages(); ?> </h5>
</div>
<?php endif;
?>
<div class="container" style="margin-bottom:5%">

    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">Loja</th>
                <th scope="col">Codigo</th>
                <th scope="col">Descrição</th>
                <th scope="col">Quantidade</th>
                <th scope="col">Custo</th>
                <th scope="col">Ano</th>
            </tr>
        </thead>
        <tbody>
            <form id="form" method="get">
                <?php if ($blocoH):
             foreach($blocoH as $registro): ?>
                <tr>

                    <th scope="row"><?=$registro->store_id?></th>
                    <th scope="row"><?=$registro->code?></th>
                    <th scope="row"><?=$registro->description?></th>
                    <td id="<?=$registro->id."-amount"?>"><?=$registro->amount?></td>
                    <td id="<?=$registro->id."-cost"?>"><?=$registro->cost?></td>
                    <th scope="row"><?=$registro->year?></th>

                </tr>
                <?php endforeach;
            endif; ?>
            </form>
        </tbody>
    </table>
    <div class="col-md-3">
        <label for="autoSizingInput">Total Inventario:</label>
        <input class="form-control" value="<?=$total?>" disabled>
    </div>
    <?php if ($paginator):
    ?>
    <div style="margin-left:2%;margin-bottom:5%"><?= $paginator->render();?><div>
            <?php endif;
    ?>
        </div>

        <script>
            $(function () {
                $("td").dblclick(function () {
                    var conteudoOriginal = $(this).text();
                    var id = $(this).attr("id");

                    $(this).addClass("celulaEmEdicao");
                    $(this).html("<input type='text' name='" + id + "' class='form-control' value='" +
                        conteudoOriginal + "' />");
                    $(this).children().first().focus();

                    $(this).children().first().keypress(function (e) {
                        if (e.which == 13) {

                            var novoConteudo = $(this).val();
                            $(this).parent().text(novoConteudo);
                            $(document).ready(function () {
                                $('#form').ajaxForm({

                                    success: function (data) {
                                        if (data.sucesso == true) {
                                            $(this).parent().removeClass(
                                                "celulaEmEdicao");
                                            document.location.reload(true);
                                        }
                                    },
                                    dataType: 'json',
                                    url: '/blocoH/edit?data=' + id + '$' +
                                        novoConteudo,
                                    resetForm: true
                                }).submit();
                            })

                        }
                    });

                    $(this).children().first().blur(function () {


                        $(this).parent().text(conteudoOriginal);
                        $(this).parent().removeClass("celulaEmEdicao");

                    });
                });
            });
        </script>