<?php 
session_start();
$v->layout("administrator/adm_theme");
?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Troca Embalagens</h1>
        <p class="lead">Parâmetro de troca embalagens do XML</p>
    </div>
</div>
<div style="margin:2%">
    <button type="button" class="btn btn-primary btn-lg" style="margin-left:2%" data-bs-toggle="modal"
        data-bs-target="#adicionar" data-bs-whatever="@mdo">+Adicionar</button>
</div>
<div class="container" style="margin-bottom:5%">
    <form method="post" action="/packagingCode/search" class="row gx-3 gy-2 align-items-center" style="margin-bottom:2%">
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeInputName">Loja</label>
            <input name="storeId" type="text" class="form-control" id="specificSizeInputName" placeholder="Loja">
        </div>
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeInputName">CNPJ</label>
            <input name="cnpj" type="text" class="form-control" id="specificSizeInputName" placeholder="CNPJ">
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Buscar</button>
        </div>
    </form>
</div>
<?php if($packagingCode): ?>
<div style="margin-left:0%;margin-top:2%">
    <h5>Página <?= $paginator->page(); ?> de <?= $paginator->pages(); ?> </h5>
</div>
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th>Loja</th>
            <th>CNPJ</th>
            <th>Codigo</th>
            <th>Embalagem Origem</th>
            <th>Embalagem Destino</th>
            <th>Quantidade Destino</th>
            <th>Opções</th>
            
        </tr>
    </thead>
    <tbody>
        <?php 
    foreach ($packagingCode as $code):
        ?>
        <tr>
            <td><?=$code->store_id ?></td>
            <td><?=$code->cnpj?></td>
            <td><?=$code->code?></td>
            <td><?=$code->packing_of?></td>
            <td><?=$code->packing_for?></td>
            <td><?=$code->quantity_for?></td>
            <td>
                <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                    <form name="excluir<?=$code->id?>">
                        <input type="hidden" name="id" value="<?=$code->id?>">
                        <button title="Excluir" type="submit" data-bs-placement="top" title="Excluir" class="btn btn-danger"><i
                                class="ion ion-ios-trash"></i></button>
                    </form>
                        <button title="Editar" type="button" class="btn btn-warning" data-bs-toggle="modal"
                            data-bs-target="#editar<?=$code->id?>" data-bs-whatever="<?=$code->id?>"><i
                                class="ion ion-edit"></i></button>
                </div>
            </td>
        </tr>
        <div class="modal fade" id="editar<?=$code->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar Troca Código</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="editar<?=$code->id?>">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Codigo Origem:</label>
                                <input type="text" class="form-control" name="sourceCode"
                                    value="<?=$code->source_code;?>">
                                <input type="hidden" name="id" id="id" value="<?=$code->id?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Codigo Destino:</label>
                                <input type="text" class="form-control" name="destinationCode" autocomplete="on"
                                    id="recipient-name" value="<?=$code->destination_code;?>">
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('form[name="excluir<?=$code->id?>"]').submit(function (event) {
                event.preventDefault();
                var x;
                var r = confirm(
                    "Excluir parametrização ?"
                );
                if (r == true) {
                    $.ajax({
                        url: '/packagingCode/delete',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 9999999999999999999999999,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });

                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                    document.location.reload(true);
                }
            });
        </script>
        <script>
            $('form[name="editar<?=$code->id?>"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/packagingCode/edit',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
        <?php
    endforeach; ?>
    </tbody>
</table>
<div style="margin-bottom:5%">
    <?= $paginator->render(); ?>
</div>
<div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Novo troca embalagem</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="adicionar">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" id="store" class="form-control" name="store" required>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">CNPJ</label>
                        <input type="text" class="form-control" name="cnpj" required>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Codigo</label>
                        <input type="text" class="form-control" name="code" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Embalagem Origem:</label>
                        <input type="text" id="login" class="form-control" name="sourcePacking" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Embalagem Origem:</label>
                        <input type="text" id="login" class="form-control" name="destinationPacking" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Quantidade:</label>
                        <input type="text" id="login" class="form-control" name="amount" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-success">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
endif; ?>

<script>
    $('form[name="adicionar"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/packagingCode/add',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.success === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.message,
                        timer: 600,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.message,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });

    $('#alteraSenha').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Botão que acionou o modal
        var recipient = button.data('whatever') // Extrai informação dos atributos data-bs-*
        // Se necessário, você pode iniciar uma requisição AJAX aqui e, então, fazer a atualização em um callback.
        // Atualiza o conteúdo do modal. Nós vamos usar jQuery, aqui. No entanto, você poderia usar uma biblioteca de data binding ou outros métodos.
        var modal = $(this)
        modal.find('.modal-body input#id').val(recipient)
    })
</script>