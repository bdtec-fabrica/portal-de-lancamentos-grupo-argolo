<?php 
$v->layout("administrator/adm_theme");
?>

<div class="bg-light p-5" style="margin-bottom:5%">
    <div class="container text-center">
        <h1 class="display-4">SLA - Chamados</h1>
        <p class="lead">Relatorio SLA dos chamados atendidos.</p>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-12 col-sm-6 col-md-10">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tempo medio de conclusão</span>
                        <span class="info-box-number">
                            <?=$totalTime?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
</section>
<section class="content">
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
        <?php foreach($stories as $store):
        ?>
            <div class="col-12 col-sm-6 col-md-10">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tempo medio de conclusão  Loja: <?=$store?></span>
                        <span class="info-box-number">
                            <?=$totalTimeStore[$store]?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <?php endforeach;
            ?>
            <!-- /.col -->
        </div>
        <!-- /.row -->
</section>
<section class="content">
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
        <?php foreach($sectors as $sector):
        ?>
            <div class="col-12 col-sm-6 col-md-10">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tempo medio de conclusão  Setor: <?=$sector?></span>
                        <span class="info-box-number">
                            <?=$totalTimeSectors[$sector]?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <?php endforeach;
            ?>
            <!-- /.col -->
        </div>
        <!-- /.row -->
</section>
<section class="content">
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
        <?php foreach($operators as $operator):
        ?>
            <div class="col-12 col-sm-6 col-md-10">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tempo medio de conclusão  Operador: <?=$operator?></span>
                        <span class="info-box-number">
                            <?=$totalTimeOperators[$operator]?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <?php endforeach;
            ?>
            <!-- /.col -->
        </div>
        <!-- /.row -->
</section>

<?php 


                // "stores" => $stores,
                // "storeGroups" => $storeGroups,
                // "totalTime" => $totalTime,
                // "totalTimeStore" =>$totalTimeStore,
                // "totalTimeOperators" => $totalTimeOperators,
                // "totalTimeSectors" => $totalTimeSectors,
                // "stories" => $stories,
                // "operators" => $operators,
                // "sectors" => $sectors
?>