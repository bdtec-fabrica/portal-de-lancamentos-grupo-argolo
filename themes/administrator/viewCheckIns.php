<?php 

$v->layout("administrator/adm_theme");


?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Marcação diaria de ponto</h1>
        <p class="lead"></a></p>
    </div>
</div>
<div>
    <form class="row g-3" action="/searchViewCheckIns" method="POST">
        <div class="col-auto">
            <select class="form-control" name="operator">
                <option value="">Todos os Operadores</option>
                <?php foreach($operators as $operator):
                ?>
                <option value="<?= $operator->id?>"><?=$operator->name?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-auto">
            <label for="inputPassword2" class="visually-hidden">Dia</label>
            <input type="date" required name="day" class="form-control" id="inputPassword2" placeholder="Password">
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary mb-3">Confirm identity</button>

        </div>

    </form>
    <div class="col_auto" style="margin:2%">
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Relatorio de
            Batimento</button>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Relatorio de Ponto</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/reportCheckIns">
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Data Inicial:</label>
                            <input type="date" name="day1" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="message-text" class="col-form-label">Data Final:</label>
                            <input type="date" name="day2" class="form-control">
                        </div>
                        <select class="form-control" name="operator">
                <option value="">Todos os Operadores</option>
                <?php foreach($operators as $operator):
                ?>
                <option value="<?= $operator->id?>"><?=$operator->name?></option>
                <?php endforeach; ?>
            </select>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<table class="table table-hover">
    <tr>
        <th>Operador</th>
        <?php
            $count = 1;
            $a = array(1,2,3,4);
        foreach($a as $daily):
        ?>
        <th><?=$count?>ª Batida</th>
        <?php
            $count++;
        endforeach;
        ?>
        <th>Quantidade de Horas Trabalhadas
    </tr>

    <?php
    foreach($dailies as $daily):
    ?>
    <tr>
        <td><?= $daily->getUser->name?></td>
        <?php
        $calc = 1;
     foreach($daily->getCheckIns as $checkIn):
        ?>
        <td><?= substr($checkIn->created_at,11) ?></td>
        <?php
        $calc++;
     endforeach;
     for($calc=$calc;$calc<=4;$calc++):
        ?>
        <td></td>
        <?php
     endfor;
     ?>

        <td>
            <?=$daily->countHours($daily->user_id,substr($daily->created_at,0,10))?>
        </td>
        </td>
    </tr>
    <?php
    endforeach;
    ?>
</table>