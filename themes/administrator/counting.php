<?php 
$v->layout("operator/_theme");  
?>
<div style="text-align:center;margin-top:5%;margin-bottom:5%">
    <h2>Auditoria de Inventario</h2>
</div>
<form class="row gy-2 gx-3 align-items-center" method="post" action="/counting/search" style="margin:10%">
    <div class="col-2">
        <input type="text" class="form-control" id="autoSizingInput" required name="store" placeholder="Loja">
    </div>
    <div class="col-2">
        <input type="text" class="form-control" id="autoSizingInput" required name="year" placeholder="Ano">
    </div>
    <div class="col-2">
        <input type="text" class="form-control" id="autoSizingInput" name="code" placeholder="Codigo">
    </div>
    <div class="col-auto">
        <input type="text" class="form-control" id="autoSizingInput" name="keyWord" placeholder="Palavra Chave">
    </div>
    <div class="col-auto">
        <select name="taxation" class="form-select">
            <option value="">Tributação ...</option>
            <option value="I">Isento</option>
            <option value="F">Substituido</option>
            <option value="7">7%</option>
            <option value="12">12%</option>
            <option value="18">18%</option>
            <option value="20">20%</option>
            <option value="25">25%</option>
            <option value="27">27%</option>
        </select>
    </div>
    <div class="col-auto">
        <button type="submit" class="btn btn-primary">Buscar</button>
    </div>
</form>
<div class="form-row" style="margin-left:85%;margin-top:2%;margin-bottom:2%">
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Funções
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#Importar"
                data-whatever="@mdo">Importar Movimento XML Oracle ⇅</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#ImportarXML"
                data-whatever="@mdo">Importar Movimento XML NFC-e ⇅</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#ImportarEFD"
                data-whatever="@mdo">Importar Movimento EFD Mensal</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#Inventario"
                data-whatever="@mdo">Importar Inventário EFD Bloco H ◴</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#Retroagir"
                data-whatever="@mdo">Retroagir com Movimentações em XML Oracle</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#Processar"
                data-whatever="@mdo">Processar Dados (Movimentação, Inventarios e Apuração</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#Bloco"
                data-whatever="@mdo">Visualizar Blocos H Separadamente</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#setCost"
                data-whatever="@mdo">Setar Custos dos Inventarios </button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#planilha"
                data-whatever="@mdo">Gerar Planilha com Dados da Apuração</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#setTributacao"
                data-whatever="@mdo">Setar Tributação dos Produtos na Apuração</button>
            <button type="button" class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#setStockMovment"
                data-whatever="@mdo">Importar Movimentação Fiscal</button>
        </div>
    </div>
</div>
<div class="modal fade" id="setStockMovment" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Importar Movimentação Fiscal</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="setStockMovment">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" name="store" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="year"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tipo:</label>
                        <select class="form-control" id="inputGroupSelect01" name="type">
                            <option selected>...</option>
                            <option value="s">Saidas</option>
                            <option value="e">Entradas</option>
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
            </form>
            <script>
                $('form[name="setStockMovment"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/counting/setStockMovement',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.msg,
                                    timer: 999999999999999,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                //document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.msg,
                                    icon: "error",
                                    timer: 999999999999999,
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }
                        }
                    })
                });
            </script>
        </div>
    </div>
</div>
<div class="modal fade" id="setTributacao" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Setar Tributação dos Produtos</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="setTributation">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" name="store" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="year"
                            class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
            </form>
            <script>
                $('form[name="setTributation"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/counting/setTaxation',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {

                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                //document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }
                        }
                    })
                });
            </script>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="planilha" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Planilha de Apuração</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="planilha" id="planilha" method="post" target="_blank">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" name="store" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="year"
                            class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <div id="respostaplanilha" style="width:100%"></div>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnplanilha" class="btn btn-primary">Confirmar</button>
                </form>
                <script>
                    $(document).ready(function () {
                        $('#btnplanilha').click(function () {
                            $('#planilha').ajaxForm({
                                uploadProgress: function (event, position, total,
                                    percentComplete) {
                                    $('progress').attr('value', percentComplete);
                                    $('#porcentagemplanilha').html(percentComplete + '%');
                                },
                                success: function (data) {
                                    $('progress').attr('value', '100');
                                    $('#porcentagemplanilha').html('100%');
                                    if (data.sucesso == true) {
                                        $('#respostaplanilha').html(
                                            '<div class="alert alert-success" role="alert">' +
                                            data.msg + '</div>');
                                        //document.location.reload(true);
                                    } else {
                                        $('#respostaplanilha').html(
                                            '<div class="alert alert-danger" role="alert">' +
                                            data.msg + '</div>');
                                    }
                                },
                                error: function () {
                                    $('#respostaplanilha').html(
                                        'Erro ao enviar requisição!!!');
                                },
                                dataType: 'csv',
                                url: '/counting/planilha',
                                resetForm: true
                            }).submit();
                        })
                    })
                </script>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="Processar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Processar Dados</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="processar" id="processar" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" name="store" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="year"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Operação:</label>
                        <select class="form-control" id="inputGroupSelect01" name="tipo">
                            <option value="1">Incluir Itens</option>
                            <option value="2">Incluir as Descrições</option>
                            <option value="3">Incluir as Movimentações</option>
                            <option value="4">Incluir os Inventarios</option>
                            <option value="5">Processar Dados</option>
                            <option value="6">Processar Custos</option>
                            <option value="7">Processar Dados Simples</option>
                            <option value="8">Remover quantidades > 10.000</option>
                            <option value="9">Aplicar custo medio</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">ICMS:</label>
                        <select class="form-control" id="inputGroupSelect01" name="icms">
                            <option value="s">Retroage itens substituidos e isentos</option>
                            <option value="n">Não retroage itens substituidos e isentos</option>
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <div id="respostaProcessar"></div>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" id="btnProcessar" class="btn btn-primary">Confirmar</button>
                </form>
                <script>
                    $(document).ready(function () {
                        $('#btnProcessar').click(function () {
                            $('#processar').ajaxForm({
                                uploadProgress: function (event, position, total,
                                    percentComplete) {
                                    $('progress').attr('value', percentComplete);
                                    $('#porcentagemProcessar').html(percentComplete + '%');
                                },
                                success: function (data) {
                                    $('progress').attr('value', '100');
                                    $('#porcentagemProcessar').html('100%');
                                    if (data.sucesso == true) {
                                        $('#respostaProcessar').html(
                                            '<div class="alert alert-success" role="alert">' +
                                            data.msg + '</div>');
                                        // //document.location.reload(true);
                                    } else {
                                        $('#respostaProcessar').html(
                                            '<div class="alert alert-danger" role="alert">' +
                                            data.msg + '</div>');
                                    }
                                },
                                error: function () {
                                    $('#respostaProcessar').html(
                                        'Erro ao enviar requisição!!!');
                                },
                                dataType: 'json',
                                url: '/counting/processar',
                                resetForm: true
                            }).submit();
                        })
                    })
                </script>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="setCost" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Setar custos Inventario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="counting/setCost" method="post" _blank>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" name="store" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="year"
                            class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Confirmar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="Bloco" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Visualizar Bloco H</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/counting/searchBlocoH" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" name="loja" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="ano"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Número do Inventario:</label>
                        <select class="form-control" id="inputGroupSelect01" name="tipo">
                            <option value="1">1º</option>
                            <option value="2">2º</option>
                            <option value="3">3º</option>
                            <option value="4">4º</option>
                            <option value="5">5º</option>
                            <option value="6">6º</option>
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnRetroagir" class="btn btn-primary">Confirmar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="Retroagir" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Retroagir Estoque Final</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="retroagir" id="retroagir" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" name="loja" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="ano"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tipo:</label>
                        <select class="form-control" id="inputGroupSelect01" name="tipo">
                            <option selected>...</option>
                            <option value="s">Saidas</option>
                            <option value="e">Entradas</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin-top:5%">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="file" type="file" class="form-control" id="inputGroupFile01">
                        </div>
                        <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                            id="porcentagemRetroagir">0%</span>
                        <div id="respostaRetroagir" style="width:100%"></div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnRetroagir" class="btn btn-primary">Confirmar</button>
                </form>
                <script>
                    $(document).ready(function () {
                        $('#btnRetroagir').click(function () {
                            $('#retroagir').ajaxForm({
                                uploadProgress: function (event, position, total,
                                    percentComplete) {
                                    $('progress').attr('value', percentComplete);
                                    $('#porcentagemRetroagir').html(percentComplete + '%');
                                },
                                success: function (data) {
                                    $('progress').attr('value', '100');
                                    $('#porcentagemRetroagir').html('100%');
                                    if (data.sucesso == true) {
                                        $('#respostaRetroagir').html('<a>' + data.msg +
                                            '</a>');
                                        //document.location.reload(true);
                                    } else {
                                        $('#respostaRetroagir').html(data.msg);
                                    }
                                },
                                error: function () {
                                    $('#respostaRetroagir').html(
                                        'Erro ao enviar requisição!!!');
                                },
                                dataType: 'json',
                                url: '/counting/retroagir',
                                resetForm: true
                            }).submit();
                        })
                    })
                </script>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="Importar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Importar Movimentações de Estoque</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="formUpload" id="formUpload" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tipo:</label>
                        <select class="form-control" id="inputGroupSelect01" name="tipo">
                            <option selected>...</option>
                            <option value="s">Saidas</option>
                            <option value="e">Entradas</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="ano"
                            class="form-control">
                    </div>
                    <div class="form-group" style="margin-top:2%">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="file" type="file" class="form-control" id="inputGroupFile01">
                        </div>
                    </div>
                    <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                        id="porcentagem">0%</span>
                    <div id="resposta" style="width:100%"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
            </div>
            </form>
            <script>
                $(document).ready(function () {
                    $('#btnEnviar').click(function () {
                        $('#formUpload').ajaxForm({
                            uploadProgress: function (event, position, total,
                                percentComplete) {
                                $('progress').attr('value', percentComplete);
                                $('#porcentagem').html(percentComplete + '%');
                            },
                            success: function (data) {
                                $('progress').attr('value', '100');
                                $('#porcentagem').html('100%');
                                if (data.sucesso == true) {
                                    $('#resposta').html('<a>' + data.msg + '</a>');
                                    //document.location.reload(true);
                                } else {
                                    $('#resposta').html(data.msg);
                                }
                            },
                            error: function () {
                                $('#resposta').html('Erro ao enviar requisição!!!');
                            },
                            dataType: 'json',
                            url: '/counting/importStockMovement',
                            resetForm: true
                        }).submit();
                    })
                })
            </script>
        </div>
    </div>
</div>
<div class="modal fade" id="ImportarXML" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Importar Movimentações de Estoque</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="counting/importXML" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="number" name="loja" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
            </form>
            <!-- <script>
                    $(document).ready(function () {
                        $('#btnEnviar').click(function () {
                            $('#formUpload').ajaxForm({
                                uploadProgress: function (event, position, total,
                                    percentComplete) {
                                    $('progress').attr('value', percentComplete);
                                    $('#porcentagem').html(percentComplete + '%');
                                },
                                success: function (data) {
                                    $('progress').attr('value', '100');
                                    $('#porcentagem').html('100%');
                                    if (data.sucesso == true) {
                                        $('#resposta').html('<a>' + data.msg + '</a>');
                                        //document.location.reload(true);
                                    } else {
                                        $('#resposta').html(data.msg);
                                    }
                                },
                                error: function () {
                                    $('#resposta').html('Erro ao enviar requisição!!!');
                                },
                                dataType: 'json',
                                url: '/counting/importStockMovement',
                                resetForm: true
                            }).submit();
                        })
                    })
                </script> -->
        </div>
    </div>
</div>
<div class="modal fade" id="ImportarEFD" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Importar Movimentações de Estoque</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="formUploadEFD" id="formUploadEFD" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tipo:</label>
                        <select class="form-control" id="inputGroupSelect01" name="tipo">
                            <option selected>...</option>
                            <option value="s">Saidas</option>
                            <option value="e">Entradas</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="ano"
                            class="form-control">
                    </div>
                    <div class="form-group" style="margin-top:2%">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="file" type="file" class="form-control" id="inputGroupFile01">
                        </div>
                    </div>
                    <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                        id="porcentagemEFD">0%</span>
                    <div id="respostaEFD" style="width:100%"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" id="btnEnviarEFD" class="btn btn-primary">Enviar</button>
            </div>
            </form>
            <script>
                $(document).ready(function () {
                    $('#btnEnviarEFD').click(function () {
                        $('#formUploadEFD').ajaxForm({
                            uploadProgress: function (event, position, total,
                                percentComplete) {
                                $('progress').attr('value', percentComplete);
                                $('#porcentagemEFD').html(percentComplete + '%');
                            },
                            success: function (data) {
                                $('progress').attr('value', '100');
                                $('#porcentagemEFD').html('100%');
                                if (data.sucesso == true) {
                                    $('#respostaEFD').html('<a>' + data.msg + '</a>');
                                    //document.location.reload(true);
                                } else {
                                    $('#respostaEFD').html(data.msg);
                                }
                            },
                            error: function () {
                                $('#respostaEFD').html('Erro ao enviar requisição!!!');
                            },
                            dataType: 'json',
                            url: '/counting/EFDimportStockMovement',
                            resetForm: true
                        }).submit();
                    })
                })
            </script>
        </div>
    </div>
</div>
<div class="modal fade" id="Inventario" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Importar Inventário</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="formUploadInv" id="formUploadInv" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <input type="text" name="loja" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <input type="number" min="<?= date("Y")-date("y");?>" max="<?= date("Y");?>" name="ano"
                            class="form-control">
                    </div>
                    <div class="form-group" style="margin-top:5%">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="file" type="file" class="form-control" id="inputGroupFile01">
                        </div>
                    </div>
                    <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                        id="porcentagemInv">0%</span>
                    <div id="respostaInv" style="width:100%"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" id="btnEnviarInv" class="btn btn-primary">Confirmar</button>
            </div>
            </form>
            <script>
                $(document).ready(function () {
                    $('#btnEnviarInv').click(function () {
                        $('#formUploadInv').ajaxForm({
                            uploadProgress: function (event, position, total,
                                percentComplete) {
                                $('progress').attr('value', percentComplete);
                                $('#porcentagemInv').html(percentComplete + '%');
                            },
                            success: function (data) {
                                $('progress').attr('value', '100');
                                $('#porcentagemInv').html('100%');
                                if (data.sucesso == true) {
                                    $('#respostaInv').html('<a>' + data.msg + '</a>');
                                    //document.location.reload(true);
                                } else {
                                    $('#respostaInv').html(data.msg);
                                }
                            },
                            error: function () {
                                $('#respostaInv').html('Erro ao enviar requisição!!!');
                            },
                            dataType: 'json',
                            url: '/counting/importInv',
                            resetForm: true
                        }).submit();
                    })
                })
            </script>
        </div>
    </div>
</div>


<?php if($paginator):
?>
<h5 style="margin-left:2%;">Página <?= $paginator->page(); ?> de <?= $paginator->pages(); ?> </h5>
<div>
    <table class="table table-bordered">
        <?php
            foreach ($countings as $counting):
                ?>
        <tr class="bg-light">
            <td colspan="5">Loja: <?=$counting->store_code?></td> <!-- 5-5-9-->
            <td colspan="5">Codigo: <?=$counting->code?></td>
            <td colspan="9">Descrição: <?=$counting->description?></td>
        </tr>
        <tr style="font-size:58%">
            <th>Inventario 2015</th>
            <th>Inventario 2015 Calculado</th>
            <th>Entradas 2016</th>
            <th>Saidas 2016</th>
            <th>Inventario 2016</th>
            <th>Entradas 2017</th>
            <th>Saidas 2017</th>
            <th>Inventario 2017</th>
            <th>Entradas 2018</th>
            <th>Saidas 2018</th>
            <th>Inventario 2018</th>
            <th>Entradas 2019</th>
            <th>Saidas 2019</th>
            <th>Inventario 2019</th>
            <th>Entradas 2020</th>
            <th>Saidas 2020</th>
            <th>Inventario 2020 Calculado</th>
            <th>Inventario 2020</th>
            <th>Diferença</th>
        </tr>
        <tr style="font-size:70%">
            <td><?=str_replace(".",",",round($counting->inventory_quantity_1,5))?></td>
            <td><?=str_replace(".",",",round($counting->initial_inventory_quantity,5))?></td>
            <td><?=str_replace(".",",",round($counting->input_1,5))?></td>
            <td><?=str_replace(".",",",round($counting->withdrawal_1,5))?></td>
            <td><?=str_replace(".",",",round($counting->inventory_quantity_2,5))?></td>
            <td><?=str_replace(".",",",round($counting->input_2,5))?></td>
            <td><?=str_replace(".",",",round($counting->withdrawal_2,5))?></td>
            <td><?=str_replace(".",",",round($counting->inventory_quantity_3,5))?></td>
            <td><?=str_replace(".",",",round($counting->input_3,5))?></td>
            <td><?=str_replace(".",",",round($counting->withdrawal_3,5))?></td>
            <td><?=str_replace(".",",",round($counting->inventory_quantity_4,5))?></td>
            <td><?=str_replace(".",",",round($counting->input_4,5))?></td>
            <td><?=str_replace(".",",",round($counting->withdrawal_4,5))?></td>
            <td><?=str_replace(".",",",round($counting->inventory_quantity_5,5))?></td>
            <td><?=str_replace(".",",",round($counting->input_5,5))?></td>
            <td><?=str_replace(".",",",round($counting->withdrawal_5,5))?></td>
            <td><?=str_replace(".",",",round($counting->inventory_quantity_6,5))?></td>
            <td><?=str_replace(".",",",round($counting->final_inventory_quantity,5))?></td>
            <td><?=str_replace(".",",",round($counting->inventory_quantity_6-$counting->final_inventory_quantity,5))?></td>
        </tr>
        <?php
    endforeach;?>
    </table>
</div>
<div style="margin-left:2%;margin-bottom:5%"><?= $paginator->render();?></div>
<?php endif;?>