<?php 
$v->layout("administrator/adm_theme");

?>
<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 60000);
</script>
<main class="main_content">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="/home2">Acompanhamento diario (lançamentos diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/1">Acompanhamento diario (lançamentos com
                    confêrencia)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/2">Duração media status diario (lançamentos
                    diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/3">Duração media status diario (lançamentos com
                    conferência)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/4">Situações Pontuais (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/5">Acompanhamento Reportados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/6">Acompanhamento Pendente Empresa (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/7">Acompanhamento Chegada Carga Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/8">Acompanhamento Atualizadas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/9">Atualizadas por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/10">Pendente cliente por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/11">Atualizadas por Operador Sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  active" aria-current="page" href="/home2/12">Atendimento Solicitações de lançamento
                    Por Loja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/13">Atualizações por hora / usúario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/14">Incluidas e Concluidas por hora</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/15">Acompanhamento Pendente Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/16">Acompanhamento Pendente Cliente Dias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/17">Acompanhamento Pendência Comercial Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/18">Acompanhamento Conferência Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/19">Acompanhamento NFe a Atualizar Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/20">Alocadas por Usuario sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/21">Pendentes Prioridades Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/22">Pendentes Usuário Media</a>
            </li>
            <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 180 or $_COOKIE['id'] == 20000151): ?>
            <li class="nav-item">
                <a class="nav-link" href="/home2/26">Notas Atualizadas Operador Hora</a>
            </li>
            <?php endif; ?>
        </ul>
        <section class="col-lg-12 connectedSortable">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1" style="text-align:center"></i>
                        Atendimento Solicitações de lançamento Por Loja
                    </h3>
                    <div class="card-tools">Completed
                        <ul class="nav nav-pills ml-auto">
                            <li class="nav-item">
                            </li>
                        </ul>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <?php
                    foreach ($stores as $store):
                    ?>
                        <div class="col-lg-2 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h4>Loja <?=$store->code?></h4>
                                    <p><?=$requestStoreIncluded[$store->code]+0?> Solicitações de lançamento(os)
                                        Aberto(os)</p>
                                    <p><?=$requestStoreIncludedA[$store->code]+0?> Itens</p>
                                    <p><?=$requestStorePendente[$store->code]+0?> Solicitações de lançamento(os)
                                        Pendente(es)
                                    </p>
                                    <p><?=$requestStorePendenteA[$store->code]+0?> Itens</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="<?=URL_BASE?>/request/storeStatus/<?=$store->store_id?>/1"
                                    class="small-box-footer">Info. <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-6">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h4>Loja <?=$store->code?></h4>
                                    <p><?=$requestStoreCompleted[$store->code]+0?> Solicitações de lançamento(os)
                                        Concluido(os)
                                    </p>
                                    <p><?=$requestStoreCompletedA[$store->code]+0?> Itens</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="<?=URL_BASE?>/request/storeStatus/<?=$store->store_id?>/3"
                                    class="small-box-footer">Info. <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <?php
                    endforeach;
                    ?>
        </section>
    </div>