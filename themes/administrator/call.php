<?php 
$v->layout("operator/_theme");
?>
<div class="bg-light p-5" style="margin-bottom:5%">
    <div class="container text-center">
        <h1 class="display-4">Ligações</h1>
        <p class="lead">Consulta as ligações registradas.</p>
    </div>
</div>

<div class="container" style="margin-bottom:5%">
    <form method="post" action="/call/search" class="row gx-3 gy-2 align-items-center" style="margin-bottom:2%">
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeInputName">Loja</label>
            <input name="store" type="text" class="form-control" id="specificSizeInputName" placeholder="Loja">
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeInputName"></label>
            <input name="dateIni" type="date" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeInputGroupUsername">Operador</label>
            <div class="input-group">
                <input name="dateFim" type="date" class="form-control" id="specificSizeInputGroupUsername">
            </div>
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect">Preference</label>
            <select name="type" class="form-select" id="specificSizeSelect">
                <option selected value=''>Tipo ...</option>
                <option value="W">Whats App</option>
                <option value="L">Ligação</option>
                <option value="C">Chamado Interno</option>
            </select>
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
    <table class="table table-striped table-light">
        <thead>
            <tr>
                <th>#</th>
                <th>Loja</th>
                <th>Operador</th>
                <th>Duração</th>
                <th>Setor</th>
                <th>Criticidade</th>
                <th>Realizada em</th>
                <th>Opções
                <th>
            </tr>
        </thead>
        <tbody>
            <?php if (is_array($calls)):
foreach($calls as $call):
    ?>
            <tr>
                <th scope="row"><?=$call->id?></th>
                <td><?=$call->store_id?></td>
                <td><?=$call->userOperator()->name?></td>
                <td><?=$call->duration?></td>
                <td><?=$call->sectors()->name?></td>
                <td><?=$call->criticism()->name?></td>
                <td><?=$call->getCreatedAt?></td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn  dropdown-toggle" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            Opções
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal">Visualizar</a></li>
                            <!-- <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">Separated link</a></li> -->
                        </ul>
                    </div>
                <td>
            </tr>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                <h5><?=$call->userOperator()->name?> <a style="font-size:60%;color:blue">
                                        <?=$call->getCreatedAtInt?></a></h5>
                                <p><?=str_replace("<br />","",$call->description)?></p>
                            </div>
                            <?php if (is_array($call->attachment)):
                            foreach($call->attachment as $attachment):
                                ?>
                            <div class="mb-3">
                                <img style="width:350px;margin-bottom:1%;" src="<?=url($attachment->name)?>"
                                    class="card-img-top" alt="...">
                            </div>
                            <?php 
                            endforeach;
                        endif;
                        ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;
endif;
?>
        </tbody>
    </table>
</div>
<?php 
// echo "<pre>";
// var_dump($calls);
// echo "</pre>";
?>