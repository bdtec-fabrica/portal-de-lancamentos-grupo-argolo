<?php 
$v->layout("administrator/adm_theme");
?>
<div class="bg-light p-5" style="margin-bottom:5%">
    <div class="container text-center">
        <h1 class="display-4">SLA - Chamados</h1>
        <p class="lead">Relatorio SLA dos chamados atendidos.</p>
    </div>
</div>
<div class="container" style="margin-bottom:10%">
    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
        data-bs-target="#relatorios">Relatorios</button>
</div>
<div class="modal fade" id="relatorios" tabindex="-1" aria-labelledby="relatoriosModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="relatoriosModalLabel">Relatorios SLA</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/SLA/SLASearch" method="POST">
                    <div class="mb-3">
                        <select name="store" class="form-select form-select-md mb-3"
                            aria-label=".form-select-sm example">
                            <option value="" selected>Lojas</option>
                            <?php foreach ($stores as $store):
                            ?>
                            <option value="<?=$store->code?>"><?=$store->code?></option>
                            <?php endforeach;
                            ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <select name="group" class="form-select form-select-lmd mb-3"
                            aria-label=".form-select-sm example">
                            <option value="" selected>Grupos</option>
                            <?php foreach ($storeGroups as $storeGroup):
                            ?>
                            <option value="<?=$storeGroup->id?>"><?=$storeGroup->name?></option>
                            <?php endforeach;
                            ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <span>Data Inicial</span>
                        <input name="dataInicial" class="form-control" type="date">
                    </div>
                    <div class="mb-3">
                        <span>Data Final</span>
                        <input name="dataFinal" class="form-control" type="date">
                    </div>
                    <div class="mb-3">
                        <select name="type" required class="form-select form-select-md mb-3"
                            aria-label=".form-select-sm example">
                            <option value="" selected>Tipo de Relatorio</option>
                            <option value="A">Analitico</option>
                            <option value="S">Sintetico</option>
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Confirmar</button>
                </form>
            </div>
        </div>
    </div>
</div>
