<?php 
session_start();
if (!$league):
$v->layout("administrator/adm_theme");
endif;
?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Painel de Notificações</h1>
        <p class="lead">Demonstra a  ultima execução de cada serviço</p>
    </div>
</div>
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th style="min-width:25px">Descrição</th>
            <th style="min-width:25px">Ultima Execução</th>
            
        </tr>
    </thead>
    <tbody>
        <?php 
    foreach ($notifications as $notification):
        ?>
        <tr>
            <td><?=$notification->description; ?></td>
            <td><?=$notification->last_run; ?></td>
        </tr>
    <?php
    endforeach;
    ?>
    </tbody>
</table>