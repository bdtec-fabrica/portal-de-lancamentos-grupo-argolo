<?php 
session_start();
$v->layout("administrator/adm_theme");
?>

<table class="table table-dark table-striped">
    <tr>
        <th>Loja</th>
        <td><?=$store->code.' - '.$store->razao?></td>
        <td><?=substr($date,0,2)."/".substr($date,2)?></td>
    </tr>
    <tr>
        <th>Arquivo 1º Período</th>
        <td><?=$store->file1($store->id,$date)?></td>
        <td><?=$store->file1_($store->id,$date)->created_at?></td>
        <form name="adicionar1">
            <input type="hidden" name="id" value="<?=$store->file1_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar1"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Inconsistência 1º Período</th>
        <td><?=$store->inconsistence1($store->id,$date)?></td>
        <td><?=$store->inconsistence1_($store->id,$date)->created_at?></td>
        <form name="adicionar2">
            <input type="hidden" name="id" value="<?=$store->inconsistence1_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar2"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Arquivo 2º Período</th>
        <td><?=$store->file2($store->id,$date)?></td>
        <td><?=$store->file2_($store->id,$date)->created_at?></td>
        <form name="adicionar3">
            <input type="hidden" name="id" value="<?=$store->file2_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar3"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Inconsistência 2º Período</th>
        <td><?=$store->inconsistence2($store->id,$date)?></td>
        <td><?=$store->inconsistence2_($store->id,$date)->created_at?></td>
        <form name="adicionar4">
            <input type="hidden" name="id" value="<?=$store->inconsistence2_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar4"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Arquivo 3º Período</th>
        <td><?=$store->file3($store->id,$date)?></td>
        <td><?=$store->file3_($store->id,$date)->created_at?></td>
        <form name="adicionar5">
            <input type="hidden" name="id" value="<?=$store->file3_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar5"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Planilha Estorno de Credito</th>
        <td><?=$store->spreadsheet($store->id,$date)?></td>
        <td><?=$store->spreadsheet_($store->id,$date)->created_at?></td>
        <form name="adicionar7">
            <input type="hidden" name="id" value="<?=$store->spreadsheet_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar7"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Conf. Vendas/NFC-E</th>
        <td><?=$store->sailsConference($store->id,$date)?></td>
        <td><?=$store->sailsConference_($store->id,$date)->created_at?></td>
        <form name="adicionar8">
            <input type="hidden" name="id" value="<?=$store->sailsConference_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar8"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Inconsistência 3º Período</th>
        <td><?=$store->inconsistence3($store->id,$date)?></td>
        <td><?=$store->inconsistence3_($store->id,$date)->created_at?></td>
        <form name="adicionar9">
            <input type="hidden" name="id" value="<?=$store->inconsistence3_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar9"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Fechamento ICMS</th>
        <td><?=$store->ICMSClousure($store->id,$date)?></td>
        <td><?=$store->ICMSClousure_($store->id,$date)->created_at?></td>
        <form name="adicionar10">
            <input type="hidden" name="id" value="<?=$store->ICMSClousure_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar10"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Arquivo ICMS Declaração</th>
        <td><?=$store->fileSendedF($store->id,$date)?></td>
        <td><?=$store->fileSendedF_($store->id,$date)->created_at?></td>
        <form name="adicionar11">
            <input type="hidden" name="id" value="<?=$store->fileSendedF_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar11"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
    <tr>
        <th>Arquivo PIS/COFINS</th>
        <td><?=$store->fileSended($store->id,$date)?></td>
        <td><?=$store->fileSended_($store->id,$date)->created_at?></td>
        <form name="adicionar11">
            <input type="hidden" name="id" value="<?=$store->fileSended_($store->id,$date)->id?>">
            <td><button type="submit" class="button btn-danger">Excluir</button></td>
        </form>
        <script>
            $('form[name="adicionar11"]').submit(function (event) {
                event.preventDefault();

                $.ajax({
                    url: '/fiscal/delete',
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (response) {

                        if (response.success === true) {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                timer: 600,
                                icon: "success",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "success"
                            });
                            document.location.reload(true);
                        } else {
                            swal({
                                title: "Bispo & Dantas",
                                text: response.message,
                                icon: "error",
                                showCancelButton: false,
                                showConfirmButton: false,
                                type: "danger"
                            });
                        }

                    }
                })
            });
        </script>
    </tr>
</table>
    <div class="row">
        <div class="col">
            <div class="" style="margin-top:5%">
                <span>Conferencia KW</span>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Dia</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i=1;$i<=date("t", strtotime(substr($date,2)."-".substr($date,0,2)."-01"));$i++): ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><form name="form">
                                    <input type="hidden" name="store_id" value="<?=$store->id?>">
                                    <input type="hidden" name="date" value="<?=date("Y-m-d", strtotime(substr($date,2)."-".substr($date,0,2)."-$i"))?>">
                                    <input type="hidden" name="conferenceType" value="kw">
                                    <button class="btn btn-outline-secondary"  type="submit"><?=$store->KW($store->id,date("Y-m-d", strtotime(substr($date,2)."-".substr($date,0,2)."-$i")))?></button>
                                </form>
                            </td>
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col">
            <div class="" style="margin-top:5%">
                <span>Conferencia Conector</span>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Dia</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i=1;$i<=date("t", strtotime(substr($date,2)."-".substr($date,0,2)."-01"));$i++): ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><form name="form">
                                    <input type="hidden" name="store_id" value="<?=$store->id?>">
                                    <input type="hidden" name="date" value="<?=date("Y-m-d", strtotime(substr($date,2)."-".substr($date,0,2)."-$i"))?>">
                                    <input type="hidden" name="conferenceType" value="conector">
                                    <button class="btn btn-outline-secondary"  type="submit"><?=$store->Conector($store->id,date("Y-m-d", strtotime(substr($date,2)."-".substr($date,0,2)."-$i")))?></button>
                                </form>
                            </td>
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        $('form[name="form"]').submit(function (event) {
            event.preventDefault();
            $.ajax({
                url: '/fiscal/conferenceAdd',
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.sucesso === true) {
                        swal({
                            title: "Bispo & Dantas",
                            text: response.msg,
                            timer: 20000,
                            icon: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            type: "success"
                        });
                        document.location.reload(true);
                    } else {
                        swal({
                            title: "Bispo & Dantas",
                            text: response.msg,
                            icon: "error",
                            showCancelButton: false,
                            showConfirmButton: false,
                            type: "danger"
                        });
                    }
                }
            })
        });
    </script>

<div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="adicionar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="adicionar">Adicionar interação</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="formUpload" id="formUpload" method="post">
                    <input name="store_id" value="<?=$store->id?>" type="hidden">
                    <input name="month" value="<?=$date?>" type="hidden">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Observação:</label>
                        <textarea name="descricao" required class="form-control" id="message-text"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                    id="porcentagem">0%</span>
                <div id="resposta" style="width:100%"></div>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#btnEnviar').click(function () {
            $('#formUpload').ajaxForm({
                uploadProgress: function (event, position, total, percentComplete) {
                    $('progress').attr('value', percentComplete);
                    $('#porcentagem').html(percentComplete + '%');
                },
                success: function (data) {
                    $('progress').attr('value', '100');
                    $('#porcentagem').html('100%');
                    if (data.sucesso == true) {
                        $('#resposta').html('<a>' + data.msg + '</a>');
                        document.location.reload(true);
                    } else {
                        $('#resposta').html(data.msg);
                    }

                },
                error: function () {
                    $('#resposta').html('Erro ao enviar requisição!!!');
                },
                dataType: 'json',
                url: '/fiscal/interacoes/add',
                resetForm: true
            }).submit();
        })
    })
</script>
<div class="" style="margin-top:5%">
    <div style="margin-left:2%;margin-right:2%;">
        <button style="margin-top:2%" type="button" class="btn btn-secondary btn-lg" data-bs-toggle="modal"
            data-bs-target="#adicionar">Adicionar
            +</button>
        <?php
        if ($interactions):
            ?>
        <?php
        else:
            ?>
        <h5 style="margin-left:43%;margin-bottom:5%">Sem Interações.</h5>
        <?php
        endif;
        ?>

        <?php 
        if(is_array($interactions)):
        foreach ($interactions as $interaction):
            ?>
        <div class="card" style="margin-top:2%;margin-left:2%;margin-right:2%;margin-bottom:2%">
            <div class="card-header">
                <?=$interaction->user()->getLogin." <a style='font-size:70%;color:#5199FF'>".$interaction->getCreatedAt?></a>
            </div>
            <div class="card-body">
                <h5 class="card-title"></h5>
                <p class="card-text"><?=nl2br($interaction->note)?></p>
            </div>
        </div>
        <?php 
        endforeach;
    endif;
        ?>
    </div>
</div>