<?php 
$v->layout("administrator/adm_theme");

?>
<div class="bg-light p-5" style="margin-bottom:5%">
    <div class="container text-center">
        <h1 class="display-4">Gerencial estrategico BDTec</h1>
        <?php if ($date1):?>
        <p class="lead"><?=$date1.' à '.$date2?></p>
        <?php endif;?>
    </div>
</div>
<div class="container-lg">
    <div class="card-header">
        Periodo das vendas
    </div>
    <div class="card-body">
        <form action="/margin/webservice37" method="post">
            <div class="row">
                <div class="form-group col-md-2">
                    <span>Criação inicial</span>
                    <input name="date1" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação final</span>
                    <input name="date2" style="font-size:80%" class="form-control" type="date">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>


</div>
<div class="container-lg">
    <table class="table table-hover sortable">
        <thead>
            <tr>
                <th scope="col">Loja</th>
                <th scope="col">Seção</th>
                <th scope="col">Venda Atual</th>
                <th scope="col">Venda Mês Anterior</th>
                <th scope="col">Percentual alcançado mês anterior</th>
                <th scope="col">Venda Ano Anterior</th>
                <th scope="col">Percentual alcançado ano anterior</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($secoes as $secao):?>
            <tr <?php if (((i0($valorVenda[$secao],$valorVendaMes[$secao])-$valorVendaMes[$secao])/i0($valorVenda[$secao],$valorVendaMes[$secao]))*100 < 0 && ((i0($valorVenda[$secao],$valorVendaAno[$secao])-$valorVendaAno[$secao])/i0($valorVenda[$secao],$valorVendaAno[$secao]))*100 < 0):?>
                class="table-danger"
                <?php elseif(((i0($valorVenda[$secao],$valorVendaMes[$secao])-$valorVendaMes[$secao])/i0($valorVenda[$secao],$valorVendaMes[$secao]))*100 < 0 || ((i0($valorVenda[$secao],$valorVendaAno[$secao])-$valorVendaAno[$secao])/i0($valorVenda[$secao],$valorVendaAno[$secao]))*100 < 0): ?>
                class="table-warning" <?php else: ?> class="table-success"<?php endif;?>>
                <form method="POST" action="/margin/webservice41">
                    <input type="hidden" name="loja" value="<?=$loja?>">
                    <input type="hidden" name="secao" value="<?=$secao?>">
                    <input type="hidden" name="date1" value="<?=$date1?>">
                    <input type="hidden" name="date2" value="<?=$date2?>">
                    <th scope="row"><?=$loja?></th>
                    <td><?=$secao?></td>
                    <td><?=number_format($valorVenda[$secao],2,",",".")?></td>
                    <td><?=number_format($valorVendaMes[$secao],2,",",".")?></td>
                    <td><?=number_format(((i0($valorVenda[$secao],$valorVendaMes[$secao])-$valorVendaMes[$secao])/i0($valorVenda[$secao],$valorVendaMes[$secao]))*100,2,",",".")?>%
                        <?php if(((i0($valorVenda[$secao],$valorVendaMes[$secao])-$valorVendaMes[$secao])/i0($valorVenda[$secao],$valorVendaMes[$secao]))*100 >0):?>
                        ↑
                        <?php else: ?>
                        ↓
                        <?php endif;?>
                    </td>
                    <td><?=number_format($valorVendaAno[$secao],2,",",".")?></td>
                    <td><?=number_format(((i0($valorVenda[$secao],$valorVendaAno[$secao])-$valorVendaAno[$secao])/i0($valorVenda[$secao],$valorVendaAno[$secao]))*100,2,",",".")?>%
                        <?php if(((i0($valorVenda[$secao],$valorVendaAno[$secao])-$valorVendaAno[$secao])/i0($valorVenda[$secao],$valorVendaAno[$secao]))*100 > 0):?>
                        ↑
                        <?php else: ?>
                        ↓
                        <?php endif;?>
                    </td>
                    <td><button type="submit" class="btn btn-primary">+</button></td>
                </form>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <form>
        <input type="button" value="Voltar" onClick="history.go(-1)">
        <input type="button" value="Avançar" onCLick="history.forward()">
        <input type="button" value="Atualizar" onClick="history.go(0)">
    </form>
</div>