<?php 
session_start();
$v->layout("administrator/adm_theme");

?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Variação de Custo</h1>

    </div>
</div>

<div class="container" style="margin-bottom:5%">
    <form method="post" action="/margin/webservice38" class="row gx-3 gy-2 align-items-center" style="margin-bottom:2%">
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <input type="number" placeholder="Loja sem digito" class="form-control" name="loja" required>
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <input type="date" class="form-control" name="date1" required>
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <input type="date" class="form-control" name="date2" required>
        </div>
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <input placeholder="% Variação" type="number" class="form-control" name="variacao">
        </div>
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <input placeholder="Codigo sem Digito" type="number" class="form-control" name="produto">
        </div>
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <select name="tipo" class="form-control">
                <option value="1">Todos os Tipos</option>
                <option value="T">Transferência</option>
                <option value="C">Compras</option>
                <option value="D">Devolução</option>
                <option value="R">Remessa</option>
                <option value="F">Frete</option>
                <option value="V">Vendas</option>
                <option value="O">Outros</option>
            </select>
        </div>
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <select name="base" class="form-control">
                <option selected disabled>Selecione a BASE...</option>
                <option value="1">Mix Bahia</option>
                <option value="2">Bispo e Dantas</option>
                <option value="3">Novo Mix</option>
                <option value="4">Unimar</option>
                <option value="7">Bem Barato</option>
            </select>
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>

<table class="table sortable">
    <tr>
        <th>Loja</th>
        <th>Codigo</th>
        <th>Descrição</th>
        <th>Custo Minimo</th>
        <!-- <th>CFOP Custo Minimo</th> -->
        <th>Agenda Custo Minimo</th>
        <th>Data Custo Minimo</th>
        <th>Custo Maximo</th>
        <!-- <th>CFOP Custo Maximo</th> -->
        <th>Agenda Custo Maximo</th>
        <th>Data Custo Maximo</th>
        <th>Variação de Custo</th>
    </tr>
    <?php foreach ($relacao as $relacao):

            ?>
    <?php foreach ($relacao as $r): 
   
    $r = (array) $r;

        ?>
    <?php 
  
    
    if ((($r['maiores']-$r['menores']) > 0)):
              if(round((($r['maiores']-$r['menores'])/$r['maiores'])*100) > $diferenca or $diferenca == null): ?>
    <tr>
        <th><?=$loja?></th>
        <th><?=$r["codigo"]?></th>
        <th><?=$r["descricao"]?></th>
        <td><?=round($r["menores"],2)?></td>
        <!-- <td><?=$r["menoresCFOP"]?></td> -->
        <td><?=$r["menoresAgenda"]?></td>
        <td><?=dateRMSToDate($r["menoresData"])?></td>
        <td><?=round($r["maiores"],2)?></td>
        <!-- <td><?=$r["maioresCFOP"]?></td> -->
        <td><?=$r["maioresAgenda"]?></td>
        <td><?=dateRMSToDate($r["maioresData"])?></td>
        <td><?=round((($r['maiores'])/$r['menores']-1)*100)."%"?></td>

    </tr>
    <?php 
        endif;
        endif;   
        endforeach;
        ?>
    <?php endforeach; ?>

</table>

