<?php 
$v->layout("administrator/adm_theme");

?>
<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 60000);
</script>
<main class="main_content">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="/home2">Acompanhamento diario (lançamentos diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/1">Acompanhamento diario (lançamentos com
                    confêrencia)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/2">Duração media status diario (lançamentos
                    diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/3">Duração media status diario (lançamentos com
                    conferência)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/4">Situações Pontuais (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/5">Acompanhamento Reportados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/6">Acompanhamento Pendente Empresa (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/7">Acompanhamento Chegada Carga Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/8">Acompanhamento Atualizadas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/9">Atualizadas por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/10">Pendente cliente por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/11">Atualizadas por Operador Sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/12">Atendimento Solicitações de lançamento Por Loja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/13">Atualizações por hora / usúario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/14">Incluidas e Concluidas por hora</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/15">Acompanhamento Pendente Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/16">Acompanhamento Pendente Cliente Dias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/17">Acompanhamento Pendência Comercial Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/18">Acompanhamento Conferência Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/19">Acompanhamento NFe a Atualizar Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/20">Alocadas por Usuario sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/home2/21">Pendentes Prioridades Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/home2/22">Pendentes Usuário Media</a>
            </li>
            <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 180 or $_COOKIE['id'] == 20000151): ?>
            <li class="nav-item">
                <a class="nav-link" href="/home2/26">Notas Atualizadas Operador Hora</a>
            </li>
            <?php endif;?>
        </ul>

        <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Operador</th>
                            <th scope="col">Notas Pendente Empresa</th>
                            <th scope="col">Tempo medio (Alteração)</th>
                            <th scope="col">Tempo medio (Criação)</th>
                            <th scope="col">Notas Aguardando Liberação</th>
                            <th scope="col">Tempo medio (Alteração)</th>
                            <th scope="col">Tempo medio (Criação)</th>
                            <th scope="col">Notas Pendencia ICMS</th>
                            <th scope="col">Tempo medio (Alteração)</th>
                            <th scope="col">Tempo medio (Criação)</th>
                            <th scope="col">Notas Cadastro</th>
                            <th scope="col">Tempo medio (Alteração)</th>
                            <th scope="col">Tempo medio (Criação)</th>
                            <th scope="col">Notas Tarefa</th>
                            <th scope="col">Tempo medio (Alteração)</th>
                            <th scope="col">Tempo medio (Criação)</th>
                            <th scope="col">Notas Pendente Fiscal</th>
                            <th scope="col">Tempo medio (Alteração)</th>
                            <th scope="col">Tempo medio (Criação)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($rows as $row): ?>
                        <tr>
                            <th scope="row"><?=$row['operator']?></th>
                            <td><?=$row['1']?></td>
                            <?php if($row['1'] > 0): ?>
                            <td><?=round($row['duracao_1_1']/$row['1'],2)?></td>
                            <td><?=round($row['duracao_1_2']/$row['1'],2)?></td>
                            <?php else: ?>
                            <td>0</td>
                            <td>0</td>
                            <?php endif; ?>
                            <td><?=$row['5']?></td>
                            <?php if($row['5'] > 0): ?>
                            <td><?=round($row['duracao_5_1']/$row['5'],2)?></td>
                            <td><?=round($row['duracao_5_2']/$row['5'],2)?></td>
                            <?php else: ?>
                            <td>0</td>
                            <td>0</td>
                            <?php endif; ?>
                            <td><?=$row['6']?></td>
                            <?php if($row['6'] > 0): ?>
                            <td><?=round($row['duracao_6_1']/$row['6'],2)?></td>
                            <td><?=round($row['duracao_6_2']/$row['6'],2)?></td>
                            <?php else: ?>
                            <td>0</td>
                            <td>0</td>
                            <?php endif; ?>
                            <td><?=$row['9']?></td>
                            <?php if($row['9'] > 0): ?>
                            <td><?=round($row['duracao_9_1']/$row['9'],2)?></td>
                            <td><?=round($row['duracao_9_2']/$row['9'],2)?></td>
                            <?php else: ?>
                            <td>0</td>
                            <td>0</td>
                            <?php endif; ?>
                            <td><?=$row['17']?></td>
                            <?php if($row['17'] > 0): ?>
                            <td><?=round($row['duracao_17_1']/$row['17'],2)?></td>
                            <td><?=round($row['duracao_17_2']/$row['17'],2)?></td>
                            <?php else: ?>
                            <td>0</td>
                            <td>0</td>
                            <?php endif; ?>
                            <td><?=$row['18']?></td>
                            <?php if($row['18'] > 0): ?>
                            <td><?=round($row['duracao_18_1']/$row['18'],2)?></td>
                            <td><?=round($row['duracao_18_2']/$row['18'],2)?></td>
                            <?php else: ?>
                            <td>0</td>
                            <td>0</td>
                            <?php endif; ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
    </div>