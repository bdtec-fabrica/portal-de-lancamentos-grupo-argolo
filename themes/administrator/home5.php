<?php 
$v->layout("administrator/adm_theme");

?>
<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 60000);
</script>
<main class="main_content">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="/home2">Acompanhamento diario (lançamentos diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/1">Acompanhamento diario (lançamentos com
                    confêrencia)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/2">Duração media status diario (lançamentos
                    diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/3">Duração media status diario (lançamentos com
                    conferência)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  active" aria-current="page" href="/home2/4">Situações Pontuais (lançamento
                    direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/5">Acompanhamento Reportados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/6">Acompanhamento Pendente Empresa (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/7">Acompanhamento Chegada Carga Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/8">Acompanhamento Atualizadas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/9">Atualizadas por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/10">Pendente cliente por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/11">Atualizadas por Operador Sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/12">Atendimento Solicitações de lançamento Por Loja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/13">Atualizações por hora / usúario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/14">Incluidas e Concluidas por hora</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/15">Acompanhamento Pendente Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/16">Acompanhamento Pendente Cliente Dias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/17">Acompanhamento Pendência Comercial Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/18">Acompanhamento Conferência Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/19">Acompanhamento NFe a Atualizar Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/20">Alocadas por Usuario sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/21">Pendentes Prioridades Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/22">Pendentes Usuário Media</a>
            </li>
            <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 180 or $_COOKIE['id'] == 20000151): ?>
            <li class="nav-item">
                <a class="nav-link" href="/home2/26">Notas Atualizadas Operador Hora</a>
            </li>
            <?php endif; ?>
        </ul>
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-3">
                    <h1 class="m-0 text-dark" style="text-align:center">Situações Pontuais (lançamento direto)</h1>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="margin-bottom:5%">
            <div class="row">
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?=$pendingClientEvent+0?></h3>
                            <p>Pendente Cliente</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-add"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$pendenteClientf?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-android-add"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?=$pendingICMSEvent+0?></h3>
                            <p>Pendente ICMS</p>
                            <p>Usuários:
                                <?php 
                if (is_array($ICMSOperators)):
                  foreach ($ICMSOperators as $ICMSOperator):
                    ?>
                                <p><?=$ICMSOperator." - ".$amountICMSOperator[$ICMSOperator]?></p>
                                <?php 
                  endforeach;
                endif; 
                    ?>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-play"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$pendenteICMSId?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-ios-play"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-dark">
                        <div class="inner">
                            <h3><?=$registerEvent+0?></h3>

                            <p>Cadastro</p>
                            <p>Usuários:
                                <?php foreach ($registerOperators as $registerOperator):
                    ?>
                                <p>
                                    <?=$registerOperator." - ".$amountRegisterOperator[$registerOperator]." tickets ".$amountItemRegisterOperator[$registerOperator]." Itens"?>
                                </p>
                                <?php endforeach; 
                    ?>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-play"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$cadastroIds?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-ios-play"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$pendingComercial+0?></h3>
                            <p>Pendente Comercial</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-play"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$pendenteInformacoesId?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-ios-play"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-6">
                    <div class="small-box bg-secondary">
                        <div class="inner">
                            <h3><?=$pendingComercialMonth+0?></h3>
                            <p>Pendente Comercial por mês</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-play"></i>
                        </div>
                        <a href="<?=URL_BASE?>/request/<?=$pendingComercialMonthIds?>"
                            class="small-box-footer">Mais
                            Informações <i class="ion ion-ios-play"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>