<?php 
session_start();
$v->layout("administrator/adm_theme");
?>

<div id="tela">
    <h1> Envios no dia </h1>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Nome:</th>
                <th>Ultima Execução </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($watsapp->lastSend() as $send): ?>
            <tr>
                <td><?= $send->name?></td>
                <td><?= $send->created_at?></td>
            </tr>
            <?php endforeach;  ?>
        </tbody>
    </table>
    <h1> Contatos Loja </h1>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Loja:</th>
                <th>Nome:</th>
                <th>Tell:</th>
                <th>Ultima Execução </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($storeContacts as $contacts): ?>
            <tr>
                <td><?= $contacts->getStore()->code?></td>
                <td><?= $contacts->name?></td>
                <td><?= $contacts->phone?></td>
                <td><?= $contacts->created_at?></td>
            </tr>
            <?php endforeach;  ?>
        </tbody>
    </table>
</div>
<style>
    h1 {
        text-align: center;
    }

    #tela {
        padding: 10%;
    }

    table
</style>