<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title; ?></title>
    <link rel="stylesheet" href="<?= url("/themes/css/bootstrap.min.css");?>">
</head>

<body>
    <table class="table table-bordered">
        <?php
            foreach ($countings as $counting):
                ?>
        <tr class="bg-light">
            <td colspan="1">Loja: <?=$counting->store_code?></td> <!-- 5-5-9-->
            <td colspan="1">Codigo: <?=$counting->code?></td>
            <td colspan="5">Descrição: <?=$counting->description?></td>
        </tr>
        <tr style="font-size:58%">
            <th>Inventario 2017</th>
            <th>Entradas 2018</th>
            <th>Saidas 2018</th>
            <th>Inventario 2018 Calculado</th>
            <th>Inventario 2018</th>
            <th>Diferença</th>
        </tr>
        <tr style="font-size:70%">
            <td><?=str_replace(".",",",$counting->inventory_quantity_1)?></td>
            <td><?=str_replace(".",",",$counting->input_3)?></td>
            <td><?=str_replace(".",",",$counting->withdrawal_3)?></td>
            <td><?=str_replace(".",",",$counting->inventory_quantity_1-$counting->withdrawal_3+$counting->input_3)?>
            </td>
            <td><?=str_replace(".",",",$counting->final_inventory_quantity)?></td>
            <td><?=str_replace(".",",",$counting->inventory_quantity_1-$counting->withdrawal_3+$counting->input_3-$counting->final_inventory_quantity)?>
            </td>
        </tr>
        <?php
    endforeach;?>
    </table>
</body>

</html>