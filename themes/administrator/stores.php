<?php 
$v->layout("administrator/adm_theme");
?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Lojas</h1>
        <p class="lead"></p>
    </div>
</div>
<div style="margin:2%">
    <button type="button" class="btn btn-primary btn-lg" style="margin-left:2%" data-bs-toggle="modal"
        data-bs-target="#atualizar" data-bs-whatever="@mdo">Atualizar Certificado</button>
</div>
<div class="container" style="margin-bottom:5%">
    <form method="post" action="/usuarios/usersSearch" class="row gx-3 gy-2 align-items-center"
        style="margin-bottom:2%">
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeInputName">Loja</label>
            <input name="store" type="text" class="form-control" id="specificSizeInputName" placeholder="Loja">
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect">Preference</label>
            <select name="group" class="form-select" id="specificSizeSelect">
                <option selected value=''>Grupo Economico ...</option>
                <?php foreach ($storeGroup as $group):
                ?>
                <option value="<?=$group->id?>"><?=$group->name?></option>
                <?php endforeach;
                ?>
            </select>
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>
<?php if($stores): ?>
<div style="margin-left:0%;margin-top:2%">
</div>
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th style="min-width:25px">Codigo</th>
            <th style="min-width:25px">Razão Social</th>
            <th style="min-width:25px">Validade Certificado</th>
            <th style="min-width:25px">Grupo</th>
            <th style="min-width:25px">Opções</th>
        </tr>
    </thead>
    <tbody>
        <?php 
    foreach ($stores as $store):
        ?>
        <tr>
            <td><?=$store->code; ?></td>
            <td><?=$store->company_name; ?></td>
            <td><?=$store->getCertValidity; ?></td>
            <td><?=$store->getGroup; ?></td>
            <td>
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Opções
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <button type="button" class="btn dropdown-item" data-bs-toggle="modal"
                            data-bs-target="#editarCadastro<?=$store->id?>" data-bs-whatever="<?=$store->id?>">Editar
                            Cadastro</button>
                    </div>
                </div>
            </td>
        </tr>
        <div class="modal fade" id="editarCadastro<?=$store->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar dados da Loja</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="editar<?=$store->id?>">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Razão Social:</label>
                                <input type="text"class="form-control" name="name" value="<?=$store->company_name?>">
                                <input type="hidden" name="id" id="id" value="<?=$store->id?>">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Grupo:</label>
                                <select name="group" class="form-control">
                                    <option value="<?=$store->group_id?>"><?=$store->getGroup?></option>
                                    <?php foreach($storeGroup as $group): 
                                ?>
                                    <option value="<?=$group->id?>"><?=$group->name?></option>
                                    <?php endforeach; 
                                ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Senha:</label>
                                <input type="text" class="form-control" name="password"
                                    value="<?=$store->getCertPassword?>">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
        $('form[name="editar<?=$store->id?>"]').submit(function (event) {
        event.preventDefault();
            $.ajax({
                url: '/lojas/update/store',
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.success === true) {
                        swal({
                            title: "Bispo & Dantas",
                            text: response.message,
                            timer: 20000,
                            icon: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            type: "success"
                        });

                    } else {
                        swal({
                            title: "Bispo & Dantas",
                            text: response.message,
                            icon: "error",
                            showCancelButton: false,
                            showConfirmButton: false,
                            type: "danger"
                        });
                    }

                }
            })
            document.location.reload(true);
        });
        </script>
        <?php
    endforeach; ?>
    </tbody>
</table>
<div style="margin-bottom:5%">
</div>
<div class="modal fade" id="atualizar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Atualizar Certificado</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="formUpload" id="formUpload" method="post">
                    <div class="form-group">
                        <label class="col-form-label">Loja:</label>
                        <input required name="store" type="number" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Nova Senha:</label>
                        <input required name="password" type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                            <input name="file" type="file" class="form-control" id="inputGroupFile01">
                        </div>
                    </div>
                    <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                        id="porcentagem">0%</span>
                    <div id="resposta" style="width:100%"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#btnEnviar').click(function () {
            $('#formUpload').ajaxForm({
                uploadProgress: function (event, position, total, percentComplete) {
                    $('progress').attr('value', percentComplete);
                    $('#porcentagem').html(percentComplete + '%');
                },
                success: function (data) {
                    $('progress').attr('value', '100');
                    $('#porcentagem').html('100%');
                    if (data.sucesso == true) {
                        $('#resposta').html(
                            '<div class="alert alert-success" role="alert">' + data
                            .msg + '</div>');
                        document.location.reload(true);
                    } else {
                        $('#resposta').html(
                            '<div class="alert alert-danger" role="alert">' + data.msg +
                            '</div>');
                    }
                },
                error: function () {
                    $('#resposta').html(
                        '<div class="alert alert-danger" role="alert">Erro ao enviar requisição!!!</div>'
                        );
                },
                dataType: 'json',
                url: '/lojas/update/cert',
                resetForm: true
            }).submit();
        })
    })
</script>


<?php
endif; ?>