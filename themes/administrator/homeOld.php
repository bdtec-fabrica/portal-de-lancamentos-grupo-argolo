<?php 
$v->layout("administrator/adm_theme");

?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Seja bem vindo <?=$usuario?>!</h1>
        <p class="lead">Seus dados de lançamento! Para dados gerais acesse <a href="<?=URL_BASE?>/home2">Aqui!</a></p>
    </div>
</div>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-3">
            <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento diario lançamentos</h1>
            <div class="col-sm-6">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-bottom:5%">
    <div class="row">
        <div class="col-lg-4 col-6">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?=$qtdAtualDiaria+0?></h3>
                    <p>Incluidos</p>
                    <p><?=$qtdAtualDiariaItens+0?> Itens</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-add"></i>
                </div>
                <a href="<?=URL_BASE?>/request/dashboard/i"
                    class="small-box-footer">Visualizar todos <i class="ion ion-android-add"></i></a>
            </div>
        </div>
        <div class="col-lg-4  col-6">
            <div class="small-box bg-secondary">
                <div class="inner">
                    <h3><?=$qtdCadastroDiaria+0?></h3>

                    <p>Cadastro</p>
                    <p><?=$qtdCadastroDiariaItens+0?> Itens</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-play"></i>
                </div>
                <a href="<?=URL_BASE?>/request/dashboard/9"
                    class="small-box-footer">Visualizar todos <i class="ion ion-ios-play"></i></a>
            </div>
        </div>
        <div class="col-lg-4 col-6">
            <div class="small-box bg-success">
                <div class="inner">
                    <h3><?=$qtdAtualizadaAtualDiaria+0?><sup style="font-size: 20px"></sup></h3>
                    <p>Concluídos</p>
                    <p><?=$qtdAtualizadaAtualDiariaItens+0?> Itens</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a href="<?=URL_BASE?>/request/dashboard/3"
                    class="small-box-footer">Visualizar todos <i class="ion ion-checkmark"></i></a>
            </div>
        </div>
        <div class="col-lg-4 col-6">
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3><?=$qtdPendenteEmpresaAtualDiaria+0?></h3>
                    <p style="font-size:95%">Pendente Empresa</p>
                    <p><?=$qtdPendenteEmpresaAtualDiariaItens+0?> Itens</p>
                </div>
                <div class="icon">
                    <i class="ion ion-alert"></i>
                </div>
                <a href="<?=URL_BASE?>/request/dashboard/1"
                    class="small-box-footer">Visualizar todos <i class="ion ion-alert"></i></a>
            </div>
        </div>
        <div class="col-lg-4 col-6">
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3><?=$qtdPendenteClienteAtualDiaria+0?></h3>
                    <p>Pendente Cliente</p>
                    <p><?=$qtdPendenteClienteAtualDiariaItens+0?> Itens</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-person"></i>
                </div>
                <a href="<?=URL_BASE?>/request/dashboard/2"
                    class="small-box-footer">Visualizar todos <i class="ion ion-android-person"></i></a>
            </div>
        </div>
        <div class="col-lg-4 col-6">
            <div class="small-box bg-cyan">
                <div class="inner">
                    <h3><?=$qtdPendenciaIcmsAtualDiaria+0?></h3>

                    <p style="font-size:95%">Pendente ICMS</p>
                    <p><?=$qtdPendenciaIcmsAtualDiariaItens+0?> Itens</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-calculator"></i>
                </div>
                <a href="<?=URL_BASE?>/request/dashboard/6"
                    class="small-box-footer">Visualizar todos <i class="ion ion-alert"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-3">
            <h1 class="m-0 text-dark" style="text-align:center">Acompanhamento Reportes</h1>
            <div class="col-sm-6">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-bottom:5%">
    <div class="row">
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <h3><?=$todayNfReportCount?></h3>
                    <p>NF reportadas hoje (Operador)</p>
                    <p></p>
                </div>
                <div class="icon">
                    <i class="ion ion ion-alert"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$todayNfReport?>"
                    class="small-box-footer">Mais Informações <i class="ion ion ion-alert"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <h3><?=$MonthlyNfReportCount?></h3>
                    <p>NF reportadas Este Mês (Operador)</p>
                    <p></p>
                </div>
                <div class="icon">
                    <i class="ion ion ion-alert"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$MonthlyNfReport?>"
                    class="small-box-footer">Mais Informações <i class="ion ion ion-alert"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <h3><?=$clientTodayNfReportCount?></h3>
                    <p>NF reportadas hoje (Cliente)</p>
                    <p></p>
                </div>
                <div class="icon">
                    <i class="ion ion ion-alert"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$clientTodayNfReport?>"
                    class="small-box-footer">Mais Informações <i class="ion ion ion-alert"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <h3><?=$clientMonthlyNfReportCount?></h3>
                    <p>NF reportadas Este Mês (Cliente)</p>
                    <p></p>
                </div>
                <div class="icon">
                    <i class="ion ion ion-alert"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$clientMonthlyNfReport?>"
                    class="small-box-footer">Mais Informações <i class="ion ion ion-alert"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <h3><?=$todayNfReportCountIndevido?></h3>
                    <p>NF reportadas hoje (Indevido)</p>
                    <p></p>
                </div>
                <div class="icon">
                    <i class="ion ion ion-alert"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$todayNfReportIndevido?>"
                    class="small-box-footer">Mais Informações <i class="ion ion ion-alert"></i></a>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <h3><?=$MonthlyNfReportCountIndevido?></h3>
                    <p>NF reportadas Este Mês (Indevido)</p>
                    <p></p>
                </div>
                <div class="icon">
                    <i class="ion ion ion-alert"></i>
                </div>
                <a href="<?=URL_BASE?>/request/<?=$MonthlyNfReportIndevido?>"
                    class="small-box-footer">Mais Informações <i class="ion ion ion-alert"></i></a>
            </div>
        </div>
    </div>
    <form method="GET" action="/daily" target="_blank">
      <button type="submit" class="btn btn-success">Realizar batimento de ponto ></button>
    </form>
    <?php if ($_COOKIE['id'] == 20000151 or $_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 180): ?>
    <form method="GET" action="/viewCheckIns" target="_blank">
      <button type="submit" class="btn btn-info">Visualizar os Batimentos do dia ></button>
    </form>
    <?php endif; ?>
    
</div>