<?php 
$v->layout("administrator/adm_theme");

?>
<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 60000);
</script>
<main class="main_content">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="/home2">Acompanhamento diario (lançamentos diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/1">Acompanhamento diario (lançamentos com
                    confêrencia)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/2">Duração media status diario (lançamentos
                    diretos)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/3">Duração media status diario (lançamentos com
                    conferência)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/4">Situações Pontuais (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/5">Acompanhamento Reportados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/6">Acompanhamento Pendente Empresa (lançamento direto)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/7">Acompanhamento Chegada Carga Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/8">Acompanhamento Atualizadas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/9">Atualizadas por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/10">Pendente cliente por Operador</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/11">Atualizadas por Operador Sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/12">Atendimento Solicitações de lançamento Por Loja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  active" aria-current="page" href="/home2/13">Atualizações por hora / usúario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/14">Incluidas e Concluidas por hora</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/15">Acompanhamento Pendente Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/16">Acompanhamento Pendente Cliente Dias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/17">Acompanhamento Pendência Comercial Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/18">Acompanhamento Conferência Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/19">Acompanhamento NFe a Atualizar Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/20">Alocadas por Usuario sem CD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/home2/21">Pendentes Prioridades Horas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home2/22">Pendentes Usuário Media</a>
            </li>
            <?php if ($_COOKIE['id'] == 1 or $_COOKIE['id'] == 148 or $_COOKIE['id'] == 150 or $_COOKIE['id'] == 152 or $_COOKIE['id'] == 180 or $_COOKIE['id'] == 20000151): ?>
            <li class="nav-item">
                <a class="nav-link" href="/home2/26">Notas Atualizadas Operador Hora</a>
            </li>
            <?php endif; ?>
        </ul>
        <section class="col-lg-12 connectedSortable ui-sortable">
            <div class="card">
                <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title">Atualizações por hora / usúario</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="d-flex">
                    </div>
                    <div class="position-relative mb-4">
                        <canvas id="visitors-chart" height="300"></canvas>
                    </div>

                    <div class="d-flex flex-row justify-content-end">
                        <?php foreach ($l as $lf):
                ?>
                        <span class="mr-2" style="font-size:80%">
                            <i style="background-color:<?=$c[$lf]?>" class="fas fa-square"></i> <?=$lf?>
                        </span>
                        <?php endforeach;
                ?>
                    </div>
                </div>
            </div>
        </section>
        <script>
            $(function () {
                'use strict'

                var ticksStyle = {
                    fontColor: '#495057',
                    fontStyle: 'bold'
                }

                var mode = 'index'
                var intersect = true

                var $visitorsChart = $('#visitors-chart')
                var visitorsChart = new Chart($visitorsChart, {
                    data: {
                        labels: [<?= $hour ?>],
                        datasets: [<?php foreach($l as $ls): ?> {
                            type: 'line',
                            data: [<?= substr($operatorCounts[$ls], 1) ?>],
                            backgroundColor: 'transparent',
                            borderColor: '<?=$c[$ls]?>',
                            pointBorderColor: '<?=$c[$ls]?>',
                            pointBackgroundColor: '<?=$c[$ls]?>',
                            pointRadius: '5',
                            pointHoverRadius: '5',
                            fill: false
                        }, <?php endforeach; ?>]
                    },
                    options: {
                        maintainAspectRatio: false,
                        tooltips: {
                            mode: mode,
                            intersect: intersect
                        },
                        hover: {
                            mode: mode,
                            intersect: intersect
                        },
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes: [{
                                gridLines: {
                                    display: true,
                                    lineWidth: '4px',
                                    color: 'rgba(0, 0, 0, .2)',
                                    zeroLineColor: 'transparent'
                                },
                                ticks: $.extend({
                                    beginAtZero: true,
                                    suggestedMax: 10
                                }, ticksStyle)
                            }],
                            xAxes: [{
                                display: true,
                                gridLines: {
                                display: true
                                },
                                ticks: ticksStyle
                            }]
                        }
                    }
                })
            })
        </script>
    </div>