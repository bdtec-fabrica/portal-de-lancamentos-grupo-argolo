<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title; ?></title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= url("/themes/css/all.min.css");?>">
    <link rel="stylesheet" href="<?= url("/themes/css/nav.css");?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?= url("/themes/css/OverlayScrollbars.min.css");?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= url("/themes/css/adminlte.min.css");?>">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= url("/themes/css/bootstrap.min.css");?>">
    <script src="<?= url("/themes/js/jquery-3.5.1.min.js");?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
        integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous">
    </script>
    <script src="<?= url("/themes/js/jquery.min.js");?>"></script>
    <script src="<?= url("/themes/js/jquery-ui.min.js");?>"></script>
    <script src="<?= url("/themes/js/bootstrap.bundle.min.js");?>"></script>
    <script src="<?= url("/themes/js/Chart.min.js");?>"></script>
    <script src="<?= url("/themes/js/sparkline.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.vmap.min.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.vmap.usa.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.knob.min.js");?>"></script>
    <script src="<?= url("/themes/js/moment.min.js");?>"></script>
    <script src="<?= url("/themes/js/daterangepicker.js");?>"></script>
    <script src="<?= url("/themes/js/tempusdominus-bootstrap-4.min.js");?>"></script>
    <script src="<?= url("/themes/js/summernote-bs4.min.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.overlayScrollbars.min.js");?>"></script>
    <script src="<?= url("/themes/js/adminlte.js");?>"></script>
    <script src="<?= url("/themes/js/dashboard.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.form.min.js");?>"></script>
    <script src="<?= url("/themes/js/sweetalert.min.js");?>"></script>
    <script src="<?= url("/themes/js/nav.js");?>"></script>
    <script src="<?= url("/themes/js/sortable.js");?>"></script>
    <script src="<?= url("/themes/js/demo.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.flot.resize.js");?>"></script>
    <script src="<?= url("/themes/js/jquery.flot.pie.js");?>"></script>
</head>

<body>
<?php if ($naocadastrada == 1):
?>
<div class="card container"><h4>Loja <?=$loja?> não cadastrada para Busca SEFAZ!</h4></div>
<?php
else:
?>
<div class="card container"><h4>Concluido!</h4>
<p><?=$baixadaPasta?> XMLs Baixados.</p>
<p><?=$salvaBanco?> NFe Salvas</p></div>
<?php
endif;
?>

</body>

</html>