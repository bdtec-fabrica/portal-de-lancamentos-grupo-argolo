<?php 
session_start();
$v->layout("administrator/adm_theme");

?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Fiscal</h1>
        <p class="lead">Gerenciamento de Rotina setor fiscal</p>
    </div>
</div>
<div style="margin:2%">
    <button type="button" class="btn btn-primary btn-lg" style="margin-left:2%" data-bs-toggle="modal"
        data-bs-target="#adicionar">+Adicionar</button>
    <button type="button" class="btn btn-primary btn-lg" data-bs-toggle="modal" data-bs-target="#exampleModal">Conferir Notas SEFAZ</button>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Conferir Notas SEFAZ</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="POST" action="/fiscal/conferenceNFESEFAZ" enctype="multipart/form-data" target="_blank">
            <div class="mb-3">
                <label for="recipient-name" class="col-form-label">Loja Sem digito:</label>
                <input type="text" class="form-control" name="store" id="recipient-name">
            </div>
            <div class="mb-3">
                <label for="recipient-name" class="col-form-label">Bases: </label>
                <select class="form-control" name="base">
                    <option value="1">MB</option>
                    <option value="2">BD</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="recipient-name" class="col-form-label">Data inicial:</label>
                <input type="date" class="form-control" name="date1" id="recipient-name">
            </div>
            <div class="mb-3">
                <label for="recipient-name" class="col-form-label">Data final:</label>
                <input type="date" class="form-control" name="date2" id="recipient-name">
            </div>

            <div class="mb-3">
                <label for="formFile" class="form-label">Importar CSV gerado na SEFAZ</label>
                <input class="form-control" type="file" name="file" id="formFile" accept=".csv">
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send message</button>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<div class="container" style="margin-bottom:5%">
    <form method="post" action="/fiscal/search" class="row gx-3 gy-2 align-items-center"
        style="margin-bottom:2%">
        <div class="col-sm-2">
            <label class="visually-hidden" for="specificSizeInputName">Loja</label>
            <select name="store" class="form-select" id="specificSizeSelect">
                <option selected value=''>Loja ...</option>
                <?php foreach ($fiscalStores as $user):
                ?>
                <option value="<?=$user->id?>"><?=$user->code." - ".$user->razao?></option>
                <?php endforeach;
                ?>
            </select>
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <select name="mes" required class="form-select" id="specificSizeSelect">
                <option selected value=''>Mês</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select>
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <select name="ano" required class="form-select" id="specificSizeSelect">
                <option selected value=''>Ano</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
                <option value="2027">2027</option>
            </select>
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect"></label>
            <select name="pendencia" class="form-select" id="specificSizeSelect">
                <option selected value=''>Pendentes</option>
                <option value="s">Sim</option>
            </select>
        </div>
        <!-- <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect">Preference</label>
            <select name="group" class="form-select" id="specificSizeSelect">
                <option selected value=''>Período ...</option>
                <?php foreach ($periods as $period):
                ?>
                <option value="<?=$period->id?>"><?=$period->name?></option>
                <?php endforeach;
                ?>
            </select>
        </div>
        <div class="col-sm-3">
            <label class="visually-hidden" for="specificSizeSelect">Preference</label>
            <select name="class" class="form-select" id="specificSizeSelect">
                <option selected value=''>Responsavel ...</option>
                <?php foreach ($users as $user):
                ?>
                <option value="<?=$user->id?>"><?=$user->name?></option>
                <?php endforeach;
                ?>
            </select>
        </div> -->
        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>

<table class="table">
    <tr>
        <th>Loja</th>
        <th>Arquivo 1º Período</th>
        <th>Inconsistência 1º Período</th>
        <th>Arquivo 2º Período</th>
        <th>Inconsistência 2º Período</th>
        <th>Arquivo 3º Período</th>
        <th>Planilha Estorno de Credito</th>
        <th>Conf. Vendas/NFC-E</th>
        <th>Inconsistência 3º Período</th>
        <th>Fechamento ICMS</th>
        <th>Arquivo ICMS Declaração </th>
        <th>Arquivo PIS/COFINS</th>
    </tr>
    <?php foreach ($stores as $store): ?>
    <tr>
        <td><a target="_blank" href="/fiscal/<?=$store->id?>/<?=$date?>" ><?=$store->code." - ".$store->razao?></a></td>
        
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="1">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="7">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->file1($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="1">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="8">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->inconsistence1($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="2">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="7">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->file2($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="2">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="8">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->inconsistence2($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="3">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="7">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->file3($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="3">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="10">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->spreadsheet($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="3">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="9">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->sailsConference($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="3">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="8">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->inconsistence3($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="3">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="11">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->ICMSClousure($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="3">
                <input type="hidden" name="type_id" value="1">
                <input type="hidden" name="activity_type_id" value="13">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->fileSendedF($store->id,$date)?></button>
            </form>
        </td>
        <td><form name="form">
                <input type="hidden" name="store_id" value="<?=$store->id?>">
                <input type="hidden" name="date" value="<?=$date?>">
                <input type="hidden" name="period_id" value="3">
                <input type="hidden" name="type_id" value="2">
                <input type="hidden" name="activity_type_id" value="12">
                <button class="btn btn-outline-secondary"  type="submit"><?=$store->fileSended($store->id,$date)?></button>
            </form>
        </td>
    </tr>
    
    <?php endforeach; ?>
</table>
<script>
                            $('form[name="form"]').submit(function (event) {
                                event.preventDefault();

                                $.ajax({
                                    url: '/fiscal/add',
                                    type: 'post',
                                    data: $(this).serialize(),
                                    dataType: 'json',
                                    success: function (response) {

                                        if (response.sucesso === true) {
                                            swal({
                                                title: "Bispo & Dantas",
                                                text: response.msg,
                                                timer: 20000,
                                                icon: "success",
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                type: "success"
                                            });
                                            document.location.reload(true);
                                        } else {
                                            swal({
                                                title: "Bispo & Dantas",
                                                text: response.msg,
                                                icon: "error",
                                                showCancelButton: false,
                                                showConfirmButton: false,
                                                type: "danger"
                                            });
                                        }

                                    }
                                })
                            });
                        </script>

<div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar Atividade</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="adicionar">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Loja:</label>
                        <select name="store" class="form-select" id="specificSizeSelect">
                            <option selected value=''>Loja ...</option>
                            <?php foreach ($fiscalStores as $user):
                ?>
                            <option value="<?=$user->id?>"><?=$user->code." - ".$user->razao?></option>
                            <?php endforeach;
                ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Período</label>
                        <select name="period" class="form-select" id="specificSizeSelect">
                            <option selected value=''>Período ...</option>
                            <?php foreach ($periods as $period):
                            ?>
                            <option value="<?=$period->id?>"><?=$period->name?></option>
                            <?php endforeach;
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Apuração:</label>
                        <select name="type" class="form-select" id="specificSizeSelect">
                            <option selected value=''>Tipo ...</option>
                            <option value="1">ICMS</option>
                            <option value="2">PIS/COFINS</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ação:</label>
                        <select name="activityType" class="form-select" id="specificSizeSelect">
                            <option selected value=''>Tipo ...</option>
                            <?php foreach ($activityType as $period):
                            ?>
                            <option value="<?=$period->id?>"><?=$period->name?></option>
                            <?php endforeach;
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Mês:</label>
                        <select name="month" required class="form-select" id="specificSizeSelect">
                            <option selected value=''>Mês</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Ano:</label>
                        <select name="year" required class="form-select" id="specificSizeSelect">
                            <option selected value=''>Ano</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('form[name="adicionar"]').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: '/fiscal/create',
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {

                if (response.success === true) {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.message,
                        timer: 600,
                        icon: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "success"
                    });
                    document.location.reload(true);
                } else {
                    swal({
                        title: "Bispo & Dantas",
                        text: response.message,
                        icon: "error",
                        showCancelButton: false,
                        showConfirmButton: false,
                        type: "danger"
                    });
                }

            }
        })
    });
</script>