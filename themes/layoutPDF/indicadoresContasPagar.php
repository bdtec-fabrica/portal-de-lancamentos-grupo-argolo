</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>


        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
            
        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }


        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }

    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Contas a pagar em aberto a partir de hoje:</span>
        <table>
            <tr>
                <th>CNPJ:</th>
                <th>Fornecedor:</th>
                <th>Número da Nota:</th>
                <th>Vencimento:</th>
                <th>Valor:</th>
            </tr>
            <?php foreach($aa1rtitu as $rtitu): ?>
            <tr>
                <td><?=$rtitu->DUP_CGC_CPF?></td>
                <td><?=$rtitu->aa2ctipo()->TIP_RAZAO_SOCIAL?></td>
                <td><?=$rtitu->DUP_TITULO?></td>
                <td><?=dateRMSToDate($rtitu->DUP_VENC_ALT)?></td>
                <td><?=$rtitu->DUP_VALOR?></td>
            </tr>
            <?php 
            $soma += $rtitu->DUP_VALOR;
        endforeach; ?>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Total a pagar: R$ <?=str_replace(',','.',$soma)?></span>
    </div>
</body>

</html>