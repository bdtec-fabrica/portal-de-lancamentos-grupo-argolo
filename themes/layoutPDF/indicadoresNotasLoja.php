</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>
        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;

        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }


        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%;text-align:center">Indicadores Notas
            Loja/Status:</span>
        <table>
            <tr>
                <th>Loja:</th>
                <th>Incluidas hoje</th>
                <th>Concluidas hoje:</th>
                <th>Pendentes:</th>
                <th>Pendente Empresa</th>
                <th>Duraçao</th>
                <th>Pendente Cliente</th>
                <th>Duraçao</th>
                <th>Aguardando Liberação</th>
                <th>Duraçao</th>
                <th>Pendencia ICMS</th>
                <th>Duraçao</th>
                <th>Cadastro</th>
                <th>Duraçao</th>
                <th>Tarefa</th>
                <th>Duraçao</th>
            </tr>
            <?php foreach($data as $row): ?>
            <tr>
                <td><?=$row['loja']?></td>
                <td><?=$row['incluida']?></td>
                <td><?=$row['concluida']?></td>
                <td><?=$row['pendente']?></td>
                <td><?=$row['1']?></td>
                <?php if ($row['1'] > 0):?>
                <td><?=round($row['1duracao']/$row['1'])?></td>
                <?php else: ?>
                <td>0</td>
                <?php endif;?>
                <td><?=$row['2']?></td>
                <?php if ($row['2'] > 0):?>
                <td><?=round($row['2duracao']/$row['2'])?></td>
                <?php else: ?>
                <td>0</td>
                <?php endif;?>
                <td><?=$row['5']?></td>
                <?php if ($row['5'] > 0):?>
                <td><?=round($row['5duracao']/$row['5'])?></td>
                <?php else: ?>
                <td>0</td>
                <?php endif;?>
                <td><?=$row['6']?></td>
                <?php if ($row['6'] > 0):?>
                <td><?=round($row['6duracao']/$row['6'])?></td>
                <?php else: ?>
                <td>0</td>
                <?php endif;?>
                <td><?=$row['9']?></td>
                <?php if ($row['9'] > 0):?>
                <td><?=round($row['9duracao']/$row['9'])?></td>
                <?php else: ?>
                <td>0</td>
                <?php endif;?>
                <td><?=$row['17']?></td>
                <?php if ($row['17'] > 0):?>
                <td><?=round($row['17duracao']/$row['17'])?></td>
                <?php else: ?>
                <td>0</td>
                <?php endif;?>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</body>

</html>