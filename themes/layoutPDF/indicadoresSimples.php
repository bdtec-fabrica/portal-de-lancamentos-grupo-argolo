</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>


        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
            
        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }


        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }

    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%;text-align:center">Compras Simples Nacional sem Aproveitamento de Credito:</span>
        <table>
            <tr>
                <th>Referencia:</th>
                <th>Descrição</th>
                <th>NCM:</th>
                <th>CFOP:</th>
                <th>Quantidade</th>
                <th>Valor</th>
                <th>Base</th>
                <th>Aliquota</th>
                <th>Valor de ICMS</th>
                <th>Número</th>
                <th>Razão</th>
                <th>Emissão</th>
                <th>Chave</th>
            </tr>
            <?php foreach($linhas as $linha): ?>
            <tr>
                <td><?=$linha['referencia']?></td>
                <td><?=$linha['descricao']?></td>
                <td><?=$linha['ncm']?></td>
                <td><?=$linha['cfop']?></td>
                <td><?=str_replace('.',',',$linha['quantidade'])?></td>
                <td><?=str_replace('.',',',$linha['valor'])?></td>
                <td><?=str_replace('.',',',$linha['base'])?></td>
                <td><?=$linha['aliquota']?></td>
                <td><?=str_replace('.',',',$linha['valorIcms'])?></td>
                <td><?=str_replace('.',',',$linha['numero'])?></td>
                <td><?=str_replace('.',',',$linha['razao'])?></td>
                <td><?=str_replace('.',',',$linha['emissao'])?></td>
                <td><?=str_replace('.',',',$linha['chave'])?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</body>

</html>