</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>


        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
            
        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }


        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }

    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Comparativo Vendas Desossa (semana passada):</span>
        <table>
            <tr>
                <th>Compras:</th>
                <th>Vendas:</th>
                <th>Participação das Compras nas Vendas:</th>
            </tr>
            <tr>
                <td><?=$compra->VALOR?></td>
                <td><?=$venda->VALOR?></td>
                <td><?=round(($compra->VALOR/$venda->VALOR)*100,2)?>%</td>
            </tr>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Comparativo Vendas Horti (semana passada):</span>
        <table>
            <tr>
                <th>Compras:</th>
                <th>Vendas:</th>
                <th>Participação das Compras nas Vendas:</th>
            </tr>
            <tr>
                <td><?=$compra2->VALOR?></td>
                <td><?=$venda2->VALOR?></td>
                <td><?=round(($compra2->VALOR/$venda2->VALOR)*100,2)?>%</td>
            </tr>
        </table>
    </div>
</body>

</html>