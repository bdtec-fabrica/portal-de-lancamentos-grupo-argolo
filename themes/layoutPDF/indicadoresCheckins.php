</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>
        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;

        }

        table {
            text-align: center;
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }

        td {
            border: 1px solid #555555;
            text-transform: uppercase;
        }

        th {
            border: 1px solid #555555;
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div>
        
        <?php
    foreach($operators as $operator):
        ?>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%;text-align:center"><?=$operator?>:</span>
            <table>
                <tr>
                    <th style="width:300px">Dia</th>
                    <?php
            $count = 1;
            $a = array(1,2,3,4);
        foreach($a as $daily):
        ?>
                    <th><?=$count?>ª Batida</th>
                    <?php
            $count++;
        endforeach;
        ?>
                    <th>Quantidade de Horas Trabalhadas
                </tr>
                </tr>
                <?php
        foreach($dailyOperators[$operator] as $daily):
        ?>
                <tr>
                    <td><?=  substr($daily->created_at,0,10)?></td>
                    <?php
        $calc = 1;
        foreach($daily->getCheckIns as $checkIn):
        ?>
                    <td><?= substr($checkIn->created_at,11) ?></td>
                    <?php
        $calc++;
        endforeach;
        for($calc=$calc;$calc<=4;$calc++):
        ?>
                    <td></td>
                    <?php
        endfor;
        ?>
                    <td>
                        <?php 
                
                if ($soma == null):
                    $soma = $daily->countHours($daily->user_id,substr($daily->created_at,0,10));
                    
                else:
                    
                    $soma = somarHoras($daily->countHours($daily->user_id,substr($daily->created_at,0,10)),$soma);
                    
                endif;
                echo $daily->countHours($daily->user_id,substr($daily->created_at,0,10))?>
                    </td>
                    </td>
                </tr>
                <?php
        endforeach;
        ?>
                <tr>
                    <th>Total de Horas</th>
                    <th><?=$soma?></th>
                </tr>
            </table>
        <?php
    $soma = null;
    endforeach;
    ?>
    </div>
</body>

</html>