</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>


        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
            
        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }


        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }

    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%;text-align:center">Produtos Sem giro nos ultimos 90 dias:</span>
        <table>
            <tr>
                <th>Codigo:</th>
                <th>Descrição</th>
                <th>Ultima Entrada:</th>
                <th>Ultima Saída:</th>
                <th>Estoque</th>
                <th>Custo de Ultima Entrada</th>
                <th>Preço</th>
            </tr>
            <?php foreach($aa2cestq as $cestq): ?>
            <tr>
                <td><?=$cestq->GET_COD_PRODUTO?></td>
                <td><?=str_replace('.',',',$cestq->getAA3CITEM()->GIT_DESCRICAO)?></td>
                <td><?=str_replace('.',',',$cestq->dtUltEnt)?></td>
                <td><?=str_replace('.',',',$cestq->dtUltFat)?></td>
                <td><?=str_replace('.',',',$cestq->GET_ESTOQUE)?></td>
                <td><?=str_replace('.',',',$cestq->GET_CUS_ULT_ENT)?></td>
                <td><?=str_replace('.',',',$cestq->getPrice())?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</body>

</html>