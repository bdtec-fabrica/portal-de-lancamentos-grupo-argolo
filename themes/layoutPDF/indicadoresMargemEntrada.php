</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>
        body {
            padding: 30px;
            font-size: small;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;

        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }


        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div>
        <span style="margin-bottom:5%;font-size:200%;margin-top:5%;text-align:center"> Produtos com margem menor que 0 ou maior que 50</span> <table>
                <thead>
                    <tr>
                        <th>Loja:</th>
                        <th>Nota</th>
                        <th>Razão Social</th>
                        <th>Codigo:</th>
                        <th>Descrição</th>
                        <th>Custo</th>
                        <th>Embalagem informada</th>
                        <th>Qtd Embalagem informada</th>
                        <th>Preço</th>
                        <th>Data do Preço</th>
                        <th>Margem</th>
                        <th>Ultima Saida</th>
                        <th>Operador</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                if (isset($consinco) && $consinco === true):
                    foreach($compras as $compra):
                             ?>
                    <tr>
                        <td><?=$compra['ESCLC_CODIGO']?></td>
                        <td><?=$compra['ESCHC_NRO_NOTA3']?></td>
                        <td><?=$compra['TIP_CODIGO']." ".$compra['TIP_RAZAO_SOCIAL']?></td>
                        <td><?=$compra['GIT_COD_ITEM']?></td>
                        <td><?=$compra['GIT_DESCRICAO']?></td>
                        <td><?=str_replace('.',',',$compra['ENTSAIC_CUS_UN'])?></td>
                        <td>1</td>
                        <td><?=str_replace('.',',',$compra['ENTSAIC_QTD_UN'])?></td>
                        <td><?=str_replace('.',',',$compra['PRE_PRECO'])?></td>
                        <td><?=$compra['PRE_DAT_INICIO']?></td>
                        <td><?=str_replace('.',',',$compra['MARGEM'])?></td>
                        <td><?=$compra['GET_DT_ULT_FAT']?></td>
                        <td><?=$compra['FIS_USUARIO_INC']?></td>
                    </tr>
                    <?php 
                        
                    endforeach; 
                else:
                    foreach($compras as $compra):
                        if($portal->find("code = $compra->ESCLC_CODIGO")->count() > 0): ?>
                <tr>
                    <td><?=$compra->ESCLC_CODIGO?></td>
                    <td><?=$compra->ESCHC_NRO_NOTA3?></td>
                    <td><?=$compra->TIP_CODIGO.'-'.$compra->TIP_DIGITO." ".$compra->TIP_RAZAO_SOCIAL?></td>
                    <td><?=$compra->GIT_COD_ITEM.'-'.$compra->GIT_DIGITO?></td>
                    <td><?=$compra->GIT_DESCRICAO?></td>
                    <td><?=str_replace('.',',',$compra->ENTSAIC_CUS_UN)?></td>
                    <td><?=str_replace('.',',',$compra->ENTSAIC_TPO_EMB)?></td>
                    <td><?=str_replace('.',',',$compra->ENTSAIC_BASE_EMB)?></td>
                    <td><?=str_replace('.',',',$compra->PRE_PRECO)?></td>
                    <td><?=dateRMSToDate($compra->PRE_DAT_INICIO)?></td>
                    <td><?=str_replace('.',',',$compra->MARGEM)?></td>
                    <td><?=$compra->GET_DT_ULT_FAT?></td>
                    <td><?=$compra->FIS_USUARIO_INC?></td>
                </tr>
                <?php 
                        endif;
                    endforeach; 
                endif;
                ?>
                </tbody>
                </table>
    </div>
</body>

</html>