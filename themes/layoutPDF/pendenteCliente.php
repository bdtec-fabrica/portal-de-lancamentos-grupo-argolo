<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>
        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;

        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }

        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Notas em pendente cliente a mais
            de 24 horas:</span>
            <?php foreach($registers1 as $register): ?>
        <table>
            <tr>
                <th>#</th>
                <th>Loja</th>
                <th>Número</th>
                <th>Inclusão</th>
                <th>Cliente</th>
                <th>Fornecedor</th>
                <th>Tipo</th>
                <th>Grupo</th>
                <th>Qtd</th>
                <th>Alteração</th>
                <th>Operador</th>
                <th>Operação</th>
                <th>Status</th>
            </tr>

            <tr>
                <td><?=$register['id']?></td>
                <td><?=$register['loja']?></td>
                <td><?=$register['numero']?></td>
                <td><?=$register['inclusao']?></td>
                <td><?=$register['cliente']?></td>
                <td><?=$register['fornecedor']?></td>
                <td><?=$register['tipo']?></td>
                <td><?=$register['grupo']?></td>
                <td><?=$register['qtd']?></td>
                <td><?=$register['alteracao']?></td>
                <td><?=$register['operador']?></td>
                <td><?=$register['operacao']?></td>
                <td><?=$register['status']?></td>
            </tr>
            
        </table>
        <table style="margin-bottom: 5%;">
            <tr>
                <th>Usuario</th>
                <th>Mensagem</th>
                <th>Inclusão</th>

            </tr>
            <?php foreach($registers2 as $register2): ?>
            <tr>

                <td><?=$register2[$register['id']]['user']?></td>
                <td><?=$register2[$register['id']]['msg']?></td>
                <td><?=$register2[$register['id']]['interactionDate']?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endforeach; ?>
    </div>
</body>

</html>