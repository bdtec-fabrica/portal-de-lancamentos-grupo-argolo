</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>


        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
            
        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }


        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }

    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Produtos Com cobertura abaixo de 7 dias:</span>
        <table>
            <tr>
                <th>Codigo:</th>
                <th>Descrição:</th>
                <th>Ultima Saida:</th>
                <th>Quantidade Estoque:</th>
                <th>Cobertura</th>
                <th>Ultima entrada:</th>
            </tr>
            <?php foreach($registers1 as $register1): ?>
            <tr>
                <td><?=$register1['codigo']?></td>
                <td><?=$register1['descricao']?></td>
                <td><?=str_replace('.',',',$register1['dtUltFat'])?></td>
                <td><?=str_replace('.',',',$register1['quantidade'])?></td>
                <td><?=str_replace('.',',',$register1['cobertura'])?></td>
                <td><?=str_replace('.',',',$register1['dtUltEnt'])?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</body>

</html>