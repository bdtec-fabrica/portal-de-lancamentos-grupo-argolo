</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>
        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;

        }

        table {
            text-align: center;
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }

        td {
            border: 1px solid #555555;
            text-transform: uppercase;
        }

        th {
            border: 1px solid #555555;
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%;text-align:center">Notas atualizadas por operador e por Hora::</span>
        <table>
            <thead>
                <tr>
                    <th scope="col">Operador</th>
                    <th scope="col">Entrada</th>
                    <th scope="col">Almoço</th>
                    <th scope="col">Retorno</th>
                    <th scope="col">Saida</th>
                    <th scope="col" colspan="2">06:00 Notas/Itens</th>
                    <th scope="col" colspan="2">07:00 Notas/Itens</th>
                    <th scope="col" colspan="2">08:00 Notas/Itens</th>
                    <th scope="col" colspan="2">09:00 Notas/Itens</th>
                    <th scope="col" colspan="2">10:00 Notas/Itens</th>
                    <th scope="col" colspan="2">11:00 Notas/Itens</th>
                    <th scope="col" colspan="2">12:00 Notas/Itens</th>
                    <th scope="col" colspan="2">13:00 Notas/Itens</th>
                    <th scope="col" colspan="2">14:00 Notas/Itens</th>
                    <th scope="col" colspan="2">15:00 Notas/Itens</th>
                    <th scope="col" colspan="2">16:00 Notas/Itens</th>
                    <th scope="col" colspan="2">17:00 Notas/Itens</th>
                    <th scope="col" colspan="2">18:00 Notas/Itens</th>
                    <th scope="col" colspan="2">19:00 Notas/Itens</th>
                    <th scope="col" colspan="2">20:00 Notas/Itens</th>
                    <th scope="col" colspan="2">21:00 Notas/Itens</th>
                    <th scope="col" colspan="2">22:00 Notas/Itens</th>
                    <th scope="col" colspan="2">23:00 Notas/Itens</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($rows as $row): ?>
                <tr>
                    <th scope="row"><?=$row['operator']?></th>
                    <td><?=$user->getCheckIns(date('Y-m-d'),$row['id'])[1]?></td>
                    <td><?=$user->getCheckIns(date('Y-m-d'),$row['id'])[2]?></td>
                    <td><?=$user->getCheckIns(date('Y-m-d'),$row['id'])[3]?></td>
                    <td><?=$user->getCheckIns(date('Y-m-d'),$row['id'])[4]?></td>
                    <td><?=$row['quantidade_06']?></td>
                    <td><?=$row['itens_06']?></td>
                    <td><?=$row['quantidade_07']?></td>
                    <td><?=$row['itens_07']?></td>
                    <td><?=$row['quantidade_08']?></td>
                    <td><?=$row['itens_08']?></td>
                    <td><?=$row['quantidade_09']?></td>
                    <td><?=$row['itens_09']?></td>
                    <td><?=$row['quantidade_10']?></td>
                    <td><?=$row['itens_10']?></td>
                    <td><?=$row['quantidade_11']?></td>
                    <td><?=$row['itens_11']?></td>
                    <td><?=$row['quantidade_12']?></td>
                    <td><?=$row['itens_12']?></td>
                    <td><?=$row['quantidade_13']?></td>
                    <td><?=$row['itens_13']?></td>
                    <td><?=$row['quantidade_14']?></td>
                    <td><?=$row['itens_14']?></td>
                    <td><?=$row['quantidade_15']?></td>
                    <td><?=$row['itens_15']?></td>
                    <td><?=$row['quantidade_16']?></td>
                    <td><?=$row['itens_16']?></td>
                    <td><?=$row['quantidade_17']?></td>
                    <td><?=$row['itens_17']?></td>
                    <td><?=$row['quantidade_18']?></td>
                    <td><?=$row['itens_18']?></td>
                    <td><?=$row['quantidade_19']?></td>
                    <td><?=$row['itens_19']?></td>
                    <td><?=$row['quantidade_20']?></td>
                    <td><?=$row['itens_20']?></td>
                    <td><?=$row['quantidade_21']?></td>
                    <td><?=$row['itens_21']?></td>
                    <td><?=$row['quantidade_22']?></td>
                    <td><?=$row['itens_22']?></td>
                    <td><?=$row['quantidade_23']?></td>
                    <td><?=$row['itens_23']?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</body>

</html>