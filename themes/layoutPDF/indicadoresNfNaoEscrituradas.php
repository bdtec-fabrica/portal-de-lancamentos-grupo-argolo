</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>


        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
            
        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }


        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }

    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%;text-align:center">NFE não escrituradas:</span>
        <table>
        <tr>
                <th scope="col">Número NFe</th>
                <th scope="col">Razão</th>
                <th scope="col">UF</th>
                <th scope="col">Emissão</th>
                <th scope="col">Chave de Acesso</th>
                <th scope="col">Natureza</th>
                <th scope="col">Dias</th>
                <th scope="col">Valor</th>
                <th scope="col">A Pagar</th>
            </tr>
        <tbody>
            <?php foreach ($linhas as $linha): ?>
            <tr>
                <td><?=$linha['numero']?></td>
                <td><?=$linha['razao']?></td>
                <td><?=$linha['uf']?></td>
                <td><?=$linha['emissao']?></td>
                <td><?=$linha['chave']?></td>
                <td><?=$linha['natureza']?></td>
                <td><?=$linha['dias']?></td>
                <td><?=number_format($linha['valor'],2,",",".")?></td>
                <td><?=number_format($linha['pagar'],2,",",".")?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
    </div>
</body>

</html>