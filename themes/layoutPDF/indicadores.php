
</html>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <style>


        body {
            padding: 30px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
            
        }

        table {
            width: 100%;
            border: 1px solid #555555;
            margin: 0;
            padding: 0;
        }

        th {
            text-transform: uppercase;
        }

        table,
        th,
        td {
            border: 1px solid 5555555;
            border-collapse: collapse;
            text-align: left;
            padding: 10px;
        }

        tr:nth-child(2n+0) {
            background: #eeeeee;
        }

        p {
            color: #888888;
            margin-top: 20px;
            text-align: center;
        }

    </style>
</head>

<body>
    <div>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Produtos Vendidos dia anterior com Margem abaixo de 5%:</span>
        <table>
            <tr>
                <th>Codigo:</th>
                <th>Descrição:</th>
                <th>Custo:</th>
                <th>Preço:</th>
                <th>Margem:</th>
            </tr>
            <?php foreach($registers1 as $register): ?>
            <tr>
                <td><?=$register['codigo']?></td>
                <td><?=$register['descricao']?></td>
                <td><?=str_replace('.',',',$register['custo'])?></td>
                <td><?=str_replace('.',',',$register['preco'])?></td>
                <td><?=str_replace('.',',',$register['margem'])?>%</td>
            </tr>
            <?php endforeach; ?>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Acumulado de vendas dia anterior X Venda do dia anterior no ano anterior:</span>
        <table>
            <tr>
                <th>Venda dia anterior:</th>
                <th>Venda dia anterior ano passado:</th>

            </tr>
            <tr>
                <td>R$ <?=str_replace('.',',',round($ag1iensa2->VALOR,2))?></td>
                <td>R$ <?=str_replace('.',',',round($ag1iensa3->VALOR,2))?></td>
            </tr>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Venda mês atual x Venda projetada para o mês:</span>
        <table>
            <tr>
                <th>Venda mês atual:</th>
                <th>Venda projetada:</th>

            </tr>
            <tr>
                <td>R$ <?=str_replace('.',',',round($ag1iensa4->VALOR,2))?></td>
                <td>R$ <?=str_replace('.',',',round($projecao,2))?></td>
            </tr>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Clientes dia anterior x Ticket Medio dia anterior - Clientes Esse Mês x Ticket Medio Esse Mês:</span>
        <table>
            <tr>
                <th>Clientes dia anterior:</th>
                <th>Ticket Medio dia Anterior:</th>
                <th>Clientes esse Mês:</th>
                <th>Ticket Medio esse Mês:</th>

            </tr>
            <tr>
                <td><?=$capcupom->QUANTIDADE?></td>
                <td><?=$capcupom->TICKET?></td>
                <td><?=$capcupom2->QUANTIDADE?></td>
                <td><?=$capcupom2->TICKET?></td>
            </tr>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Venda por forma de pagamento no dia anterior:</span>
        <table>
            <tr>
                <th>Forma de pagamento:</th>
                <th>Valor da venda:</th>
                <th>Percentual sobre as vendas</th>
            </tr>
            <?php
            foreach($AGGFLSFIN as $AGGFLSFIN):
            ?>
            <tr>
                <td><?=finalizaMB($AGGFLSFIN->CD_FIN)?></td>
                <td><?=str_replace(".",',',$AGGFLSFIN->VL_VDA)?></td>
                <td><?=str_replace(".",",",round(($AGGFLSFIN->VL_VDA/$ag1iensa2->VALOR)*100,2))?></td>
            </tr>
            <?php endforeach;
            ?>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">20 produtos mais vendidos dia anterior:</span>
        <table>
            <tr>
                <th>Codigo:</th>
                <th>Descrição:</th>
                <th>Preço:</th>
                <th>Quantidade:</th>
                <th>Margem</th>
            </tr>
            <?php
            $count = 1; 
            foreach($registers2 as $register):
                if ($count > 20):
                    break;
                endif; 
            ?>
            <tr>
                <td><?=$register['codigo']?></td>
                <td><?=$register['descricao']?></td>
                <td><?=str_replace('.',',',$register['preco'])?></td>
                <td><?=str_replace('.',',',$register['quantidade'])?></td>
                <td><?=str_replace('.',',',$register['margem'])?>%</td>
            </tr>
            <?php
            $count++;
            endforeach; ?>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Compras do mês atual x representação nas vendas:</span>
        <table>
            <tr>
                <th>Compras mês Atual:</th>
                <th>Representação nas Vendas:</th>
            </tr>
            <tr>
                
                <td><?=str_replace(".",",",round($ag1iensa6->VALOR,2))?></td>
                <?php if ($ag1iensa4->VALOR == 0): ?>
                <td>0</td>
                <?php else: ?>
                    <td><?=str_replace(".",",",round(($ag1iensa6->VALOR/$ag1iensa4->VALOR)*100,2))?></td>
                <?php endif; ?>
            </tr>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Compra mês Anterior representação nas vendas:</span>
        <table>
            <tr>
                <th>Compras mês Anterior:</th>
                <th>Representação nas Vendas:</th>
            </tr>
            <tr>
                <td><?=str_replace(".",",",round($ag1iensa7->VALOR,2))?></td>
                <?php if ($ag1iensa72->VALOR > 0): ?>
                <td><?=str_replace(".",",",round(($ag1iensa7->VALOR/$ag1iensa72->VALOR)*100,2))?></td>
                <?php else: ?>
                <td>0</td>
                <?php endif; ?>
            </tr>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Compras mês Atual ano Anterior:</span>
        <table>
            <tr>
                <th>Compras mês Anterior:</th>
                <th>Representação nas Vendas:</th>
            </tr>
            <tr>
                <td><?=str_replace(".",",",round($ag1iensa8->VALOR,2))?></td>
                <?php if ($ag1iensa82->VALOR == 0) : ?>
                <td>0</td>
                <?php else: ?>
                <td><?=str_replace(".",",",round(($ag1iensa8->VALOR/$ag1iensa82->VALOR)*100,2))?></td>
                <?php endif; ?>
            </tr>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Vendas Ultimos 30 dias:</span>
        <table>
            <tr>
                <th>Dia:</th>
                <th>Valor Venda:</th>
                <th>Margem media:</th>
            </tr>
            <?php foreach($vendas as $venda):?>
            <tr>
                <td><?=dateRMSToDate($venda->ESCHC_DATA3)?></td>
                <td><?=str_replace('.',',',$venda->VENDA)?></td>
                <td><?=str_replace('.',',',$venda->MARGEM)?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Comparação das vendas 30 dias:</span>
        <table>
            <tr>
                <th>Dia da semana/Data:</th>
                <th>Valor:</th>
                <th>Dia da semana/Data:</th>
                <th>Valor:</th>
            </tr>
            <?php foreach($comparativo as $venda): ?>
            <tr>
                <td><?=$venda['DATA_ATUAL']?></td>
                <td><?=$venda['VALOR_ATUAL']?></td>
                <td><?=$venda['DATA_ANTERIOR']?></td>
                <td><?=$venda['VALOR_ANTERIOR']?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Comparativo Vendas Desossa (semana passada):</span>
        <table>
            <tr>
                <th>Compras:</th>
                <th>Vendas:</th>
                <th>Participação das Compras nas Vendas:</th>
            </tr>
            <tr>
                <td><?=$compra->VALOR?></td>
                <td><?=$venda3->VALOR?></td>
                <?php if ($venda3->VALOR > 0): ?>
                <td><?=round(($compra->VALOR/$venda3->VALOR)*100,2)?>%</td>
                <?php else: ?>
                <td>0</td>
                <?php endif; ?>
            </tr>
        </table>
        <span style="margin-bottom: 5%;font-size:200%;margin-top:5%text-align:center">Comparativo Vendas Horti (semana passada):</span>
        <table>
            <tr>
                <th>Compras:</th>
                <th>Vendas:</th>
                <th>Participação das Compras nas Vendas:</th>
            </tr>
            <tr>
                <td><?=$compra2->VALOR?></td>
                <td><?=$venda2->VALOR?></td>
                <?php if ($venda2->VALOR > 0): ?>
                <td><?=round(($compra2->VALOR/$venda2->VALOR)*100,2)?>%</td>
                <?php else: ?>
                <td>0</td>
                <?php endif; ?>
            </tr>
        </table>
    </div>
</body>

</html>