<?php 
$v->layout("administrator/adm_theme");

?>
<main class="main_content">
    <div class="container">
        <form action="/fiscal-stock/view/comparacao" method="post">
            <div class="container card-header" style="text-align: left;">
                <h5>Verificação Movimentações não Fiscais</h5>
            </div>
            <div class="row card-body">
                <div class="form-group col-md-2">
                    <span>Data inicial</span>
                    <input type="hidden" name="id" value="<?=$ic->id?>">
                    <input name="date1" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Data final</span>
                    <input name="date2" style="font-size:80%" class="form-control" type="date">
                </div>
                <select name="base" class="form-control">
                    <option selected disabled>Selecione a BASE...</option>
                    <option value="1">Mix Bahia</option>
                    <option value="2">Bispo e Dantas</option>
                    <option value="3">Novo Mix</option>
                    <option value="4">Unimar</option>
                    <option value="7">Bem Barato</option>
                </select>
            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <table class="table table-striped table-light sortable">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Descriminção</th>
                <th>Quantidade</th>
                <?php if ($entrada): ?>
                <th>Entradas não fiscais</th>
                <?php endif;?>
                <?php if ($saida): ?>
                <th>Saidas não fiscais</th>
                <?php endif;?>
                <?php if ($entrada): ?>
                <th>Nova Quantidade</th>
                <?php endif;?>
                <th>Und</th>
                <th>Unitario</th>
            </tr>
        </thead>
        <tbody>
            <?php if (is_array($ic->inventarioContabilItens())):
                        foreach($ic->inventarioContabilItens() as $i):
                ?>
            <tr class=" a <?php if ($saida[$i->codigo]+$i->quantidade-$entrada[$i->codigo] <= 0):
                        echo "table-danger"; 
                    endif;?>">
                <th scope="row "><?=$i->codigo?></th>
                <td><?=$i->discriminacao?></td>
                <td><?=str_replace(".",",",$i->quantidade)?></td>
                <?php if ($entrada): ?>
                <td><?=str_replace(".",",",round($entrada[$i->codigo],2))?></td>
                <?php endif;?>
                <?php if ($saida): ?>
                <td><?=str_replace(".",",",round($saida[$i->codigo],2))?></td>
                <?php endif;?>
                <?php if ($saida): ?>
                <td><?=str_replace(".",",",round($saida[$i->codigo]+$i->quantidade-$entrada[$i->codigo],2))?></td>
                <?php endif;?>
                <td><?=$i->un?></td>
                <td><?=str_replace(".",",",$i->unitario)?></td>
                <?php unset($saida[$i->codigo]);
                          unset($entrada[$i->codigo]);
                          ?>
                <!-- <td>
                        <div class="btn-group">
                            <button type="button" class="btn  dropdown-toggle" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                Opções
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#" data-bs-toggle="modal"
                                        data-bs-target="#exampleModal">Visualizar</a></li>
                            </ul>
                        </div>
                    <td> -->
            </tr>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                <h5> <a style="font-size:60%;color:blue">
                                    </a></h5>
                                <p></p>
                            </div>

                            <div class="mb-3">
                                <img style="width:350px;margin-bottom:1%;" src="" class="card-img-top" alt="...">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;
            endif;

            ?>
             <?php
             
            foreach ($entrada as $key => $value): 
                $itemsFora[$key] = $key;
            endforeach;
            foreach ($saida as $key => $value): 
                $itemsFora[$key] = $key;
            endforeach;

            ?>
                    <?php foreach ($itemsFora as $i): 
                        if ($saida[$i]-$entrada[$i] > 0):?>
                    <tr>
                        <td><?=$i?></td>
                        <td><?=$aa3citem->getAA3CITEM2($i)->GIT_DESCRICAO?></td>
                        <td>0</td>
                        <td><?=str_replace(".",",",round($entrada[$i],2))?></td>
                        <td><?=str_replace(".",",",round($saida[$i],2))?></td>
                        <td><?=str_replace(".",",",round($saida[$i]-$entrada[$i],2))?></td>
                        <td><?=$aa3citem->getAA3CITEM2($i)->GIT_TPO_EMB_VENDA?></td>
                        <td><?=str_replace(".",",",$ag1iensa->getLastCost($periodo, $i, $store))?></td>
                    </tr>

                <?php
            endif;
             endforeach; ?>
                </tbody>
            </table>

            