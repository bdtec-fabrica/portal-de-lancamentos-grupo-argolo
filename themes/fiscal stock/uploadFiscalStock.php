<?php 
$v->layout("administrator/adm_theme");
?>
<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Importação Inventario Contabil</h1>
                <p class="lead">Importa Inventario Contabil no layout: "Class. Fiscal", "Codigo", "Discriminacao",
                    "Quantidade", "Und", "Unitario", "Parcial", "Total"</p>
            </div>
        </div>
    </div>
    <div class="container" style="width:500px">
        <form name="formUpload" id="formUpload" method="post">
            <div class="form-group">
                <div class="input-group mb-3">
                    <input name="loja" placeholder="Codigo da Loja sem o digito" type="number" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group mb-3">
                    <input name="data" type="date" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group mb-3">
                    <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                    <input name="file" type="file" class="form-control" id="inputGroupFile01">
                </div>
            </div>
            <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                id="porcentagem">0%</span>
            <div id="resposta" style="width:100%"></div>
            <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
        </form>
    </div>
    <div style="margin-top: 5%;">
        <table class="table table-striped table-light">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Loja</th>
                    <th>Operador</th>
                    <th>Data</th>
                    <th>Inclusão no sistema</th>
                </tr>
            </thead>
            <tbody>
                <?php if (is_array($ic)):
                        foreach($ic as $i):
                ?>
                <tr>
                    <th scope="row"><a target="_blank" href="<?=URL_BASE?>/fiscal-stock/view/<?=$i->id?>"><?=$i->id?></a></th>
                    <td><?=$i->store_code?></td>
                    <td><?=$i->userOperator()->name?></td>
                    <td><?=$i->date?></td>
                    <td><?=$i->created_at?></td>
                    <!-- <td>
                        <div class="btn-group">
                            <button type="button" class="btn  dropdown-toggle" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                Opções
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#" data-bs-toggle="modal"
                                        data-bs-target="#exampleModal">Visualizar</a></li>
                            </ul>
                        </div>
                    <td> -->
                </tr>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-3">
                                    <h5> <a style="font-size:60%;color:blue">
                                           </a></h5>
                                    <p></p>
                                </div>
                                
                                <div class="mb-3">
                                    <img style="width:350px;margin-bottom:1%;" src=""
                                        class="card-img-top" alt="...">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach;
            endif;
            ?>
            </tbody>
        </table>
    </div>
</main>
<script>
    $(document).ready(function () {
        $('#btnEnviar').click(function () {
            $('#formUpload').ajaxForm({
                uploadProgress: function (event, position, total, percentComplete) {
                    $('progress').attr('value', percentComplete);
                    $('#porcentagem').html(percentComplete + '%');
                },
                success: function (data) {
                    $('progress').attr('value', '100');
                    $('#porcentagem').html('100%');
                    if (data.sucesso == true) {
                        $('#resposta').html(
                            '<div class="alert alert-success" role="alert">' +
                            data.msg + '</div>');
                        document.location.reload(true);
                    } else {
                        $('#resposta').html(
                            '<div class="alert alert-danger" role="alert">' +
                            data.msg + '</div>');
                    }
                },
                error: function () {
                    $('#resposta').html(
                        '<div class="alert alert-danger" role="alert">Erro ao enviar requisição!!!</div>'
                    );
                },
                dataType: 'json',
                url: '/fiscal-stock/import',
                resetForm: true
            }).submit();
        })
    })
</script>