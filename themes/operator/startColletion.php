<?php 
session_start();
$v->layout("operator/operator_theme");
?>
<!-- <div class="jumbotron jumbotron-fluid">
  <div class="container text-center">
    <h1 class="display-4">Conferência Cega BDTec</h1>
    <p class="lead"></p>
  </div>
</div> -->

<?php if ($paginator): ?>
<h5 style="margin-left:2%;margin-top:5%">Página <?= $paginator->page(); ?> de <?= $paginator->pages(); ?> </h5>
<?php endif; ?>

  <?php $i = 0;
foreach($items as $item):
    
?>
<form name="digitar-conferencia">
  <div class="container" style="text-align:center;font-size:100%">
    <div class="row row-cols-1 border">
      <div class="col-xl bg-dark rounded-3" style="padding:2%">
        Referencia
      </div>
      <div class="col border rounded-3" style="padding:2%">
        <?=$item->reference?>
      </div>
      <div class="col bg-dark rounded-3" style="padding:2%">
        EAN NFe
      </div>
      <div class="col border rounded-3" style="padding:2%">
        <?=$item->ean?>
      </div>
      <div class="col bg-dark rounded-3" style="padding:2%">
        Descrição NFe
      </div>
      <div class="col border rounded-3" style="padding:2%">
        <?=$item->description?>
      </div>
      <div class="col bg-dark rounded-3" style="padding:2%">
        Embalagem NFe
      </div>
      <div class="col border rounded-3" style="padding:2%">
        <?=$item->packing?>
      </div>
      <div class="col bg-dark rounded-3" style="padding:2%">
        Quantidade da Embalagem Encontrada
      </div>
      <div class="col border rounded-3" style="padding:1%">
        <input type="hidden" name="id" value="<?=$id?>">
        <input value="<?=$item->getPackingQuantity()?>" class="form-control" type="number" name="packing_quantity" step="any">
      </div>
      <div class="col bg-dark rounded-3" style="padding:2%">
        EAN Encontrado
      </div>
      <div class="col border rounded-3" style="padding:1%">
        <input style="font-size:80%" type="hidden" name="code" value="<?=$item->id?>">
        <input  value="<?=$item->getEan()?>" class="form-control" type="number" name="ean_found" step="any">
      </div>
      <div class="col bg-dark rounded-3" style="padding:2%">
        Quantidade Encontrada
      </div>
      <div class="col border rounded-3" style="padding:1%">
        <input type="hidden" name="id" value="<?=$id?>">
        <input value="<?=$item->getQuantity()?>" class="form-control" type="number" name="quantity_found" step="any">
      </div>
      
    </div>
  </div>
  <ul class="nav justify-content-center" style="margin-top:4%;margin-bottom:4%">
  <button type="submit" class="btn btn-lg btn-success">Salvar</button>
</ul>
</form>

  <?php
  endforeach;
?>
  <?php if ($paginator): 
    ?>
  <div class="container"><?= $paginator->render();?><div>
      <?php endif;
    ?>

<ul class="nav justify-content-center" style="margin-top:4%;margin-bottom:4%">
<form action="/nfSefaz" method="get">
  <input type="hidden" name="concluir" value="<?=$id?>">
  <button type="submit" class="btn btn- btn-danger">Finalizar</button>
</form>
</ul>

<script>
  $('form[name="digitar-conferencia"]').submit(function (event) {
  event.preventDefault();
  $.ajax({
      url: '/blindColletion/save',
      type: 'post',
      data: $(this).serialize(),
      dataType: 'json',
      success: function (response) {

        if (response.success === true) {
          swal({
            title: response.message,
            icon: "success",
            buttons: true,
            dangerMode: true,
          })          
        } else {
      swal({
        title: "Bispo & Dantas",
        text: response.message,
        icon: "error",
        showCancelButton: false,
        showConfirmButton: false,
        type: "danger"
      });
    }

  }
  })
  });
</script>

</div>