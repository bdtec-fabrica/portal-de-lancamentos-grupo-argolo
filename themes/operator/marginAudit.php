<?php 
session_start();
$v->layout("client/_theme");

?>

<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Auditoria de Margens</h1>
        <p class="lead">Demonstrativo das margens geradas.</p>
    </div>
</div>
<div class="container" style="margin-bottom:5%">
    <form method="post" action="/auditoria-margem/search" class="row gx-3 gy-2 align-items-center"
        style="margin-bottom:2%">
        <div class="col-sm-6">
        <label for="specificSizeInputName">Loja</label>
            <select class="form-control" name="store">
              <option selected value="">Todas...</option>
              <?php foreach($groups as $group):
              ?>
              <option value="<?=$group->code?>"><?=$group->code." - ".$group->company_name?></option>
              <?php endforeach;?>
            </select>
          </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Nota Fiscal</label>
            <input name="nota" type="text" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Data agenda inicial</label>
            <input name="initialDate" type="date" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Data agenda final</label>
            <input name="finalDate" type="date" class="form-control" id="specificSizeInputName">
        </div>
        <div class="col-sm-2">
            <label for="specificSizeInputName">Margem ></label>
            <input type="number" name="maior" class="form-control">
        </div>

        <div class="col-sm-2">
            <label for="specificSizeInputName">Margem <</label>
            <input type="number" name="menor" class="form-control">
        </div>
        

        <div class="col-auto">
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </div>
    </form>
</div>
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th>Loja</th>
            <th>Nota</th>
            <th>Codigo</th>
            <th>Descrição</th>
            <th>Custo Bruto (R$)</th>
            <th>Custo Liquido (R$)</th>
            <th>ICMS (%)</th>
            <th>PIS/COFINS (%)</th>
            <th>Margem (%)</th>
            <th>Preço (R$)</th>
            
        </tr>
    </thead>
    <tbody>
            <?php
        foreach ($ag1iensa as $movment):
            if($movment->validateStore):
                if($movment->getMargin($movment->ESCLC_CODIGO) >= $maior || $movment->getMargin($movment->ESCLC_CODIGO) <= $menor):
                    if (!$operador):
               ?>
                        <tr>
                            <td><?=$movment->ESCLC_CODIGO?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                           <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCLC_CODIGO),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin($movment->ESCLC_CODIGO))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice,2))?></td>
                            
                        </tr>
               <?php  
                    else:
                        if($operador == $movment->getAA1CFISC()->getNfReceipt()->operator_id):
                            ?>
                        <tr>
                            <td><?=$movment->ESCLC_CODIGO?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                           <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCLC_CODIGO),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin($movment->ESCLC_CODIGO))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice,2))?></td>
                            
                        </tr>
               <?php 
                        endif;
                    endif;               
               endif;               
            else:
               
            endif;
            
        endforeach;
        ?>
         <?php
        foreach ($ag1iensa2 as $movment):
            if($movment->validateStore):
                if($movment->getMargin($movment->ESCLC_CODIGO) >= $maior || $movment->getMargin($movment->ESCLC_CODIGO) <= $menor):
                    if (!$operador):
               ?>
                        <tr>
                            <td><?=$movment->ESCLC_CODIGO?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                           <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCLC_CODIGO),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin($movment->ESCLC_CODIGO))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice,2))?></td>
                            
                        </tr>
               <?php  
                    else:
                        if($operador == $movment->getAA1CFISC()->getNfReceipt()->operator_id):
                            ?>
                        <tr>
                            <td><?=$movment->ESCLC_CODIGO?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                           <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCLC_CODIGO),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin($movment->ESCLC_CODIGO))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice,2))?></td>
                            
                        </tr>
               <?php 
                        endif;
                    endif;               
               endif;               
            else:
               
            endif;
            
        endforeach;
        ?>
                 <?php
        foreach ($ag1iensa3 as $movment):
            if($movment->validateStore):
                if($movment->getMargin($movment->ESCLC_CODIGO) >= $maior || $movment->getMargin($movment->ESCLC_CODIGO) <= $menor):
                    if (!$operador):
               ?>
                        <tr>
                            <td><?=$movment->ESCLC_CODIGO?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                           <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCLC_CODIGO),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin($movment->ESCLC_CODIGO))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice,2))?></td>
                            
                        </tr>
               <?php  
                    else:
                        if($operador == $movment->getAA1CFISC()->getNfReceipt()->operator_id):
                            ?>
                        <tr>
                            <td><?=$movment->ESCLC_CODIGO?></td>
                            <td><?=$movment->ESCHC_NRO_NOTA?></td>
                            <td><?=$movment->ESITC_CODIGO.$movment->ESITC_DIGITO?></td>
                            <td><?=$movment->getItemRegister()->GIT_DESCRICAO?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_PRC_UN,2))?></td>
                            <td><?=str_replace(".",",",round($movment->ENTSAIC_CUS_UN,2))?></td>
                           <td><?=str_replace(".",",",round($movment->getICMSAliquot($movment->ESCLC_CODIGO),2))?></td>
                            <td><?=str_replace(".",",",round($movment->getPISCOFINSAliquot,2))?></td>
                            <td><?=round($movment->getMargin($movment->ESCLC_CODIGO))?></td>
                            <td><?=str_replace(".",",",round($movment->getPrice,2))?></td>
                            
                        </tr>
               <?php 
                        endif;
                    endif;               
               endif;               
            else:
               
            endif;
            
        endforeach;
        ?>

    </tbody>
</table>