<?php
$v->layout("client/_theme");
?>
<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Painel Nota Fiscal Eletronica BDTec</h1>
                <p class="lead"></p>
            </div>
        </div>
        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
            data-bs-toggle="collapse" data-bs-target="#multiCollapseExample1" aria-expanded="false"
            aria-controls="multiCollapseExample1">Filtro
            ↴</button>
    </div>
    <div class="collapse multi-collapse container" id="multiCollapseExample1">
        <form action="/nfSefaz/search" method="post">
            <div class="row">
                <div class="form-group col-md-1">
                    <span>#Codigo</span>
                    <input name="id" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Número</span>
                    <input name="nfNumber" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>CNPJ</span>
                    <input name="cnpj" class="form-control" type="text">
                </div>
                <div class="form-group col-md-5">
                    <span>Chave Acesso</span>
                    <input name="accessKey" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Emissão inicial</span>
                    <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Emissão final</span>
                    <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Status</span>
                    <select name="status" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <option value="0">Não Postada</option>
                        <option value="2">Postada</option>
                    </select>
                </div>
                <div class="form-group col-md-5">
                <span>Loja</span>
                    <select name="store" class="form-control">
                        <option selected value="">Todas...</option>
                        <?php foreach($groups as $group):
                        ?>
                        <option value="<?=$group->code?>"><?=$group->code." - ".$group->company_name?></option>
                        <?php
                            endforeach;?>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <span>Manifestação no portal</span>
                    <select class="form-control" name="evento">
                        <option value='' selected diabled>Selecione ...</option>
                        <option value="210200">Confirmação da Operação</option>
                        <option value="210210">Ciência da Operação</option>
                        <option value="210220">Desconhecimento da Operação</option>
                        <option value="210240">Operação não Realizada</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <span>Lançadas no RMS</span>
                    <select class="form-control" name="lancada">]
                        <option value='' selected diabled>Selecione ...</option>
                        <option value="1">Não</option>
                        <option value='2'>Sim</option>
                    </select>
                </div>

            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <button class='btn btn-primary' onclick='openWin()'>Busca Sefaz</button>
    <div class="container" style="margin-bottom:3%;margin-top:3%">
    <?php if (!$c2):?>
        <button type="button" class="btn btn-secondary btn-lg" data-bs-toggle="modal"
            data-bs-target="#adicionar">Postagem Manual</button>
    <?php endif;?>
    </div>
    <div class="modal fade" id="adicionar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Postagem Manual</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="formUpload" id="formUpload" method="post">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Número do documento:</label>
                            <input required name="nfNumber" type="number" class="form-control" id="recipient-name">
                            <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                                id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                            <input name="accessKey" type="text" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de documento:</label>
                                <select class="form-select" name="type" required>
                                    <option value="1">Avulsa</option>
                                </select>
                            </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Quantidade de itens</label>
                            <input required name="itemsQuantity" type="number" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">1º Vencimento</label>
                            <input required name="dueDate1" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">2º Vencimento</label>
                            <input required name="dueDate2" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">3º Vencimento</label>
                            <input required name="dueDate3" type="date" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Observações:</label>
                            <textarea required name="note" class="form-control" id="message-text"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Prioridade:</label>
                            <select class="form-select" name="nfeOperation">
                                <option value="99">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <span class="input-group-text" for="inputGroupFile01">Arquivo</span>
                                <input name="files[]" multiple type="file" class="form-control" id="inputGroupFile01">
                            </div>
                        </div>
                        <progress style="width:100%" class="progress-bar" value="0" max="100"></progress><span
                            id="porcentagem">0%</span>
                        <div id="resposta" style="width:100%"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="button" id="btnEnviar" class="btn btn-primary">Enviar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#btnEnviar').click(function () {
                $('#formUpload').ajaxForm({
                    uploadProgress: function (event, position, total, percentComplete) {
                        $('progress').attr('value', percentComplete);
                        $('#porcentagem').html(percentComplete + '%');
                    },
                    success: function (data) {
                        $('progress').attr('value', '100');
                        $('#porcentagem').html('100%');
                        if (data.sucesso == true) {
                            $('#resposta').html(
                                '<div class="alert alert-success" role="alert">' + data
                                .msg + '</div>');
                            document.location.reload(true);
                        } else {
                            $('#resposta').html(
                                '<div class="alert alert-danger" role="alert">' + data
                                .msg +
                                '</div>');
                        }
                    },
                    error: function () {
                        $('#resposta').html(
                            '<div class="alert alert-danger" role="alert">Erro ao enviar requisição!!!</div>'
                        );
                    },
                    dataType: 'json',
                    url: '/request/addRequest',
                    resetForm: true
                }).submit();
            })
        })
    </script>
    <script language='javascript' type='text/javascript'>
        function openWin() {
            myWindow = window.open('http://lancamentosnfe.bdtecsolucoes.com.br:8090/nfSefaz/sefazSearch/<?=$store?>',
                '_blank', 'width=300, height=300');
        }
    </script>
    <?php if ($paginator): ?>
    <h5 style="margin-left:2%;margin-top:2%">Página <?= $paginator->page(); ?> de <?= $paginator->pages(); ?> </h5>
    <?php endif; ?>


    <div class="table-responsive-lg" style="padding-left:2%;padding-right:4%">
        <table class="table dt-responsive table-hover nowrap w-100 sortable" style="font-size:75%">
            <thead class="">
                <tr>
                    <th>#</th>
                    <th>Loja</th>
                    <th>Número</th>
                    <th>Razão Social</th>
                    <th>Emissão</th>
                    <th>Chave de Acesso</th>
                    <th>NSU</th>
                    <th>Status</th>
                    <th>Visualizar</th>
                    <th>Postar</th>
                    <th>Manifestar</th>
                </tr>
            </thead>
            <tbody>

                <?php
            if ($nfes):
            foreach ($nfes as $nfe):
                if($status == 2):
                    if($nfe->getStatus == 2):
            ?>
                <tr <?php if ($nfe->getStatus == 2):
                ?> style="background:green" <?php endif;
                ?>>
                    <th scope="row"><?=$nfe->id?></th>
                    <td>Loja</td>
                    <td><?= $nfe->nfe_number?></td>
                    <td><?= $nfe->getFornecedor?></td>
                    <td><?= $nfe->emission_date?></td>
                    <td><?= $nfe->access_key?></td>
                    <td><?= $nfe->nsu?></td>
                    <td><?= $nfe->status_id?></td>
                    <td><button <?php if ($nfe->getStatus == 2):
                ?> disabled <?php endif;  ?> type="button" data-bs-placement="top" title="Visualizar"
                            class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#visualizar<?=$nfe->id?>"
                            data-whatever="@mdo"><i class="ion ion-eye"></i></button>
                        <div class="modal fade" id="visualizar<?=$nfe->id?>" tabindex="-1"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $nfe->nfe_number?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $nfe->access_key?>">
                                        </div>

                                        <?php if (file_exists(__DIR__."/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml")): ?>
                                        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#multiCollapseExample<?=$nfe->id?>" aria-expanded="false"
                                            aria-controls="multiCollapseExample1">Itens
                                            ↴</button>

                                    </div>
                                    <div class="collapse multi-collapse container"
                                        id="multiCollapseExample<?=$nfe->id?>">
                                        <div class="form-group">
                                            <div class="container">
                                                <?php foreach ($nfe->getItems as $det): ?>
                                                <div class="row border">
                                                    <div class="col border">
                                                        <?= $det->prod->cProd?>
                                                    </div>
                                                    <div class="col border">
                                                        <?= $det->prod->cEAN?>
                                                    </div>
                                                    <div class="col border">
                                                        <?= $det->prod->xProd?>
                                                    </div>
                                                </div> <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row container">
                                            <a download style="text-decoration:none;margin-bottom:5%"
                                                href="<?="/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml"?>"><button
                                                    class="btn btn-outline-success">XML <i
                                                        class="ion ion-android-download"></i></button></a>
                                            <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                                <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                                <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                                <a>
                                                    <button style="margin-bottom:5%" type="submit"
                                                        class="btn btn-outline-warning">PDF <i
                                                            class="ion ion-android-download"></i></button></a>
                                            </form>
                                            <form name="reprocessar<?=$nfe->id?>">
                                                <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                                <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                                <a>
                                                    <button type="submit" class="btn btn-outline-info">Troca Codigo<i
                                                            class="ion ion-android-download"></i></button></a>
                                            </form>
                                            <script>
                                                $('form[name="reprocessar<?=$nfe->id?>"]').submit(function (event) {
                                                    event.preventDefault();
                                                    $.ajax({
                                                        url: '/request/trocaCodigo/nfe',
                                                        type: 'post',
                                                        data: $(this).serialize(),
                                                        dataType: 'json',
                                                        success: function (response) {
                                                            if (response.success === true) {
                                                                swal({
                                                                    title: "Bispo & Dantas",
                                                                    text: response.message,
                                                                    timer: 20000,
                                                                    icon: "success",
                                                                    showCancelButton: false,
                                                                    showConfirmButton: false,
                                                                    type: "success"
                                                                });
                                                            } else {
                                                                swal({
                                                                    title: "Bispo & Dantas",
                                                                    text: response.message,
                                                                    icon: "error",
                                                                    showCancelButton: false,
                                                                    showConfirmButton: false,
                                                                    type: "danger"
                                                                });
                                                            }
                                                        }
                                                    })
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <?php endif; ?>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Voltar</button>
                                </div>
                            </div>
                        </div>
    </div>

    </td>
    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap">Postar</button>
        <div class="modal fade" id="exampleModal<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Postar NFE</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="adicionar<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do
                                    documento:</label>
                                <input required readonly name="nfNumber" value="<?= $nfe->nfe_number?>" type="number"
                                    class="form-control" id="recipient-name">
                                <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="accessKey" type="text" readonly value="<?= $nfe->access_key?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de
                                    documento:</label>
                                <select class="form-select" name="type" required>
                                    <option value="">Selecione um tipo</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 1:</label>
                                <input name="dueDate1" type="date" value="<?=$nfe->getDueDate[0]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 2:</label>
                                <input name="dueDate2" type="date" value="<?= $nfe->getDueDate[1]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 3:</label>
                                <input name="dueDate3" type="date" value="<?= $nfe->getDueDate[2]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Observações:</label>
                                <textarea name="note" class="form-control" id="message-text"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                <select class="form-select" name="nfeOperation">
                                    <option value="99">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                </div>
            </div>
            <script>
                $('form[name="adicionar<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    // document.getElementById('blanket').style.display = 'block';document.getElementById('aguarde').style.display = 'block';
                    $.ajax({
                        url: '/request/addRequestAuto',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    <td>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#manifestar<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap">Manifestar</button>
        <div class="modal fade" id="manifestar<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Manifestar NFE</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="manifestacao<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Codigo Loja</label>
                                <input class="form-control" value="<?=$nfe->store_id?>" name="store" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Chave de Acesso</label>
                                <input class="form-control" value="<?=$nfe->access_key?>" name="key" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Tipo de Evento</label>
                                <select class="form-control" id="select" name="evento" onchange="atribuir()">
                                    <option value='' selected diabled>Selecione ...</option>
                                    <option value="210200">Confirmação da Operação</option>
                                    <option value="210210">Ciência da Operação</option>
                                    <option value="210220">Desconhecimento da Operação</option>
                                    <option value="210240">Operação não Realizada</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Justificativa</label>
                                <textarea name="xJust" class="form-control" id="text" rows="3" required
                                    readonly></textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                    <script>
                        function atribuir() {
                            var select = document.getElementById('select');
                            var value = select.options[select.selectedIndex].value;

                            if (value == 210200) {
                                document.getElementById('text').value = "Nota Fiscal Escriturada com Sucesso";
                            } else if (value == 210210) {
                                document.getElementById('text').value = "Empresa ciente da operacao desta NFe";
                            } else if (value == 210220) {
                                document.getElementById('text').value = "Empresa desconhece esta operacao desta NFe";
                            } else if (value == 210240) {
                                document.getElementById('text').value =
                                    "A operação referente a esta NFe não foi realizada";
                            }

                        }
                    </script>
                </div>
            </div>
            <script>
                $('form[name="manifestacao<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/request/manifestacao/nfe',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    </tr>
    <?php       endif;
            elseif($status == 0):
                if($nfe->getStatus != 2):
                    ?>
                <tr <?php if ($nfe->getStatus == 2):
                ?> style="background:green" <?php endif;
                ?>>
                    <th scope="row"><?=$nfe->id?></th>
                    <td><?= $nfe->store_id?></td>
                    <td><?= $nfe->nfe_number?></td>
                    <td><?= $nfe->getFornecedor?></td>
                    <td><?= $nfe->emission_date?></td>
                    <td><?= $nfe->access_key?></td>
                    <td><?= $nfe->nsu?></td>
                    <td><?= $nfe->status_id?></td>
                    <td><button <?php if ($nfe->getStatus == 2):
                ?> disabled <?php endif;  ?> type="button" data-bs-placement="top" title="Visualizar"
                            class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#visualizar<?=$nfe->id?>"
                            data-whatever="@mdo"><i class="ion ion-eye"></i></button>
                        <div class="modal fade" id="visualizar<?=$nfe->id?>" tabindex="-1"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $nfe->nfe_number?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $nfe->access_key?>">
                                        </div>

                                        <?php if (file_exists(__DIR__."/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml")): ?>
                                        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#multiCollapseExample<?=$nfe->id?>" aria-expanded="false"
                                            aria-controls="multiCollapseExample1">Itens
                                            ↴</button>

                                    </div>
                                    <div class="collapse multi-collapse container"
                                        id="multiCollapseExample<?=$nfe->id?>">
                                        <div class="form-group">
                                            <div class="container">
                                                <?php foreach ($nfe->getItems as $det): ?>
                                                <div class="row border">
                                                    <div class="col border">
                                                        <?= $det->prod->cProd?>
                                                    </div>
                                                    <div class="col border">
                                                        <?= $det->prod->cEAN?>
                                                    </div>
                                                    <div class="col border">
                                                        <?= $det->prod->xProd?>
                                                    </div>
                                                </div> <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row container">
                                            <a download style="text-decoration:none;margin-bottom:5%"
                                                href="<?="/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml"?>"><button
                                                    class="btn btn-outline-success">XML <i
                                                        class="ion ion-android-download"></i></button></a>
                                            <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                                <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                                <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                                <a>
                                                    <button style="margin-bottom:5%" type="submit"
                                                        class="btn btn-outline-warning">PDF <i
                                                            class="ion ion-android-download"></i></button></a>
                                            </form>
                                            <form name="reprocessar<?=$nfe->id?>">
                                                <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                                <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                                <a>
                                                    <button type="submit" class="btn btn-outline-info">Troca Codigo<i
                                                            class="ion ion-android-download"></i></button></a>
                                            </form>
                                            <script>
                                                $('form[name="reprocessar<?=$nfe->id?>"]').submit(function (event) {
                                                    event.preventDefault();
                                                    $.ajax({
                                                        url: '/request/trocaCodigo/nfe',
                                                        type: 'post',
                                                        data: $(this).serialize(),
                                                        dataType: 'json',
                                                        success: function (response) {
                                                            if (response.success === true) {
                                                                swal({
                                                                    title: "Bispo & Dantas",
                                                                    text: response.message,
                                                                    timer: 20000,
                                                                    icon: "success",
                                                                    showCancelButton: false,
                                                                    showConfirmButton: false,
                                                                    type: "success"
                                                                });
                                                            } else {
                                                                swal({
                                                                    title: "Bispo & Dantas",
                                                                    text: response.message,
                                                                    icon: "error",
                                                                    showCancelButton: false,
                                                                    showConfirmButton: false,
                                                                    type: "danger"
                                                                });
                                                            }
                                                        }
                                                    })
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <?php endif; ?>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Voltar</button>
                                </div>
                            </div>
                        </div>
    </div>

    </td>
    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap">Postar</button>
        <div class="modal fade" id="exampleModal<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Postar NFE</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="adicionar<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do
                                    documento:</label>
                                <input required readonly name="nfNumber" value="<?= $nfe->nfe_number?>" type="number"
                                    class="form-control" id="recipient-name">
                                <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="accessKey" type="text" readonly value="<?= $nfe->access_key?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de
                                    documento:</label>
                                <select class="form-select" name="type" required>
                                    <option value="">Selecione um tipo</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 1:</label>
                                <input name="dueDate1" type="date" value="<?=$nfe->getDueDate[0]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 2:</label>
                                <input name="dueDate2" type="date" value="<?= $nfe->getDueDate[1]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 3:</label>
                                <input name="dueDate3" type="date" value="<?= $nfe->getDueDate[2]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Observações:</label>
                                <textarea name="note" class="form-control" id="message-text"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                <select class="form-select" name="nfeOperation">
                                    <option value="99">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                </div>
            </div>
            <script>
                $('form[name="adicionar<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    // document.getElementById('blanket').style.display = 'block';document.getElementById('aguarde').style.display = 'block';
                    $.ajax({
                        url: '/request/addRequestAuto',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    <td>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#manifestar<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap">Manifestar</button>
        <div class="modal fade" id="manifestar<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Manifestar NFE</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="manifestacao<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Codigo Loja</label>
                                <input class="form-control" value="<?=$nfe->store_id?>" name="store" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Chave de Acesso</label>
                                <input class="form-control" value="<?=$nfe->access_key?>" name="key" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Tipo de Evento</label>
                                <select class="form-control" id="select" name="evento" onchange="atribuir()">
                                    <option value='' selected diabled>Selecione ...</option>
                                    <option value="210200">Confirmação da Operação</option>
                                    <option value="210210">Ciência da Operação</option>
                                    <option value="210220">Desconhecimento da Operação</option>
                                    <option value="210240">Operação não Realizada</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Justificativa</label>
                                <textarea name="xJust" class="form-control" id="text" rows="3" required
                                    readonly></textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                    <script>
                        function atribuir() {
                            var select = document.getElementById('select');
                            var value = select.options[select.selectedIndex].value;

                            if (value == 210200) {
                                document.getElementById('text').value = "Nota Fiscal Escriturada com Sucesso";
                            } else if (value == 210210) {
                                document.getElementById('text').value = "Empresa ciente da operacao desta NFe";
                            } else if (value == 210220) {
                                document.getElementById('text').value = "Empresa desconhece esta operacao desta NFe";
                            } else if (value == 210240) {
                                document.getElementById('text').value =
                                    "A operação referente a esta NFe não foi realizada";
                            }

                        }
                    </script>
                </div>
            </div>
            <script>
                $('form[name="manifestacao<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/request/manifestacao/nfe',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    </tr>


                    <?php
                    endif;
                else:
                    ?>
                    <tr <?php if ($nfe->getStatus == 2):
                ?> style="background:green" <?php endif;
                ?>>
                    <th scope="row"><?=$nfe->id?></th>
                    <td><?= $nfe->store_id?></td>
                    <td><?= $nfe->nfe_number?></td>
                    <td><?= $nfe->getFornecedor?></td>
                    <td><?= $nfe->emission_date?></td>
                    <td><?= $nfe->access_key?></td>
                    <td><?= $nfe->nsu?></td>
                    <td><?= $nfe->status_id?></td>
                    <td><button <?php if ($nfe->getStatus == 2):
                ?> disabled <?php endif;  ?> type="button" data-bs-placement="top" title="Visualizar"
                            class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#visualizar<?=$nfe->id?>"
                            data-whatever="@mdo"><i class="ion ion-eye"></i></button>
                        <div class="modal fade" id="visualizar<?=$nfe->id?>" tabindex="-1"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Detalhes da Solicitação</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número do
                                                documento:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $nfe->nfe_number?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                            <input name="title" disabled type="text" class="form-control"
                                                id="recipient-name" value="<?= $nfe->access_key?>">
                                        </div>

                                        <?php if (file_exists(__DIR__."/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml")): ?>
                                        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#multiCollapseExample<?=$nfe->id?>" aria-expanded="false"
                                            aria-controls="multiCollapseExample1">Itens
                                            ↴</button>

                                    </div>
                                    <div class="collapse multi-collapse container"
                                        id="multiCollapseExample<?=$nfe->id?>">
                                        <div class="form-group">
                                            <div class="container">
                                                <?php foreach ($nfe->getItems as $det): ?>
                                                <div class="row border">
                                                    <div class="col border">
                                                        <?= $det->prod->cProd?>
                                                    </div>
                                                    <div class="col border">
                                                        <?= $det->prod->cEAN?>
                                                    </div>
                                                    <div class="col border">
                                                        <?= $det->prod->xProd?>
                                                    </div>
                                                </div> <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row container">
                                            <a download style="text-decoration:none;margin-bottom:5%"
                                                href="<?="/../../source/XML/$nfe->store_id/NFe$nfe->access_key.xml"?>"><button
                                                    class="btn btn-outline-success">XML <i
                                                        class="ion ion-android-download"></i></button></a>
                                            <form target="_blank" action="/nfSefaz/pdf/download" method="POST">
                                                <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                                <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                                <a>
                                                    <button style="margin-bottom:5%" type="submit"
                                                        class="btn btn-outline-warning">PDF <i
                                                            class="ion ion-android-download"></i></button></a>
                                            </form>
                                            <form name="reprocessar<?=$nfe->id?>">
                                                <input type="hidden" name="key" value="<?=$nfe->access_key?>">
                                                <input type="hidden" name="store" value="<?=$nfe->store_id?>">
                                                <a>
                                                    <button type="submit" class="btn btn-outline-info">Troca Codigo<i
                                                            class="ion ion-android-download"></i></button></a>
                                            </form>
                                            <script>
                                                $('form[name="reprocessar<?=$nfe->id?>"]').submit(function (event) {
                                                    event.preventDefault();
                                                    $.ajax({
                                                        url: '/request/trocaCodigo/nfe',
                                                        type: 'post',
                                                        data: $(this).serialize(),
                                                        dataType: 'json',
                                                        success: function (response) {
                                                            if (response.success === true) {
                                                                swal({
                                                                    title: "Bispo & Dantas",
                                                                    text: response.message,
                                                                    timer: 20000,
                                                                    icon: "success",
                                                                    showCancelButton: false,
                                                                    showConfirmButton: false,
                                                                    type: "success"
                                                                });
                                                            } else {
                                                                swal({
                                                                    title: "Bispo & Dantas",
                                                                    text: response.message,
                                                                    icon: "error",
                                                                    showCancelButton: false,
                                                                    showConfirmButton: false,
                                                                    type: "danger"
                                                                });
                                                            }
                                                        }
                                                    })
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <?php endif; ?>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Voltar</button>
                                </div>
                            </div>
                        </div>
    </div>

    </td>
    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap">Postar</button>
        <div class="modal fade" id="exampleModal<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Postar NFE</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="adicionar<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do
                                    documento:</label>
                                <input required readonly name="nfNumber" value="<?= $nfe->nfe_number?>" type="number"
                                    class="form-control" id="recipient-name">
                                <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="accessKey" type="text" readonly value="<?= $nfe->access_key?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de
                                    documento:</label>
                                <select class="form-select" name="type" required>
                                    <option value="">Selecione um tipo</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 1:</label>
                                <input name="dueDate1" type="date" value="<?=$nfe->getDueDate[0]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 2:</label>
                                <input name="dueDate2" type="date" value="<?= $nfe->getDueDate[1]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 3:</label>
                                <input name="dueDate3" type="date" value="<?= $nfe->getDueDate[2]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Observações:</label>
                                <textarea name="note" class="form-control" id="message-text"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                <select class="form-select" name="nfeOperation">
                                    <option value="99">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                </div>
            </div>
            <script>
                $('form[name="adicionar<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    // document.getElementById('blanket').style.display = 'block';document.getElementById('aguarde').style.display = 'block';
                    $.ajax({
                        url: '/request/addRequestAuto',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    <td>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#manifestar<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap">Manifestar</button>
        <div class="modal fade" id="manifestar<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Manifestar NFE</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="manifestacao<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Codigo Loja</label>
                                <input class="form-control" value="<?=$nfe->store_id?>" name="store" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Chave de Acesso</label>
                                <input class="form-control" value="<?=$nfe->access_key?>" name="key" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Tipo de Evento</label>
                                <select class="form-control" id="select" name="evento" onchange="atribuir()">
                                    <option value='' selected diabled>Selecione ...</option>
                                    <option value="210200">Confirmação da Operação</option>
                                    <option value="210210">Ciência da Operação</option>
                                    <option value="210220">Desconhecimento da Operação</option>
                                    <option value="210240">Operação não Realizada</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Justificativa</label>
                                <textarea name="xJust" class="form-control" id="text" rows="3" required
                                    readonly></textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                    <script>
                        function atribuir() {
                            var select = document.getElementById('select');
                            var value = select.options[select.selectedIndex].value;

                            if (value == 210200) {
                                document.getElementById('text').value = "Nota Fiscal Escriturada com Sucesso";
                            } else if (value == 210210) {
                                document.getElementById('text').value = "Empresa ciente da operacao desta NFe";
                            } else if (value == 210220) {
                                document.getElementById('text').value = "Empresa desconhece esta operacao desta NFe";
                            } else if (value == 210240) {
                                document.getElementById('text').value =
                                    "A operação referente a esta NFe não foi realizada";
                            }

                        }
                    </script>
                </div>
            </div>
            <script>
                $('form[name="manifestacao<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/request/manifestacao/nfe',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    </tr>
<?php       endif;
            endforeach;
            endif;
            ?>
    </tbody>
    </table>
    </div>
    <?php if ($paginator):
    ?>
    <div style="margin-left:2%;margin-bottom:5%"><?= $paginator->render();?><div>
            <?php endif;
    ?>
</main>