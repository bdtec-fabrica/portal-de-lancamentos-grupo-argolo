<?php 
$v->layout("operator/operator_theme");
?>
<main class="main_content">
    <div class="container">
        <div class="bg-light p-5" style="margin-bottom:5%">
            <div class="container text-center">
                <h1 class="display-4">Painel Nota Fiscal Eletronica BDTec</h1>
                <p class="lead"></p>
            </div>
        </div>
        <button class="btn btn-secondary" style="margin-left:2%;margin-bottom:2%" type="button"
            data-bs-toggle="collapse" data-bs-target="#multiCollapseExample1" aria-expanded="false"
            aria-controls="multiCollapseExample1">Filtro
            ↴</button>
    </div>
    <div class="collapse multi-collapse container" id="multiCollapseExample1">
        <form action="/nfSefaz/search" method="post">
            <div class="row">
                <div class="form-group col-md-1">
                    <span>#Codigo</span>
                    <input name="id" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Número</span>
                    <input name="nfNumber" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>CNPJ</span>
                    <input name="cnpj" class="form-control" type="text">
                </div>
                <div class="form-group col-md-5">
                    <span>Chave Acesso</span>
                    <input name="accessKey" class="form-control" type="text">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação inicial</span>
                    <input name="criacaoInicial" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Criação final</span>
                    <input name="criacaoFinal" style="font-size:80%" class="form-control" type="date">
                </div>
                <div class="form-group col-md-2">
                    <span>Status</span>
                    <select name="status" class="form-control" id="inputGroupSelect01">
                        <option selected disabled>Todos</option>
                        <option value="0">Não Postada</option>
                        <option value="2">Postada</option>
                    </select>
                </div>
                <div class="form-group col-md-1">
                    <span>Loja</span>
                    <input class="form-control" name="store">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <button class='btn btn-primary' style="margin:2%" onclick='openWin()'>Busca Sefaz</button>
    <button class="btn btn-outline-success" style="margin:2%" data-bs-toggle="modal"
        data-bs-target="#exampleModal">Adicionar Notas
        Conferência Cega</button>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Adicionar Notas
                        Conferência Cega</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form name="newColletion">
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Loja:</label>
                            <input name="store" type="text" class="form-control" id="recipient-name" required>
                        </div>
                            <div class="form-group">
                                <table class="table table-dark table-striped">
                                    <thead>
                                        <tr>
                                            <th>Chave de Acesso NFe</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="linhas" class="linhas">
                                        <tr>
                                            <td><input id="cad" class="form-control" name="accessKey[1]" required></td>
                                            <td><button type="button" class="btn btn-secondary" id="add-campo"> +
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Gravar</button>
                    </form>
                    <script>
                        var cont = 1;
                        $("#add-campo").click(function () {
                            cont++;
                            $("#linhas").append('<tr id="campo' + cont + '">' +
                                '<td><input id="cad" class="form-control" name="accessKey[' + cont +
                                ']" required></td>' +
                                '<td><button type="button" id="' + cont +
                                '" class="btn btn-outline-danger btn-apagar"> - </button></td>' +
                                '</tr>');
                        });

                        $("#linhas").on("click", ".btn-apagar", function () {
                            var button_id = $(this).attr("id");
                            $('#campo' + button_id).remove();
                        });
                    </script>
                    <script>
                        $('form[name="newColletion"]').submit(function (event) {
                            event.preventDefault();
                            $.ajax({
                                url: '/blindColletion',
                                type: 'post',
                                data: $(this).serialize(),
                                dataType: 'json',
                                success: function (response) {
                                    if (response.success === true) {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            timer: 20000,
                                            icon: "success",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "success"
                                        });
                                        document.location.reload(true);
                                    } else {
                                        swal({
                                            title: "Bispo & Dantas",
                                            text: response.message,
                                            icon: "error",
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            type: "danger"
                                        });
                                    }
                                }
                            })
                        });
                    </script>


                </div>
            </div>
        </div>
    </div>
    <script language='javascript' type='text/javascript'>
        function openWin() {
            myWindow = window.open('https://novomixcamacari.bispoedantas.com.br/nfSefaz/sefazSearch/<?=$store?>',
                '_blank', 'width=300, height=300');
        }
    </script>
    <button type="button" style="margin:2%" class="btn btn-primary" data-bs-toggle="modal"
        data-bs-target="#pendentColletion">Conferências</button>

    <div class="modal fade" id="pendentColletion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Conferências</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="mb-3">
                        <table class="table" style="font-size:70%">
                            <thead>
                                <tr>
                                    <th>#Id</th>
                                    <th>Loja</th>
                                    <th>Razão Social do Fornecedor</th>
                                    <th>Inclusão</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($collectionHeaders as $collectionHeader): 
                              
                            ?>
                                <tr <?php if ($collectionHeader->getStatus == 'E'):
                                ?> class="bg-danger" <?php elseif($collectionHeader->getStatus == 'P'):
                                ?> class="bg-warning" <?php elseif($collectionHeader->getStatus == 'C'):
                                ?> class="bg-success" <?php endif;
                                ?>>
                                    <td><a
                                            href="/blindColletion/<?=$collectionHeader->id?>"><?=$collectionHeader->id?></a>
                                    </td>
                                    <td><?=$collectionHeader->store_id?></td>
                                    <td><?=$collectionHeader->razao?></td>
                                    <td><?=$collectionHeader->created_at?></td>
                                </tr>
                                <?php 
                        endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Voltar</button>
                </div>
            </div>
        </div>
    </div>
    <?php if ($paginator): ?>
    <h5 style="margin-left:2%;margin-top:2%">Página <?= $paginator->page(); ?> de <?= $paginator->pages(); ?> </h5>
    <?php endif; ?>


    <div class="table-responsive-lg" style="padding-left:2%;padding-right:4%">
        <table class="table dt-responsive table-hover nowrap w-100 sortable" style="font-size:75%">
            <thead class="">
                <tr>
                    <th>#</th>
                    <th>Loja</th>
                    <th>Número</th>
                    <th>Razão Social</th>
                    <th>Emissão</th>
                    <th>Chave de Acesso</th>
                    <th>NSU</th>
                    <th>Status</th>
                    <th>Postar</th>
                </tr>
            </thead>
            <tbody>

                <?php 
            if ($nfes):
            foreach ($nfes as $nfe):
            ?>
                <tr <?php if ($nfe->getStatus == 2):
                ?> style="background:green" <?php endif; 
                ?>>
                    <th scope="row"><?=$nfe->id?></th>
                    <td><?= $nfe->store_id?></td>
                    <td><?= $nfe->nfe_number?></td>
                    <td><?= $nfe->getFornecedor?></td>
                    <td><?= $nfe->emission_date?></td>
                    <td><?= $nfe->access_key?></td>
                    <td><?= $nfe->nsu?></td>
                    <td><?= $nfe->status_id?></td>
    </div>

    </td>
    <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal<?=$nfe->id?>"
            data-bs-whatever="@getbootstrap">Postar</button>
        <div class="modal fade" id="exampleModal<?=$nfe->id?>" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Postar NFE</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form name="adicionar<?=$nfe->id?>">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Número do
                                    documento:</label>
                                <input required readonly name="nfNumber" value="<?= $nfe->nfe_number?>" type="number"
                                    class="form-control" id="recipient-name">
                                <input name="client" value="<?= $_COOKIE['id'] ?>" type="hidden" class="form-control"
                                    id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Chave de acesso:</label>
                                <input name="accessKey" type="text" readonly value="<?= $nfe->access_key?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tipo de
                                    documento:</label>
                                <select class="form-select" name="type" required>
                                    <option value="">Selecione um tipo</option>
                                    <?php foreach ($nfeType as $type): ?>
                                    <option value="<?=$type->id?>"><?=$type->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 1:</label>
                                <input name="dueDate1" type="date" value="<?=$nfe->getDueDate[0]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 2:</label>
                                <input name="dueDate2" type="date" value="<?= $nfe->getDueDate[1]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Vencimento 3:</label>
                                <input name="dueDate3" type="date" value="<?= $nfe->getDueDate[2]->dVenc?>"
                                    class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Observações:</label>
                                <textarea name="note" class="form-control" id="message-text"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Prioridade:</label>
                                <select class="form-select" name="nfeOperation">
                                    <option value="99">Não</option>
                                    <option value="2">Sim</option>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" name="cdi">Confirmar</button>

                    </div>
                    </form>
                </div>
            </div>
            <script>
                $('form[name="adicionar<?=$nfe->id?>"]').submit(function (event) {
                    event.preventDefault();
                    // document.getElementById('blanket').style.display = 'block';document.getElementById('aguarde').style.display = 'block';
                    $.ajax({
                        url: '/request/addRequestAuto',
                        type: 'post',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.success === true) {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    timer: 600,
                                    icon: "success",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "success"
                                });
                                document.location.reload(true);
                            } else {
                                swal({
                                    title: "Bispo & Dantas",
                                    text: response.message,
                                    icon: "error",
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    type: "danger"
                                });
                            }

                        }
                    })
                });
            </script>
    </td>
    </tr>
    <?php 
            // endif;
            endforeach;
            endif;
            ?>
    </tbody>
    </table>
    </div>
    <?php if ($paginator): 
    ?>
    <div style="margin-left:2%;margin-bottom:5%"><?= $paginator->render();?><div>
            <?php endif;
    ?>
</main>