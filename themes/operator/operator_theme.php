<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $title; ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= url("/themes/css/all.min.css");?>">
  <link rel="stylesheet" href="<?= url("/themes/css/nav.css");?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= url("/themes/css/OverlayScrollbars.min.css");?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= url("/themes/css/adminlte.min.css");?>">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= url("/themes/css/bootstrap.min.css");?>">
  <script src="<?= url("/themes/js/jquery-3.5.1.min.js");?>"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
    integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
    integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous">
  </script>
  <script src="<?= url("/themes/js/jquery.min.js");?>"></script>
  <script src="<?= url("/themes/js/jquery-ui.min.js");?>"></script>
  <script src="<?= url("/themes/js/bootstrap.bundle.min.js");?>"></script>
  <script src="<?= url("/themes/js/Chart.min.js");?>"></script>
  <script src="<?= url("/themes/js/sparkline.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.vmap.min.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.vmap.usa.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.knob.min.js");?>"></script>
  <script src="<?= url("/themes/js/moment.min.js");?>"></script>
  <script src="<?= url("/themes/js/daterangepicker.js");?>"></script>
  <script src="<?= url("/themes/js/tempusdominus-bootstrap-4.min.js");?>"></script>
  <script src="<?= url("/themes/js/summernote-bs4.min.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.overlayScrollbars.min.js");?>"></script>
  <script src="<?= url("/themes/js/adminlte.js");?>"></script>
  <script src="<?= url("/themes/js/dashboard.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.form.min.js");?>"></script>
  <script src="<?= url("/themes/js/sweetalert.min.js");?>"></script>
  <script src="<?= url("/themes/js/nav.js");?>"></script>
  <script src="<?= url("/themes/js/sortable.js");?>"></script>
  <script src="<?= url("/themes/js/demo.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.flot.resize.js");?>"></script>
  <script src="<?= url("/themes/js/jquery.flot.pie.js");?>"></script>
</head>

<body class="sidebar-mini layout-fixed sidebar-collapse">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-dark navbar-indigo">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button">≡</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="/" class="nav-link">Inicio</a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
          <a href="/logout" class="nav-link">Sair</a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="/" class="brand-link" style="text-decoration:none">
        <img src="<?= url("/themes/images/bd.jpeg");?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
          style="opacity: .8">
        <span class="brand-text font-weight-light">BDTec Lançamentos</span>
      </a>

      <!-- Sidebar -->
      <!-- <div class="sidebar">
         Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!-- <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> -->
          <!-- </div>
          <div class="info">
            <a href="#" class="d-block" style="text-decoration:none">Usuário</a>
          </div>
        </div> -->

          <!-- SidebarSearch Form -->
          <!-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li class="nav-header">Consultas Notas Fiscais</li>

              <li class="nav-item">
                <a href="/nfSefaz" class="nav-link">
                  <i class="ion ion-android-list"></i>
                  <p>
                    Painel NFe

                  </p>
                </a>
              </li>
            </ul>
          </nav>
          </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <!-- <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Fixed Layout</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Layout</a></li>
              <li class="breadcrumb-item active">Fixed Layout</li>
            </ol>
          </div>
        </div>
      </div> .container-fluid 
    </section> -->

      <!-- Main content -->
      <section class="content">

        <main class="main_content">
          <?=$v->section("content"); ?>
        </main>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <!-- <b>Version</b> 3.1.0 -->
      </div>
      <!-- <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved. -->
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->


</body>

</html>