<?php 
session_start();
$v->layout("administrator/adm_theme");

?>
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4">Registro de inventario</h1>
        <p class="lead">Digite apenas as quantidades existentes, quantidades zeradas podem ficar em branco</p>
    </div>
</div>
<form method="POST" action="/apuracao/salvaInventario">
        <input type="hidden" value="<?=$id?>" name="id">
<table class="table table-hover sortable">
    <thead>
        <tr>
            <th>Codigo</th>
            <th>Descrição</th>
            <th>EAN Principal</th>
            <th>Quantidade</th>
        </tr>
    </thead>
    <tbody>

        <?php foreach ($items as $item):
        ?>
        <tr>
            <td><?=$item->GIT_COD_ITEM?></td>
            <td><?=$item->GIT_DESCRICAO?></td>
            <td><?=$item->GIT_CODIGO_EAN13?></td>
            <input type="hidden" name="code[<?=$item->GIT_COD_ITEM?>]" value="<?=$item->GIT_COD_ITEM?>">
            <td><input class="form-control" type="number" name="amount<?=$item->GIT_COD_ITEM?>" step="any" ></td>
        </tr>
        <?php endforeach; ?>

        
    </tbody>
</table>

<button class="btn btn-primary" type="submit">Confirmar</button>

        
        </form>